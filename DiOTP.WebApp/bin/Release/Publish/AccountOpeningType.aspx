﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="AccountOpeningType.aspx.cs" Inherits="DiOTP.WebApp.AccountOpeningType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        .btn-account-type {
            font-size: 18px;
            width: 300px;
            position: relative;
        }

            .btn-account-type i {
                position: absolute;
                left: -1px;
                top: -1px;
                background: #61a4e6;
                color: #ffffff;
                padding: 10.5px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">Open Account</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h5 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Open Account</h5>
            </div>

            <div class="row mb-20">
                <div class="col-md-12">
                    <h3>Select to apply for new account</h3>
                    <div>
                        <a href="/OpenJointAccount.aspx" class="btn btn-primary btn-account-type">Joint Account</a>
                        <a href="/OpenAgentAccount.aspx" class="btn btn-primary btn-account-type">Agent Account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
