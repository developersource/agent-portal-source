﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_personal")]
    public class AgentRegPersonal
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_reg_id"), Display("agent_reg_id")]
        public Int32 agent_reg_id { get; set; }
        [Database("name"), Display("name")]
        public String name { get; set; }
        [Database("id_no"), Display("id_no")]
        public String id_no { get; set; }
        [Database("dob"), Display("dob")]
        public DateTime? dob { get; set; }
        [Database("mother_maiden_name"), Display("mother_maiden_name")]
        public String mother_maiden_name { get; set; }
        [Database("sex"), Display("sex")]
        public String sex { get; set; }
        [Database("race"), Display("race")]
        public String race { get; set; }
        [Database("religion"), Display("religion")]
        public String religion { get; set; }
        [Database("religion_others"), Display("religion_others")]
        public String religion_others { get; set; }
        [Database("highest_education"), Display("highest_education")]
        public String highest_education { get; set; }
        [Database("years_of_experience"), Display("years_of_experience")]
        public Int32 years_of_experience { get; set; }
        [Database("bankruptcy_declaration"), Display("bankruptcy_declaration")]
        public String bankruptcy_declaration { get; set; }
        [Database("mail_addr1"), Display("mail_addr1")]
        public String mail_addr1 { get; set; }
        [Database("mail_addr2"), Display("mail_addr2")]
        public String mail_addr2 { get; set; }
        [Database("mail_addr3"), Display("mail_addr3")]
        public String mail_addr3 { get; set; }
        [Database("mail_addr4"), Display("mail_addr4")]
        public String mail_addr4 { get; set; }
        [Database("post_code"), Display("post_code")]
        public String post_code { get; set; }
        [Database("state"), Display("state")]
        public String state { get; set; }
        [Database("country"), Display("country")]
        public String country { get; set; }
        [Database("tel_no_office"), Display("tel_no_office")]
        public String tel_no_office { get; set; }
        [Database("tel_no_home"), Display("tel_no_home")]
        public String tel_no_home { get; set; }
        [Database("hand_phone"), Display("hand_phone")]
        public String hand_phone { get; set; }
        [Database("email"), Display("email")]
        public String email { get; set; }
        [Database("contact_person"), Display("contact_person")]
        public String contact_person { get; set; }
        [Database("contact_no"), Display("contact_no")]
        public String contact_no { get; set; }
        [Database("income_tax_no"), Display("income_tax_no")]
        public String income_tax_no { get; set; }
        [Database("epf_no"), Display("epf_no")]
        public String epf_no { get; set; }
        [Database("socso"), Display("socso")]
        public String socso { get; set; }
        [Database("language"), Display("language")]
        public String language { get; set; }
        [Database("nationality"), Display("nationality")]
        public String nationality { get; set; }
        [Database("marital_status"), Display("marital_status")]
        public String marital_status { get; set; }
        [Database("spouse_name"), Display("spouse_name")]
        public String spouse_name { get; set; }
        [Database("spouse_id_no"), Display("spouse_id_no")]
        public String spouse_id_no { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32? created_by { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
