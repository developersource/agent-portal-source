﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Alerts : System.Web.UI.Page
    {
        private static readonly Lazy<INotificationCategoriesDefService> lazyNotificationCategoriesDefServiceObj = new Lazy<INotificationCategoriesDefService>(() => new NotificationCategoriesDefService());
        public static INotificationCategoriesDefService INotificationCategoriesDefService { get { return lazyNotificationCategoriesDefServiceObj.Value; } }

        private static readonly Lazy<INotificationTypesDefService> lazyNotificationTypesDefServiceObj = new Lazy<INotificationTypesDefService>(() => new NotificationTypesDefService());
        public static INotificationTypesDefService INotificationTypesDefService { get { return lazyNotificationTypesDefServiceObj.Value; } }

        private static readonly Lazy<IUserNotificationSettingService> lazyUserNotificationSettingServiceObj = new Lazy<IUserNotificationSettingService>(() => new UserNotificationSettingService());
        public static IUserNotificationSettingService IUserNotificationSettingService { get { return lazyUserNotificationSettingServiceObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyIUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyIUserAccountObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserLogSubService> lazyIUserLogSuberviceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());
        public static IUserLogSubService IUserLogSubService { get { return lazyIUserLogSuberviceObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyUtmcFundDetailServiceObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());
        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyUtmcFundDetailServiceObj.Value; } }

        public static List<UtmcFundInformation> UTMCFundInformations { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null && Session["isVerified"] != null)
                {
                    User user = (User)Session["user"];

                    // if (!IsPostBack)
                    {
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                        }
                    }

                    Bind();
                    BindTable();

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void Bind()
        {
            List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
            Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
            if (responseUFIList.IsSuccess)
            {
                UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
            }

            string Id = ddlUserAccountId.SelectedValue;
            UserAccount primaryAcc = new UserAccount();
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];

                if (!IsPostBack)
                {
                    ddlUserAccountId.Items.Clear();

                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        foreach (UserAccount x in userAccounts)
                            if (x.IsPrinciple == 1)
                            {
                                ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(x.HolderClass) + " - " + x.AccountNo.ToString(), x.Id.ToString()));
                            }
                        if (!string.IsNullOrEmpty(Id))
                        {
                            ddlUserAccountId.SelectedIndex = ddlUserAccountId.Items.IndexOf(ddlUserAccountId.Items.FindByValue(Id));
                            primaryAcc = userAccounts.FirstOrDefault(a => a.Id.ToString() == Id);
                        }
                    }
                }

                Response responseUNSList = IUserNotificationSettingService.GetDataByPropertyName(nameof(UserNotificationSetting.UserId), user.Id.ToString(), true, 0, 0, false);
                if (responseUNSList.IsSuccess)
                {
                    List<UserNotificationSetting> UserNotifications = (List<UserNotificationSetting>)responseUNSList.Data;
                    UserNotifications = UserNotifications.Where(x => x.Status == 1).ToList();
                    if (!IsPostBack)
                    {
                        UserNotificationSetting incomeDistributionSetting = UserNotifications.Where(x => x.NotificationTypeDefId == 3 && x.UserAccountId == Convert.ToInt32(ddlUserAccountId.SelectedValue)).FirstOrDefault();
                        if (incomeDistributionSetting == null || incomeDistributionSetting.Value == "0")
                            chkDistribution.Checked = false;
                        else
                            chkDistribution.Checked = true;
                    }
                    //Notifications
                    Response responseNCDList = INotificationCategoriesDefService.GetData(0, 0, false);
                    if (responseNCDList.IsSuccess)
                    {
                        List<NotificationCategoriesDef> NotificationCategories = (List<NotificationCategoriesDef>)responseNCDList.Data;

                        NotificationCategories.ForEach(x =>
                        {
                            Response responseNTDList = INotificationTypesDefService.GetDataByPropertyName(nameof(NotificationTypesDef.NotificationCategoryDefId), x.Id.ToString(), true, 0, 0, false);
                            if (responseNTDList.IsSuccess)
                            {
                                List<NotificationTypesDef> NotificationTypes = (List<NotificationTypesDef>)responseNTDList.Data;
                                NotificationTypes = NotificationTypes.Where(y => y.Id != 3).ToList();
                                //x.NotificationCategoryDefIdNotificationTypesDefs = NotificationTypes;
                                List<ListItem> listItems = new List<ListItem>();
                                List<UserNotificationSetting> uns = new List<UserNotificationSetting>();
                                HtmlGenericControl div = new HtmlGenericControl("div");
                                div.Attributes.Add("class", "col-md-12");
                                //HtmlGenericControl h5 = new HtmlGenericControl("h5");
                                //h5.InnerHtml = x.Name;
                                //div.Controls.Add(h5);

                                NotificationTypes.ForEach(y =>
                                        {
                                            HtmlGenericControl divRow = new HtmlGenericControl("div");
                                            divRow.Attributes.Add("class", "row");
                                            ListItem l = new ListItem
                                            {
                                                Text = y.Name + "<br /><small class='fs-12'>(" + y.Description + ")</small>",
                                                Value = y.Id.ToString()
                                            };
                                            if (UserNotifications.Count > 0)
                                            {
                                                UserNotificationSetting un = UserNotifications.Where(z => z.NotificationTypeDefId == y.Id).FirstOrDefault();
                                                if (un != null)
                                                    uns.Add(un);
                                            }
                                            listItems.Add(l);

                                            HtmlGenericControl divCol1 = new HtmlGenericControl("div");
                                            divCol1.Attributes.Add("class", "col-md-6");
                                            Label CB = new Label();
                                            CB.ClientIDMode = ClientIDMode.Static;
                                            string characterString = (y.Name == "Fund Price Target" ? "MYR" : "%");
                                            CB.Text = y.Name + " <span class='alert-percent-span'>(" + characterString + ")</span> <br /><small class='fs-12'>(" + y.Description + ")</small>";
                                            CB.ID = y.FieldId;
                                            divCol1.Controls.Add(CB);
                                            divRow.Controls.Add(divCol1);

                                            HtmlGenericControl divCol3 = new HtmlGenericControl("div");
                                            divCol3.Attributes.Add("class", "col-md-4");
                                            DropDownList DDL = new DropDownList();
                                            DDL.ID = y.FieldId + "FundSelect";
                                            DDL.ClientIDMode = ClientIDMode.Static;
                                            DDL.CssClass = "form-control";
                                            //Response responseUFIList = UtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ",0, 0, false);
                                            //if (responseUFIList.IsSuccess)
                                            {
                                                //List<UtmcFundInformation> UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                                                DDL.Items.Add(new ListItem
                                                {
                                                    Text = "Select Fund",
                                                    Value = ""
                                                });

                                                if (y.Name == "Fund Performance Target")
                                                {
                                                    if (primaryAcc != null)
                                                    {
                                                        List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(primaryAcc.AccountNo, UTMCFundInformations);
                                                        UTMCFundInformations = UTMCFundInformations.Where(z => holderInvs.Where(zz => zz.CurrUnitHldg > 0).Select(zz => zz.FundId).Contains(z.IpdFundCode)).ToList();
                                                    }
                                                }

                                                if (UTMCFundInformations.Count != 0)
                                                {
                                                    foreach (UtmcFundInformation uFI in UTMCFundInformations)
                                                    {
                                                        ListItem listItem = new ListItem
                                                        {
                                                            Text = uFI.FundName.Capitalize(),
                                                            Value = uFI.Id.ToString(),
                                                        };
                                                        DDL.Items.Add(listItem);
                                                    }
                                                }

                                                if (y.FieldId == "incomeDistribution")
                                                {

                                                    HtmlGenericControl divCol2 = new HtmlGenericControl("div");
                                                    divCol2.Attributes.Add("class", "col-md-2 mb-8");
                                                    DropDownList TB = new DropDownList();
                                                    TB.ID = y.FieldId + "Text";
                                                    TB.ClientIDMode = ClientIDMode.Static;
                                                    TB.CssClass = "form-control";
                                                    TB.Items.Add(new ListItem
                                                    {
                                                        Text = "Select - ",
                                                        Value = "0"
                                                    });
                                                    TB.Items.Add(new ListItem
                                                    {
                                                        Text = "Yes",
                                                        Value = "1"
                                                    });
                                                    TB.Items.Add(new ListItem
                                                    {
                                                        Text = "No",
                                                        Value = "2"
                                                    });
                                                    divCol2.Controls.Add(TB);
                                                    divRow.Controls.Add(divCol2);
                                                }
                                                else
                                                {
                                                    HtmlGenericControl divCol2 = new HtmlGenericControl("div");
                                                    divCol2.Attributes.Add("class", "col-md-2 mb-8");
                                                    TextBox TB = new TextBox();
                                                    //TB.Text = "Enter Target Value";
                                                    TB.ID = y.FieldId + "Text";
                                                    TB.ClientIDMode = ClientIDMode.Static;
                                                    TB.CssClass = "form-control";
                                                    divCol2.Controls.Add(TB);
                                                    divRow.Controls.Add(divCol2);
                                                }


                                                divCol3.Controls.Add(DDL);
                                                divRow.Controls.Add(divCol3);
                                                div.Controls.Add(divRow);
                                            }
                                        });
                                //div.Controls.Add(cb);

                                notificationSettings.Controls.Add(div);
                            }
                        });
                    }
                }
            }

        }

        public void BindTable()
        {
            StringBuilder asb = new StringBuilder();

            User user = (User)Session["user"];

            Response responseUNSList = IUserNotificationSettingService.GetDataByPropertyName(nameof(UserNotificationSetting.UserId), user.Id.ToString(), true, 0, 0, false);
            if (responseUNSList.IsSuccess)
            {
                List<UserNotificationSetting> UserNotifications = (List<UserNotificationSetting>)responseUNSList.Data;
                UserNotifications = UserNotifications.Where(x => x.UserAccountId == Convert.ToInt32(ddlUserAccountId.SelectedValue)).ToList();
                UserNotifications = UserNotifications.Where(x => x.Status == 1 && x.NotificationTypeDefId != 3).ToList();
                UserNotifications = UserNotifications.OrderBy(x => x.FundId).ToList();
                List<long> fundCodeList = new List<long>();
                foreach (UserNotificationSetting u in UserNotifications)
                {
                    fundCodeList.Add(u.FundId);
                }
                fundCodeList = fundCodeList.Distinct().ToList();

                List<string> fundNameL = new List<string>();
                List<string> fundPriceTargetL = new List<string>();
                //List<string> incomeDistributionL = new List<string>();
                List<string> fundPerformanceTargetL = new List<string>();

                string fundname = "";
                string fundpricetarget = "";
                //string incomedistribution = "";
                string fundperformancetarget = "";

                foreach (long i in fundCodeList)
                {
                    Response response = IUtmcFundInformationService.GetSingle(Convert.ToInt32(i));
                    if (response.IsSuccess)
                    {
                        fundname = ((UtmcFundInformation)(response.Data)).FundName.Capitalize();

                        if (UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 2).FirstOrDefault() != null)
                            fundpricetarget = UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 2).FirstOrDefault().Value.ToString();

                        //if (UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 3).FirstOrDefault() != null)
                        //    incomedistribution = UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 3).FirstOrDefault().Value.ToString();

                        if (UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 4).FirstOrDefault() != null)
                            fundperformancetarget = UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 4).FirstOrDefault().Value.ToString();

                        fundNameL.Add(fundname);

                        if (fundpricetarget != "")
                            fundPriceTargetL.Add(fundpricetarget);
                        else
                            fundPriceTargetL.Add("-");

                        if (fundperformancetarget != "")
                            fundPerformanceTargetL.Add(fundperformancetarget);
                        else
                            fundPerformanceTargetL.Add("-");

                        fundname = "";
                        fundpricetarget = "";
                        //incomedistribution = "";
                        fundperformancetarget = "";
                    }
                }

                int pricetarget = 0;
                //int incomedistributionD = 0;
                int performancetarget = 0;
                //string IncomeDistributionString = "-";

                for (int i = 0; i < fundCodeList.Count(); i++)
                {
                    if (UserNotifications.Where(x => x.Value == fundPriceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault() != null)
                        pricetarget = UserNotifications.Where(x => x.Value == fundPriceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault().Id;

                    //if (UserNotifications.Where(x => x.Value == incomeDistributionL[i] && x.FundId == fundCodeList[i]).FirstOrDefault() != null)
                    //    incomedistributionD = UserNotifications.Where(x => x.Value == incomeDistributionL[i] && x.FundId == fundCodeList[i]).FirstOrDefault().Id;

                    if (UserNotifications.Where(x => x.Value == fundPerformanceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault() != null)
                        performancetarget = UserNotifications.Where(x => x.Value == fundPerformanceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault().Id;


                    //if (incomeDistributionL[i] == "1")
                    //    IncomeDistributionString = "Yes";
                    //else
                    //    IncomeDistributionString = "No";


                    asb.Append(@"  <tr>
                <td>" + fundNameL[i] + @"</td> ");
                    if (fundPriceTargetL[i] == "-")
                        asb.Append(@"<td>" + fundPriceTargetL[i] + @"</td>");
                    else
                        asb.Append(@"<td>" + fundPriceTargetL[i] + @" <a class='text-danger alert-remove delete-notify' data-id='" + (pricetarget != 0 ? pricetarget.ToString() : "0") + "' href='javascript:;'> <i class='fa fa-trash-o' aria-hidden='true'></i> </a></td>");

                    if (fundPerformanceTargetL[i] == "-")
                        asb.Append(@"<td> " + fundPerformanceTargetL[i] + @"</td></tr>");
                    else
                        asb.Append(@"<td> " + fundPerformanceTargetL[i] + @" <a class='text-danger alert-remove delete-notify' data-id='" + (performancetarget != 0 ? performancetarget.ToString() : "0") + "' href='javascript:;'>  <i class='fa fa-trash-o' aria-hidden='true'></i> </a></td></tr>");

                    pricetarget = 0;
                    //incomedistributionD = 0;
                    performancetarget = 0;

                }
                usersTbody.InnerHtml = asb.ToString();

                if (UserNotifications.Count == 0)
                {
                    tblHeader.Visible = false;
                }
                else
                {
                    tblHeader.Visible = true;
                }
            }
        }

        protected void btnNotifySave_Click(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                Response responseUNSList = IUserNotificationSettingService.GetDataByPropertyName(nameof(UserNotificationSetting.UserId), user.Id.ToString(), true, 0, 0, false);
                if (responseUNSList.IsSuccess)
                {
                    bool isValid = true;
                    List<UserNotificationSetting> ExistingUserNotifications = (List<UserNotificationSetting>)responseUNSList.Data;
                    List<UserNotificationSetting> UserNotifications = new List<UserNotificationSetting>();
                    Response responseNCDList = INotificationCategoriesDefService.GetData(0, 0, false);

                    //New code
                    Response responseAllUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                    if (responseAllUFIList.IsSuccess)
                    {
                        UTMCFundInformations = (List<UtmcFundInformation>)responseAllUFIList.Data;
                    }


                    if (responseNCDList.IsSuccess)
                    {
                        List<NotificationCategoriesDef> NotificationCategories = (List<NotificationCategoriesDef>)responseNCDList.Data;
                        NotificationCategories.ForEach(nc =>
                        {
                            Response responseNTDList = INotificationTypesDefService.GetDataByPropertyName(nameof(NotificationTypesDef.NotificationCategoryDefId), nc.Id.ToString(), true, 0, 0, false);
                            if (responseNTDList.IsSuccess)
                            {
                                List<NotificationTypesDef> NotificationTypes = (List<NotificationTypesDef>)responseNTDList.Data;
                                NotificationTypes = NotificationTypes.Where(x => x.Id != 3).ToList();
                                NotificationTypes.ForEach(nt =>
                                {
                                    DropDownList DTB = new DropDownList();
                                    TextBox TB = new TextBox();

                                    TB = (TextBox)notificationSettings.FindControl(nt.FieldId + "Text");

                                    DropDownList DDL = (DropDownList)notificationSettings.FindControl(nt.FieldId + "FundSelect");
                                    Response responseUFIList = IUtmcFundDetailService.GetDataByFilter("utmc_fund_information_id=" + DDL.SelectedValue, 0, 0, false);
                                    if (responseUFIList.IsSuccess)
                                    {
                                        UtmcFundDetail fundDetail = ((List<UtmcFundDetail>)responseUFIList.Data).FirstOrDefault();
                                        if (TB != null)
                                        {
                                            if (TB.Text.Contains("-"))
                                            {
                                                isValid = false;
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Negative values not allowed.', '');", true);
                                            }
                                            if (TB.Text == "0")
                                            {
                                                isValid = false;
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Zero values not allowed.', '');", true);
                                            }
                                            if (DDL.SelectedValue == "0")
                                            {
                                                isValid = false;
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please select a fund.', '');", true);
                                            }
                                            if (TB.Text != "" && DDL.SelectedValue != "0" && !TB.Text.Contains("-") && TB.Text != "0")
                                            {
                                                UtmcFundInformation uFI = UTMCFundInformations.FirstOrDefault(x => x.Id == Convert.ToInt32(DDL.SelectedValue));
                                                UserNotifications.Add(new UserNotificationSetting
                                                {
                                                    UserId = user.Id,
                                                    NotificationTypeDefId = nt.Id,
                                                    FundId = uFI.Id,
                                                    FundCode = uFI.IpdFundCode,
                                                    UserAccountId = Convert.ToInt32(ddlUserAccountId.SelectedValue),
                                                    Value = TB.Text,
                                                    CreatedBy = user.Id,
                                                    CreatedDate = DateTime.Now,
                                                    UpdatedBy = user.Id,
                                                    Status = 1,
                                                    ActualPrice = fundDetail.LatestNavPrice,
                                                    ExpectedPrice = fundDetail.LatestNavPrice * (1 + (Convert.ToDecimal(TB.Text) / 100))
                                                });
                                            }
                                            TB.Text = null;
                                            DDL.SelectedValue = "";
                                        }
                                        else
                                        {
                                            isValid = false;
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please enter a value.', '');", true);
                                        }
                                    }
                                });
                            }
                        });
                    }
                    if (isValid)
                    {
                        if (chkDistribution.Checked == true)
                        {
                            UserNotifications.Add(new UserNotificationSetting
                            {
                                UserId = user.Id,
                                NotificationTypeDefId = 3,
                                FundId = 1,
                                FundCode = "",
                                UserAccountId = Convert.ToInt32(ddlUserAccountId.SelectedValue),
                                Value = (chkDistribution.Checked == true ? "1" : "0"),
                                CreatedBy = user.Id,
                                CreatedDate = DateTime.Now,
                                UpdatedBy = user.Id,
                                Status = 1
                            });
                        }

                        List<UserNotificationSetting> newUns = new List<UserNotificationSetting>();
                        List<UserNotificationSetting> oldUns = new List<UserNotificationSetting>();
                        List<UserNotificationSetting> deletedUns = new List<UserNotificationSetting>();

                        foreach (UserNotificationSetting newuns in UserNotifications)
                        {
                            if (newuns.Value != "" && newuns.FundId != 0)
                            {
                                UserNotificationSetting checkexisted = ExistingUserNotifications.Where(x => x.NotificationTypeDefId == newuns.NotificationTypeDefId && x.FundId == newuns.FundId && x.Value == newuns.Value && x.Status == 1 && x.UserAccountId == Convert.ToInt32(ddlUserAccountId.SelectedValue)).FirstOrDefault();
                                if (checkexisted != null)
                                {
                                    //can be skip because exactly same record found.
                                    //continue;
                                }

                                UserNotificationSetting checkexistedUpdate = ExistingUserNotifications.Where(x => x.NotificationTypeDefId == newuns.NotificationTypeDefId && x.FundId == newuns.FundId && x.UserAccountId == Convert.ToInt32(ddlUserAccountId.SelectedValue)).FirstOrDefault();
                                if (checkexistedUpdate != null)
                                {
                                    checkexistedUpdate.Value = newuns.Value;
                                    checkexistedUpdate.FundId = newuns.FundId;
                                    checkexistedUpdate.Status = 1;
                                    checkexistedUpdate.UserAccountId = Convert.ToInt32(ddlUserAccountId.SelectedValue);
                                    checkexistedUpdate.UpdatedBy = user.Id;
                                    checkexistedUpdate.UpdatedDate = DateTime.Now;
                                    //Update record since value is different.
                                    oldUns.Add(checkexistedUpdate);
                                    //continue;
                                }

                                newUns.Add(newuns);
                            }
                        }

                        //UserLogMain ulm = new UserLogMain()
                        //{
                        //    Description = "User updated fund alert notifications",
                        //    TableName = "user_notification_settings",
                        //    UserId = user.Id,
                        //    UserAccountId = Convert.ToInt32(ddlUserAccountId.SelectedValue),
                        //    UpdatedDate = DateTime.Now,
                        //    RefId = 0,
                        //    RefValue = "",
                        //    StatusType = 1
                        //};

                        //Response response = IUserLogMainService.PostData(ulm);
                        //ulm = (UserLogMain)response.Data;

                        if (oldUns.Count > 0)
                            IUserNotificationSettingService.UpdateBulkData(oldUns);
                        if (newUns.Count > 0)
                            IUserNotificationSettingService.PostBulkData(newUns);
                        if (deletedUns.Count > 0)
                            IUserNotificationSettingService.UpdateBulkData(deletedUns);

                        BindTable();
                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        try
                        {
                            //using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", user.EmailId))
                            //{
                            //    mm.Subject = "Alert notification configuration updated";
                            //    string body = "Hello ," + user.Username;
                            //    body += "<br /><br />MA Acoount - " + ddlUserAccountId.SelectedValue + " alert configuration has been updated.";
                            //    body += "<br />" + DateTime.Now;
                            //    body += "<br /><br />Thanks";
                            //    mm.Body = body;
                            //    mm.IsBodyHtml = true;
                            //    SmtpClient smtp = new SmtpClient();
                            //    smtp.Host = "mail.petraware.com";
                            //    smtp.EnableSsl = true;
                            //    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                            //    smtp.UseDefaultCredentials = true;
                            //    smtp.Credentials = NetworkCred;
                            //    smtp.Port = 587;
                            //    smtp.Send(mm);
                            //}
                            Email email = new Email();
                            email.user = user;
                            EmailService.SendAlertUpdateMail(email, ddlUserAccountId.SelectedItem.Text, "Watch List configuration updated", "Watch List configuration has been updated", false, "");
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog("Alert Page SendEmail: " + ex.Message);
                        }
                    }
                }
            }
            else
            {

            }

        }

        protected void ddlUserAccountId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Bind();
            BindTable();
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            if (ddlUserAccountId.SelectedValue != null && ddlUserAccountId.SelectedValue != "")
                Response.Redirect("Settings.aspx?AccountNo=" + ddlUserAccountId.SelectedValue, false);
            else
                Response.Redirect("Settings.aspx", false);
        }

        protected void Delete_Setting_Click(object sender, EventArgs e)
        {
            if (SettingId.Value != "")
            {
                int id = Convert.ToInt32(SettingId.Value);

                Response response = IUserNotificationSettingService.GetSingle(id);
                if (response.IsSuccess)
                {
                    UserNotificationSetting usn = (UserNotificationSetting)response.Data;

                    if (usn != null)
                    {
                        usn.Status = 0;
                        IUserNotificationSettingService.UpdateData(usn);
                        SettingId.Value = "";
                        BindTable();
                    }
                }
            }
        }
    }
}