﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Buy_Fund : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            String fundCode = Request.QueryString["fundCode"];
            if (fundCode == null || fundCode == "")
            {
                if (Session["transFundCode"] != null)
                {
                    fundCode = Session["transFundCode"].ToString();
                }
            }
            else
            {
                Session["transFundCode"] = fundCode;
            }
            if (Session["user"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Buy-Fund.aspx'", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx?redirectUrl=Buy-Fund.aspx'", true);
                }
            }
            else
            {
                Online_Transactions ot = new Online_Transactions();
                bool isSATUpdated = ot.CheckIfSATUpdated();
                if (!isSATUpdated)
                {
                    RunScript(fundCode);
                }
            }
            if (Session["isVerified"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Buy-Fund.aspx'", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx?redirectUrl=Buy-Fund.aspx'", true);
                }
            }
            else
            {
                String isVerified = (Session["isVerified"].ToString());
                if (isVerified == "0")
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Buy-Fund.aspx'", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx?redirectUrl=Buy-Fund.aspx'", true);
                    }
                }
            }
            UtmcFundInformation utmcFundInformation = null;
            if (fundCode != null)
            {
                String propName = nameof(UtmcFundInformation.FundCode);
                utmcFundInformation = IUtmcFundInformationService.GetDataByPropertyName(propName, fundCode, true, 0, 0, false).FirstOrDefault();
                //propName = nameof(FundInfo.FundCode);
                //FundInfo fundInfo = IFundInfoService.GetDataByPropertyName(propName, utmcFundInformation.IpdFundCode).FirstOrDefault();
            }
            if (!IsPostBack)
            {
                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    user = IUserService.GetSingle(user.Id);

                    List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                    UserAccount primaryAcc = new UserAccount();
                    if (Session["SelectedAccountHolderId"] != null)
                    {
                        primaryAcc = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == Session["SelectedAccountHolderId"].ToString()).FirstOrDefault();
                    }
                    List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = (List<UtmcDetailedMemberInvestment>) Portfolio.GetMAAccountUtmcMemberInvestments(primaryAcc.MaHolderRegId.ToString());
                    Decimal maAccountRM = 0;
                    utmcDetailedMemberInvestments.ForEach(x => {
                        maAccountRM += x.utmcMemberInvestments[0].ActualTransferredFromEpfRm;
                    });

                    hdnCurrentUnitHolding.Value = String.Format("{0:0.00}", maAccountRM);


                    List<UtmcFundInformation> UTMCFundInformations = IUtmcFundInformationService.GetData(0, 0, false);
                    ddlFundList.Items.Add(new ListItem
                    {
                        Text = "Select Fund",
                        Value = ""
                    });

                    if(CustomValues.isEPF(primaryAcc.MaHolderRegIdMaHolderReg.HolderCls))
                    {
                        UTMCFundInformations = UTMCFundInformations.Where(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().IsEpfApproved == 1).ToList();
                        ddlPaymentMethod.SelectedValue = "EPF";
                        ddlPaymentMethod.Enabled = false;
                    }
                    else
                    {
                        ddlPaymentMethod.SelectedValue = "CASH";
                        ddlPaymentMethod.Enabled = false;
                    }

                    Online_Transactions ot = new Online_Transactions();

                    string userAccSATGroupByScore = ot.GetGroupByScore(primaryAcc.SatScore);
                    int group = userAccSATGroupByScore[1];

                    foreach (UtmcFundInformation uFI in UTMCFundInformations)
                    {
                        string risk = "";
                        if (uFI.SatGroup.ToLower().Contains(userAccSATGroupByScore.ToLower()))
                        {
                            risk = "Recommended";
                        }
                        else
                        {
                            string uFSATGroup = uFI.SatGroup;
                            string[] gs = uFSATGroup.Split(',');
                            foreach (string g in gs)
                            {
                                int groupF = g[1];
                                if (groupF < group)
                                {
                                    risk = "Low Risk";
                                }
                                if (groupF > group)
                                {
                                    risk = "High Risk";
                                }
                            }
                        }
                        ListItem listItem = new ListItem
                        {
                            Text = uFI.FundCode + " - " + uFI.FundName + " : " + risk,
                            Value = uFI.Id.ToString(),
                        };
                        if (utmcFundInformation != null)
                        {
                            listItem.Attributes.Add("data-risk", risk);
                            if (uFI.Id == utmcFundInformation.Id)
                            {
                                listItem.Selected = true;
                                ddlFundList.Items.Add(listItem);
                            }
                            else
                            {
                                ddlFundList.Items.Add(listItem);
                            }
                        }
                        else
                        {
                            ddlFundList.Items.Add(listItem);
                        }
                    }
                    BindCart();
                }
            }
        }

        public void RunScript(string fundCode)
        {
            if (fundCode != null && fundCode != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "if(confirm('Please Proceed to Update SAT.')) window.location.href='SAT-Form.aspx?redirectUrl=Buy-Fund.aspx'; else window.location.href='Funds-listing.aspx'", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "if(confirm('Please Proceed to Update SAT.')) window.location.href='SAT-Form.aspx?redirectUrl=Buy-Fund.aspx'; else window.location.href='Index.aspx'", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFunds(Int32 scheme)
        {
            List<UtmcFundInformation> utmcFundInformations = IUtmcFundInformationService.GetData(0, 0, false);
            if (scheme == 0)
            {
                utmcFundInformations = utmcFundInformations.Where(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().IsEpfApproved == 0).ToList();
            }
            else if (scheme == 1)
            {
                utmcFundInformations = utmcFundInformations.Where(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().IsEpfApproved == 1).ToList();
            }
            return utmcFundInformations;
        }

        public void BindCart()
        {
            List<Cart> cartItems = GetCartItemsByPrimaryAccount();
            if (cartItems.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Cart cartItem in cartItems)
                {
                    sb.Append(@"<tr>
                                    <td><span class='fundName' data-placement='bottom' data-toggle='tooltip' title='" + cartItem.UtmcFundInformation.FundName + @"'>" + cartItem.UtmcFundInformation.FundCode + @"</span></td>
                                    <td class='text-right currencyFormat'>" + cartItem.Amount + @"</td>
                                    <td class='text-right'>
                                        <div class='btn-group'>
                                            <a href='javascript:;' 
                                                data-fundId=" + cartItem.UtmcFundInformation.Id + @" 
                                                data-id=" + cartItem.Id + @" 
                                                data-amount=" + cartItem.Amount + @" 
                                                class='btn btn-sm btn-default editFromCart' 
                                                data-toggle='tooltip' 
                                                title='Edit'><i class='fa fa-edit'></i></a>
                                            <a href='javascript:;' 
                                                data-fundId=" + cartItem.UtmcFundInformation.Id + @" 
                                                data-id=" + cartItem.Id + @" 
                                                class='btn btn-sm btn-default removeFromCart' 
                                                data-toggle='tooltip' 
                                                title='Remove'><i class='fa fa-times'></i></a>
                                        </div>
                                    </td>
                                </tr>");
                }
                tbodyCart.InnerHtml = sb.ToString();
                cartTotalAmount.InnerHtml = cartItems.Sum(x => x.Amount).ToString();
            }
        }

        public void Reset()
        {
            chkConfirmFundInformation.Checked = false;
            ddlFundList.SelectedIndex = 0;
            txtInvestmentAmount.Text = "";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFundDetails(Int32 Id)
        {
            UtmcFundInformation UtmcFundInformation = IUtmcFundInformationService.GetSingle(Id);

            User user = (User)System.Web.HttpContext.Current.Session["user"];
            List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
            UserAccount primaryAcc = new UserAccount();
            if (System.Web.HttpContext.Current.Session["SelectedAccountHolderId"] != null)
            {
                primaryAcc = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == System.Web.HttpContext.Current.Session["SelectedAccountHolderId"].ToString()).FirstOrDefault();
            }

            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = (List<UtmcDetailedMemberInvestment>)Portfolio.GetMAAccountUtmcMemberInvestments(primaryAcc.MaHolderRegId.ToString());
            Decimal maAccountRM = 0;
            utmcDetailedMemberInvestments.ForEach(x => {
                maAccountRM += x.utmcMemberInvestments[0].ActualTransferredFromEpfRm;
            });
            
            List<int> splits = CustomGenerator.SplitNumber(UtmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinInitialInvestmentCash, Convert.ToInt32(maAccountRM), 5);

            return new { UtmcFundInformation = UtmcFundInformation, splits = splits };
        }

        public List<Cart> GetCartItemsByPrimaryAccount()
        {
            List<Cart> cartItems = new List<Cart>();
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                string selectedAccountHolderId = "";
                if (Session["SelectedAccountHolderId"] != null)
                {
                    selectedAccountHolderId = Session["SelectedAccountHolderId"].ToString();
                }
                UserAccount primaryAccount = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == selectedAccountHolderId).FirstOrDefault();
                if (Session["cartItems"] != null)
                {
                    cartItems = (List<Cart>)Session["cartItems"];
                    cartItems = cartItems.Where(x => x.userAccount.Id == primaryAccount.Id).ToList();
                    if (cartItems.Count > 0)
                    {
                        if (cartItems.FirstOrDefault().Type == 2 || cartItems.FirstOrDefault().Type == 3)
                        {
                            cartItems = new List<Cart>();
                            Session["cartItems"] = cartItems;
                        }
                    }
                }
            }
            return cartItems;
        }

        public List<Cart> GetAllCartItems()
        {
            List<Cart> cartItems = new List<Cart>();
            if (Session["user"] != null)
            {
                if (Session["cartItems"] != null)
                {
                    cartItems = (List<Cart>)Session["cartItems"];
                }
            }
            return cartItems;
        }

        public static Int32 Id = 1;

        protected void btnAddToCart_Click(object sender, EventArgs e)
        {
            List<Cart> cartItems = GetAllCartItems();
            if (ddlFundList.SelectedValue != "" && ddlFundList.SelectedValue != null)
            {
                User user = (User)Session["user"];
                List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                string selectedAccountHolderId = "";
                if (Session["SelectedAccountHolderId"] != null)
                {
                    selectedAccountHolderId = Session["SelectedAccountHolderId"].ToString();
                }
                UserAccount primaryAccount = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == selectedAccountHolderId).FirstOrDefault();

                Session["transFundCode"] = null;
                Int32 fundId = Convert.ToInt32(ddlFundList.SelectedValue);
                Int32 investmentAmount = Convert.ToInt32(txtInvestmentAmount.Text);
                Cart cartItem = cartItems.Where(x => x.UtmcFundInformation.Id == fundId && x.userAccount.Id == primaryAccount.Id).FirstOrDefault();
                if (cartItem == null)
                {
                    UtmcFundInformation UtmcFundInformation = IUtmcFundInformationService.GetSingle(fundId);
                    cartItems.Add(new Cart
                    {
                        Id = Id,
                        userAccount = primaryAccount,
                        UtmcFundInformation = UtmcFundInformation,
                        Amount = investmentAmount,
                        PaymentMethod = ddlPaymentMethod.SelectedValue,
                        Type = 1
                    });
                    Id ++;
                }
                else
                {
                    cartItem.Amount = cartItem.Amount + investmentAmount;
                }
                Session["cartItems"] = cartItems;
                BindCart();
                Reset();
            }
        }

        protected void btnUpdateToCart_Click(object sender, EventArgs e)
        {
            List<Cart> cartItems = GetAllCartItems();
            if (ddlFundList.SelectedValue != "" && ddlFundList.SelectedValue != null)
            {
                User user = (User)Session["user"];
                List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                string selectedAccountHolderId = "";
                if (Session["SelectedAccountHolderId"] != null)
                {
                    selectedAccountHolderId = Session["SelectedAccountHolderId"].ToString();
                }
                UserAccount primaryAccount = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == selectedAccountHolderId).FirstOrDefault();

                Int32 fundId = Convert.ToInt32(ddlFundList.SelectedValue);
                Int32 Id = Convert.ToInt32(hdnId.Value);
                Cart cartItem = cartItems.Where(x => x.Id == Id).FirstOrDefault();
                Int32 investmentAmount = Convert.ToInt32(txtInvestmentAmount.Text);
                if (cartItem != null)
                {
                    int index = cartItems.IndexOf(cartItem);
                    cartItem.Amount = investmentAmount;
                    cartItems = cartItems.Where(x => x.Id != Id).ToList();
                    cartItems.Insert(index, cartItem);
                }
                Session["cartItems"] = cartItems;
                hdnId.Value = "0";
                BindCart();
                Reset();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object RemoveFromCart(Int32 Id)
        {
            Buy_Fund bf = new Buy_Fund();
            List<Cart> cartItems = bf.GetAllCartItems();
            cartItems = cartItems.Where(x => x.Id != Id).ToList();
            System.Web.HttpContext.Current.Session["cartItems"] = cartItems;
            cartItems = bf.GetCartItemsByPrimaryAccount();
            return cartItems.Sum(x => x.Amount);
        }

        protected void btnProceedToCart_Click(object sender, EventArgs e)
        {
            Response.Redirect("Show-Cart.aspx");
        }
    }
}