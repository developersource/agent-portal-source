﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace DiOTP.WebApp
{
    public partial class AgentMaster : System.Web.UI.MasterPage
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
                {
                    if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "1")
                    {
                        User user = (User)(Session["user"]);
                        Response responseUser = IUserService.GetSingle(user.Id);
                        if (responseUser.IsSuccess)
                        {
                            user = (User)responseUser.Data;
                            aAccountName.InnerHtml = user.Username + " <div class='aAccountName-joint'>(" + user.AgentCode + ")</div>";
                            string[] types = new string[] { "WA", "WM", "GM", "SGM" };
                            smallAccountPlan.InnerHtml = "AGENT ("+ Session["AgentType"] + ")";

                            //Check if the agent
                            //string queryGetAgentData = @"SELECT * FROM agent_regs where user_id='" +user.Id+ "'";
                            //Response responseGetAgentData = GenericService.GetDataByQuery(queryGetAgentData, 0, 0, false, null, false, null, true);
                            //{
                            //    var agentDyn = responseGetAgentData.Data;
                            //    var responseJSON = JsonConvert.SerializeObject(agentDyn);
                            //    List<DiOTP.Utility.AgentReg> agentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseJSON);
                            //    AgentReg singleAgent = agentList.FirstOrDefault();
                            //    StringBuilder sb = new StringBuilder();

                            //    sb.Append(@"<li class='logout'>
                            //                <i class='fa fa-user-circle'></i><a id ='aAccountName' runat='server' clientidmode='Static'>Name</a>
                            //                <small id = 'smallAccountPlan' runat='server' clientidmode='Static'>Account Plan</small>
                            //            </li>");
                            //    if (singleAgent.process_status.ToString() == "2")
                            //    {
                            //        sb.Append(@"<li><a href='/ExamMaterials.aspx'>Dashboard</a></li>
                            //                    <li><a href='/Portfolio.aspx'>Back to Investor Portal</a></li>");
                            //    }
                            //    else
                            //    {
                            //        sb.Append(@"<li><a href='/AgentDashboard.aspx'>Dashboard</a></li>
                            //                    <li>< a href = '/AgentProfile.aspx'>Agent Profile</a></li>
                            //                    <li><a href = '/AgentClients.aspx'>My Clients</a></li>
                            //                    <li><a href ='/ELearning.aspx'>E-Learning</a></li>              
                            //                    <li class='sub-menu'>
                            //                        <a href = 'javascript:;'><span>Client Recruitment</span></a>
                            //                        <ul class='sub'>
                            //                             <li><a href = '/AccountOpening.aspx' target='_blank'>UT Registration</a></li>
                            //                             <li><a href = 'https://prsenrolment.ppa.my/' target='_blank'>PRS Registration</a></li>
                         
                            //                        </ul>
                            //                    </li>
                            //                    <li><a href = '/AgentSignups.aspx'>Agent Recruitment</a></li>
                            //                    <li class='sub-menu'>
                            //                        <a href = 'javascript:;'><span>Resources</span></a>
                            //                        <ul class='sub'>
                            //                             <li><a href = 'https://apexis.com.my/resources/downloads/forms' target='_blank'>Forms</a></li>
                            //                             <li><a href = 'https://apexis.com.my/resources/downloads/resources' target='_blank'>Fund Fact Sheets</a></li>
                            //                        </ul>
                            //                    </li>
                            //                    <li class='sub-menu'>
                            //                        <a href = 'javascript:;'><span>Reports</span></a>
                            //                        <ul class='sub'>
                            //                             <li><a href = '/MasterReport.aspx'>Master Report</a></li>
                            //                             <li><a href = '/ByGroupTypeReport.aspx'>By Group type Report</a></li>
                            //                             <li><a href = '/ByAgencyGroupReport.aspx'>By Agency Group Report</a></li>
                            //                            <li><a href = '/MTDTransactionReport.aspx'>MTD Transaction Report</a></li>
                            //                        </ul>
                            //                    </li>");
                            //    }
                            //    agentSideNav.InnerHtml = sb.ToString();
                            //}

                            Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                            if (IsImpersonate)
                            {
                            }
                        }
                    }
                }

                //if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "0")
                //{
                //    User user = (User)(Session["user"]);
                //    Response responseUser = IUserService.GetSingle(user.Id);
                //    if (responseUser.IsSuccess)
                //    {
                //        user = (User)responseUser.Data;
                //        aAccountName.InnerHtml = user.Username + " <div class='aAccountName-joint'>(N/A)</div>";
                //        string[] types = new string[] { "WA", "WM", "GM", "SGM" };
                //        smallAccountPlan.InnerHtml = "AGENT TRAINEE";

                //        //Check if the agent
                //        string queryGetAgentData = @"SELECT * FROM agent_regs where user_id='" + user.Id + "'";
                //        Response responseGetAgentData = GenericService.GetDataByQuery(queryGetAgentData, 0, 0, false, null, false, null, true);
                //        {
                //            var agentDyn = responseGetAgentData.Data;
                //            var responseJSON = JsonConvert.SerializeObject(agentDyn);
                //            List<DiOTP.Utility.AgentReg> agentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseJSON);
                //            AgentReg singleAgent = agentList.FirstOrDefault();
                //            StringBuilder sb = new StringBuilder();

                //            if (singleAgent.process_status.ToString() == "2")
                //            {
                //                dashboardNAV.Attributes.Add("class", "hide");
                //                profileNAV.Attributes.Add("class", "hide");
                //                clientsNAV.Attributes.Add("class", "hide");
                //                eLearningNAV.Attributes.Add("class", "hide");
                //                agentRecruitmentNAV.Attributes.Add("class", "hide");
                //                clientRecruitmentNAV.Attributes.Add("class", "hide");
                //                resourcesNAV.Attributes.Add("class", "hide");
                //                reportsNAV.Attributes.Add("class","hide");


                //                traineeNAV.Attributes.Remove("class");
                //                traineeNAV.Attributes.Add("class", "btn");
                //            }

                //        }

                //        Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                //        if (IsImpersonate)
                //        {
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog("AgentMaster Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void lnkLogout3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Logout.aspx", false);
        }


    }
}

