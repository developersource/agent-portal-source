﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegularSavingPlans.aspx.cs" Inherits="Admin.RegularSavingPlans" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="RSPForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                            <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                            <input type="text" id="FromDate" name="FromDate" runat="server" class="form-control" placeholder="From Date" />
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="ToDate" name="ToDate" runat="server" class="form-control" placeholder="To Date" />
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <button type="button" class="popovers btn action-button button-common-label pull-right" data-action="Search"><i class="fa fa-search"></i></button>

                                        <span class="common-divider pull-right"></span>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully rejected" data-status="Rejected" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Reject" data-isconfirm="1" data-isreason="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Successfully approved" data-status="Approved" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Approve" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                <span class="common-divider pull-right"></span>
                                                <button id="download" data-message="Success" data-status="Active" data-original-title="Download as excel" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Download" data-isconfirm="0"><i class="fa fa-download"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <small>
                                    <strong>Note:</strong>
                                    <span>You can search here with ID No, Username, MA Account, Bank Name, Order No</span>
                                </small>
                            </div>
                            <div class="col-md-6">
                                <small>
                                    <strong>Note:</strong>
                                    <span>You can Download/Approve/Terminate on Regular Saving Plans</span>
                                </small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="filter-section mt-10">
                                    <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                    <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="All">All</button>
                                    <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="2">Enrolled</button>
                                    <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="3">Approved</button>
                                    <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="39">Rejected</button>

                                    <small>
                                        <br />
                                        <strong>Note:</strong>
                                        <span>Order Status Filtering on Transactions</span>
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th>Order Date</th>
                                            <th>ID No</th>
                                            <th>Username</th>
                                            <th>MA Account</th>
                                            <th>Bank Name</th>
                                            <th>Seller Order No</th>
                                            <th>Processed Date</th>
                                            <th>e-Mandate Ref No/ Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody id="usersTbody" runat="server">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <script>
        $ui(document).ready(function () {
            $ui('#FromDate, #ToDate').datepicker({
                format: 'dd/mm/yyyy',
                pickTime: false,
                autoclose: true
            });

            //$ui('#ToDate').datepicker({
            //    format: 'mm/dd/yyyy',
            //    pickTime: false,
            //    autoclose: true
            //});
        });
    </script>
</body>
</html>
