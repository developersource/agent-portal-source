﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    class ZoomAPIDTOs
    {
    }

    public class Meeting
    {
        public String topic { get; set; }
        public Int32 type { get; set; }
        public DateTime start_time { get; set; }
        public Int32 duration { get; set; }
        public String schedule_for { get; set; }
        public String timezone { get; set; }
        public String password { get; set; }
        public String agenda { get; set; }
        public Recurrence recurrence { get; set; }
        public Settings settings { get; set; }
        public Boolean registrants_email_notification { get; set; }
    }
    public class Recurrence
    {
        public Int32 type { get; set; }
        public Int32 repeat_interval { get; set; }
        public String weekly_days { get; set; }
        public Int32 monthly_day { get; set; }
        public Int32 monthly_week { get; set; }
        public Int32 monthly_week_day { get; set; }
        public Int32 end_times { get; set; }
        public String end_date_time { get; set; }
    }
    public class Settings
    {
        public Boolean host_video { get; set; }
        public Boolean participant_video { get; set; }
        public Boolean cn_meeting { get; set; }
        public Boolean in_meeting { get; set; }
        public Boolean join_before_host { get; set; }
        public Boolean mute_upon_entry { get; set; }
        public Boolean watermark { get; set; }
        public Boolean use_pmi { get; set; }
        public Int32 approval_type { get; set; }
        public String audio { get; set; }
        public String auto_recording { get; set; }
        public Boolean enforce_login { get; set; }
        public String enforce_login_domains { get; set; }
        public String alternative_hosts { get; set; }
        public Boolean close_registration { get; set; }
        public Boolean show_share_button { get; set; }
        public Boolean allow_multiple_devices { get; set; }
        public Boolean registrants_confirmation_email { get; set; }
        public Boolean waiting_room { get; set; }
        public Boolean request_permission_to_unmute_participants { get; set; }
        public Boolean registrants_email_notification { get; set; }
        public Boolean meeting_authentication { get; set; }
        public String authentication_option { get; set; }
        public String authentication_name { get; set; }
        public String authentication_domains { get; set; }
        public String encryption_type { get; set; }
        public EnableSetting approved_or_denied_countries_or_regions { get; set; }
        public EnableSetting breakout_room { get; set; }
        public Boolean device_testing { get; set; }
        public Int32 registration_type { get; set; }
        public String[] global_dial_in_countries { get; set; }
    }

    public class EnableSetting
    {
        public Boolean enable { get; set; }
    }

    public class MeetingResponse
    {
        public String uuid { get; set; }
        public Double id { get; set; }
        public String host_id { get; set; }
        public String host_email { get; set; }
        public String topic { get; set; }
        public Int32 type { get; set; }
        public String status { get; set; }
        public String timezone { get; set; }
        public String agenda { get; set; }
        public DateTime created_at { get; set; }
        public String start_url { get; set; }
        public String join_url { get; set; }
        public String password { get; set; }
        public String h323_password { get; set; }
        public String pstn_password { get; set; }
        public String encrypted_password { get; set; }
        public Settings settings { get; set; }
    }
}
