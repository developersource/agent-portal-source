using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class FundChartInfoService : IFundChartInfoService
    {
        private static readonly Lazy<IFundChartInfoRepo> lazy = new Lazy<IFundChartInfoRepo>(() => new FundChartInfoRepo());
        public static IFundChartInfoRepo IFundChartInfoRepo { get { return lazy.Value; } }

        public Response PostData(FundChartInfo obj)
        {
            Response response = new Response();
            try
            {
                response = IFundChartInfoRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FundChartInfo> objs)
        {
            Int32 result = 0;
            try
            {
                result = IFundChartInfoRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FundChartInfo obj)
        {
            Response response = new Response();
            try
            {
                response = IFundChartInfoRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FundChartInfo> objs)
        {
            Int32 result = 0;
            try
            {
                result = IFundChartInfoRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FundChartInfo DeleteData(Int32 Id)
        {
            FundChartInfo obj = new FundChartInfo();
            try
            {
                obj = IFundChartInfoRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IFundChartInfoRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FundChartInfo obj = new FundChartInfo();
            try
            {
                response = IFundChartInfoRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundChartInfo> objs = new List<FundChartInfo>();
            try
            {
                response = IFundChartInfoRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IFundChartInfoRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundChartInfo> objs = new List<FundChartInfo>();
            try
            {
                response = IFundChartInfoRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IFundChartInfoRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundChartInfo> objs = new List<FundChartInfo>();
            try
            {
                response = IFundChartInfoRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IFundChartInfoRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response PostChartData(FundChartInformation obj, Int32 dataValue)
        {
            Response response = new Response();
            Response responseFCIList = GetDataByPropertyName(nameof(FundChartInfo.FundId), obj.fundId.ToString(), true, 0, 0, true);
            response = responseFCIList;
            if (responseFCIList.IsSuccess)
            {
                List<FundChartInfo> fundChartInfo = (List<FundChartInfo>)responseFCIList.Data;
                FundChartInfo singleFundChartInfo = fundChartInfo.Where(x => x.DataValue == dataValue).FirstOrDefault();
                if (singleFundChartInfo == null)
                {
                    response = PostData(new FundChartInfo
                    {
                        Average = obj.average,
                        CurrentNav = obj.currentNav,
                        CurrentNavDate = obj.currentNavDate,
                        DataValue = dataValue,
                        FundId = obj.fundId,
                        FundName = obj.fundName.Capitalize(),
                        Labels = String.Join(",", obj.Labels.ToArray()),
                        MaxChange = obj.maxChange,
                        MaxNav = obj.maxNav,
                        MinChange = obj.minChange,
                        MinNav = obj.minNav,
                        NavDates = String.Join(",", obj.NavDates.ToArray()),
                        NavPrices = String.Join(",", obj.NavPrices.ToArray()),
                        ThreeYearAnnualisedPercent = obj.threeYearAnnualisedPercent,
                        TotalReturns = obj.totalReturns,
                        NavChanges = String.Join(",", obj.Values.ToArray())
                    });
                }
                else
                {
                    singleFundChartInfo.Average = obj.average;
                    singleFundChartInfo.CurrentNav = obj.currentNav;
                    singleFundChartInfo.CurrentNavDate = obj.currentNavDate;
                    singleFundChartInfo.DataValue = dataValue;
                    singleFundChartInfo.FundId = obj.fundId;
                    singleFundChartInfo.FundName = obj.fundName.Capitalize();
                    singleFundChartInfo.Labels = String.Join(",", obj.Labels.ToArray());
                    singleFundChartInfo.MaxChange = obj.maxChange;
                    singleFundChartInfo.MaxNav = obj.maxNav;
                    singleFundChartInfo.MinChange = obj.minChange;
                    singleFundChartInfo.MinNav = obj.minNav;
                    singleFundChartInfo.NavDates = String.Join(",", obj.NavDates.ToArray());
                    singleFundChartInfo.NavPrices = String.Join(",", obj.NavPrices.ToArray());
                    singleFundChartInfo.ThreeYearAnnualisedPercent = obj.threeYearAnnualisedPercent;
                    singleFundChartInfo.TotalReturns = obj.totalReturns;
                    singleFundChartInfo.NavChanges = String.Join(",", obj.Values.ToArray());

                    response = UpdateData(singleFundChartInfo);
                }
            }
            return response;
        }
    }
}
