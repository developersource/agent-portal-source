using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class FormDataFieldValueRepo : IFormDataFieldValueRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(FormDataFieldValue obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FormDataFieldValue>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FormDataFieldValue> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FormDataFieldValue>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FormDataFieldValue obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FormDataFieldValue>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FormDataFieldValue> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FormDataFieldValue>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FormDataFieldValue DeleteData(Int32 Id)
        {
            Response responseFDFV = GetSingle(Id);
            if (responseFDFV.IsSuccess)
            {
                FormDataFieldValue obj = (FormDataFieldValue)responseFDFV.Data;
                try
                {
                    string query = obj.ObjectToQuery<FormDataFieldValue>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseFDFV = GetSingle(Id);
                    if (responseFDFV.IsSuccess)
                    {
                        FormDataFieldValue obj = (FormDataFieldValue)responseFDFV.Data;
                        query += obj.ObjectToQuery<FormDataFieldValue>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FormDataFieldValue obj = new FormDataFieldValue();
            try
            {
                string query = "select * from form_data_field_values where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new FormDataFieldValue
                       {
                           Id = x.Field<Int32>("id"),
                           UserAccountId = x.Field<Int32>("user_account_id"),
                           FormDataFieldId = x.Field<Int32>("form_data_field_id"),
                           Value = x.Field<String>("value"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormDataFieldValue> objs = new List<FormDataFieldValue>();
            try
            {
                string query = "select * from form_data_field_values";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormDataFieldValue
                        {
                            Id = x.Field<Int32>("id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            FormDataFieldId = x.Field<Int32>("form_data_field_id"),
                            Value = x.Field<String>("value"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_data_field_values";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<FormDataFieldValue>(propertyName);
            List<FormDataFieldValue> objs = new List<FormDataFieldValue>();
            try
            {
                string query = "select * from form_data_field_values where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from form_data_field_values where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormDataFieldValue
                        {
                            Id = x.Field<Int32>("id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            FormDataFieldId = x.Field<Int32>("form_data_field_id"),
                            Value = x.Field<String>("value"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<FormDataFieldValue>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_data_field_values where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from form_data_field_values where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormDataFieldValue> objs = new List<FormDataFieldValue>();
            try
            {
                string query = "select * from form_data_field_values where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormDataFieldValue
                        {
                            Id = x.Field<Int32>("id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            FormDataFieldId = x.Field<Int32>("form_data_field_id"),
                            Value = x.Field<String>("value"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_data_field_values where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
