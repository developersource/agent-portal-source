﻿using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (!IsPostBack)
                {

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                string oldPassword = txtOldPassword.Text;

                string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(oldPassword);
                if (encryptedPassword == user.Password)
                {
                    string newPassword = txtPassword.Text;
                    string confirmPassword = txtConfirmPassword.Text;
                    if (newPassword == confirmPassword)
                        user.Password = CustomEncryptorDecryptor.EncryptPassword(newPassword);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("ChangePassword Page btnChangePassword_Click: " + ex.Message);
            }

        }
    }
}