﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("commission_structures")]
    public class CommissionStructure
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("apply_to"), Display("id")]
        public String apply_to { get; set; }
        [Database("commission_def_id"), Display("id")]
        public Int32 commission_def_id { get; set; }
        [Database("commission_settings_id"), Display("id")]
        public Int32 commission_settings_id { get; set; }
        [Database("percent"), Display("id")]
        public Decimal percent { get; set; }
        [Database("is_active"), Display("id")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("id")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("id")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("id")]
        public DateTime updated_date { get; set; }
        [Database("updated_by"), Display("id")]
        public Int32 updated_by { get; set; }
        [Database("status"), Display("id")]
        public Int32 status { get; set; }
    }

    public class CommissionStructureDetail
    {
        public Int32 id { get; set; }
        public String apply_to { get; set; }
        public Int32 commission_def_id { get; set; }
        public Int32 commission_settings_id { get; set; }
        public Decimal percent { get; set; }
        public Int32 is_active { get; set; }
        public DateTime created_date { get; set; }
        public Int32 created_by { get; set; }
        public DateTime updated_date { get; set; }
        public Int32 updated_by { get; set; }
        public Int32 status { get; set; }

        public String code { get; set; }
        public String name { get; set; }
        public String structure_name { get;set;}
    }

    public class AgentDetail
    {
        public Int32 agent_user_id { get; set; }
        public String agent_code { get; set; }
        public String agent_name { get; set; }
        public String agent_role { get; set; }
        public Decimal psc_percent { get; set; }
        public Decimal oc_percent { get; set; }
        public Int32 upline1_user_id { get; set; }
        public String upline1_code { get; set; }
        public String upline1_name { get; set; }
        public String upline1_role { get; set; }
        public Decimal psc_percent1 { get; set; }
        public Decimal oc_percent1 { get; set; }
        public Int32 upline2_user_id { get; set; }
        public String upline2_code { get; set; }
        public String upline2_name { get; set; }
        public String upline2_role { get; set; }
        public Decimal psc_percent2 { get; set; }
        public Decimal oc_percent2 { get; set; }
        public Int32 upline3_user_id { get; set; }
        public String upline3_code { get; set; }
        public String upline3_name { get; set; }
        public String upline3_role { get; set; }
        public Decimal psc_percent3 { get; set; }
        public Decimal oc_percent3 { get; set; }
    }


    [Database("comm_structure_mappings")]
    public class CommStructureMapping
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("commission_setting_id"), Display("id")]
        public Int32 commission_setting_id { get; set; }
        [Database("fund_id"), Display("id")]
        public Int32 fund_id { get; set; }
        [Database("is_active"), Display("id")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("id")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("id")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("id")]
        public DateTime updated_date { get; set; }
        [Database("updated_by"), Display("id")]
        public Int32 updated_by { get; set; }
        [Database("status"), Display("id")]
        public Int32 status { get; set; }
    }

    public class CommStructMapDetail
    {
        public Int32 id { get; set; }
        public Int32 commission_setting_id { get; set; }
        public String commission_setting_name { get; set; }
        public Int32 fund_id { get; set; }
        public String fund_name { get; set; }
        public Int32 is_active { get; set; }
        public DateTime created_date { get; set; }
        public Int32 created_by { get; set; }
        public DateTime updated_date { get; set; }
        public Int32 updated_by { get; set; }
        public Int32 status { get; set; }
    }

    [Database("comm_ledger")]
    public class CommLedger
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_id"), Display("id")]
        public Int32 agent_id { get; set; }
        [Database("agent_code"), Display("id")]
        public String agent_code { get; set; }
        [Database("agent_name"), Display("id")]
        public String agent_name { get; set; }
        [Database("user_id"), Display("id")]
        public Int32 user_id { get; set; }
        [Database("user_name"), Display("id")]
        public String user_name { get; set; }
        [Database("user_order_id"), Display("id")]
        public Int32 user_order_id { get; set; }
        [Database("order_no"), Display("id")]
        public String order_no { get; set; }
        [Database("comm_psc"), Display("id")]
        public Decimal comm_psc { get; set; }
        [Database("comm_oc"), Display("id")]
        public Decimal comm_oc { get; set; }
        [Database("is_active"), Display("id")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("id")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("id")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("id")]
        public DateTime updated_date { get; set; }
        [Database("updated_by"), Display("id")]
        public Int32 updated_by { get; set; }
        [Database("status"), Display("id")]
        public Int32 status { get; set; }
    }
}
