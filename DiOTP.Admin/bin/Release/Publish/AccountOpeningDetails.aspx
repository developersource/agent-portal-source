﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountOpeningDetails.aspx.cs" Inherits="Admin.AccountOpeningDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                

                <%--<div class="col-md-6">
                    <h3 class="table-heading">Bank Details<small class="pull-right"><strong id="asOnDate" runat="server"></strong></small></h3>
                    <table class="table userDetailsTable">
                        <tr>
                            <th>No. of MA Accounts</th>
                            <td id="noOfMAAccounts" runat="server">-</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-right">
                                <a id="impersonateUserLink" runat="server" href='javascript:;' target='_blank' class='btn btn-sm btn-primary'>Impersonate User</a>
                            </td>
                        </tr>
                    </table>
                </div>--%>

                <div class="col-md-12">
                    <h3 class="table-heading">Account Detail Info</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>ID No.</th>
                                <th>Salutation</th>
                                <th>Gender</th>
                                <th>Resident of Malaysia</th>
                                <th>Nationality</th>
                                <th>Race</th>
                                <th>Bumiputra Status</th>
                                <th>Date of Birth</th>
                                <th>Marital Status</th>
                                <th>Agent Code</th>
                            </tr>
                        </thead>
                        <tbody id="accountDetailInfoTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Address</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Address Type</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Post Code</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Telephone number</th>
                            </tr>
                        </thead>
                        <tbody id="addressTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Occupation</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Occupation</th>
                                <th>Nature of Business</th>
                                <th>Monthly Income</th>
                                <th>Employer</th>
                                
                            </tr>
                        </thead>
                        <tbody id="occupationTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Financial Profile</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Purpose</th>
                                <th>Source</th>
                                <th>Funder name</th>
                                <th>Relationship</th>
                                <th>Funders Industory</th>
                                <th>Funder is Money Changer</th>
                                <th>Funder is Beneficial Owner</th>
                                <th>Fund Owner Name</th>
                                <th>Estimated Net Worth</th>
                            </tr>
                        </thead>
                        <tbody id="financialTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Bank Details</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Account name</th>
                                <th>Account number</th>
                                <th>Bank name</th>
                                <th>Account Type</th>
                                <th>Currency</th>
                            </tr>
                        </thead>
                        <tbody id="bankDetailsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">PEP</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>PEP Status</th>
                                <th>Family PEP Status</th>
                            </tr>
                        </thead>
                        <tbody id="pepTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">CRS</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Tax Residency Status</th>
                                <th>TIN</th>
                            </tr>
                        </thead>
                        <tbody id="crsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Uploaded Documents</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Document Type</th>
                                <th>Uploaded Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="documentsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                

            </div>
        </div>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $('.dataTable.maAccounts').DataTable({
                        dom: '',
                        ordering: false
                    });
                }, 500);
            });
        </script>
    </form>
</body>
</html>
