﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class Profile : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserTypesDefService> lazyUserTypesDefServiceObj = new Lazy<IUserTypesDefService>(() => new UserTypesDefService());
        public static IUserTypesDefService IUserTypesDefService { get { return lazyUserTypesDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else
            {
                Boolean isCurrentUser = false;
                User user = (User)Session["admin"];
                StringBuilder asb = new StringBuilder();
                Response response = IUserService.GetSingle(user.Id);
                if (response.IsSuccess)
                {
                    user = (User)response.Data;
                    if (user != null)
                    {
                        isCurrentUser = true;
                        Response responseUTList = IUserTypeService.GetDataByFilter(" user_id='" + user.Id + "'", 0, 0, false);
                        if (responseUTList.IsSuccess)
                        {
                            if (user.Id == 0 || user.Id == 1)
                            {
                                asb.Append(@"<tr>
                                                <td>Username</td>
                                                <td><strong>" + user.Username + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><strong>" + user.EmailId + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Last Login Date</td>
                                                <td><strong>" + (isCurrentUser ? (Session["LastLoginTime"] == null ? "" : ((DateTime)(Session["LastLoginTime"])).ToString("dd/MM/yyyy HH:mm:ss")) : (user.LastLoginDate == null ? "" : user.LastLoginDate.Value.ToString("dd/MM/yyyy HH:mm:ss"))) + @"</strong></td>                  
                                            </tr>");
                            }
                            else
                            {
                                UserType userType = ((List<UserType>)responseUTList.Data).FirstOrDefault();
                                if (userType != null)
                                {
                                    Response responseUTDList = IUserTypesDefService.GetDataByFilter(" name like '%Admin' ", 0, 0, false);
                                    if (responseUTDList.IsSuccess)
                                    {
                                        UserTypesDef userTypesDefs = ((List<UserTypesDef>)responseUTDList.Data).Where(x => x.Id == userType.UserTypeId).FirstOrDefault();
                                        if (userTypesDefs != null)
                                        {
                                            asb.Append(@"<tr>
                                                            <td>Username</td>
                                                            <td><strong>" + user.Username + @"</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>User Type</td>
                                                            <td><strong>" + userTypesDefs.Name + @"</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Mobile number</td>
                                                            <td><strong>" + user.MobileNumber + @"</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td><strong>" + user.EmailId + @"</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Last Login Date</td>
                                                            <td><strong>" + (isCurrentUser ? (Session["LastLoginTime"] == null ? "" : ((DateTime)(Session["LastLoginTime"])).ToString("dd/MM/yyyy HH:mm:ss")) : (user.LastLoginDate == null ? "" : user.LastLoginDate.Value.ToString("dd/MM/yyyy HH:mm:ss"))) + @"</strong></td>
                                                        </tr>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                usersTbody.InnerHtml = asb.ToString();
            }
        }
    }
}