﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.FPXLibary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class FPXDirect : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }

        private static readonly Lazy<IPaymentDetailService> lazyIPaymentDetailServiceObj = new Lazy<IPaymentDetailService>(() => new PaymentDetailService());
        public static IPaymentDetailService IPaymentDetailService { get { return lazyIPaymentDetailServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        public static List<Cart> cartItems = new List<Cart>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Controller c = new Controller();
                String fpx_buyerBankBranch = Request.Form["fpx_buyerBankBranch"];
                String fpx_buyerBankId = Request.Form["fpx_buyerBankId"];
                String fpx_buyerIban = Request.Form["fpx_buyerIban"];
                String fpx_buyerId = Request.Form["fpx_buyerId"];
                String fpx_buyerName = Request.Form["fpx_buyerName"];
                String fpx_creditAuthCode = Request.Form["fpx_creditAuthCode"];
                String fpx_creditAuthNo = Request.Form["fpx_creditAuthNo"];
                String fpx_debitAuthCode = Request.Form["fpx_debitAuthCode"];
                String fpx_debitAuthNo = Request.Form["fpx_debitAuthNo"];
                String fpx_fpxTxnId = Request.Form["fpx_fpxTxnId"];
                String fpx_fpxTxnTime = Request.Form["fpx_fpxTxnTime"];
                String fpx_makerName = Request.Form["fpx_makerName"];
                String fpx_msgToken = Request.Form["fpx_msgToken"];
                String fpx_msgType = Request.Form["fpx_msgType"];
                String fpx_sellerExId = Request.Form["fpx_sellerExId"];
                String fpx_sellerExOrderNo = Request.Form["fpx_sellerExOrderNo"];
                String fpx_sellerId = Request.Form["fpx_sellerId"];
                String fpx_sellerOrderNo = Request.Form["fpx_sellerOrderNo"];
                String fpx_sellerTxnTime = Request.Form["fpx_sellerTxnTime"];
                String fpx_txnAmount = Request.Form["fpx_txnAmount"];
                String fpx_txnCurrency = Request.Form["fpx_txnCurrency"];
                String fpx_checkSum = Request.Form["fpx_checkSum"];
                String fpx_checkSumString = "";
                fpx_checkSumString = fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|" + fpx_creditAuthCode + "|" + fpx_creditAuthNo + "|" + fpx_debitAuthCode + "|" + fpx_debitAuthNo + "|" + fpx_fpxTxnId + "|" + fpx_fpxTxnTime + "|" + fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|";
                fpx_checkSumString += fpx_sellerExId + "|" + fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency;

                String mesg_type = Request.Form["mesg_type"];
                String mesgFromFpx = Request.Form["mesgFromFpx"];
                String mesg_token = Request.Form["mesg_token"];
                String key_type = Request.Form["key_type"];
                String seller_ex_desc = Request.Form["seller_ex_desc"];
                String seller_ex_id = Request.Form["seller_ex_id"];
                String order_no = Request.Form["order_no"];
                String seller_txn_time = Request.Form["seller_txn_time"];
                String seller_order_no = Request.Form["seller_order_no"];
                String seller_id = Request.Form["seller_id"];
                String seller_fpx_bank_code = Request.Form["seller_fpx_bank_code"];
                String buyer_mail_id = Request.Form["buyer_mail_id"];
                String txn_amt = Request.Form["txn_amt"];
                String checksum = Request.Form["checksum"];
                String debit_auth_code = Request.Form["debit_auth_code"];
                String debit_auth_no = Request.Form["debit_auth_no"];
                String buyer_bank = Request.Form["buyer_bank"];
                String buyer_bank_branch = Request.Form["buyer_bank_branch"];
                String buyer_name = Request.Form["buyer_name"];
                String FPX_TXN_ID = Request.Form["FPX_TXN_ID"];
                String finalVerifiMsg = "ERROR";
                String xmlMessage = "";
                xmlMessage = fpx_checkSumString;
                finalVerifiMsg = c.nvl_VerifiMsg(fpx_checkSumString, fpx_checkSum, Request.PhysicalApplicationPath); //Certificate Path

                string fpx_response_code = fpx_debitAuthCode;
                string query = "SELECT * FROM fpx_response_codes where code = '" + fpx_response_code + "' ";
                Response responseCodesList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);

                var uoDyn = responseCodesList.Data;
                var responseJSON = JsonConvert.SerializeObject(uoDyn);
                List<FpxResponseCode> responseCodes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FpxResponseCode>>(responseJSON);
                string fpxResponseDesc = "";
                if (responseCodes.Count > 0)
                {
                    FpxResponseCode fpxResponseCode = responseCodes.FirstOrDefault();
                    fpxResponseDesc = fpxResponseCode.Name;
                }
                int OrderStatus = 1;
                if (fpx_debitAuthCode.CompareTo("00") == 0)
                {
                    OrderStatus = 2;
                }
                else if (fpx_debitAuthCode.CompareTo("09") == 0)
                {
                    OrderStatus = 22;
                }
                else if (fpx_debitAuthCode.CompareTo("99") == 0)
                {
                    OrderStatus = 22;
                }
                else
                {
                    OrderStatus = 29;
                }

                Response responseUOList = IUserOrderService.GetDataByFilter(" trans_no='" + fpx_sellerTxnTime + "' and order_no='" + fpx_sellerOrderNo + "' ", 0, 0, true);
                List<UserOrder> userOrders = new List<UserOrder>();
                if (responseUOList.IsSuccess)
                {
                    userOrders = (List<UserOrder>)responseUOList.Data;

                    Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), fpx_sellerOrderNo, true, 0, 0, false);
                    if (response.IsSuccess)
                    {
                        List<UserOrder> ol = (List<UserOrder>)response.Data;
                        if (ol.Count > 0)
                        {
                            userOrders = ol;
                        }
                    }


                    if (userOrders.Count > 0)
                    {

                        string fundName = "";
                        string fundName2 = "";
                        List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                        List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();

                        foreach (UserOrder uo in userOrders)
                        {
                            UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                            Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                            if (responseUFI1.IsSuccess)
                            {
                                List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                                if (utmcFundInformations.Count > 0)
                                {
                                    utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                                }
                            }
                            UtmcFundInformation utmcFundInformation2 = new UtmcFundInformation();
                            if (uo.ToFundId != 0)
                            {
                                Response responseUFI2 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.ToFundId + " ", 0, 0, true);
                                if (responseUFI2.IsSuccess)
                                {
                                    List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI2.Data;
                                    if (utmcFundInformations.Count > 0)
                                    {
                                        utmcFundInformation2 = utmcFundInformations.FirstOrDefault();
                                    }
                                }
                            }

                            UserAccount userAccount2 = new UserAccount();
                            if (uo.ToAccountId != 0)
                            {
                                Response responseUFI2 = IUserAccountService.GetDataByFilter(" id=" + uo.ToAccountId + " and status='1' ", 0, 0, true);
                                if (responseUFI2.IsSuccess)
                                {
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUFI2.Data;
                                    if (userAccounts.Count > 0)
                                    {
                                        userAccount2 = userAccounts.FirstOrDefault();
                                    }
                                }
                            }

                            fundName = utmcFundInformation1.FundName.Capitalize();
                            if (uo.ToFundId != 0)
                                fundName2 = utmcFundInformation2.FundName.Capitalize();
                            funds.Add(utmcFundInformation1);
                            funds2.Add(utmcFundInformation2);
                        }
                        Logger.WriteLog("OrderNo: " + userOrders.FirstOrDefault().OrderNo);

                        Response responseUAList = IUserAccountService.GetDataByFilter(" id=" + userOrders.FirstOrDefault().UserAccountId + " and user_id=" + userOrders.FirstOrDefault().UserId + " and status=1 ", 0, 0, true);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            UserAccount primaryAccount = new UserAccount();
                            if (userAccounts.Count > 0)
                            {
                                primaryAccount = userAccounts.FirstOrDefault();

                                Logger.WriteLog("userOrders.Count: " + userOrders.Count);

                                userOrders.ForEach(x =>
                                {
                                    Logger.WriteLog("userOrders: " + x.FundId + " - " + x.Amount + ", " + x.Units);
                                    x.OrderStatus = OrderStatus;
                                    x.FpxTransactionId = fpx_fpxTxnId;
                                    x.FpxStatus = fpx_debitAuthCode;
                                    x.BankCode = fpx_buyerBankId;
                                    x.PaymentDate = DateTime.Now;
                                });

                                int updatedRecs = IUserOrderService.UpdateBulkData(userOrders);
                                Logger.WriteLog("updatedRecs: " + updatedRecs);

                                if (OrderStatus == 2)
                                {
                                    Email email = new Email();
                                    User user = (User)Session["user"];
                                    if (user == null)
                                    {
                                        Response responseUser = IUserService.GetSingle(userOrders.FirstOrDefault().UserId);
                                        if (responseUser.IsSuccess)
                                        {
                                            user = (User)responseUser.Data;
                                        }
                                    }
                                    email.user = user;
                                    EmailService.SendOrderEmail(userOrders, email, funds, funds2, primaryAccount.AccountNo);
                                }
                                else if (OrderStatus == 29)
                                {
                                    Email email = new Email();
                                    User user = (User)Session["user"];
                                    if (user == null)
                                    {
                                        Response responseUser = IUserService.GetSingle(userOrders.FirstOrDefault().UserId);
                                        if (responseUser.IsSuccess)
                                        {
                                            user = (User)responseUser.Data;
                                        }
                                    }
                                    email.user = user;
                                    email.fpxResponseDesc = fpxResponseDesc;
                                    EmailService.SendOrderEmail(userOrders, email, funds, funds2, primaryAccount.AccountNo);
                                }

                                if (!string.IsNullOrEmpty(fpx_fpxTxnId))
                                {
                                    Response responsePaymentDetails = GenericService.PullData<PaymentDetail>(" transaction_id = '" + fpx_fpxTxnId + "' ", 0, 0, true, nameof(PaymentDetail.Id));
                                    if (responsePaymentDetails.IsSuccess)
                                    {
                                        List<PaymentDetail> paymentDetails = (List<PaymentDetail>)responsePaymentDetails.Data;
                                        PaymentDetail paymentDetail = new PaymentDetail();
                                        paymentDetail.BuyerAccNo = fpx_buyerName;
                                        paymentDetail.BuyerBankBranch = fpx_buyerBankBranch;
                                        paymentDetail.BuyerBankId = fpx_buyerBankId;
                                        paymentDetail.BuyerEmail = fpx_buyerId;
                                        paymentDetail.BuyerIban = fpx_buyerIban;
                                        paymentDetail.BuyerId = fpx_buyerId;
                                        paymentDetail.BuyerName = fpx_buyerName;
                                        paymentDetail.CreatedOn = DateTime.Now;
                                        paymentDetail.MakerName = fpx_makerName;
                                        paymentDetail.MsgToken = fpx_msgToken;
                                        paymentDetail.MsgType = fpx_msgType;
                                        paymentDetail.SellerBankCode = fpx_sellerExId;
                                        paymentDetail.SellerExId = fpx_sellerExId;
                                        paymentDetail.SellerExOrderNo = fpx_sellerExOrderNo;
                                        paymentDetail.SellerId = fpx_sellerId;
                                        paymentDetail.SellerOrderNo = fpx_sellerOrderNo;
                                        paymentDetail.SellerTxnTime = fpx_sellerTxnTime;
                                        paymentDetail.FpxTxnTime = fpx_fpxTxnTime;
                                        paymentDetail.TransactionId = fpx_fpxTxnId;
                                        paymentDetail.TxnAmount = fpx_txnAmount;
                                        paymentDetail.TxnCurrency = fpx_txnCurrency;
                                        paymentDetail.Version = "6.0";
                                        if (paymentDetails.Count == 0)
                                        {
                                            IPaymentDetailService.PostData(paymentDetail);
                                        }
                                        else
                                        {
                                            paymentDetail.Id = paymentDetails.FirstOrDefault().Id;
                                            IPaymentDetailService.UpdateData(paymentDetail);
                                        }
                                    }

                                }
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}