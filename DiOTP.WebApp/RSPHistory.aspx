﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="RSPHistory.aspx.cs" Inherits="DiOTP.WebApp.RSPHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        .loader-bg {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">MyApex</a></li>
                    <li><a href="/Settings.aspx">Settings</a></li>
                    <li class="active">Regular Savings Plan History</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Regular Savings Plan History</h3>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h5>Regular Savings Plan history for Master Account: <span id="spanUsername" runat="server"></span></h5>

                    <div class="tabs-horizontal my-tabs mob" id="tabsDiv" runat="server">
                        <ul class="nav nav-tabs my-nav-tabs mb-10">
                            <%--<li id="type1" runat="server">
                                <a href="RSPHistory.aspx" class="mr-5">Buy</a>
                            </li>
                            <li id="type2" runat="server">
                                <a href="RSPHistory.aspx?type=2" class="mr-5 ml-5">Sell</a>
                            </li>
                            <li id="type3" runat="server">
                                <a href="RSPHistory.aspx?type=3" class="mr-5 ml-5">Switch</a>
                            </li>--%>
                            <li id="type5" runat="server" class="mr-5 mb-m-5">
                                <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="10" Text="All"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Pending"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Approved"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="Denied"></asp:ListItem>
                                </asp:DropDownList>
                            </li>
                            <li id="type4" runat="server">
                                <asp:DropDownList ID="ddlTransactionPeriod" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="Transaction Period"></asp:ListItem>
                                    <asp:ListItem Value="-3" Text="Past 3 months"></asp:ListItem>
                                    <asp:ListItem Value="-6" Text="Past 6 months"></asp:ListItem>
                                    <asp:ListItem Value="-12" Text="Past 12 months"></asp:ListItem>
                                </asp:DropDownList>
                            </li>
                            <li id="type6" runat="server">
                                <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-infos1 ml-10 mr-5" OnClick="btnsearch_Click" />
                            </li>
                            <li id="type7" runat="server">
                                <asp:Button ID="btnreset" runat="server" CssClass="btn btn-infos1 ml-5 mr-5" Text="Reset" OnClick="btnreset_Click" />
                            </li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane fade active in" id="buy">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-font-size-13 table-cell-pad-5 table-bordered OrderHistoryTable">
                                            <thead id="theadUserOrders" runat="server">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Order Number</th>
                                                    <th>Fund Name</th>
                                                    <th>Order Date</th>
                                                    <th class="text-right">Investment Amount
                                                        <br />
                                                        MYR</th>
                                                    <th class="text-right">Order Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyUserOrders" runat="server">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script>
        var isCurFormat;
        var isCurFormatNoSymbol;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: 4 });
            });
            $('.unitFormatNoDecimal').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: 0 });
            });
        }
        FormatAllUnit();
        $(document).ready(function () {

            var orderHistoryTable = $('.OrderHistoryTable').DataTable({
                dom: "t<'row'<'col-md-6 infoDiv'><'col-md-6 text-right paginationDiv'>>",
                select: true,
                destroy: true,
                responsive: true,
                order: [],
                ordering: false,
                searching: false,
                length: false,
                autoWidth: false
            });

            var pageNo = parseInt($('#hdnCurrentPageNo').val());
            var total = parseInt($('#hdnTotalRecordsCount').val());
            var numPerPage = parseInt($('#hdnNumberPerPage').val());
            var pageNos = parseInt((total / numPerPage) + 1);

            $('.infoDiv').html("Showing page " + (pageNo == NaN ? "1" : pageNo) + " of " + pageNos);

            var ul = $('<ul class="pagination" />');
            var liFirst = $('<li />').addClass('firstPage');
            var aFirst = $('<a />').attr('href', 'javascript:;').addClass('pageNumber ' + (pageNo == 1 ? 'active' : '')).html('First').appendTo(liFirst);
            liFirst.appendTo(ul);
            var liPrev = $('<li />').addClass('prev');
            var aPrev = $('<a />').attr('href', 'javascript:;').addClass('pageNumber').html('Prev').appendTo(liPrev);
            liPrev.appendTo(ul);
            var className = 'pageNumber pageNoDis';
            for (i = 1; i <= pageNos; i++) {
                if (i < 3) {
                    if (i == pageNo)
                        className = 'pageNumber pageNoDis active';
                    else
                        className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                    li.appendTo(ul);
                }
                else if (i > pageNo - 3 && i < pageNo + 3) {
                    if (i == pageNo)
                        className = 'pageNumber pageNoDis active';
                    else
                        className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                    li.appendTo(ul);
                }
                else if (i > pageNos - 2) {
                    className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                    li.appendTo(ul);
                }
                if (i == 3 && pageNo >= 6 || i == pageNos - 3 && pageNo <= pageNos - 6) {
                    className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" />').html('...').appendTo(li);
                    li.appendTo(ul);
                }
            }
            var liNext = $('<li />').addClass('next');
            var aNext = $('<a />').attr('href', 'javascript:;').addClass('pageNumber').html('Next').appendTo(liNext);
            liNext.appendTo(ul);
            var liLast = $('<li />').addClass('lastPage');
            var aLast = $('<a />').attr('href', 'javascript:;').addClass('pageNumber ' + (pageNo == pageNos ? 'active' : '')).html('Last').appendTo(liLast);
            liLast.appendTo(ul);
            $('.paginationDiv').append(ul);
            $('li.firstPage').click(function () {
                var curPageNo = parseInt(1);
                $('#hdnCurrentPageNo').val(curPageNo);
                $('form').submit();
            });
            $('li.lastPage').click(function () {
                var curPageNo = parseInt($('.pageNoDis').last().text());
                $('#hdnCurrentPageNo').val(curPageNo);
                $('form').submit();
            });
            $('li.next').click(function () {
                var curPageNo = parseInt($('#buy .pageNumber.pageNoDis.active').text());
                if (curPageNo >= parseInt($('.pageNoDis').last().text())) {

                }
                else {
                    $('#hdnCurrentPageNo').val(curPageNo + 1);
                    $('form').submit();
                }

            });
            $('li.prev').click(function () {
                var curPageNo = parseInt($('#buy .pageNumber.pageNoDis.active').text());
                if (curPageNo <= parseInt($('.pageNoDis').first().text())) {

                }
                else {
                    $('#hdnCurrentPageNo').val(curPageNo - 1);
                    $('form').submit();
                }
            });
            $('.pageNumber.pageNoDis').click(function () {
                var curPageNo = parseInt($(this).text());
                $('#hdnCurrentPageNo').val(curPageNo);
                $('form').submit();
            });
        });
    </script>
</asp:Content>
