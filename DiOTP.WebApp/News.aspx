﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="DiOTP.WebApp.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Announcements</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Announcements</h3>
                    </div>

                  
                    <div class="row">
                        <div class="col-md-12">

                            <div class="news-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="Content/Banner/15.png" alt="Content" class="img-responsive" />
                                    </div>
                                    <div class="col-md-8">
                                        <div class="news-right">
                                            <h4 id="contentTitle" runat="server">Grand launch of eApexIs Online.</h4>
                                            <span id="contentDate" runat="server">01.01.2018</span>
                                            <p class="line-clamp" id="contentContent" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra nisl ut tincidunt placerat. Suspendisse sollicitudin felis scelerisque augue aliquet, ut congue ipsum iaculis. Morbi ornare ac arcu in imperdiet. Ut et iaculis quam. Duis ultrices enim est, id sodales sapien dignissim et. Cras dolor orci, semper vulputate nisi non, dictum fringilla tellus. </p>
                                            <button type="button" class="btn btn-readmore" onclick="javascript:window.location.href='News-Content.aspx'">Read more</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="news-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="Content/Banner/BANNER-2-Banner.jpg" alt="Content" class="img-responsive" />
                                    </div>
                                    <div class="col-md-8">
                                        <div class="news-right">
                                            <h4>Launch of FPX.</h4>
                                            <span>01.01.2018</span>
                                            <p class="line-clamp">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra nisl ut tincidunt placerat. Suspendisse sollicitudin felis scelerisque augue aliquet, ut congue ipsum iaculis. Morbi ornare ac arcu in imperdiet. Ut et iaculis quam. Duis ultrices enim est, id sodales sapien dignissim et. Cras dolor orci, semper vulputate nisi non, dictum fringilla tellus. </p>
                                            <button type="button" class="btn btn-readmore" onclick="javascript:window.location.href='News-Content.aspx'">Read more</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="news-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="Content/Banner/9.jpg" alt="Content" class="img-responsive" />
                                    </div>
                                    <div class="col-md-8">
                                        <div class="news-right">
                                            <h4>Security with Two Level Authentication.</h4>
                                            <span>01.01.2018</span>
                                            <p class="line-clamp">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra nisl ut tincidunt placerat. Suspendisse sollicitudin felis scelerisque augue aliquet, ut congue ipsum iaculis. Morbi ornare ac arcu in imperdiet. Ut et iaculis quam. Duis ultrices enim est, id sodales sapien dignissim et. Cras dolor orci, semper vulputate nisi non, dictum fringilla tellus. </p>
                                            <button type="button" class="btn btn-readmore" onclick="javascript:window.location.href='News-Content.aspx'">Read more</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="news-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="Content/Banner/2.jpg" alt="Content" class="img-responsive" />
                                    </div>
                                    <div class="col-md-8">
                                        <div class="news-right">
                                            <h4>Trading Platform.</h4>
                                            <span>01.01.2018</span>
                                            <p class="line-clamp">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra nisl ut tincidunt placerat. Suspendisse sollicitudin felis scelerisque augue aliquet, ut congue ipsum iaculis. Morbi ornare ac arcu in imperdiet. Ut et iaculis quam. Duis ultrices enim est, id sodales sapien dignissim et. Cras dolor orci, semper vulputate nisi non, dictum fringilla tellus. </p>
                                            <button type="button" class="btn btn-readmore" onclick="javascript:window.location.href='News-Content.aspx'">Read more</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
