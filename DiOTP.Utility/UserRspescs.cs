﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_rspes")]
    public class UserRspescs
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("Fund_ID"), Display("Fund_ID")]
        public Int32 Fund_ID { get; set; }
        [Database("User_Id"), Display("User_Id")]
        public Int32 User_Id { get; set; }
        [Database("Rsp_Amount"), Display("Rsp_Amount")]
        public decimal Rsp_Amount { get; set; }
        [Database("Fund_Category"), Display("Fund_Category")]
        public Int32 Fund_Category { get; set; }
        [Database("Ma_Holder_bankId"), Display("Ma_Holder_bankId")]
        public Int32 Ma_Holder_bankId { get; set; }
        [Database("Rsp_Date"), Display("Rsp_Date")]
        public DateTime Rsp_Date { get; set; }
        [Database("Status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
