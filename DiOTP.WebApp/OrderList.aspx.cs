﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.WebApp
{
    public partial class OrderList : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        public int UserChoosetype;
        public List<UserAccount> userAccounts { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null && Session["isVerified"] != null)
                {

                    if (!IsPostBack)
                    {
                        User user = (User)Session["user"];

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {

                            userAccounts = (List<UserAccount>)responseUAList.Data;

                            foreach (UserAccount x in userAccounts)
                            {
                                ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(x.HolderClass) + " - " + x.AccountNo.ToString(), "'" + x.AccountNo + "'"));
                            }
                            ddlUserAccountId.Items.Insert(0, new ListItem("All Accounts", String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToList())));

                            string accNo = Request.QueryString["accNo"];
                            string typeString = Request.QueryString["type"];
                            if (!String.IsNullOrEmpty(accNo))
                            {
                                ddlUserAccountId.SelectedValue = "'" + accNo + "'";
                            }
                            if (!String.IsNullOrEmpty(typeString))
                            {
                                ddlOrderType.SelectedValue = typeString;
                            }
                            BindOrderList();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void BindOrderList()
        {
            if (Session["user"] != null && Session["isVerified"] != null && ddlUserAccountId.SelectedValue != "")
            {
                Boolean IsImpersonated = ServicesManager.IsImpersonated(Context);
                User user = (User)Session["user"];
                spanMA.InnerHtml = ddlUserAccountId.SelectedItem.Text;
                string accountNos = ddlUserAccountId.SelectedValue;
                string orderType = ddlOrderType.SelectedValue;
                string orderStatus = ddlOrderStatus.SelectedValue;
                string transPeriod = ddlTransactionPeriod.SelectedValue;
                if (orderStatus == "0")
                {
                    orderStatus = "('1', '12')";
                }
                else if (orderStatus == "1")
                {
                    orderStatus = "('2','22')";
                }
                else if (orderStatus == "2")
                {
                    orderStatus = "('3')";
                }
                if (transPeriod == "-3")
                {
                    transPeriod = "TIMESTAMPDIFF(MONTH, uo.created_date, NOW()) < 3";
                }
                else if (transPeriod == "-6")
                {
                    transPeriod = "TIMESTAMPDIFF(MONTH, uo.created_date, NOW()) < 6";
                }
                else if (transPeriod == "-12")
                {
                    transPeriod = "TIMESTAMPDIFF(MONTH, uo.created_date, NOW()) < 12";
                }
                StringBuilder filter = new StringBuilder();
                filter.Append(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units, ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, uo.confirmation_date FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.user_id='" + user.Id + "' ");
                if (orderType != "")
                {
                    filter.Append(" and uo.order_type = '" + orderType + "' ");
                }
                else
                {
                    filter.Append(" and uo.order_type in ('1', '2', '3') ");
                }
                if (orderStatus != "")
                {
                    filter.Append(" and uo.order_status in " + orderStatus + " ");
                }
                else
                {
                    filter.Append(" and uo.order_status not in ('18', '19', '29', '39') ");
                }
                if (accountNos != "")
                {
                    filter.Append(" and ua.account_no in (" + accountNos + ") ");
                }
                if (transPeriod != "")
                {
                    filter.Append(" and " + transPeriod + " ");
                }

                filter.Append(" group by uo.order_no");
                filter.Append(" order by FIELD(uo.order_status, '12', '1', '22', '2', '3'), uo.confirmation_date desc, uo.created_date desc ");
                Response responseUserOrderList = GenericService.GetDataByQuery(filter.ToString(), 0, 0, false, null, false, null, true);
                if (responseUserOrderList.IsSuccess)
                {
                    var rawDynData = responseUserOrderList.Data;
                    var responseJSON = JsonConvert.SerializeObject(rawDynData);
                    List<OrderListDTO> UserOrderList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseJSON);

                    StringBuilder tableBuilder = new StringBuilder();
                    int index = 1;

                    UserOrderList.Where(x => x.order_status == 1 || x.order_status == 12).ToList().ForEach(x =>
                      {
                          Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, uo.consultantid, ua.account_no, uo.order_no, uo.order_type, uo.created_by, uo.created_date, uo.amount, uo.units, 
                            ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT, 
                            uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, uo.trans_amt, 
                            uo.trans_units, uo.app_fee, uo.exit_fee, uo.fee_amt, uo.net_amt, uo.trans_pr FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID
                            where uo.order_no='" + x.order_no + @"' and uo.order_type in (1,2,3) order by uo.created_date desc ", 0, 0, false, null, false, null, true);

                          if (responseMO.IsSuccess)
                          {
                              var rawDynMOData = responseMO.Data;
                              var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                              List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                              string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                              string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                              decimal totalAmtEst = 0;
                              decimal totalUnitsEst = 0;

                              string ordertype = "";
                              if (x.order_type == (int)OrderType.Buy)
                                  ordertype = "BUY";
                              else if (x.order_type == (int)OrderType.Sell)
                                  ordertype = "SELL";
                              else if (x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut)
                                  ordertype = "SWITCH";
                              else if (x.order_type == (int)OrderType.RSP)
                                  ordertype = "RSP";

                              string status = "";
                              if (x.order_status == 1 || x.order_status == 12)
                              {
                                  status = "Pending";// Order Placed but not yet proceed to FPX/ not yet verified by user
                              }
                              else if (x.order_status == 2 && (x.order_type == (int)OrderType.RSP))
                              {
                                  status = "Processing";//Pending: FPX verified
                              }
                              else if (x.order_status == 2 && (x.order_type == (int)OrderType.Sell || x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut))
                                  status = "Processing";// Pending for order approval
                              else if (x.order_status == 18)
                                  status = "Order cancelled";
                              else if (x.order_status == 22 && x.payment_method == "1")
                                  status = "Processing";//Pending: for FPX Payment
                              else if (x.order_status == 22 && x.payment_method == "2")
                                  status = "Processing";//Pending: For payment approval
                              else if (x.order_status == 29 && x.payment_method == "1")
                                  status = "Payment failed";
                              else if (x.order_status == 29 && x.payment_method == "2")
                                  status = "Payment rejected";
                              else if (x.order_status == 3)
                                  status = "Completed";
                              else if (x.order_status == 39)
                                  status = "Order rejected";
                              else if (x.order_status == 2)
                                  status = "Processing";//pending: Payment success

                              tableBuilder.Append(@"<tr>
                                                <td><a href='javascript:;' class='show-extended-row'><i class='fa fa-plus'></i></a></td>
                                                <td>" + index + @"</td>
                                                <td>" + x.account_no + @"</td>
                                                <td>" + x.order_no + @"</td>
                                                <td>" + ordertype + @"</td>
                                                <td>" + ((status == "Pending") ? x.created_date.ToString("dd/MM/yyyy HH:mm:ss") : ((status == "Processing") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : ((status == "Completed" || status == "Payment failed" || status == "Order rejected") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : "-"))) + @"</td>
                                                <td>" + status + @"</td>
                                                <td class='text-right'>" + (ordertype == "BUY" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                            </tr>");
                              StringBuilder mos = new StringBuilder();
                              string actionsHTML = "-";

                              if ((x.order_status == 1 || x.order_status == 12) && !IsImpersonated)
                              {
                                  actionsHTML = "";
                                  actionsHTML += "<div class='btn-group'>";
                                  actionsHTML += "<a class='btn btn-info btn-sm mb-5' href='Show-Cart.aspx?UON=" + x.order_no + "'>" + (x.order_type == 1 ? "Pay" : (x.order_type == 2 || x.order_type == 3 ? "Request" : (x.order_type == 6 ? "Verify" : "-"))) + "</a>";
                                  actionsHTML += "<a class='btn btn-default btn-sm cancel-order mb-5' data-orderno='" + x.order_no + "' href='javascript:;'>Cancel</a>";
                                  actionsHTML += "</div>";
                              }
                              string commonActionRow = "<td rowspan='" + (x.order_type == 1 || x.order_type == 2 ? UserOrderMOList.Count : (UserOrderMOList.Count * 2)) + @"' class='text-center'>" + actionsHTML + @"</td>";
                              int indexsub = 0;
                              Decimal totalUnitsValueMYR = 0;
                              string totalUnitsInValue = "";

                              int idx = 0;

                              UserOrderMOList.ForEach(y =>
                              {
                                  string transno = y.trans_no == "" ? "-" : y.trans_no;
                                  OrderListDTO uo = y;
                                  int i = 0;

                                  string url = "";
                                  Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.order_no, true, 0, 0, true);
                                  if (responseUOFList.IsSuccess)
                                  {
                                      UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                      if (uof != null)
                                          url = uof.Url;
                                      else
                                          url = "FPX PAYMENT";
                                  }
                                  if (ordertype == "BUY" || ordertype == "RSP")
                                  {
                                      decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                      totalAmtEst += UnitsValueMYR;
                                      decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                                      totalUnitsEst += UnitsValue;
                                      commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                      Type t = GetType("FPXBank");
                                      string BankName = uo.fpx_bank_code;
                                      FPXBank b = FPXEnumeration.GetByBankId(t, BankName);
                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td>" + (uo.payment_method == "1" ? "FPX PAYMENT (" + (b == null ? BankName : b.DisplayName) + @")" : (uo.payment_method == "2" ? "Proof of Payment <a href=\"" + url + "\" target='_blank'>view</a>" : "-")) + @"</td>
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.fee_amt.HasValue ? y.fee_amt.Value.ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            " + (y.order_status == 1 || y.order_status == 2 || y.order_status == 22 ? "<td>" + y.initial_sales_charges_percent + "</td>" : "") + @"
                                            <td>" + (y.created_by == y.user_id ? "Self" : y.consultantid) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed" : "-"))) + @"</td>
                                            " + (y.order_status == 1 || y.order_status == 12 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                      indexsub++;
                                      idx++;
                                  }
                                  else if (ordertype == "SELL")
                                  {
                                      decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                      totalAmtEst += UnitsValueMYR;
                                      decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                                      totalUnitsEst += UnitsValue;
                                      commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "To Be Confirmed" : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed" : "-"))) + @"</td>
                                            " + (y.order_status == 1 || y.order_status == 12 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                      indexsub++;
                                      idx++;
                                  }
                                  else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchOut || ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchIn)
                                  {
                                      decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td> Switch Sell </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'></td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "" + y.units.ToString("N4", new CultureInfo("en-US")) : "")) + @"</td>
                                            " + (y.order_status == 1 || y.order_status == 12 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");

                                      indexsub++;
                                      idx++;

                                      string UnitsInValue = (UnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name2.Capitalize() + @"</td>
                                            <td> Switch Buy </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.app_fee.HasValue ? Math.Round((y.amount * y.app_fee.Value) / 100, 2).ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "To Be Confirmed" : "")) + @"</td>
                                            
                                        </tr>");
                                      idx++;

                                  }
                                  i++;
                              });

                              if (ordertype == "BUY" || ordertype == "RSP")
                              {
                                  if (x.order_status == 3)
                                  {
                                      totalAmt = UserOrderMOList.Sum(y => y.trans_amt.HasValue ? y.trans_amt.Value : 0).ToString("N2", new CultureInfo("en-US"));
                                      totalUnits = UserOrderMOList.Sum(y => y.trans_units.HasValue ? y.trans_units.Value : 0).ToString("N4", new CultureInfo("en-US"));
                                  }
                                  else
                                  {
                                      totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                                      totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                                  }
                                  tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Payment Method</th>
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 22 ? "<th>Sales Charge (%)</th>" : "") + @"                                                            
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th>Creator</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                            " + (x.order_status == 1 || x.order_status == 12 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 3 || UserOrderMOList.FirstOrDefault().order_status == 22 ? @"<td></td>" : "") + @"
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "(Est.) " + totalUnitsEst.ToString("N4", new CultureInfo("en-US")))) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>

                                    </tr>");
                              }
                              else if (ordertype == "SELL")
                              {
                                  tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                            " + (x.order_status == 1 || x.order_status == 12 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            
                                                            <td></td>
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "To Be Confirmed")) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                              }
                              else if (ordertype == "SWITCH")
                              {
                                  tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Type</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                            " + (x.order_status == 1 || x.order_status == 12 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <!--<tr>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                            <td class='text-right'>Total (" + (ordertype == "BUY" || ordertype == "RSP" ? "MYR" : (ordertype == "SELL" || ordertype == "SWITCH" ? "Units" : "")) + @")</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? "To Be Confirmed" : "")) + @"</td>
                                                            <td></td>
                                                        </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                              }

                              index++;
                          }
                          else
                          {
                              ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMO.Message + "\", '');", true);
                          }
                      });

                    UserOrderList.Where(x => x.order_status == 2 || x.order_status == 22).OrderByDescending(x => x.created_date).ToList().ForEach(x =>
                      {
                          Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, uo.consultantid, ua.account_no, uo.order_no, uo.order_type, uo.created_by, uo.created_date, uo.amount, uo.units, 
                            ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT, 
                            uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, uo.trans_amt, 
                            uo.trans_units, uo.app_fee, uo.exit_fee, uo.fee_amt, uo.net_amt, uo.trans_pr FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID
                            where uo.order_no='" + x.order_no + @"' and uo.order_type in (1,2,3) order by uo.created_date desc ", 0, 0, false, null, false, null, true);

                          if (responseMO.IsSuccess)
                          {
                              var rawDynMOData = responseMO.Data;
                              var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                              List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                              string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                              string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                              decimal totalAmtEst = 0;
                              decimal totalUnitsEst = 0;

                              string ordertype = "";
                              if (x.order_type == (int)OrderType.Buy)
                                  ordertype = "BUY";
                              else if (x.order_type == (int)OrderType.Sell)
                                  ordertype = "SELL";
                              else if (x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut)
                                  ordertype = "SWITCH";
                              else if (x.order_type == (int)OrderType.RSP)
                                  ordertype = "RSP";

                              string status = "";
                              if (x.order_status == 1)
                              {
                                  status = "Pending";// Order Placed but not yet proceed to FPX/ not yet verified by user
                              }
                              else if (x.order_status == 2 && (x.order_type == (int)OrderType.RSP))
                              {
                                  status = "Processing";//Pending: FPX verified
                              }
                              else if (x.order_status == 2 && (x.order_type == (int)OrderType.Sell || x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut))
                                  status = "Processing";// Pending for order approval
                              else if (x.order_status == 18)
                                  status = "Order cancelled";
                              else if (x.order_status == 22 && x.payment_method == "1")
                                  status = "Processing";//Pending: for FPX Payment
                              else if (x.order_status == 22 && x.payment_method == "2")
                                  status = "Processing";//Pending: For payment approval
                              else if (x.order_status == 29 && x.payment_method == "1")
                                  status = "Payment failed";
                              else if (x.order_status == 29 && x.payment_method == "2")
                                  status = "Payment rejected";
                              else if (x.order_status == 3)
                                  status = "Completed";
                              else if (x.order_status == 39)
                                  status = "Order rejected";
                              else if (x.order_status == 2)
                                  status = "Processing";//pending: Payment success

                              tableBuilder.Append(@"<tr>
                                                <td><a href='javascript:;' class='show-extended-row'><i class='fa fa-plus'></i></a></td>
                                                <td>" + index + @"</td>
                                                <td>" + x.account_no + @"</td>
                                                <td>" + x.order_no + @"</td>
                                                <td>" + ordertype + @"</td>
                                                <td>" + ((status == "Pending") ? x.created_date.ToString("dd/MM/yyyy HH:mm:ss") : ((status == "Processing") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : ((status == "Completed" || status == "Payment failed" || status == "Order rejected") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : "-"))) + @"</td>
                                                <td>" + status + @"</td>
                                                <td class='text-right'>" + (ordertype == "BUY" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                            </tr>");
                              StringBuilder mos = new StringBuilder();
                              string actionsHTML = "-";

                              if (x.order_status == 1 && !IsImpersonated)
                              {
                                  actionsHTML = "";
                                  actionsHTML += "<div class='btn-group'>";
                                  actionsHTML += "<a class='btn btn-info btn-sm mb-5' href='Show-Cart.aspx?UON=" + x.order_no + "'>" + (x.order_type == 1 ? "Pay" : (x.order_type == 2 || x.order_type == 3 ? "Request" : (x.order_type == 6 ? "Verify" : "-"))) + "</a>";
                                  actionsHTML += "<a class='btn btn-default btn-sm cancel-order mb-5' data-orderno='" + x.order_no + "' href='javascript:;'>Cancel</a>";
                                  actionsHTML += "</div>";
                              }
                              string commonActionRow = "<td rowspan='" + (x.order_type == 1 || x.order_type == 2 ? UserOrderMOList.Count : (UserOrderMOList.Count * 2)) + @"' class='text-center'>" + actionsHTML + @"</td>";
                              int indexsub = 0;
                              Decimal totalUnitsValueMYR = 0;
                              string totalUnitsInValue = "";
                              int idx = 0;

                              UserOrderMOList.ForEach(y =>
                              {
                                  string transno = y.trans_no == "" ? "-" : y.trans_no;
                                  OrderListDTO uo = y;
                                  int i = 0;

                                  string url = "";
                                  Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.order_no, true, 0, 0, true);
                                  if (responseUOFList.IsSuccess)
                                  {
                                      UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                      if (uof != null)
                                          url = uof.Url;
                                      else
                                          url = "FPX PAYMENT";
                                  }
                                  if (ordertype == "BUY" || ordertype == "RSP")
                                  {
                                      decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                      totalAmtEst += UnitsValueMYR;
                                      decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                                      totalUnitsEst += UnitsValue;
                                      commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td>" + (uo.payment_method == "1" ? "FPX PAYMENT (" + uo.fpx_bank_code + @")" : (uo.payment_method == "2" ? "Proof of Payment <a href=\"" + url + "\" target='_blank'>view</a>" : "-")) + @"</td>
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.fee_amt.HasValue ? y.fee_amt.Value.ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            " + (y.order_status == 1 || y.order_status == 2 || y.order_status == 22 ? "<td>" + y.initial_sales_charges_percent + "</td>" : "") + @"
                                            <td>" + (y.created_by == y.user_id ? "Self" : y.consultantid) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                      indexsub++;
                                      idx++;
                                  }
                                  else if (ordertype == "SELL")
                                  {
                                      decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                      totalAmtEst += UnitsValueMYR;
                                      decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                                      totalUnitsEst += UnitsValue;
                                      commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "To Be Confirmed" : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                      indexsub++;
                                      idx++;
                                  }
                                  else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchOut || ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchIn)
                                  {
                                      decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td> Switch Sell </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'></td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "" + y.units.ToString("N4", new CultureInfo("en-US")) : "")) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");

                                      indexsub++;
                                      idx++;

                                      string UnitsInValue = (UnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                                      mos.Append(@"<tr>
                                            <td>" + y.Fund_Name2.Capitalize() + @"</td>
                                            <td> Switch Buy </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.app_fee.HasValue ? Math.Round((y.amount * y.app_fee.Value) / 100, 2).ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "To Be Confirmed" /*"(Est.) " + UnitsInValue*/ : "")) + @"</td>
                                        </tr>");
                                      idx++;

                                  }

                                  i++;
                              });

                              if (ordertype == "BUY" || ordertype == "RSP")
                              {
                                  if (x.order_status == 3)
                                  {
                                      totalAmt = UserOrderMOList.Sum(y => y.trans_amt.HasValue ? y.trans_amt.Value : 0).ToString("N2", new CultureInfo("en-US"));
                                      totalUnits = UserOrderMOList.Sum(y => y.trans_units.HasValue ? y.trans_units.Value : 0).ToString("N4", new CultureInfo("en-US"));
                                  }
                                  else
                                  {
                                      totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                                      totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                                  }
                                  tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Payment Method</th>
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 22 ? "<th>Sales Charge (%)</th>" : "") + @"                                                            
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th>Creator</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                            " + (x.order_status == 1 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 3 || UserOrderMOList.FirstOrDefault().order_status == 22 ? @"<td></td>" : "") + @"
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "(Est.) " + totalUnitsEst.ToString("N4", new CultureInfo("en-US")))) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "(Est.) " + totalAmtEst.ToString("N2", new CultureInfo("en-US")))) + @"</td>
                                                            
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                              }
                              else if (ordertype == "SELL")
                              {
                                  tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                            " + (x.order_status == 1 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            
                                                            <td></td>
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "To Be Confirmed")) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                              }
                              else if (ordertype == "SWITCH")
                              {
                                  tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Type</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                            " + (x.order_status == 1 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <!--<tr>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                            <td class='text-right'>Total (" + (ordertype == "BUY" || ordertype == "RSP" ? "MYR" : (ordertype == "SELL" || ordertype == "SWITCH" ? "Units" : "")) + @")</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? "To Be Confirmed" : "")) + @"</td>
                                                            <td></td>
                                                        </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                              }

                              index++;
                          }
                          else
                          {
                              ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMO.Message + "\", '');", true);
                          }
                      });

                    UserOrderList.Where(x => x.order_status == 3).ToList().ForEach(x =>
                    {
                        Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, uo.consultantid, ua.account_no, uo.order_no, uo.order_type, uo.created_by, uo.created_date, uo.amount, uo.units, 
                            ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT, 
                            uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, uo.trans_amt, 
                            uo.trans_units, uo.app_fee, uo.exit_fee, uo.fee_amt, uo.net_amt, uo.trans_pr FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID
                            where uo.order_no='" + x.order_no + @"' and uo.order_type in (1,2,3,4) order by uo.created_date desc ", 0, 0, false, null, false, null, true);

                        if (responseMO.IsSuccess)
                        {
                            var rawDynMOData = responseMO.Data;
                            var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                            List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                            string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                            string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                            decimal totalAmtEst = 0;
                            decimal totalUnitsEst = 0;

                            string ordertype = "";
                            if (x.order_type == (int)OrderType.Buy)
                                ordertype = "BUY";
                            else if (x.order_type == (int)OrderType.Sell)
                                ordertype = "SELL";
                            else if (x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut)
                                ordertype = "SWITCH";
                            else if (x.order_type == (int)OrderType.RSP)
                                ordertype = "RSP";

                            string status = "";
                            if (x.order_status == 1)
                            {
                                status = "Pending";// Order Placed but not yet proceed to FPX/ not yet verified by user
                            }
                            else if (x.order_status == 2 && (x.order_type == (int)OrderType.RSP))
                            {
                                status = "Processing";//Pending: FPX verified
                            }
                            else if (x.order_status == 2 && (x.order_type == (int)OrderType.Sell || x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut))
                                status = "Processing";// Pending for order approval
                            else if (x.order_status == 18)
                                status = "Order cancelled";
                            else if (x.order_status == 22 && x.payment_method == "1")
                                status = "Processing";//Pending: for FPX Payment
                            else if (x.order_status == 22 && x.payment_method == "2")
                                status = "Processing";//Pending: For payment approval
                            else if (x.order_status == 29 && x.payment_method == "1")
                                status = "Payment failed";
                            else if (x.order_status == 29 && x.payment_method == "2")
                                status = "Payment rejected";
                            else if (x.order_status == 3)
                                status = "Completed";
                            else if (x.order_status == 39)
                                status = "Order rejected";
                            else if (x.order_status == 2)
                                status = "Processing";//pending: Payment success

                            tableBuilder.Append(@"<tr>
                                                <td><a href='javascript:;' class='show-extended-row'><i class='fa fa-plus'></i></a></td>
                                                <td>" + index + @"</td>
                                                <td>" + x.account_no + @"</td>
                                                <td>" + x.order_no + @"</td>
                                                <td>" + ordertype + @"</td>
                                                <td>" + ((status == "Pending") ? x.created_date.ToString("dd/MM/yyyy HH:mm:ss") : ((status == "Processing") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : ((status == "Completed" || status == "Payment failed" || status == "Order rejected") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : "-"))) + @"</td>
                                                <td>" + status + @"</td>
                                                <td class='text-right'>" + (ordertype == "BUY" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                            </tr>");
                            StringBuilder mos = new StringBuilder();
                            string actionsHTML = "-";

                            if (x.order_status == 1 && !IsImpersonated)
                            {
                                actionsHTML = "";
                                actionsHTML += "<div class='btn-group'>";
                                actionsHTML += "<a class='btn btn-info btn-sm mb-5' href='Show-Cart.aspx?UON=" + x.order_no + "'>" + (x.order_type == 1 ? "Pay" : (x.order_type == 2 || x.order_type == 3 ? "Request" : (x.order_type == 6 ? "Verify" : "-"))) + "</a>";
                                actionsHTML += "<a class='btn btn-default btn-sm cancel-order mb-5' data-orderno='" + x.order_no + "' href='javascript:;'>Cancel</a>";
                                actionsHTML += "</div>";
                            }
                            string commonActionRow = "<td rowspan='" + (x.order_type == 1 || x.order_type == 2 ? UserOrderMOList.Count : (UserOrderMOList.Count * 2)) + @"' class='text-center'>" + actionsHTML + @"</td>";
                            int indexsub = 0;
                            Decimal totalUnitsValueMYR = 0;


                            int idx = 0;
                            List<OrderListDTO> orderListDTOs4 = UserOrderMOList.Where(uom => uom.order_type == 4).ToList();
                            UserOrderMOList = UserOrderMOList.Where(uom => uom.order_type != 4).ToList();
                            UserOrderMOList.ForEach(y =>
                            {
                                string transno = y.trans_no == "" ? "-" : y.trans_no;
                                OrderListDTO uo = y;
                                int i = 0;

                                string url = "";
                                Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.order_no, true, 0, 0, true);
                                if (responseUOFList.IsSuccess)
                                {
                                    UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                    if (uof != null)
                                        url = uof.Url;
                                    else
                                        url = "FPX PAYMENT";
                                }
                                if (ordertype == "BUY" || ordertype == "RSP")
                                {
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                    totalAmtEst += UnitsValueMYR;
                                    decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                                    totalUnitsEst += UnitsValue;
                                    commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td>" + (uo.payment_method == "1" ? "FPX PAYMENT (" + uo.fpx_bank_code + @")" : (uo.payment_method == "2" ? "Proof of Payment <a href=\"" + url + "\" target='_blank'>view</a>" : "-")) + @"</td>
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.fee_amt.HasValue ? y.fee_amt.Value.ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            " + (y.order_status == 1 || y.order_status == 2 || y.order_status == 22 ? "<td>" + y.initial_sales_charges_percent + "</td>" : "") + @"
                                            <td>" + (y.created_by == y.user_id ? "Self" : y.consultantid) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SELL")
                                {
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                    totalAmtEst += UnitsValueMYR;
                                    decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                                    totalUnitsEst += UnitsValue;
                                    commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "To Be Confirmed" /*"(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US"))*/ : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchOut || ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchIn)
                                {
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td> Switch Sell </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'></td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "" + y.units.ToString("N4", new CultureInfo("en-US")) : "")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "" + y.amount.ToString("N4", new CultureInfo("en-US")) : "")) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");

                                    indexsub++;
                                    idx++;

                                    OrderListDTO orderListDTO4 = orderListDTOs4.Where(old => old.Fund_Name == y.Fund_Name && old.Fund_Name2 == y.Fund_Name2 && old.units == 0).First();
                                    string transno2 = orderListDTO4.trans_no == "" ? "-" : orderListDTO4.trans_no;

                                    string UnitsInValue = (UnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name2.Capitalize() + @"</td>
                                            <td> Switch Buy </td>
                                            
                                            <td>" + transno2 + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (orderListDTO4.fee_amt.HasValue ? Math.Round(orderListDTO4.fee_amt.Value, 2).ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && orderListDTO4.trans_units.HasValue ? orderListDTO4.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "To Be Confirmed" /*"(Est.) " + UnitsInValue*/ : "")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && orderListDTO4.trans_amt.HasValue ? orderListDTO4.trans_amt.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "To Be Confirmed" /*"(Est.) " + UnitsInValue*/ : "")) + @"</td>
                                        </tr>");
                                    idx++;

                                }
                                i++;
                            });

                            if (ordertype == "BUY" || ordertype == "RSP")
                            {
                                if (x.order_status == 3)
                                {
                                    totalAmt = UserOrderMOList.Sum(y => y.trans_amt.HasValue ? y.trans_amt.Value : 0).ToString("N2", new CultureInfo("en-US"));
                                    totalUnits = UserOrderMOList.Sum(y => y.trans_units.HasValue ? y.trans_units.Value : 0).ToString("N4", new CultureInfo("en-US"));
                                }
                                else
                                {
                                    totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                                    totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                                }
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Payment Method</th>
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 22 ? "<th>Sales Charge (%)</th>" : "") + @"                                                            
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th>Creator</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                            " + (x.order_status == 1 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 3 || UserOrderMOList.FirstOrDefault().order_status == 22 ? @"<td></td>" : "") + @"
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "To Be Confirmed")) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                            }
                            else if (ordertype == "SELL")
                            {
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                            " + (x.order_status == 1 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            
                                                            <td></td>
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "To Be Confirmed")) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                            }
                            else if (ordertype == "SWITCH")
                            {
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Type</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount</th>
                                                            " + (x.order_status == 1 ? "<th width='150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <!--<tr>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                            <td class='text-right'>Total (" + (ordertype == "BUY" || ordertype == "RSP" ? "MYR" : (ordertype == "SELL" || ordertype == "SWITCH" ? "Units" : "")) + @")</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? "To Be Confirmed" : "")) + @"</td>
                                                            <td></td>
                            <td></td>
                                                        </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                            }

                            index++;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMO.Message + "\", '');", true);
                        }
                    });

                    if (UserOrderList.Count == 0)
                    {
                        tableBuilder.Append(@"<tr>
                                                <td colspan='8' class='text-center'>No data available in table</td>
                                            </tr>");
                    }
                    tbodyUserOrders.InnerHtml = tableBuilder.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUserOrderList.Message + "\", '');", true);
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindOrderList();
        }
        protected void CancelOrder_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            string orderNo = hdnCancelOrderNo.Value;
            Response responseUOList = IUserOrderService.GetDataByFilter(" order_no = '" + orderNo + "' and user_id=" + user.Id + " ", 0, 0, false);
            if (responseUOList.IsSuccess)
            {
                List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;
                List<UserOrder> multipleOrders = new List<UserOrder>();

                userOrders.ForEach(x =>
                {
                    Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                    if (response.IsSuccess)
                    {
                        List<UserOrder> ol = (List<UserOrder>)response.Data;
                        if (ol.Count > 0)
                        {
                            multipleOrders.AddRange(ol);
                        }
                    }
                });

                multipleOrders.ForEach(x =>
                {
                    x.OrderStatus = 18;
                });
                IUserOrderService.UpdateBulkData(multipleOrders);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Order no: " + orderNo + " cancelled.', 'OrderList.aspx');", true);
            }
            // terminated RSP fund

        }

        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

    }
}