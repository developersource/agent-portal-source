﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="CompleteSignup.aspx.cs" Inherits="DiOTP.AgentApp.CompleteSignup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css" />
    <!-- Latest version of minified stylesheet for Accordion Wizard -->
    <link rel="stylesheet" href="http://sathomas.me/acc-wizard/css/acc-wizard.min.css" />
    <style>
        .field-required {
            border-bottom: 1px solid #ff1b1b !important;
        }

        .panel-heading {
            background-color: #059cce !important;
            background-image: none !important;
        }

            .panel-heading > .panel-title {
                color: #fff;
            }

        .sticky {
            position: fixed;
            top: 90px;
            margin-left: -40px;
        }

            .sticky + .content {
                padding-top: 60px;
            }

        ol.acc-wizard-sidebar li.acc-wizard-completed:after {
            display: inline-block;
            vertical-align: middle;
            font-size: 15px;
            width: 1.875em;
            content: "\00a0\2713";
            position: relative;
            bottom: .1em;
            color: #282828;
        }

        input[type="checkbox"] {
            margin-right: 10px;
            margin-left: 10px;
            margin-top: 10px;
        }

            input[type="checkbox"] + label {
                margin-right: 20px;
            }
    </style>

    <style>
        input[type="radio"], input[type="checkbox"] {
            margin-right: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <asp:HiddenField ID="hdnViewOnly" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnOpenAgentId" runat="server" ClientIDMode="Static" />
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Dashboard.aspx">Dashboard</a></li>
                    <li class="active">Register</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Register</h3>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row acc-wizard">
                        <div class="col-md-2">
                            <div id="sticky-div">
                                <h5>Follow the steps below to Open</h5>
                                <ol class="acc-wizard-sidebar">
                                    <li class="acc-wizard-todo"><a href="#agency">Registration Details</a></li>
                                    <li class="acc-wizard-todo"><a href="#personal">Personal</a></li>
                                    <li class="acc-wizard-todo"><a href="#payment">Credit Banks/ Insurance</a></li>
                                    <%--<li class="acc-wizard-todo"><a href="#exam">Examination</a></li>--%>
                                    <%--<li class="acc-wizard-todo"><a href="#fimm">FIMM/ Renewal</a></li>--%>
                                    <li class="acc-wizard-todo"><a href="#work">Work Experience</a></li>
                                    <li class="acc-wizard-todo"><a href="#docs">Documents</a></li>
                                    <li class="acc-wizard-todo"><a href="#confirm">Confirmation</a></li>
                                    <li class="acc-wizard-todo"><a href="#recommend">Recommendation</a></li>
                                </ol>
                                <a class="text-primary ml-40" href="/AgentSignups.aspx"><i class="fa fa-arrow-circle-left mt-20 mr-10"></i><span>Back</span></a>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div id="accordion-demo" class="panel-group">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#agency" data-parent="#accordion-demo" data-toggle="collapse">Registration Details
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="agency" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="form-agency" class="form-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="label">Did you pass CUTE?</label>
                                                        <div class="row checkbox-group-1">
                                                            <div class="col-md-3">
                                                                <asp:CheckBox ID="chkIsCUTEPassedYes" runat="server" ClientIDMode="Static" Text=" Yes" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:CheckBox ID="chkIsCUTEPassedNo" runat="server" ClientIDMode="Static" Text=" No" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Do you have introducer?</label>
                                                        <div class="row checkbox-group-2">
                                                            <div class="col-md-3">
                                                                <asp:CheckBox ID="chkRecommendedAgentYes" runat="server" ClientIDMode="Static" Text=" Yes" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:CheckBox ID="chkRecommendedAgentNo" runat="server" ClientIDMode="Static" Text=" No" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide" id="IndroducerDiv">
                                                        <label class="label">Introducer Code *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtIntroCode" runat="server" CssClass="form-control" placeholder="Enter Introducer Code" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtIntroCode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Agent Code *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtAgentCode" runat="server" CssClass="form-control" placeholder="Enter Agent Code" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtAgentCode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Rank *</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlRank" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">R01</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlRank" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                            <%--<asp:TextBox ID="txtRank" runat="server" CssClass="form-control" placeholder="Enter Rank"></asp:TextBox>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Name *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Enter Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Registration Type *</label>
                                                        <div class="input-group">
                                                            <asp:CheckBox ID="chkRegTypeUTS" runat="server" ClientIDMode="Static" Text="UTS"></asp:CheckBox>
                                                            <asp:CheckBox ID="chkRegTypePRS" runat="server" ClientIDMode="Static" Text="PRS"></asp:CheckBox>
                                                            <div id="err-chkRegTypeUTS" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Job Grade</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtJobGrade" runat="server" CssClass="form-control" placeholder="Enter Job Grade" ClientIDMode="Static"></asp:TextBox>
                                                            <!-- Applicable for Staff Agents of the UTMC. If we choose Agent Rank whose Agent Type is STAFF Agent, job grade is mandatory. -->
                                                            <div id="err-txtJobGrade" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Rollout Number *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtRolloutNo" runat="server" CssClass="form-control" placeholder="Enter Rollout Number" ClientIDMode="Static"></asp:TextBox>
                                                            <!-- Employee Number is the Roll out number of Staff Agent -->
                                                            <div id="err-txtRolloutNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Job Type *</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlJobType" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Full Time</asp:ListItem>
                                                                <asp:ListItem Value="1">Part Time</asp:ListItem>
                                                                <asp:ListItem Value="1">External</asp:ListItem>
                                                                <asp:ListItem Value="1">Unknown</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlJobType" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Old Agent Code *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtOldAgentCode" runat="server" CssClass="form-control" placeholder="Enter Old Agent Code" ClientIDMode="Static"></asp:TextBox>
                                                            <!-- Agent code being used before ISS is implemented. -->
                                                            <div id="err-txtOldAgentCode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Industry Group</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlIndustryGroup" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">IG1</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlIndustryGroup" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Recruiter *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtRecruiter" runat="server" CssClass="form-control" placeholder="Enter Recruiter" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtRecruiter" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Supervisor *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtSupervisor" runat="server" CssClass="form-control" placeholder="Enter Supervisor" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtSupervisor" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Ex Upline Agent Code</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtExUplineCode" runat="server" CssClass="form-control" placeholder="Enter Ex Upline Agent Code" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtExUplineCode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Agent Office/Branch</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlOffice" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                            </asp:DropDownList>
                                                            <div id="err-ddlOffice" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Region</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlRegion" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">R1</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlRegion" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.panel-body -->
                                    </div>
                                    <!-- /#prerequisites -->
                                </div>
                                <!-- /.panel.panel-default -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#personal" data-parent="#accordion-demo" data-toggle="collapse">Personal
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="personal" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="form-personal" class="form-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="label">Agent Name *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtAgentName" runat="server" CssClass="form-control" placeholder="Enter Agent Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtAgentName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide">
                                                        <label class="label">Mnemonic *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtMenmonic" runat="server" CssClass="form-control" placeholder="Enter Mnemonic" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtMenmonic" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">NRIC/Passport *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control" placeholder="Enter NRIC/Passport" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtIDNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Date of Birth *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control datepicker-text" placeholder="Select Date of Birth" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtDOB" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <%--<label class="label">Father Name *</label>--%>
                                                        <label class="label">Mother Maiden Name *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtMotherMaidenName" runat="server" CssClass="form-control" placeholder="Enter Mother Maiden Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtMotherMaidenName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Sex</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlSex" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlSex" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Race *</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlRace" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Malay</asp:ListItem>
                                                                <asp:ListItem Value="2">Chinese</asp:ListItem>
                                                                <asp:ListItem Value="3">Indian</asp:ListItem>
                                                                <asp:ListItem Value="4">Others</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlRace" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide" id="race-others">
                                                        <label class="label">Other race*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtRaceDesc" runat="server" CssClass="form-control" placeholder="Enter your race" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtRaceDesc" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Religion *</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlReligion" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Muslim</asp:ListItem>
                                                                <asp:ListItem Value="2">Buddhist</asp:ListItem>
                                                                <asp:ListItem Value="3">Christian</asp:ListItem>
                                                                <asp:ListItem Value="4">Others</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlReligion" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide" id="religion-others">
                                                        <label class="label">Other Religion*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtReligionDesc" runat="server" CssClass="form-control" placeholder="Enter your race" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtReligionDesc" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Highest Education *</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlEducation" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">SPM</asp:ListItem>
                                                                <asp:ListItem Value="2">STPM</asp:ListItem>
                                                                <asp:ListItem Value="3">Diploma</asp:ListItem>
                                                                <asp:ListItem Value="4">Degree</asp:ListItem>
                                                                <asp:ListItem Value="5">Post-Graduate</asp:ListItem>
                                                                <asp:ListItem Value="6">Others</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlHighestEducation" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide" id="education-others">
                                                        <label class="label">Other Education*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtEducationDesc" runat="server" CssClass="form-control" placeholder="Enter your highest education" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtEducationDesc" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Years Of Experience*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtYearsOfExperience" runat="server" CssClass="form-control" placeholder="Enter your years of experience" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtYearsOfExperience" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h5>Mailing Address</h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="label">Address Line 1*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtAddrLine1" runat="server" CssClass="form-control" placeholder="Enter Address Line 1" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtAddrLine1" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="label">Address Line 2*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtAddrLine2" runat="server" CssClass="form-control" placeholder="Enter Address Line 2" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtAddrLine2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="label">Address Line 3*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtAddrLine3" runat="server" CssClass="form-control" placeholder="Enter Address Line 3" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtAddrLine3" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="label">Address Line 4*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtAddrLine4" runat="server" CssClass="form-control" placeholder="Enter Address Line 4" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtAddrLine4" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Post Code *</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPostCode" runat="server" CssClass="form-control" placeholder="Enter Post Code" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtPostCode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">State</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlState" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">Kuala Lumpur</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlState" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Country</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlCountry" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">Malaysia</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlCountry" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <label class="label">City</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeholder="Enter City"></asp:TextBox>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-md-4">
                                                        <label class="label">Telephone No. (Office)</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtTelNoOffice" runat="server" CssClass="form-control" placeholder="Enter Telephone No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtTelNoOffice" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Telephone No. (Home)</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtTelNoHome" runat="server" CssClass="form-control" placeholder="Enter Telephone No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtTelNoHome" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Handphone No.</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtHandPhoneNo" runat="server" CssClass="form-control" placeholder="Enter Handphone No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtHandPhoneNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <label class="label">Fax No.</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtFaxNo" runat="server" CssClass="form-control" placeholder="Enter Fax No."></asp:TextBox>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-md-4">
                                                        <label class="label">Email</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Enter Email" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtEmail" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <label class="label">Email 2</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtEmail2" runat="server" CssClass="form-control" placeholder="Enter Email"></asp:TextBox>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-md-4">
                                                        <label class="label">Contact Person </label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtContactPerson" runat="server" CssClass="form-control" placeholder="Enter Contact Person" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtContactPerson" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <label class="label">Contact Person 2</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtContactPerson2" runat="server" CssClass="form-control" placeholder="Enter Contact Person"></asp:TextBox>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-md-12">
                                                        <hr />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Income Tax No.</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtIncomeTaxNo" runat="server" CssClass="form-control" placeholder="Enter Income Tax No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtIncomeTaxNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">EPF No.</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtEPFNo" runat="server" CssClass="form-control" placeholder="Enter EPF No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtEPFNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">SOCSO</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtSOCSO" runat="server" CssClass="form-control" placeholder="Enter SOCSO" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtSOCSO" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Language</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlLanguage" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">English</asp:ListItem>
                                                                <asp:ListItem Value="2">Malay</asp:ListItem>
                                                                <asp:ListItem Value="3">Chinese</asp:ListItem>
                                                                <asp:ListItem Value="4">Tamil</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlLanguage" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Nationality</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlNationality" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Malaysian</asp:ListItem>
                                                                <asp:ListItem Value="1">Chinese</asp:ListItem>
                                                                <asp:ListItem Value="1">Indian</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlNationality" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Marital Status</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlMaritalStatus" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="Single">Single</asp:ListItem>
                                                                <asp:ListItem Value="Married">Married</asp:ListItem>
                                                                <asp:ListItem Value="Divorced">Divorced</asp:ListItem>
                                                                <asp:ListItem Value="Others">Others</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlMaritalStatus" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide" id="marital-others">
                                                        <label class="label">Other Marital Status*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtMaritalDesc" runat="server" CssClass="form-control" placeholder="Enter your marital status" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtMaritalDesc" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 checkbox-group-1">
                                                        <label class="label">Bankruptcy Declaration</label>
                                                        <div class="input-group">
                                                            <asp:CheckBox ID="chkBankruptcyYes" runat="server" ClientIDMode="Static" Text="Yes"></asp:CheckBox>
                                                            <asp:CheckBox ID="chkBankruptcyNo" runat="server" ClientIDMode="Static" Text="No"></asp:CheckBox>
                                                            <div id="err-chkBankruptcy" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hide" id="bankruptcy-desc">
                                                        <label class="label">Bankruptcy Declaration Text*</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBankruptcyDesc" runat="server" CssClass="form-control" placeholder="Enter Bankruptcy Declaration" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-BankruptcyDesc" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Spouse Name</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtSpouseName" runat="server" CssClass="form-control" placeholder="Enter Spouse Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtSpouseName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Spouse NRIC/Passport</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtSpouseIDNo" runat="server" CssClass="form-control" placeholder="Spouse NRIC/Passport" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtSpouseIDNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <label class="label">Additional Info</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="TextBox37" TextMode="MultiLine" runat="server" CssClass="form-control" placeholder="Enter Additional Info"></asp:TextBox>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.panel-body -->
                                    </div>
                                    <!-- /#addwizard -->
                                </div>
                                <!-- /.panel.panel-default -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#payment" data-parent="#accordion-demo" data-toggle="collapse">Credit Banks/Insurance
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="payment" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="form-payment" class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Credit Banks</h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>Domestic</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Currency</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlCurrency" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="MYR" Selected="True">MYR</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlCurrency" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Type of Account</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlBankAccountType" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="Savings">Savings</asp:ListItem>
                                                                <asp:ListItem Value="Current">Current</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlBankAccountType" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Bank Name</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control" placeholder="Enter Bank Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtBankName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Account No.</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBankAccNo" runat="server" CssClass="form-control" placeholder="Enter Account No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtBankAccNo" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Payment Mode</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlPaymentMode" runat="server" ClientIDMode="Static" CssClass="form-control" Enabled="false">
                                                                <asp:ListItem Value="Direct Credit" Selected="True">Direct Credit</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlPaymentMode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>International</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Currency</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlCurrencyI" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="SGD">SGD</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlCurrencyI" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">IC/Passport</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPaymentICI" runat="server" CssClass="form-control" placeholder="Enter Passport No" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtPaymentICI" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Type of Account</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlBankAccountTypeI" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="Savings">Savings</asp:ListItem>
                                                                <asp:ListItem Value="Current">Current</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlBankAccountTypeI" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Bank Name</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBankNameI" runat="server" CssClass="form-control" placeholder="Enter Bank Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtBankNameI" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Account No.</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBankAccNoI" runat="server" CssClass="form-control" placeholder="Enter Account No." ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtBankAccNoI" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Payment Mode</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlPaymentModeI" runat="server" ClientIDMode="Static" CssClass="form-control" Enabled="false">
                                                                <asp:ListItem Value="Direct Credit" Selected="True">Direct Credit</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlPaymentModeI" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h5>Insurance</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Beneficiary Name</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBeneficiaryName" runat="server" CssClass="form-control" placeholder="Enter Beneficiary Name" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtBeneficiaryName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Beneficiary NRIC/Passport</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBeneficiaryNRIC" runat="server" CssClass="form-control" placeholder="Enter Beneficiary NRIC/Passport" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtBeneficiaryNRIC" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-12">
                                                        <h5>Monthly Fixed income</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Office Subsidy Commission</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtOfficeSubsidyComm" runat="server" CssClass="form-control" placeholder="Enter Office Subsidy Commission" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtOfficeSubsidyComm" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Amount</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtOfficeSubsidyAmount" runat="server" CssClass="form-control" placeholder="Enter Amount" ClientIDMode="Static"></asp:TextBox>
                                                            <div id="err-txtOfficeSubsidyAmount" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.panel-body -->
                                    </div>
                                    <!-- /#adjusthtml -->
                                </div>
                                <!-- /.panel.panel-default -->


                                <!-- /.panel.panel-default -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#work" data-parent="#accordion-demo" data-toggle="collapse">Work Experience
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="work" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="form-work" class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Previous Employer</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Employer Name</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPreviousEmployerName" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Previous Employer Name"></asp:TextBox>
                                                            <div id="err-txtPreviousEmployerName" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Position Held</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPositionHeld" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Position Held"></asp:TextBox>
                                                            <div id="err-txtPositionHeld" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Length of Service (Years)</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtLengthOfService" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Length of Service"></asp:TextBox>
                                                            <div id="err-txtLengthOfService" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Annual Income</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlAnnualIncome" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="< MYR 5,000">< MYR 5,000</asp:ListItem>
                                                                <asp:ListItem Value="MYR 5,000 to MYR 10,000">MYR 5,000 to MYR 10,000</asp:ListItem>
                                                                <asp:ListItem Value="> MYR 10,000">> MYR 10,000</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlAnnualIncome" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Reason for Leaving</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtReasonEmp" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Reason for Leaving"></asp:TextBox>
                                                            <div id="err-txtReasonEmp" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--<div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Previous Employer - Second</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Previous Employer Name</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPreviousEmployerName2" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter Previous Employer Name"></asp:TextBox>
                                                            <div id="err-txtPreviousEmployerName2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Position Held</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPositionHeld2" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Position Held"></asp:TextBox>
                                                            <div id="err-txtPositionHeld2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Length of Service (Years)</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtLengthOfService2" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Length of Service"></asp:TextBox>
                                                            <div id="err-txtLengthOfService2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Annual Income</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlAnnualIncome2" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="< MYR 5,000">< MYR 5,000</asp:ListItem>
                                                                <asp:ListItem Value="MYR 5,000 to MYR 10,000">MYR 5,000 to MYR 10,000</asp:ListItem>
                                                                <asp:ListItem Value="> MYR 10,000">> MYR 10,000</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlAnnualIncome2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <label class="label">Annual Income</label>
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="DropDownList1" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="< MYR 5,000">< MYR 5,000</asp:ListItem>
                                                                <asp:ListItem Value="MYR 5,000 to MYR 10,000">MYR 5,000 to MYR 10,000</asp:ListItem>
                                                                <asp:ListItem Value="> MYR 10,000">> MYR 10,000</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="err-ddlAnnualIncome2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="label">Reason for Leaving</label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtReasonEmp2" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Reason for Leaving"></asp:TextBox>
                                                            <div id="err-txtReasonEmp2" class="" style="width: 100%; height: 20px; float: left;"></div>
                                                        </div>
                                                    </div>--%>
                                            </div>
                                        </div>
                                        <!--/.panel-body -->
                                    </div>
                                    <!-- /#adjusthtml -->
                                </div>
                                <!-- /.panel.panel-default -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#docs" data-parent="#accordion-demo" data-toggle="collapse">Documents
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="docs" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <style>
                                                label.file-upload {
                                                    display: block;
                                                    border: 1px solid #d3d3d3;
                                                    padding: 15px;
                                                }

                                                    label.file-upload span:not(.account-type) {
                                                        display: block;
                                                        height: 250px;
                                                        overflow-y: scroll;
                                                    }

                                                    label.file-upload i.fa-file-picture-o {
                                                        font-size: 100px;
                                                        line-height: 200px;
                                                        opacity: 0.5;
                                                    }

                                                #signature-div span.sign {
                                                    display: block;
                                                    border: 1px solid #d3d3d3;
                                                    padding: 15px;
                                                }

                                                #signature-div label {
                                                    border: none;
                                                    padding: 0;
                                                }

                                                .multi {
                                                    width: 100px;
                                                    display: inline-block;
                                                }

                                                label.file-upload .multi i {
                                                    font-size: 100px;
                                                    line-height: 100px;
                                                }

                                                label.file-upload span::-webkit-scrollbar {
                                                    width: 3px;
                                                }

                                                #webcam .btn-info {
                                                    position: absolute;
                                                    z-index: 9;
                                                    margin-left: 0;
                                                    margin-top: 10px;
                                                }

                                                    #webcam .btn-info#try-snap-again {
                                                        position: absolute;
                                                        z-index: 9;
                                                        margin-left: 0;
                                                        margin-top: 10px;
                                                        bottom: 10px;
                                                        left: calc(50% - 34.0025px);
                                                    }

                                                button#snap {
                                                    left: calc(50% - 34.0025px);
                                                    /*bottom: 0;*/
                                                    bottom: 75px;
                                                }

                                                #upload-row [class*='col-'] {
                                                    margin-bottom: 15px;
                                                }

                                                .w-97 {
                                                    width: 97%;
                                                }

                                                #webcam-pic {
                                                    height: 254px;
                                                    overflow-y: scroll;
                                                    border: 1px solid #d3d3d3;
                                                    padding: 15px;
                                                }

                                                #video {
                                                    height: 350px;
                                                    overflow-y: scroll;
                                                    border: 1px solid #d3d3d3;
                                                    padding: 15px;
                                                }

                                                .account-type {
                                                    display: inline-block !important;
                                                }
                                            </style>
                                            <div id="form-docs" class="form-body">
                                                <div class="row mb-20">
                                                    <div class="col-md-12">
                                                        <label for="uploadNRICFrontPage" class="file-upload">
                                                            Upload Copy of Applicant's NRIC/Passport (Front):
                                                            <br />
                                                            <small>(click here to upload)</small><br />
                                                            <small>Note: jpeg/png/pdf</small>
                                                            <span style="height: 180px;" id="uploadNRICFrontPageImage" runat="server" clientidmode="static">
                                                                <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                                            </span>
                                                            <p style="height: 180px;" id="uploadNRICFrontPageImageLink" runat="server" clientidmode="static"></p>
                                                        </label>
                                                        <input type="file" name="uploadNRIC" id="uploadNRICFrontPage" class="form-control hide" accept="image/*,application/pdf" />

                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="uploadNRICBackPage" class="file-upload">
                                                            Upload Copy of Applicant's NRIC (Back):
                                                            <br />
                                                            <small>(click here to upload)</small><br />
                                                            <small>Note: jpeg/png/pdf</small>
                                                            <span style="height: 180px;" id="uploadNRICBackPageImage" runat="server" clientidmode="static">
                                                                <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                                            </span>
                                                            <p style="height: 180px;" id="uploadNRICBackPageImageLink" runat="server" clientidmode="static"></p>
                                                        </label>
                                                        <input type="file" name="uploadNRIC" id="uploadNRICBackPage" class="form-control hide" accept="image/*,application/pdf" />
                                                    </div>
                                                    <div class="col-md-12 hide" id="existing-selfie">
                                                        <label for="uploadSelfie" class="file-upload">
                                                            Upload <span id="selfisAccountType" runat="server" class="account-type" style="height: 20px !important">Principal</span> Applicant's selfie with NRIC/Passport (Front):
                                                            <br />
                                                            <small>(click here to snap)</small>
                                                            <br />
                                                            <small>Note: jpeg/png/pdf</small>
                                                            <span id="uploadSelfieImage" runat="server" clientidmode="static">
                                                                <i class="fa fa-file-image-o"></i>
                                                            </span>
                                                            <p style="height: 180px;" id="uploadSelfieImageLink" runat="server" clientidmode="static"></p>
                                                        </label>
                                                        <input type="hidden" name="uploadSelfie" id="uploadSelfie" class="form-control" />
                                                        <input type="hidden" name="uploadSelfieFileName" id="uploadSelfieFileName" class="form-control" />
                                                        <input type="hidden" name="noCam" id="noCam" />
                                                    </div>
                                                    <div class="col-md-12 text-center mb-20" id="webcam">
                                                        <button type="button" id="try-snap-again" class="btn btn-info hide">Try Snap again</button>
                                                        <label>
                                                            Upload
                                                            <span id="selfis2AccountType" runat="server" class="account-type" style="height: 20px !important">Principal</span>
                                                            Applicant's selfie with NRIC/Passport (Front):</label>
                                                        <div class="position-relative text-center hide" id="webcam-pic" style="position: relative;">
                                                            <canvas id="canvas" width="640" height="480" style="position: absolute; width: 100%; max-width: 800px;"></canvas>
                                                            <img id="inputImg" src="" style="max-width: 800px; width: 100%;" />
                                                        </div>
                                                        <div class="row" id="cam-option">
                                                            <div class="col-md-12">
                                                                <video id="video" width="640" height="480" autoplay></video>
                                                                <div class="btn-group">
                                                                    <button type="button" id="snap" class="btn btn-info">Snap Photo</button>
                                                                </div>
                                                                <p style="margin-bottom: 15px;">(or)</p>
                                                            </div>
                                                        </div>
                                                        <div class="row" id="upload-selfie-option">
                                                            <div class="col-md-6 col-md-offset-3 text-center">
                                                                <input type="file" name="uploadSelfieFile" id="uploadSelfieFile" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="uploadPOP" class="file-upload">
                                                            Upload Proof of Payent:
                                                            <br />
                                                            <small>(click here to upload)</small><br />
                                                            <small>Note: jpeg/png/pdf</small>
                                                            <span style="height: 180px;" id="uploadPOPImage" runat="server" clientidmode="static">
                                                                <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                                            </span>
                                                            <p style="height: 180px;" id="uploadPOPImageLink" runat="server" clientidmode="static"></p>
                                                        </label>
                                                        <input type="file" name="uploadPOP" id="uploadPOP" class="form-control hide" accept="image/*,application/pdf" />
                                                    </div>
                                                </div>
                                                <div id="ifNewAgent" class="hide">
                                                    <div class="row mb-20">
                                                        <div class="col-md-12">
                                                            <label for="uploadQualification" class="file-upload">
                                                                Upload Qualification:
                                                                <br />
                                                                <small>(click here to upload)</small><br />
                                                                <small>Note: jpeg/png/pdf</small>
                                                                <span style="height: 180px;" id="uploadQualificationImage" runat="server" clientidmode="static">
                                                                    <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                                                </span>
                                                                <p style="height: 180px;" id="uploadQualificationImageLink" runat="server" clientidmode="static"></p>
                                                            </label>
                                                            <input type="file" name="uploadQualification" id="uploadQualification" class="form-control hide" accept="image/*,application/pdf" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="ifExistingAgentF" class="hide">
                                                    <div class="row mb-20">
                                                        <div class="col-md-12">
                                                            <h5>FiMM Documents</h5>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="uploadFiMMCard" class="file-upload">
                                                                Upload FiMM Card:
                                                                <br />
                                                                <small>(click here to upload)</small><br />
                                                                <small>Note: jpeg/png/pdf</small>
                                                                <span style="height: 180px;" id="uploadFiMMCardImage" runat="server" clientidmode="static">
                                                                    <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                                                </span>
                                                                <p style="height: 180px;" id="uploadFiMMCardImageLink" runat="server" clientidmode="static"></p>
                                                            </label>
                                                            <input type="file" name="uploadFiMMCard" id="uploadFiMMCard" class="form-control hide" accept="image/*,application/pdf" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="uploadResult" class="file-upload">
                                                                Upload Result:
                                                                <br />
                                                                <small>(click here to upload)</small><br />
                                                                <small>Note: jpeg/png/pdf</small>
                                                                <span style="height: 180px;" id="uploadResultImage" runat="server" clientidmode="static">
                                                                    <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                                                </span>
                                                                <p style="height: 180px;" id="uploadResultImageLink" runat="server" clientidmode="static"></p>
                                                            </label>
                                                            <input type="file" name="uploadResult" id="uploadResult" class="form-control hide" accept="image/*,application/pdf" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.panel-body -->
                                    </div>
                                    <!-- /#adjusthtml -->
                                </div>
                                <!-- /.panel.panel-default -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#confirm" data-parent="#accordion-demo" data-toggle="collapse">Confirmation
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="confirm" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="form-confirm" class="form-body">
                                                <div id="ifExistingAgent">
                                                    <div class="row mb-20">
                                                        <div class="col-md-12 mb-10">
                                                            <h5>FIMM Details</h5>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>New Card Received On *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtNewCardRecievedOn" runat="server" class="form-control datepicker-text" placeholder="Select New Card Recieved Date" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>FIMM No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMNo" runat="server" class="form-control" placeholder="Enter FIMM number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Approval Date *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtApprovalDate" runat="server" class="form-control datepicker-text" placeholder="Select Approval Date" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Expiry Date *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtExpiryDate" runat="server" class="form-control datepicker-text" placeholder="Select Expiry Date" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-10">
                                                            <h5>Payment Details - From Agent</h5>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>MICR Code *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMMICRCode" runat="server" class="form-control" placeholder="Enter MICR Code" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Bank Name *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMBankName" runat="server" class="form-control" placeholder="Enter Bank Name" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Cheque No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMChequeno" runat="server" class="form-control" placeholder="Enter Cheque No" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Transaction Ref No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMTransRefno" runat="server" class="form-control" placeholder="Enter Transaction Ref No" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Amount *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMAmount" runat="server" class="form-control" placeholder="Enter Amount" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Date Received *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMDateReceived" runat="server" class="form-control datepicker-text" placeholder="Select Date Received" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Payment Ref No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMPaymentRefno" runat="server" class="form-control" placeholder="Enter Payment Ref No" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Deposit Account No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFIMMDepositAccountNo" runat="server" class="form-control" placeholder="Enter Deposit Account No" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-10">
                                                            <h5>Payment Details - To FiMM</h5>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Withdrawal A/C No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtToFiMMWithAccNo" runat="server" class="form-control" placeholder="Enter Withdrawal Account No" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>MICR Code *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtToFiMMMICRCode" runat="server" class="form-control" placeholder="Enter MICR Code" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Bank Name *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtToFiMMBankName" runat="server" class="form-control" placeholder="Enter Bank Name" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Cheque No *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtToFiMMChequeno" runat="server" class="form-control" placeholder="Enter Cheque No" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>Date Sent *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtToFiMMDateSent" runat="server" class="form-control datepicker-text" placeholder="Select Date Sent" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-20">
                                                            <span>FiMM Renewal Amount *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtFiMMRenewalAmount" runat="server" class="form-control" placeholder="Enter FiMM Renewal Amount" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>UTMC Renewal Amount *</span>
                                                            <div>
                                                                <asp:TextBox ID="txtUTMCRenewalAmount" runat="server" class="form-control" placeholder="Enter UTMC Renewal Amount" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <ol type="a">
                                                        <li>I declare that the information given in this application are true, accurate and complete. I undertake to notify Apex Investment Services Berhad should there be any change(s) in my personal particulars.<br />
                                                        </li>
                                                        <li>I have read, understood and agreed to the Terms and Conditions of eApexis Portal.<br />
                                                        </li>
                                                        <li>I have read, understood and agreed to be bound by the Privacy Policy and agree with the processing of my personal data.</li>
                                                        <li>I hereby consent to receive notice of all statements and reports including statements of transaction, interim and annual statements, fund reports and/or other communications in electronic form and will be made available for my viewing and printing at eApexis Portal.</li>
                                                        <li>I declare that I am not a U.S person and in the event of a change in my status that I become a U.S Person, I shall notify Apex Investment Services Berhad of the change.</li>
                                                        <li>I declare that I am not undischarged bankrupt, have not committed any act of bankruptcy within the past 12 months and no bankruptcy order has been made against me or is pending against me during that period.  If any information is found false or misleading, Apex Investment Services Berhad may reject any of my application and instructions including but not limited to, any transactional-related activities</li>
                                                        <li>I shall indemnify and keep Apex Investment Services Berhad fully indemnified against any actions, proceedings, claims, losses, damages, costs and expenses which may be brought against, suffered or incurred and/or failing to act on any instructions given by me unless due to the willful default or negligence of Apex Investment Services Berhad.<br />
                                                        </li>
                                                    </ol>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="pull-left" style="width: 4%; display: inline-block;">
                                                            <input type="checkbox" id="chkDeclare" name="chkDeclare" style="float: left;" checked="checked"/>
                                                        </div>
                                                        <div class="pull-left" style="display: inline-block; line-height: 19px; margin-bottom: 15px; width: 96%;">
                                                            <label class="w-97" for="chkDeclare" style="margin-bottom: 0px;">I have read, understood and agree to all the above and wish to apply for eApexis portal which allows me to access my investment account information, view statements and fund reports, perform investments and transaction requests.</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>
                                        <!--/.panel-body -->
                                    </div>
                                    <!-- /#adjusthtml -->
                                </div>
                                <!-- /.panel.panel-default -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#recommend" data-parent="#accordion-demo" data-toggle="collapse">Recommendation
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="recommend" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="form-recommend" class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span>Recommendation *</span>
                                                        <asp:TextBox ID="txtRecommendation" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" ClientIDMode="Static" Height="100"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div>
                                                    <%--<button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep11">Next</button>
                                                    <div class="hide">--%>
                                                    <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-infos1 pull-right hide" Text="Submit" OnClick="btnSubmit_Click" />
                                                    <%--</div>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">

    <!-- ===================================================================== -->
    <!-- Javascript placed at the end of the document so the pages load faster -->
    <!-- ===================================================================== -->

    <!-- Latest version of minified js for Accordion Wizard -->
    <%--<script src="http://sathomas.me/acc-wizard/js/acc-wizard.min.js"></script>--%>
    <script src="Content/acc-open-steps/acc-wizard.min.js"></script>
    <script src="Content/acc-open-steps/js/face-api.js"></script>
    <script src="Content/acc-open-steps/js/commons.js"></script>
    <script src="Content/acc-open-steps/js/faceDetectionControls.js"></script>

    <!-- Or, for development, use latest version of Accordion Wizard -->
    <!-- <script src="js/acc-wizard.js"></script> -->
    <script type="text/javascript">

        window.onscroll = function () { makeMenuSticky() };

        var navbar = document.getElementById("sticky-div");
        var sticky = navbar.offsetTop;

        function makeMenuSticky() {
            if (window.pageYOffset >= sticky) {
                navbar.classList.add("sticky");
                navbar.classList.add("col-md-2");
            } else {
                navbar.classList.remove("sticky");
                navbar.classList.remove("col-md-2");
            }
        }



        function beforeNext() {
            //alert('s');
            //Not Working
            return false;
        }
        var noOfFiles = 0;
        var loadingFiles = [];
        function onNext(parent, panel) {
            console.log(panel.id);
            var hash = "#" + panel.id;
            var formArray = [];
            $(panel).find('*').filter(':input').each(function () {
                if ($(this).attr("type") == 'file') {
                    if ($(this)[0].files.length > 0) {
                        var inputIdPrefix = $(this).attr('id')
                        $.each($(this)[0].files, function (idxFile, objFile) {
                            noOfFiles++;
                            var inputId = inputIdPrefix + (idxFile == 0 ? "" : idxFile);
                            var FileName = objFile.name;
                            var fReader = new FileReader();
                            fReader.readAsDataURL(objFile);
                            fReader.onloadend = function (event) {
                                formArray.push({
                                    name: inputId,
                                    value: event.target.result
                                });
                                formArray.push({
                                    name: inputId + "FileName",
                                    value: FileName
                                });
                                loadingFiles.push(true);
                                console.log(noOfFiles + " - " + loadingFiles.length);
                                //if (noOfFiles == loadingFiles.length) {
                                //    btnNextStepAjax(panel, parent, formArray, hash);
                                //}
                            }
                        });
                        //var inputId = $(this).attr('id');
                        //var FileName = $(this)[0].files[0].name;
                        ////var blobURL = URL.createObjectURL($(this)[0].files[0])
                        ////formArray.push({
                        ////    name: inputId,
                        ////    value: blobURL
                        ////});
                        ////btnNextStepAjax(id, formArray);
                        //var fReader = new FileReader();
                        //fReader.readAsDataURL($(this)[0].files[0]);
                        //fReader.onloadend = function (event) {
                        //    formArray.push({
                        //        name: inputId,
                        //        value: event.target.result
                        //    });
                        //    formArray.push({
                        //        name: inputId + "FileName",
                        //        value: FileName
                        //    });
                        //    loadingFiles.push(true);
                        //    console.log(noOfFiles + " - " + loadingFiles.length);
                        //    if (noOfFiles == loadingFiles.length) {
                        //        btnNextStepAjax(id, formArray);
                        //    }
                        //}
                    }
                }
                else if ($(this).attr("type") == 'checkbox') {
                    if ($(this).is(':checked')) {
                        formArray.push({
                            name: $(this).attr('id'),
                            value: $(this).val()
                        });
                    }
                    else {
                        formArray.push({
                            name: $(this).attr('id'),
                            value: null
                        });
                    }
                }
                else {
                    formArray.push({
                        name: $(this).attr('id'),
                        value: $(this).val()
                    });
                }
            });
            formArray.push({
                name: 'hdnOpenAgentId',
                value: $('#hdnOpenAgentId').val()
            });
            formArray.push({
                name: 'panelId',
                value: panel.id
            });
            formArray = formArray.filter(function (item) {
                return item.name != undefined && item.name.indexOf('_') === -1;
            });
            console.log(formArray);
            //$('.loader-bg').fadeIn();
            //$('.loader-bg').fadeOut();

            //var agentCode = $(panel).find('#txtAgentCode').val();
            //if (noOfFiles == 0)
            //    btnNextStepAjax(panel, parent, formArray, hash);
        }
        function btnNextStepAjax(panel, parent, formArray, hash) {
            $.ajax({
                url: "OpenAgentAccount.aspx/btnNext_Click",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify({ formInputs: formArray }),
                success: function (data) {
                    $('.err-msg').remove();
                    $('.field-required').removeClass('field-required');
                    $('.loader-bg').fadeOut();
                    var json = data.d;
                    if (json.CustomValidations != null) {
                        if (json.CustomValidations.length > 0) {
                            $('#' + json.CustomValidations[0].Name).focus();
                        }
                        $.each(json.CustomValidations, function (idx, obj) {
                            console.log(obj);
                            $('#' + obj.Name).addClass('field-required');
                            if (($('#' + obj.Name).hasClass('field-required'))) {
                                $('#err-' + obj.Name).html('<div class="err-msg"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>');
                            }
                            if (!($('#' + obj.Name).hasClass('field-required'))) {
                                if (obj.Name == 'addr1') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('#addr3');
                                }
                                else if (obj.Name == 'txtOfficeAddress1') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('#txtOfficeAddress3');
                                }
                                else if (obj.Name == 'addr1M') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('#addr3M');
                                }
                                else if (obj.Name == 'ddlTaxResidencyStatus') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.ddlTaxResidencyStatus').val() == "") {
                                            var tx = $(objTax).find('.ddlTaxResidencyStatus');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });
                                }
                                else if (obj.Name == 'txtTINNum') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.txtTINNum').val() == "") {
                                            var tx = $(objTax).find('.txtTINNum');
                                            $(tx).addClass('field-required');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter(tx);
                                        }
                                        else {

                                        }
                                    });
                                }
                                else if (obj.Name == 'txtnoTIN') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.txtnoTIN').val() == "") {
                                            var tx = $(objTax).find('.txtnoTIN');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });

                                }
                                else if (obj.Name == 'optionBReason') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.optionBReason').val() == "") {
                                            var tx = $(objTax).find('.optionBReason');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });

                                }
                                else {
                                    $('<div class="err-msg" id="err-' + obj.Name + '"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('#' + obj.Name);
                                }
                                $('#' + obj.Name).addClass('field-required');
                                if (obj.Name == 'checkbox-group-3') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-3');
                                }
                                else if (obj.Name == 'checkbox-group-4') {

                                    $('<div class="err-msg"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-4');
                                }
                                else if (obj.Name == 'checkbox-group-1') {

                                    $('<div class="err-msg" style=""><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-1');
                                }
                                else if (obj.Name == 'checkbox-group-2') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-2');
                                }
                                else if (obj.Name == 'checkbox-group-5') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('#placeForResidentErr');
                                }
                                else if (obj.Name == 'checkbox-group-8') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-8');
                                }
                                else if (obj.Name == 'checkbox-group-9') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-9');
                                }
                                else if (obj.Name == 'checkbox-group-10') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red;">' + obj.Message + '</small></div>').insertAfter('#placeForNationalityErr');
                                }

                            }
                        });
                    }
                    if (!json.IsSuccess) {
                        setTimeout(function () {
                            $('#accordion-demo [href="' + hash + '"]').click();
                        }, 500);
                        if (json.Message == 'Please fill in details') { }
                        else
                            ShowCustomMessage('Alert', json.Message, '');
                    }
                    else if (json.IsSuccess) {
                        var agentRegExisting = json.Data;
                        console.log(agentRegExisting);
                        if (panel.id == 'agency') {
                            var AgentRegPersonal = agentRegExisting.AgentRegPersonal;

                        }
                        $(".acc-wizard-sidebar", $(parent))
                            .children("li")
                            .children("a[href='" + hash + "']")
                            .parent("li")
                            .removeClass("acc-wizard-todo")
                            .addClass("acc-wizard-completed");
                    }
                }
            });
        }
        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
        function parseJsonDate(jsonDateString) {
            var formattedDate = new Date(parseInt(jsonDateString.replace('/Date(', '')));
            var hours = formattedDate.getHours();
            var minutes = formattedDate.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var newDate = pad(formattedDate.getDate(), 2) + "/" + pad(formattedDate.getMonth() + 1, 2) + "/" + formattedDate.getFullYear() + " " + strTime;
            return newDate;
        }
        $(document).ready(function () {
            $(".acc-wizard").accwizard({
                beforeNext: beforeNext,
                onNext: onNext,
                nextText: "Next",
                backText: "Back",
                nextClasses: "btn btn-infos1",
                backClasses: "btn",
                autoButtonsShowSubmit: true,
                onBack: function () {
                }, onInit: function () {
                    if ($('#hdnViewOnly').val() != "1") {
                        if(!$('#txtRecommendation').is(':disabled'))
                            $('#recommend .acc-wizard-step').append('<button type="button" id="btnSubmitRef" class="btn btn-infos1">Submit</button>');
                    }
                    else {
                        $('.acc-wizard :input').attr('readonly', 'readonly');
                        $('.acc-wizard select').attr('disabled', 'disabled');
                    }
                }, onDestroy: function () {
                },
                onSubmit: function () {
                    alert('Form submitted!');
                    return true;
                }
            });

            $('#chkIfAttendedExam').click(function () {
                if ($(this).is(":checked")) {
                    $('#divNotAttendedExam').addClass('hide');
                    $('#divAttendedExam').removeClass('hide');
                }
                else {
                    $('#divNotAttendedExam').removeClass('hide');
                    $('#divAttendedExam').addClass('hide');
                }
            });

            $('#ddlSessionId').change(function () {
                var SessionId = $(this).val();
                if (SessionId == "") {
                    $('#txtLanguage').val('');
                    $('#txtLocation').val('');
                    $('#txtExamDate').val('');
                }
                else {
                    $.ajax({
                        url: "CompleteSignup.aspx/GetSessionDetails",
                        contentType: 'application/json; charset=utf-8',
                        type: "POST",
                        dataType: "JSON",
                        data: JSON.stringify({ SessionId: SessionId }),
                        success: function (data) {
                            var json = data.d;
                            if (!json.IsSuccess) {
                                ShowCustomMessage('Alert', json.Message, '');
                            }
                            else if (json.IsSuccess) {
                                var CustomAgentExamSchedule = json.Data;
                                var dat = parseJsonDate(CustomAgentExamSchedule.exam_datetime);
                                $('#txtLanguage').val(CustomAgentExamSchedule.language);
                                $('#txtLocation').val(CustomAgentExamSchedule.location);
                                $('#txtExamDate').val(dat);
                            }
                        }
                    });
                }
            });
            $('#ddlSessionIdOld').change(function () {
                var SessionId = $(this).val();
                if (SessionId == "") {
                    $('#txtLanguageAttended').val('');
                    $('#txtLocationAttended').val('');
                    $('#txtExamDateAttended').val('');
                }
                else {
                    $.ajax({
                        url: "CompleteSignup.aspx/GetSessionDetails",
                        contentType: 'application/json; charset=utf-8',
                        type: "POST",
                        dataType: "JSON",
                        data: JSON.stringify({ SessionId: SessionId }),
                        success: function (data) {
                            var json = data.d;
                            if (!json.IsSuccess) {
                                ShowCustomMessage('Alert', json.Message, '');
                            }
                            else if (json.IsSuccess) {
                                var CustomAgentExamSchedule = json.Data;
                                var dat = parseJsonDate(CustomAgentExamSchedule.exam_datetime);
                                $('#txtLanguageAttended').val(CustomAgentExamSchedule.language);
                                $('#txtLocationAttended').val(CustomAgentExamSchedule.location);
                                $('#txtExamDateAttended').val(dat);
                            }
                        }
                    });
                }
            });

            $('#btnAddExamAttended').click(function () {
                var ddlSessionIdOld = $('#ddlSessionIdOld').val();
                var ddlExamFirstTime = $('#ddlExamFirstTime').val();
                var txtExamResitDateAttended = $('#txtExamResitDateAttended').val();
                var ddlExamResultAttended = $('#ddlExamResultAttended').val();

                $.ajax({
                    url: "CompleteSignup.aspx/AddExamAttended",
                    contentType: 'application/json; charset=utf-8',
                    type: "POST",
                    dataType: "JSON",
                    data: JSON.stringify({ SessionId: ddlSessionIdOld, FirstTime: ddlExamFirstTime, ExamResitDateAttended: txtExamResitDateAttended, ExamResultAttended: ddlExamResultAttended }),
                    success: function (data) {
                        var json = data.d;
                        if (!json.IsSuccess) {
                            ShowCustomMessage('Alert', json.Message, '');
                        }
                        else if (json.IsSuccess) {
                            var AgentExamsAttendedHtml = json.Data;
                            $('#tbodyAttendedExams').html(AgentExamsAttendedHtml);
                            //var dat = parseJsonDate(CustomAgentExamSchedule.exam_datetime);
                            //$('#txtLanguage').val(CustomAgentExamSchedule.language);
                            //$('#txtLocation').val(CustomAgentExamSchedule.location);
                            //$('#txtExamDate').val(dat);


                            $('#ddlSessionIdOld').val('');
                            $('#txtLanguageAttended').val('');
                            $('#txtLocationAttended').val('');
                            $('#txtExamDateAttended').val('');

                            $('#ddlExamFirstTime').val('');
                            $('#txtExamResitDateAttended').val('');
                            $('#ddlExamResultAttended').val('');


                        }
                    }
                });

            });
            $('#btnSubmitRef').click(function () {
                $('#btnSubmit').click();
            });

            $('#ddlRace').change(function () {
                if ($(this).val() == 'O') {

                    $('#race-others').removeClass('hide');

                }
                else {
                    $('#race-others').addClass('hide');
                    $('#txtRaceDesc').val('');
                }
            });

            $('#ddlReligion').change(function () {
                if ($(this).val() == '4') {

                    $('#religion-others').removeClass('hide');

                }
                else {
                    $('#religion-others').addClass('hide');
                    $('#txtReligionsDesc').val('');
                }
            });

            $('#ddlEducation').change(function () {
                if ($(this).val() == '6') {

                    $('#education-others').removeClass('hide');

                }
                else {
                    $('#education-others').addClass('hide');
                    $('#txtEducationDesc').val('');
                }
            });

            $('#ddlMaritalStatus').change(function () {
                if ($(this).val() == 'Others') {

                    $('#marital-others').removeClass('hide');

                }
                else {
                    $('#marital-others').addClass('hide');
                    $('#txtMaritalDesc').val('');
                }
            });

            $('.checkbox-group-1 input:checkbox').click(function () {
                $('.checkbox-group-1 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkBankruptcyYes') {
                    if ($(this).is(':checked')) {
                        $('#bankruptcy-desc').removeClass('hide');
                    }
                    else {
                        $('#bankruptcy-desc').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkBankruptcyNo') {
                    if ($(this).is(':checked')) {
                        $('#bankruptcy-desc').addClass('hide');
                    }
                    else {

                    }
                }
            });

            $('#uploadSelfieFile').change(function () {
                $('#custom-popup-close').addClass('hide');
                ShowCustomMessage('Loading...', 'Please wait while we are processing the image.', '');
                try {
                    var fileInput = $(this)[0];
                    if ($(this)[0].files.length > 0) {
                        $('.loader-bg').fadeIn();
                        var fileType = $(this)[0].files[0]["type"];
                        var validImageTypes = ["image/jpeg", "image/png", "image/jpg"];
                        if ($.inArray(fileType, validImageTypes) < 0) {
                            $('#custom-popup-close').removeClass('hide');
                            $('#uploadSelfieFile').val('');
                            $('.loader-bg').fadeOut();
                            ShowCustomMessage('Alert', 'Invalid file', '');
                        }
                        else {
                            $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                            const fsize = $(this)[0].files[0].size;
                            const fileSizeMB = Math.round((fsize / 1024));
                            if (fileSizeMB >= 2048) {
                                ShowCustomMessage('Alert', 'File too Big, please select a file less than 2 MB', '');
                                $('#custom-popup-close').removeClass('hide');
                                $('#uploadSelfieFile').val('');
                                $('.loader-bg').fadeOut();
                                return false;
                            } else if (fileSizeMB < 20) {
                                ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                                $('#custom-popup-close').removeClass('hide');
                                $('#uploadSelfieFile').val('');
                                $('.loader-bg').fadeOut();
                                return false;
                            } else {
                                var FileName = $(this)[0].files[0].name;
                                var reader = new FileReader();
                                var baseString;
                                reader.onloadend = function () {
                                    baseString = reader.result;
                                    //console.log(baseString);
                                    $('#inputImg').get(0).src = baseString;
                                    updateResults();
                                };
                                reader.readAsDataURL($(this)[0].files[0]);
                                //var blobURL = URL.createObjectURL($(this)[0].files[0]);
                                //$('#inputImg').get(0).src = blobURL;
                                //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' />");
                            }
                        }
                    }
                }
                catch (ex) {
                    ShowCustomMessage('Alert', 'Invalid file', '');
                    $('#custom-popup-close').removeClass('hide');
                    $('.loader-bg').fadeOut();
                }
            });

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, { type: contentType });
                return blob;
            }


            //console.log(faceapi.nets);
            faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
            faceapi.nets.faceLandmark68TinyNet.loadFromUri('/models')
            const modelPath = '/models';
            async function updateResults() {
                try {
                    //const webcamElement = document.getElementById('webcam1');
                    //const canvasElement = document.getElementById('canvas1');
                    //const webcam = new Webcam(webcamElement, 'user', canvasElement);
                    //webcam.start()
                    //    .then(result => {
                    //        console.log("webcam started");
                    //    })
                    //    .catch(err => {
                    //        console.log(err);
                    //    });
                    //$("#snapShotFromWebCam").click(function () {
                    //    console.log("webcam picture");
                    //    var picture = webcam.snap();
                    //    console.log(picture);
                    //    $('#inputImg').get(0).src = picture;
                    //});

                    //faceapi.nets.tinyFaceDetector.load(modelPath);
                    //faceapi.nets.faceLandmark68TinyNet.load(modelPath);
                    //faceapi.nets.faceRecognitionNet.load(modelPath);
                    //faceapi.nets.faceExpressionNet.load(modelPath);
                    //faceapi.nets.ageGenderNet.load(modelPath);

                    //if (document.getElementsByTagName("canvas").length == 0) {
                    //    canvas = faceapi.createCanvasFromMedia(webcamElement)
                    //    document.getElementById('webcam-container').append(canvas)
                    //    faceapi.matchDimensions(canvas, displaySize)
                    //}

                    //faceDetection = setInterval(async () => {
                    //    const detections = await faceapi.detectAllFaces(webcamElement, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks(true).withFaceExpressions().withAgeAndGender()
                    //    const resizedDetections = faceapi.resizeResults(detections, displaySize)
                    //    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
                    //    faceapi.draw.drawDetections(canvas, resizedDetections)
                    //    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
                    //    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
                    //    resizedDetections.forEach(result => {
                    //        const { age, gender, genderProbability } = result
                    //        new faceapi.draw.DrawTextField(
                    //            [
                    //                `${faceapi.round(age, 0)} years`,
                    //                `${gender} (${faceapi.round(genderProbability)})`
                    //            ],
                    //            result.detection.box.bottomRight
                    //        ).draw(canvas)
                    //    })
                    //}, 300)




                    //console.log(isFaceDetectionModelLoaded());
                    if (!isFaceDetectionModelLoaded()) {
                        return
                    }

                    const inputImgEl = $('#inputImg').get(0)
                    const options = getFaceDetectorOptions()
                    const useTinyModel = true
                    const results = await faceapi.detectAllFaces(inputImgEl, options).withFaceLandmarks(useTinyModel)
                    //console.log(results);
                    if (results.length == 1) {
                        const canvas = $('#canvas').get(0);
                        console.log(canvas);
                        faceapi.matchDimensions(canvas, inputImgEl);
                        faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
                        ShowCustomMessage("Alert", "Face detected. Thank you.", "");
                        $('#custom-popup-close').removeClass('hide');
                        var img = inputImgEl;
                        var base64 = img.src;
                        $('#uploadSelfie').val(base64);
                        let r = Math.random().toString(36).substring(7);
                        console.log("random", r);
                        var contentType = img.src.split(',')[0].split(';')[0].split('/')[1];
                        var today = new Date();
                        $('#uploadSelfieFileName').val('Webcam' + r + today.getFullYear() + "" + (today.getMonth() + 1) + "" + today.getDate() + "" + today.getHours() + "" + today.getMinutes() + "" + today.getSeconds() + "" + today.getMilliseconds() + "." + contentType);
                        $('.loader-bg').fadeOut();
                        //var base64 = img.src.split(',')[1];
                        //var contentType = img.src.split(',')[0].split(';')[0];
                        //var blob = b64toBlob(base64, contentType);
                        //var blobUrl = URL.createObjectURL(blob);
                        //console.log(blobUrl);
                        //URL ***************************************************************
                        $('#video').addClass('hide');
                        $('#snap').addClass('hide');
                        $('#webcam-pic').removeClass('hide');
                        $('#cam-option').addClass('hide');
                        $('#try-snap-again').removeClass('hide');
                        $('#upload-selfie-option').addClass('hide');
                    }
                    else if (results.length == 0) {
                        $('.loader-bg').fadeOut();
                        ShowCustomMessage("Alert", "No face detected! Please try again.", "");
                        $('#custom-popup-close').removeClass('hide');
                        $('#try-snap-again').removeClass('hide');
                    }
                    else if (results.length > 1) {
                        $('.loader-bg').fadeOut();
                        const canvas = $('#canvas').get(0);
                        faceapi.matchDimensions(canvas, inputImgEl);
                        faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
                        ShowCustomMessage("Alert", "Multiple faces detected! Please try again.", "");
                        $('#custom-popup-close').removeClass('hide');
                        $('#try-snap-again').removeClass('hide');
                    }
                }
                catch (ex) {
                    ShowCustomMessage('Alert', 'Invalid file', '');
                    $('#custom-popup-close').removeClass('hide');
                    $('.loader-bg').fadeOut();
                }
            }

            updateResults();
            // Grab elements, create settings, etc.
            var video = document.getElementById('video');

            // Get access to the camera!
            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                // Not adding `{ audio: true }` since we only want video now
                navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    video.srcObject = stream;
                    video.play();
                    $('#noCam').val("0");
                    $('#cam-option').removeClass('hide');
                }, function () {
                    document.getElementById('video').style.display = "none";
                    $('#noCam').val("1");
                    $('#cam-option').addClass('hide');
                });
            }

            /* Legacy code below: getUserMedia
        else if(navigator.getUserMedia) { // Standard
            navigator.getUserMedia({ video: true }, function(stream) {
                video.src = stream;
                video.play();
            }, errBack);
        } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
            navigator.webkitGetUserMedia({ video: true }, function(stream){
                video.src = window.webkitURL.createObjectURL(stream);
                video.play();
            }, errBack);
        } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
            navigator.mozGetUserMedia({ video: true }, function(stream){
                video.srcObject = stream;
                video.play();
            }, errBack);
        }
        */

            // Elements for taking the snapshot
            var canvas = document.getElementById('canvas');
            var context = canvas.getContext('2d');
            var video = document.getElementById('video');



            // Trigger photo take
            document.getElementById("snap").addEventListener("click", function () {
                context.drawImage(video, 0, 0, 640, 480);
                var dataURL = $('#canvas')[0].toDataURL();
                //console.log(dataURL);
                $('#inputImg').get(0).src = dataURL;
                $('.loader-bg').fadeIn();
                ShowCustomMessage('Loading...', 'Please wait while we are processing the image.', '');
                $('#custom-popup-close').addClass('hide');
                updateResults();
            });

            document.getElementById("try-snap-again").addEventListener("click", function () {
                $('#video').removeClass('hide');
                $('#snap').removeClass('hide');
                $('#cam-option').removeClass('hide');
                $('#webcam-pic').addClass('hide');
                $('#try-snap-again').addClass('hide');
                $('#upload-selfie-option').removeClass('hide');
            });

            //document.getElementById("existing-selfie").addEventListener("click", function () {
            //    $('#webcam').removeClass('hide');
            //    $('#existing-selfie').addClass('hide');
            //    $('#upload-selfie-option').removeClass('hide');
            //    if ($('#noCam').val() == "0") {
            //        $('#cam-option').removeClass('hide');
            //        //$('#webcam').removeClass('hide');
            //    }
            //    else {
            //        $('#cam-option').addClass('hide');
            //        //$('#webcam').addClass('hide');
            //        //$('#no-cam-div').removeClass('hide');
            //    }
            //});
            $('#uploadNRICFrontPage').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadNRICFrontPage').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadNRICFrontPage').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });
            $('#uploadNRICBackPage').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadNRICBackPage').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadNRICBackPage').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' />");
                    }
                }
            });

            $('#uploadPOP').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadPOP').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadPOP').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Proof of Payment' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='Proof of Payment' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });
            $('#uploadQualification').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadQualification').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadQualification').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Qualification' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='Qualification' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });

            $('#uploadFiMMCard').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadFiMMCard').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadFiMMCard').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Qualification' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='Qualification' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });

            $('#uploadResult').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadResult').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadResult').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Qualification' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='Qualification' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });

        });


    </script>



    <script>
        $(document).ready(function () {
            $('#accordion-demo :input:not(.btn, #txtRecommendation)').prop('disabled', true);
            $(".datepicker-text").datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
            });
            $('.checkbox-group-1 input:checkbox').click(function () {
                $('.checkbox-group-1 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkIsCUTEPassedNo' && $(this).is(':checked')) {
                    if ($(this).is(':checked')) {
                        $('#ifChecked').removeClass('hide');
                        $('#ifNewAgent').removeClass('hide');
                        $('#ifExistingAgent').addClass('hide');
                        $('#ifExistingAgentF').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkIsCUTEPassedYes' && $(this).is(':checked')) {
                    if ($(this).is(':checked')) {
                        $('#ifChecked').removeClass('hide');
                        $('#ifNewAgent').addClass('hide');
                        $('#ifExistingAgent').removeClass('hide');
                        $('#ifExistingAgentF').removeClass('hide');
                    }
                }
                else {
                    $('#ifChecked').addClass('hide');
                }
            });


            $('.checkbox-group-2 input:checkbox').click(function () {
                $('.checkbox-group-2 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkRecommendedAgentYes' && $(this).is(':checked')) {
                    if ($(this).is(':checked')) {
                        $('#IndroducerDiv').removeClass('hide');
                        $('#txtIntroCode').removeAttr('readonly');
                        $('#txtIntroCode').val("");
                    }
                }
                else if ($(this).attr('id') == 'chkRecommendedAgentNo' && $(this).is(':checked')) {
                    if ($(this).is(':checked')) {
                        $('#IndroducerDiv').addClass('hide');
                        $('#txtIntroCode').attr('readonly', 'readonly');
                        $('#txtIntroCode').val("MG0001");
                    }
                }
                else {
                    $('#IndroducerDiv').addClass('hide');
                    //$('#ifChecked').addClass('hide');
                }
            });

            if ($("#chkIsCUTEPassedYes").is(':checked')) {
                $('#IndroducerDiv').removeClass('hide');
                $('#txtIntroCode').removeAttr('readonly');
                $('#ifExistingAgent').removeClass('hide');
                $('#ifExistingAgentF').removeClass('hide');
            }
            else if ($("#chkIsCUTEPassedNo").is(':checked')) {
                $('#IndroducerDiv').addClass('hide');
                $('#txtIntroCode').attr('readonly', 'readonly');
                $('#ifExistingAgent').addClass('hide');
                $('#ifExistingAgentF').addClass('hide');
            }

            if ($("#chkRecommendedAgentYes").is(':checked')) {
                $('#IndroducerDiv').removeClass('hide');
                $('#txtIntroCode').removeAttr('readonly');
            }
            else if ($("#chkRecommendedAgentNo").is(':checked')) {
                $('#IndroducerDiv').addClass('hide');
                $('#txtIntroCode').attr('readonly', 'readonly');
            }

            $('#chkListSessionId input:checkbox').click(function () {
                console.log($('#chkListSessionId input:checkbox:checked'));
                if ($('#chkListSessionId input:checkbox:checked').length > 2) {
                    $('#chkListSessionId input:checkbox:checked').not(this).first().prop('checked', false);
                    //ShowCustomMessage("Alert", "You can only select any two preferences", "");
                    //return false;
                    //$('#chkListSessionId input:checkbox').not(this).prop('checked', false);
                }
            });

            if ($('#chkIsNewAgentYes').is(':checked')) {
                $('#ifChecked').removeClass('hide');
                $('#ifNewAgent').removeClass('hide');
            }
            else if ($('#chkIsNewAgentNo').is(':checked')) {
                $('#ifChecked').removeClass('hide');
                $('#ifNewAgent').addClass('hide');
            }
            if ($('#chkBankruptcyYes').is(':checked')) {
                $('#bankruptcy-desc').removeClass('hide');
            }
            else if ($('#chkBankruptcyNo').is(':checked')) {
                $('#bankruptcy-desc').addClass('hide');
            }
            if ($('#uploadSelfieImage').find('img').length > 0) {
                $('#existing-selfie').removeClass('hide');
                $('#webcam').addClass('hide');
            }
        });
    </script>
</asp:Content>
