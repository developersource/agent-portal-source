﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="TaxInvoice.aspx.cs" Inherits="DiOTP.WebApp.TaxInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        #accountSelectedDropdown {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section portfolio-section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">Portfolio</a></li>
                            <li class="active">Tax Invoice</li>
                        </ol>
                    </div>

                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Tax Invoice/ CAS/ Credit Note</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="statement">
                                <h5 class="hidden-lg hidden-md">Generate below for selected account:</h5>
                                <div class="row">
                                    <div class="col-md-3 mb-8">
                                        <label>Account Number:</label>
                                        <asp:DropDownList ID="ddlFundAccount" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 mb-8">
                                        <label>From</label>
                                        <asp:TextBox ID="txtDateStart" runat="server" TextMode="Date" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 mb-8">
                                        <label>To</label>
                                        <asp:TextBox ID="txtDateEnd" runat="server" TextMode="Date" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 mb-8">
                                        <asp:Button ID="btnGenerate" runat="server" Text="Submit" CssClass="btn btn-block btn-infos1 mt-30" OnClientClick="return clientStatementValidation()" OnClick="btnGenerate_Click" />
                                    </div>
                                </div>
                            </div>

                            <h5>Tax Invoice : <span id="spanUsername" runat="server"></span></h5>
                            <table class="table table-font-size-13 table-cell-pad-5 myDataTable" id="TransactionFile">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">Transaction No</th>
                                        <th style="text-align: center">MA No</th>
                                        <th style="text-align: center">Date</th>
                                        <th style="text-align: center">Tax Invoice</th>
                                        <th style="text-align: center">CAS</th>
                                        <th style="text-align: center">Credit Note</th>
                                    </tr>
                                </thead>
                                <tbody id="taxInvoiceTbody" runat="server">
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>


</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="/Content/js/jquery.dataTables.min.js"></script>
    <script src="/Content/js/dataTables.bootstrap.min.js"></script>
    <script src="/Content/js/dataTables.responsive.min.js"></script>
    <script src="/Content/js/responsive.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $("#TransactionFile").DataTable({
                responsive: true,
            });
        });

        function clientStatementValidation() {
            if ($('#txtDateStart').val() == "") {
                ShowCustomMessage('Alert', 'Please select a Date(From)', '');
                return false;
            }
            else if ($('#txtDateEnd').val() == "") {
                ShowCustomMessage('Alert', 'Please select a Date(To)', '');
                return false;
            }
            else if ($('#txtDateStart').val() > $('#txtDateEnd').val()) {
                ShowCustomMessage('Alert', 'Date(From) cannot be greater than Date(To)', '');
                return false;
            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }
    </script>
</asp:Content>
