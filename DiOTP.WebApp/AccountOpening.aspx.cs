﻿using DiOTP.PolicyAndRules;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.WebApp
{
    public partial class AccountOpening : System.Web.UI.Page
    {
        public static Int32 SMSExpirationTimeForAOInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeForAOInSeconds"]);

        public string EmailVerificationTimeInMinutes = ConfigurationManager.AppSettings["EmailVerificationTimeInMinutes"].ToString();

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<IAccountOpeningService> lazyObjAO = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());

        public static IAccountOpeningService IAccountOpeningService { get { return lazyObjAO.Value; } }

        private static readonly Lazy<IAccountOpeningAddressService> lazyObjAOA = new Lazy<IAccountOpeningAddressService>(() => new AccountOpeningAddressService());

        public static IAccountOpeningAddressService IAccountOpeningAddressService { get { return lazyObjAOA.Value; } }

        private static readonly Lazy<IAccountOpeningOccupationService> lazyObjO = new Lazy<IAccountOpeningOccupationService>(() => new AccountOpeningOccupationService());

        public static IAccountOpeningOccupationService IAccountOpeningOccupationService { get { return lazyObjO.Value; } }

        private static readonly Lazy<IAccountOpeningFinancialProfileService> lazyObjFP = new Lazy<IAccountOpeningFinancialProfileService>(() => new AccountOpeningFinancialProfileService());

        public static IAccountOpeningFinancialProfileService IAccountOpeningFinancialProfileService { get { return lazyObjFP.Value; } }

        private static readonly Lazy<IAccountOpeningBankDetailService> lazyObjBD = new Lazy<IAccountOpeningBankDetailService>(() => new AccountOpeningBankDetailService());

        public static IAccountOpeningBankDetailService IAccountOpeningBankDetailService { get { return lazyObjBD.Value; } }

        private static readonly Lazy<IAccountOpeningCRSDetailService> lazyObjCRS = new Lazy<IAccountOpeningCRSDetailService>(() => new AccountOpeningCRSDetailService());

        public static IAccountOpeningCRSDetailService IAccountOpeningCRSDetailService { get { return lazyObjCRS.Value; } }

        private static readonly Lazy<IAccountOpeningFileService> lazyObjF = new Lazy<IAccountOpeningFileService>(() => new AccountOpeningFileService());

        public static IAccountOpeningFileService IAccountOpeningFileService { get { return lazyObjF.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        public static List<BanksDef> bankdefs = new List<BanksDef>();

        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());

        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static string keyOrg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnSMSExpirationTimeForAOInSeconds.Value = SMSExpirationTimeForAOInSeconds.ToString();
            if (Session["AccountOpeningExisting"] != null || Session["OTPIsVerified"] != null)
            {
                Session["AccountOpeningExisting"] = null;
                Session["OTPIsVerified"] = null;
            }
            string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/");
            string key = Request.QueryString["key"];
            keyOrg = key;
            if (!string.IsNullOrEmpty(key))
            {
                key = key.Replace(" ", "+");
                key = CustomEncryptorDecryptor.DecryptText(key);
                string date = key.Split('_')[1];
                DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                string from = key.Split('_')[2];
                string accType = key.Split('_')[3];
                string accPrincipleUserId = key.Split('_')[4];
                if (from == "admin")
                {
                    if (accType == "2")
                    {
                        Session["IsJointAccountOpening"] = "1";
                    }
                    string id = key.Split('_')[0];
                    Response responseA = IAccountOpeningService.GetSingle(Convert.ToInt32(id));
                    if (responseA.IsSuccess)
                    {
                        Utility.AccountOpening accountOpening = (Utility.AccountOpening)responseA.Data;
                        if (accountOpening.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                        {
                            HttpContext.Current.Session["AccountOpeningExisting"] = accountOpening;
                            //divAboutYouHeader.Visible = false;
                            //divAboutYou.Visible = false;
                            usChecks.Visible = false;
                            divPersonalHeader.Visible = false;
                            divPersonal.Visible = false;
                            divContactHeader.Visible = false;
                            divContact.Visible = false;
                            divOccupationHeader.Visible = false;
                            divOccupation.Visible = false;
                            divFinancialHeader.Visible = false;
                            divFinancial.Visible = false;
                            bankDetailsHeader.Visible = false;
                            bankDetails.Visible = false;
                            divDisclosureHeader.Visible = false;
                            divDisclosure.Visible = false;
                            divReenterPasswor.Visible = false;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseA.Message + "\", '');", true);
                    }
                }
            }

            if (HttpContext.Current.Session["IsJointAccountOpening"] == null || Convert.ToInt32(HttpContext.Current.Session["IsJointAccountOpening"].ToString()) == 0)
            {
                AccountTypeSpan.InnerHtml = "Individual Applicant";
            }
            else
            {
                IdFrontAccountType.InnerHtml = "Joint";
                IDBackAccountType.InnerHtml = "Joint";
                selfisAccountType.InnerHtml = "Joint";
                AccountTypeSpan.InnerHtml = "Joint Applicant";
                AccountTypeSpan.InnerHtml = "Joint Applicant";
                selfisAccountType.InnerHtml = "Joint";
                selfis2AccountType.InnerHtml = "Joint";
                accTypeSign.InnerHtml = "Joint";
                bankDetailsHeader.Visible = false;
                bankDetails.Visible = false;
            }
            if (!IsPostBack)
            {
                Response responseMonthlyIncome = ServicesManager.GetMonthlyIncomeData();
                if (responseMonthlyIncome.IsSuccess)
                {
                    List<IncomeDTO> incomeDTOs = (List<IncomeDTO>)responseMonthlyIncome.Data;
                    ddlMonthlyIncome.Items.Clear();
                    ddlMonthlyIncome.DataSource = incomeDTOs.Where(x => x.SDESC != "NIL").Select(x => new IncomeDTO { INCOMECODE = x.INCOMECODE, SDESC = x.SDESC.Capitalize(), DOWNLOADIND = x.DOWNLOADIND }).ToList();
                    ddlMonthlyIncome.DataTextField = "SDESC";
                    ddlMonthlyIncome.DataValueField = "INCOMECODE";
                    ddlMonthlyIncome.DataBind();
                    ddlMonthlyIncome.Items.Insert(0, new ListItem { Text = "Select Monthly Income", Value = "" });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMonthlyIncome.Message + "\", '');", true);
                }

                Response responseRace = ServicesManager.GetRaceData();
                if (responseRace.IsSuccess)
                {
                    List<RaceDTO> raceDTOs = (List<RaceDTO>)responseRace.Data;
                    race.Items.Clear();
                    race.DataSource = raceDTOs.Where(x => x.SDESC != "NOT AVAILABLE").Select(x => new RaceDTO { SDESC = x.SDESC.Capitalize() }).ToList();
                    race.DataTextField = "SDESC";
                    race.DataValueField = "SDESC";
                    race.DataBind();
                    race.Items.Insert(0, new ListItem { Text = "Select Race", Value = "" });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRace.Message + "\", '');", true);
                }
                //Response responseStatesList = IStatesDefService.GetData(0, 0, false);
                //StatesDef statesDef = new StatesDef();
                //if (responseStatesList.IsSuccess)
                //{
                //    state.Items.Clear();
                //    List<StatesDef> states = (List<StatesDef>)responseStatesList.Data;
                //    if (states.Count > 0)
                //    {
                //        state.DataSource = states;
                //        state.DataTextField = "Name";
                //        state.DataValueField = "Name";
                //        state.DataBind();
                //        state.Items.Insert(0, new ListItem { Text = "Select State", Value = "" });
                //    }
                //}

                //----------------------------------------------------------------------------Country Details------------------------------------------------------------------------------------------------------
                Response responseCDList = ICountriesDefService.GetData(0, 0, false);
                CountriesDef countriesDef = new CountriesDef();
                if (responseCDList.IsSuccess)
                {
                    country.Items.Clear();
                    List<CountriesDef> countries = (List<CountriesDef>)responseCDList.Data;
                    if (countries.Count > 0)
                    {
                        ListItem brunei = new ListItem { Text = "Brunei", Value = "Brunei" };
                        ListItem singapore = new ListItem { Text = "Singapore", Value = "Singpore" };
                        ListItem malaysia = new ListItem { Text = "Malaysia", Value = "Malaysia" };

                        country.Items.Clear();
                        country.DataSource = countries.Where(x => x.Name != "MALAYSIA" && x.Name != "SINGAPORE" && x.Name != "BRUNEI").Select(x => new CountriesDef { Name = x.Name.Capitalize() }).ToList();
                        country.DataTextField = "Name";
                        country.DataValueField = "Name";
                        country.DataBind();
                        country.Items.Insert(0, brunei);
                        country.Items.Insert(0, singapore);
                        country.Items.Insert(0, malaysia);
                        country.Value = "Malaysia";
                        //countries.IndexOf(countriesDef);
                        //country.Disabled = true;

                        countryM.Items.Clear();
                        countryM.DataSource = countries.Where(x => x.Name != "MALAYSIA" && x.Name != "SINGAPORE" && x.Name != "BRUNEI").Select(x => new CountriesDef { Name = x.Name.Capitalize() }).ToList();
                        countryM.DataTextField = "Name";
                        countryM.DataValueField = "Name";
                        countryM.DataBind();
                        countryM.Items.Insert(0, brunei);
                        countryM.Items.Insert(0, singapore);
                        countryM.Items.Insert(0, malaysia);
                        countryM.Value = "Malaysia";
                        //countryM.Disabled = true;

                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = countries.Where(x => x.Name != "MALAYSIA" && x.Name != "SINGAPORE" && x.Name != "BRUNEI").Select(x => new CountriesDef { Name = x.Name.Capitalize() }).ToList();
                        ddlCountry.DataTextField = "Name";
                        ddlCountry.DataValueField = "Name";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, brunei);
                        ddlCountry.Items.Insert(0, singapore);
                        ddlCountry.Items.Insert(0, malaysia);
                        ddlCountry.Value = "Malaysia";
                        //ddlCountry.Disabled = true;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCDList.Message + "\", '');", true);
                }
                //----------------------------------------------------------------------------Bank Details------------------------------------------------------------------------------------------------------
                Response responseBDList = IBanksDefService.GetDataByFilter(" status = 1 ", 0, 0, true);

                if (!IsPostBack)
                {
                    if (responseBDList.IsSuccess)
                    {
                        bankdefs = (List<BanksDef>)responseBDList.Data;
                        bankdefs = bankdefs.OrderBy(x => x.Name).ToList();
                        foreach (BanksDef x in bankdefs)
                        {
                            ListItem listItem = new ListItem
                            {
                                Text = x.Name,
                                Value = x.Id.ToString()
                            };
                            //listItem.Attributes.Add("accountnumlength", x.NoFormat);
                            ddlBank.Items.Add(listItem);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseBDList.Message + "\", '');", true);
                    }


                }

                //Response responseSDList = IStatesDefService.GetDataByPropertyName(nameof(StatesDef.CountryDefId), "10", true, 0, 0, false);
                //if (responseSDList.IsSuccess)
                //{
                //    List<StatesDef> states = ((List<StatesDef>)responseSDList.Data).OrderBy(a => a.Name).ToList();
                //    state.Items.Clear();
                //    state.DataSource = states;
                //    state.DataTextField = "Name";
                //    state.DataValueField = "Name";
                //    state.DataBind();
                //    state.Items.Insert(0, new ListItem { Text = "Select State", Value = "" });

                //    stateM.Items.Clear();
                //    stateM.DataSource = states;
                //    stateM.DataTextField = "Name";
                //    stateM.DataValueField = "Name";
                //    stateM.DataBind();
                //    stateM.Items.Insert(0, new ListItem { Text = "Select State", Value = "" });

                //    ddlState.Items.Clear();
                //    ddlState.DataSource = states;
                //    ddlState.DataTextField = "Name";
                //    ddlState.DataValueField = "Name";
                //    ddlState.DataBind();
                //    ddlState.Items.Insert(0, new ListItem { Text = "Select State", Value = "" });
                //}

                Response responseOcc = ServicesManager.GetOccupationData();
                if (responseOcc.IsSuccess)
                {
                    List<OccupationDTO> occupationDTOs = (List<OccupationDTO>)responseOcc.Data;
                    ddlOccupation.Items.Clear();
                    ddlOccupation.DataSource = occupationDTOs;
                    ddlOccupation.DataTextField = "OCCCODE";
                    ddlOccupation.DataValueField = "DOWNLOADIND";
                    ddlOccupation.DataBind();
                    ddlOccupation.Items.Insert(0, new ListItem { Text = "Select Occupation", Value = "", Selected = true });

                    //ddlFunderIndustry.Items.Clear();
                    //ddlFunderIndustry.DataSource = occupationDTOs;
                    //ddlFunderIndustry.DataTextField = "OCCCODE";
                    //ddlFunderIndustry.DataValueField = "SDESC";
                    //ddlFunderIndustry.DataBind();
                    //ddlFunderIndustry.Items.Insert(0, new ListItem { Text = "Select Industry", Value = "", Selected = true });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseOcc.Message + "\", '');", true);
                }

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object IsReCaptchaValid(string gRecaptchaResponse)
        {
            string secretKey = "6LfXd58UAAAAACGUfwpdtRDFOlKFHjxCI5JSUeAd";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, gRecaptchaResponse));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            if (status == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object SetIPAddress(string idNo, string mobileNo, string ip)
        {
            Response response = new Response();
            Utility.AccountOpening accountOpening = new Utility.AccountOpening();
            Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idNo + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
            if (responseUList.IsSuccess)
            {
                List<Utility.AccountOpening> accountOpenings = (List<Utility.AccountOpening>)responseUList.Data;
                if (accountOpenings.Count == 1)
                {
                    accountOpening = accountOpenings.FirstOrDefault();
                    accountOpening.RegisterIPAddress = TrackIPAddress.GetUserPublicIP(ip) + ", " + ip;
                    response = IAccountOpeningService.UpdateData(accountOpening);
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Something went wrong.";
                }
            }
            else
                response = responseUList;
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetOccupations()
        {
            Response response = new Response();
            try
            {
                Response responseOcc = ServicesManager.GetOccupationData();
                if (responseOcc.IsSuccess)
                {
                    response.IsSuccess = true;
                    List<OccupationDTO> occupationDTOs = (List<OccupationDTO>)responseOcc.Data;
                    response.Data = occupationDTOs;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = responseOcc.Message;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
            return response;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PhoneVerify(string mobileNo, string idNo, string gRecaptchaResponse, string LocalIPAddress)
        {
            Response response = new Response();
            try
            {
                User sessionUser = (User)HttpContext.Current.Session["user"];
                string relation = HttpContext.Current.Session["JointRelationship"] != null ? HttpContext.Current.Session["JointRelationship"].ToString() : "";
                if (sessionUser == null)
                {
                    string key = keyOrg;
                    if (!string.IsNullOrEmpty(key))
                    {
                        key = key.Replace(" ", "+");
                        key = CustomEncryptorDecryptor.DecryptText(key);
                        string date = key.Split('_')[1];
                        DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                        string from = key.Split('_')[2];
                        string accType = key.Split('_')[3];
                        string accPrincipleUserId = key.Split('_')[4];
                        if (from == "admin")
                        {
                            if (accType == "2")
                            {
                                string id = key.Split('_')[0];
                                Response responseA = IAccountOpeningService.GetSingle(Convert.ToInt32(id));
                                if (responseA.IsSuccess)
                                {
                                    Utility.AccountOpening accountOpeningJ = (Utility.AccountOpening)responseA.Data;
                                    if (accountOpeningJ.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                                    {
                                        Response responseU = IUserService.GetSingle(Convert.ToInt32(accPrincipleUserId));
                                        if (responseU.IsSuccess)
                                        {
                                            sessionUser = (User)responseU.Data;
                                            relation = accountOpeningJ.JointRelationship;
                                        }
                                        else
                                        {
                                            return responseU;
                                        }
                                    }
                                }
                                else
                                {
                                    return responseA;
                                }
                            }
                        }
                    }
                }

                String oracleIdNo = "";
                if (idNo.Length == 12)
                {
                    oracleIdNo = idNo.Substring(0, 6) + "-" + idNo.Substring(6, 2) + "-" + idNo.Substring(8, 4);
                }
                else
                {
                    oracleIdNo = idNo; // Current Registering ID
                }

                Response responseIdNo = ServiceCalls.ServicesManager.GetMaHolderRegsByIdNo(oracleIdNo); // To check whether Current Registering ID Exists as either individual ID or Joint secondary holder ID
                if (responseIdNo.IsSuccess)
                {
                    List<MaHolderReg> maHolderRegs = (List<MaHolderReg>)responseIdNo.Data;

                    if (HttpContext.Current.Session["IsJointAccountOpening"] == null || Convert.ToInt32(HttpContext.Current.Session["IsJointAccountOpening"].ToString()) == 0) // To check whether current account opening attempt is Joint account opening or individual account opening
                    {
                        //ACCOUNT OPENING IS FOR INDIVIDUAL ACCOUNT
                        MaHolderReg maHolderReg = maHolderRegs.FirstOrDefault(x => CustomValues.GetAccounPlan(x.HolderCls) == "CASH"); // To check there is existing individual account for this account
                        if (maHolderReg != null)
                        {
                            response.Message = "This Id Number has already been registered";
                            return response;
                        }
                    }
                    else
                    {
                        //ACCOUNT OPENING IS FOR A JOINT ACCOUNT
                        List<MaHolderReg> maHolderRegJoints = maHolderRegs.Where(x => CustomValues.GetAccounPlan(x.HolderCls) == "JOINT").ToList(); // To check there is existing joint account for this account
                        if (maHolderRegJoints.Count > 0)
                        {
                            //User primaryUser = (User)HttpContext.Current.Session["user"]; // Session User ID after login with Individual Account
                            MaHolderReg maHolderReg = maHolderRegJoints.FirstOrDefault(x => x.IdNo.Replace("-", "").Replace(" ", "") == sessionUser.IdNo); // To check the primary holder id for the returning joint holder account is equal to current joint holder primary id
                            if (maHolderReg != null)
                            {
                                response.Message = "This Id Number has already been registered as joint with you.";
                                return response;
                            }
                        }

                    }
                    //if (maHolderRegs.Count > 0)
                    //{
                    //    response.Message = "This Id Number has already been registered";
                    //    return response;
                    //}
                    //else


                    if (mobileNo.Length != 9 && mobileNo.Length != 10)
                    {
                        response.IsSuccess = false;
                        response.Message = "Please enter valid Mobile number!";
                        return response;
                    }
                    else
                    {
                        //if ((idNo.All(Char.IsDigit) && idNo.Length != 12))
                        //{
                        //    response.IsSuccess = false;
                        //    response.Message = "NRIC must be 12 digit";
                        //    return response;
                        //}
                        //else
                        //{

                        if ((idNo.All(Char.IsDigit) && idNo.Length == 12))
                        {
                            int minimumAgeLimit = 18;
                            int minimumAge = Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(0, 4)) - minimumAgeLimit;
                            string year = Convert.ToInt32(idNo.Substring(0, 2)) > Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(2, 2)) ? "19" + idNo.Substring(0, 2) : "20" + idNo.Substring(0, 2);
                            //Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(2, 2)) - minimumAgeLimit;
                            //string year = Convert.ToInt32(userYear) < (minimumAge) ? "19" : "20" + idNo.Substring(0, 2);
                            string month = idNo.Substring(2, 2);
                            string day = idNo.Substring(4, 2);
                            DateTime bornDate = Convert.ToDateTime(year + "-" + month + "-" + day);
                            if ((Convert.ToInt32(year) > (minimumAge)) || (Convert.ToInt32(year) == (minimumAge) && ((bornDate.Month > DateTime.Now.Month))) || (Convert.ToInt32(year) == (minimumAge) && (bornDate.Month == DateTime.Now.Month) && (bornDate.Day > DateTime.Now.Day)))
                            {
                                response.IsSuccess = false;
                                response.Message = "Aged below 18 are not allowed";
                                return response;
                            }
                            else
                            {
                                response.Message = "IC";
                            }
                        }
                        else
                        {
                            response.Message = "IC";
                        }
                        response.IsSuccess = true;
                        //if (mobileFrontNumber == "60") { 
                        mobileNo = "0" + mobileNo;
                        //    }
                        //else
                        if ((bool)IsReCaptchaValid(gRecaptchaResponse))
                        {
                            //mobileNo = mobileFrontNumber + mobileNo;

                            if (HttpContext.Current.Session["IsJointAccountOpening"] == null || Convert.ToInt32(HttpContext.Current.Session["IsJointAccountOpening"].ToString()) == 0)
                            {
                                response = (Response)UserPolicy.RequestMobileVerification(mobileNo, idNo, "Account Opening");
                                Utility.AccountOpening accountOpening = (Utility.AccountOpening)response.Data;
                                HttpContext.Current.Session["AccountOpeningExisting"] = accountOpening;

                            }
                            else
                            {
                                response = (Response)UserPolicy.RequestMobileVerification(mobileNo, idNo, "Account Opening", 2, sessionUser, relation);
                                Utility.AccountOpening accountOpening = (Utility.AccountOpening)response.Data;
                                HttpContext.Current.Session["AccountOpeningExisting"] = accountOpening;
                            }
                            //SetIPAddress(idNo, mobileNo, LocalIPAddress);
                            return response;
                        }
                        else
                        {
                            response.Message = "Please verify yourself!";
                            return response;
                        }
                        //}
                    }

                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = responseIdNo.Message;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object SendEmail(string address, string title, string content, int type, string mobileNo, string idno, string name, int relation)
        {
            User sessionUser = new Utility.User();
            Response response = new Response();
            List<CustomValidation> customValidations = new List<CustomValidation>();
            try
            {
                Utility.AccountOpening accountOpeningExisting = (Utility.AccountOpening)HttpContext.Current.Session["AccountOpeningExisting"];
                if (accountOpeningExisting == null)
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired.";
                }
                else
                {
                    String query = "email_id = '" + address + "'";
                    Response responseUserExists = IUserService.GetDataByFilter(query, 0, 0, false);
                    if (responseUserExists.IsSuccess)
                    {
                        List<User> Users = (List<User>)responseUserExists.Data;
                        if (Users.Count > 0)
                        {
                            response.Message = "This email has been registered";
                            response.IsSuccess = false;
                        }
                        else
                        {
                            String content2 = "";
                            if (Utility.Helper.CustomValidator.IsValidEmail(address))
                            {
                                Email email = new Email();
                                email.user = sessionUser;

                                if (type == 1)
                                {
                                    //String name = "";
                                    if (name != "" && name != null)
                                    {
                                        mobileNo = "0" + mobileNo;
                                        Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                                        if (responseUList.IsSuccess)
                                        {
                                            List<Utility.AccountOpening> accountOpenings = (List<Utility.AccountOpening>)responseUList.Data;
                                            if (accountOpenings.Count > 0) {
                                                Response responseEmail = IAccountOpeningService.GetDataByFilter("email ='" + address + "' and process_status not in (0,29,39,4,49) and status in (0,1) ", 0, 0, false);
                                                if (responseUList.IsSuccess)
                                                {
                                                    List<Utility.AccountOpening> usedEmail = (List<Utility.AccountOpening>)responseEmail.Data;
                                                    if (usedEmail.Count > 0)
                                                    {
                                                        response.Message = "This email has already been submitted for account opening request.";
                                                        response.IsSuccess = false;
                                                        return response;
                                                    }
                                                    else
                                                    {
                                                        if (accountOpenings.FirstOrDefault(x => x.AccountType == accountOpeningExisting.AccountType) != null)
                                                        {
                                                            accountOpeningExisting = accountOpenings.FirstOrDefault(x => x.AccountType == accountOpeningExisting.AccountType);
                                                        }
                                                        else
                                                        {
                                                            accountOpeningExisting = accountOpenings.FirstOrDefault();
                                                        }
                                                        content2 = CustomGenerator.GenerateSixDigitPin();
                                                        accountOpeningExisting.Email = address;
                                                        accountOpeningExisting.EmailVerificationCode = Convert.ToInt32(content2);
                                                        accountOpeningExisting.IsEmailRequested = 1;
                                                        accountOpeningExisting.IsEmailVerified = 0;
                                                        accountOpeningExisting.EmailCodeSentDate = DateTime.Now;
                                                        response = IAccountOpeningService.UpdateData(accountOpeningExisting);
                                                        //name = accountOpeningExisting.Name;
                                                        if (response.IsSuccess)
                                                        {
                                                            //accountOpening = (Utility.AccountOpening)responsePost.Data;
                                                            //response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                                                        }
                                                        else
                                                        {
                                                            return response;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    response.IsSuccess = false;
                                                    return response;
                                                }
                                            }
                                            else
                                            {
                                                response.Message = "No account found.";
                                                response.IsSuccess = false;
                                                return response;
                                            }
                                        }
                                        email.user.Username = name;
                                    }
                                    else
                                    {
                                        customValidations.Add(new CustomValidation { Name = "fullName", Message = "Please fill." });
                                        response.Message = "Please enter name to send email";
                                        response.IsSuccess = false;
                                        return response;
                                    }
                                }
                                else if (type == 2)
                                {
                                    Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                                    if (responseUList.IsSuccess)
                                    {
                                        List<Utility.AccountOpening> accountOpenings = (List<Utility.AccountOpening>)responseUList.Data;
                                        if (accountOpenings.Count == 1)
                                        {
                                            accountOpeningExisting = accountOpenings.FirstOrDefault();
                                            content2 = "";
                                            email.user.Username = accountOpeningExisting.Name;
                                            //if (response.IsSuccess)
                                            //{
                                            //    //accountOpening = (Utility.AccountOpening)responsePost.Data;
                                            //    //response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                                            //}
                                            //else
                                            //{
                                            //}
                                        }
                                        else
                                        {
                                            //Nothing to do
                                        }
                                    }
                                    else
                                    {
                                        return responseUList;
                                    }
                                }
                                else if (type == 4)
                                {
                                    Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                                    if (responseUList.IsSuccess)
                                    {
                                        List<Utility.AccountOpening> accountOpenings = (List<Utility.AccountOpening>)responseUList.Data;
                                        if (accountOpenings.Count == 1)
                                        {
                                            accountOpeningExisting = accountOpenings.FirstOrDefault();
                                            content2 = "";
                                            email.user.Username = accountOpeningExisting.Name;
                                            //if (response.IsSuccess)
                                            //{
                                            //    //accountOpening = (Utility.AccountOpening)responsePost.Data;
                                            //    //response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                                            //}
                                            //else
                                            //{
                                            //}
                                        }
                                        else
                                        {
                                            //Nothing to do
                                        }
                                    }
                                    else
                                    {
                                        return responseUList;
                                    }
                                }
                                email.user.EmailId = address;
                                bool send = EmailService.SendAccountOpeningMail(email, title, content, DateTime.Now.ToString(), content2, type, relation);
                                if (send == true)
                                {
                                    response.Message = "sent" + "," + (Convert.ToInt32("60"));
                                }
                                else
                                {
                                    response.Message = "failed" + "," + (Convert.ToInt32("60"));
                                }
                            }
                            else
                            {
                                response.Message = "Invalid Email";
                            }
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseUserExists.Message;
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Account Opening Page SendEmail: " + ex.Message);
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetBankAccNoLength(Int32 bankId)
        {
            Response response = new Response();
            try
            {
                Utility.AccountOpening sessionUser = (Utility.AccountOpening)HttpContext.Current.Session["AccountOpeningExisting"];
                if (sessionUser == null)
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired.";
                    return response;
                }
                else
                { 
                    BanksDef banksDef = bankdefs.FirstOrDefault(x => x.Id == bankId);
                    string accNoLength = banksDef.NoFormat;
                    response.IsSuccess = true;
                    response.Data = accNoLength;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object btnNext_Click(List<NameValuePairs> formInputs, String IPAddress)
        {
            Response response = new Response() { IsSuccess = true };
            Utility.AccountOpening accountOpening = new Utility.AccountOpening();
            Utility.AccountOpening accountOpeningExisting = new Utility.AccountOpening();
            List<CustomValidation> customValidations = new List<CustomValidation>();
            if (HttpContext.Current.Session["AccountOpeningExisting"] != null)
            {
                accountOpeningExisting = (Utility.AccountOpening)HttpContext.Current.Session["AccountOpeningExisting"];
                Response responseU = IAccountOpeningService.GetSingle(accountOpeningExisting.Id); //This is to get latest data (update data)
                if (responseU.IsSuccess)
                {
                    accountOpeningExisting = (Utility.AccountOpening)responseU.Data;
                    //if(accountOpeningExisting.ProcessStatus != (int)AOProcessStatus.Incomplete || accountOpeningExisting.ProcessStatus != (int)AOProcessStatus.Completed)
                    //{
                    //    response.IsSuccess = false;
                    //    response.Message = "This ID number has already submitted.";
                    //}
                }
                else
                {
                    return responseU;
                }
            }

            var BtnClickedId = formInputs.FirstOrDefault(x => x.name == "hdnBtnClickedId").value;
            if (BtnClickedId == "btnNextStep0" || BtnClickedId == "btnNextStep1")
            {

            }
            else
            {
                if (HttpContext.Current.Session["AccountOpeningExisting"] == null)
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired.";
                    return response;
                }
            }

            //var BtnClickedId = HttpContext.Current.Request.QueryString["hdnBtnClickedId"];
            if (BtnClickedId == "btnNextStep1")
            {
                var USCGPNo = formInputs.FirstOrDefault(x => x.name == "USCGPNo") != null ? formInputs.FirstOrDefault(x => x.name == "USCGPNo").value : null;
                //var USCGPNo = HttpContext.Current.Request.QueryString["USCGPNo"];
                if (USCGPNo == null && accountOpeningExisting.ProcessStatus != (Int32)AOProcessStatus.UnclearDocsRequested)
                {
                    response.IsSuccess = false;
                    response.Message = "Please select";
                    customValidations.Add(new CustomValidation { Name = "checkbox-group-1", Message = "Please select tax residency status" });
                    return response;
                }
                else if (accountOpeningExisting.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                {
                    accountOpening.UsCitizen = accountOpeningExisting.UsCitizen;
                    Response responseFList = IAccountOpeningFileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, true);
                    if (responseFList.IsSuccess)
                    {
                        List<Utility.AccountOpeningFile> accountOpeningFiles = (List<Utility.AccountOpeningFile>)responseFList.Data;
                        accountOpeningExisting.accountOpeningFiles = accountOpeningFiles;
                        response.Data = accountOpeningExisting;
                    }
                    else
                    {
                        return responseFList;
                    }
                    response.Data = accountOpeningExisting;
                }
                else
                {
                    accountOpening.UsCitizen = USCGPNo == "on" ? 0 : 1;
                }
                if (accountOpeningExisting.IsMobileVerified == 0)
                {
                    var s = HttpContext.Current.Session["OTPIsVerified"];
                    var idno = formInputs.FirstOrDefault(x => x.name == "txtNRIC").value;
                    //var idno = HttpContext.Current.Request.QueryString["txtNRIC"];
                    var mobileNo = formInputs.FirstOrDefault(x => x.name == "txtMobileNo").value;
                    //var mobileNo = HttpContext.Current.Request.QueryString["txtMobileNo"];
                    var otpCode = formInputs.FirstOrDefault(x => x.name == "txtOTP").value;
                    var gRecaptchaResponse = formInputs.FirstOrDefault(x => x.name == "gRecaptchaResponse").value;
                    //var otpCode = HttpContext.Current.Request.QueryString["txtOTP"];
                    //_________________________________________________________________________________NULL VALIDATION___________________________________________________________________________
                    if (idno == "" || mobileNo == "" || otpCode == "")
                    {
                        if (idno == "")
                            customValidations.Add(new CustomValidation { Name = "txtNRIC", Message = "NRIC/Passport Number is required" });
                        if (mobileNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtMobileNo", Message = "Your mobile number is required" });
                        if (!(bool)IsReCaptchaValid(gRecaptchaResponse))
                        {
                            customValidations.Add(new CustomValidation { Name = "Div7", Message = "Please verify yourself" });
                        }
                        if (otpCode == "")
                            customValidations.Add(new CustomValidation { Name = "txtOTP", Message = "Please fill in your OTP" });
                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    else if (mobileNo.Length != 9 && mobileNo.Length != 10)
                    {
                        response.IsSuccess = false;
                        response.Message = "Please enter valid Mobile number";
                        customValidations.Add(new CustomValidation { Name = "txtMobileNo", Message = "Please enter valid Mobile number" });
                    }
                    else
                    {
                        //_____________________________________________________________________________NRIC VALIDATION___________________________________________________________________________

                        mobileNo = "0" + mobileNo;
                        Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                        if (responseUList.IsSuccess)
                        {
                            List<Utility.AccountOpening> accountOpenings = (List<Utility.AccountOpening>)responseUList.Data;
                            if (accountOpenings.Count > 0)
                            {
                                int AccountType = 0;
                                if (HttpContext.Current.Session["IsJointAccountOpening"] == null || Convert.ToInt32(HttpContext.Current.Session["IsJointAccountOpening"].ToString()) == 0)
                                {
                                    AccountType = 1;
                                }
                                else
                                {
                                    AccountType = 2;
                                }
                                if (accountOpenings.FirstOrDefault(x => x.AccountType == AccountType) != null)
                                {
                                    accountOpeningExisting = accountOpenings.FirstOrDefault(x => x.AccountType == AccountType);
                                //accountOpeningExisting = accountOpenings.FirstOrDefault();
                                    if (otpCode.Length == 6 && otpCode.All(Char.IsDigit))
                                    {

                                        if (accountOpeningExisting.OtpCode == Convert.ToInt32(otpCode))
                                        {
                                            accountOpeningExisting.UsCitizen = accountOpening.UsCitizen;
                                            accountOpeningExisting.IsMobileVerified = 1;
                                            string dob;
                                            if ((idno.All(Char.IsDigit) && idno.Length == 12) && accountOpeningExisting.Dob == "")
                                            {
                                                string year = Convert.ToInt32(idno.Substring(0, 2)) > Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(2, 2)) ? "19" + idno.Substring(0, 2) : "20" + idno.Substring(0, 2);
                                                string month = idno.Substring(2, 2);
                                                string day = idno.Substring(4, 2);
                                                dob = day + "/" + month + "/" + year;
                                                accountOpeningExisting.Dob = dob;
                                            }
                                            accountOpeningExisting.Status = 1;
                                            HttpContext.Current.Session["AccountOpeningExisting"] = accountOpeningExisting;
                                            response.Data = accountOpeningExisting;
                                            if (accountOpeningExisting.StartedDate == default(DateTime))
                                                accountOpeningExisting.StartedDate = DateTime.Now;
                                            accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                                            if (accountOpeningExisting.LastBtnClicked == "")
                                            {
                                                accountOpeningExisting.LastBtnClicked = BtnClickedId;
                                            }

                                        }
                                        else if (accountOpeningExisting.IsOtpExpired == 1)
                                        {

                                            accountOpeningExisting.IsMobileVerified = 0;
                                            response.IsSuccess = false;
                                            response.Message = "OTP Expired!";
                                        }
                                        else
                                        {
                                            accountOpeningExisting.IsMobileVerified = 0;
                                            response.IsSuccess = false;
                                            response.Message = "Invalid OTP!";
                                            customValidations.Add(new CustomValidation { Name = "txtOTP", Message = "Invalid OTP" });
                                        }
                                    }
                                    else
                                    {
                                        accountOpeningExisting.IsMobileVerified = 0;
                                        response.IsSuccess = false;
                                        response.Message = "Invalid OTP!";
                                        customValidations.Add(new CustomValidation { Name = "txtOTP", Message = "Invalid OTP" });
                                    }
                                }
                                else
                                {
                                    accountOpeningExisting.IsMobileVerified = 0;
                                    response.IsSuccess = false;
                                    customValidations.Add(new CustomValidation { Name = "txtOTP", Message = "No Account Found" });
                                }
                            }
                            else
                            {
                                accountOpeningExisting.IsMobileVerified = 0;
                                response.IsSuccess = false;
                                customValidations.Add(new CustomValidation { Name = "txtOTP", Message = "No Account Found" });
                            }
                        }
                        else
                        {
                            accountOpeningExisting.IsMobileVerified = 0;
                            return responseUList;
                        }
                    }
                    IAccountOpeningService.UpdateData(accountOpeningExisting);

                    if (accountOpeningExisting.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                    {
                        Response responseFList = IAccountOpeningFileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, true);
                        if (responseFList.IsSuccess)
                        {
                            List<Utility.AccountOpeningFile> accountOpeningFiles = (List<Utility.AccountOpeningFile>)responseFList.Data;
                            accountOpeningExisting.accountOpeningFiles = accountOpeningFiles;
                            response.Data = accountOpeningExisting;
                        }
                        else
                        {
                            return responseFList;
                        }
                        response.Data = accountOpeningExisting;
                    }



                }
                else
                {
                }
            }
            else if (BtnClickedId == "btnNextStep0")
            {
                response.IsSuccess = true;
            }
            else if (BtnClickedId == "btnNextStep3")
            {



                var idno = formInputs.FirstOrDefault(x => x.name == "txtNRIC").value;
                //var idno = HttpContext.Current.Request.QueryString["txtNRIC"];
                var mobileNo = formInputs.FirstOrDefault(x => x.name == "txtMobileNo").value;
                //var mobileNo = HttpContext.Current.Request.QueryString["txtMobileNo"];
                var email = formInputs.FirstOrDefault(x => x.name == "email").value;
                //var email = HttpContext.Current.Request.QueryString["email"];
                var emailCode = formInputs.FirstOrDefault(x => x.name == "txtEmailCode").value;
                //var email = HttpContext.Current.Request.QueryString["email"];
                var password = formInputs.FirstOrDefault(x => x.name == "password").value;
                //var password = HttpContext.Current.Request.QueryString["password"];
                var repassword = formInputs.FirstOrDefault(x => x.name == "repassword").value;
                //var repassword = HttpContext.Current.Request.QueryString["repassword"];
                var agent = formInputs.FirstOrDefault(x => x.name == "agent").value;
                //var agent = HttpContext.Current.Request.QueryString["agent"];
                if (email == "" || password == "" || repassword == "")
                {
                    if (email == "")
                        customValidations.Add(new CustomValidation { Name = "email", Message = "Email is required" });
                    if (password == "")
                        customValidations.Add(new CustomValidation { Name = "password", Message = "Please set up your password" });
                    if (repassword == "")
                        customValidations.Add(new CustomValidation { Name = "repassword", Message = "Please re-enter your password" });
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                }
                else if (password != "sample" && password != repassword)
                {
                    response.IsSuccess = false;
                    response.Message = "Passwords doesn't match";
                    customValidations.Add(new CustomValidation { Name = "repassword", Message = "Passwords doesn't match" });
                }
                else
                {
                    var hasUpperChar = password.Any(char.IsUpper);
                    var hasLowerChar = password.Any(char.IsLower);
                    var rule2 = password.Any(char.IsNumber);
                    int rule3 = (password.Length >= 8 && password.Length <= 16) ? 1 : 0;
                    if (rule2 == false)
                    {
                        customValidations.Add(new CustomValidation { Name = "password", Message = "Password must contain at least one number." });
                        response.Message = "Password must contain at least one number.";
                        response.IsSuccess = false;
                        return response;
                    }
                    if (rule3 == 0)
                    {
                        customValidations.Add(new CustomValidation { Name = "password", Message = "Password length must between 8 to 16 characters." });
                        response.Message = "Password length must between 8 to 16 characters.";
                        response.IsSuccess = false;
                        return response;
                    }
                    if (hasUpperChar == false || hasLowerChar == false)
                    {
                        customValidations.Add(new CustomValidation { Name = "password", Message = "Password must contain at least one uppercase alphabet letter and one lowercase alphabet letter." });
                        response.Message = "Password must contain at least one uppercase alphabet letter and one lowercase alphabet letter.";
                        response.IsSuccess = false;
                        return response;
                    }
                    String query = "email_id = '" + email + "'";
                    Response responseUserExists = IUserService.GetDataByFilter(query, 0, 0, false);
                    if (responseUserExists.IsSuccess)
                    {
                        List<User> Users = (List<User>)responseUserExists.Data;
                        if (Users.Count > 0)
                        {
                            response.Message = "This email has been registered";
                            response.IsSuccess = false;
                            customValidations.Add(new CustomValidation { Name = "email", Message = "This email has been registered." });
                            return response;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseUserExists.Message;
                        customValidations.Add(new CustomValidation { Name = "email", Message = "Error while checking email. Please try again." });
                        return response;
                    }
                    mobileNo = "0" + mobileNo;
                    Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        List<Utility.AccountOpening> accountOpenings = (List<Utility.AccountOpening>)responseUList.Data;
                        if (accountOpenings.Count == 1)
                        {
                            accountOpeningExisting = accountOpenings.FirstOrDefault();
                            if (accountOpeningExisting.EmailVerificationCode != 0)
                            {
                                if (accountOpeningExisting.Email == email)
                                {
                                    if (accountOpeningExisting.IsEmailVerified == 0) //_______________________________________________When verifies for the first time
                                    {
                                        if(accountOpeningExisting.IsEmailRequested == 0)
                                        {
                                            customValidations.Add(new CustomValidation { Name = "txtEmailCode", Message = "Please request email code" });
                                            response.IsSuccess = false;
                                            response.Message = "Please request email code!";
                                            return response;
                                        }
                                        else if (emailCode == "")
                                        {
                                            customValidations.Add(new CustomValidation { Name = "txtEmailCode", Message = "Please filln in your email code" });
                                            response.IsSuccess = false;
                                            response.Message = "Please fill email code!";
                                            return response;
                                        }
                                        else
                                        {

                                            if (emailCode.Length == 6 && emailCode.All(Char.IsDigit))
                                            {
                                                if (accountOpeningExisting.EmailVerificationCode == Convert.ToInt32(emailCode))
                                                {
                                                    if (DateTime.Now.Subtract(accountOpeningExisting.EmailCodeSentDate.Value) < TimeSpan.FromHours(24))
                                                    {
                                                        accountOpeningExisting.Email = email;
                                                        accountOpeningExisting.IsEmailVerified = 1;
                                                        accountOpeningExisting.IsEmailRequested = 0;
                                                        accountOpeningExisting.Password = password;
                                                        accountOpeningExisting.AgentCode = agent;
                                                        accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                                                        accountOpeningExisting.LastBtnClicked = BtnClickedId;
                                                        Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                                                        if (responsePost.IsSuccess)
                                                        {
                                                            response.Data = accountOpeningExisting;
                                                        }
                                                        else
                                                        {
                                                            return responsePost;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        response.IsSuccess = false;
                                                        response.Message = "Email Code Expired";
                                                        return response;
                                                    }
                                                }
                                                else
                                                {
                                                    response.IsSuccess = false;
                                                    response.Message = "Wrong Email Verification Code";
                                                    return response;
                                                }
                                            }
                                            else
                                            {
                                                response.IsSuccess = false;
                                                response.Message = "Wrong Email Verification Code";
                                                return response;
                                            }
                                        }

                                    }
                                    else //_____________________________________________________________________________________Verified before
                                    {
                                        accountOpeningExisting.AgentCode = agent;
                                        accountOpeningExisting.Email = email;
                                        accountOpeningExisting.IsEmailVerified = 1;
                                        accountOpeningExisting.IsEmailRequested = 0;
                                        accountOpeningExisting.Password = password;
                                        accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                                        accountOpeningExisting.LastBtnClicked = BtnClickedId;
                                        Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                                        if (responsePost.IsSuccess)
                                        {
                                            response.Data = accountOpeningExisting;
                                        }
                                        else
                                        {
                                            return responsePost;
                                        }
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = "You have updated Email. Please request code again.";
                                    return response;
                                }
                                
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Please request Email Verification Code before proceeding";
                                return response;
                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        return responseUList;
                    }
                }
                var ROMYes = formInputs.FirstOrDefault(x => x.name == "ROMYes") != null ? formInputs.FirstOrDefault(x => x.name == "ROMYes").value : null;
                var ROMNo = formInputs.FirstOrDefault(x => x.name == "ROMNo") != null ? formInputs.FirstOrDefault(x => x.name == "ROMNo").value : null;
                var Malaysian = formInputs.FirstOrDefault(x => x.name == "Malaysian") != null ? formInputs.FirstOrDefault(x => x.name == "Malaysian").value : null;
                var NonMalaysian = formInputs.FirstOrDefault(x => x.name == "NonMalaysian") != null ? formInputs.FirstOrDefault(x => x.name == "NonMalaysian").value : null;
                var title = formInputs.FirstOrDefault(x => x.name == "title").value;
                //var titleOther = formInputs.FirstOrDefault(x => x.name == "titleOther").value;
                var fullName = formInputs.FirstOrDefault(x => x.name == "fullName").value;
                var dob = formInputs.FirstOrDefault(x => x.name == "dob").value;
                var gender = formInputs.FirstOrDefault(x => x.name == "gender").value;
                var race = formInputs.FirstOrDefault(x => x.name == "race").value;
                var raceDesc = formInputs.FirstOrDefault(x => x.name == "txtRaceDesc").value;
                var maritalStatus = formInputs.FirstOrDefault(x => x.name == "maritalStatus").value;
                var bumiputraStatus = formInputs.FirstOrDefault(x => x.name == "bumiputraStatus").value;
                if ((ROMYes == null && ROMNo == null) || (Malaysian == null && NonMalaysian == null) || (title == "" /*|| title == "Others" & titleOther == ""*/) || fullName == "" || dob == "" || gender == "" || race == "" || maritalStatus == "" || bumiputraStatus == "")
                {
                    if (ROMYes == null && ROMNo == null)
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-5", Message = "Yes/No for resident of Malaysia is required" });
                    if (Malaysian == null && NonMalaysian == null)
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-10", Message = "Nationality is required." });
                    if (title == "")
                        customValidations.Add(new CustomValidation { Name = "title", Message = "Salutation is required." });
                    //if(title == "Others" & titleOther == "")
                    //    customValidations.Add(new CustomValidation { Name = "titleOther", Message = "Please fill." });
                    if (fullName == "")
                        customValidations.Add(new CustomValidation { Name = "fullName", Message = "Your full name is required." });
                    if (dob == "")
                        customValidations.Add(new CustomValidation { Name = "dob", Message = "Date of birth is required." });
                    if (gender == "")
                        customValidations.Add(new CustomValidation { Name = "gender", Message = "Gender is required." });
                    if (race == "")
                        customValidations.Add(new CustomValidation { Name = "race", Message = "Race is required." });
                    if (race == "Others" && raceDesc == "")
                        customValidations.Add(new CustomValidation { Name = "txtRaceDesc", Message = "Race description is required." });
                    if (maritalStatus == "")
                        customValidations.Add(new CustomValidation { Name = "maritalStatus", Message = "Marital status is required." });
                    if (bumiputraStatus == "")
                        customValidations.Add(new CustomValidation { Name = "bumiputraStatus", Message = "Bumiputra status is required." });
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                }
                else
                {
                    int minimumAgeLimit = 18;
                    int minimumAge = Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(0, 4)) - minimumAgeLimit;
                    DateTime minimumDate = DateTime.Now.AddYears(-minimumAgeLimit);
                    //Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(2, 2)) - minimumAgeLimit;
                    //string year = Convert.ToInt32(userYear) < (minimumAge) ? "19" : "20" + idNo.Substring(0, 2);
                    DateTime bornDate = DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (bornDate > minimumDate)
                    {
                        response.IsSuccess = false;
                        response.Message = "Aged below 18 are not allowed";
                        return response;
                    }
                    else
                    {
                        accountOpeningExisting.ResidentOfMalaysia = ROMYes == "on" ? 1 : 0;
                        accountOpeningExisting.Tin = "1";
                        accountOpeningExisting.Nationality = Malaysian == "on" ? "Malaysian" : (NonMalaysian == "on" ? "NonMalaysian" : "");
                        accountOpeningExisting.Salutation = title;// == "Others" ? titleOther : title;
                        accountOpeningExisting.Name = fullName;
                        accountOpeningExisting.Password = password;
                        accountOpeningExisting.Dob = dob;
                        accountOpeningExisting.Gender = gender;
                        accountOpeningExisting.Race = race;
                        accountOpeningExisting.RaceDescription = raceDesc;
                        accountOpeningExisting.MaritalStatus = maritalStatus;
                        accountOpeningExisting.BumiputraStatus = bumiputraStatus;
                        accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                        accountOpeningExisting.LastBtnClicked = BtnClickedId;
                        Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                        if (responsePost.IsSuccess)
                        {
                            response.Data = accountOpeningExisting;
                        }
                        else
                        {
                            return responsePost;
                        }

                        //ADDRESS
                        Response responseAOAist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and address_type in (1,2) and status = 1 ", 0, 0, false);
                        if (responseAOAist.IsSuccess)
                        {
                            List<Utility.AccountOpeningAddress> accountOpeningAs = (List<Utility.AccountOpeningAddress>)responseAOAist.Data;
                            accountOpeningAs.ForEach(x =>
                            {
                                if (x.TelNo != null && x.TelNo != "")
                                {
                                    if (x.TelNo.Substring(0, 1) == "0")
                                        x.TelNo = x.TelNo.Substring(1, x.TelNo.Length - 1);
                                }
                            });
                            accountOpeningExisting.accountOpeningAddresses = accountOpeningAs;
                        }
                        else
                        {
                            return responseAOAist;
                        }
                        //ADDRESS
                        response.Data = accountOpeningExisting;
                    }
                }
            }
            else if (BtnClickedId == "btnNextStep5")
            {
                var addr1 = formInputs.FirstOrDefault(x => x.name == "addr1").value;
                //var addr1 = HttpContext.Current.Request.QueryString["addr1"];
                var addr2 = formInputs.FirstOrDefault(x => x.name == "addr2").value;
                //var addr2 = HttpContext.Current.Request.QueryString["addr2"];
                var addr3 = formInputs.FirstOrDefault(x => x.name == "addr3").value;
                //var addr3 = HttpContext.Current.Request.QueryString["addr3"];
                var city = formInputs.FirstOrDefault(x => x.name == "city").value;
                //var city = HttpContext.Current.Request.QueryString["city"];
                var postCode = formInputs.FirstOrDefault(x => x.name == "postCode").value;
                //var postCode = HttpContext.Current.Request.QueryString["postCode"];
                var state = formInputs.FirstOrDefault(x => x.name == "state").value;
                //var state = HttpContext.Current.Request.QueryString["state"];
                var country = formInputs.FirstOrDefault(x => x.name == "country").value;
                //var country = HttpContext.Current.Request.QueryString["country"];
                var homeFrontNumber = formInputs.FirstOrDefault(x => x.name == "homeFrontNumber").value;
                //var homeFrontNumber = HttpContext.Current.Request.QueryString["telNoH"];
                var telNoH = formInputs.FirstOrDefault(x => x.name == "telNoH").value;
                //var telNoH = HttpContext.Current.Request.QueryString["telNoH"];
                var sameAsResidentialAddr = formInputs.FirstOrDefault(x => x.name == "sameAsResidentialAddr") != null ? formInputs.FirstOrDefault(x => x.name == "sameAsResidentialAddr").value : null;
                //var sameAsResidentialAddr = HttpContext.Current.Request.QueryString["sameAsResidentialAddr"];
                var addr1M = formInputs.FirstOrDefault(x => x.name == "addr1M").value;
                //var addr1M = HttpContext.Current.Request.QueryString["addr1M"];
                var addr2M = formInputs.FirstOrDefault(x => x.name == "addr2M").value;
                //var addr2M = HttpContext.Current.Request.QueryString["addr2M"];
                var addr3M = formInputs.FirstOrDefault(x => x.name == "addr3M").value;
                //var addr3M = HttpContext.Current.Request.QueryString["addr3M"];
                var cityM = formInputs.FirstOrDefault(x => x.name == "cityM").value;
                //var cityM = HttpContext.Current.Request.QueryString["cityM"];
                var postCodeM = formInputs.FirstOrDefault(x => x.name == "postCodeM").value;
                //var postCodeM = HttpContext.Current.Request.QueryString["postCodeM"];
                var stateM = formInputs.FirstOrDefault(x => x.name == "stateM").value;
                //var stateM = HttpContext.Current.Request.QueryString["stateM"];
                var countryM = formInputs.FirstOrDefault(x => x.name == "countryM").value;
                //var countryM = HttpContext.Current.Request.QueryString["countryM"];
                if (addr1 == "" || city == "" || postCode == "" || state == "" || country == "" || (sameAsResidentialAddr == null && (addr1M == "" || cityM == "" || postCodeM == "" || stateM == "" || countryM == "")))
                {
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                    if (addr1 == "")
                        customValidations.Add(new CustomValidation { Name = "addr1", Message = "Address is required" });
                    if (city == "")
                        customValidations.Add(new CustomValidation { Name = "city", Message = "City" });
                    if (postCode == "")
                        customValidations.Add(new CustomValidation { Name = "postCode", Message = "Postcode is required" });
                    if (state == "")
                        customValidations.Add(new CustomValidation { Name = "state", Message = "State" });
                    if (country == "")
                        customValidations.Add(new CustomValidation { Name = "country", Message = "Country is required" });
                    //if (telNoH == "")
                    //    customValidations.Add(new CustomValidation { Name = "telNoH", Message = "Telephone number is required" });
                    if (sameAsResidentialAddr == null && addr1M == "")
                        customValidations.Add(new CustomValidation { Name = "addr1M", Message = "Mailing address is required" });
                    if (sameAsResidentialAddr == null && cityM == "")
                        customValidations.Add(new CustomValidation { Name = "cityM", Message = "Mailing city is required" });
                    if (sameAsResidentialAddr == null && postCodeM == "")
                        customValidations.Add(new CustomValidation { Name = "postCodeM", Message = "Please fill mailing postcode" });
                    if (sameAsResidentialAddr == null && stateM == "")
                        customValidations.Add(new CustomValidation { Name = "stateM", Message = "Please fill mailing state" });
                    if (sameAsResidentialAddr == null && countryM == "")
                        customValidations.Add(new CustomValidation { Name = "countryM", Message = "Please fill mailing cocuntry" });
                }
                else
                {
                    if (homeFrontNumber == "60")
                    {
                        telNoH = "0" + telNoH;
                    }
                    else
                    {
                        telNoH = homeFrontNumber + telNoH;
                    }
                    Response responseAOARist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and address_type=1 and status = 1 ", 0, 0, false);
                    if (responseAOARist.IsSuccess)
                    {
                        List<Utility.AccountOpeningAddress> accountOpeningARs = (List<Utility.AccountOpeningAddress>)responseAOARist.Data;
                        AccountOpeningAddress accountOpeningAddressR = new AccountOpeningAddress()
                        {
                            AccountOpeningId = accountOpeningExisting.Id,
                            AddressType = 1,//Residential
                            Addr1 = addr1,
                            Addr2 = addr2,
                            Addr3 = addr3,
                            City = city,
                            PostCode = postCode,
                            State = state,
                            Country = country,
                            TelNo = telNoH,
                            Status = 1
                        };
                        if (accountOpeningARs.Count == 0)
                        {
                            Response responsePost = IAccountOpeningAddressService.PostData(accountOpeningAddressR);
                            if (responsePost.IsSuccess)
                            {
                                response.Data = accountOpeningExisting;
                            }
                            else
                            {
                                return responsePost;
                            }
                        }
                        if (accountOpeningARs.Count == 1)
                        {
                            accountOpeningAddressR.Id = accountOpeningARs.FirstOrDefault().Id;
                            Response responsePost = IAccountOpeningAddressService.UpdateData(accountOpeningAddressR);
                            if (responsePost.IsSuccess)
                            {
                                response.Data = accountOpeningExisting;
                            }
                            else
                            {
                                return responsePost;
                            }
                        }

                        Response responseAOAMist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and address_type=2 and status = 1 ", 0, 0, false);
                        if (responseAOAMist.IsSuccess)
                        {
                            List<Utility.AccountOpeningAddress> accountOpeningAMs = (List<Utility.AccountOpeningAddress>)responseAOAMist.Data;
                            AccountOpeningAddress accountOpeningAddressM = new AccountOpeningAddress()
                            {
                                AccountOpeningId = accountOpeningExisting.Id,
                                AddressType = 2,//Mailing
                                Addr1 = addr1M,
                                Addr2 = addr2M,
                                Addr3 = addr3M,
                                City = cityM,
                                PostCode = postCodeM,
                                State = stateM,
                                Country = countryM,
                                TelNo = "",
                                Status = 1
                            };
                            if (accountOpeningAMs.Count == 0)
                            {
                                Response responsePostA = IAccountOpeningAddressService.PostData(accountOpeningAddressM);
                                if (responsePostA.IsSuccess)
                                {
                                }
                                else
                                {
                                    return responsePostA;
                                }
                            }
                            if (accountOpeningAMs.Count == 1)
                            {
                                accountOpeningAddressM.Id = accountOpeningAMs.FirstOrDefault().Id;
                                Response responsePostA = IAccountOpeningAddressService.UpdateData(accountOpeningAddressM);
                                if (responsePostA.IsSuccess)
                                {
                                }
                                else
                                {
                                    return responsePostA;
                                }
                            }
                            accountOpeningExisting.LastBtnClicked = BtnClickedId;
                            Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }

                            //OCCUPATION
                            Response responseOist = IAccountOpeningOccupationService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                            if (responseOist.IsSuccess)
                            {
                                List<Utility.AccountOpeningOccupation> accountOpeningOccupations = (List<Utility.AccountOpeningOccupation>)responseOist.Data;
                                if (accountOpeningOccupations.Count == 1)
                                {
                                    accountOpeningExisting.AccountOpeningOccupation = accountOpeningOccupations.FirstOrDefault();
                                    response.Data = accountOpeningExisting;
                                }

                                //ADDRESS
                                Response responseAOAist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and address_type in (3) and status = 1 ", 0, 0, false);
                                if (responseAOAist.IsSuccess)
                                {
                                    List<Utility.AccountOpeningAddress> accountOpeningAs = (List<Utility.AccountOpeningAddress>)responseAOAist.Data;
                                    if (accountOpeningExisting.accountOpeningAddresses != null && accountOpeningAs.Count == 1)
                                    {
                                        if (accountOpeningExisting.accountOpeningAddresses.FirstOrDefault(x => x.AddressType == 3) == null)
                                        {
                                            accountOpeningExisting.accountOpeningAddresses.Add(accountOpeningAs.FirstOrDefault());
                                            accountOpeningExisting.accountOpeningAddresses.ForEach(x =>
                                            {

                                                if (x.TelNo.Substring(0, 1) == "0")
                                                    x.TelNo = x.TelNo.Substring(1, x.TelNo.Length - 1);
                                            });

                                        }

                                        response.Data = accountOpeningExisting;
                                    }
                                    else
                                    {

                                        accountOpeningAs.ForEach(x =>
                                        {
                                            if (x.TelNo.Substring(0, 1) == "0")
                                                x.TelNo = x.TelNo.Substring(1, x.TelNo.Length - 1);
                                        });
                                        accountOpeningExisting.accountOpeningAddresses = accountOpeningAs;
                                    }
                                }
                                else
                                {
                                    return responseAOAist;
                                }
                            }
                            else
                            {
                                return responseOist;
                            }

                            //ADDRESS
                            //OCCUPATION
                        }
                        else
                        {
                            return responseAOAMist;
                        }
                    }
                    else
                    {
                        return responseAOARist;
                    }


                }
            }
            else if (BtnClickedId == "btnNextStep6")
            {
                var Employed = formInputs.FirstOrDefault(x => x.name == "Employed") != null ? formInputs.FirstOrDefault(x => x.name == "Employed").value : null;
                //var Employed = HttpContext.Current.Request.QueryString["Employed"];
                var Unemployed = formInputs.FirstOrDefault(x => x.name == "Unemployed") != null ? formInputs.FirstOrDefault(x => x.name == "Unemployed").value : null;
                //var Unemployed = HttpContext.Current.Request.QueryString["Unemployed"];
                var ddlOccupation = formInputs.FirstOrDefault(x => x.name == "ddlOccupation").value;
                //var ddlOccupation = HttpContext.Current.Request.QueryString["ddlOccupation"];
                var txtOccupationDesc = formInputs.FirstOrDefault(x => x.name == "txtOccupationDesc").value;
                //var txtOccupationDesc = HttpContext.Current.Request.QueryString["txtOccupationDesc"];
                var officeFrontNumber = formInputs.FirstOrDefault(x => x.name == "officeFrontNumber").value;
                //var homeFrontNumber = HttpContext.Current.Request.QueryString["telNoH"];
                var txtEmployerName = formInputs.FirstOrDefault(x => x.name == "txtEmployerName").value;
                //var txtEmployerName = HttpContext.Current.Request.QueryString["txtEmployerName"];
                var ddlNatureOfBusiness = formInputs.FirstOrDefault(x => x.name == "ddlNatureOfBusiness").value;
                //var ddlNatureOfBusiness = HttpContext.Current.Request.QueryString["ddlNatureOfBusiness"];
                var ddlMonthlyIncome = formInputs.FirstOrDefault(x => x.name == "ddlMonthlyIncome").value;
                //var ddlMonthlyIncome = HttpContext.Current.Request.QueryString["ddlMonthlyIncome"];
                var txtOfficeAddress1 = formInputs.FirstOrDefault(x => x.name == "txtOfficeAddress1").value;
                //var addr1 = HttpContext.Current.Request.QueryString["txtOfficeAddress1"];
                var txtOfficeAddress2 = formInputs.FirstOrDefault(x => x.name == "txtOfficeAddress2").value;
                //var addr2 = HttpContext.Current.Request.QueryString["txtOfficeAddress2"];
                var txtOfficeAddress3 = formInputs.FirstOrDefault(x => x.name == "txtOfficeAddress3").value;
                //var addr3 = HttpContext.Current.Request.QueryString["txtOfficeAddress3"];
                var txtPostCode = formInputs.FirstOrDefault(x => x.name == "txtPostCode").value;
                //var postCode = HttpContext.Current.Request.QueryString["txtPostCode"];
                var txtCity = formInputs.FirstOrDefault(x => x.name == "txtCity").value;
                //var city = HttpContext.Current.Request.QueryString["txtCity"];
                var ddlState = formInputs.FirstOrDefault(x => x.name == "ddlState").value;
                //var state = HttpContext.Current.Request.QueryString["ddlState"];
                var ddlCountry = formInputs.FirstOrDefault(x => x.name == "ddlCountry").value;
                //var country = HttpContext.Current.Request.QueryString["ddlCountry"];
                var txtTelephone = formInputs.FirstOrDefault(x => x.name == "txtTelephone").value;
                //var telNoO = HttpContext.Current.Request.QueryString["txtTelephone"];

                if (
                    (Employed == null && Unemployed == null)
                    ||
                    ddlOccupation == "" ||
                    (ddlOccupation == "Others" && txtOccupationDesc == "") ||
                        (
                            Employed != null &&
                            (
                                txtEmployerName == "" || ddlNatureOfBusiness == "" || ddlMonthlyIncome == "" || txtOfficeAddress1 == "" || txtCity == "" || txtPostCode == "" || ddlState == "" || ddlCountry == ""
                            )
                        )
                    )
                {
                    if (Employed == null && Unemployed == null)
                    {
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-2", Message = "Employment status is required" });
                    }
                    if (ddlOccupation == "")
                    {
                        if (Employed != null)
                            customValidations.Add(new CustomValidation { Name = "ddlOccupation", Message = "Occupation is required" });
                        else if (Unemployed != null)
                            customValidations.Add(new CustomValidation { Name = "ddlOccupation", Message = "Status is required" });
                    }
                    if (ddlOccupation == "Others" && txtOccupationDesc == "")
                        customValidations.Add(new CustomValidation { Name = "txtOccupationDesc", Message = "Occupation description is required" });
                    if (Employed != null && txtEmployerName == "")
                        customValidations.Add(new CustomValidation { Name = "txtEmployerName", Message = "Employer name is required" });
                    if (Employed != null && (ddlNatureOfBusiness == "" || ddlNatureOfBusiness == "Select"))
                        customValidations.Add(new CustomValidation { Name = "ddlNatureOfBusiness", Message = "Nature of business is required" });
                    if (Employed != null && (ddlMonthlyIncome == "" || ddlMonthlyIncome == "Select"))
                        customValidations.Add(new CustomValidation { Name = "ddlMonthlyIncome", Message = "Monthly income is required" });
                    if (Employed != null && txtOfficeAddress1 == "")
                        customValidations.Add(new CustomValidation { Name = "txtOfficeAddress1", Message = "Office address is required" });
                    if (Employed != null && txtCity == "")
                        customValidations.Add(new CustomValidation { Name = "txtCity", Message = "Office city is required" });
                    if (Employed != null && txtPostCode == "")
                        customValidations.Add(new CustomValidation { Name = "txtPostCode", Message = "Office post code is required" });
                    if (Employed != null && ddlState == "")
                        customValidations.Add(new CustomValidation { Name = "ddlState", Message = "Office state is required" });
                    if (Employed != null && ddlCountry == "")
                        customValidations.Add(new CustomValidation { Name = "ddlCountry", Message = "Office country is required" });
                    //if (Employed != null && txtTelephone == "")
                    //    customValidations.Add(new CustomValidation { Name = "txtTelephone", Message = "Office telephone number is required" });
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                }
                else if (Unemployed != null)
                {
                    Response responseAOOist = IAccountOpeningOccupationService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                    if (responseAOOist.IsSuccess)
                    {
                        List<Utility.AccountOpeningOccupation> accountOpeningOs = (List<Utility.AccountOpeningOccupation>)responseAOOist.Data;
                        AccountOpeningOccupation accountOpeningO = new AccountOpeningOccupation()
                        {
                            AccountOpeningId = accountOpeningExisting.Id,
                            Employed = (Employed == "on" ? 1 : 0),
                            Occupation = ddlOccupation,
                            OccupationOthers = txtOccupationDesc,
                            Status = 1
                        };
                        if (accountOpeningOs.Count == 0)
                        {
                            Response responsePostO = IAccountOpeningOccupationService.PostData(accountOpeningO);
                            if (responsePostO.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePostO;
                            }

                        }
                        if (accountOpeningOs.Count == 1)
                        {
                            accountOpeningO.Id = accountOpeningOs.FirstOrDefault().Id;
                            Response responsePostO = IAccountOpeningOccupationService.UpdateData(accountOpeningO);
                            if (responsePostO.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePostO;
                            }
                        }
                        //if (ddlOccupation.ToString().Substring(0, 2) == "H-")
                        //{

                        //        accountOpeningExisting.IsAdditionalRequired = 1;
                        //}
                        //else
                        {
                            accountOpeningExisting.IsAdditionalRequired = 0;

                        }
                        //if (ddlNatureOfBusiness.ToString() == "Business in Low Density Goods (e.g: Moblie Phones/Clothing)" || ddlNatureOfBusiness.ToString() == "Cash Intensive Business (e.g: Restaurant/Convenient Store)" || ddlNatureOfBusiness.ToString() == "Casino/Betting/Gambling Related" || ddlNatureOfBusiness.ToString() == "Entertainment Outlets/Karaoke/Spa Massage/Internet Café" || ddlNatureOfBusiness.ToString() == "Money Services Business (e.g. Remittance Agent/Non-Bank Money Lender)" || ddlNatureOfBusiness.ToString() == "Non-profit organization/Charity" || ddlNatureOfBusiness.ToString() == "Offshore Banking/Offshore Trust" || ddlNatureOfBusiness.ToString() == "Pawn Brokers/Stock Brokers" || ddlNatureOfBusiness.ToString() == "Precious Stone/Metal Dealer (e.g. Gold/Jewelry)" || ddlNatureOfBusiness.ToString() == "Real Estate/Property" || ddlNatureOfBusiness.ToString() == "Import/Export Companies" || ddlNatureOfBusiness.ToString() == "Military Transactions" || ddlNatureOfBusiness.ToString() == "Used automobile/truck/machines part dealer")
                        //{
                        //    accountOpeningExisting.IsAdditionalRequired = 1;
                        //}
                        //else
                        //{

                        //}
                        accountOpeningExisting.LastBtnClicked = BtnClickedId;
                        Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                        if (responsePost.IsSuccess)
                        {
                        }
                        else
                        {
                            return responsePost;
                        }

                        Response responseFPList = IAccountOpeningFinancialProfileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                        if (responseFPList.IsSuccess)
                        {
                            List<Utility.AccountOpeningFinancialProfile> accountOpeningFinancialProfiles = (List<Utility.AccountOpeningFinancialProfile>)responseFPList.Data;
                            if (accountOpeningFinancialProfiles.Count == 1)
                            {
                                accountOpeningExisting.AccountOpeningFinancialProfile = accountOpeningFinancialProfiles.FirstOrDefault();
                                response.Data = accountOpeningExisting;
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            return responseFPList;
                        }
                    }
                    else
                    {
                        return responseAOOist;
                    }
                }
                else
                {
                    Response responseAOOist = IAccountOpeningOccupationService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                    if (responseAOOist.IsSuccess)
                    {
                        List<Utility.AccountOpeningOccupation> accountOpeningOs = (List<Utility.AccountOpeningOccupation>)responseAOOist.Data;
                        AccountOpeningOccupation accountOpeningO = new AccountOpeningOccupation()
                        {
                            AccountOpeningId = accountOpeningExisting.Id,
                            Employed = (Employed == "on" ? 1 : 0),
                            Occupation = ddlOccupation,
                            OccupationOthers = txtOccupationDesc,
                            EmployerName = txtEmployerName,
                            NatureOfBusiness = ddlNatureOfBusiness,
                            MonthlyIncome = ddlMonthlyIncome,
                            Status = 1
                        };
                        if (accountOpeningOs.Count == 0)
                        {
                            Response responsePost = IAccountOpeningOccupationService.PostData(accountOpeningO);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }

                        }
                        if (accountOpeningOs.Count == 1)
                        {
                            accountOpeningO.Id = accountOpeningOs.FirstOrDefault().Id;
                            Response responsePost = IAccountOpeningOccupationService.UpdateData(accountOpeningO);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }
                        }
                        if (officeFrontNumber == "60")
                        {
                            txtTelephone = "0" + txtTelephone;
                        }
                        else
                        {
                            txtTelephone = officeFrontNumber + txtTelephone;
                        }
                        Response responseAOAOist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and address_type=3 and status = 1 ", 0, 0, false);
                        if (responseAOAOist.IsSuccess)
                        {
                            List<Utility.AccountOpeningAddress> accountOpeningAOs = (List<Utility.AccountOpeningAddress>)responseAOAOist.Data;
                            AccountOpeningAddress accountOpeningAddressO = new AccountOpeningAddress()
                            {
                                AccountOpeningId = accountOpeningExisting.Id,
                                AddressType = 3,//Office
                                Addr1 = txtOfficeAddress1,
                                Addr2 = txtOfficeAddress2,
                                Addr3 = txtOfficeAddress3,
                                City = txtCity,
                                PostCode = txtPostCode == "" ? "0" : txtPostCode,
                                State = ddlState,
                                Country = ddlCountry,
                                TelNo = txtTelephone,
                                Status = 1
                            };
                            if (accountOpeningAOs.Count == 0)
                            {
                                Response responsePostA = IAccountOpeningAddressService.PostData(accountOpeningAddressO);
                                if (responsePostA.IsSuccess)
                                {
                                }
                                else
                                {
                                    return responsePostA;
                                }
                            }
                            if (accountOpeningAOs.Count == 1)
                            {
                                accountOpeningAddressO.Id = accountOpeningAOs.FirstOrDefault().Id;
                                Response responsePostA = IAccountOpeningAddressService.UpdateData(accountOpeningAddressO);
                                if (responsePostA.IsSuccess)
                                {
                                }
                                else
                                {
                                    return responsePostA;
                                }
                            }
                            accountOpeningExisting.LastBtnClicked = BtnClickedId;

                            List<string> additionalNOBList = new List<string> {
                                "Aviation",
                                "Business in Low Density Goods (e.g: Moblie Phones/Clothing)",
                                "Casino/Betting/Gambling Related",
                                "Cash Intensive Business (e.g: Convenient Store)",
                                "Entertainment Outlets/Karaoke/Spa Massage/Internet Café",
                                "Licensed Gaming Outlet",
                                "Gymkhana, Club or Association",
                                "Licensed Money Lending",
                                "Military Transactions",
                                "Money Remittance",
                                "Money Lender",
                                "Money Services Business / Money Changer",
                                "Non-Government Organizarion (NGO) /Charitable body",
                                "Offshore Banking/Offshore Trust/Offshore Corporations",
                                "Pawn Brokers/Stock Brokers",
                                "Precious Stone/Metal Dealer (e.g. Gold/Jewelry)",
                                "Property/Real Estate",
                                "Restaurants / Bar",
                                "Tour and Travel Services",
                                "Used automobile/truck/machines part dealer",
                                "Import/Export Companies",
                                "Professional service providers acting as intermediaries (Lawyer, accountants)"
                            };

                            if (/*(ddlOccupation.ToString().Substring(0, 2) == "H-") || */(additionalNOBList.Contains(ddlNatureOfBusiness.ToString())))
                            {
                                accountOpeningExisting.IsAdditionalRequired = 1;
                            }
                            else
                            {
                                accountOpeningExisting.IsAdditionalRequired = 0;
                            }
                            Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }
                            Response responseFPList = IAccountOpeningFinancialProfileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                            if (responseFPList.IsSuccess)
                            {
                                List<Utility.AccountOpeningFinancialProfile> accountOpeningFinancialProfiles = (List<Utility.AccountOpeningFinancialProfile>)responseFPList.Data;
                                if (accountOpeningFinancialProfiles.Count == 1)
                                {
                                    accountOpeningExisting.AccountOpeningFinancialProfile = accountOpeningFinancialProfiles.FirstOrDefault();
                                    response.Data = accountOpeningExisting;
                                }
                            }
                            else
                            {
                                return responseFPList;
                            }
                        }
                        else
                        {
                            return responseAOAOist;
                        }
                    }
                    else
                    {
                        return responseAOOist;
                    }
                }

            }
            else if (BtnClickedId == "btnNextStep7")
            {
                var ddlPurposeOfTransaction = formInputs.FirstOrDefault(x => x.name == "ddlPurposeOfTransaction").value;
                //var ddlPurposeOfTransaction = HttpContext.Current.Request.QueryString["ddlPurposeOfTransaction"];
                var txtPurposeOfTransaction = formInputs.FirstOrDefault(x => x.name == "txtPurposeOfTransaction").value;
                //var txtPurposeOfTransaction = HttpContext.Current.Request.QueryString["txtPurposeOfTransaction"];
                var ddlSourceOfFunds = formInputs.FirstOrDefault(x => x.name == "ddlSourceOfFunds").value;
                //var ddlSourceOfFunds = HttpContext.Current.Request.QueryString["ddlSourceOfFunds"];
                var ddlRelationship = formInputs.FirstOrDefault(x => x.name == "ddlRelationship").value;
                //var ddlRelationship = HttpContext.Current.Request.QueryString["ddlRelationship"];
                var txtFunderName = formInputs.FirstOrDefault(x => x.name == "txtFunderName").value;
                //var txtFunderName = HttpContext.Current.Request.QueryString["txtFunderName"];
                var ddlFunderIndustry = formInputs.FirstOrDefault(x => x.name == "ddlFunderIndustry").value;
                //var ddlFunderIndustry = HttpContext.Current.Request.QueryString["ddlFunderIndustry"];
                var chkFunderInvolvedYes = formInputs.FirstOrDefault(x => x.name == "chkFunderInvolvedYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkFunderInvolvedYes").value : null;
                //var chkFunderInvolvedYes = HttpContext.Current.Request.QueryString["chkFunderInvolvedYes"];
                var chkFunderInvolvedNo = formInputs.FirstOrDefault(x => x.name == "chkFunderInvolvedNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkFunderInvolvedNo").value : null;
                //var chkFunderInvolvedNo = HttpContext.Current.Request.QueryString["chkFunderInvolvedNo"];
                var chkFunderBeneficialOwnerYes = formInputs.FirstOrDefault(x => x.name == "chkFunderBeneficialOwnerYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkFunderBeneficialOwnerYes").value : null;
                //var chkFunderBeneficialOwnerYes = HttpContext.Current.Request.QueryString["chkFunderBeneficialOwnerYes"];
                var chkFunderBeneficialOwnerNo = formInputs.FirstOrDefault(x => x.name == "chkFunderBeneficialOwnerNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkFunderBeneficialOwnerNo").value : null;
                //var chkFunderBeneficialOwnerNo = HttpContext.Current.Request.QueryString["chkFunderBeneficialOwnerNo"];
                var txtFundOwnerName = formInputs.FirstOrDefault(x => x.name == "txtFundOwnerName").value;
                //var txtFundOwnerName = HttpContext.Current.Request.QueryString["txtFundOwnerName"];
                var ddlEstimatedNetWorth = formInputs.FirstOrDefault(x => x.name == "ddlEstimatedNetWorth").value;
                //var ddlEstimatedNetWorth = HttpContext.Current.Request.QueryString["ddlEstimatedNetWorth"];
                //var txtNetWorth = formInputs.FirstOrDefault(x => x.name == "txtNetWorth").value;
                //var txtNetWorth = HttpContext.Current.Request.QueryString["txtNetWorth"];
                var chkFundManagementCompanyYes = formInputs.FirstOrDefault(x => x.name == "chkFundManagementCompanyYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkFundManagementCompanyYes").value : null;
                //var chkFundManagementCompanyYes = HttpContext.Current.Request.QueryString["chkFundManagementCompanyYes"];
                var chkFundManagementCompanyNo = formInputs.FirstOrDefault(x => x.name == "chkFundManagementCompanyNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkFundManagementCompanyNo").value : null;
                //var chkFundManagementCompanyNo = HttpContext.Current.Request.QueryString["chkFundManagementCompanyNo"];
                //<<<<<<< HEAD

                //if (ddlPurposeOfTransaction == "" || ddlPurposeOfTransaction == "Select" || ddlSourceOfFunds == "" || ddlSourceOfFunds == "Select" || (ddlSourceOfFunds == "Others" && ((ddlRelationship == "" || ddlRelationship == "Select") || txtFunderName == "" || (ddlFunderIndustry == "" || ddlFunderIndustry == "Select" ) || (chkFunderInvolvedYes == null && chkFunderInvolvedNo == null) || (chkFunderBeneficialOwnerYes == null && chkFunderBeneficialOwnerNo == null) || txtFundOwnerName == "")) || ddlEstimatedNetWorth == "" || ddlEstimatedNetWorth == "Select")
                //=======

                if (ddlPurposeOfTransaction == "" || ddlPurposeOfTransaction == "Select" || ddlSourceOfFunds == "Select" || ddlSourceOfFunds == "" || ddlPurposeOfTransaction == "Others" && (txtPurposeOfTransaction == "" || txtPurposeOfTransaction == null) || (ddlSourceOfFunds == "Others" && ((ddlRelationship == "" || ddlRelationship == "Select") || txtFunderName == "" || (ddlFunderIndustry == "" || ddlFunderIndustry == "Select") || (chkFunderInvolvedYes == null && chkFunderInvolvedNo == null) || (chkFunderBeneficialOwnerYes == null && chkFunderBeneficialOwnerNo == null) || txtFundOwnerName == "")) || ddlEstimatedNetWorth == "" || ddlEstimatedNetWorth == "Select")
                //>>>>>>> b6e694bd60b4837ff8a8a0d5f0de96c022a29646
                {
                    if (ddlPurposeOfTransaction == "" || ddlPurposeOfTransaction == "Select")
                        customValidations.Add(new CustomValidation { Name = "ddlPurposeOfTransaction", Message = "Purpose of transaction is required" });
                    if (ddlPurposeOfTransaction == "Others" && (txtPurposeOfTransaction == "" || txtPurposeOfTransaction == null))
                        customValidations.Add(new CustomValidation { Name = "txtPurposeOfTransaction", Message = "Description purpose of transaction" });
                    if (ddlSourceOfFunds == "" || ddlSourceOfFunds == "Select")
                        customValidations.Add(new CustomValidation { Name = "ddlSourceOfFunds", Message = "Source of funds is required" });
                    if (ddlSourceOfFunds == "Others" && (ddlRelationship == "" || ddlRelationship == "Select"))
                        customValidations.Add(new CustomValidation { Name = "ddlRelationship", Message = "Relationship is required" });
                    if (ddlSourceOfFunds == "Others" && txtFunderName == "" || txtFunderName == "Select")
                        customValidations.Add(new CustomValidation { Name = "txtFunderName", Message = "Funder name is required" });
                    if (ddlSourceOfFunds == "Others" && (ddlFunderIndustry == "" || ddlFunderIndustry == "Select"))
                        customValidations.Add(new CustomValidation { Name = "ddlFunderIndustry", Message = "Funder industry is required" });
                    if (ddlSourceOfFunds == "Others" && (chkFunderInvolvedYes == null && chkFunderInvolvedNo == null))
                    {
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-3", Message = "Funder involved is required" });

                    }
                    if (ddlSourceOfFunds == "Others" && (chkFunderBeneficialOwnerYes == null && chkFunderBeneficialOwnerNo == null))
                    {
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-4", Message = "Funder beneficial owner" });
                    }
                    if (ddlSourceOfFunds == "Others" && txtFundOwnerName == "" || txtFunderName == "Select")
                        customValidations.Add(new CustomValidation { Name = "txtFundOwnerName", Message = "Fund owner name is required" });
                    if (ddlEstimatedNetWorth == "" || ddlEstimatedNetWorth == "Select")
                        customValidations.Add(new CustomValidation { Name = "ddlEstimatedNetWorth", Message = "Estimated net worth is required" });
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                }
                else
                {
                    Response responseU = IAccountOpeningService.GetSingle(accountOpeningExisting.Id);
                    if (responseU.IsSuccess)
                    {
                        accountOpeningExisting = (Utility.AccountOpening)responseU.Data;
                    }
                    Response responseAOFPist = IAccountOpeningFinancialProfileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                    if (responseAOFPist.IsSuccess)
                    {
                        List<Utility.AccountOpeningFinancialProfile> accountOpeningFPs = (List<Utility.AccountOpeningFinancialProfile>)responseAOFPist.Data;
                        AccountOpeningFinancialProfile accountOpeningFinancialProfile = new AccountOpeningFinancialProfile()
                        {
                            AccountOpeningId = accountOpeningExisting.Id,
                            Purpose = ddlPurposeOfTransaction,
                            PurposeDescription = txtPurposeOfTransaction,
                            Source = ddlSourceOfFunds,
                            Relationship = ddlRelationship,
                            NameOfFunder = txtFunderName,
                            FundersIndustory = ddlFunderIndustry,
                            IsFunderMoneyChanger = (chkFunderInvolvedYes == "on" ? 1 : 0),
                            IsFunderBeneficialOwner = (chkFunderBeneficialOwnerYes == "on" ? 1 : 0),
                            FundOwnerName = txtFundOwnerName,
                            EstimatedNetWorth = ddlEstimatedNetWorth,
                            EmployedByFundCompany = (chkFundManagementCompanyYes == "on" ? 1 : 0),
                            Status = 1
                        };
                        if (ddlEstimatedNetWorth.ToString() == "Above MYR 3,000,000")
                        {
                            accountOpeningExisting.IsAdditionalRequired2 = 1;
                        }
                        else
                        {
                            accountOpeningExisting.IsAdditionalRequired2 = 0;
                        }
                        accountOpeningExisting.LastBtnClicked = BtnClickedId;
                        Response responsePostAO = IAccountOpeningService.UpdateData(accountOpeningExisting);
                        if (responsePostAO.IsSuccess)
                        {
                        }
                        else
                        {
                            return responsePostAO;
                        }
                        if (accountOpeningFPs.Count == 0)
                        {
                            Response responsePost = IAccountOpeningFinancialProfileService.PostData(accountOpeningFinancialProfile);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }
                        }
                        if (accountOpeningFPs.Count == 1)
                        {
                            accountOpeningFinancialProfile.Id = accountOpeningFPs.FirstOrDefault().Id;
                            Response responsePost = IAccountOpeningFinancialProfileService.UpdateData(accountOpeningFinancialProfile);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }
                        }
                    }

                    if (accountOpeningExisting.AccountType == 1)
                    {


                        Response responseBDList = IAccountOpeningBankDetailService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                        if (responseBDList.IsSuccess)
                        {
                            List<Utility.AccountOpeningBankDetail> accountOpeningBankDetails = (List<Utility.AccountOpeningBankDetail>)responseBDList.Data;
                            if (accountOpeningBankDetails.Count == 1)
                            {
                                accountOpeningExisting.AccountOpeningBankDetail = accountOpeningBankDetails.FirstOrDefault();
                            }
                        }
                        else
                        {
                            return responseBDList;
                        }
                    }
                    else {
                        Response responseAOCRSist = IAccountOpeningCRSDetailService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                        if (responseAOCRSist.IsSuccess)
                        {
                            List<Utility.AccountOpeningCRSDetail> accountOpeningCRSDetails = (List<Utility.AccountOpeningCRSDetail>)responseAOCRSist.Data;
                            accountOpeningExisting.accountOpeningCRSDetails = accountOpeningCRSDetails;
                        }
                        else
                        {
                            return responseAOCRSist;
                        }
                    }
                }
                response.Data = accountOpeningExisting;
            }
            else if (BtnClickedId == "btnNextStep8")
            {
                if (HttpContext.Current.Session["IsJointAccountOpening"] == null || Convert.ToInt32(HttpContext.Current.Session["IsJointAccountOpening"].ToString()) == 0)
                {
                    var chkDepositSalesBankAccountYes = formInputs.FirstOrDefault(x => x.name == "chkDepositSalesBankAccountYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkDepositSalesBankAccountYes").value : null;
                    //var chkDepositSalesBankAccountYes = HttpContext.Current.Request.QueryString["chkDepositSalesBankAccountYes"];
                    var chkDepositSalesBankAccountNo = formInputs.FirstOrDefault(x => x.name == "chkDepositSalesBankAccountNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkDepositSalesBankAccountNo").value : null;
                    //var chkDepositSalesBankAccountNo = HttpContext.Current.Request.QueryString["chkDepositSalesBankAccountNo"];
                    var ddlBankAccountType = formInputs.FirstOrDefault(x => x.name == "ddlBankAccountType").value;
                    //var ddlBankAccountType = HttpContext.Current.Request.QueryString["ddlBankAccountType"];
                    var ddlCurrency = formInputs.FirstOrDefault(x => x.name == "ddlCurrency").value;
                    //var ddlCurrency = HttpContext.Current.Request.QueryString["ddlCurrency"];
                    var ddlBank = formInputs.FirstOrDefault(x => x.name == "ddlBank").value;
                    //string accNoLength = "0";
                    if (ddlBank == "" || ddlBank == "Select Bank")
                    {
                        customValidations.Add(new CustomValidation { Name = "ddlBank", Message = "Bank name is required" });
                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    else
                    {
                        BanksDef banksDef = bankdefs.FirstOrDefault(x => x.Id == Convert.ToInt32(ddlBank));
                        //accNoLength = banksDef.NoFormat;
                    }
                    //var ddlBank = HttpContext.Current.Request.QueryString["ddlBank"];
                    var txtBankAccountName = formInputs.FirstOrDefault(x => x.name == "txtBankAccountName").value;
                    //var txtBankAccountName = HttpContext.Current.Request.QueryString["txtBankAccountName"];
                    var txtBankAccountNo = formInputs.FirstOrDefault(x => x.name == "txtBankAccountNo").value;
                    //var txtBankAccountNo = HttpContext.Current.Request.QueryString["txtBankAccountNo"];
                    //(chkDepositSalesBankAccountYes == null && chkDepositSalesBankAccountNo == null) || 
                    if (txtBankAccountNo != "")
                    {
                        //if (txtBankAccountNo.Length == Convert.ToInt32(accNoLength))
                        {
                            if (ddlBankAccountType == "" || ddlBankAccountType == "Select Account Type" || ddlCurrency == "" || ddlBank == "" || txtBankAccountName == "" || txtBankAccountNo == "")
                            {
                                if (ddlBankAccountType == "" || ddlBankAccountType == "Select Account Type")
                                    customValidations.Add(new CustomValidation { Name = "ddlBankAccountType", Message = "Bank account type required" });
                                if (ddlCurrency == "")
                                    customValidations.Add(new CustomValidation { Name = "ddlCurrency", Message = "Currency is required" });
                                if (ddlBank == "")
                                    customValidations.Add(new CustomValidation { Name = "ddlBank", Message = "Bank name is required" });
                                if (txtBankAccountName == "")
                                    customValidations.Add(new CustomValidation { Name = "txtBankAccountName", Message = "Bank Account Name is required" });
                                if (txtBankAccountNo == "")
                                    customValidations.Add(new CustomValidation { Name = "txtBankAccountNo", Message = "Bank account number is required" });
                                response.IsSuccess = false;
                                response.Message = "Please fill in details";
                            }
                            else
                            {
                                Response responseAOBDist = IAccountOpeningBankDetailService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                                if (responseAOBDist.IsSuccess)
                                {
                                    List<Utility.AccountOpeningBankDetail> accountOpeningBDs = (List<Utility.AccountOpeningBankDetail>)responseAOBDist.Data;
                                    AccountOpeningBankDetail accountOpeningBankDetail = new AccountOpeningBankDetail()
                                    {
                                        AccountOpeningId = accountOpeningExisting.Id,
                                        Agreement = (chkDepositSalesBankAccountYes == "on" ? 1 : 0),
                                        AccountType = ddlBankAccountType,
                                        Currency = ddlCurrency,
                                        BankId = Convert.ToInt32(ddlBank),
                                        AccountName = txtBankAccountName,
                                        AccountNo = txtBankAccountNo,
                                        Status = 1
                                    };
                                    if (accountOpeningBDs.Count == 0)
                                    {
                                        Response responsePostB = IAccountOpeningBankDetailService.PostData(accountOpeningBankDetail);
                                        if (responsePostB.IsSuccess)
                                        {
                                        }
                                        else
                                        {
                                            return responsePostB;
                                        }
                                    }
                                    if (accountOpeningBDs.Count == 1)
                                    {
                                        accountOpeningBankDetail.Id = accountOpeningBDs.FirstOrDefault().Id;
                                        Response responsePostB = IAccountOpeningBankDetailService.UpdateData(accountOpeningBankDetail);
                                        if (responsePostB.IsSuccess)
                                        {
                                        }
                                        else
                                        {
                                            return responsePostB;
                                        }
                                    }
                                }
                                else
                                {
                                    return responseAOBDist;
                                }
                            }
                            accountOpeningExisting.LastBtnClicked = BtnClickedId;
                            Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                            if (responsePost.IsSuccess)
                            {
                            }
                            else
                            {
                                return responsePost;
                            }

                            Response responseAOCRSist = IAccountOpeningCRSDetailService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                            if (responseAOCRSist.IsSuccess)
                            {
                                List<Utility.AccountOpeningCRSDetail> accountOpeningCRSDetails = (List<Utility.AccountOpeningCRSDetail>)responseAOCRSist.Data;
                                accountOpeningExisting.accountOpeningCRSDetails = accountOpeningCRSDetails;
                            }
                            else
                            {
                                return responseAOCRSist;
                            }
                        }
                        //else
                        //{
                        //    customValidations.Add(new CustomValidation { Name = "txtBankAccountNo", Message = "Account Number length should be in format" });
                        //    response.IsSuccess = false;
                        //    response.Message = "Bank Account Number length should be in format";
                        //}
                    }
                    else
                    {
                        customValidations.Add(new CustomValidation { Name = "txtBankAccountNo", Message = "Account Number is required" });
                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                }
            }
            else if (BtnClickedId == "btnNextStep9")
            {
                var chkPEPYes = formInputs.FirstOrDefault(x => x.name == "chkPEPYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkPEPYes").value : null;
                //var chkPEPYes = HttpContext.Current.Request.QueryString["chkPEPYes"];
                var chkPEPNo = formInputs.FirstOrDefault(x => x.name == "chkPEPNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkPEPNo").value : null;
                //var chkPEPNo = HttpContext.Current.Request.QueryString["chkPEPNo"];
                var txtPEPSpecify = formInputs.FirstOrDefault(x => x.name == "txtPEPSpecify").value;
                //var txtPEPSpecify = HttpContext.Current.Request.QueryString["txtPEPSpecify"];
                var txtPEPFamilyM = formInputs.FirstOrDefault(x => x.name == "txtPEPFamilyM").value;
                //var txtPEPFamilyM = HttpContext.Current.Request.QueryString["txtPEPFamilyM"];

                if ((chkPEPYes == null && chkPEPNo == null) || (chkPEPYes != null && (txtPEPSpecify == "")))
                {
                    if ((chkPEPYes == null && chkPEPNo == null))
                    {
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-8", Message = "Yes/No is required for PEP status" });
                    }
                    if ((chkPEPYes != null && (txtPEPSpecify == "")))
                    {
                        customValidations.Add(new CustomValidation { Name = "txtPEPSpecify", Message = "Please specify your PEP Status" });
                        //customValidations.Add(new CustomValidation { Name = "txtPEPFamilyM", Message = "PEP Family member relationship is required" });
                    }
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                }
                else
                {
                    accountOpeningExisting.AreYouPep = (chkPEPYes == "on" ? 1 : 0);
                    accountOpeningExisting.PepStatus = txtPEPSpecify;
                    accountOpeningExisting.FPepStatus = txtPEPFamilyM;
                    accountOpeningExisting.TaxResidencyStatus = "1";
                    accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                    accountOpeningExisting.LastBtnClicked = BtnClickedId;
                    if (accountOpeningExisting.IsAdditionalRequired == 1 || accountOpeningExisting.IsAdditionalRequired2 == 1)
                        accountOpeningExisting.IsAdditionalRequired = 1;
                    else
                        accountOpeningExisting.IsAdditionalRequired = 0;

                    Response responsePostA = IAccountOpeningService.UpdateData(accountOpeningExisting);
                    if (responsePostA.IsSuccess)
                    {
                        response.Data = accountOpeningExisting;
                    }
                    else
                    {
                        return responsePostA;
                    }
                }
                var chkCRSYes = formInputs.FirstOrDefault(x => x.name == "chkCRSYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkCRSYes").value : null;
                //var chkCRSYes = HttpContext.Current.Request.QueryString["chkCRSYes"];
                var chkCRSNo = formInputs.FirstOrDefault(x => x.name == "chkCRSNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkCRSNo").value : null;
                //var chkCRSNo = HttpContext.Current.Request.QueryString["chkCRSNo"];
                var ddlTaxResidencyStatuses = formInputs.Where(x => x.name == "ddlTaxResidencyStatus").Select(x => x.value).ToList();
                //var ddlTaxResidencyStatus = HttpContext.Current.Request.QueryString["ddlTaxResidencyStatus"];
                var chkNoTINNums = formInputs.Where(x => x.name == "chkNoTINNum").Select(x => x.value).ToList();
                //var chkNoTINNum = HttpContext.Current.Request.QueryString["chkNoTINNum"];
                var noTINNumReason = formInputs.Where(x => x.name == "txtnoTIN").Select(x => x.value).ToList();
                //var noTINNumReason = HttpContext.Current.Request.QueryString["noTINNumReason"];
                var optionBReason = formInputs.Where(x => x.name == "optionBReason").Select(x => x.value).ToList();
                //var optionBReason = HttpContext.Current.Request.QueryString["optionBReason"];
                var txtTINNums = formInputs.Where(x => x.name == "txtTINNum").Select(x => x.value).ToList();
                //var txtTINNum = HttpContext.Current.Request.QueryString["txtTINNum"];
                if ((chkCRSYes == null && chkCRSNo == null) || (chkCRSYes == "on" && (ddlTaxResidencyStatuses.Where(x => x == "").Count() > 0)))
                {
                    if ((chkCRSYes == null && chkCRSNo == null))
                    {
                        customValidations.Add(new CustomValidation { Name = "checkbox-group-9", Message = "CRS status is required" });
                    }
                    if ((chkCRSYes == "on" && (ddlTaxResidencyStatuses.Where(x => x == "").Count() > 0)))
                    {
                        customValidations.Add(new CustomValidation { Name = "ddlTaxResidencyStatus", Message = "Tax residency status is required" });
                        customValidations.Add(new CustomValidation { Name = "txtTINNum", Message = "Please specify" });
                        customValidations.Add(new CustomValidation { Name = "txtnoTIN", Message = "Please specify" });
                        customValidations.Add(new CustomValidation { Name = "optionBReason", Message = "Reason description is required" });
                    }
                    response.IsSuccess = false;
                    response.Message = "Please fill in details";
                }
                else
                {
                    accountOpeningExisting.TaxResidentOutsideMalaysia = (chkCRSYes == "on" ? 1 : 0);
                    if (chkCRSYes == "on")
                    {
                        int idx = 0;
                        ddlTaxResidencyStatuses.ForEach(x =>
                        {
                            if ((chkNoTINNums[idx] == "on" && noTINNumReason[idx] != "") || (chkNoTINNums[idx] == null && txtTINNums[idx] != ""))
                            {
                                if (noTINNumReason[idx] == "The Account Holder is otherwise unable to obtain a TIN or equivalent number")
                                {
                                    if (optionBReason[idx] == null || optionBReason[idx] == "")
                                    {
                                        customValidations.Add(new CustomValidation { Name = "optionBReason", Message = "Reason description is required" });
                                        response.IsSuccess = false;
                                        response.Message = "Please fill in details";

                                    }
                                    else
                                    {
                                        noTINNumReason[idx] += ", " + optionBReason[idx];
                                    }

                                }
                                else
                                {

                                }
                                Response responseAOCRSist = IAccountOpeningCRSDetailService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and tax_residency_status='" + x + "' and status = 1 ", 0, 0, false);
                                if (responseAOCRSist.IsSuccess)
                                {
                                    List<Utility.AccountOpeningCRSDetail> accountOpeningCRSDetails = (List<Utility.AccountOpeningCRSDetail>)responseAOCRSist.Data;
                                    AccountOpeningCRSDetail accountOpeningCRSDetail = new AccountOpeningCRSDetail
                                    {
                                        AccountOpeningId = accountOpeningExisting.Id,
                                        TaxResidencyStatus = x,
                                        Tin = txtTINNums[idx],
                                        NoTinReason = noTINNumReason[idx],
                                        Status = 1
                                    };
                                    if (accountOpeningCRSDetails.Count == 0)
                                    {
                                        IAccountOpeningCRSDetailService.PostData(accountOpeningCRSDetail);
                                    }
                                    else if (accountOpeningCRSDetails.Count == 1)
                                    {
                                        accountOpeningCRSDetail.Id = accountOpeningCRSDetails.FirstOrDefault().Id;
                                        IAccountOpeningCRSDetailService.UpdateData(accountOpeningCRSDetail);
                                    }
                                }
                                else
                                {
                                    response = responseAOCRSist;
                                }
                            }
                            else
                            {
                                if (chkNoTINNums[idx] == null && txtTINNums[idx] == "")
                                    customValidations.Add(new CustomValidation { Name = "txtTINNum", Message = "Please specify" });
                                if (chkNoTINNums[idx] == "on" && noTINNumReason[idx] == "")
                                    customValidations.Add(new CustomValidation { Name = "txtnoTIN", Message = "Please specify" });

                                response.IsSuccess = false;
                                response.Message = "Please fill in details";
                            }
                            idx++;
                        });
                        //accountOpeningExisting.TaxResidencyStatus = ddlTaxResidencyStatus;
                        //accountOpeningExisting.Tin = txtTINNum;
                    }
                    else
                    {
                        //Do Nothing
                    }
                }

                accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                accountOpeningExisting.LastBtnClicked = BtnClickedId;
                Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                if (responsePost.IsSuccess)
                {
                }
                else
                {
                    return responsePost;
                }

                Response responseFList = IAccountOpeningFileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, true);
                if (responseFList.IsSuccess)
                {
                    List<Utility.AccountOpeningFile> accountOpeningFiles = (List<Utility.AccountOpeningFile>)responseFList.Data;
                    accountOpeningExisting.accountOpeningFiles = accountOpeningFiles;
                    response.Data = accountOpeningExisting;
                }
                else
                {
                    return responseFList;
                }
                response.Data = accountOpeningExisting;
            }
            else if (BtnClickedId == "btnNextStep11")
            {
                List<Int32> ReuploadFileTypes = new List<Int32>();
                if (accountOpeningExisting.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                {
                    ReuploadFileTypes = accountOpeningExisting.AdditionalDesc.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }


                //var uploadNRICFrontPage = HttpContext.Current.Request.QueryString["uploadNRICFrontPage"];
                List<string> ValidFileTypes = new List<string> { "data:image/png", "data:image/jpg", "data:image/jpeg", "data:application/pdf" };
                List<string> ValidSelfieFileTypes = new List<string> { "data:image/png", "data:image/jpg", "data:image/jpeg" };
                string path = "/Content/acc-opening-uploads/" + DateTime.Now.ToString("yyyyMMdd") + "/";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(path)))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
                int fileCount = Directory.GetFiles(HttpContext.Current.Server.MapPath(path)).Count();
                fileCount++;

                Response responseAOFist = IAccountOpeningFileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and status = 1 ", 0, 0, false);
                if (responseAOFist.IsSuccess)
                {
                    List<Utility.AccountOpeningFile> accountOpeningFs = (List<Utility.AccountOpeningFile>)responseAOFist.Data;

                    List<FileInput> fileInputs = new List<FileInput>();
                    if (formInputs.FirstOrDefault(x => x.name == "uploadNRICFrontPage") != null) //___________________________________________NRIC FRONT PAGE VALIDATION
                    {
                        var uploadNRICFrontPage = formInputs.FirstOrDefault(x => x.name == "uploadNRICFrontPage").value;
                        if (ValidFileTypes.FirstOrDefault(x => uploadNRICFrontPage.Contains(x)) != null)
                        {
                            var uploadNRICFrontPageFileName = formInputs.FirstOrDefault(x => x.name == "uploadNRICFrontPageFileName").value;

                            var last = uploadNRICFrontPageFileName.LastIndexOf('.');
                            var butLast = uploadNRICFrontPageFileName.Substring(0, last).Replace(".", "");
                            uploadNRICFrontPageFileName = butLast + uploadNRICFrontPageFileName.Substring(last);
                            string uploadNRICFrontPageFileNameNew = uploadNRICFrontPageFileName.Split('.')[0] + fileCount + "." + uploadNRICFrontPageFileName.Split('.')[1];
                            fileCount++;
                            fileInputs.Add(new FileInput
                            {
                                Type = 1,
                                Base64String = uploadNRICFrontPage,
                                FileName = uploadNRICFrontPageFileName,
                                FileNameNew = uploadNRICFrontPageFileNameNew
                            });
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach valid file type for NRIC Front Page";
                            return response;
                        }
                    }
                    else if (accountOpeningFs.FirstOrDefault(x => x.FileType == 1) == null || ReuploadFileTypes.Contains(1))
                    {
                        response.IsSuccess = false;
                        response.Message = "Please attach NRIC Front Page";
                        return response;
                    }
                    if (formInputs.FirstOrDefault(x => x.name == "uploadNRICBackPage") != null) //_________________________NRIC BACK PAGE VALIDATION
                    {
                        var uploadNRICBackPage = formInputs.FirstOrDefault(x => x.name == "uploadNRICBackPage").value;
                        if (ValidFileTypes.FirstOrDefault(x => uploadNRICBackPage.Contains(x)) != null)
                        {
                            var uploadNRICBackPageFileName = formInputs.FirstOrDefault(x => x.name == "uploadNRICBackPageFileName").value;
                            var last = uploadNRICBackPageFileName.LastIndexOf('.');
                            var butLast = uploadNRICBackPageFileName.Substring(0, last).Replace(".", "");
                            uploadNRICBackPageFileName = butLast + uploadNRICBackPageFileName.Substring(last);
                            string uploadNRICBackPageFileNameNew = uploadNRICBackPageFileName.Split('.')[0] + fileCount + "." + uploadNRICBackPageFileName.Split('.')[1];
                            fileCount++;
                            fileInputs.Add(new FileInput { Type = 2, Base64String = uploadNRICBackPage, FileName = uploadNRICBackPageFileName, FileNameNew = uploadNRICBackPageFileNameNew });
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach valid file type for NRIC Back Page";
                            return response;
                        }
                    }
                    else if (accountOpeningFs.FirstOrDefault(x => x.FileType == 2) == null || ReuploadFileTypes.Contains(2))
                    {
                        response.IsSuccess = false;
                        response.Message = "Please attach NRIC Back Page";
                        return response;
                    }
                    if (formInputs.FirstOrDefault(x => x.name == "uploadSelfie") != null) //_____________________________________________UPLOAD SELFIE VALIDATION
                    {
                        var uploadSelfie = formInputs.FirstOrDefault(x => x.name == "uploadSelfie").value;
                        if (uploadSelfie != "")
                        {
                            if (ValidFileTypes.FirstOrDefault(x => uploadSelfie.Contains(x)) != null)
                            {
                                var uploadSelfieFileName = formInputs.FirstOrDefault(x => x.name == "uploadSelfieFileName").value;
                                var last = uploadSelfieFileName.LastIndexOf('.');
                                var butLast = uploadSelfieFileName.Substring(0, last).Replace(".", "");
                                uploadSelfieFileName = butLast + uploadSelfieFileName.Substring(last);
                                string uploadSelfieFileNameNew = uploadSelfieFileName.Split('.')[0] + fileCount + "." + uploadSelfieFileName.Split('.')[1];
                                fileCount++;
                                fileInputs.Add(new FileInput { Type = 3, Base64String = uploadSelfie, FileName = uploadSelfieFileName, FileNameNew = uploadSelfieFileNameNew });
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach valid file type for Selfie";
                                return response;
                            }
                        }
                        else if (accountOpeningFs.FirstOrDefault(x => x.FileType == 3) == null || ReuploadFileTypes.Contains(3))
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach Selfie";
                            return response;
                        }
                    }
                    else if (accountOpeningFs.FirstOrDefault(x => x.FileType == 3) == null || ReuploadFileTypes.Contains(3))
                    {
                        response.IsSuccess = false;
                        response.Message = "Please attach Selfie";
                        return response;
                    }
                    if (formInputs.FirstOrDefault(x => x.name == "uploadSignature") != null) //__________________________________________UPLOAD SIGNATURE VALIDATION
                    {
                        var uploadSignature = formInputs.FirstOrDefault(x => x.name == "uploadSignature").value;
                        if (ValidFileTypes.FirstOrDefault(x => uploadSignature.Contains(x)) != null)
                        {
                            var uploadSignatureFileName = formInputs.FirstOrDefault(x => x.name == "uploadSignatureFileName").value;
                            var last = uploadSignatureFileName.LastIndexOf('.');
                            var butLast = uploadSignatureFileName.Substring(0, last).Replace(".", "");
                            uploadSignatureFileName = butLast + uploadSignatureFileName.Substring(last);
                            string uploadSignatureFileNameNew = uploadSignatureFileName.Split('.')[0] + fileCount + "." + uploadSignatureFileName.Split('.')[1];
                            fileInputs.Add(new FileInput { Type = 4, Base64String = uploadSignature, FileName = uploadSignatureFileName, FileNameNew = uploadSignatureFileNameNew });
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach valid file type for Signature";
                            return response;
                        }
                    }
                    else if (accountOpeningFs.FirstOrDefault(x => x.FileType == 4) == null || ReuploadFileTypes.Contains(4))
                    {
                        response.IsSuccess = false;
                        response.Message = "Please attach Signature";
                        return response;
                    }


                    var chkSignature = formInputs.FirstOrDefault(x => x.name == "chkSignature") != null ? formInputs.FirstOrDefault(x => x.name == "chkSignature").value : null;
                    if (chkSignature == null)
                    {
                        response.IsSuccess = false;
                        response.Message = "Please agree for the purpose of submitting signature";
                        return response;
                    }

                    if (formInputs.FirstOrDefault(x => x.name == "uploadAdditional") != null) //________________________________________________UPLOAD ADDITIONAL VALIDATION
                    {
                        var uploadAdditionals = formInputs.Where(x => x.name.Contains("uploadAdditional") && !x.name.Contains("FileName")).ToList();
                        Boolean isAllValid = true;
                        int idxFile = 0;
                        uploadAdditionals.ForEach(ua =>
                        {
                            var uploadAdditional = ua.value;
                            if (ValidFileTypes.FirstOrDefault(x => uploadAdditional.Contains(x)) != null)
                            {
                                var uploadAdditionalFileName = formInputs.FirstOrDefault(x => x.name == "uploadAdditional" + (idxFile == 0 ? "" : idxFile + "") + "FileName").value;
                                var last = uploadAdditionalFileName.LastIndexOf('.');
                                var butLast = uploadAdditionalFileName.Substring(0, last).Replace(".", "");
                                uploadAdditionalFileName = butLast + uploadAdditionalFileName.Substring(last);
                                string uploadAdditionalFileNameNew = uploadAdditionalFileName.Split('.')[0] + fileCount + "." + uploadAdditionalFileName.Split('.')[1];
                                fileInputs.Add(new FileInput { Type = 5, Base64String = uploadAdditional, FileName = uploadAdditionalFileName, FileNameNew = uploadAdditionalFileNameNew });
                            }
                            else
                                isAllValid = false;
                            idxFile++;
                        });
                        if (!isAllValid)
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach valid file type for Additional Documents";
                            return response;
                        }

                    }
                    else if (accountOpeningExisting.IsAdditionalRequired == 1 && accountOpeningFs.FirstOrDefault(x => x.FileType == 5) == null || ReuploadFileTypes.Contains(5))
                    {
                        response.IsSuccess = false;
                        response.Message = "Please attach Additional Documents";
                        return response;
                    }

                    if (formInputs.FirstOrDefault(x => x.name == "uploadSupporting") != null)
                    {
                        var uploadSupportings = formInputs.Where(x => x.name.Contains("uploadSupporting") && !x.name.Contains("FileName")).ToList();
                        Boolean isAllValid = true;
                        int idxFile = 0;
                        uploadSupportings.ForEach(ua =>
                        {
                            var uploadSupporting = ua.value;
                            if (ValidFileTypes.FirstOrDefault(x => uploadSupporting.Contains(x)) != null)
                            {
                                var uploadSupportingFileName = formInputs.FirstOrDefault(x => x.name == "uploadSupporting" + (idxFile == 0 ? "" : idxFile + "") + "FileName").value;
                                var last = uploadSupportingFileName.LastIndexOf('.');
                                var butLast = uploadSupportingFileName.Substring(0, last).Replace(".", "");
                                uploadSupportingFileName = butLast + uploadSupportingFileName.Substring(last);
                                string uploadSupportingFileNameNew = uploadSupportingFileName.Split('.')[0] + fileCount + "." + uploadSupportingFileName.Split('.')[1];
                                fileInputs.Add(new FileInput { Type = 6, Base64String = uploadSupporting, FileName = uploadSupportingFileName, FileNameNew = uploadSupportingFileNameNew });
                            }
                            else
                                isAllValid = false;
                            idxFile++;
                        });
                        if (!isAllValid)
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach valid file type for supporting documents";
                            return response;
                        }
                    }
                    else if (accountOpeningFs.FirstOrDefault(x => x.FileType == 6) == null || ReuploadFileTypes.Contains(6))
                    {
                        response.IsSuccess = false;
                        response.Message = "Please attach Supporting Documents";
                        return response;
                    }

                    if (fileInputs.Where(x => x.Type == 5).Count() > 0)
                    {
                        accountOpeningFs.Where(x => x.FileType == 5).ToList().ForEach(x =>
                        {
                            x.Status = 0;
                            IAccountOpeningFileService.UpdateData(x);
                        });
                    }
                    fileInputs.ForEach(x =>
                    {
                        string Base64 = x.Base64String.Split(',')[1];
                        byte[] bytes = Convert.FromBase64String(Base64);
                        File.WriteAllBytes(HttpContext.Current.Server.MapPath(path) + x.FileNameNew, bytes);

                        string filePath = path + x.FileNameNew;

                        //Response responseAOFist = IAccountOpeningFileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and file_type='" + x.Type + "' and status = 1 ", 0, 0, false);
                        //if (responseAOFist.IsSuccess)
                        {
                            //List<Utility.AccountOpeningFile> accountOpeningFs = (List<Utility.AccountOpeningFile>)responseAOFist.Data;
                            AccountOpeningFile accountOpeningFile = new AccountOpeningFile
                            {
                                AccountOpeningId = accountOpeningExisting.Id,
                                FileType = x.Type,
                                Name = x.FileNameNew,
                                Url = filePath,
                                Status = 1,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now
                            };
                            if (accountOpeningFs.FirstOrDefault(y => y.FileType == x.Type && y.Status == 1) != null)
                            {
                                AccountOpeningFile accountOpeningFileOld = accountOpeningFs.FirstOrDefault(y => y.FileType == x.Type && y.Status == 1);
                                accountOpeningFileOld.Status = 0;
                                Response responseUpdate = IAccountOpeningFileService.UpdateData(accountOpeningFileOld);
                                if (responseUpdate.IsSuccess)
                                {
                                    Response responsePost = IAccountOpeningFileService.PostData(accountOpeningFile);
                                    if (responsePost.IsSuccess)
                                    {
                                        accountOpeningExisting.LastBtnClicked = BtnClickedId;
                                    }
                                    else
                                    {
                                        response = responsePost;
                                    }
                                }
                                else
                                {
                                    response = responseUpdate;
                                }

                            }
                            else
                            {
                                Response responsePost = IAccountOpeningFileService.PostData(accountOpeningFile);
                                if (responsePost.IsSuccess)
                                {
                                }
                                else
                                {
                                    response = responsePost;
                                }
                            }
                        }
                    });

                }
                else
                {
                    return responseAOFist;
                }
            }
            else if (BtnClickedId == "btnFinish")
            {
                var chkDeclare = formInputs.FirstOrDefault(x => x.name == "chkDeclare") != null ? formInputs.FirstOrDefault(x => x.name == "chkDeclare").value : null;
                var txtreEnterPassword = formInputs.FirstOrDefault(x => x.name == "txtreEnterPassword") != null ? formInputs.FirstOrDefault(x => x.name == "txtreEnterPassword").value : "";
                if (accountOpeningExisting.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                {
                    txtreEnterPassword = accountOpeningExisting.Password;
                }
                if (chkDeclare == null)
                {
                    response.IsSuccess = false;
                    response.Message = "Please agree!";
                }
                else if (txtreEnterPassword == "")
                {
                    response.IsSuccess = false;
                    response.Message = "Please enter password!";
                }
                else if (accountOpeningExisting.Password != txtreEnterPassword)
                {
                    response.IsSuccess = false;
                    response.Message = "Wrong Password!";
                }
                else
                {
                    String reason = "";
                    accountOpeningExisting.Declaration = 1;
                    accountOpeningExisting.SubmittedDate = DateTime.Now;
                    accountOpeningExisting.LastUpdatedDate = DateTime.Now;
                    accountOpeningExisting.LastBtnClicked = BtnClickedId;
                    Boolean IsIDValid = false;
                    Int32 relation = 0;
                    if (accountOpeningExisting.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsRequested)
                    {
                        accountOpeningExisting.ProcessStatus = (Int32)AOProcessStatus.UnclearDocsUploaded;
                        IsIDValid = true;
                    }
                    else
                        accountOpeningExisting.ProcessStatus = (Int32)AOProcessStatus.Completed;
                    //accountOpeningExisting.ProcessStatus = 0;
                    Response responsePost = IAccountOpeningService.UpdateData(accountOpeningExisting);
                    if (responsePost.IsSuccess)
                    {
                    }
                    else
                    {
                        return responsePost;
                    }
                    accountOpening = accountOpeningExisting;
                    //if (accountOpening.ProcessStatus == (Int32)AOProcessStatus.Completed && accountOpening.IsIdentityVerified == 1)
                    //{
                    //    accountOpening.IsIdentityVerified = 0;
                    //}
                    if (accountOpening.ProcessStatus != (Int32)AOProcessStatus.UnclearDocsUploaded && accountOpening.IsIdentityVerified == 0)
                    {

                        //string[] names = accountOpening.Name.Split(' ');
                        //String[] dob = accountOpening.Dob.Split('/');
                        //Int32 DayOfBirth = Convert.ToInt32(dob[0]);
                        //Int32 MonthOfBirth = Convert.ToInt32(dob[1]);
                        //Int32 YearOfBirth = Convert.ToInt32(dob[2]);
                        if (HttpContext.Current.Session["JointRelationship"] != null)
                        {
                            relation = 2;
                        }
                        else
                        {
                            relation = 1;
                        }

                        //Trulioo.Client.V1.Model.PersonInfo personInfo = new Trulioo.Client.V1.Model.PersonInfo
                        //{
                        //    FirstGivenName = names[0],
                        //    FirstSurName = names.Length > 1 ? names[1] : names[0],
                        //    DayOfBirth = DayOfBirth,
                        //    MonthOfBirth = MonthOfBirth,
                        //    YearOfBirth = YearOfBirth
                        //};
                        //List<Trulioo.Client.V1.Model.NationalId> NationalIds = new List<Trulioo.Client.V1.Model.NationalId> { new Trulioo.Client.V1.Model.NationalId { Number = accountOpening.IdNo, Type = "nationalid" } };

                        //Trulioo.Client.V1.Model.Location location = new Trulioo.Client.V1.Model.Location();
                        //ADDRESS
                        //Response responseAOAist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and address_type in (1) and status = 1 ", 0, 0, false);
                        //if (responseAOAist.IsSuccess)
                        //{
                        //    List<Utility.AccountOpeningAddress> accountOpeningAs = (List<Utility.AccountOpeningAddress>)responseAOAist.Data;
                        //    if(accountOpeningAs.Count == 1)
                        //    {
                        //        AccountOpeningAddress accountOpeningAddress = accountOpeningAs.FirstOrDefault();
                        //        location = new Trulioo.Client.V1.Model.Location {
                                    
                        //            PostalCode = accountOpeningAddress.PostCode
                        //        };
                        //    }
                        //}
                        //else
                        //{
                        //    return responseAOAist;
                        //}

                        //Response resVerify = VerifyeKYC(personInfo, location, NationalIds, null);//new Trulioo.Client.V1.Model.Document { }
                        //if (resVerify.IsSuccess)
                        //{
                        //    Trulioo.Client.V1.Model.VerifyResult verifyResult = (Trulioo.Client.V1.Model.VerifyResult)resVerify.Data;
                        //    if (verifyResult != null)
                        //    {
                        //        if (verifyResult.Record.RecordStatus == "match")
                        //        {
                        //            accountOpening.ProcessStatus = (Int32)AOProcessStatus.IDVerified;
                        //            IsIDValid = true;
                        //        }
                        //        else
                        //        {
                        //            accountOpening.ProcessStatus = (Int32)AOProcessStatus.IDVerificationFailed;
                        //            accountOpening.SettlementDate = DateTime.Now;
                        //            IsIDValid = false;
                        //        }


                        //        verifyResult.Record.DatasourceResults.ToList().ForEach(dr =>
                        //        {
                        //            List<string> noMatchFields = dr.DatasourceFields.ToList().Where(d => d.Status != "match").Select(d => d.FieldName).ToList();
                        //            if (noMatchFields.Select(nmf => nmf.Contains("Name")).ToList().Count > 0)
                        //            {
                        //                reason += "'Name' ";
                        //            }
                        //            if (noMatchFields.Select(nmf => nmf.Contains("Birth")).ToList().Count > 0)
                        //            {
                        //                reason += "'Date of Birth' ";
                        //            }
                        //            if (noMatchFields.Select(nmf => nmf.Contains("nationalid")).ToList().Count > 0)
                        //            {
                        //                reason += "'NRIC/Passport' ";
                        //            }
                        //        });

                        //        string result = JsonConvert.SerializeObject(verifyResult);
                        //        if (resVerify.IsSuccess)
                        //        {
                        //            accountOpening.IsIdentityVerified = 1;
                        //            var plainTextBytes = Encoding.UTF8.GetBytes(result);
                        //            string base64String = Convert.ToBase64String(plainTextBytes);
                        //            accountOpening.IdentityStatus = base64String;

                        //            Response responsePostU = IAccountOpeningService.UpdateData(accountOpening);
                        //            if (responsePostU.IsSuccess)
                        //            {
                        //                SetIPAddress(accountOpening.IdNo, accountOpening.MobileNo, IPAddress);
                        //            }
                        //            else
                        //            {
                        //                return responsePostU;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            accountOpening.IsIdentityVerified = 1;
                        //            var plainTextBytes = Encoding.UTF8.GetBytes(result);
                        //            string base64String = Convert.ToBase64String(plainTextBytes);
                        //            accountOpening.IdentityStatus = base64String;

                        //            Response responsePostU = IAccountOpeningService.UpdateData(accountOpening);
                        //            if (responsePostU.IsSuccess)
                        //            {
                        //                SetIPAddress(accountOpening.IdNo, accountOpening.MobileNo, IPAddress);
                        //            }
                        //            else
                        //            {
                        //                return responsePostU;
                        //            }
                        //            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Identity Verification failed.', '');", true);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        response.IsSuccess = false;
                        //        response.Message = "Identity Verification Api Exception";
                        //    }
                        //}
                        //else
                        //{
                        //    response = resVerify;
                        //}
                    }
                    //else if (accountOpening.ProcessStatus != (Int32)AOProcessStatus.UnclearDocsUploaded && accountOpening.IsIdentityVerified == 1)
                    //{
                    //    //Identity Already Verified

                    //    var plainTextBytes = Convert.FromBase64String(accountOpening.IdentityStatus);
                    //    var result = Encoding.UTF8.GetString(plainTextBytes);
                    //    Trulioo.Client.V1.Model.VerifyResult verifyResult = JsonConvert.DeserializeObject<Trulioo.Client.V1.Model.VerifyResult>(result);
                    //    if (verifyResult.Record.RecordStatus == "match")
                    //    {
                    //        accountOpening.ProcessStatus = (Int32)AOProcessStatus.IDVerified;
                    //        IsIDValid = true;
                    //    }
                    //    else
                    //    {

                    //        accountOpening.ProcessStatus = (Int32)AOProcessStatus.IDVerificationFailed;
                    //        accountOpening.SettlementDate = DateTime.Now;
                    //        IsIDValid = false;
                    //    }

                    //    Response responsePostU = IAccountOpeningService.UpdateData(accountOpening);
                    //    if (responsePostU.IsSuccess)
                    //    {
                    //    }
                    //    else
                    //    {
                    //        return responsePostU;
                    //    }
                    //}
                    //if (IsIDValid)
                    //{
                    if (accountOpening.ProcessStatus != (Int32)AOProcessStatus.UnclearDocsUploaded)
                    {
                        SendEmail(accountOpeningExisting.Email, "", "", 2, accountOpeningExisting.MobileNo, accountOpeningExisting.IdNo, "", relation);
                        response.Message = "Thank you for applying a new Unit Trust Investment Account!<br/> A confirmation email has been sent to you.";
                    }
                    else if (accountOpening.ProcessStatus == (Int32)AOProcessStatus.UnclearDocsUploaded)
                    {
                        response.Message = "Thank you for uploading unclear documents as requested.";
                    }
                    //}
                    //else
                    //{
                    //    //EmailService.SendAccountOpeningMail(email, title, content, date.ToString(), content2, 4);
                    //    SendEmail(accountOpeningExisting.Email, "", "", 4, accountOpeningExisting.MobileNo, accountOpeningExisting.IdNo, "", relation);
                    //    response.IsSuccess = false;
                    //    response.Message = "E-KYC identity verification failed. " + reason + "not match";
                    //}
                    //Response responseEmail = EmailService.SendeKYCDocumentVerificationEmail(accountOpeningExisting);
                    //if (!responseEmail.IsSuccess)
                    //{
                    //    response.Message += responseEmail.Message;
                    //}
                }
            }
            else
            {
                response.IsSuccess = false;
            }
            response.CustomValidations = customValidations;
            if (response.IsSuccess)
            {
                response.Data = accountOpeningExisting;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object CheckPassword(String password)
            {
            Response response = new Response { IsSuccess = true };
            try
            {
                List<CustomValidation> customValidations = new List<CustomValidation>();
                var hasUpperChar = password.Any(char.IsUpper);
                var hasLowerChar = password.Any(char.IsLower);
                var rule2 = password.Any(char.IsNumber);
                int rule3 = (password.Length >= 8 && password.Length <= 16) ? 1 : 0;
                String message = "Password must ";
                List<String> subMessages = new List<String>();
                Boolean flag = false;
                if (password == "")
                {
                    
                    flag = true;
                    response.Message = "Please set up your password";
                    response.IsSuccess = false;
                    return response;
                }
                if (rule2 == false)
                {
                    subMessages.Add("contain at least one number");
                    flag = true;
                }
                if (rule3 == 0)
                {
                    subMessages.Add("have length between 8 to 16 characters");
                    flag = true;
                }
                if (hasUpperChar == false || hasLowerChar == false)
                {
                    subMessages.Add("contain at least one uppercase alphabet letter and one lowercase alphabet letter");
                    flag = true;
                }
                if (flag == true)
                {
                    
                    response.Message = message + String.Join(", ", subMessages) + ".";
                    response.IsSuccess = false;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object CheckEmail(String email)
        {
            Response response = new Response { IsSuccess = true };
            try
            {
                String query = "email_id = '" + email + "'";
                Response responseUserExists = IUserService.GetDataByFilter(query, 0, 0, false);
                if (responseUserExists.IsSuccess)
                {
                    List<User> Users = (List<User>)responseUserExists.Data;
                    if (Users.Count > 0)
                    {
                        response.Message = "This email has been registered";
                        //response.IsSuccess = false;
                    }
                    else
                    {
                        String content2 = "";
                        if (Utility.Helper.CustomValidator.IsValidEmail(email))
                        {
                            response.IsSuccess = true;
                        }
                        else
                        {
                            response.Message = "Invalid Email";
                        }
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = responseUserExists.Message;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetTransactionResponse(String ExperienceTransactionId, String TransactionRecordId)
        {
            Response response = new Response { IsSuccess = true };
            try
            {
                Response resTransRec = DigitalizationServiceManager.GetTransactionRecord(ExperienceTransactionId, TransactionRecordId);
                if (resTransRec.IsSuccess)
                {
                    Trulioo.Client.V1.Model.VerifyResult verifyResult = (Trulioo.Client.V1.Model.VerifyResult)resTransRec.Data;
                    response.Data = verifyResult;
                }
                else
                {
                    return resTransRec;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object AgentVerify(string agentCode)
        {
            Response response = new Response();

            string invalid = "Invalid Introducer Code";
            string noInput = "Please Enter Introducer Code";
            string agentName;
            try
            {
                //check agent code
                //if input is not empty or null => get agent name.
                if (!String.IsNullOrEmpty(agentCode))
                {
                    string queryAgentCode = @"SELECT ar.id, ar.agent_code , arp.name from agent_regs ar left join agent_reg_personal arp on ar.id = arp.agent_reg_id where ar.agent_code = '" + agentCode + "'";
                    Response responseAgentCode = GenericService.GetDataByQuery(queryAgentCode, 0, 0, false, null, false, null, false);
                    if (responseAgentCode.IsSuccess)
                    {
                        var agentCodeDyn = responseAgentCode.Data;
                        var responseJSON = JsonConvert.SerializeObject(agentCodeDyn);
                        List<dynamic> agentCodeDynamicList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);

                        if (agentCodeDynamicList.Count != 0)
                        {
                            dynamic agentCodeDynamic = agentCodeDynamicList.First();
                            agentName = agentCodeDynamic.Name;
                            return agentName;
                        }
                        return invalid;
                    }
                    else
                    {
                        response.Message = "Invalid Agent Code";
                        response.IsSuccess = false;
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Account Opening Page verify agent: " + ex.Message);
            }
            return noInput;
        }

        public class NameValuePairs
        {
            public String name { get; set; }
            public String value { get; set; }
        }

        public class FileInput
        {
            public Int32 Type { get; set; }
            public String Base64String { get; set; }
            public String FileName { get; set; }
            public String FileNameNew { get; set; }
        }

        public static Response VerifyeKYC(Trulioo.Client.V1.Model.PersonInfo personInfo, Trulioo.Client.V1.Model.Location location, List<Trulioo.Client.V1.Model.NationalId> NationalIds, Trulioo.Client.V1.Model.Document document)
        {
            personInfo.FirstGivenName = "Leo";
            personInfo.FirstSurName = "Moggie";
            personInfo.DayOfBirth = 1;
            personInfo.MonthOfBirth = 10;
            personInfo.YearOfBirth = 1941;
            NationalIds = new List<Trulioo.Client.V1.Model.NationalId> { new Trulioo.Client.V1.Model.NationalId { Number = "411001-13-5027", Type = "nationalid" } };
            Response response = new Response() { IsSuccess = true };
            try
            {

                Response resAuth = DigitalizationServiceManager.TestAuth();
                if (resAuth.IsSuccess)
                {
                    //Response resEntities = DigitalizationServiceManager.GetTestEntities();
                    //if (resEntities.IsSuccess)
                    {
                        Response resConsents = DigitalizationServiceManager.GetConsents();
                        if (resConsents.IsSuccess)
                        {
                            List<string> Consents = (List<string>)resConsents.Data;
                            Response resDocumentTypes = DigitalizationServiceManager.GetDocumentTypes();
                            if (resDocumentTypes.IsSuccess)
                            {
                                DocumentType documentType = (DocumentType)resDocumentTypes.Data;
                                Trulioo.Client.V1.Model.VerifyRequest verifyRequestBody = new Trulioo.Client.V1.Model.VerifyRequest
                                {
                                    AcceptTruliooTermsAndConditions = true,
                                    CleansedAddress = true,
                                    VerboseMode = true,
                                    ConfigurationName = "Identity Verification",
                                    ConsentForDataSources = Consents.ToArray(),
                                    CountryCode = "MY",
                                    DataFields = new Trulioo.Client.V1.Model.DataFields
                                    {
                                        PersonInfo = personInfo,
                                        Location = location,
                                        Document = document,
                                        NationalIds = NationalIds.ToArray()
                                    }
                                };
                                Response resVerify = DigitalizationServiceManager.Verify(verifyRequestBody);
                                if (!resVerify.IsSuccess)
                                    return resVerify;
                                else
                                {
                                    Trulioo.Client.V1.Model.VerifyResult verifyResult = (Trulioo.Client.V1.Model.VerifyResult)resVerify.Data;
                                    response.Data = verifyResult;
                                    if (verifyResult.Record.RecordStatus == "match")
                                    {
                                        response.IsSuccess = true;
                                        string result = JsonConvert.SerializeObject(verifyResult.Record.DatasourceResults);
                                        response.Message = result;
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        string result = JsonConvert.SerializeObject(verifyResult.Record.DatasourceResults);
                                        response.Message = result;
                                    }
                                }
                            }
                        }
                        else
                        {
                            return resConsents;
                        }
                    }
                    //else
                    {
                        //return resEntities;
                    }
                }
                else
                {
                    return resAuth;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

    }
}