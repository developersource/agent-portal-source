﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddCourse : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazySiteContentServiceObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazySiteContentServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Staff"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            // Bind data for Instructor
            Response responseInstructor = GenericService.GetDataByFilter<Instructor>("status = 1 ", 0, 0, false, null, true, null, false, false, null);
            if (responseInstructor.IsSuccess)
            {
                List<Instructor> instructors = (List<Instructor>)responseInstructor.Data;
                foreach (Instructor x in instructors)
                    InstructorId.Items.Add(new ListItem(x.Name, x.Id.ToString()));
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                    "alert('" + responseInstructor.Message + "');", true);
            }
            //validations for duration type and duration
            string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
            CreatedBy.Value = "0";
            UpdatedBy.Value = "0";
            CreatedDate.Value = currentDateString;
            UpdatedDate.Value = currentDateString;
            string idString = Request.QueryString["id"];
            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);
                Response response = GenericService.GetSingle<DiOTP.Utility.CourseListing>(id, true, null, false);
                if (response.IsSuccess)
                {
                    DiOTP.Utility.CourseListing courseListing = (DiOTP.Utility.CourseListing)response.Data;

                    //To display file upload when it is an online course
                    if (courseListing.IsOnline == 1) {
                        uploadDiv.Attributes.Remove("class");
                        uploadDiv.Attributes.Add("class", "row mb-6");
                    }

                    Id.Value = courseListing.Id.ToString();
                    duration.Attributes.Remove("class");
                    duration.Attributes.Add("class", "row mb-6");
                    if (courseListing.DurationType.ToString() == "1")
                    {
                        Day.Value = courseListing.Duration.ToString();
                        
                        durationDay.Attributes.Remove("class");
                        durationTime.Attributes.Remove("class");
                        durationTime.Attributes.Add("class", "hide");
                        Day.Attributes.Remove("class");
                        Day.Attributes.Add("class", "form-control");
                        Hour.Attributes.Remove("class");
                        Hour.Attributes.Add("class", "form-control hide");
                        Minute.Attributes.Remove("class");
                        Minute.Attributes.Add("class", "form-control hide");
                    }
                    else
                    {
                        string[] duration = courseListing.Duration.ToString().Split(',');
                        Hour.Value = duration[0].ToString().Trim( new char[] { 'h', 'r'});
                        Minute.Value = duration[1].ToString().Trim(new char[] { 'm', 'i', 'n' });
                        durationDay.Attributes.Remove("class");
                        durationDay.Attributes.Add("class", "hide");
                        durationTime.Attributes.Remove("class");
                        Day.Attributes.Remove("class");
                        Day.Attributes.Add("class", "form-control hide");
                        Hour.Attributes.Remove("class");
                        Hour.Attributes.Add("class", "form-control");
                        Minute.Attributes.Remove("class");
                        Minute.Attributes.Add("class", "form-control");
                    }
                    
                    Topic.Value = courseListing.Topic;
                    
                    Synopsis.Value = courseListing.Synopsis;
                    Outcome.Value = courseListing.Outcome;
                    Venue.Value = courseListing.Venue;
                    DurationNote.Value = courseListing.DurationNote;
                    DurationType.Value = courseListing.DurationType.ToString();
                    Synopsis.Value = courseListing.Synopsis;
                    SpecialNote.Value = courseListing.SpecialNote;
                    txtUrl.Text = courseListing.CourseFileUrl;
                    EnrollEndDate.Value = courseListing.EnrollEndDate.ToString("MM/dd/yyyy");
                    CpdPoints.Value = courseListing.CpdPoints.ToString();
                    IsOnline.Value = courseListing.IsOnline.ToString();
                    IsAssessment.Value = courseListing.IsAssessment.ToString();
                    String[] licenseType = courseListing.LicenseType.Split(',');
                    foreach (String i in licenseType) {
                        if (i == "UTS")
                            UTS.Checked = true;
                        else if (i == "PRS")
                            PRS.Checked = true;
                        else if (i == "NotApplicable")
                            notApplicable.Checked = true;
                    }
                    String[] attendies = courseListing.Attendies.Split(',');
                    foreach (String i in attendies)
                    {
                        if (i == "NewJoiners")
                            NewJoiners.Checked = true;
                        else if (i == "SGM")
                            SGM.Checked = true;
                        else if (i == "GM")
                            GM.Checked = true;
                    }
                    
                    Capacity.Value = courseListing.Capacity.ToString();
                    InstructorId.Value = courseListing.InstructorId.ToString();
                    Status.Value = courseListing.Status.ToString();
                    CreatedBy.Value = courseListing.CreatedBy.ToString();
                    CreatedDate.Value = courseListing.CreatedDate.ToString("MM/dd/yyyy");
                    UpdatedBy.Value = courseListing.UpdatedBy.ToString();
                    UpdatedDate.Value = courseListing.UpdatedDate == null ? DateTime.Now.ToString("MM/dd/yyyy") : courseListing.UpdatedDate.ToString("MM/dd/yyyy");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}