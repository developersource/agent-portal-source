﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddSalesBooking.aspx.cs" Inherits="Admin.AddSalesBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addSalesBookingForm" runat="server">
        <div id="FormId" data-value="addSalesBookingForm">
            <div class="row">
                <div class="col-md-12">
                    <span class="badge badge-secondary">Booking Type</span>
                        <br />
                        <br />
                    <div class="row">
                        <div class="col-md-4">
                             <label class="fs-14 mt-2">Date:</label>
                        </div>
                        <div class="col-md-8">
                            <textbox id="currentDate" runat="server" clientidmode="static" disabled="true"/>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
