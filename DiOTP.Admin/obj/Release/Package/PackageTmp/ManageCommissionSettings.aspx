﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageCommissionSettings.aspx.cs" Inherits="Admin.ManageCommissionSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="manageCommissionSettingsForm" runat="server">
        <div id="FormId" data-value="manageCommissionSettingsForm">
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-10">
                        <label class="label label-default">Commission Structure Settings</label>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Commission Setting:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />                            
                            <select name="ddlCommissionSetting" id="ddlCommissionSetting" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Apply To:</label>
                        </div>
                        <div class="col-md-6">

                            <asp:DropDownList  ID="ddlApplyTo" runat="server" clientIdMode="Static" cssClass="form-control">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="WA">WA</asp:ListItem>
                                <asp:ListItem Value="WM">WM</asp:ListItem>
                                <asp:ListItem Value="GM">GM</asp:ListItem>
                                <asp:ListItem Value="SGM">SGM</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <hr />
                    <div class="mb-10">
                        <label class="label label-default">Commission Definitions</label>
                    </div>

                   <div class="row mb-6" id="commDefDetails" runat="server">
<%--                        <div class="col-md-3">
                            <label class="fs-14 mt-2">newCode2:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Enter Percentage" class="form-control" name="commDefPercentage" id="commDefPercentage" runat="server"  />
                        </div>--%>
                    </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
