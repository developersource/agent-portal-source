﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class OrderListDTO
    {
        public Int32 ID { get; set; }
        public Int32 user_id { get; set; }
        public Int32 user_account_id { get; set; }
        public string account_no { get; set; }
        public string order_no { get; set; }
        public Int32 order_type { get; set; }
        public Int32 created_by { get; set; }
        public DateTime created_date { get; set; }
        public Decimal amount { get; set; }
        public Decimal units { get; set; }
        public string Fund_Name { get; set; }
        public string Fund_Name2 { get; set; }
        public string payment_method { get; set; }
        public DateTime payment_date { get; set; }
        public DateTime settlement_date { get; set; }
        public Decimal initial_sales_charges_percent { get; set; }
        public Int32 order_status { get; set; }
        public string fpx_transaction_id { get; set; }
        public string fpx_bank_code { get; set; }
        public string trans_no { get; set; }
        public string consultantid { get; set; }
        public Int32 bank_id { get; set; }
        public Int32 distribution_instruction { get; set; }
        public Decimal LATEST_NAV_PRICE1 { get; set; }
        public Decimal? LATEST_NAV_PRICE2 { get; set; }
        public string reject_reason { get; set; }
        public Decimal? trans_amt { get; set; }
        public Decimal? trans_units { get; set; }
        public Decimal? app_fee { get; set; }
        public Decimal? exit_fee { get; set; }
        public Decimal? fee_amt { get; set; }
        public Decimal? net_amt { get; set; }
        public Decimal? trans_pr { get; set; }
        public DateTime confirmation_date { get; set; }
    }
}
