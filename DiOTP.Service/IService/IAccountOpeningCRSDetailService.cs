﻿using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service.IService
{
    public interface IAccountOpeningCRSDetailService : IDefaultInterface<AccountOpeningCRSDetail>
    {
    }
}
