﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Online_Transactions : System.Web.UI.Page
    {

        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountService = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountService.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String fundCode = Request.QueryString["fundCode"];
                if (fundCode == null || fundCode == "")
                    if (Session["transFundCode"] != null)
                        fundCode = Session["transFundCode"].ToString();
                    else
                        Session["transFundCode"] = fundCode;

                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx?redirectUrl=Online-Transactions.aspx?fundCode=" + fundCode + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx?redirectUrl=Online-Transactions.aspx?fundCode=" + fundCode + "');", true);
                    }
                }
                if (!IsPostBack)
                {
                    if (Session["user"] != null)
                    {
                        bool isSATUpdated = CheckIfSATUpdated();
                        if (!isSATUpdated)
                        {
                            hdnIsSATUptoDate.Value = "0";
                            RunScript(fundCode);
                        }
                        else
                            hdnIsSATUptoDate.Value = "1";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void RunScript(string fundCode)
        {
            if (fundCode != null && fundCode != "")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "if(confirm('Please Proceed to Update SAT.')) window.location.href='SAT-Form.aspx?redirectUrl=Online-Transactions.aspx'; else window.location.href='Funds-listing.aspx'", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "if(confirm('Please Proceed to Update SAT.')) window.location.href='SAT-Form.aspx?redirectUrl=Online-Transactions.aspx'; else window.location.href='Index.aspx'", true);
        }

        public string GetGroupByScore(Int32 SatScore)
        {
            if (SatScore >= 0 && SatScore < 6)
                return "G1";
            else if (SatScore >= 6 && SatScore <= 13)
                return "G2";
            else if (SatScore >= 14 && SatScore <= 22)
                return "G3";
            else if (SatScore >= 23 && SatScore <= 30)
                return "G4";
            else if (SatScore >= 31)
                return "G5";
            else
                return "";
        }

        public bool CheckIfSATUpdated()
        {
            bool isSATUpdated = false;
            try
            {
                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    Response response = IUserService.GetSingle(user.Id);
                    if (response.IsSuccess)
                    {
                        user = (User)response.Data;

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            UserAccount primaryAcc = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();

                            if (primaryAcc.IsSatChecked == 0)
                                isSATUpdated = false;
                            if (primaryAcc.SatUpdatedDate != null)
                            {
                                DateTime SatUpdatedDate = primaryAcc.SatUpdatedDate.Value;
                                int yearDays = 365;
                                if (DateTime.IsLeapYear(SatUpdatedDate.Year))
                                    yearDays = 366;

                                if ((DateTime.Now - SatUpdatedDate).TotalDays > yearDays)
                                    isSATUpdated = false;
                                else
                                    isSATUpdated = true;
                            }
                            else
                                isSATUpdated = false;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("online-Transactions Page CheckIfSATUpdated: " + ex.Message);
            }
            return isSATUpdated;
        }

        protected void lnkBuy_Click(object sender, EventArgs e)
        {
            string AccountId = hdnSelectedAccountNo.Value;
            if (!string.IsNullOrEmpty(AccountId) && AccountId != "0")
                Response.Redirect("BuyFunds.aspx", false);
            else
                errorMessage.InnerHtml = "Please select an Account";
        }

        protected void lnkSell_Click(object sender, EventArgs e)
        {
            string AccountId = hdnSelectedAccountNo.Value;
            if (!string.IsNullOrEmpty(AccountId) && AccountId != "0")
                Response.Redirect("SellFunds.aspx", false);
            else
                errorMessage.InnerHtml = "Please select an Account";
        }

        protected void lnkSwitch_Click(object sender, EventArgs e)
        {
            string AccountId = hdnSelectedAccountNo.Value;
            if (!string.IsNullOrEmpty(AccountId) && AccountId != "0")
                Response.Redirect("Under-Construction.aspx", false);
            else
                errorMessage.InnerHtml = "Please select an Account";
        }

        protected void lnkTransfer_Click(object sender, EventArgs e)
        {
            string AccountId = hdnSelectedAccountNo.Value;
            if (!string.IsNullOrEmpty(AccountId) && AccountId != "0")
                Response.Redirect("Under-Construction.aspx", false);
            else
                errorMessage.InnerHtml = "Please select an Account";
        }

        protected void lnkCoolingOff_Click(object sender, EventArgs e)
        {
            string AccountId = hdnSelectedAccountNo.Value;
            if (!string.IsNullOrEmpty(AccountId) && AccountId != "0")
                Response.Redirect("Under-Construction.aspx", false);
            else
                errorMessage.InnerHtml = "Please select an Account";

        }

    }
}