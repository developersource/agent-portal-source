﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    
    public class AgentAddress
    {
        public Int32 Id { get; set; }
        public Int32 AccountOpeningId { get; set; }
        public String Addr1 { get; set; }
        public String Addr2 { get; set; }
        public String Addr3 { get; set; }
        public String Addr4 { get; set; }
        public String City { get; set; }
        public String PostCode { get; set; }
        public String State { get; set; }
        public String Country { get; set; }
        public String TelNo { get; set; }
        public Int32 Status { get; set; }
    }
}
