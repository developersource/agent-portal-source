﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ExamMaterials : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string queryMaterialList = @"Select * from agent_trainee_materials where is_active='1'";
            Response responseMaterial = GenericService.GetDataByQuery(queryMaterialList, 0, 0, false, null, false, null, true);
            if (responseMaterial.IsSuccess)
            {
                var materialDyn = responseMaterial.Data;
                var responseJSON = JsonConvert.SerializeObject(materialDyn);
                List<AgentTraineeMaterial> materialList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTraineeMaterial>>(responseJSON);
                StringBuilder sb = new StringBuilder();
                int index = 1;

                materialList.ForEach(x =>
                {
                    string dirFullPath = HttpContext.Current.Server.MapPath(x.material_url_path);
          
                    sb.Append(
                                    @"
                                            <tr class='" + "trTextFormat" + @"'>
                                            <td>" + index + @"</td>
                                            <td><a href='" + x.material_url_path + "'target='_blank'>" + x.material_name + " " + @"<i class='fa fa-eye'></i></a></td>
                                            <td>" + x.material_description + @"</td>
                                        </tr>");
                    index++;

                });
                if (materialList.Count == 0) {
                    sb.Append(@"<tr class='" + "trTextFormat" + @"'>
                                            <td colspan='3'>No Data Available For Display.</td>
                                            <td style='display:none'></td>
                                            <td style='display:none'></td>
                                        </tr>");
                }
                tbodyAgentTraineeMaterialList.InnerHtml = sb.ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseMaterial.Message + "');", true);
            }


        }
    }
}