﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DiOTP.OracleData
{
    public class DataManager
    {
        public string GetOracleConnection()
        {

            string Host = ConfigurationManager.AppSettings["Oracle_Host"];
            string Port = ConfigurationManager.AppSettings["Oracle_Port"];
            string DatabaseName = ConfigurationManager.AppSettings["Oracle_DatabaseName"];
            string Useraame = ConfigurationManager.AppSettings["Oracle_UserName"];
            string Password = ConfigurationManager.AppSettings["Oracle_Password"];


            string ConnectionString = "Data Source = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = " + Host + ")(PORT = " + Port + ")))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = " + DatabaseName + "))); User Id = " + Useraame + "; Password = " + Password + ";";
            return ConnectionString;

        }
        public string GetSqlConnection()
        {
            string ServerName = ConfigurationManager.AppSettings["MySql_ServerName"];
            string MyDatabase = ConfigurationManager.AppSettings["MySql_Database"];
            string Myusername = ConfigurationManager.AppSettings["MySql_Username"];
            string Mypassword = ConfigurationManager.AppSettings["MySql_Password"];

            string conStr = "server=" + ServerName + ";user id=" + Myusername + "; password=" + Mypassword + ";database=" + MyDatabase + "; SslMode = None;";
            return conStr;
        }
        public string Read_Single_MYSQL_Column_Value(string Query)
        {
            string Result = "";
            MySqlConnection connection = new MySqlConnection(GetSqlConnection());
            MySqlCommand mycommand = connection.CreateCommand();

            mycommand.CommandText = Query;
            MySqlDataAdapter myDA = new MySqlDataAdapter(mycommand);
            try
            {
                connection.Open();
                MySqlDataReader reader = mycommand.ExecuteReader();
                while (reader.Read())
                {
                    Result = reader[0].ToString();
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), "DB_Layer");
            }
            finally
            {
                connection.Close();
            }
            return Result;
        }
        public string Find_Value_exists(string Query)
        {
            string Result = "0";
            MySqlConnection connection = new MySqlConnection(GetSqlConnection());
            MySqlCommand mycommand = connection.CreateCommand();

            mycommand.CommandText = Query;
            MySqlDataAdapter myDA = new MySqlDataAdapter(mycommand);
            try
            {
                connection.Open();
                MySqlDataReader reader = mycommand.ExecuteReader();
                while (reader.HasRows)
                {
                    Result = "1";
                    break;
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), "DB_Layer");
            }
            finally
            {
                connection.Close();
            }
            return Result;
        }
    }
}
