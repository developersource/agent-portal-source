﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("commission_defs")]
    public class CommissionDefinitions
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("code"), Display("code")]
        public String code { get; set; }
        [Database("name"), Display("name")]
        public String name { get; set; }
        [Database("is_active"), Display("isActive")]
        public Int32 isActive { get; set; }
        [Database("created_date"), Display("createdDate")]
        public DateTime createdDate { get; set; }
        [Database("created_by"), Display("createdBy")]
        public Int32 createdBy { get; set;  }
        [Database("updated_date"), Display("updatedDate")]
        public DateTime updatedDate { get; set; }
        [Database("updated_by"), Display("updatedBy")]
        public Int32 updatedBy { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
