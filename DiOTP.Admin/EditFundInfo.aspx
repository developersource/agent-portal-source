﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditFundInfo.aspx.cs" Inherits="Admin.EditFundInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label>Category</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Launch Date</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtLaunchDate" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Launch Price</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtLaunchPrice" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Pricing Basis</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtPriceBasis" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Latest NAV Price</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtLatestNAVPrice" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Historical Income Distribution</label>
                    </div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="ddlHistoricalIncomeDistribution" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="no" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Approved by EPF</label>
                    </div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="ddlApprovedByEPF" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="no" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Shariah Compliant</label>
                    </div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="ddlShariahCompliant" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="no" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Risk Rating</label>
                    </div>
                    <div class="col-md-8">
                       <asp:TextBox ID="txtRiskRating" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label>Fund Size</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundSize" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Minimum Initial Investment</label>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <label>CASH</label>
                                <asp:TextBox ID="txtMinInitialInvestmentCASH" runat="server" CssClass="form-control" placeholder="CASH"></asp:TextBox>
                            </div>
                            <div class="col-md-6">
                                <label>EPF</label>
                                <asp:TextBox ID="txtMinInitialInvestmentEPF" runat="server" CssClass="form-control" placeholder="EPF"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Minimum Subsequent Investment</label>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <label>CASH</label>
                                <asp:TextBox ID="txtMinSubsequentInvestmentCASH" runat="server" CssClass="form-control" placeholder="CASH"></asp:TextBox>
                            </div>
                            <div class="col-md-6">
                                <label>EPF</label>
                                <asp:TextBox ID="txtMinSubsequentInvestmentEPF" runat="server" CssClass="form-control" placeholder="EPF"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Minimum RSP Investment</label>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Initial</label>
                                <asp:TextBox ID="txtMinRSPInvestmentInitial" runat="server" CssClass="form-control" placeholder="Initial"></asp:TextBox>
                            </div>
                            <div class="col-md-6">
                                <label>Additional</label>
                                <asp:TextBox ID="txtMinRSPInvestmentAdditional" runat="server" CssClass="form-control" placeholder="Additional"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Minimum Redemption Amount</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtMinRedemptionAmount" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Minimum Holding</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtMinHolding" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
