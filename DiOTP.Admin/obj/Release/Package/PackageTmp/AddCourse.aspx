﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCourse.aspx.cs" Inherits="Admin.AddCourse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addCourseForm" runat="server">
        <div id="FormId" data-value="addCourseForm">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Topic:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="hidden" name="SiteContentTypeId" value="2" id="SiteContentTypeId" runat="server" clientidmode="static" />
                            <input type="hidden" name="IsDisplay" value="1" id="IsDisplay" runat="server" clientidmode="static" />
                            <input type="hidden" name="IsContentDisplay" value="1" id="IsContentDisplay" runat="server" clientidmode="static" />
                            <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                            <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                            <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                            <input type="hidden" name="UpdatedBy" value="0" id="UpdatedBy" runat="server" clientidmode="static" />
                            <input type="hidden" name="UpdatedDate" value="0" id="UpdatedDate" runat="server" clientidmode="static" />
                            <input type="hidden" name="ImageUrl" id="ImageUrl" runat="server" clientidmode="static" />                            
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <%--<div id="divImg" class="fileupload-new thumbnail" style="width: 200px; height: 150px;" runat="server"  clientidmode="Static">
                                    <img id="bannerImage" runat="server" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>--%>
                                <%--<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <%--<div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i>Change</span>
                                        <asp:FileUpload ID="fuBannerImage" runat="server" ClientIDMode="Static" CssClass="default filePicker" />
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
                                </div>
                                <div id="uploadStatus">0%</div>--%>
                                <input type="text" name="Topic" id="Topic" runat="server" clientidmode="static" class="form-control" placeholder="Enter Topic" />
                            </div>
                            <%--<span class="label label-danger">NOTE!</span>
                            <span>Attached image thumbnail is
                                    supported in Latest Firefox, Chrome, Opera,
                                    Safari and Internet Explorer 10 only
                            </span>--%>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Synopsis:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Synopsis" id="Synopsis" runat="server" clientidmode="static" class="form-control" placeholder="Enter Synopsis" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Learning Outcome:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Outcome" id="Outcome" runat="server" clientidmode="static" class="form-control" placeholder="Enter Learning Outcomes" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Training Type:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="Type" id="IsOnline" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select Training Type</option>
                                <option value="1">Online</option>
                                <option value="0">Offline</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Venue / App:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Venue" id="Venue" runat="server" clientidmode="static" class="form-control" placeholder="Enter Venue / App" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Capacity:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Capacity" id="Capacity" runat="server" clientidmode="static" class="form-control" placeholder="Enter Capacity" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">License:</label>
                        </div>
                        <div class="col-md-8 checkbox-group-1" id="licenseChecks" style="padding: 0px;">
                            <div class="col-md-3">
                                <input id="UTS" type="checkbox" runat="server" clientidmode="static" name="UTS" style="margin-right: 5px;" /><label for="UTS">UTS</label>
                            </div>
                            <div class="col-md-3">
                                <input id="PRS" type="checkbox" runat="server" clientidmode="static" name="PRS" style="margin-right: 5px;" /><label for="PRS">PRS</label>
                            </div>
                            <div class="col-md-6">
                                <input id="notApplicable" type="checkbox" runat="server" clientidmode="static" name="notApplicable" style="margin-right: 5px;" /><label for="notApplicable" class="">Not Applicable</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Attendies:</label>
                        </div>
                        <div class="col-md-8 checkbox-group-1" id="attendiesChecks" style="padding: 0px;">
                            <div class="col-md-5">
                                <input id="NewJoiners" type="checkbox" runat="server" clientidmode="static" name="NewJoiners" style="margin-right: 5px;" /><label for="NewJoiners">New Joiners</label>
                            </div>
                            <div class="col-md-4">
                                <input id="SGM" type="checkbox" runat="server" clientidmode="static" name="SGM" style="margin-right: 5px;" /><label for="SGM">SGM</label>
                            </div>
                            <div class="col-md-3">
                                <input id="GM" class="" type="checkbox" runat="server" clientidmode="static" name="GM" style="margin-right: 5px;" /><label for="GM">GM</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Assessment:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="IsAssessment" id="IsAssessment" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="1">Required</option>
                                <option value="0">Not Required</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Instructor:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="InstructorId" id="InstructorId" runat="server" clientidmode="static" class="form-control">
                                <option value="0">Select Instructor</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Duration Type:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="DurationType" id="DurationType" runat="server" clientidmode="static" class="form-control">
                                <option value="0">Select Duration Type</option>
                                <option value="1">Days</option>
                                <option value="2">Time</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6 hide" id="duration" runat="server">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Duration:</label>
                        </div>
                        <div class="col-md-8">
                            <div id="durationDay" runat="server" class="hide">
                                <div class="row">
                                    <label class="col-md-3" style="padding-top: 5px;">Day:</label>
                                    <div class="col-md-9">
                                        <input type="text" name="Day" id="Day" runat="server" clientidmode="static" class="form-control" placeholder="Enter Day" />
                                    </div>
                                </div>
                            </div>
                            <div id="durationTime" runat="server" class="hide">
                                <div class="row">
                                    <label class="col-md-3" style="padding-top: 5px;">Hour:</label>
                                    <div class="col-md-9">
                                        <input type="text" name="Time" id="Hour" runat="server" clientidmode="static" class="form-control col-md-9" placeholder="Enter Hour" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3" style="padding-top: 5px;">Minute:</label>
                                    <div class="col-md-9">
                                        <input type="text" name="Time" id="Minute" runat="server" clientidmode="static" class="form-control col-md-9" placeholder="Enter Minute" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Duration Notes:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="DurationNote" id="DurationNote" runat="server" clientidmode="static" class="form-control" placeholder="Enter Duration Note" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Enrollment Closing Date:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="EnrollEndDate" id="EnrollEndDate" runat="server" clientidmode="static" class="form-control default-date-picker" placeholder="Select Enroll End Date" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">CPD Points:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="CpdPoints" id="CpdPoints" runat="server" clientidmode="static" class="form-control" placeholder="Enter CPD Points" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Special note:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="SpecialNote" id="SpecialNote" runat="server" clientidmode="static" class="form-control" placeholder="Enter Special Note" />
                        </div>
                    </div>
                    <div class="row mb-6 hide" id="uploadDiv" runat="server">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2" id="MaterialUploadlbl" runat="server">File Attachment:</label>
                        </div>
                        <div class="col-md-8 materialDiv">
                            <%--<asp:FileUpload ID="MaterialUploadBtn" runat="server" ClientIDMode="Static" CssClass="default filePicker hide form-control" text="File Upload" style="width:270px;"/>
                            <input type="hidden" name="CourseFileUrl" id="CourseFileUrl" runat="server" clientidmode="static" />--%>
                            <asp:TextBox ID="txtUrl" runat="server" CssClass="document-title form-control" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                            <div class="fileupload fileupload-new" data-provides="fileupload" id="statusOption" runat="server" clientidmode="static">
                                    <div>
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> <span id="btnFileText">Select file</span></span> <i class="fa fa-spinner fa-spin hide"></i>
                                            <asp:FileUpload ID="MaterialUploadBtn" runat="server" ClientIDMode="Static" CssClass="default filePicker" style="width:270px;"/>
                                             <input type="hidden" name="CourseFileUrl" id="CourseFileUrl" runat="server" clientidmode="static" />
                                        </span>
                                    </div>
                                </div>
                        </div>
                    </div>

                </div>
            </div>

            <%--<div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-2">
                            <label class="fs-14 mt-2">Content:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea placeholder="Enter Content" rows="5" class="form-control" name="Content" id="Content" runat="server"></textarea>
                        </div>
                    </div>
                </div>--%>
        </div>
        <script>
            $ui(document).ready(function () {

                //Only allow file upload to process if it is an online course.
                $('#IsOnline').change(function () {
                    //Check if is online
                    if ($(this).val() == 1) {
                        console.log('is online');
                        $('#uploadDiv').removeClass('hide');
                        //$('.materialDiv').removeClass('hide');
                    }
                    else if ($(this).val() == null) {
                        $('#uploadDiv').addClass('hide');
                        //$('.materialDiv').addClass('hide');
                    }
                    else {
                        console.log('not online');
                        $('#uploadDiv').addClass('hide');
                        //$('.materialDiv').addClass('hide');
                    }
                });

                $ui('.default-date-picker').datepicker({
                    format: 'mm/dd/yyyy',
                    pickTime: false,
                    autoclose: true
                });

                 function sendFile(file) {
                    var formData = new FormData();
                    formData.append('file', file);
                    $ui.ajax({
                        type: 'post',
                        url: 'fileUploadMaterial.ashx',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (status) {
                            console.log(status);
                            $('#CourseFileUrl').val(status);      
                        },
                        error: function () {
                            alert("Whoops something went wrong!");
                        }
                    });
                 }

                var _URL = window.URL || window.webkitURL;
                $ui('.filePicker').unbind('change');
                $ui('.filePicker').change(function () {
                    var file;
                    file = this.files[0];
                    console.log(file);
                    
                    var extensions = ["ppt","pptx","pdf"];
                    var fileLink = document.getElementById('MaterialUploadBtn').value;
                    var fileExtensionType = fileLink.split(".").pop();
                    console.log(fileLink);
                    console.log(fileExtensionType.value);
                    //Check if file extension is included in the list.
                    if (extensions.some(fileExt => fileExtensionType.includes(fileExt))) {
                        var fullFilePath = fileLink;
                        var lastDot = fullFilePath.lastIndexOf('\\')+1;
                        var fileName = fullFilePath.substring(lastDot, fullFilePath.length);

                        document.getElementById('txtUrl').value = fileName;
                        console.log("This is a valid file type!");
                        sendFile(file);
                                         
                    }
                    else {
                        document.getElementById('URL').value = "";
                        alert("Only Powerpoint or PDF files are allowed!");
                    }
                });

                $ui('.default-color-picker').colorpicker();

                $('#DurationType').change(function () {

                    if ($(this).val() == '1') {
                        $('#duration').removeClass('hide');
                        $('#durationDay').removeClass('hide');
                        $('#Day').removeClass('hide');
                        $('#durationTime').addClass('hide');
                    }
                    else if ($(this).val() == '2') {
                        $('#duration').removeClass('hide');
                        $('#durationTime').removeClass('hide');
                        $('#Hour').removeClass('hide');
                        $('#Minute').removeClass('hide');
                        $('#durationDay').addClass('hide');
                    }
                    else {
                        $('#duration').addClass('hide');
                        $('#durationTime').addClass('hide');
                        $('#durationDay').addClass('hide');
                    }
                });
            });
        </script>
    </form>
</body>
</html>
