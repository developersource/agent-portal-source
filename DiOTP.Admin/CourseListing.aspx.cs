﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Admin.ServiceCalls;

namespace Admin
{
    public partial class CourseListing : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazyUserServiceObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT t.*, i.name as instructor_name FROM ");
            string mainQCount = (@"select count(*) from ");
            filter.Append(@"agent_training_topics t
                            left join instructors i on i.id = t.instructor_id
                             where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }


                //string siteContentTypeId = Converter.GetColumnNameByPropertyName<SiteContent>(nameof(SiteContent.SiteContentTypeId));
                //filter.Append("and " + siteContentTypeId + "='2'");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameTitle = Converter.GetColumnNameByPropertyName<CourseListing>(nameof(CourseListing.Title));
                    filter.Append(" and " + columnNameTitle + " like '%" + Search.Value.Trim() + "%'");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                    {
                        if (FilterValue.Value == "0")
                        {
                            filter.Append(@"and t.is_active in ('0') ");
                        }
                        else if (FilterValue.Value == "1")
                        {
                            filter.Append(@"and t.is_active in ('1') ");
                        }
                        else
                        {

                        }

                    }
                    else
                    {
                        filter.Append(@"and t.is_active in ('0', '1') ");
                    }
                }
                else
                {
                    filter.Append(@"and t.is_active in ('0', '1') ");
                }

                filter.Append("order by t.status desc, t.is_active desc, t.created_date desc");


                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseSCList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);


                if (responseSCList.IsSuccess)
                {
                    var CourseDyn = responseSCList.Data;
                    var responseJSON = JsonConvert.SerializeObject(CourseDyn);
                    List<DiOTP.Utility.CourseListingView> courselisting = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CourseListingView>>(responseJSON);


                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (DiOTP.Utility.CourseListingView c in courselisting)
                    {
                        String[] licenseType = c.LicenseType.Split(',');
                        asb.Append(@"<tr>
                <td class='icheck'>
                    <div class='square single-row'>
                         <div class='checkbox'>
                            <input type='checkbox' name='checkRow' class='checkRow' value='" + c.Id + @"' /> <label>" + index + @"</label><br/>
                         </div>
                      </div>
                     <span class='row-status'>" + (c.IsActive == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                 </td>
                <td>" + (c.StartDate == default(DateTime) ? "-" : c.StartDate.ToString("dd/MM/yyyy")) + @"</td>
                <td>" + c.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                <td>" + c.Topic + @"</td>
                <td>" + c.Synopsis + @"</td>
                <td>" + c.Outcome + @"</td>
                <td>" + (c.IsOnline == 1 ? "Online" : "Offline") + @"</td>
                <td class='td-limit'>" + c.Venue + @"</td>
                <td>" + (c.Capacity == 0 ? "N/A" : c.Capacity.ToString()) + @"</td>
                <td class='td-limit'>" + c.LicenseType + @"</td>
                <td>" + c.Attendies + @"</td>
                <td>" + (c.IsAssessment == 1 ? "Required" : "Not Required") + @"</td>
                <td>" + c.InstructorName + @"</td>
                <td>" + c.DurationType + @"</td>
                <td>" + c.Duration + @"</td>
                <td>" + c.DurationNote + @"</td>
                <td>" + c.EnrollEndDate.ToString("dd/MM/yyyy") + @"</td>
                <td>" + c.CpdPoints + @"</td>
                <td>" + c.SpecialNote + @"</td>
                <td>" + c.UpdatedDate + @"</td>
                <td>" + c.UpdatedBy + @"</td>
            </tr>
            ");
                        index++;

                    }
                    courseTbody.InnerHtml = asb.ToString();
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                User loginUser = (User)HttpContext.Current.Session["admin"];
                if (loginUser == null)
                {
                    response1.IsSuccess = false;
                    response1.Message = "Session Expired!";
                    responseMsgs.Add(response1.Message);
                }
                else
                {
                    LoginRole loginRole = ServicesManager.GetRole(HttpContext.Current);
                    if (!loginRole.isAdminOfficer && action == "Activate")
                    {
                        response1.IsSuccess = false;
                        response1.Message = "Admin Officer only can Approve!";
                        responseMsgs.Add(response1.Message);
                    }
                    else
                    {
                        string idString = String.Join(",", ids);
                        string queryC = (@" SELECT * FROM agent_training_topics where id in (" + idString + ") ");
                        Response responseCList = GenericService.GetDataByQuery(queryC, 0, 0, false, null, false, null, false);
                        if (responseCList.IsSuccess)
                        {
                            var CsDyn = responseCList.Data;
                            var responseCJSON = JsonConvert.SerializeObject(CsDyn);
                            List<DiOTP.Utility.CourseListing> courseListings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CourseListing>>(responseCJSON);
                            if (action == "Activate")
                            {
                                courseListings.ForEach(x =>
                                {
                                    if (x.IsActive == 0)
                                    {
                                        x.StartDate = DateTime.ParseExact(rejectReason, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        x.StartAssignedBy = loginUser.Id;
                                        x.IsActive = 1;
                                        if (x.IsOnline == 1)
                                        {
                                            //Instructor instructor = new Instructor();
                                            //Response responseInstructor = GenericService.GetDataByFilter<Instructor>(" id=" + x.InstructorId + " ", 0, 0, false, null, true, null, false, false, null);
                                            //if (responseInstructor.IsSuccess)
                                            //{
                                            //    List<Instructor> instructors = (List<Instructor>)responseInstructor.Data;
                                            //    instructor = instructors.FirstOrDefault();
                                            //}
                                            //else
                                            //{
                                            //    response1.IsSuccess = false;
                                            //    responseMsgs.Add(responseInstructor.Message);
                                            //}
                                            Meeting meeting = new Meeting
                                            {
                                                topic = x.Topic,
                                                type = 2,
                                                start_time = x.StartDate,
                                                duration = 40,
                                                schedule_for = "",
                                                //schedule_for = instructor.EmailId,
                                                timezone = "",
                                                password = "12345",
                                                agenda = x.Synopsis,
                                                recurrence = new Recurrence
                                                {
                                                    type = 1,
                                                    repeat_interval = 1,
                                                    weekly_days = "",
                                                    monthly_day = 0,
                                                    monthly_week = 0,
                                                    monthly_week_day = 0,
                                                    end_times = 1,
                                                    end_date_time = ""
                                                },
                                                settings = new DiOTP.Utility.CustomClasses.Settings
                                                {
                                                    host_video = true,
                                                    participant_video = true,
                                                    cn_meeting = false,
                                                    in_meeting = false,
                                                    join_before_host = false,
                                                    mute_upon_entry = false,
                                                    watermark = false,
                                                    use_pmi = false,
                                                    approval_type = 2,
                                                    registration_type = 2,
                                                    audio = "",
                                                    auto_recording = "",
                                                    enforce_login = true,
                                                    enforce_login_domains = "",
                                                    alternative_hosts = "",
                                                    global_dial_in_countries = new string[] { },
                                                    registrants_email_notification = true,
                                                    meeting_authentication = true
                                                }
                                            };

                                            Response resMeeting = ZoomIntegrationService.CreateMeeting(meeting, "https://api.zoom.us/v2/users/me/meetings?status=active&page_size=30&page_number=1");
                                            if (resMeeting.IsSuccess)
                                            {
                                                MeetingResponse meetingResponse = (MeetingResponse)resMeeting.Data;
                                                x.StartUrl = meetingResponse.start_url;
                                                x.JoinUrl = meetingResponse.join_url;
                                            }
                                            else
                                            {
                                                response1.IsSuccess = false;
                                                responseMsgs.Add(resMeeting.Message);
                                            }
                                        }
                                        Response resUpdate = GenericService.UpdateData<DiOTP.Utility.CourseListing>(x);
                                        if (resUpdate.IsSuccess)
                                        {
                                            response1.IsSuccess = true;
                                            responseMsgs.Add("Activated Successfully");
                                        }
                                        else
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add(resUpdate.Message);
                                        }
                                    }
                                });
                            }
                            if (action == "Deactivate")
                            {
                                courseListings.ForEach(x =>
                                {
                                    if (x.IsActive == 1)
                                    {
                                        x.IsActive = 0;
                                        Response resUpdate = GenericService.UpdateData<DiOTP.Utility.CourseListing>(x);
                                        if (resUpdate.IsSuccess)
                                        {
                                            response1.IsSuccess = true;
                                            responseMsgs.Add("Deactivated Successfully");
                                        }
                                        else
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add(resUpdate.Message);
                                        }
                                    }
                                });
                            }
                        }
                        else
                        {
                            response1.IsSuccess = false;
                            responseMsgs.Add(responseCList.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add("Action: " + ex.Message + " - Exception");
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(DiOTP.Utility.CourseListingCustom obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            if (obj.Topic == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Topic";
                return response;
            }
            if (obj.Synopsis == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Synopsis";
                return response;
            }
            if (obj.Outcome == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Learning Outcome";
                return response;
            }
            if (obj.IsOnline == "")
            {
                response.IsSuccess = false;
                response.Message = "Select your Training Type";
                return response;
            }
            if (obj.IsOnline == "1" && obj.CourseFileUrl == "")
            {
                response.IsSuccess = false;
                response.Message = "Please upload the course material";
                return response;
            }
            if (obj.Venue == "")
            {
                response.IsSuccess = false;
                response.Message = "Select Venue / App";
                return response;
            }
            if (obj.Capacity == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter Capacity";
                return response;
            }
            if (obj.UTS == null && obj.PRS == null && obj.notApplicable == null)
            {
                response.IsSuccess = false;
                response.Message = "Choose License";
                return response;
            }
            if (obj.NewJoiners == null && obj.SGM == null && obj.GM == null)
            {
                response.IsSuccess = false;
                response.Message = "Choose Attendies";
                return response;
            }
            if (obj.IsAssessment == "")
            {
                response.IsSuccess = false;
                response.Message = "Choose Assessment";
                return response;
            }
            if (obj.InstructorId == "0")
            {
                response.IsSuccess = false;
                response.Message = "Select Instructor";
                return response;
            }
            if (obj.DurationType == 0)
            {
                response.IsSuccess = false;
                response.Message = "Select Duration Type";
                return response;
            }
            if (obj.DurationType == 1)
            {
                if (obj.Day == "")
                {
                    response.IsSuccess = false;
                    response.Message = "Enter Number of Days";
                    return response;
                }
            }
            else if (obj.DurationType == 2)
            {
                if (obj.Hour == "")
                {
                    response.IsSuccess = false;
                    response.Message = "Enter Number of Hours";
                    return response;
                }
                if (obj.Minute == "")
                {
                    response.IsSuccess = false;
                    response.Message = "Enter Number of Minutes";
                    return response;
                }
            }
            if (obj.EnrollEndDate == default(DateTime))
            {
                response.IsSuccess = false;
                response.Message = "Select Enroll End Date";
                return response;
            }
            if (obj.CpdPoints == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter Cpd Points";
                return response;
            }
            String licenseType = "";
            if (obj.UTS != null)
            {
                licenseType += "UTS";
            }
            if (obj.PRS != null)
            {
                if (licenseType == "")
                {
                    licenseType += "PRS";
                }
                else
                {
                    licenseType += ",PRS";
                }
            }
            if (obj.notApplicable != null)
            {
                if (licenseType == "")
                {
                    licenseType += "NotApplicable";
                }
                else
                {
                    licenseType += ",NotApplicable";
                }
            }
            String attendies = "";
            if (obj.NewJoiners != null)
            {
                attendies += "NewJoiners";
            }
            if (obj.SGM != null)
            {
                if (attendies == "")
                {
                    attendies += "SGM";
                }
                else
                {
                    attendies += ",SGM";
                }
            }
            if (obj.GM != null)
            {
                if (attendies == "")
                {
                    attendies += "GM";
                }
                else
                {
                    attendies += ",GM";
                }
            }



            //if (obj.LicenseType == 0)
            //{
            //    response.IsSuccess = false;
            //    response.Message = "Select your License Type";
            //    return response;
            //}
            //if (obj.Attendies == 0)
            //{
            //    response.IsSuccess = false;
            //    response.Message = "Select Attendies";
            //    return response;
            //}

            try
            {
                Response responseGet = GenericService.GetSingle<DiOTP.Utility.CourseListing>(obj.Id, true, null, false);
                if (responseGet.IsSuccess)
                {
                    DiOTP.Utility.CourseListing courseListing = (DiOTP.Utility.CourseListing)responseGet.Data;
                    if (courseListing == null)
                    {
                        courseListing = new DiOTP.Utility.CourseListing
                        {
                            Id = obj.Id,
                            Topic = obj.Topic,
                            Synopsis = obj.Synopsis,
                            Outcome = obj.Outcome,
                            IsOnline = Convert.ToInt32(obj.IsOnline),
                            Venue = obj.Venue,
                            Capacity = Convert.ToInt32(obj.Capacity),
                            LicenseType = licenseType,
                            Attendies = attendies,
                            IsAssessment = Convert.ToInt32(obj.IsAssessment),
                            InstructorId = Convert.ToInt32(obj.InstructorId),
                            DurationType = obj.DurationType,
                            Duration = (obj.DurationType == 1 ? obj.Day : obj.Hour + "hr," + obj.Minute + "min"),
                            DurationNote = obj.DurationNote,
                            CourseFileUrl = obj.CourseFileUrl,
                            EnrollEndDate = obj.EnrollEndDate,
                            CpdPoints = Convert.ToInt32(obj.CpdPoints),
                            SpecialNote = obj.SpecialNote,
                            IsActive = 0,
                            StartDate = default(DateTime),
                            StartAssignedBy = 0,
                            CreatedBy = loginUser.Id,
                            CreatedDate = DateTime.Now,
                            UpdatedBy = loginUser.Id,
                            UpdatedDate = DateTime.Now,
                            Status = 1,
                        };
                    }
                    else
                    {
                        courseListing = new DiOTP.Utility.CourseListing
                        {
                            Id = obj.Id,
                            Topic = obj.Topic,
                            Synopsis = obj.Synopsis,
                            Outcome = obj.Outcome,
                            IsOnline = Convert.ToInt32(obj.IsOnline),
                            Venue = obj.Venue,
                            Capacity = Convert.ToInt32(obj.Capacity),
                            LicenseType = licenseType,
                            Attendies = attendies,
                            IsAssessment = Convert.ToInt32(obj.IsAssessment),
                            InstructorId = Convert.ToInt32(obj.InstructorId),
                            DurationType = obj.DurationType,
                            Duration = (obj.DurationType == 1 ? obj.Day : obj.Hour + "hr," + obj.Minute + "min"),
                            DurationNote = obj.DurationNote,
                            CourseFileUrl = obj.CourseFileUrl,
                            EnrollEndDate = obj.EnrollEndDate,
                            CpdPoints = Convert.ToInt32(obj.CpdPoints),
                            SpecialNote = obj.SpecialNote,
                            IsActive = obj.Id == 0 ? 0 : courseListing.IsActive,
                            StartDate = default(DateTime),
                            StartAssignedBy = 0,
                            CreatedBy = loginUser.Id,
                            CreatedDate = DateTime.Now,
                            UpdatedBy = loginUser.Id,
                            UpdatedDate = DateTime.Now,
                            Status = 1,
                        };
                    }
                    if (obj.Id == 0)
                    {
                        GenericService.PostData<DiOTP.Utility.CourseListing>(courseListing);
                    }
                    else
                    {
                        GenericService.UpdateData<DiOTP.Utility.CourseListing>(courseListing);
                    }
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Banner" + courseListing.Id + " has successfully updated",
                        TableName = "site_content",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }
                    //Audit Log Ends here
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                    response = responseGet;
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add language dir action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}