﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }


        public static User user = new User();
        public static UserAccount userAcc = new UserAccount();
        public static int opt = new int();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdnSMSExpirationTimeInSeconds.Value = SMSExpirationTimeInSeconds.ToString();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnMobileSubmit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMobileNumber.Text) && !string.IsNullOrEmpty(txtMobileIc.Text))
            {
                Response response = IsValidPhoneNumber(txtMobileNumber.Text);
                if (!response.IsSuccess)
                {
                    lblInfo.Attributes.Add("class", "alert text-danger");
                    lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Invalid mobile number.";
                }
                else
                {
                    Response responseUserList = IUserService.GetDataByFilter(" REPLACE(REPLACE(mobile_number, '-', ''), ' ', '') = '" + txtMobileNumber.Text + "' and id_no='" + txtMobileIc.Text.Replace("-", "") + "' ", 0, 0, false);
                    if (responseUserList.IsSuccess)
                    {
                        List<User> users = ((List<User>)responseUserList.Data).Where(x => x.UserRoleId == 3).ToList();
                        if (users.Count > 0)
                        {

                            string accountNo = users.FirstOrDefault().UserIdUserAccounts.FirstOrDefault().AccountNo;
                            Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(accountNo);
                            if (responseMA.Data != null)
                            {
                                MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                if (maHolderReg.IdNo.Replace("-", "").ToString() == txtMobileIc.Text.Replace("-", "") || maHolderReg.IdNo2.Replace("-", "").ToString() == txtMobileIc.Text.Replace("-", ""))
                                {
                                    foreach (User u in users)
                                    {
                                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + u.Id + "' and status='1' ", 0, 0, false);
                                        if (responseUAList.IsSuccess)
                                        {
                                            List<UserAccount> uas = new List<UserAccount>();

                                            if (rdnUser.Checked)
                                            {
                                                opt = 1;
                                                uas = ((List<UserAccount>)responseUAList.Data).Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
                                                if (uas.Count > 0)
                                                {
                                                    userAcc = uas.FirstOrDefault();
                                                    user = u;
                                                }
                                                else
                                                {
                                                    uas = ((List<UserAccount>)responseUAList.Data).Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                                                    if (uas.Count > 0)
                                                    {
                                                        userAcc = uas.FirstOrDefault();
                                                        user = u;
                                                    }
                                                    else
                                                    {
                                                        uas = ((List<UserAccount>)responseUAList.Data).Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                                                        if (uas.Count > 0)
                                                        {
                                                            userAcc = uas.FirstOrDefault();
                                                            user = u;
                                                        }
                                                    }
                                                }
                                            }
                                            else if (rdnCorp.Checked)
                                            {
                                                opt = 2;
                                                uas = ((List<UserAccount>)responseUAList.Data).Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CORP").ToList();
                                                if (uas.Count > 0)
                                                {
                                                    userAcc = uas.FirstOrDefault();
                                                    user = u;
                                                }
                                            }

                                            if (userAcc != null)
                                            {
                                                if (IsReCaptchValid() == true)
                                                {
                                                    hdnMobilePinPop.Value = "1";
                                                }
                                                else
                                                {
                                                    lblInfo.Attributes.Add("class", "alert text-danger");
                                                    lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Please verify yourself.";
                                                }
                                            }
                                            else
                                            {
                                                lblInfo.Attributes.Add("class", "alert text-danger");
                                                lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> No MA record found.";
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                        }
                                    }
                                }
                                else
                                {
                                    lblInfo.Attributes.Add("class", "alert text-danger");
                                    lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Invalid NRIC/Passport..";
                                }
                            }

                        }
                        else
                        {
                            lblInfo.Attributes.Add("class", "alert text-danger");
                            lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> No user record found.";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUserList.Message + "\", '');", true);
                    }
                }
            }
            else
            {
                lblInfo.Attributes.Add("class", "alert text-danger");
                lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Please enter registered Mobile Number and NRIC/Passport";
            }
        }

        public static string CheckAndRequestPin(String mobileNo)
        {
            string returnString = "";

            try
            {
                if (user != null)
                {
                    if (user.Status == 1)
                    {
                        if (user.IsLoginLocked == 0)
                        {
                            if (user.ResetExpired == 1)
                            {
                                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                String result = "";
                                String title = "P/W RESET";
                                result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                Int32 length = mobileNo.Length;
                                String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);
                                user.PasswordResetCode = mobileVerificationPin;
                                user.ResetExpired = 0;
                                IUserService.UpdateData(user);

                                UserLogMain ulm = new UserLogMain()
                                {
                                    Description = "Requested OTP for reset password",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    RefId = user.Id,
                                    RefValue = mobileVerificationPin,
                                    StatusType = 1,
                                };
                                Response responseLog = IUserLogMainService.PostData(ulm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }

                                Thread PinTimerThread = new Thread(PinTimer);
                                PinTimerThread.Start();
                                returnString = "sent, " + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                            }
                            else
                            {
                                Thread PinTimerThread = new Thread(PinTimer);
                                PinTimerThread.Start();
                                returnString = "already sent, " + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                            }
                        }
                        else
                        {
                            returnString = "locked";
                        }
                    }
                    else
                    {
                        returnString = "deactivated";
                    }
                }
                else
                {
                    returnString = "no account";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("ForgotPassword Page CheckAndRequestPin: " + ex.Message);
            }
            return returnString;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PhoneVerify(string toMobileNumber)
        {
            if (opt == 1)
            {
                Response responseMAHolderRegList = ServicesManager.GetMaHolderRegByAccountNo(userAcc.AccountNo);
                if (responseMAHolderRegList.IsSuccess)
                {
                    MaHolderReg maHolderReg = (MaHolderReg)responseMAHolderRegList.Data;
                    if (maHolderReg != null)
                    {
                        return CheckAndRequestPin((userAcc.IsPrinciple == 0 ? maHolderReg.JointTelNo : maHolderReg.HandPhoneNo));
                    }
                    else
                    {
                        return "No record found";
                    }
                }
                else
                {
                    return responseMAHolderRegList.Message;
                }
            }
            else if (opt == 2)
            {
                return CheckAndRequestPin(toMobileNumber);
            }
            else
            {
                return "";
            }
        }

        protected void btnEmailSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtEmailId.Text) && !string.IsNullOrEmpty(txtICNo.Text))
                {
                    if (Utility.Helper.CustomValidator.IsValidEmail(txtEmailId.Text))
                    {
                        String emailId = txtEmailId.Text;
                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        Response responseUList = IUserService.GetDataByPropertyName(nameof(Utility.User.EmailId), emailId, true, 0, 0, false);
                        if (responseUList.IsSuccess)
                        {
                            List<User> users = ((List<User>)responseUList.Data).Where(x => x.UserRoleId == 3).ToList();
                            if (users.Count > 0)
                            {
                                User user = users.FirstOrDefault();
                                if (user.IsLoginLocked == 0)
                                {
                                    if (IsReCaptchValid() == true)
                                    {
                                        //List<UserAccount> userAccounts = new List<UserAccount>();
                                        string accountNo = user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).AccountNo;
                                        //user.UserIdUserAccounts.ForEach(x =>
                                        //{
                                        //    if (x.IsPrimary == 1)
                                        //    {
                                        //        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(x.AccountNo);
                                        //        accountNo = x.AccountNo;
                                        //        MaHolderReg maHolderReg = new MaHolderReg();
                                        //        if (responseMHR.IsSuccess)
                                        //        {
                                        //            x.MaHolderRegIdMaHolderReg = (MaHolderReg)responseMHR.Data;
                                        //            userAccounts.Add(x);
                                        //        }
                                        //    }
                                        //});
                                        Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(accountNo);
                                        if (responseMA.Data != null)
                                        {
                                            MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                            if ((user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).IsPrinciple == 1 && maHolderReg.IdNo.Replace("-", "").ToString() == txtICNo.Text.Replace("-", "")) ||
                                                (user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).IsPrinciple == 0 && maHolderReg.IdNo2.Replace("-", "").ToString() == txtICNo.Text.Replace("-", "")))
                                            {
                                                //user.UserIdUserAccounts = userAccounts;
                                                Email email = new Email();
                                                email.user = user;
                                                email.link = siteURL + "ActivateAccount.aspx";

                                                var unique_code = CustomGenerator.GenerateSixDigitPin();
                                                user.EmailCode = unique_code;

                                                user.ModifiedDate = DateTime.Now;
                                                IUserService.UpdateData(user);

                                                UserLogMain ulm = new UserLogMain()
                                                {
                                                    Description = "Requested email link for reset password",
                                                    TableName = "users",
                                                    UpdatedDate = DateTime.Now,
                                                    UserId = user.Id,
                                                    UserAccountId = 0,
                                                    RefId = user.Id,
                                                    RefValue = user.EmailId,
                                                    StatusType = 1
                                                };
                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                if (!responseLog.IsSuccess)
                                                {
                                                    //Audit log failed
                                                }

                                                EmailService.SendResetPasswordLink(email, unique_code);
                                                lblInfo.Attributes.Add("class", "alert alert-success");
                                                lblInfo.InnerHtml = "<i class='fa fa-check icon'></i>Reset password link send to Email successfully.";
                                                Response.Redirect("Index.aspx?q=ResetPasswordEmail", false);
                                            }
                                            else
                                            {
                                                lblInfo.Attributes.Add("class", "alert text-danger");
                                                lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Invalid NRIC/Passport.";
                                            }
                                        }

                                    }
                                    else
                                    {
                                        lblInfo.Attributes.Add("class", "alert text-danger");
                                        lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Please verify yourself.";
                                    }
                                }
                                else
                                {
                                    lblInfo.Attributes.Add("class", "alert text-danger");
                                    lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Account is Locked.";
                                    Session["locked"] = user.IsLoginLocked;
                                    Response.Redirect(string.Format("Index.aspx?locked={0}", user.IsLoginLocked), false);
                                }
                            }
                            else
                            {
                                lblInfo.Attributes.Add("class", "alert text-danger");
                                lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> No Account Registered with this Email.";
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        lblInfo.Attributes.Add("class", "alert text-danger");
                        lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Invalid Email.";
                    }
                }
                else
                {
                    lblInfo.Attributes.Add("class", "alert text-danger");
                    lblInfo.InnerHtml = "<i class='fa fa-close icon'></i> Please enter registered Email ID/NRIC No";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("ForgotPassword Page btnEmailSubmit_Click: " + ex.Message);
            }
        }

        protected void btnResetByMobileSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string siteURL = ConfigurationManager.AppSettings["siteURL"];
                String resetPin = txtResetPin.Text;

                Response responseUList = IUserService.GetDataByFilter("ID = '" + userAcc.UserId + "' ", 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    User user = ((List<User>)responseUList.Data).FirstOrDefault();
                    if (!string.IsNullOrEmpty(resetPin))
                    {
                        if (user.PasswordResetCode != "0")
                        {
                            if (user.ResetExpired == 0)
                            {
                                if (Int32.TryParse(resetPin.Trim(), out int result))
                                {
                                    if (Convert.ToInt32(user.PasswordResetCode) == Convert.ToInt32(resetPin))
                                    {
                                        hdnMobilePinRequested.Value = "0";
                                        user.PasswordResetCode = "0";
                                        user.ResetExpired = 1;
                                        var unique_code = CustomGenerator.GenerateSixDigitPin();
                                        user.EmailCode = unique_code;
                                        IUserService.UpdateData(user);

                                        UserLogMain ulm = new UserLogMain()
                                        {
                                            Description = "Reset password successful by OTP",
                                            TableName = "users",
                                            UpdatedDate = DateTime.Now,
                                            UserId = user.Id,
                                            UserAccountId = 0,
                                            RefId = user.Id,
                                            RefValue = unique_code,
                                            StatusType = 1
                                        };
                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                        if (!responseLog.IsSuccess)
                                        {
                                            //Audit log failed
                                        }

                                        string key = CustomEncryptorDecryptor.EncryptText(user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + user.EmailCode);
                                        Response.Redirect(siteURL + "ActivateAccount.aspx?key=" + key, false);
                                    }
                                    else
                                    {
                                        UserLogMain ulm = new UserLogMain()
                                        {
                                            Description = "Reset password failed due to Wrong OTP",
                                            TableName = "users",
                                            UpdatedDate = DateTime.Now,
                                            UserId = user.Id,
                                            UserAccountId = 0,
                                            RefId = user.Id,
                                            RefValue= resetPin,
                                            StatusType = 0
                                        };
                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                        if (!responseLog.IsSuccess)
                                        {
                                            //Audit log failed
                                        }

                                        lblOTP.InnerHtml = "Wrong OTP. Please Enter Again.";
                                    }
                                }
                                else
                                {
                                    lblOTP.InnerHtml = "OTP must be Numeric.";
                                }
                            }
                            else
                            {
                                lblOTP.InnerHtml = "OTP may Invalid or Expired.<br /> Please Request Again.";
                                hdnMobilePinRequested.Value = "0";
                            }
                        }
                        else
                        {
                            lblOTP.InnerHtml = "Please Request OTP.";
                            hdnMobilePinRequested.Value = "0";
                        }
                    }
                    else
                    {
                        lblOTP.InnerHtml = "Please Enter OTP.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("ForgotPassword Page btnResetByMobileSubmit_Click: " + ex.Message);
            }
        }

        public static Int32 SMSExpirationTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeInSeconds"]);
        public static Int32 currentSec = 0;
        public static void PinTimer()
        {
            while (currentSec <= SMSExpirationTimeInSeconds)
            {
                if (currentSec == SMSExpirationTimeInSeconds)
                {
                    Response responseUser = IUserService.GetSingle(user.Id);
                    user = (User)responseUser.Data;
                    user.ResetExpired = 1;
                    IUserService.UpdateData(user);
                    currentSec = 0;
                    break;
                }
                else
                {
                    Thread.Sleep(1000);
                    currentSec++;
                }

            }
        }

        protected void btnRequestNewPin_Click(object sender, EventArgs e)
        {
            if (txtMobileNumber.Text != null && txtMobileNumber.Text != "")
            {
                String mobileNo = txtMobileNumber.Text;
                CheckAndRequestPin(mobileNo);
            }
        }

        public bool IsReCaptchValid()
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "6LfXd58UAAAAACGUfwpdtRDFOlKFHjxCI5JSUeAd";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");

            if (status == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static Response IsValidPhoneNumber(string phoneNumber)
        {
            Response response = new Response();
            try
            {
                //will match +61 or +66- or 0 or nothing followed by a nine digit number
                bool isValid = Regex.Match(phoneNumber,
                    @"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$").Success;
                //to vary this, replace 61 with an international code of your choice 
                //or remove [\+]?61[-]? if international code isn't needed
                //{8} is the number of digits in the actual phone number less one
                if (isValid)
                {
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid mobile number";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Response responseUser = IUserService.GetSingle(user.Id);
            user = (User)responseUser.Data;
            user.PasswordResetCode = "0";
            user.ResetExpired = 1;
            IUserService.UpdateData(user);
            Response.Redirect("ForgotPassword.aspx", false);
        }
    }
}