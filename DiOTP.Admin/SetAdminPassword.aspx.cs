﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class SetAdminPassword : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeObj.Value; } }


        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());
        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        public string EmailValidityTimeInMinutes = ConfigurationManager.AppSettings["EmailValidityTimeInMinutes"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/");
            string key = Request.QueryString["key"].Trim();
            if (!string.IsNullOrEmpty(key))
            {
                key = key.Replace(" ", "+");
                key = CustomEncryptorDecryptor.DecryptText(key);
                string date = key.Split('_')[1];
                DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                if ((DateTime.Now - dateTime).TotalMinutes > Convert.ToInt32(EmailValidityTimeInMinutes))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('Link expired!'); window.location.href = 'Login.aspx'", true);
                }
                else
                {
                    String emailCode = key.Split('_')[2];
                    key = key.Split('_')[0];
                    string propName = nameof(DiOTP.Utility.User.UniqueKey);
                    Response responseUList = IUserService.GetDataByPropertyName(propName, key, true, 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        User user = ((List<User>)responseUList.Data).FirstOrDefault();
                        if (user.EmailCode == emailCode)
                        {
                            UserSecurity uS = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                            uS.IsVerified = 1;
                            uS.VerifiedDate = DateTime.Now;
                            IUserSecurityService.UpdateData(uS);
                            List<UserSecurity> uSs = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId != 1).ToList();
                            uSs.Add(uS);
                            user.UserIdUserSecurities = uSs;
                            Session["admin"] = user;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('Link expired!');window.location.href='Login.aspx'", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                    "alert('" + responseUList.Message + "');", true);
                    }
                }
            }
        }

        protected void btnSetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                var hasUpperChar = txtPassword.Text.Any(char.IsUpper);
                var hasLowerChar = txtPassword.Text.Any(char.IsLower);
                
                var rule2 = txtPassword.Text.Any(char.IsNumber);
                int rule3;
                if (txtPassword.Text.Length >= 8 && txtPassword.Text.Length <= 16)
                    rule3 = 1;
                else
                    rule3 = 0;

                User user = (User)Session["admin"];
                string newPassword = txtPassword.Text;
                string confirmPassword = txtConfirmPassword.Text;
                string message = "";
                bool isValid = true;

                if (newPassword == "" || confirmPassword == "")
                {
                    message += "Please enter your password.<br/>";
                    isValid = false;
                }
                if (newPassword != confirmPassword)
                {
                    message += "New Password and Confirm New Password must be the same.<br/>";
                    isValid = false;
                }
                if (hasUpperChar == false)
                {
                    message += "New Password must contain at least one uppercase alphabet letter.<br/>";
                    isValid = false;
                }
                if (hasLowerChar == false)
                {
                    message += "New Password must contain at least one lowercase alphabet letter.<br/>";
                    isValid = false;
                }
                if (rule2 == false)
                {
                    message += "New Password must contain at least one number.<br/>";
                    isValid = false;
                }
                if (rule3 == 0)
                {
                    message += "New Password length must between 8 to 16 characters.<br/>";
                    isValid = false;
                }
                if (CustomEncryptorDecryptor.EncryptPassword(newPassword) == user.Password)
                {
                    message += "This password has been used before, please choose another password in order to proceed.<br/>";
                    isValid = false;
                }

                if (isValid == true)
                {
                    user.Password = CustomEncryptorDecryptor.EncryptPassword(newPassword);
                    user.TransPwd = user.Password;
                    user.IsActive = 1;
                    user.Status = 1;
                    user.ModifiedDate = DateTime.Now;
                    user.ModifiedBy = user.Id;
                    Response resUpdate = IUserService.UpdateData(user);
                    if (resUpdate.IsSuccess)
                    {
                        Response responseUTList = IUserTypeService.GetDataByPropertyName(nameof(UserType.UserId), user.Id.ToString(), true, 0, 0, true);
                        if (responseUTList.IsSuccess)
                        {
                            UserType userType = ((List<UserType>)responseUTList.Data).FirstOrDefault();
                            if (userType != null)
                            {
                                userType.IsVerified = 1;
                                IUserTypeService.UpdateData(userType);
                                Response obj = IUserService.GetSingle(user.Id);
                                if (obj.IsSuccess)
                                {
                                    user = (User)obj.Data;
                                    setPassword.Visible = false;
                                    proceedLogin.Visible = true;

                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUTList.Message + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + resUpdate.Message + "');", true);
                    }
                }
                else
                {
                    divMessage.Attributes.Add("class", "alert text-danger");
                    divMessage.InnerHtml = "<i class='fa fa-close icon'></i> " + message + " ";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setpassword Page btnSetPassword_Click: " + ex.Message);
            }
        }
    }
}