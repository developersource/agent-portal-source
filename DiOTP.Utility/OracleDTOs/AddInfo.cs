﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    [Database("uts.add_info")]
    public class AddInfo
    {
        [Database("info_code"), Display("InfoCode")]
        public String InfoCode { get; set; }
        [Database("info_number"), Display("InfoNumber")]
        public int InfoNumber { get; set; }
        [Database("ref_code"), Display("RefCode")]
        public String RefCode { get; set; }
        [Database("ref_value"), Display("RefValue")]
        public String RefValue { get; set; }
        [Database("download_ind"), Display("DownloadInd")]
        public String DownloadInd { get; set; }
    }
}
