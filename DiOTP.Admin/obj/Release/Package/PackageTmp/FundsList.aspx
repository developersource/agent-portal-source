﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FundsList.aspx.cs" Inherits="Admin.FundsList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="fundsListForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                         <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                   <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully suspended" data-status="Suspended" data-original-title="Suspend" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Suspend" data-isconfirm="1"><i class="fa fa-pause"></i></button>
                                                <button data-message="Successfully deactivated" data-status="Inactive" data-original-title="Deactivate" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Deactivate" data-isconfirm="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Successfully activated" data-status="Active" data-original-title="Activate" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Activate" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                <span class="common-divider pull-right"></span>
                                                <%--<button data-original-title="Add" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Add" data-url="AddAdmin.aspx" data-title="Add Admin"><i class="fa fa-plus"></i></button>
                                                --%>
                                                <button data-original-title="Sync NAV is to fetch current NAV from backend if changed after the background process scheduled time." data-trigger="hover" data-placement="bottom" type="button" class="popovers btn btn-warning action-button pull-right sync-nav-button" data-action="SyncNAV" data-isconfirm="0"><i class="fa fa-refresh"></i> Sync NAV</button>    
                                                <button data-original-title="Configure" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="ConfigureFund" data-url="ConfigureFund.aspx" data-title="Configure Fund"><i class="fa fa-plus"></i></button>
                                                <button data-original-title="Edit" data-trigger="hover" data-placement="bottom" data-content="Select one record" type="button" class="popovers btn action-button pull-right" data-action="Edit" data-url="AddFundDetails.aspx" data-title="Edit Fund Details" data-modal-size="lg"><i class="fa fa-edit"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can search here with Fund Code and Fund Name</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can Edit/Configure/Sync Nav/Activate/Deactivate/Suspend on Funds</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable fundList" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th>Name</th>
                                            <th>Unit Price</th>
                                            <th>Change</th>
                                            <th>Change %</th>
                                            <th>Currency</th>
                                            <th>Category</th>
                                            <th>NAV Date </th>
                                            <th class="hide">Fund Class</th>
                                            <th class="hide">IsRetail </th>
                                        </tr>
                                    </thead>
                                    <tbody id="fundListTableBody" runat="server">
                                        <tr>
                                            <td>Fundname</td>
                                            <td>Unit Price</td>
                                            <td>Change</td>
                                            <td>Change %</td>
                                            <td>Category</td>
                                            <td>Nav Date</td>
                                            <td>
                                                <a href="#" class="easyui-linkbutton btn btn-success" onclick="addTab('Fund Details','FundDetails.aspx')">Fund Details</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
