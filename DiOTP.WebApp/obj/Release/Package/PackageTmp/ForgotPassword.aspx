﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="DiOTP.WebApp.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section image">
        <div class="inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="reset-text">
                            <p style="font-size: 20px !important;"><strong style="color: #000;">Note:</strong> Recover your account by any one of the options below.</p>
                        </div>
                        <br />
                        <div align="center" id="captcha" class="g-recaptcha" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="Register-section-email">
                                    <div class="register-head">
                                        <h4><strong>Recover by Email ID</strong></h4>
                                    </div>
                                    <asp:Panel ID="emailPanel" runat="server" DefaultButton="btnEmailSubmit">
                                        <div class="register-content">


                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                                <asp:TextBox ID="txtICNo" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="NRIC / OLD NRIC / PASSPORT"></asp:TextBox>
                                            </div>

                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter your Email ID"></asp:TextBox>
                                            </div>
                                            <div class="form-group mb-20" style="margin-top: 40px;">
                                                <asp:Button ID="btnEmailSubmit" runat="server" CssClass="btn login-btn btn-block" Text="Submit" ClientIDMode="Static" OnClick="btnEmailSubmit_Click" OnClientClick="return emailValidation()" />
                                            </div>
                                            <div class="fs-18 text-center">A link will be send to your email.</div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="Register-section-mobile">
                                    <div class="register-head">
                                        <h4><strong>Recover by Mobile Number</strong></h4>
                                    </div>
                                    <asp:Panel ID="phonePanel" runat="server" DefaultButton="btnMobileSubmit">
                                        <div class="register-content">

                                              <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                                <asp:TextBox ID="txtMobileIc" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="NRIC / OLD NRIC / PASSPORT"></asp:TextBox>
                                            </div>

                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone-square fa" aria-hidden="true"></i></span>
                                                <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter your Mobile Number"></asp:TextBox>
                                            </div>
                                            <asp:RadioButton ID="rdnUser" runat="server" Text="User" GroupName="usertype" ClientIDMode="Static" Checked="true" CssClass="radio-inline" Style="font-size: 12px; line-height: 20px;" />
                                            &nbsp
                                                    <asp:RadioButton ID="rdnCorp" runat="server" Text="Corporate User" GroupName="usertype" ClientIDMode="Static" CssClass="radio-inline" Style="font-size: 12px; line-height: 20px;" />
                                            <div class="form-group mb-20">
                                                <asp:Button ID="btnMobileSubmit" runat="server" CssClass="btn login-btn btn-block" Text="Submit" OnClick="btnMobileSubmit_Click" OnClientClick="return mobileValidation()" />
                                            </div>
                                            <div class="fs-18 text-center">An OTP will be send to your phone.</div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <label class="label label-info" id="lblInfo" runat="server"></label>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>



</asp:Content>
<asp:Content ContentPlaceHolderID="modalPlace" ID="modalHolder" runat="server">
    <div class="modal fade" id="modalMobilePinPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <asp:Button ID="btnExit" runat="server" Text="x" OnClick="btnExit_Click" Visible="true" Style="position: relative; float: right; background-color: transparent; border: none;" />
                <div class="modal-header text-center">
                    <strong>Reset Your Password</strong>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <%--<label>Enter your Activation Code:</label>--%>
                            <button type="button" id="btnRequest" runat="server" clientidmode="static" class="btn btn-warning" style="position: absolute; right: 15px;">Request OTP</button>
                            <asp:TextBox ID="txtResetPin" runat="server" CssClass="form-control mb-10" placeholder="Enter OTP"></asp:TextBox>
                            <asp:Button ID="btnResetByMobileSubmit" runat="server" CssClass="btn btn-block btn-info" Text="Submit" OnClick="btnResetByMobileSubmit_Click" />
                            <div class="text-center mt-10">
                                <label class="label label-info" id="lblOTP" runat="server" clientidmode="static"></label>
                            </div>
                            <div class="mt-10 countdown text-center">
                                <span id="countdownLabelText">&nbsp</span>
                                <label class="label label-warning" id="countDownTime" runat="server" clientidmode="static"></label>
                            </div>
                            <%--                            <div class="mt-10 text-center hide" id="requestNewPinLinkDiv">
                                <asp:Button ID="btnRequestNewPin" ClientIDMode="Static" runat="server" OnClick="btnRequestNewPin_Click" CssClass="text text-info" Text="Request new"></asp:Button>
                                here
                            </div>--%>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnSMSExpirationTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMobilePinPop" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMobilePinRequested" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSMSLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" ID="scriptsPlace" runat="server">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
    <script>
        function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        var pin = 0;
        var hdnMobilePinPop = parseInt($('#hdnMobilePinPop').val());
        var hdnSMSExpirationTimeInSeconds = parseInt($('#hdnSMSExpirationTimeInSeconds').val());
        var hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
        if (hdnMobilePinRequested == 1) {
            $('#btnRequest').attr('disabled', 'disabled');
            $('#countdownLabelText').html('Your OTP expires in');
            $('#countdownMAActivation').html('Your OTP expires in');
            StartTimer();
        }
        else {
            $('#btnRequest').removeAttr('disabled');
            $('#countdownMAActivation').html('');
            $('#countdownTimeMAActivation').html('');
        }
        function StartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);

            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelText').html('OTP Expired. Please Request Again.');
                    $('#countDownTime').html('');
                    $('#lblOTP').html('');
                    $('#btnRequest').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                    hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }


        $(document).ready(function () {
            if (hdnMobilePinPop == 1) {
                $('#modalMobilePinPop').modal({
                    keyboard: false,
                    backdrop: "static"
                });
                //$('#btnRequestNewPin').attr('disabled', 'disabled');
            }

            $('form').on('submit', function () {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            });
            //if (hdnSMSExpirationTimeInSeconds != 0) {
            //    var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            //    //var timer2 = "5:01";
            //    var interval = setInterval(function () {
            //        var timer = timer2.split(':');
            //        //by parsing integer, I avoid all extra string processing
            //        var hours = parseInt(timer[0], 10);
            //        var minutes = parseInt(timer[1], 10);
            //        var seconds = parseInt(timer[2], 10);
            //        --seconds;
            //        minutes = (seconds < 0) ? --minutes : minutes;
            //        hours = (minutes < 0) ? --hours : hours;
            //        if (hours < 0) {
            //            clearInterval(interval);
            //            $('.countdown').html('Your OTP Expired.');
            //            //$('#requestNewPinLinkDiv').removeClass('hide');
            //            //$('#btnRequestNewPin').removeAttr('disabled');
            //        }
            //        else {
            //            seconds = (seconds < 0) ? 59 : seconds;
            //            seconds = (seconds < 10) ? '0' + seconds : seconds;
            //            minutes = (minutes < 0) ? 59 : minutes;
            //            minutes = (minutes < 10) ? '0' + minutes : minutes;
            //            hours = (hours < 10) ? '0' + hours : hours;
            //            $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
            //            timer2 = hours + ':' + minutes + ':' + seconds;
            //        }
            //    }, 1000);

            //}

            $('#btnRequest').click(function () {
                if (hdnMobilePinPop == 1) {
                    if (hdnMobilePinRequested == 0) {
                        var m = $('#txtMobileNumber').val();
                        $.ajax({
                            url: "ForgotPassword.aspx/PhoneVerify",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            data: { toMobileNumber: JSON.stringify(m) },
                            success: function (data) {
                                var jsons = data.d.split(',');
                                var json = jsons[0];
                                //console.log(json);
                                $('#hdnMobilePinRequested').val(1);
                                if (json == "sent") {
                                    $('#lblOTP').html("OTP sent to mobile number <br/>" + jsons[1] + " successfully.");
                                    $('#countdownLabelText').html('Please Enter your OTP in');
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    StartTimer();
                                }
                                else if (json == "already sent") {
                                    $('#lblOTP').html("OTP already sent to mobile number <br/>" + jsons[1] + " .");
                                    $('#countdownLabelText').html('Please Enter your OTP in');
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    StartTimer();
                                }
                                else if (json == "locked") {
                                    $('#countdownLabelText').html('');
                                    $('#lblOTP').html("Account is Locked.");
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    $('#hdnMobilePinRequested').val(0);
                                }
                                else if (json == "deactivated") {
                                    $('#countdownLabelText').html('');
                                    $('#lblOTP').html("Account is deactivated.");
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    $('#hdnMobilePinRequested').val(0);
                                }
                                else {
                                    $('#lblOTP').html("No Account Registered with this Mobile Number.");
                                }

                            }
                        });
                    }
                }
            });
        });

        function emailValidation() {
            var isValid = true;

            $('#txtEmailId,#txtICNo').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "borderBottom": "1px solid red",
                        //"background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                }
            });
            if (isValid) {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            return isValid;
        }

        function mobileValidation() {
            var isValid = true;

            $('#txtMobileIc,#txtMobileNumber').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "borderBottom": "1px solid red",
                        //"background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                }
            });
            if (isValid) {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            return isValid;
        }

    </script>
</asp:Content>
