﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultInternalStaff.aspx.cs" Inherits="Admin.DefaultInternalStaff" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Internal Staff - FinTech Agency</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="/Content/images/favicon.html" />

    <link href="Content/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />
    <link href="Content/js/responsive-datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="Content/js/responsive-datatables/dataTables.responsive.min.css" rel="stylesheet" />
    <link href="Content/dynamic-tabs/easyui.css" rel="stylesheet" />
    <link href="Content/dynamic-tabs/icon.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link href="Content/js/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="Content/js/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link href="Content/js/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" />
    <!--icheck-->
    <link href="Content/js/iCheck/skins/square/square.css" rel="stylesheet" />
    <link href="Content/bs3/css/bootstrap.min.css" rel="stylesheet" />
    <%--<link href="Content/css/bootstrap-reset.css" rel="stylesheet" />--%>
    <link href="Content/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="Content/css/style.css" rel="stylesheet" />
    <link href="Content/mycj/mystyles.css?v=1.0" rel="stylesheet" />
    <link href="Content/mycj/My-StyleSheet.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="largeModal" class="modal">
            <div class="modal-dialog lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="largeModalTitle"></h4>
                    </div>
                    <div class="modal-body" id="largeModalContent">
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <%--<button class="btn btn-success" type="button">Save</button>--%>
                    </div>
                </div>
            </div>
        </div>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="commonModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="commonModalTitle"></h4>
                    </div>
                    <div class="modal-body" id="commonModalContent">
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button" id="btnCommonModalSubmit">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="innerLargeModal" class="modal fade">
            <div class="modal-dialog lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="innerLargeModalTitle"></h4>
                    </div>
                    <div class="modal-body" id="innerLargeModalContent">
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <%--<button class="btn btn-success" type="button">Save</button>--%>
                    </div>
                </div>
            </div>
        </div>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="innerMediumModal" class="modal fade">
            <div class="modal-dialog md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="innerMediumModalTitle"></h4>
                    </div>
                    <div class="modal-body" id="innerMediumModalContent">
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button" id="btnCommonInnerModalSubmit">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <section id="container">
            <div class="loadingDivAdmin">
                <div class="icon">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
            <div class="side-space">
                <div id="sidebar" class="nav-collapse">
                    <div class="leftside-navigation">
                        <div class="logoo">
                            <a>
                                <img src="/Content/MyImage/petraware.png" title="Petraware" class="img-responsive" /></a>
                        </div>

                        <ul class="sidebar-menu" id="navAccordion" runat="server" clientidmode="static">
                        </ul>

                        <%--<ul class="sidebar-menu" id="navAccordion" runat="server" clientidmode="static">
                            <li>
                                <a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="Dashboard.aspx" data-title="Dashboard" data-closable="false">
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-users"></i>
                                    <span>Users</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="UserAccounts.aspx" data-title="User Accounts">
                                        <span>User Accounts</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="KYCVerification.aspx" data-title="KYC Verification">
                                        <span>KYC Verification</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="AddressVerification.aspx" data-title="Address Verification">
                                        <span>Address Verification</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="BankDetailsVerification.aspx" data-title="Bank Details Verification">
                                        <span>Bank Details Verification</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="ChangeLog.aspx" data-title="Change Log">
                                        <span>Change Log</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="HardcopyRequest.aspx" data-title="Hardcopy Request">
                                        <span>Hardcopy Request</span>
                                    </a></li>
                                </ul>
                            </li>

                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-money"></i>
                                    <span>Funds</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="FundsList.aspx" data-title="Funds List">
                                        <span>Funds List</span>
                                    </a></li>
                                </ul>
                            </li>

                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-dollar"></i>
                                    <span>Transactions</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="Transactions.aspx" data-title="Transactions">
                                        <span>Transactions</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="RegularSavingPlans.aspx" data-title="Regular Saving Plan">
                                        <span>Regular Saving Enrollment</span>
                                    </a></li>
                                </ul>
                            </li>

                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span>Statements</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="StatementLinks.aspx" data-title="Statement Links">
                                        <span>Update Statement Links</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="TaxVoucher.aspx" data-title="Tax Voucher">
                                        <span>Update Tax Voucher</span>
                                    </a></li>
                                </ul>
                            </li>

                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-list-alt"></i>
                                    <span>Content Management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="BannerListing.aspx" data-title="Banner Listing">
                                        <span>Banner Listing</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="AnnouncementListing.aspx" data-title="Announcement Listing">
                                        <span>Announcement Listing</span>
                                    </a></li>
                                    <li><a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="LanguageDirectory.aspx" data-title="Language Directory">
                                        <span>Language Directory</span>
                                    </a></li>
                                </ul>
                            </li>
                        </ul>--%>
                    </div>
                </div>
            </div>

            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script src="Content/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script src="Content/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
            <script>
                $.browser = {};
                (function () {
                    $.browser.msie = false;
                    $.browser.version = 0;
                    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                        $.browser.msie = true;
                        $.browser.version = RegExp.$1;
                    }
                })();
            </script>
            <script type="text/javascript" src="Content/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
            <link href="Content/js/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet" />
            <script type="text/javascript" src="Content/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
            <script>
                var $ui = $.noConflict(true);
            </script>

            <script src="Content/js/jquery.js"></script>
            <script src="Content/bs3/js/bootstrap.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
            <script src="Content/dynamic-tabs/jquery.easyui.min.js"></script>
            <script src="Content/js/jquery.dcjqaccordion.2.7.js"></script>
            <script src="Content/js/skycons/skycons.js"></script>
            <script src="Content/js/jquery.scrollTo.min.js"></script>
            <script src="Content/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
            <script src="Content/js/jquery.nicescroll.js"></script>
            <script src="Content/js/jquery.scrollTo/jquery.scrollTo.js"></script>
            <script src="Content/js/responsive-datatables/jquery.dataTables.min.js.js"></script>
            <script src="Content/js/responsive-datatables/dataTables.responsive.min.js"></script>
            <script src="Content/js/iCheck/jquery.icheck.js"></script>
            <div id="snackbar">Some text some message..</div>
            <div class="main-space">
                <section class="top-space">
                    <div class="row">
                        <div class="col-md-12 notify-row" id="top_menu">
                            <h3>Hi <span id="userName" runat="server" class="text-capitalize">Admin</span>,</h3>
                            <ul>
                                <li>
                                    <a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="Profile.aspx" data-title="Profile">
                                        <i class="fa fa-user"></i>
                                        <span>Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="easyui-linkbutton tab-menu-item" data-url="Settings.aspx" data-title="Settings">
                                        <i class="fa fa-cog"></i>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li>
                                    <asp:LinkButton ID="btnLogout" runat="server" data-toggle="tooltip" data-placement="bottom" title="Logout" OnClick="btnLogout_Click">
                                        <i class="fa fa-power-off fs-17"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </section>

                <section id="main-content">
                    <section class="wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="tt" class="easyui-tabs">
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </section>


        <script src="Content/mycj/myscripts.js"></script>
        <script src="Content/js/scripts.js"></script>
        <script>
            $(document).ready(function () {
                $(document).on('hidden.bs.modal', function (event) {
                    if ($('.modal:visible').length) {
                        $('body').addClass('modal-open');
                    }
                    $('.colorpicker').remove();

                });
                $('body').on('click', '.tab-menu-item', function () {
                    var title = $(this).attr('data-title');
                    var url = $(this).attr('data-url');
                    var closable = $(this).attr('data-closable');
                    var showModal = $(this).attr('data-showModal');
                    if (showModal != '1') {
                        if ($('#tt').tabs('exists', title + '<span class="data-url hide">' + url + '</span>')) {
                            $('#tt').tabs('select', title + '<span class="data-url hide">' + url + '</span>');
                        } else {
                            if (closable == "false")
                                closable = false;
                            else
                                closable = true;
                            $.get(url, function (content) {
                                $('#tt').tabs('add', {
                                    title: title + '<span class="data-url hide">' + url + '</span>',
                                    content: content,
                                    closable: closable
                                });
                            }, 'html');
                        }
                    }
                    else {
                        var modalSize = $(this).attr('data-modalSize');
                        var dataId = $(this).attr('data-id');
                        var isModal = $(this).attr('data-isModal');
                        var dataAction = $(this).attr("data-action");
                        $('body').removeClass('EditFI');
                        $('body').removeClass('EditFC');
                        $('body').removeClass('EditFT');
                        $('body').removeClass('Upload');
                        $('body').addClass(dataAction);
                        if (isModal != '1') {
                            $('#largeModalContent').html("");
                            if (modalSize == 'L') {
                                $('#largeModal').modal('toggle');
                                $('#largeModalTitle').html(title);
                                $('#largeModalContent').load(url + "?id=" + dataId);
                            }
                        }
                        else {
                            $('#innerLargeModalContent').html("");
                            if (modalSize == 'L') {
                                $('#innerLargeModal').modal('toggle');
                                $('#innerLargeModalTitle').html(title);
                                $('#innerLargeModalContent').load(url + "?id=" + dataId);
                            }
                            if (modalSize == 'M') {
                                var dataInnerId = $(this).attr('data-innerId');
                                var fileTypeId = $(this).attr('data-fileTypeId');
                                $('#innerMediumModal').modal('toggle');
                                $('#innerMediumModalTitle').html(title);
                                var url = url + "?id=" + dataId + "&innerId=" + dataInnerId + "&fileTypeId=" + fileTypeId + "&titleIn=" + title;
                                var replacedurl = url.split(' ').join('%20');
                                $('#innerMediumModalContent').load(replacedurl);
                            }
                        }
                    }
                });
                $('body').on('click', '.tabs-first', function () {
                    //$.get("Dashboard.aspx", function (content) {
                    //    var selectedTab = $('#tt').tabs("getTab", 0);
                    //    $('#tt').tabs('update', {
                    //        tab: selectedTab,
                    //        options: {
                    //            content: content
                    //        }
                    //    });
                    //}, 'html');
                });
                var isAjaxActive = 0;
                var ajaxRequest;
                var refreshDashboard = function () {
                    if (isAjaxActive == 0) {
                        $('#IsNewSearch').val('1');
                        var data = $('#dashboardForm').serializeArray();
                        data = data.filter(function (item) {
                            return item.name.indexOf('_') === -1;
                        });
                        var url = "Dashboard.aspx?" + $.param(data);
                        var tab = $('#tt').tabs('getSelected');
                        var index = $('#tt').tabs('getTabIndex', tab);
                        if (index == 0) {
                            ajaxRequest = $.post(url, function (content) {
                                isAjaxActive = 0;
                                var selectedTab = $('#tt').tabs("getTab", 0);
                                $('#tt').tabs('update', {
                                    tab: selectedTab,
                                    options: {
                                        content: content
                                    }
                                });
                                // setTimeout(refreshDashboard, 2000);
                            }, 'html');
                        }
                        else {
                            // setTimeout(refreshDashboard, 2000);
                        }
                    }
                    else {
                        isAjaxActive = 1;
                        //  setTimeout(refreshDashboard, 2000);
                    }
                }
                // setTimeout(refreshDashboard, 2000);
            });
        </script>
        <script>
            function showSnackBar(message) {
                var x = $("#snackbar");
                x.html(message);
                x.addClass('show');
                setTimeout(function () {
                    x.className = x.removeClass('show');
                }, 6000); // 3000 = 3 seconds
            }
        </script>


        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fundInformation" class="modal fade">
            <div class="modal-dialog lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Fund Information</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Category</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Launch Date</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Launch Price</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Pricing Basis</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Latest NAV Price</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Historical Income Distribution</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Approved by EPF</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Shariah Complaint</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox8" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Risk Rating</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox24" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Fund Size</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox25" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Minimum Initial Investment (CASH/EPF)</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox26" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Minimum Subsequent Investment (CASH/EPF)</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox27" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Minimum RSP Investment (Initial/Additional)</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox28" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Minimum Redemption Amount</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox29" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Minimum Holding</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox30" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fundCharges" class="modal fade">
            <div class="modal-dialog sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Fund Information</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Discounted Initial Sales Charge</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="txtDISC" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Annual Management Charge</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox9" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Trustee Fee</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Switching Fee</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox11" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Redemption Fee</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox12" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Transfer Fee</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox13" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Other Significant Fees</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fundTerms" class="modal fade">
            <div class="modal-dialog lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Fund Information</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Cooling Off</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox15" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Distribution Policy</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox16" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Investment Objective</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox17" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Investment Strategy and Policy</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="TextBox18" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
