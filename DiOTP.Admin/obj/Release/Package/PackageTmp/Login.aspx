﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Admin.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="/Content/images/favicon.html" />
    <title>Login</title>
    <link href="Content/bs3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/css/bootstrap-reset.css" rel="stylesheet" />
    <link href="Content/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="Content/css/style.css" rel="stylesheet" />
    <link href="Content/css/style-responsive.css" rel="stylesheet" />
</head>

<body>

    <form class="form-signin" runat="server">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
            </div>
            <label class="checkbox">
                <asp:CheckBox ID="rememberMe" runat="server"/>Remember Me
<%--                <input type="checkbox" value="remember-me"/>
                Remember me--%>
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal">Forgot Password?</a>
                </span>
            </label>
            <asp:Button CssClass="btn btn-lg btn-login btn-block" Text="Sign in" runat="server" ID="btnSignIn" OnClientClick="return clientValidation()" OnClick="btnSignIn_Click" />
        </div>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter your Email ID"></asp:TextBox>
                    </div>
                    <div class="modal-footer">
                        <label class="label label-default pull-left" id="lblInfo" runat="server"></label>
                        <asp:Button ID="btnCancel" runat="server" data-dismiss="modal" class="btn btn-default" Text="Cancel" />
                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-success" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="Content/js/jquery.js"></script>
    <script src="Content/bs3/js/bootstrap.min.js"></script>
    <script>
        function OpenFPModal() {
            $('[href="#myModal"]').click();
        }
        function clientValidation() {
            var isValid = true;
            $('#txtUsername,#txtPassword').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": "LightBlue"
                    });
                }
            });
            if (isValid == false) {
                return false;
            }
        }

        /**
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        */
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });
    </script>

    </form>
</body>
</html>
