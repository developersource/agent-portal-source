﻿using DiOTP.OracleData;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DiOTP.WebApi.Controllers
{
    public class FundInfoController : ApiController
    {
        public Response Get(string fund_ids)
        {
            Response response = new Response();
            string query = @"SELECT A.FUND_ID,A.FUND_TYPE,A.L_NAME,B.EPF_APPROVE,
                            B.UNIT_CREATN_MIN,B.UNIT_CREATN_MAX,B.REINV_AMT_MIN,B.REINV_AMT_MAX,B.UNIT_HLDG_MAX,B.UNIT_HLDG_MIN,B.EPF_APPROVE,
                            A.COMMENCE_DT,A.CURRENCY,A.UP_SELL,B.subseq_sale_min,B.INITIAL_SALE_MIN,B.COOLING_PRD,A.NO_OF_HOLDER,A.CASH_AVAIL
                             FROM UTS.FUND A
                            INNER JOIN UTS.FUND_POLICY B ON A.FUND_ID = B.FUND_ID
                            WHERE A.FUND_ID IN("+ fund_ids + ")";
            response = NewRepoClass.GetDataByQuery(query, 0, 0, false, null, false, null, true);
            return response;
        }
    }
}
