﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxVoucher.aspx.cs" Inherits="Admin.TaxVoucher" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="taxVoucherForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <%--<div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>--%>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-original-title="Upload" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Upload" data-url="UploadTaxVoucher.aspx" data-title="Upload Tax Vouchers"><i class="fa fa-upload"></i></button>
                                                <span class="common-divider pull-right"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can Upload Tax Vouchers file with correct name(.pdf)</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Username</th>
                                            <th>Account No</th>
                                            <th>Entitlement Date</th>
                                            <th>Upload Date Time</th>
                                            <th>Tax Voucher</th>
                                        </tr>
                                    </thead>
                                    <tbody id="taxVoucherTbody" runat="server">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
