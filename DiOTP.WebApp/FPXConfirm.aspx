﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FPXConfirm.aspx.cs" Inherits="DiOTP.WebApp.FPXConfirm1" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Security.Cryptography.X509Certificates" %>
<%@ Import Namespace="DiOTP.WebApp.FPXLibary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apex</title>
    <link rel="stylesheet" type="text/css" href="/Content/assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/Content/assets/css/style.css" />
    <link href="/Content/assets/icons/fontawesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/Content/assets/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="/Content/MyCJ/My-StyleSheet.css" rel="stylesheet" />
    <link href="/Content/MyCJ/MyStyles.css?v=1.9" rel="stylesheet" />
    <link href="/Content/assets/css/Loader.css" rel="stylesheet" />
    <link href="/Content/MyCJ/Slider.css?v=1.4" rel="stylesheet" />
    <link href="/Content/MyCJ/diotp_style.css?v=1.0" rel="stylesheet" />
    <link href="Content/MyCJ/Help.css?v=1.3" rel="stylesheet" />
    <link href="/Content/MyCJ/MobileStyles.css?v=1.13" rel="stylesheet" />
    <%--<link href="Content/MyCJ/animateWhatsapp.css" rel="stylesheet" />--%>
    <script src="/Content/assets/js/modernizr.custom.js"></script>
</head>
<body class="header-dark navbar-transparent navbar-fixed with-topbar withAnimation pb-30" onload="StopLoading()">
     <%
         Controller c = new Controller();
         String checksum = "";
         Random RandomNumber = new Random();
         String fpx_msgType = "AR";
         String fpx_msgToken = "01";
         String fpx_sellerExId = "EX00008813";
         String fpx_sellerExOrderNo = Request.QueryString["transDate"].Substring(6, 6);
         String fpx_sellerOrderNo = Request.QueryString["transDate"].Substring(8, 6);
         String fpx_sellerTxnTime = Request.QueryString["transDate"];
         String fpx_sellerId = "SE00010202";
         String fpx_sellerBankCode = "01";
         String fpx_txnCurrency = "MYR";
         String fpx_txnAmount = Request.QueryString["txn_amt"];
         String fpx_buyerEmail = "";
         String fpx_buyerId = Request.QueryString["buyer_mail_id"];
         String fpx_buyerName = Request.QueryString["buyerName"];
         String fpx_buyerBankId = "TEST0021";
         String fpx_buyerBankBranch = Request.QueryString["buyerBankBranch"];
         String fpx_buyerAccNo = Request.QueryString["buyerAccNo"];
         String fpx_makerName = Request.QueryString["fpx_makerName"];
         String fpx_buyerIban = Request.QueryString["buyerIBAN"];
         String fpx_productDesc = "PD";
         String fpx_version = "6.0";
         String fpx_checkSum = "";
         fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
         fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
         fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
         fpx_checkSum = fpx_checkSum.Trim();

         checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key name 
         String finalMsg = fpx_checkSum;

%>
    <form id="form1" method="post" action="https://uat.mepsfpx.com.my/FPXMain/seller2DReceiver.jsp">
        <div class="loader-bg">
            <div class="loader">
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__ball"></div>
            </div>
        </div>

        <nav class="navbar yamm" role="navigation">
            <div class="container">
                <!-- Desktop logo---- mobile hidden -->
                <div class="navbar-header hidden-xs">
                    <a class="navbar-brand" href="/Index.aspx">
                        <img src="/Content/MyImage/logo.png" alt="Apex Logo" height="60" />
                    </a>
                </div>
                <!-- Desktop logo ends--- mobile hidden -->
                <div class="collapse navbar-collapse no-transition" id="bs-example-navbar-collapse-1 hidden-sm">
                    <ul class="nav navbar-nav navbar-right">

                        <!-- Mobile Menu Hidden Content---------- Mobile Menu Hidden Content-->

                        <li id="liLogout2" runat="server" visible="false">
                            <a href="#">
                                <img src="/Content/MyImage/1.png" height="80" />
                            </a>
                            <a href="/Portfolio.aspx" id="usernameMobile" runat="server">Username</a>
                            <asp:LinkButton ID="lnkLogout2" runat="server"> Logout</asp:LinkButton>
                        </li>

                        <li id="liMobilelogin" runat="server" visible="true">
                            <a href="#">
                                <img src="/Content/MyImage/1.png" height="70" /></a>
                            <ul class="mobile-up-menu">
                                <li>
                                    <button type="button" class="btn btn-infos ft-btn" onclick="javascript:window.location.href='Login.aspx'">Login</button>
                                </li>
                                <li id="Register2" runat="server">
                                    <button type="button" class="btn btn-infos mt-8 fot-btn" onclick="javascript:window.location.href='/Mobile-Register.aspx'">Register</button>
                                </li>
                            </ul>
                        </li>
                        <li id="mobile-footer">
                            <ul class="list-inline">
                                <li>
                                    <div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook fa-fw"></i></a></div>
                                </li>
                                <li>
                                    <div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter fa-fw"></i></a></div>
                                </li>
                                <li>
                                    <div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google +"><i class="fa fa-google-plus fa-fw"></i></a></div>
                                </li>
                            </ul>
                        </li>

                        <li id="mobile-contact-down">
                            <ul class="mobile-up-contact">
                                <li class="mobile-title">Copyright 2018 ©. All Rights Reserved.</li>
                            </ul>
                        </li>
                        <!-- Mobile Menu Hidden Content ends ---------- Mobile Menu Hidden Content ends -->

                        <!-- Desktop Menu visible------- Desktop Menu visible -->

                        <li id="liAccount" runat="server" visible="false">
                            <a href="/Portfolio.aspx" class="fs-15 fms-11" style="text-transform: none;">eApexIs</a>
                        </li>

                        <li>
                            <a href="/Funds-listing.aspx" class="fs-13 fms-11" id="menuItemFundCenter" runat="server">Fund Center</a>
                        </li>

                        <li class="dropdown diotp-dropdown">
                            <a class="dropdown-toggle fs-13 fms-11" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;">EMIS
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/Index.aspx">ELC</a></li>
                                <li><a href="/Index.aspx">Withdrawal</a></li>
                                <li><a href="/Index.aspx">Redemption</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="/Help-Center.aspx" class="fs-13 fms-11">Help</a>
                        </li>

                        <li>
                            <a href="/Contact.aspx" class="fs-13 fms-11">Contact Us</a>
                        </li>

                        <li class="dropdown search hidden-sm hidden-xs">
                            <button type="button" onclick="myFunction()" class="dropbtn">
                                <i class="fa fa-search"></i>
                            </button>
                            <div id="myDropdown" class="dropdown-content">
                                <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()" />
                                <ul id="fundSearchList">
                                </ul>
                            </div>
                        </li>

                        <li class="dropdown search hidden-sm hidden-xs" id="notificationBell" runat="server" visible="false">
                            <button type="button" class="dropbtn">
                                <div class="box">
                                    <svg class="bell" width="20px" height="8%" viewBox="0 0 21 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <title>Notification Bell</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                            <g id="Bell_SVG" sketch:type="MSLayerGroup" fill="#ffff33">
                                                <path d="M10.5,23 C12.4329966,23 14,21.4329966 14,19.5 C14,17.5670034 12.4329966,16 10.5,16 C8.56700338,16 7,17.5670034 7,19.5 C7,21.4329966 8.56700338,23 10.5,23 Z M8.48610111,19.034 C8.62220942,19.034 8.68975947,19.101 8.68975947,19.236 C8.68975947,19.73 8.87022901,20.156 9.2331845,20.493 C9.57194296,20.852 10.0024485,21.032 10.5005041,21.032 C10.6356042,21.032 10.7041625,21.099 10.7041625,21.234 C10.7041625,21.368 10.6356042,21.436 10.5005041,21.436 C9.88952902,21.436 9.3682846,21.211 8.93878727,20.785 C8.50828172,20.358 8.28244275,19.842 8.28244275,19.236 C8.28244275,19.101 8.3499928,19.034 8.48610111,19.034 Z" id="Oval-1" sketch:type="MSShapeGroup"></path>
                                                <path d="M1.60607806,19.236 L7.26415094,19.236 C7.2641511,19.2360001 13.7358494,19.2360001 13.7358491,19.236 L19.3939219,19.236 C19.8234193,19.236 20.2085554,19.079 20.5251332,18.765 C20.8417111,18.45 21,18.069 21,17.642 C19.3939219,16.296 18.1941524,14.635 17.3795189,12.659 C16.5648855,10.684 16.1575688,8.597 16.1575688,6.419 C16.1575688,5.028 15.7502521,3.95 14.9577992,3.142 C14.1431658,2.312 13.0341351,1.841 11.6317154,1.661 C11.6992654,1.526 11.7224543,1.369 11.7224543,1.212 C11.7224543,0.875 11.6085266,0.584 11.360507,0.359 C11.1336598,0.112 10.8392626,0 10.5005041,0 C10.1607374,0 9.88952902,0.112 9.66268184,0.359 C9.41365404,0.584 9.30073455,0.875 9.30073455,1.212 C9.30073455,1.369 9.32291517,1.526 9.39147343,1.661 C7.98804551,1.841 6.87901484,2.312 6.06438139,3.142 C5.24974795,3.95 4.84243123,5.028 4.84243123,6.419 C4.84243123,8.597 4.4351145,10.684 3.62048106,12.659 C2.80584762,14.635 1.60607806,16.296 0,17.642 C0,18.069 0.158288924,18.45 0.474866772,18.765 C0.79144462,19.079 1.17658073,19.236 1.60607806,19.236 Z" id="Path" sketch:type="MSShapeGroup"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </button>
                            <div id="notificationDropdown" class="dropdown-notific">
                                <ul id="notification" runat="server">
                                </ul>
                            </div>
                        </li>

                        <li id="liLogin" runat="server">
                            <button type="button" class="btn mt-26 ml-10 ft-btn" onclick="javascript:window.location.href='Index.aspx'">Login</button>
                        </li>

                        <li id="liRegister" runat="server">
                            <button type="button" class="btn btn-infos1 mt-26 ml-10" onclick="javascript:window.location.href='OTP-Activation.aspx'">Register</button>
                        </li>


                        <li class="dropdown" id="liLogout" runat="server" visible="false">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="Username" runat="server">Username</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <asp:LinkButton ID="lnkLogout" runat="server">Logout</asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                        <!-- Desktop Menu visible ends------- Desktop Menu visible ends  -->
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Wrapper content starts....
            wrapper content starts....-->

        <div id="wrapper">
            <!--- Mobile logo, login and register-->
            <div class="container">
                <div class="navbar-header visible-xs visible-sm">
                    <a class="navbar-brand" href="Index.aspx">
                        <img src="/Content/MyImage/logo.png" alt="Apex Logo" /></a>
                    <span class="mobile-account" id="mobileAccount" runat="server" clientidmode="static">
                        <a href="/Login.aspx" class="menu-login">Login</a>
                        <a href="/Mobile-Register.aspx" class="menu-register">Join OTP</a>
                    </span>

                    <button type="button" class="navbar-toggle" id="sidebar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>
            <!--- Mobile logo, login and register ends ---->
            <div class="inner-body">
                <section class="section">
                    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Cart</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head" id="cartType" runat="server">Cart</h3>
                    </div>
                </div>
                </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-20">
                    <div class="table-head">
                        <h5 class="mt-0 ff-ls text-uppercase" style="margin-bottom: 2px !important;">Payment Confirmation</h5>
                    </div>
  <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13 table-cart">
    <tbody>
      <tr>
        <td style="padding-left: 1px; padding-right: 1px;" align="left" valign="top" width="100%" colspan=2>
		<table bgcolor="#FDE6C4" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
              <tr>
                <td style="padding-top: 2px;" valign="top"><table class="infoBox" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td valign="top"><table width="100%" height="200" border="0" bgcolor="#FDE6C4">
                            <tbody>
                              <tr>
                                <td><table class="infoBox" aborder="1" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents">
                                        <td valign="top" width="99%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
											<!-- Changes Ended -->

											<tr>
                                                <td>
                                                  <table border="0" cellpadding="10" cellspacing="0" width="100%" height="100%">
                                                    <tr>
                                                      <td width="71%" align=center class="main">&nbsp;</td>
                                                    </tr>                                                   
													<tr>
                                                       <td width="71%" align='right' valign='top' class='main'><b>Total Amount</b></td>
                                                        <td width="29%" align='right' valign='top' class='main'>MYR
                                                          <input type="text" name="txn_amt" id="txn_amt" size='10' value='<%=fpx_txnAmount%>' ></td>
                                                    </tr>
                                                  </table></td>
                                              </tr>
                                            
											</tbody>
                                          </table>
                                      </tr>
                                    </tbody>
                                  </table>
								 </td>
                              </tr>    
							  
                              <tr>
                                <td><form name="form1" method="post" target='fpx'>
                                  <table border="0" width="100%">
									<!-- Submit transaction via FPX --> 
                                              <tr>
                                                <td height="164" align="center" class="main"><b>Payment Method via FPX</b>
												<p>&nbsp;</p>
												<input type="submit" style="cursor:hand" value="Click to Pay"  />
												  <p>&nbsp;</p>
                                                  <p> <img src="image/FPXButton.PNG" border="2"/></p>
                                                  <p>&nbsp;</p>
												  <p class="main">&nbsp;</p>
                                                  <p class="main"><strong>* You must have Internet Banking Account in order to make transaction using FPX.</strong></p>
                                                  <p>&nbsp;</p>
                                                  <p class="main"><strong>* Please ensure that your browser's pop up blocker has been disabled to avoid any interruption during making transaction.</strong></p>
                                                  <p>&nbsp;</p>
                                                  <p class="main"><strong>* Do not close browser / refresh page until you receive response.</strong></p>
                                                <p>&nbsp;</p>
                                                </td>
                                              </tr>              
                          </table>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td colspan="3" align="right" height="35" valign="top" width="722"><table border="0" cellpadding="0" cellspacing="0" width="722">
            <tbody>
              <tr> </tr>
              <tr>
                <td colspan="2"><table style="border: 1px solid rgb(84, 141, 212);" class="menu" cellpadding="0" cellspacing="0" height="19" width="722">
                    <tbody>
                      <tr>
                        <td align="center">&nbsp;&nbsp;Copyright © 2015 All rights reserved&nbsp;&nbsp; </td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td  colspan="2"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tbody>
                      <tr> </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <div style="display:none">
  <hr>
  <span class="infoBelow" >This parameter should be hidden from customer </span>
        <p>&nbsp;</p>
        <table width="100%" border="0" align="center" cellpadding="7" cellspacing="0">
            <tr>
                <td colspan="100"></td>
            </tr>
            <tr>
                <td class="infoBelow" align="center">
                    <p>Sending message to FPX Plugin</p>
                    <p>&nbsp;</p>
                    <textarea cols="80" rows="10"><%=checksum%> </textarea>
                    <p>&nbsp;</p>
            </tr>
            <tr>
                <td class="infoBelow" align="center">
                    <p>Receiving response from FPX Plugin</p>
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>
            </tr>
            <!--Hidden Fields to carry the values to next page  Starts-->
            <input type="hidden" value='<%=fpx_msgType%>' name="fpx_msgType">
            <input type="hidden" value='<%=fpx_msgToken%>' name="fpx_msgToken">
            <input type="hidden" value='<%=fpx_sellerExId%>' name="fpx_sellerExId">
            <input type="hidden" value='<%=fpx_sellerExOrderNo%>' name="fpx_sellerExOrderNo">
            <input type="hidden" value='<%=fpx_sellerTxnTime%>' name="fpx_sellerTxnTime">
            <input type="hidden" value='<%=fpx_sellerOrderNo%>' name="fpx_sellerOrderNo">
            <input type="hidden" value='<%=fpx_sellerBankCode%>' name="fpx_sellerBankCode">
            <input type="hidden" value='<%=fpx_txnCurrency%>' name="fpx_txnCurrency">
            <input type="hidden" value='<%=fpx_txnAmount%>' name="fpx_txnAmount">
            <input type="hidden" value='<%=fpx_buyerEmail%>' name="fpx_buyerEmail">
            <input type="hidden" value='<%=checksum%>' name="fpx_checkSum">
            <input type="hidden" value='' name="fpx_buyerName">
            <input type="hidden" value='<%=fpx_buyerBankId%>' name="fpx_buyerBankId">
            <input type="hidden" value='' name="fpx_buyerBankBranch">
            <input type="hidden" value='' name="fpx_buyerAccNo">
            <input type="hidden" value='' name="fpx_buyerId">
            <input type="hidden" value='' name="fpx_makerName">
            <input type="hidden" value='' name="fpx_buyerIban">
            <input type="hidden" value='<%=fpx_productDesc%>' name="fpx_productDesc">
            <input type="hidden" value='<%=fpx_version%>' name="fpx_version">
            <input type="hidden" value='<%=fpx_sellerId%>' name="fpx_sellerId">
            <input type="hidden" value='<%=checksum%>' name="checkSum_String">
            <input type="hidden" value='<%= Request.PhysicalApplicationPath%>'>

            <!--Hidden Fields to carry the values to next page  Ends-->


            <textarea rows="20" cols="100" name="val12" readonly><%=finalMsg%></textarea>

            <label>
                <input type="hidden" name="ItemName" value="Computer Appliances">
            </label>
        </table>
	</div>
                    </div>
                </div>
                        </div>
                    </section>
                </div>
            <!-- Footer content starts here---- footer content starts here-->
            <div class="post-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6" style="font-family: 'Montserrat' , sans-serif;">
                            Copyright 2018 © <a href="https://www.apexis.com.my/" target="_blank">Apex Investment Services Berhad.</a> All rights reserved.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="footer-list">
                                <li><a href="/TermsAndConditions.aspx" target="_blank">Terms & Conditions</a></li>
                                <li><a href="/Disclaimer.aspx" target="_blank">Disclaimer</a></li>
                                <li><a href="/PrivacyPolicy.aspx" target="_blank">Privacy Policy</a></li>
                                <li><a href="/InternetRisk.aspx" target="_blank">Internet Risk</a></li>
                                <li><a href="/TransactionNotice.aspx" target="_blank">Transaction Notice</a></li>
                                <li><a href="https://www.apexis.com.my/" target="_blank">Apex Investment Corporate Website</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer content starts here---- footer content starts here-->
        </div>
        <!-- Wrapper content ends....
            wrapper content ends....-->
        <script data-cfasync="false" src="/Content/cdn-cgi/scripts/af2821b0/cloudflare-static/email-decode.min.js"></script>
        <script src="/Content/assets/js/jquery.min.js"></script>
        <script src="/Content/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/Content/assets/js/corpress.min.js"></script>
        <script src="/Content/js/froogaloop2.min.js"></script>
        <script src="/Content/assets/twitter/js/jquery.tweet.js"></script>
        <script src="/Content/assets/form/js/contact-form.js"></script>
        <script src="/Content/assets/js/main.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script>
            function StopLoading() {
                $('.loader-bg').fadeOut(100);
            }
        </script>
    </form>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5c73ba5ca726ff2eea594977/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>
