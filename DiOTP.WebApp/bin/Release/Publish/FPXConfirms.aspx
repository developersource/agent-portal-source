﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="FPXConfirms.aspx.cs" Inherits="DiOTP.WebApp.FPXConfirms" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Security.Cryptography.X509Certificates" %>
<%@ Import Namespace="DiOTP.WebApp.FPXLibary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <%
    Controller c = new Controller();
    String checksum = "";
    Random RandomNumber = new Random();
    String fpx_msgType = "AR";
    String fpx_msgToken = "01";
    String fpx_sellerExId = "EX00008813";
    String fpx_sellerExOrderNo = Request.QueryString["transDate"];
    String fpx_sellerOrderNo = Request.QueryString["transDate"];
    String fpx_sellerTxnTime = Request.QueryString["transDate"];
    String fpx_sellerId = "SE00010202";
    String fpx_sellerBankCode = "01";
    String fpx_txnCurrency = "MYR";
    String fpx_txnAmount = Request.Form["txn_amt"];
    String fpx_buyerEmail = "";
    String fpx_buyerId = Request.QueryString["buyer_mail_id"];
    String fpx_buyerName = Request.QueryString["buyerName"];
    String fpx_buyerBankId = "TEST0021";
    String fpx_buyerBankBranch = Request.QueryString["buyerBankBranch"];
    String fpx_buyerAccNo = Request.QueryString["buyerAccNo"];
    String fpx_makerName = Request.QueryString["fpx_makerName"];
    String fpx_buyerIban = Request.QueryString["buyerIBAN"];
    String fpx_productDesc = "PD";
    String fpx_version = "6.0";
    String fpx_checkSum = "";
    fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
    fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
    fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
    fpx_checkSum = fpx_checkSum.Trim();

    checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key name 
    String finalMsg = fpx_checkSum;

%>

    <form method="post" action="https://uat.mepsfpx.com.my/FPXMain/sellerNVPReceiver.jsp">
  <table border="0" cellpadding="0" cellspacing="0" height="96%" width="722">
    <tbody>
      <tr>
        <td colspan="3" align="left" height="111"><table style="background:#FDE6C4; border: 1px solid rgb(222, 217, 197);" cellpadding="0" cellspacing="0" height="111" width="722">
            <tbody>
              <tr>
                <td><table style="border: 1px solid rgb(255, 255, 255);" border="0" cellpadding="0" cellspacing="0" height="109" width="720">
                    <tbody>
                      <tr>
                        <td align="center"><strong>SAMPLE FPX MERCHANT PAGE</strong></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td colspan="3" align="left" height="21"><table style="border: 1px solid rgb(84, 141, 212);" class="menu" cellpadding="0" cellspacing="0" height="19" width="722">
            <tbody>
              <tr>
                <td align="center"><a href=# onClick="showpage('faq')">FPX FAQ</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href=# onClick="showpage('popup')">POP UP BLOCKER SETTING</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href=# onClick="showpage('contact')">
                  CONTACT US</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <a href=# onClick=window.open("http://www.myclear.org.my/personal-fpx.html")>ABOUT FPX</a> </td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td style="padding-left: 1px; padding-right: 1px;" align="left" valign="top" width="100%" colspan=2>
		<table bgcolor="#FDE6C4" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
              <tr>
			  <td class="main" align="left" >
			  * Please disable your pop-up blocker before you proceed. <a href=# onClick="showpage('popup')">(Refer to Pop up Blocker Settings for details)</a>
			  </td>
			  </tr>
			  <tr>
                <td align="left" height="27" width="722"><p style="color: rgb(0, 0, 0); font-family: Tahoma,sans-serif; font-size: 12px; padding-left: 5px;"><b>My Shopping Cart&nbsp;&nbsp;>&nbsp;&nbsp;Transaction Details&nbsp;&nbsp;>&nbsp;&nbsp;Order Confirmation</b></p></td>
              </tr>
              <tr>
                <td style="padding-top: 2px;" valign="top"><table class="infoBox" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td valign="top"><table width="100%" height="200" border="0" bgcolor="#FDE6C4">
                            <tbody>
                              <tr>
                                <td><table class="infoBox" aborder="1" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents">
                                        <td valign="top" width="99%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                     
											<tr >
                                                <td width="50%" span=3>
													<table border="0" cellpadding="10" cellspacing="0" width="100%">
														<tbody>
														<!-- Order Details --> 
															<tr>
															<td class="main" ><b>Order Details</b> </td>
														   </tr>
														    <tr>
																<td class='main' valign='top'>Order No  </td>
																<td class='main' align='left' valign='top'><input class=productdata type="text" size='16' value='<%=fpx_sellerOrderNo%>' disabled></td> 
																<td class='main' valign='top'>Mail ID   </td>
																<td class='main' align='left' valign='top'><input class=productdata type="text" size='16' value='<%=fpx_buyerEmail%>' disabled></td> 
														    </tr>
														
														  <!-- End of List Products Chosen --> 
														</tbody>
													  </table>
													  <table border="0" cellpadding="10" cellspacing="0" width="100%" height="100%">
														<tr>
														  <td width="71%" align=center class="main">&nbsp;</td>
														</tr>                                                   
													
													  </table>
												  
												  </td>
                                              </tr>
                                            
											<!-- Changes Ended -->

											<tr>
                                                <td><table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tbody>
													<!-- List of Products Chosen --> 
                                                      <tr>
                                                        <td class="main" ><b>Products</b> </td>
                                                        <td class="smallText" align="right"><b>Quantity</b></td>
                                                        <td class="smallText" align="right"><b>Total</b></td>
                                                      </tr>
                                                     <tr>
                                                        <td class='main' valign='top'>Kensington Pocket mouse </td>
                                                        <td class='main' align='right' valign='top'>
															<input class=productdata type="text" size='4' value='1' disabled></td>
                                                        <td class='main' align='right' valign='top'>MYR
                                                          	<input class=productdata type="text" size='10' value='25.00' disabled></td>
                                                      </tr>
                                                      <tr>
                                                        <td class='main' valign='top'>Apple Keyboard </td>
                                                        <td class='main' align='right' valign='top'>
															<input class=productdata type="text" size='4' value='1' disabled></td>
                                                        <td class='main' align='right' valign='top'>MYR
                                                          	<input class=productdata type="text" size='10' value='25.00' disabled></td>
                                                      </tr>
                                                      <tr>
                                                        <td class='main' valign='top'>Earphone with Remote and Mic </td>
                                                        <td class='main' align='right' valign='top'>
															<input class=productdata type="text" size='4' value='1' disabled></td>
                                                        <td class='main' align='right' valign='top'>MYR
                                                          	<input class=productdata type="text" size='10' value='50.00' disabled></td>
                                                      </tr>
													  <!-- End of List Products Chosen --> 
                                                    </tbody>
                                                  </table>
                                                  <table border="0" cellpadding="10" cellspacing="0" width="100%" height="100%">
                                                    <tr>
                                                      <td width="71%" align=center class="main">&nbsp;</td>
                                                    </tr>                                                   
													<tr>
                                                       <td width="71%" align='right' valign='top' class='main'><b>Total Amount</b></td>
                                                        <td width="29%" align='right' valign='top' class='main'>MYR
                                                          <input type="text" name="txn_amt" id="txn_amt" size='10' value='<%=fpx_txnAmount%>' ></td>
                                                    </tr>
                                                  </table></td>
                                              </tr>
                                            
											</tbody>
                                          </table>
                                      </tr>
                                    </tbody>
                                  </table>
								 </td>
                              </tr>    
							  
                              <tr>
                                <td><form name="form1" method="post" target='fpx'>
                                  <table border="0" width="100%">
									<!-- Submit transaction via FPX --> 
                                              <tr>
                                                <td height="164" align="center" class="main"><b>Payment Method via FPX</b>
												<p>&nbsp;</p>
												<input type="submit" style="cursor:hand" value="Click to Pay"  />
												  <p>&nbsp;</p>
                                                  <p> <img src="image/FPXButton.PNG" border="2"/></p>
                                                  <p>&nbsp;</p>
												  <p class="main">&nbsp;</p>
                                                  <p class="main"><strong>* You must have Internet Banking Account in order to make transaction using FPX.</strong></p>
                                                  <p>&nbsp;</p>
                                                  <p class="main"><strong>* Please ensure that your browser's pop up blocker has been disabled to avoid any interruption during making transaction.</strong></p>
                                                  <p>&nbsp;</p>
                                                  <p class="main"><strong>* Do not close browser / refresh page until you receive response.</strong></p>
                                                <p>&nbsp;</p>
                                                </td>
                                              </tr>              
                          </table>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td colspan="3" align="right" height="35" valign="top" width="722"><table border="0" cellpadding="0" cellspacing="0" width="722">
            <tbody>
              <tr> </tr>
              <tr>
                <td colspan="2"><table style="border: 1px solid rgb(84, 141, 212);" class="menu" cellpadding="0" cellspacing="0" height="19" width="722">
                    <tbody>
                      <tr>
                        <td align="center">&nbsp;&nbsp;Copyright © 2015 All rights reserved&nbsp;&nbsp; </td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td  colspan="2"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tbody>
                      <tr> </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <div style="display:none">
  <hr>
  <span class="infoBelow" >This parameter should be hidden from customer </span>
        <p>&nbsp;</p>
        <table width="100%" border="0" align="center" cellpadding="7" cellspacing="0">
            <tr>
                <td colspan="100"></td>
            </tr>
            <tr>
                <td class="infoBelow" align="center">
                    <p>Sending message to FPX Plugin</p>
                    <p>&nbsp;</p>
                    <textarea cols="80" rows="10"><%=checksum%> </textarea>
                    <p>&nbsp;</p>
            </tr>
            <tr>
                <td class="infoBelow" align="center">
                    <p>Receiving response from FPX Plugin</p>
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>
            </tr>
            <!--Hidden Fields to carry the values to next page  Starts-->
            <input type="hidden" value='<%=fpx_msgType%>' name="fpx_msgType">
            <input type="hidden" value='<%=fpx_msgToken%>' name="fpx_msgToken">
            <input type="hidden" value='<%=fpx_sellerExId%>' name="fpx_sellerExId">
            <input type="hidden" value='<%=fpx_sellerExOrderNo%>' name="fpx_sellerExOrderNo">
            <input type="hidden" value='<%=fpx_sellerTxnTime%>' name="fpx_sellerTxnTime">
            <input type="hidden" value='<%=fpx_sellerOrderNo%>' name="fpx_sellerOrderNo">
            <input type="hidden" value='<%=fpx_sellerBankCode%>' name="fpx_sellerBankCode">
            <input type="hidden" value='<%=fpx_txnCurrency%>' name="fpx_txnCurrency">
            <input type="hidden" value='<%=fpx_txnAmount%>' name="fpx_txnAmount">
            <input type="hidden" value='<%=fpx_buyerEmail%>' name="fpx_buyerEmail">
            <input type="hidden" value='<%=checksum%>' name="fpx_checkSum">
            <input type="hidden" value='' name="fpx_buyerName">
            <input type="hidden" value='<%=fpx_buyerBankId%>' name="fpx_buyerBankId">
            <input type="hidden" value='' name="fpx_buyerBankBranch">
            <input type="hidden" value='' name="fpx_buyerAccNo">
            <input type="hidden" value='' name="fpx_buyerId">
            <input type="hidden" value='' name="fpx_makerName">
            <input type="hidden" value='' name="fpx_buyerIban">
            <input type="hidden" value='<%=fpx_productDesc%>' name="fpx_productDesc">
            <input type="hidden" value='<%=fpx_version%>' name="fpx_version">
            <input type="hidden" value='<%=fpx_sellerId%>' name="fpx_sellerId">
            <input type="hidden" value='<%=checksum%>' name="checkSum_String">
            <input type="hidden" value='<%= Request.PhysicalApplicationPath%>'>

            <!--Hidden Fields to carry the values to next page  Ends-->


            <textarea rows="20" cols="100" name="val12" readonly><%=finalMsg%></textarea>

            <label>
                <input type="hidden" name="ItemName" value="Computer Appliances">
            </label>
        </table>
	</div>
    </form>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
