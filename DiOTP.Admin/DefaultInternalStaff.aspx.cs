﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class DefaultInternalStaff : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Staff"] == null)
            {
                navAccordion.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Default.aspx'", true);
            }
            else
            {
                User user = (User)(Session["Staff"]);
                userName.InnerHtml = user.Username;

                //Identify user authority level
                bool isOfficer = false;
                bool isManager = false;
                bool isAdmin = false;
                bool isSuperAdmin = false;

                if (user.Status == 1)
                {
                    //include user type after defining
                    if (Session["isInternalStaff"] != null && Session["isInternalStaff"].ToString() == "1")
                        isOfficer = true;
                    //include user type after defining
                    if (Session["isInternalStaff"] != null && Session["isInternalStaff"].ToString() == "1")
                        isManager = true;
                    if (Session["isSuperAdmin"] != null && Session["isInternalStaff"].ToString() == "1")
                        isSuperAdmin = true;

                    StringBuilder stringBuilder = new StringBuilder();
                    //Dashboard for all.
                    stringBuilder.Append(@"<li>
                                <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='DashboardInternalStaff.aspx' data-title='Dashboard' data-closable='false'>
                                           <span> Dashboard </span>
                                       </a>
                                   </li>");

                    if(isSuperAdmin || isAdmin)
                    {
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-file-text'></i>
                                    <span>User Management</span>
                                </a>
                                <ul class='sub'>
                                   <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='InternalStaffList.aspx' data-title='Staff List'>
                                        <span>Staff List</span>
                                    </a></li></ul></li>");
                    }

                    stringBuilder.Append(@"
                                    <li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-file-text'></i>
                                    <span>Booking Activities</span>
                                </a>
                                <ul class='sub'>
                                   <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='SalesBooking.aspx' data-title='Sales Booking'>
                                        <span>Sales Booking</span>
                                    </a></li>
                                    <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='RedemptionBooking.aspx' data-title='Redemption Booking'>
                                        <span>Redemption Booking</span>
                                    </a></li>
                                    <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='SwitchBooking.aspx' data-title='Switch Booking'>
                                        <span>Switch Booking</span>
                                    </a></li>
                                    <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='TransferBooking.aspx' data-title='Transfer Booking'>
                                        <span>Transfer Booking</span>
                                    </a></li> </ul></li>
                                <li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-file-text'></i>
                                    <span>Reporting</span>
                                </a>
                                <ul class='sub'>
                                   <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Enrollments.aspx' data-title='TCR'>
                                        <span>Trade Checking Report</span>
                                    </a></li>
                                    <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Enrollments.aspx' data-title='Summary Report'>
                                        <span>Summary Report</span>
                                    </a></li></ul></li>");

                    //General Internal Staff Accessible
                    string genInternalHTML = "";
                    if (isOfficer || isManager)
                        genInternalHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Admins.aspx' data-title='Admin List'>
                                        <span>Admin List</span>
                                    </a></li>";
                    //if (isSuperAdmin || isMainAdmin || isContentAdmin)
                    //    genInternalHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='InstructorList.aspx' data-title='Instructor List'>
                    //                    <span>Instructor List</span>
                    //                </a></li>";
                    //genInternalHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='InternalStaffList.aspx' data-title='Instructor List'>
                    //                        <span>Internal Staff List</span>
                    //                        </a></li>";
                    //// Users
                    //if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-users'></i>
                    //                <span>Users</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                " + adminListHTML + @"
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='UserAccounts.aspx' data-title='Individual Accounts'>
                    //                    <span>Individual Accounts</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CorporateAccounts.aspx' data-title='Corporate Accounts'>
                    //                    <span>Corporate Accounts</span>
                    //                </a></li>
                    //                <!--<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='KYCVerification.aspx' data-title='KYC Verification'>
                    //                    <span>KYC Verification</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AddressVerification.aspx' data-title='Address Verification'>
                    //                    <span>Address Verification</span>
                    //                </a></li>-->
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AddressChanging.aspx' data-title='MA Address Verification'>
                    //                    <span>MA Address Verification</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BankDetailsVerification.aspx' data-title='Bank Details Verification'>
                    //                    <span>Bank Details Verification</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='MABanks.aspx' data-title='MA Bank Binding List'>
                    //                    <span>MA Bank Binding List</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='HardcopyRequest.aspx' data-title='Hardcopy Request'>
                    //                    <span>Hardcopy Request</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='ChangeLog.aspx?pageLength=50' data-title='Audit Log'>
                    //                    <span>Audit Log</span>
                    //                </a></li>" + (isSuperAdmin == true || isMainAdmin == true ? @"
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AdminLog.aspx?pageLength=50' data-title='Admin Log'>
                    //                    <span>AdminLog</span>
                    //                </a></li>" : "") + @"
                    //                <!--<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='DistributionInstruction.aspx' data-title='Distribution Instruction'>
                    //                    <span>Distribution Instruction</span>
                    //                </a></li>-->
                    //            </ul>
                    //        </li>");
                    //string agentListHTML = "";
                    //if (isSuperAdmin || isMainAdmin || isAdminOfficer || isAdminManager)
                    //{
                    //    agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Agents.aspx' data-title='Agent List'>
                    //                    <span>Agent List</span>
                    //                </a></li>";
                    //    agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AgentRequests.aspx' data-title='Agent Recruitment'>
                    //                    <span>Agent Recruitment</span>
                    //                </a></li>";
                    //    agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AgentRankListing.aspx' data-title='Agent Rank'>
                    //                    <span>Agent Rank Management</span>
                    //                </a></li>";
                    //    //agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Enrollments.aspx' data-title='E-Learning Enrollment'>
                    //    //                <span>Enrollments</span>
                    //    //            </a></li>";
                    //}
                    //if (isSuperAdmin || isMainAdmin || isAdminOfficer || isAdminManager)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-briefcase'></i>
                    //                <span>Agents</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                " + agentListHTML + @"
                    //            </ul>
                    //        </li>");
                    ////E-Learning

                    //if (isSuperAdmin || isMainAdmin || isAdminOfficer || isAdminManager)
                    //{
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-file-text'></i>
                    //                <span>E-Learning</span>
                    //            </a>
                    //            <ul class='sub'>
                    //               <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Enrollments.aspx' data-title='Enrollments'>
                    //                    <span>E-Learning Enrollments</span>
                    //                </a></li>");
                    //    //if (isSuperAdmin || isMainAdmin || isContentAdmin || isAdminOfficer)
                    //    //{
                    //    //    stringBuilder.Append(@"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CourseListing.aspx' data-title='Course Listing'>
                    //    //                <span>Course Listing</span>
                    //    //            </a></li>");
                    //    //}

                    //    stringBuilder.Append(@"</ul>
                    //      </li>");
                    //}
                    //if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-users'></i>
                    //                <span>Account Opening</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AccountOpeningRequest.aspx' data-title='Account Opening Requests'>
                    //                    <span>Account Opening Requests</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CorporateAccountOpeningDocs.aspx' data-title='Corporate Accounts Opening Docs'>
                    //                    <span>Corporate Accounts Opening Docs</span>
                    //                </a></li>
                                    
                    //            </ul>
                    //        </li>");

                    //// Funds
                    //if (isSuperAdmin || isMainAdmin || isContentAdmin)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-money'></i>
                    //                <span>Funds</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='FundsList.aspx' data-title='Funds List'>
                    //                    <span>Funds List</span>
                    //                </a></li>
                    //            </ul>
                    //        </li>");
                    //// Transactions
                    //if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-dollar'></i>
                    //                <span>Transactions</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Payments.aspx' data-title='Payments'>
                    //                    <span>Payments</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Transactions.aspx' data-title='Transactions'>
                    //                    <span>Transactions</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='RegularSavingPlans.aspx' data-title='Regular Saving Enrollment'>
                    //                    <span>Regular Saving Enrollment</span>
                    //                </a></li>
                    //            </ul>
                    //        </li>");
                    //// Statements
                    //if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-list'></i>
                    //                <span>Statements</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='StatementLinks.aspx' data-title='Statement Links'>
                    //                    <span>Update Statement Links</span>
                    //                </a></li>
                    //            </ul>
                    //        </li>");
                    //// Content Management
                    //StringBuilder subContentMenu = new StringBuilder();
                    //if (isSuperAdmin || isMainAdmin || isContentAdmin || isAdminOfficer)
                    //{
                    //    subContentMenu.Append(@"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CourseListing.aspx' data-title='Course Listing'>
                    //                    <span>Course Listing</span>
                    //                </a></li>");
                    //}
                    //if (isSuperAdmin || isMainAdmin || isContentAdmin)
                    //{
                    //    subContentMenu.Append(@"
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BannerListing.aspx' data-title='Banner Listing'>
                    //                    <span>Banner Listing</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionDefinitions.aspx' data-title='Commission Listing'>
                    //                    <span>Commission Listing</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionSettings.aspx' data-title='Commission Settings Listing'>
                    //                    <span>Commission Settings Listing</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AnnouncementListing.aspx' data-title='Announcement Listing'>
                    //                    <span>Announcement Listing</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item hide' data-url='LanguageDirectory.aspx' data-title='Language Directory'>
                    //                    <span>Language Directory</span>
                    //                </a></li>");
                    //}
                    //if (isSuperAdmin || isMainAdmin || isContentAdmin || isAdminOfficer)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-list-alt'></i>
                    //                <span>Content Management</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                " + subContentMenu.ToString() + @"
                    //            </ul>
                    //        </li>");

                    //if (isSuperAdmin || isMainAdmin || isContentAdmin)
                    //    stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-cogs'></i>
                    //                <span>Configuration</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='ExamSchedule.aspx' data-title='Exam Schedules'>
                    //                    <span>Exam Schedules</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionStructure.aspx' data-title='Commission Structure'>
                    //                    <span>Commission Structure</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionMappings.aspx' data-title='Commission Mappings'>
                    //                    <span>Commission Mappings</span>
                    //                </a></li>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AgentTraineeMaterialListing.aspx' data-title='Trainee Materials'>
                    //                    <span>Trainee Materials</span>
                    //                </a></li>
                    //            </ul>
                    //        </li>");

                    //if (isAdminOfficer) stringBuilder.Append(@"<li class='sub-menu'>
                    //            <a href='javascript:;'>
                    //                <i class='fa fa-cogs'></i>
                    //                <span>Configuration</span>
                    //            </a>
                    //            <ul class='sub'>
                    //                <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='ExamSchedule.aspx' data-title='Exam Schedules'>
                    //                    <span>Exam Schedules</span>
                    //                </a></li>
                    //            </ul>
                    //        </li>");
                    //<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BankList.aspx' data-title='Bank List'>
                    //                    <span>Bank List</span>
                    //                </a></li>
                    navAccordion.InnerHtml = stringBuilder.ToString();
                }
                else
                {
                    navAccordion.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Account is deactivated.'); window.location.href='Login.aspx?redirectUrl=Default.aspx'", true);
                }
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            if (loginUser != null)
            {
                //Audit Log starts here
                AdminLogMain alm = new AdminLogMain()
                {
                    Description = "Admin Logout Successful",
                    TableName = "users",
                    UpdatedDate = DateTime.Now,
                    UserId = loginUser.Id
                };
                Response responseLog = IAdminLogMainService.PostData(alm);
                if (!responseLog.IsSuccess)
                {
                    //Audit log failed
                }
                else
                {

                }
                //Audit Log Ends here

                Session.Clear();
                Session.Abandon();
            }
            else
            {

            }

            Response.Redirect("Login.aspx", false);

        }
    }
}