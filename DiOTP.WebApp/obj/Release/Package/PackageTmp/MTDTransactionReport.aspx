﻿<%@ Page Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="MTDTransactionReport.aspx.cs" Inherits="DiOTP.WebApp.AgentDashboardTemp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link href="Content/mycj/mystyles.css?v=1.0" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">        

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Reports</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">MTD Transaction Report</h3>
            </div>
            <div id="containerPage">
                <style>

                    .trTextFormat {
                        text-align: center;
                    }

                    .customBtn {
                      background-color: DodgerBlue;
                      border: none;
                      color: white;
                      padding: 11px 25px;
                      cursor: pointer;
                      font-size: 16px;
                    }

                    .ddlStyles {
                    
                        width:100%;
                    }

                    .transFilter , tbody.filterTable > tr > td {
                        border: 0px solid transparent;
                    }

                    #filterBtn, #filterCSS {
                        width:100%;
                        background-color: DodgerBlue;
                        border: none;
                        color: white;
                        padding: 5px 5px;
                        cursor: pointer;
                        font-size: 16px;
                    }

                                        /* Snack bar */
                    #snackbar {
                        visibility: hidden; /* Hidden by default. Visible on click */
                        max-width: 300px; /* Set a default minimum width */
                        margin-left: -150px; /* - Divide value of min-width by 2 */
                        background-color: #333; /* Black background color */
                        color: #fff; /* White text color */
                        text-align: center; /* Centered text */
                        border-radius: 2px; /* Rounded borders */
                        padding: 10px; /* Padding */
                        position: absolute; /* Sit on top of the screen */
                        z-index: 1050; /* Add a z-index if needed */
                        left: 50%; /* Center the snackbar */
                        top: 110px; /* 30px from the top */
                    }

                        /* Show the snackbar when clicking on a button (class added with JavaScript) */
                        #snackbar.show {
                            visibility: visible; /* Show the snackbar */
                            /* Add animation: Take 0.5 seconds to fade in and out the snackbar. 
                       However, delay the fade out process for 2.5 seconds */
                            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                            animation: fadein 0.5s, fadeout 0.5s 2.5s;
                        }

                    /* Animations to fade the snackbar in and out */
                    @-webkit-keyframes fadein {
                        from {
                            top: 0;
                            opacity: 0;
                        }

                        to {
                            top: 30px;
                            opacity: 1;
                        }
                    }

                    @keyframes fadein {
                        from {
                            top: 0;
                            opacity: 0;
                        }

                        to {
                            top: 30px;
                            opacity: 1;
                        }
                    }

                    @-webkit-keyframes fadeout {
                        from {
                            top: 30px;
                            opacity: 1;
                        }

                        to {
                            top: 0;
                            opacity: 0;
                        }
                    }

                    @keyframes fadeout {
                        from {
                            top: 30px;
                            opacity: 1;
                        }

                        to {
                            top: 0;
                            opacity: 0;
                        }
                    }
                </style>

                <%--Segment 1--%>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <h4><u>Month To Date Transactions</u></h4>
                        </div>
                        <div class="loadingDivReport">
                            <div class="icon">
                                <i class="fa fa-spinner fa-spin hide"></i>
                            </div>
                        </div>
                        <div id="snackbar">Some text some message..</div>
                    </div>


                    <div class="row ">
                        <div class="col-sm-5">
                            <table class="table table-borderless table-condensed transFilter table-responsive">
                                    <tbody class="filterTable">
                                        <tr>
                                            <td>Transaction Type:</td>
                                            <td>
                                                <select class="ddlStyles" name="ddlTransactionType" id="ddlTransactionType" clientidmode="static" runat="server">
                                                    <option value="">Select</option>
                                                    <option value="SA">Sales</option>
                                                    <option value="RD">Redemption</option>
                                                    <option value="SW">Switch</option>
                                                </select>                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Product Type:</td>
                                            <td>
                                                <select class="ddlStyles" name="ddlProductType" id="ddlProductType" clientidmode="static" runat="server">
                                                    <option value="">Select</option>
                                                    <option value="UT">Unit Trust</option>
                                                    <option value="PRS">Private Retirement Scheme</option>
                                                </select>                                                

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Plan Type:</td>
                                            <td>
                                                <select class="ddlStyles" name="ddlPlanType" id="ddlPlanType" clientidmode="static" runat="server">
                                                    <option value="">Select</option>
                                                    <option value="CS" class="UTPlanType hide">Cash</option>
                                                    <option value="EPF" class="UTPlanType hide">EPF</option>
                                                    <option value="PRS" class="PRSPlanType hide">Private Retirement Scheme</option>
                                                </select> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fund Name:</td>
                                            <td>
                                                <select class="ddlStyles" name="ddlFundList" id="ddlFundList" clientidmode="static" runat="server">
                                                    <option value="">Select</option>
                                                </select> 
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <%--<span id="filterCss"><i class="fa fa-filter"></i><asp:Button runat="server" id="filterBtn" CssClass="fa fa-filter" Text="Apply Report Filter" OnClick="filterBtn_Click"/></span>--%>
                                                <button type="submit" id="filterBtn"><i class="fa fa-filter">Apply Report Filter</i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
                    
                     <%--Segment 2--%>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="table-responsive" style="border: none!important;">
                                <table class="table table-font-size-13 table-bordered " id="transactionTable" style="width: 95% !important">
                                    <thead id="theadTransactionList" runat="server">
                                        <tr>
                                            <th>Index</th>
                                            <th>Ledger Date</th>
                                            <th>Booking Date</th>
                                            <th>MA Number</th>
                                            <th>Name</th>
                                            <th>Transaction Type</th>
                                            <th>Product Type</th>
                                            <th>Plan Type</th>
                                            <th>Fund Name</th>
                                            <th>Amount Invested/Redeemed</th>
                                            <th>Sales Charge %</th>
                                            <th>Commission Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyTransactionList" runat="server" clientidmode="Static">
                                        <tr class="trTextFormat">
                                            <td>1</td>
                                            <td>Alex Wong</td>
                                            <td>CC0014</td>
                                            <td>Active</td>
                                            <td>WM</td>
                                            <td>10,000.00</td>
                                            <td>10</td>
                                            <td>25</td>
                                            <td>Active</td>
                                            <td>25</td>
                                            <td>Active</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="pull-right" style="margin-right:75px;">
                            <button type="submit" id="downloadBtn" class="btn-submit fa fa-download customBtn"> Download Report</button>
                            </div>

                        </div>

                    </div>
                </div>

                </div>
            </div>
    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">

    <script>
            function whenPostback() {
                if ($("#ddlProductType").val() == "UT") {
                    //$("#ddlPlanType").prop("disabled", false);
                     $(".UTPlanType").removeClass("hide");
                     $(".PRSPlanType").addClass("hide");
                }
                if  ($("#ddlProductType").val() == "PRS") {
                   // $("#ddlPlanType").prop("disabled", false);
                     $(".PRSPlanType").removeClass("hide");
                     $(".UTPlanType").addClass("hide");
                }
                if($("#ddlProductType").val() == ""){
                    //$("#ddlPlanType").prop("disabled", true);
                    $("#ddlPlanType").prop("selectedIndex",0);
                     $(".PRSPlanType").addClass("hide");
                     $(".UTPlanType").addClass("hide");
                }

                console.log("hehe");
            }
        
            function showSnackBar(message) {
                var x = $("#snackbar");
                x.html(message);
                x.addClass('show');
                setTimeout(function () {
                    x.className = x.removeClass('show');
                }, 6000); // 3000 = 3 seconds
            }
    </script>
    <script>
        $(document).ready(function () {
            //if ($('#ddlProductType').val() == "UT") {
            //        //$("#ddlPlanType").prop("disabled", false);
            //        $("#ddlPlanType").prop("selectedIndex",0);
            //         $(".UTPlanType").removeClass("hide");
            //         $(".PRSPlanType").addClass("hide");
            //}
            //if ($('#ddlProductType').val() == "PRS") {
            //        //$("#ddlPlanType").prop("disabled", false);
            //        $("#ddlPlanType").prop("selectedIndex",0);
            //         $(".UTPlanType").removeClass("hide");
            //         $(".PRSPlanType").addClass("hide");
            //}
            //if ($('#ddlProductType').val() == "") {
            //        //$("#ddlPlanType").prop("disabled", false);
            //        $("#ddlPlanType").prop("selectedIndex",0);
            //         $(".UTPlanType").removeClass("hide");
            //         $(".PRSPlanType").addClass("hide");
            //}

            //To Check if user has selected an option for Product_Type to show Plan_Type based on Product_Type OnChange.
            $("#ddlProductType").click(function () {
                if ($(this).val() == "UT") {
                    //$("#ddlPlanType").prop("disabled", false);
                    $("#ddlPlanType").prop("selectedIndex",0);
                     $(".UTPlanType").removeClass("hide");
                     $(".PRSPlanType").addClass("hide");
                }
                if  ($(this).val() == "PRS") {
                   // $("#ddlPlanType").prop("disabled", false);
                    $("#ddlPlanType").prop("selectedIndex",0);
                     $(".PRSPlanType").removeClass("hide");
                     $(".UTPlanType").addClass("hide");
                }
                if($(this).val() == ""){
                    //$("#ddlPlanType").prop("disabled", true);
                    $("#ddlPlanType").prop("selectedIndex",0);
                     $(".PRSPlanType").addClass("hide");
                     $(".UTPlanType").addClass("hide");
                }

            });


            
            //Ajax for downloading
            $('#downloadBtn').click(function () {
                //variable to get all filter data
                var filter = $('#ddlTransactionType').val() + ',' + $('#ddlProductType').val() + ',' + $('#ddlPlanType').val() + ',' + $('#ddlFundList').val();
                //filter += $('#ddlTransactionType').val();
                //filter += ',' + $('#ddlProductType').val();
                //filter += ',' + $('#ddlPlanType').val();
                //filter += ',' + $('#ddlFundList').val();

                //if ($('#ddlTransactionType').val() != '' || $('#ddlTransactionType').val() != null) {
                //    filter += $('#ddlTransactionType').val();
                //}
                //if ($('#ddlProductType').val() != '' || $('#ddlProductType').val() != null) {

                //    if ($('#ddlTransactionType').val() != '' || $('#ddlTransactionType').val() != null) {
                //    filter += ',';
                //    }

                //    filter += $('#ddlProductType').val();
                //}
                //if ($('#ddlPlanType').val() != '' || $('#ddlPlanType').val() != null) {

                //    if ($('#ddlProductType').val() != '' || $('#ddlProductType').val() != null) {
                //    filter += ',';
                //    }
                //    filter += $('#ddlPlanType').val();
                //}
                //if ($('#ddlFundList').val() != '' || $('#ddlFundList').val() != null) {

                //    if ($('#ddlPlanType').val() != '' || $('#ddlPlanType').val() != null) {
                //    filter += ',';
                //    }
                //    filter += $('#ddlFundList').val();
                //}
                $('#loadingDivReport').show();
                $.ajax({
                    url: "MTDTransactionReport.aspx/Download",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: {
                        filter: JSON.stringify(filter),
                    },
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess) {
                            if (data.d.IsSuccess) {
                                var fileBase64 = data.d.Data;
                                console.log(fileBase64);
                                showSnackBar(response.Message + " Downloaded");
                                //var bindata = window.btoa(fileBase64);
                                //console.log(bindata);
                                var link = document.createElement('a');
                                link.download = (data.d.Message == null ? "download" : data.d.Message);
                                link.href = fileBase64;
                                link.click();

                                //window.location.href = fileBase64;
                                //$(elementA).remove();
                                //window.location.href = fileName;
                                //setTimeout(function () {
                                //    showSnackBar("Downloaded");
                                //}, 1000);
                                
                                $('.loadingDivReport').hide();
                            }
                            else {
                                showSnackBar(response.Message);
                                $('.loadingDivReport').hide();
                            }
                        }
                        else {
                            showSnackBar(response.Message);
                            $('.loadingDivReport').hide();
                        }
                    },
                    error: function (jqXHR, exception) {
                        $('.loader-bg').fadeOut();
                        var msg = '';
                        if (jqXHR.status === 0) {
                            msg = 'Not connect.\n Verify Network.';
                        } else if (jqXHR.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            msg = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msg = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            msg = 'Time out error.';
                        } else if (exception === 'abort') {
                            msg = 'Ajax request aborted.';
                        } else {
                            msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        ShowCustomMessage('Error', msg, '');
                    }
                });
            });
        });



    </script>
</asp:Content>

