﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using KellermanSoftware.CompareNetObjects;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;


namespace Admin
{
    public partial class CommissionDefinitions : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyfdObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyfdObj.Value; } }

        private static readonly Lazy<IUtmcFundChargeService> lazyUtmcFundChargeServiceObj = new Lazy<IUtmcFundChargeService>(() => new UtmcFundChargeService());

        public static IUtmcFundChargeService IUtmcFundChargeService { get { return lazyUtmcFundChargeServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> lazyUtmcFundFileServiceObj = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IUtmcFundFileService { get { return lazyUtmcFundFileServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        public static int idString;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mainQcount = @"Select Count(*) from commission_defs ";
            //string mainQ = @"Select * from commission_defs";

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                

                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append("where 1=1");
                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and code like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or name like '%" + Search.Value.Trim() + "%'");

                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQcount + filter.ToString()).Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                //Get data 
                string queryCommissionList = @"Select * from commission_defs ";
                Response responseCommDefList = GenericService.GetDataByQuery(queryCommissionList + filter.ToString(), 0, 0, false, null, false, null, false);
                if (responseCommDefList.IsSuccess)
                {
                    var CommissionListDyn = responseCommDefList.Data;
                    var responseCommissionListJSON = JsonConvert.SerializeObject(CommissionListDyn);
                    List<DiOTP.Utility.CommissionDefinitions> commissionList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseCommissionListJSON);
                    StringBuilder sb = new StringBuilder();
                    int index = 1;

                    commissionList.ForEach(x =>
                    {
                        sb.Append(@"<tr class='fs-12'>
                                    <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + x.id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (x.isActive == 1 ? "<span class='label label-success'>Active</span>" : (x.isActive == 0 ? "<span class='label label-danger'>Inactive</span" : ""/*"<span class='label label-danger'>Suspended</span>"*/)) + @"</span>
                                    </td>
                                    <td>" + x.code + @"</td>
                                    <td>" + x.name + @"</td>
                                </tr>");
                        index++;

                    });
                    commissionDefinitionsTableBody.InnerHtml = sb.ToString();
                }
                else {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseCommDefList.Message + "');", true);
                }
               
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            try
            {
                string idString = String.Join(",", ids);
                string query = @"Select * from commission_defs where ID in (" + idString + ")";
                Response responseCDList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);
                if (responseCDList.IsSuccess)
                {
                    int tempStatus;
                    var CDefinitionsDyn = responseCDList.Data;
                    var responseJSON = JsonConvert.SerializeObject(CDefinitionsDyn);
                    List<DiOTP.Utility.CommissionDefinitions> commDefinitions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseJSON);
                    Response responsePost = new Response();                   
                    if (action == "Deactivate")
                    {
                        string queryDFilter = @"Select * from commission_structures where commission_def_id in (" + idString + ")";
                        Response responseCSList = GenericService.GetDataByQuery(queryDFilter, 0, 0, false, null, false, null, true);
                        if (responseCSList.IsSuccess) {
                            var CStructuresDyn = responseCSList.Data;
                            var responseCSJSON = JsonConvert.SerializeObject(CStructuresDyn);
                            List<DiOTP.Utility.CommissionStructure> commStruct = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionStructure>>(responseCSJSON);

                            //if the same commission id exist and is_active means a definition is in use 
                            foreach (DiOTP.Utility.CommissionStructure structure in commStruct) {
                                if (structure.is_active == 1 && structure.commission_def_id == Convert.ToInt32(idString)) {
                                    responsePost.Message = "Definition is in use!";
                                    return responsePost;
                                }                                
                            }
                            //if (commStruct.FirstOrDefault().is_active == 1 && commStruct.FirstOrDefault().commission_def_id == Convert.ToInt32(idString))
                            //{

                            //    responsePost.Message = "Definition is in use";
                            //    return responsePost;
                            //}
                            commDefinitions.ForEach(x =>
                            {
                                tempStatus = x.isActive;
                                x.isActive = 0;
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = x.name + " Commission Definition updated",
                                    TableName = "`commission_defs`",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "is_active",
                                    ValueOld = tempStatus.ToString(),
                                    ValueNew = "0",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                                //update com def
                                responsePost = GenericService.UpdateData<DiOTP.Utility.CommissionDefinitions>(x);
                            });


                        }
                        
                    }
                    if (action == "Activate")
                    {
                        commDefinitions.ForEach(x =>
                        {
                            tempStatus = x.isActive;
                            x.isActive = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.name + " Commission Definition updated",
                                TableName = "`commission_defs`",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "is_active",
                                ValueOld = tempStatus.ToString(),
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                            responsePost = GenericService.UpdateData<DiOTP.Utility.CommissionDefinitions>(x);
                        });                        
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Commission Definition action: " + ex.Message);
                return false;
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(dynamic obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            int id = Convert.ToInt32(obj["Id"]);
            String code = obj["commissionCode"];
            String name = obj["commissionName"];

            if (code == "")
            {
                response.IsSuccess = false;
                response.Message = "Commission Code cannot be empty";
                return response;
            }
            if (name == "")
            {
                response.IsSuccess = false;
                response.Message = "Commission Name cannot be empty";
                return response;
            }

            try
            {
                if (HttpContext.Current.Session["admin"] != null)
                {
                    if (id == 0)
                    {
                        DiOTP.Utility.CommissionDefinitions commissionDefinitionObj1 = new DiOTP.Utility.CommissionDefinitions
                        {

                            code = code,
                            name = name,
                            isActive = 1,
                            createdDate = DateTime.Now,
                            createdBy = loginUser.Id,
                            updatedDate = DateTime.Now,
                            updatedBy = loginUser.Id,
                            status = 1
                        };
                        List<DiOTP.Utility.CommissionDefinitions> commissionDefinitionList = new List<DiOTP.Utility.CommissionDefinitions>();
                        commissionDefinitionList.Add(commissionDefinitionObj1);

                        //DiOTP.Utility.CommissionDefinitions commissionDefinitionObj2 = new DiOTP.Utility.CommissionDefinitions
                        //{

                        //    code = CommissionCode,
                        //    name = CommissionName,
                        //    isActive = 1,
                        //    createdDate = DateTime.Now,
                        //    createdBy = loginUser.Id,
                        //    updatedDate = DateTime.Now,
                        //    updatedBy = loginUser.Id,
                        //    status = 1
                        //};

                        //commissionDefinitionList.Add(commissionDefinitionObj2);
                        //DiOTP.Utility.CommissionDefinitions commissionDefinitionObj3 = new DiOTP.Utility.CommissionDefinitions
                        //{

                        //    code = CommissionCode,
                        //    name = CommissionName,
                        //    isActive = 1,
                        //    createdDate = DateTime.Now,
                        //    createdBy = loginUser.Id,
                        //    updatedDate = DateTime.Now,
                        //    updatedBy = loginUser.Id,
                        //    status = 1
                        //};
                        //commissionDefinitionList.Add(commissionDefinitionObj3);
                        Response responsePost = GenericService.PostBulkData<DiOTP.Utility.CommissionDefinitions>(commissionDefinitionList);
                        if (responsePost.IsSuccess)
                        {
                            response.IsSuccess = true;
                            response.Message = "Successfully added";
                        }
                        else
                        {
                            response = responsePost;
                        }
                    }
                    else
                    {
                        string queryAReg = (@" SELECT * FROM commission_defs where id='" + id + "' ");
                        Response responseCList = GenericService.GetDataByQuery(queryAReg, 0, 0, false, null, false, null, false);
                        if (responseCList.IsSuccess)
                        {
                            var commissionsDyn = responseCList.Data;
                            var responseJSON = JsonConvert.SerializeObject(commissionsDyn);
                            List<DiOTP.Utility.CommissionDefinitions> commissionList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseJSON);
                            if (commissionList.Count == 1)
                            {
                                DiOTP.Utility.CommissionDefinitions comissionObj = commissionList.FirstOrDefault();
                                comissionObj.name = name;
                                comissionObj.code = code;
                                comissionObj.updatedBy = loginUser.Id;
                                comissionObj.updatedDate = DateTime.Now;
                                Response responseUpdate = GenericService.UpdateData<DiOTP.Utility.CommissionDefinitions>(comissionObj);
                                if (!responseUpdate.IsSuccess)
                                {
                                    response = responseUpdate;
                                }
                                else {
                                    response.IsSuccess = true;
                                    response.Message = "Successfully added";
                                }
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Commission Definition not found";
                            }
                        }
                        else
                        {
                            response = responseCList;
                        }
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired!";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }


    }


}