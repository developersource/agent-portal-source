﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="DiOTP.WebApp.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section image">
        <div class="inner">
            <div class="container">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4 col-sm-6">
                                <div class="Register-section">
                                    <div class="register-head">
                                        <h4><strong>Change Password</strong></h4>
                                    </div>
                                    <div class="register-content">
                                        <div id="divMessage" runat="server"></div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key fa" aria-hidden="true"></i></span>
                                            <asp:TextBox TextMode="Password" ID="txtOldPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your Old Password"></asp:TextBox>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key fa" aria-hidden="true"></i></span>
                                            <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your New Password"></asp:TextBox>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                                            <asp:TextBox TextMode="Password" ID="txtConfirmPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Confirm your Password"></asp:TextBox>
                                        </div>
                                        <div class="form-group mb-20">
                                            <asp:Button ID="btnChangePassword" runat="server" CssClass="btn btn-block login-btn" Text="Register" OnClick="btnChangePassword_Click" />
                                        </div>
                                        <hr class="my-hr" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
