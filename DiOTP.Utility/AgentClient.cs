﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_clients")]
    public class AgentClient
    {

        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_user_id"), Display("agent_user_id")]
        public Int32 agent_user_id { get; set; }
        [Database("agent_code"), Display("agent_code")]
        public String agent_code { get; set; }
        [Database("ipd_fund_code"), Display("ipd_fund_code")]
        public String ipd_fund_code { get; set; }
        [Database("user_id"), Display("user_id")]
        public Int32 user_id { get; set; }
        [Database("is_account_opening"), Display("is_account_opening")]
        public Int32 is_account_opening { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("updated_date")]
        public DateTime? updated_date { get; set; }
        [Database("updated_by"), Display("updated_by")]
        public Int32? updated_by { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
