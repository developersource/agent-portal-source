using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("transaction_ledger")]
    public class TransactionLedger
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("order_no"), Display("OrderNo")]
        public String OrderNo { get; set; }
        [Database("trans_no"), Display("TransNo")]
        public String TransNo { get; set; }
        [Database("transaction_code_id"), Display("TransactionCodeId")]
        public Int32 TransactionCodeId { get; set; }
        [Database("amount"), Display("Amount")]
        public Decimal? Amount { get; set; }
        [Database("units"), Display("Units")]
        public Decimal Units { get; set; }
        [Database("payment_method"), Display("PaymentMethod")]
        public Int32 PaymentMethod { get; set; }
        [Database("order_status"), Display("OrderStatus")]
        public Int32 OrderStatus { get; set; }
        [Database("is_printed"), Display("IsPrinted")]
        public Int32 IsPrinted { get; set; }
        [Database("is_downloaded"), Display("IsDownloaded")]
        public Int32 IsDownloaded { get; set; }
        [Database("version"), Display("Version")]
        public Int32 Version { get; set; }

        [Database("order_placed_date"), Display("OrderPlacedDate")]
        public DateTime OrderPlacedDate { get; set; }
        [Database("order_payment_date"), Display("OrderPaymentDate")]
        public DateTime OrderPaymentDate { get; set; }
        [Database("order_confirm_date"), Display("OrderConfirmDate")]
        public DateTime OrderConfirmDate { get; set; }
        [Database("order_sync_date"), Display("OrderSyncDate")]
        public DateTime OrderSyncDate { get; set; }
        [Database("ref_no"), Display("RefNo")]
        public String RefNo { get; set; }
    }
}
