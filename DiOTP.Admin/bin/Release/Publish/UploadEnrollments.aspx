﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadEnrollments.aspx.cs" Inherits="Admin.UploadEnrollments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="uploadEnrollmentsForm" runat="server">
        <div class="row" id="FormId" data-value="uploadEnrollmentsForm">
            <div class="col-md-12">
                <input type="file" name="statements" id="statements" class="form-control" />
            </div>
            <div class="col-md-12 statementStatus hide">
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th style='width: 50px;'>S. No.</th>
                            <th>Status</th>
                            <th>Agent Code</th>
                            <th>Attended</th>
                            <th>Passed</th>
                        </tr>
                    </thead>
                    <tbody id="statementUploadResult">
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
