﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiOTP.Utility.CustomClasses;

using System.Threading.Tasks;
using DiOTP.Utility;
using System.Net.Http;
using System.Configuration;

namespace DiOTP.PolicyAndRules
{
    public class ServiceManager
    {
        public static Response GetMaHolderRegByAccountNo(string accNo)
        {
            Response responseCustom = new Response();
            MaHolderReg maHolderReg = new MaHolderReg();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    filter.Append(" left join UTS.ADD_INFO a on a.INFO_NUMBER = h.HOLDER_NO and a.REF_CODE='09' where h.holder_no = '" + accNo + "'");
                    var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            List<MaHolderReg> maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                            maHolderReg = maHolderRegs.FirstOrDefault();
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = maHolderReg;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static List<HolderLedger> GetHolderLedgerByHolderNo(string filter)
        {
            List<HolderLedger> holderLedgers = new List<HolderLedger>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderLedgers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderLedger>>(responseFromApi.Data.ToString());
                }
            }
            return holderLedgers;
        }

    }
}
