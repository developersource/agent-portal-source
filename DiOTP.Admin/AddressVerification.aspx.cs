﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using MoreLinq;
using DiOTP.Utility.CustomClasses;
using System.Net.Mail;

namespace Admin
{
    public partial class AddressVerification : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserFileService> lazyUserFileServiceObj = new Lazy<IUserFileService>(() => new UserFileService());

        public static IUserFileService IUserFileService { get { return lazyUserFileServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {

                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                StringBuilder jfilter = new StringBuilder();
                
                filter.Append(" users.status='1' and user_files.user_file_type_id=2");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameUsername = Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.Username));
                    filter.Append(" and (users." + columnNameUsername + " like '%" + Search.Value.Trim() + "%'");
                    string columnNameEmail = Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.EmailId));
                    filter.Append(" or users." + columnNameEmail + " like '%" + Search.Value.Trim() + "%')");
                }
                filter.Append(" group by users.id");
                jfilter.Append(" user_files.user_id=users.id ");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IUserService.GetCountByUT(filter.ToString(), jfilter.ToString(), "user_files").ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseUList = IUserService.GetDataByUT(filter.ToString(), skip, take, true, jfilter.ToString(), "user_files");
                if (responseUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUList.Data;
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (User user in users)
                    {
                        Response responseUF = IUserFileService.GetDataByFilter(" user_id='" + user.Id + "' and user_file_type_id=2", 0, 0, true);
                        if (responseUF.IsSuccess)
                        {
                            UserFile userFile = ((List<UserFile>)responseUF.Data).FirstOrDefault();
                            if (userFile != null)
                            {
                                asb.Append(@"<tr>
                             <td class='icheck'>
                                <div class='square single-row'>
                                    <div class='checkbox'>
                                        <input type='checkbox' name='checkRow' class='checkRow' value='" + userFile.Id + @"' /> <label>" + index + @"</label><br/>
                                    </div>
                                </div>
                                <span class='row-status'>" +
                                        (userFile.Status == 1 ?
                                            "<span class='label label-success'>Approved</span>" :
                                            userFile.Status == 0 ?
                                                "<span class='label label-warning'>Pending</span>" :
                                                "<span class='label label-danger'>Rejected</span>")
                                        + @"</span>
                            </td>
                                <td>" + user.Username + @"</a>
                                <td>" + user.EmailId + @"</td>
                                <td><a href='" + userFile.Url + @"' target='_blank'><img height='100' src='" + userFile.Url + @"' title='" + userFile.Title + @"' /></a></td>
                            </tr>"
                                    );
                                index++;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseUF.Message + "');", true);
                        }
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
            }
        }
       
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            Response response = new Response();
            string message = "";
            try
            {
                string idString = String.Join(",", ids);
                Response responseUFList = IUserFileService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUFList.IsSuccess)
                {
                    List<UserFile> userFiles = (List<UserFile>)responseUFList.Data;
                    if (action == "Activate")
                    {
                        userFiles.ForEach(x =>
                        {
                            if (x.Status == 0)
                            {
                                Response responseUList = IUserService.GetSingle(x.UserId);
                                if (responseUList.IsSuccess)
                                {
                                    User userX = (User)responseUList.Data;

                                    Email email = new Email();
                                    email.user = userX;
                                    EmailService.SendUpdateMail(email, "Address Proof status notification", "New address proof is approved", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                }
                                x.Status = 1;
                                message += "" + x.Title + " Approved.<br/>";
                            }
                            else if (x.Status == 99)
                            {
                                message += "" + x.Title + " is already rejected.<br/>";
                            }
                            else if (x.Status == 1)
                            {
                                message += "" + x.Title + " is already approved.<br/>";
                            }
                        });
                        IUserFileService.UpdateBulkData(userFiles);
                    }
                    if (action == "Reject")
                    {
                        userFiles.ForEach(x =>
                        {
                            if (x.Status == 0)
                            {
                                Response responseUList = IUserService.GetSingle(x.UserId);
                                if (responseUList.IsSuccess)
                                {
                                    User userX = (User)responseUList.Data;

                                    Email email = new Email();
                                    email.user = userX;
                                    EmailService.SendUpdateMail(email, "Address Proof status notification", "New address proof is rejected", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                }
                                x.Status = 99;
                                message += "" + x.Title + " Rejected.<br/>";
                            }
                            else if (x.Status == 99)
                            {
                                message += "" + x.Title + " is already rejected.<br/>";
                            }
                            else if (x.Status == 1)
                            {
                                message += "" + x.Title + " is already approved.<br/>";
                            }
                        });
                        IUserFileService.UpdateBulkData(userFiles);
                    }
                }
                response.IsSuccess = true;
                response.Message = message;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.WriteLine("User accounts action: " + ex.Message);
            }
            return response;
        }
    }
}