﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class News : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazySiteContentObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazySiteContentObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["Id"] != null)
                {
                    Int32 Id = Convert.ToInt32(Request.QueryString["Id"]);
                    Response response = ISiteContentService.GetSingle(Id);
                    if (response.IsSuccess)
                    {
                        SiteContent sc = (SiteContent)response.Data;
                        contentTitle.InnerHtml = sc.Title;
                        contentDate.InnerHtml = sc.CreatedDate.ToString("dd-MM-yyyy");
                        contentContent.InnerHtml = sc.Content;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}