﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("occupation_requests")]
    public class OccupationRequests 
    {
        [Database("id")]
        public Int32 Id { get; set; }
        [Database("holder_no")]
        public String HolderNo { get; set; }
        [Database("name_of_employer")]
        public String NameOfEmployer { get; set; }
        [Database("addr_1")]
        public String Addr1 { get; set; }
        [Database("addr_2")]
        public String Addr2 { get; set; }
        [Database("addr_3")]
        public String Addr3 { get; set; }
        [Database("postcode")]
        public String Postcode { get; set; }
        [Database("city")]
        public String City { get; set; }
        [Database("state")]
        public String State { get; set; }
        [Database("country")]
        public String Country { get; set; }
        [Database("office_tel_no_2")]
        public String OfficeTelNo2 { get; set; }
        [Database("Isemployed")]
        public String Isemployed { get; set; }
        [Database("Status")]
        public String Status { get; set; }
        [Database("StatusInfo")]
        public String StatusInfo { get; set; }




    }
}
