using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class LanguageDirRepo : ILanguageDirRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(LanguageDir obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<LanguageDir>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<LanguageDir> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<LanguageDir>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(LanguageDir obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<LanguageDir>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<LanguageDir> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<LanguageDir>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public LanguageDir DeleteData(Int32 Id)
        {
            Response responseLD = GetSingle(Id);
            if (responseLD.IsSuccess)
            {
                LanguageDir obj = (LanguageDir)responseLD.Data;
                try
                {
                    string query = obj.ObjectToQuery<LanguageDir>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseLD = GetSingle(Id);
                    if (responseLD.IsSuccess)
                    {
                        LanguageDir obj = (LanguageDir)responseLD.Data;
                        query += obj.ObjectToQuery<LanguageDir>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            LanguageDir obj = new LanguageDir();
            try
            {
                string query = "select * from language_dirs where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                        select new LanguageDir
                        {
                            Id = x.Field<Int32>("id"),
                            Module = x.Field<Int32>("module"),
                            Remarks = x.Field<String>("remarks"),
                            Status = x.Field<Int32>("status"),
                            TextEn = x.Field<String>("text_en"),
                            TextId = x.Field<String>("text_id"),
                            TextMs = x.Field<String>("text_ms"),
                            TextPage = x.Field<String>("text_page"),
                            TextZh = x.Field<String>("text_zh"),
                        }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<LanguageDir> objs = new List<LanguageDir>();
            try
            {
                string query = "select * from language_dirs";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new LanguageDir
                        {
                            Id = x.Field<Int32>("id"),
                            Module = x.Field<Int32>("module"),
                            Remarks = x.Field<String>("remarks"),
                            Status = x.Field<Int32>("status"),
                            TextEn = x.Field<String>("text_en"),
                            TextId = x.Field<String>("text_id"),
                            TextMs = x.Field<String>("text_ms"),
                            TextPage = x.Field<String>("text_page"),
                            TextZh = x.Field<String>("text_zh"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from language_dirs";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<LanguageDir>(propertyName);
            List<LanguageDir> objs = new List<LanguageDir>();
            try
            {
                string query = "select * from language_dirs where " + columnName + " = '" + propertyValue + "'";
                if(!isEqual)
                    query = "select * from language_dirs where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new LanguageDir
                        {
                         Id = x.Field<Int32>("id"),
                         Module = x.Field<Int32>("module"),
                         Remarks = x.Field<String>("remarks"),
                         Status = x.Field<Int32>("status"),
                         TextEn = x.Field<String>("text_en"),
                         TextId = x.Field<String>("text_id"),
                         TextMs = x.Field<String>("text_ms"),
                         TextPage = x.Field<String>("text_page"),
                         TextZh = x.Field<String>("text_zh"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<LanguageDir>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from language_dirs where " + columnName + " = '" + propertyValue + "'";
                if(!isEqual)
                    query = "select count(*) from language_dirs where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<LanguageDir> objs = new List<LanguageDir>();
            try
            {
                string query = "select * from language_dirs where " + filter +"";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new LanguageDir
                        {
                            Id = x.Field<Int32>("id"),
                            Module = x.Field<Int32>("module"),
                            Remarks = x.Field<String>("remarks"),
                            Status = x.Field<Int32>("status"),
                            TextEn = x.Field<String>("text_en"),
                            TextId = x.Field<String>("text_id"),
                            TextMs = x.Field<String>("text_ms"),
                            TextPage = x.Field<String>("text_page"),
                            TextZh = x.Field<String>("text_zh"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from language_dirs where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
