﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCommissionDefinitions.aspx.cs" Inherits="Admin.AddCommissionDefinitions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addCommissionDefinitionsForm" runat="server">
        <div id="FormId" data-value="addCommissionDefinitionsForm">
            <div class="row">
                <div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Commission Code:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="text" name="commissionCode" id="commissionCode" runat="server" clientidmode="static" class="form-control" placeholder="Enter a Commission Code" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Commission Name:</label>
                        </div>
                        <div class="col-md-9">
                            <textarea placeholder="Enter Commission Name" rows="3" class="form-control" name="commissionName" id="commissionName" runat="server"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
