﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class HardcopyRequest : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserService = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserService.Value; } }

        private static readonly Lazy<IUserHardcopyRequestService> lazyUserHardcopyRequestService = new Lazy<IUserHardcopyRequestService>(() => new UserHardcopyRequestService());

        public static IUserHardcopyRequestService IUserHardcopyRequestService { get { return lazyUserHardcopyRequestService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyIUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyIUserAccountObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (!IsPostBack)
                {
                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void RefreshData()
        {
            try
            {
                if (Session["user"] != null)
                {
                    User sessionUser = (User)Session["user"];

                    string Id = Request.QueryString["ID"];

                    if (!IsPostBack)
                    {
                        ddlUserAccountId.Items.Clear();

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + sessionUser.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            foreach (UserAccount x in userAccounts)
                                ddlUserAccountId.Items.Add(new ListItem(x.AccountNo.ToString(), x.Id.ToString()));

                            if (Id != null)
                                ddlUserAccountId.SelectedIndex = ddlUserAccountId.Items.IndexOf(ddlUserAccountId.Items.FindByValue(Id));
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }

                    //check if user hardcopyrequest requested existed.

                    Response responseUHRList = IUserHardcopyRequestService.GetDataByPropertyName(nameof(UserHardcopyRequest.UserId), sessionUser.Id.ToString(), true, 0, 0, true);
                    if (responseUHRList.IsSuccess)
                    {
                        List<UserHardcopyRequest> ExistingRequest = (List<UserHardcopyRequest>)responseUHRList.Data;
                        UserHardcopyRequest accountHardCopy = ExistingRequest.Where(x => x.UserAccountId == Convert.ToInt32(ddlUserAccountId.SelectedValue) && x.Status != 88).FirstOrDefault();

                        //if not found, auto insert a new record as hardcopy request activated.

                        if (accountHardCopy == null)
                        {
                            UserHardcopyRequest x = new UserHardcopyRequest();
                            x.UserId = sessionUser.Id;
                            x.UserAccountId = Convert.ToInt32(ddlUserAccountId.SelectedValue);
                            x.IsActivated = 1;
                            x.CreatedDate = DateTime.Now;
                            x.Status = 1;

                            Response response = IUserHardcopyRequestService.PostData(x);
                            x = (UserHardcopyRequest)response.Data;
                            //refresh page once inserted new record
                            Response.Redirect("HardcopyRequest.aspx?ID=" + ddlUserAccountId.SelectedValue, false);
                        }
                        else
                        {
                            if (accountHardCopy.IsActivated == 0)
                            {
                                hardcopyActivation.Visible = true;
                                hardcopyDeactivation.Visible = false;
                            }
                            else if (accountHardCopy.IsActivated == 1)
                            {
                                hardcopyActivation.Visible = false;
                                hardcopyDeactivation.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUHRList.Message + "\", '');", true);
                    }
                }
                else
                    Response.Redirect("Index.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.WriteLog("HardcopyRequest Page RefreshData: " + ex.Message);
            }
        }

        protected void btnActivate_Click(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                User sessionUser = (User)Session["user"];

                Response response = IUserAccountService.GetSingle(Convert.ToInt32(ddlUserAccountId.SelectedValue));
                if (response.IsSuccess)
                {
                    UserAccount ua = (UserAccount)response.Data;

                    ua.HardcopyUpdatedDate = DateTime.Now;
                    ua.IsHardcopy = 1;
                    Response response2 = IUserAccountService.UpdateData(ua);
                    ua = (UserAccount)response2.Data;

                    UserHardcopyRequest uhr = new UserHardcopyRequest
                    {
                        UserId = sessionUser.Id,
                        UserAccountId = ua.Id,
                        IsActivated = 1,
                        CreatedDate = DateTime.Now,
                        Status = 1,
                    };

                    Response response1 = IUserHardcopyRequestService.PostData(uhr);
                    uhr = (UserHardcopyRequest)response1.Data;

                    string siteURL = ConfigurationManager.AppSettings["siteURL"];
                    try
                    {
                        using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", sessionUser.EmailId))
                        {
                            mm.Subject = "Hard Copy Request Activated";
                            string body = "Hello ," + sessionUser.Username;
                            body += "<br /><br />MA Acoount - " + ddlUserAccountId.SelectedValue + " hardcopy request activated";
                            body += "<br />" + DateTime.Now;
                            body += "<br /><br />Thanks";
                            mm.Body = body;
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "mail.petraware.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog("HardcopyRequest Page btnActivate_Click: " + ex.Message);
                    }
                }
                Response.Redirect(Request.RawUrl, false);
            }
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                User sessionUser = (User)Session["user"];

                Response response = IUserAccountService.GetSingle(Convert.ToInt32(ddlUserAccountId.SelectedValue));
                if (response.IsSuccess)
                {
                    UserAccount ua = (UserAccount)response.Data;

                    ua.HardcopyUpdatedDate = DateTime.Now;
                    ua.IsHardcopy = 0;
                    Response response2 = IUserAccountService.UpdateData(ua);
                    ua = (UserAccount)response2.Data;

                    UserHardcopyRequest uhr = new UserHardcopyRequest
                    {
                        UserId = sessionUser.Id,
                        UserAccountId = ua.Id,
                        IsActivated = 0,
                        CreatedDate = DateTime.Now,
                        Status = 1,
                    };

                    Response response1 = IUserHardcopyRequestService.PostData(uhr);
                    uhr = (UserHardcopyRequest)response1.Data;

                    string siteURL = ConfigurationManager.AppSettings["siteURL"];
                    try
                    {
                        using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", sessionUser.EmailId))
                        {
                            mm.Subject = "Hard Copy Request Deactivated";
                            string body = "Hello ," + sessionUser.Username;
                            body += "<br /><br />MA Acoount - " + ddlUserAccountId.SelectedValue + " hardcopy request deactivated";
                            body += "<br />" + DateTime.Now;
                            body += "<br /><br />Thanks";
                            mm.Body = body;
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "mail.petraware.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog("HardcopyRequest Page btnDeactivate_Click: " + ex.Message);
                    }
                    
                }
                Response.Redirect(Request.RawUrl, false);
            }
        }

        protected void ddlUserAccountId_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("HardcopyRequest.aspx?ID=" + ddlUserAccountId.SelectedValue, false);
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("Settings.aspx", false);
        }
    }
}