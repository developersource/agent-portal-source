﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class EditFundInfo : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyUtmcFundDetailServiceObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyUtmcFundDetailServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                string idString = Request.QueryString["id"];
                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);
                    Response response = IUtmcFundInformationService.GetSingle(id);
                    if (response.IsSuccess)
                    {
                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)response.Data;

                        Response responseUFDList = IUtmcFundDetailService.GetDataByPropertyName(nameof(UtmcFundDetail.UtmcFundInformationId), utmcFundInformation.Id.ToString(), true, 0, 0, false);
                        if (responseUFDList.IsSuccess) {
                            UtmcFundDetail ufd = ((List<UtmcFundDetail>)responseUFDList.Data).FirstOrDefault();

                            txtCategory.Text = utmcFundInformation.LipperCategoryOfFund;
                            txtLaunchDate.Text = ufd.LaunchDate.ToString("dd/MM/yyyy");
                            txtLaunchPrice.Text = ufd.LaunchPrice.ToString();
                            txtPriceBasis.Text = ufd.PricingBasis;
                            txtLatestNAVPrice.Text = ufd.LatestNavPrice.ToString();
                            ddlHistoricalIncomeDistribution.SelectedValue = ufd.HistoricalIncomeDistribution.ToString();
                            ddlApprovedByEPF.SelectedValue = ufd.IsEpfApproved.ToString();
                            ddlShariahCompliant.SelectedValue = ufd.ShariahCompliant.ToString();
                            txtRiskRating.Text = ufd.RiskRating;
                            txtFundSize.Text = ufd.FundSizeRm;
                            txtMinInitialInvestmentCASH.Text = ufd.MinInitialInvestmentCash.ToString();
                            txtMinInitialInvestmentEPF.Text = ufd.MinInitialInvestmentEpf.ToString();
                            txtMinSubsequentInvestmentCASH.Text = ufd.MinSubsequentInvestmentCash.ToString();
                            txtMinSubsequentInvestmentEPF.Text = ufd.MinSubsequentInvestmentEpf.ToString();
                            txtMinRSPInvestmentInitial.Text = ufd.MinRspInvestmentInitialRm.ToString();
                            txtMinRSPInvestmentAdditional.Text = ufd.MinRspInvestmentAdditionalRm.ToString();
                            txtMinRedemptionAmount.Text = ufd.MinRedAmountUnits.ToString();
                            txtMinHolding.Text = ufd.MinHoldingUnits.ToString();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUFDList.Message + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + response.Message + "');", true);
                    }
                }
            }
        }
    }
}