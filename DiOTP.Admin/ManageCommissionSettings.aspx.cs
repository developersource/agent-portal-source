﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;

namespace Admin
{
    public partial class ManageCommissionSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] != null)
            {
                User sessionUser = (User)Session["admin"];
                string idString = Request.QueryString["Id"];

                //Populate dropdown value of commission settings
                string queryCSL = @"Select * from commission_settings";
                Response responseCSettingsList = GenericService.GetDataByQuery(queryCSL, 0, 0, false, null, false, null, true);
                if (responseCSettingsList.IsSuccess)
                {
                    var csDyn = responseCSettingsList.Data;
                    var responseCSJSON = JsonConvert.SerializeObject(csDyn);
                    List<DiOTP.Utility.CommissionSettings> cs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionSettings>>(responseCSJSON);

                    ddlCommissionSetting.DataSource = cs;
                    ddlCommissionSetting.DataTextField = "code";
                    ddlCommissionSetting.DataValueField = "id";
                    ddlCommissionSetting.DataBind();
                    ddlCommissionSetting.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                }

                if (idString != "0" && idString != null && idString != "")
                {
                    //to populate data for edit
                    string queryEdit = (@"SELECT cs.* from commission_structures cs where cs.is_active = 1 order by cs.commission_settings_id, cs.apply_to");
                    Response responseCStructureList = GenericService.GetDataByQuery(queryEdit, 0, 0, false, null, false, null, true);
                    if (responseCStructureList.IsSuccess)
                    {
                        var cStructureDyn = responseCStructureList.Data;
                        var responseCStructureJSON = JsonConvert.SerializeObject(cStructureDyn);
                        List<DiOTP.Utility.CommissionStructureDetail> commStruct = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionStructureDetail>>(responseCStructureJSON);

                        var Structures = commStruct;
                        var StructureGrouped = Structures.GroupBy(
                            x => new { x.commission_settings_id, x.apply_to },
                            x => x, (key, y) => new { structId = key.commission_settings_id, structApplyTo = key.apply_to, percentValues = y.ToList() });

                        List<String> percentList = new List<String>();
                        for (int i = 0; i < StructureGrouped.FirstOrDefault().percentValues.Count; i++) {

                            percentList.Add(StructureGrouped.FirstOrDefault().percentValues[i].percent.ToString());
                        }

                        ddlCommissionSetting.Value = StructureGrouped.FirstOrDefault().structId.ToString();
                        ddlApplyTo.SelectedValue = StructureGrouped.FirstOrDefault().structApplyTo.ToString();
                        Id.Value = idString;
                        //var getPercentById = Structures.ToLookup(x => x.id, x => x.percent);
                        //var percentById = getPercentById[Convert.ToInt32(idString)];

                        string queryPopulate = (@"SELECT * from commission_defs where is_active = 1 ");
                        Response responseCDList = GenericService.GetDataByQuery(queryPopulate, 0, 0, false, null, false, null, true);
                        if (responseCDList.IsSuccess)
                        {
                            var cdDyn = responseCDList.Data;
                            var responseCDJSON = JsonConvert.SerializeObject(cdDyn);
                            List<DiOTP.Utility.CommissionDefinitions> cd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseCDJSON);

                            StringBuilder asb = new StringBuilder();
                            int percentIndex = 0;
                            int index = 1;
                            int objIndex = 3;
                            foreach (DiOTP.Utility.CommissionDefinitions comdef in cd)
                            {
                                asb.Append(@" 
                            <div class='col-md-8'>
                                <label class='fs-14 mt-2'>" + comdef.code + "-" + comdef.name + @"</label>
                            </div>
                            <div class='col-md-4'>
                                <input type = 'text' class='hide' value ='" + comdef.id + @"' id='comdefId" + objIndex + @"' name='comdefId" + objIndex + @"' runat='server' clientidmode='static'/>
                                <input type = 'text' value='"+ percentList[percentIndex] +@"' placeholder='Enter Percentage' class='form-control' name='txt" + objIndex + @"' id='txt" + objIndex + @"' runat='server'/>
                            </div>
                        ");
                                index++;
                                objIndex++;
                                percentIndex++;
                            }
                            commDefDetails.InnerHtml = asb.ToString();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseCDList.Message + "\");", true);
                        }
                    }
                }
                else {
                    string queryPopulate = (@"SELECT * from commission_defs where is_active = 1 ");
                    Response responseCDList = GenericService.GetDataByQuery(queryPopulate, 0, 0, false, null, false, null, true);
                    if (responseCDList.IsSuccess)
                    {
                        var cdDyn = responseCDList.Data;
                        var responseCDJSON = JsonConvert.SerializeObject(cdDyn);
                        List<DiOTP.Utility.CommissionDefinitions> cd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseCDJSON);

                        StringBuilder asb = new StringBuilder();
                        int index = 1;
                        int objIndex = 3;
                        double commPercent = 0.0;
                        foreach (DiOTP.Utility.CommissionDefinitions comdef in cd)
                        {
                            if (ddlApplyTo.SelectedValue == "SGM" ) {

                            }

                            asb.Append(@" 
                            <div class='col-md-8'>
                                <label class='fs-14 mt-2'>" + comdef.code + "-" + comdef.name + @"</label>
                            </div>
                            <div class='col-md-4'>
                                <input type = 'text' class='hide' value ='" + comdef.id + @"' id='comdefId" + comdef.id + @"' name='comdefId" + comdef.id + @"' runat='server' clientidmode='static'/>
                                <input type = 'text' value='0' placeholder='Enter Percentage' class='form-control' name='txt" + comdef.id + @"' id='txt" + comdef.id + @"' runat='server'/>
                            </div>
                        ");
                            index++;
                            objIndex++;
                        }
                        commDefDetails.InnerHtml = asb.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseCDList.Message + "\");", true);
                    }
                }
            }

        }
    }
}