using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("banks_def")]
    public class BanksDef
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("isin"), Display("Isin")]
        public String Isin { get; set; }
        [Database("code"), Display("Code")]
        public String Code { get; set; }
        [Database("short_name"), Display("ShortName")]
        public String ShortName { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("no_format"), Display("NoFormat")]
        public String NoFormat { get; set; }
        public List<MaHolderBank> BankDefIdMaHolderBanks { get; set; }
    }
}
