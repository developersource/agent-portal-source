﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class Email
    {
        public string fromEmail { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string password { get; set; }
        public string host { get; set; }
        public int port { get; set; }
        public User user { get; set; }
        public string link { get; set; }
        public string fpxResponseDesc { get; set; }
        public string rejectReason { get; set; }
    }
}
