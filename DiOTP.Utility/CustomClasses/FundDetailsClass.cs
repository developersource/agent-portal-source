﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class FundDetailsClass
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("fund_code"), Display("FundCode")]
        public String FundCode { get; set; }
        [Database("fund_name"), Display("FundName")]
        public String FundName { get; set; }
        [Database("utmc_fund_categories_def_id"), Display("UtmcFundCategoriesDefId")]
        public Int32 UtmcFundCategoriesDefId { get; set; }
        [Database("effective_date"), Display("EffectiveDate")]
        public DateTime EffectiveDate { get; set; }
        [Database("lipper_category_of_fund"), Display("LipperCategoryOfFund")]
        public String LipperCategoryOfFund { get; set; }
        [Database("conventional"), Display("Conventional")]
        public String Conventional { get; set; }
        [Database("status"), Display("Status")]
        public String Status { get; set; }
        [Database("foreign_fund"), Display("ForeignFund")]
        public String ForeignFund { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime ReportDate { get; set; }
        [Database("fund_base_currency"), Display("FundBaseCurrency")]
        public String FundBaseCurrency { get; set; }
        [Database("is_emis"), Display("IsEmis")]
        public Int32? IsEmis { get; set; }
        [Database("fund_cls"), Display("FundCls")]
        public String FundCls { get; set; }
        [Database("is_retail"), Display("IsRetail")]
        public Int32? IsRetail { get; set; }
        [Database("sat_group"), Display("SatGroup")]
        public String SatGroup { get; set; }
        [Database("is_rsp"), Display("IsRsp")]
        public Int32? IsRsp { get; set; }
        //
        [Database("utmc_fund_information_id"), Display("UtmcFundInformationId")]
        public Int32 UtmcFundInformationId { get; set; }
        [Database("launch_date"), Display("LaunchDate")]
        public DateTime LaunchDate { get; set; }
        [Database("relaunch_date"), Display("RelaunchDate")]
        public DateTime? RelaunchDate { get; set; }
        [Database("launch_price"), Display("LaunchPrice")]
        public Decimal LaunchPrice { get; set; }
        [Database("pricing_basis"), Display("PricingBasis")]
        public String PricingBasis { get; set; }
        [Database("investment_objective"), Display("InvestmentObjective")]
        public String InvestmentObjective { get; set; }
        [Database("investment_strategy_and_policy"), Display("InvestmentStrategyAndPolicy")]
        public String InvestmentStrategyAndPolicy { get; set; }
        [Database("latest_nav_price"), Display("LatestNavPrice")]
        public Decimal LatestNavPrice { get; set; }
        [Database("historical_income_distribution"), Display("HistoricalIncomeDistribution")]
        public Int32 HistoricalIncomeDistribution { get; set; }
        [Database("is_epf_approved"), Display("IsEpfApproved")]
        public Int32 IsEpfApproved { get; set; }
        [Database("shariah_compliant"), Display("ShariahCompliant")]
        public Int32 ShariahCompliant { get; set; }
        [Database("risk_rating"), Display("RiskRating")]
        public String RiskRating { get; set; }
        [Database("fund_size_rm"), Display("FundSizeRm")]
        public String FundSizeRm { get; set; }
        [Database("min_initial_investment_cash"), Display("MinInitialInvestmentCash")]
        public Int32 MinInitialInvestmentCash { get; set; }
        [Database("min_initial_investment_epf"), Display("MinInitialInvestmentEpf")]
        public Int32 MinInitialInvestmentEpf { get; set; }
        [Database("min_subsequent_investment_cash"), Display("MinSubsequentInvestmentCash")]
        public Int32 MinSubsequentInvestmentCash { get; set; }
        [Database("min_subsequent_investment_epf"), Display("MinSubsequentInvestmentEpf")]
        public Int32 MinSubsequentInvestmentEpf { get; set; }
        [Database("min_rsp_investment_initial_rm"), Display("MinRspInvestmentInitialRm")]
        public Int32 MinRspInvestmentInitialRm { get; set; }
        [Database("min_rsp_investment_additional_rm"), Display("MinRspInvestmentAdditionalRm")]
        public Int32 MinRspInvestmentAdditionalRm { get; set; }
        [Database("min_red_amount_units"), Display("MinRedAmountUnits")]
        public Int32 MinRedAmountUnits { get; set; }
        [Database("min_holding_units"), Display("MinHoldingUnits")]
        public Int32 MinHoldingUnits { get; set; }
        [Database("colling_off_period"), Display("CollingOffPeriod")]
        public String CollingOffPeriod { get; set; }
        [Database("distribution_policy"), Display("DistributionPolicy")]
        public String DistributionPolicy { get; set; }
        //
        [Database("initial_sales_charges_percent"), Display("InitialSalesChargesPercent")]
        public Decimal InitialSalesChargesPercent { get; set; }
        [Database("annual_management_charge_percent"), Display("AnnualManagementChargePercent")]
        public Decimal AnnualManagementChargePercent { get; set; }
        [Database("trustee_fee_percent"), Display("TrusteeFeePercent")]
        public Decimal TrusteeFeePercent { get; set; }
        [Database("switching_fee_percent"), Display("SwitchingFeePercent")]
        public Decimal SwitchingFeePercent { get; set; }
        [Database("redemption_fee_percent"), Display("RedemptionFeePercent")]
        public Decimal RedemptionFeePercent { get; set; }
        [Database("transfer_fee_rm"), Display("TransferFeeRm")]
        public Decimal TransferFeeRm { get; set; }
        [Database("other_significant_fee_rm"), Display("OtherSignificantFeeRm")]
        public Decimal OtherSignificantFeeRm { get; set; }
        [Database("epf_sales_charges_percent"), Display("EpfSalesChargesPercent")]
        public Decimal EpfSalesChargesPercent { get; set; }
        [Database("gst_percent"), Display("GstPercent")]
        public Decimal GstPercent { get; set; }
    }
}
