﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="TrainingRoadmap.aspx.cs" Inherits="DiOTP.WebApp.TrainingRoadmap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        .container-roadmap {
            background: #eff7f8;
            margin: 0 auto 30px auto;
            position: relative;
            box-shadow: 2px 5px 5px rgba(119, 119, 119, 0.3);
/*            height: calc(100vh - 302px);*/
        }

        .leftbox {
            left: 10px;
            position: absolute;
            width: 15px;
            height: 100%;
            background-color: #059cce;
            box-shadow: 1px 1px 2px rgba(119, 119, 119, 0.5);
        }

        .rightbox {
            height: 100%;
            margin-left: 40px;
        }

        .rb-container {
            margin: auto;
            display: block;
            position: relative;
        }

            .rb-container ul.rb {
                margin: 2.5em 0;
                padding: 0;
                display: inline-block;
            }

                .rb-container ul.rb li {
                    list-style: none;
                    margin: auto;
                    margin-left: 50px;
                    min-height: 50px;
                    border-left: 1px dashed #059cce;
                    padding: 0 0 50px 30px;
                    position: relative;
                }

                    .rb-container ul.rb li:last-child {
                        border-left: 0;
                    }

                    .rb-container ul.rb li::before {
                        position: absolute;
                        left: -10px;
                        top: -5px;
                        content: " ";
                        border: 8px solid #059cce;
                        border-radius: 500%;
                        background: #fff;
                        height: 20px;
                        width: 20px;
                        transition: all 500ms ease-in-out;
                    }

                    .rb-container ul.rb li:hover::before {
                        border-color: #fff;
                        background: #059cce;
                        transition: all 1000ms ease-in-out;
                    }

        ul.rb li .timestamp {
            color: #059cce;
            position: relative;
            width: 100px;
            font-size: 12px;
        }

        .item-title {
        }

        .container-3 {
            width: 5em;
            vertical-align: right;
            white-space: nowrap;
            position: absolute;
        }

            .container-3 .icon {
                margin: 1.3em 3em 0 31.5em;
                position: absolute;
                width: 150px;
                height: 30px;
                z-index: 1;
                color: #4f5b66;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/AgentDashboard.aspx">Dashboard</a></li>
                    <li class="active">e-Learning</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">e-Learning</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a class="text-primary" href="/ELearning.aspx"><i class="fa fa-arrow-circle-left mt-20 mr-10"></i><span>Back</span></a>
                    <div class="container-roadmap">
                        <div class="leftbox">
                        </div>
                        <div class="rightbox">
                            <div class="rb-container">
                                <ul class="rb" id="timelineUL" runat="server">
                                    <li class="rb-item">
                                        <div class="timestamp">
                                            3rd May 2020<br>
                                            7:00 PM
                                        </div>
                                        <div class="item-title">Chris Serrano posted a photo on your wall.</div>

                                    </li>
                                    <li class="rb-item">
                                        <div class="timestamp">
                                            19th May 2020<br>
                                            3:00 PM
                                        </div>
                                        <div class="item-title">Mia Redwood commented on your last post.</div>

                                    </li>

                                    <li class="rb-item">
                                        <div class="timestamp">
                                            17st June 2020<br>
                                            7:00 PM
                                        </div>
                                        <div class="item-title">Lucas McAlister just send you a message.</div>

                                    </li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
