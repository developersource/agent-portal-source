﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentDashboard1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = (User)(Session["user"]);
            if (user != null)
            {
                //To instantiate hidden agent code for ajax
                agentCode.Value = user.AgentCode;
                string agentPos = "";
                string agentGroup = "";
                //StringBuilder asb = new StringBuilder();
                string queryGetAgentPos = @"SELECT agent_pos,agent_group FROM agent_positions where agent_code = '" + user.AgentCode + "'";
                Response responseAgentDetailList = GenericService.GetDataByQuery(queryGetAgentPos, 0, 0, false, null, false, null, false);
                if (responseAgentDetailList.IsSuccess)
                {
                    var agentDetailDyn = responseAgentDetailList.Data;
                    var responseAgentJSON = JsonConvert.SerializeObject(agentDetailDyn);
                    List<AgentPosition> agentDetailsObjList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentJSON);
                    //List<dynamic> agentDetailsObjList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentJSON);
                    AgentPosition agentObj = agentDetailsObjList.FirstOrDefault();

                    agentPos = agentObj.AgentPos;
                    agentGroup = agentObj.AgentGroup;
                }

                //To Populate CPD Points
                string mainQE = @"SELECT SUM(att.cpd_points) as cpd_points FROM agent_training_topics att
                inner join agent_training_enrollments ate on att.id = ate.agent_training_topic_id
                where (ate.is_reported=1 and ate.is_attended=1) and ((att.is_assessment = 1 and ate.is_passed=1) or (att.is_assessment=0 and ate.is_passed=0)) and ate.user_id=" + user.Id + " and YEAR(ate.created_date)=" + DateTime.Now.Year + ";";
                Response responseEList = GenericService.GetDataByQuery(mainQE, 0, 0, false, null, false, null, true);
                if (responseEList.IsSuccess)
                {
                    var CourseEDyn = responseEList.Data;
                    var responseEJSON = JsonConvert.SerializeObject(CourseEDyn);
                    List<dynamic> CPDPointsObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseEJSON);
                    if (CPDPointsObj.Count > 0)
                    {
                        if (CPDPointsObj.FirstOrDefault().cpd_points < 16) {
                            lblCPDPoints.InnerHtml = "<span style='color:red;'>" + CPDPointsObj.FirstOrDefault().cpd_points + "/16</span>";
                        }
                        if (CPDPointsObj.FirstOrDefault().cpd_points >= 16)
                        {
                            lblCPDPoints.InnerHtml = "<span style='color:green;'>" + CPDPointsObj.FirstOrDefault().cpd_points + "/16</span>";
                        }
                    }
                    else
                    {
                        lblCPDPoints.InnerHtml = "<span style='color:red;'>0/16</span>";
                    }
                    CPDYear.InnerHtml = "(" + DateTime.Now.Year + ")";
                }

                //To populate the Transactions for AUM data
                //string queryGetMaHolder = @"select GROUP_CONCAT(ua.account_no) as account_no, ap.agent_pos, ap.agent_code,ar.user_id
                //                            from di_otp_ag.agent_positions ap
                //                            join agent_regs ar on ap.agent_code = ar.agent_code
                //                            join agent_reg_personal arp on ar.id = arp.agent_reg_id
                //                            join agent_clients ac on ac.agent_user_id = ar.user_id
                //                            join user_accounts ua on ua.user_id = ac.user_id
                //                            where ap.agent_pos like '" + agentPos + "%' group by ar.user_id order by char_length(agent_pos) asc";
                string queryGetMaHolder = @"select GROUP_CONCAT(ua.account_no) as account_no, ap.agent_pos, ap.agent_code,ar.user_id
                                            from agent_positions ap
                                            inner join agent_regs ar on ap.agent_code = ar.agent_code
                                            inner join agent_reg_personal arp on ar.id = arp.agent_reg_id
                                            inner join agent_clients ac on ac.agent_user_id = ar.user_id
                                            inner join user_accounts ua on ua.user_id = ac.user_id
                                            where ap.agent_group = '" + agentGroup + "' group by ar.user_id order by char_length(agent_pos) asc";
                Response responseGetMaHolder = GenericService.GetDataByQuery(queryGetMaHolder, 0, 0, false, null, false, null, true);
                if (responseGetMaHolder.IsSuccess)
                {
                    var maHolderDyn = responseGetMaHolder.Data;
                    var responseMaHolderDyn = JsonConvert.SerializeObject(maHolderDyn);
                    List<dynamic> maHolderAccountList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseMaHolderDyn);
                    string personalMaHolderAcc = "";
                    string downlineMaHolderAcc = "";
                    string indirectDownlineMaHolderAcc = "";
                    string allMaHolderAcc = "";
                    double totalGroup = 0.0;
                    double personalGroup = 0.0;
                    double personal = 0.0;
                    int agentPosCounter = 0;
                    int directCount = 0;
                    int indirectCount = 0;
                    maHolderAccountList.ForEach(x => {

                        //if same agentPos, personal
                        if (x.agent_pos == agentPos)
                        {
                            personalMaHolderAcc = x.account_no;
                            agentPosCounter = Convert.ToString(x.agent_pos).Length;
                        }
                        //if it is initial + 3 = downline. Else indirect.
                        else if (Convert.ToString(x.agent_pos).Length == (agentPosCounter + 3))
                        {
                            //if == 0, it is first acc
                            if (directCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            downlineMaHolderAcc = x.account_no;
                            directCount++;
                        }
                        else
                        {
                            //if == 0, it is first acc
                            if (indirectCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            indirectDownlineMaHolderAcc = x.account_no;
                            indirectCount++;
                        }
                    });

                    //get all ma holder accounts
                    allMaHolderAcc = personalMaHolderAcc + (!String.IsNullOrEmpty(downlineMaHolderAcc) ? "," + downlineMaHolderAcc : "") + (!String.IsNullOrEmpty(indirectDownlineMaHolderAcc) ? "," + indirectDownlineMaHolderAcc : "");

                    //To get current holdings from Oracle for all
                    string queryGetAllPCH = @"select curr_unit_hldg as current_unit_holdings, holder_no from UTS.holder_inv where holder_no in (" + allMaHolderAcc + ")";
                    Response responseGetAllPCH = ServicesManager.GetDataByQuery(queryGetAllPCH);
                    if (responseGetAllPCH.IsSuccess)
                    {
                        var getAllPCHDyn = responseGetAllPCH.Data;
                        var responseGetAllPCHData = JsonConvert.SerializeObject(getAllPCHDyn);
                        List<dynamic> allCHList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetAllPCHData);

                        //Sort personal,personal group, total group
                        personal += allCHList.Where(x => x.HOLDERNO.ToString().Contains(personalMaHolderAcc)).Sum(x => (double)x.CURRENTUNITHOLDINGS);

                        //Personal group
                        if (!String.IsNullOrEmpty(downlineMaHolderAcc))
                        {
                            //personalGroup += allCHList.Where(x => x.HOLDERNO.ToString().Contains(downlineMaHolderAcc)).Sum(x => (double)x.CURRENTUNITHOLDINGS) + personal;
                            personalGroup += allCHList.Where(x => downlineMaHolderAcc.Contains(x.HOLDERNO.ToString())).Sum(x => (double)x.CURRENTUNITHOLDINGS) + personal;

                        }
                        else
                        {
                            personalGroup += personal;
                        }

                        //Total group
                        if (!String.IsNullOrEmpty(indirectDownlineMaHolderAcc))
                        {
                            totalGroup += allCHList.Where(x => indirectDownlineMaHolderAcc.Contains(x.HOLDERNO.ToString())).Sum(x => (double)x.CURRENTUNITHOLDINGS) + personalGroup;
                        }
                        else
                        {
                            totalGroup += personalGroup;
                        }


                        lblPersonalVal.Text = personal.ToString("N", CultureInfo.CurrentCulture);
                        lblPersonalGroupVal.Text = personalGroup.ToString("N", CultureInfo.CurrentCulture);
                        lblTotalGroupVal.Text = totalGroup.ToString("N", CultureInfo.CurrentCulture);
                    }

                    //personal
                    //    if (!String.IsNullOrEmpty(personalMaHolderAcc))
                    //    {
                    //        //To get current holdings from Oracle for personal
                    //        string queryGetPCH = @"select Sum(curr_unit_hldg) as current_unit_holdings from UTS.holder_inv where holder_no in (" + personalMaHolderAcc + ")";
                    //        Response responseGetPCH = ServicesManager.GetDataByQuery(queryGetPCH);
                    //        if (responseGetPCH.IsSuccess)
                    //        {
                    //            var getPCHDyn = responseGetPCH.Data;
                    //            var responseGetPCHData = JsonConvert.SerializeObject(getPCHDyn);
                    //            List<dynamic> personalCHList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetPCHData);

                    //            lblPersonalVal.Text = personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? "0.00" : Convert.ToDouble(personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS).ToString("N", CultureInfo.CurrentCulture);
                    //            personalGroup += (personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? 0.00 : Convert.ToDouble(personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS));
                    //            totalGroup += personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? "0.00" : Convert.ToDouble(personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        lblPersonalVal.Text = "0.00";
                    //    }

                    //    if (!String.IsNullOrEmpty(downlineMaHolderAcc))
                    //    {
                    //        //To get current holdings from Oracle for group
                    //        string queryGetDirectPGCH = @"select Sum(curr_unit_hldg) as current_unit_holdings from UTS.holder_inv where holder_no in (" + downlineMaHolderAcc + ")";
                    //        Response responseGetDirectPGCH = ServicesManager.GetDataByQuery(queryGetDirectPGCH);
                    //        if (responseGetDirectPGCH.IsSuccess)
                    //        {
                    //            var getDirectPGCHDyn = responseGetDirectPGCH.Data;
                    //            var responseGetDirectPGCHData = JsonConvert.SerializeObject(getDirectPGCHDyn);
                    //            List<dynamic> personalGroupCHList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetDirectPGCHData);

                    //            //personalGroup += personalGroupCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ?? 0;
                    //            personalGroup += (personalGroupCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? 0.00 : Convert.ToDouble(personalGroupCHList.FirstOrDefault().CURRENTUNITHOLDINGS));
                    //            totalGroup += personalGroup;
                    //        }
                    //    }

                    //    if (!String.IsNullOrEmpty(indirectDownlineMaHolderAcc))
                    //    {

                    //        //Populate current holdings for personal + group = total group
                    //        //To get current holdings from Oracle for Indirect Downlines.
                    //        string queryGetIndirectPGCH = @"select Sum(curr_unit_hldg) as current_unit_holdings from UTS.holder_inv where holder_no in (" + indirectDownlineMaHolderAcc + ")";
                    //        Response responseGetIndirectPGCH = ServicesManager.GetDataByQuery(queryGetIndirectPGCH);
                    //        if (responseGetIndirectPGCH.IsSuccess)
                    //        {
                    //            var getIndirectPGCHDyn = responseGetIndirectPGCH.Data;
                    //            var responseGetIndirectPGCHData = JsonConvert.SerializeObject(getIndirectPGCHDyn);
                    //            List<dynamic> indirectGroupList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetIndirectPGCHData);

                    //            totalGroup += (indirectGroupList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? 0 : Convert.ToDouble(indirectGroupList.FirstOrDefault().CURRENTUNITHOLDINGS));
                    //        }
                    //    }
                    //    lblPersonalGroupVal.Text = personalGroup.ToString("N", CultureInfo.CurrentCulture);
                    //    lblTotalGroupVal.Text = totalGroup.ToString("N", CultureInfo.CurrentCulture);
                }

                //To populate Sales and redemption table data

                string querySR = @"select 
                                    GROUP_CONCAT(uo.reject_reason) as trans_no, 
                                    uo.order_type, uo.order_status, ap.agent_code, 
                                    ap.agent_pos
                                    from user_orders uo
                                    inner join agent_clients ac on uo.user_id = ac.user_id
                                    inner join agent_regs ar on ar.user_id = ac.agent_user_id
                                    inner join agent_positions ap on ap.agent_code =  ar.agent_code 
                                    where ap.agent_group = '" + agentGroup + "' and uo.order_status = 3 group by ap.agent_pos Order by ap.agent_pos asc";
                Response responserSR = GenericService.GetDataByQuery(querySR, 0, 0, false, null, false, null, true);
                if (responserSR.IsSuccess)
                {
                    var srDyn = responserSR.Data;
                    var responseSR = JsonConvert.SerializeObject(srDyn);
                    List<dynamic> agentSR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSR);

                    //maholder account numbers
                    string personalMaHolderAcc = "";
                    string downlineMaHolderAcc = "";
                    string indirectDownlineMaHolderAcc = "";
                    int maDownlineCount = 0;
                    int maIndirectCount = 0;

                    //Temp for initial pos.
                    int agentPosCounter = 0;

                    //values for sales
                    double salesPersonalMTD = 0.0;
                    double salesPGMTD = 0.0;
                    double salesPGTMTD = 0.0;
                    double salesPersonalYTD = 0.0;
                    double salesPGYTD = 0.0;
                    double salesPGTYTD = 0.0;

                    //valus for redemption
                    double rdPersonalMTD = 0.0;
                    double rdPGMTD = 0.0;
                    double rdPGTMTD = 0.0;
                    double rdPersonalYTD = 0.0;
                    double rdPGYTD = 0.0;
                    double rdPGTYTD = 0.0;

                    agentSR.ForEach(x => {
                        //if order_type = 1 (Sales), order_status = 3 (approved) 39(rejected),MTD = all days in current month, agentPos = current agent Pos
                        string[] transNos = x.trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++)
                        {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",", transNos);
                        if (x.agent_pos == agentPos)
                        {

                            personalMaHolderAcc = transactionsNo;
                            agentPosCounter = Convert.ToString(x.agent_pos).Length;
                        }
                        else if (Convert.ToString(x.agent_pos).Length == (agentPosCounter + 3))
                        {
                            //if == 0, it is first acc
                            if (maDownlineCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            downlineMaHolderAcc += transactionsNo;
                            maDownlineCount++;
                        }
                        else
                        {
                            //if == 0, it is first acc
                            if (maIndirectCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            indirectDownlineMaHolderAcc = transactionsNo;
                            maIndirectCount++;
                        }
                    });

                    string allMaHolderAcc = personalMaHolderAcc + (!String.IsNullOrEmpty(downlineMaHolderAcc) ? "," + downlineMaHolderAcc : "") + (!String.IsNullOrEmpty(indirectDownlineMaHolderAcc) ? "," + indirectDownlineMaHolderAcc : "");

                    //Retrieve all sales and redemption value
                    string queryGetAllSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + allMaHolderAcc + ")";

                    Response responseGetAllSR = ServicesManager.GetDataByQuery(queryGetAllSR);
                    if (responseGetAllSR.IsSuccess)
                    {
                        var getAllSRDyn = responseGetAllSR.Data;
                        var responseGetAllSRJSON = JsonConvert.SerializeObject(getAllSRDyn);
                        List<dynamic> allSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetAllSRJSON);

                        //Sales
                        salesPersonalMTD = allSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year && personalMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                        salesPersonalYTD = allSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year && personalMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);

                        rdPersonalMTD = allSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year && personalMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                        rdPersonalYTD = allSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year && personalMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);

                        if (!String.IsNullOrEmpty(downlineMaHolderAcc))
                        {
                            salesPGMTD = allSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year && downlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            salesPGYTD = allSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year && downlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            salesPGMTD += salesPersonalMTD;
                            salesPGYTD += salesPersonalYTD;

                            rdPGMTD = allSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year && downlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            rdPGYTD = allSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year && downlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            salesPGTMTD += salesPGMTD;
                            salesPGTYTD += salesPGYTD;
                        }
                        else
                        {
                            salesPGMTD += salesPersonalMTD;
                            salesPGYTD += salesPersonalYTD;
                            salesPGTMTD += salesPGMTD;
                            salesPGTYTD += salesPGYTD;
                        }

                        if (!String.IsNullOrEmpty(indirectDownlineMaHolderAcc))
                        {
                            salesPGTMTD = allSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year && indirectDownlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            salesPGTYTD = allSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year && indirectDownlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            rdPGMTD += rdPersonalMTD;
                            rdPGYTD += rdPersonalYTD;

                            rdPGTMTD = allSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year && indirectDownlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            rdPGTYTD = allSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year && indirectDownlineMaHolderAcc.Contains(x.TRANSNO.ToString())).Sum(x => x.TRANSAMT);
                            rdPGTMTD += rdPGMTD;
                            rdPGTYTD += rdPGYTD;
                        }
                        else
                        {
                            rdPGMTD += rdPersonalMTD;
                            rdPGYTD += rdPersonalYTD;
                            rdPGTMTD += rdPGMTD;
                            rdPGTYTD += rdPGYTD;
                        }

                        lblSalesMTDPS.Text = salesPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDPS.Text = salesPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdMPD.Text = rdPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYPD.Text = rdPersonalYTD.ToString("N", CultureInfo.CurrentCulture);

                        lblSalesMTDPGS.Text = salesPGMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDPGS.Text = salesPGYTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdMPGD.Text = rdPGMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYPGD.Text = rdPGYTD.ToString("N", CultureInfo.CurrentCulture);

                        lblSalesMTDTGS.Text = salesPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDTGS.Text = salesPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdMTGD.Text = rdPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYTGD.Text = rdPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    }

                    ////populate personal value
                    //string queryGetPMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + personalMaHolderAcc + ")";
                    //Response responseGetPersonalSR = ServicesManager.GetDataByQuery(queryGetPMaholderSR);
                    //if (responseGetPersonalSR.IsSuccess)
                    //{
                    //    var getPersonalSRDyn = responseGetPersonalSR.Data;
                    //    var responseGetPersonalSRData = JsonConvert.SerializeObject(getPersonalSRDyn);
                    //    List<dynamic> personalSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetPersonalSRData);

                    //    salesPersonalMTD = personalSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                    //    salesPersonalYTD = personalSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                    //    rdPersonalMTD = personalSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                    //    rdPersonalYTD = personalSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                    //    //display sales value @personal
                    //    lblSalesMTDPS.Text = salesPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblSalesYTDPS.Text = salesPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                    //    //display redemption value @personal
                    //    lblRdMPD.Text = rdPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblRdYPD.Text = rdPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                    //}
                    //else
                    //{
                    //    //display sales value @personal
                    //    lblSalesMTDPS.Text = salesPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblSalesYTDPS.Text = salesPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                    //    //display redemption value @personal
                    //    lblRdMPD.Text = rdPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblRdYPD.Text = rdPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                    //}

                    ////populate PG value
                    //string queryGetDirectMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + downlineMaHolderAcc + ")";
                    //Response responseGetDirectSR = ServicesManager.GetDataByQuery(queryGetDirectMaholderSR);
                    //if (responseGetDirectSR.IsSuccess)
                    //{
                    //    var getDirectSRDyn = responseGetDirectSR.Data;
                    //    var responseGetDirectSRData = JsonConvert.SerializeObject(getDirectSRDyn);
                    //    List<dynamic> directDownlinelSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetDirectSRData);

                    //    salesPGMTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                    //    salesPGYTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                    //    rdPGMTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                    //    rdPGYTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                    //    //display sales value @personal
                    //    salesPGMTD += salesPersonalMTD;
                    //    salesPGYTD += salesPersonalYTD;
                    //    lblSalesMTDPGS.Text = salesPGMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblSalesYTDPGS.Text = salesPGYTD.ToString("N", CultureInfo.CurrentCulture);
                    //    //display redemption value @personal
                    //    rdPGMTD += rdPersonalMTD;
                    //    rdPGYTD += rdPersonalYTD;
                    //    lblRdMPGD.Text = rdPGMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblRdYPGD.Text = rdPGYTD.ToString("N", CultureInfo.CurrentCulture);
                    //}
                    //else
                    //{
                    //    //display sales value @personal
                    //    salesPGMTD += salesPersonalMTD;
                    //    salesPGYTD += salesPersonalYTD;
                    //    lblSalesMTDPGS.Text = salesPGMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblSalesYTDPGS.Text = salesPGYTD.ToString("N", CultureInfo.CurrentCulture);
                    //    //display redemption value @personal
                    //    rdPGMTD += rdPersonalMTD;
                    //    rdPGYTD += rdPersonalYTD;
                    //    lblRdMPGD.Text = rdPGMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblRdYPGD.Text = rdPGYTD.ToString("N", CultureInfo.CurrentCulture);
                    //}

                    ////populate PGT value
                    //string queryGetIndirectMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + indirectDownlineMaHolderAcc + ")";
                    //Response responseGetIndirectSR = ServicesManager.GetDataByQuery(queryGetIndirectMaholderSR);
                    //if (responseGetIndirectSR.IsSuccess)
                    //{
                    //    var getIndirectSRDyn = responseGetIndirectSR.Data;
                    //    var responseGetIndirectSRData = JsonConvert.SerializeObject(getIndirectSRDyn);
                    //    List<dynamic> indirectDownlinelSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetIndirectSRData);

                    //    salesPGTMTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                    //    salesPGTYTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                    //    rdPGTMTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                    //    rdPGTYTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                    //    //display sales value @personal
                    //    salesPGTMTD += salesPGMTD;
                    //    salesPGTYTD += salesPGYTD;
                    //    lblSalesMTDTGS.Text = salesPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblSalesYTDTGS.Text = salesPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    //    //display redemption value @personal
                    //    rdPGTMTD += rdPGMTD;
                    //    rdPGTYTD += rdPGYTD;
                    //    lblRdMTGD.Text = rdPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblRdYTGD.Text = rdPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    //}
                    //else
                    //{
                    //    //display sales value @personal
                    //    salesPGTMTD += salesPGMTD;
                    //    salesPGTYTD += salesPGYTD;
                    //    lblSalesMTDTGS.Text = salesPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblSalesYTDTGS.Text = salesPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    //    //display redemption value @personal
                    //    rdPGTMTD += rdPGMTD;
                    //    rdPGTYTD += rdPGYTD;
                    //    lblRdMTGD.Text = rdPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                    //    lblRdYTGD.Text = rdPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    //}
                }

                //To populate commission table data
                string queryCommTable = @"select *, YEAR(updated_date) as updated_year, Month(updated_date) as updated_month from comm_ledger where agent_code = '" + user.AgentCode + "'";
                Response responseAgentCommTable = GenericService.GetDataByQuery(queryCommTable, 0, 0, false, null, false, null, true);
                if (responseAgentCommTable.IsSuccess)
                {
                    var agentCommTableDyn = responseAgentCommTable.Data;
                    var responseAgentComm = JsonConvert.SerializeObject(agentCommTableDyn);
                    List<dynamic> agentCommList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentComm);

                    double commMTD = agentCommList.Where(x => x.updated_month == DateTime.Now.Month && x.updated_year == DateTime.Now.Year).Sum(x => x.comm_psc + x.comm_oc);
                    double commYTD = agentCommList.Where(x => x.updated_year == DateTime.Now.Year).Sum(x => x.comm_psc + x.comm_oc);

                    lblCommissionMPSC.Text = commMTD.ToString("N", CultureInfo.CurrentCulture);
                    lblCommissionYPSC.Text = commYTD.ToString("N", CultureInfo.CurrentCulture);
                }


                //To populate direct and indirect agents table data
                string queryDownlines = @"select arp.name,utd.name as agent_rank,GROUP_CONCAT(ua.account_no) as account_no, arp.Status, ap.agent_pos, ap.agent_code,ar.user_id,  IFNULL(SUM(att.cpd_points),0) as cpd_points,  arf.expiry_date as expire_time_left
                                        from agent_positions ap
                                        inner join agent_regs ar on ap.agent_code = ar.agent_code
                                        inner join agent_reg_personal arp on ar.id = arp.agent_reg_id
										inner join user_types ut on ut.user_id = ar.user_id
										inner join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                                        inner join agent_reg_fimms arf on arf.agent_reg_id = ar.id
										left join agent_clients ac on ac.agent_user_id = ar.user_id
										left join user_accounts ua on ua.user_id = ac.user_id
                                        left join agent_training_enrollments ate on ate.user_id = ar.user_id 
                                        left join agent_training_topics att on ate.agent_training_topic_id  = att.id and (ate.is_reported=1 and ate.is_attended=1) and ((att.is_assessment = 1 and ate.is_passed=1) or (att.is_assessment=0 and ate.is_passed=0))
                                        where ap.agent_group = '" + agentGroup + "' and char_length(ap.agent_pos) >" + agentPos.Length + " group by arp.id";
                Response responseAgentDownlines = GenericService.GetDataByQuery(queryDownlines, 0, 0, false, null, false, null, true);
                if (responseAgentDownlines.IsSuccess)
                {
                    var agentDownlineDyn = responseAgentDownlines.Data;
                    var responseAgentDownline = JsonConvert.SerializeObject(agentDownlineDyn);
                    List<dynamic> agentDownlineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentDownline);

                    string accNos = String.Join(",", agentDownlineList.Where(x => !String.IsNullOrEmpty(Convert.ToString(x.account_no))).Select(x => x.account_no));

                    //To get current holdings from Oracle 
                    if (String.IsNullOrEmpty(accNos)) {
                        accNos = "0";
                    }
                    string queryGetAUM = @"select curr_unit_hldg, holder_no from UTS.holder_inv where holder_no in (" + accNos + ")";
                    Response responseGetAUM = ServicesManager.GetDataByQuery(queryGetAUM);
                    if (responseGetAUM.IsSuccess)
                    {
                        var getAUMDyn = responseGetAUM.Data;
                        var responseGetAUMData = JsonConvert.SerializeObject(getAUMDyn);
                        List<dynamic> personalAUMList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetAUMData);


                        int directIndex = 0;
                        int indirectIndex = 0;
                        StringBuilder directTableBuilder = new StringBuilder();
                        StringBuilder indirectTableBuilder = new StringBuilder();
                        agentDownlineList.ForEach(x =>
                        {
                            string agentAccNos = x.account_no;
                            string aum = "0.00";
                            if (!String.IsNullOrEmpty(agentAccNos))
                            {
                                string[] agentAccountNosSplit = agentAccNos.Split(',');
                                var agentAUM = personalAUMList.Where(y => agentAccountNosSplit.ToList().Contains(Convert.ToString(y.HOLDERNO))).Sum(y => y.CURRUNITHLDG);
                                aum = Convert.ToDouble(agentAUM).ToString("N", CultureInfo.CurrentCulture);
                            }

                            string agentPositionString = x.agent_pos;
                            int agentPositionLength = agentPositionString.Length;

                            //Populate Direct Downline
                            if (agentPositionLength > agentPos.Length && agentPositionLength <= agentPos.Length + 3)
                            {
                                ++directIndex;
                                directTableBuilder.Append(
                                                @"
                                            <tr class='" + "trTextFormat" + @"'>
                                            <td><b>" + directIndex + @"</b></td>
                                            <td><b>" + x.name + @"</b></td>
                                            <td><b>" + x.agent_code + @"</b></td>
                                            <td><b>" + x.agent_rank + @"</b></td>
                                            <td><b>" + x.Status + @"</b></td>
                                            <td><b>" + aum + @"</b></td>
                                            <td><b>10</td>
                                            <td><b>" + x.cpd_points + @"</b></td>
                                            <td><b>" + x.expire_time_left.ToString("dd/MM/yyyy") + @"</b></td>
                                        </tr>");

                            }
                            //Populate Indirect Downline
                            else
                            {
                                ++indirectIndex;
                                indirectTableBuilder.Append(
                                                @"
                                            <tr class='" + "trTextFormat" + @"'>
                                            <td><b>" + indirectIndex + @"</b></td>
                                            <td><b>" + x.name + @"</b></td>
                                            <td><b>" + x.agent_code + @"</b></td>
                                            <td><b>" + x.agent_rank + @"</b></td>
                                            <td><b>" + x.Status + @"</b></td>
                                            <td><b>" + aum + @"</b></td>
                                            <td><b>10</td>
                                            <td><b>" + x.cpd_points + @"</b></td>
                                            <td><b>" + x.expire_time_left.ToString("dd/MM/yyyy") + @"</b></td>
                                        </tr>");

                            }
                        });
                        if (directIndex != 0)
                        {
                            tbodyDirectDownline.InnerHtml = directTableBuilder.ToString();
                        }
                        else
                        {
                            tbodyDirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='9'><b>No data available for display.</b></td></tr>";
                        }

                        if (indirectIndex != 0)
                        {
                            tbodyIndirectDownline.InnerHtml = indirectTableBuilder.ToString();
                        }
                        else
                        {
                            tbodyIndirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='9'><b>No data available for display.</b></td></tr>";
                        }

                    }
                    else
                    {
                        tbodyDirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='9'><b>No data available for display.</b></td></tr>";

                        tbodyIndirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='9'><b>No data available for display.</b></td></tr>";
                    }

                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetCommissionStats(string commCode)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (user != null)
            {
                string queryCommStats = @"select sum(comm_psc) as comm_psc, sum(comm_oc) as comm_oc, YEAR(updated_date) as updated_year, Month(updated_date)as updated_month from comm_ledger 
                                    where agent_code = '" + user.AgentCode + "' group by month(updated_date), year(updated_date) order by month(updated_date) asc";
                Response responseCommStats = GenericService.GetDataByQuery(queryCommStats, 0, 0, false, null, false, null, true);
                if (responseCommStats.IsSuccess)
                {
                    var commStatsDyn = responseCommStats.Data;
                    var responseCommStatsJSON = JsonConvert.SerializeObject(commStatsDyn);
                    List<dynamic> commStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseCommStatsJSON);
                    List<getStatsByYear> statByYear = new List<getStatsByYear>();


                    statByYear.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYear.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                    });

                    statByYear.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                    });

                    //To initialize 0 values for agent, who has no commission on a specific month.
                    statByYear.ForEach(y => {
                        List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                        // take first 3 years assign months, year and commval 0;
                        for (int i = 0; i < 12; i++)
                        {

                            getStatsByMonth statMonth = new getStatsByMonth();
                            statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                            statMonth.commVal = "0";
                            statByMonths.Add(statMonth);

                        }
                        y.statByMonth = statByMonths;
                    });


                    if (commCode == "PC")
                    {
                        commStatsList.GroupBy(x => new { x.updated_year, x.updated_month }).ToList().ForEach(x =>
                         {

                             if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                             {
                                 getStatsByYear singleStatByYear = new getStatsByYear
                                 {
                                     Year = x.First().updated_year,
                                     statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.FirstOrDefault().updated_month)), commVal = x.FirstOrDefault().comm_psc }).ToList()
                                 };

                                 var yearIndex = statByYear.IndexOf(statByYear.Where(i => i.Year == Convert.ToString(x.FirstOrDefault().updated_year)).First());
                                 var monthIndex = statByYear[yearIndex].statByMonth.IndexOf(statByYear[yearIndex].statByMonth.FirstOrDefault(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.FirstOrDefault().updated_month))));
                                 statByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.FirstOrDefault().commVal;
                             }
                         });

                    }

                    if (commCode == "OC")
                    {
                        commStatsList.GroupBy(x => new { x.updated_year, x.updated_month }).ToList().ForEach(x =>
                        {
                            if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().updated_year,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = x.First().comm_oc }).ToList()
                                };

                                var yearIndex = statByYear.IndexOf(statByYear.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
                                var monthIndex = statByYear[yearIndex].statByMonth.IndexOf(statByYear[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
                                statByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                            }
                        });
                    }

                    if (commCode == "ALL")
                    {
                        commStatsList.GroupBy(x => new { x.updated_year, x.updated_month }).ToList().ForEach(x =>
                        {
                            if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().updated_year,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = (Convert.ToDouble(x.First().comm_psc) + Convert.ToDouble(x.First().comm_oc)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
                                };

                                var yearIndex = statByYear.IndexOf(statByYear.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
                                var monthIndex = statByYear[yearIndex].statByMonth.IndexOf(statByYear[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
                                statByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                            }
                        });
                    }

                    statByYear = statByYear.OrderBy(x => x.Year).ToList();

                    return statByYear;
                }
            }

            return null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetSalesStats()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (user != null)
            {
                //Initialize object with 0 values for months and year
                List<getStatsByYear> SalesStatsByYear = new List<getStatsByYear>();
                SalesStatsByYear.Add(new getStatsByYear
                {
                    Year = Convert.ToString(DateTime.Now.Year),
                });

                //To initialize 0 values for agent, who has no commission on a specific month.
                SalesStatsByYear.ForEach(y => {
                    List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                    // take first 3 years assign months, year and commval 0;
                    for (int i = 0; i < 12; i++)
                    {

                        getStatsByMonth statMonth = new getStatsByMonth();
                        statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                        statMonth.commVal = "0";
                        statByMonths.Add(statMonth);

                    }
                    y.statByMonth = statByMonths;
                });

                string querySalesStats = @"select 
                                    GROUP_CONCAT(uo.reject_reason) as trans_no, 
                                    uo.order_type, uo.order_status, uo.consultantID
                                    from user_orders uo
                                    where uo.consultantID = '" + user.AgentCode + "' and uo.order_status = 3 and uo.order_type = 1";
                Response responseSalesStats = GenericService.GetDataByQuery(querySalesStats, 0, 0, false, null, false, null, true);
                if (responseSalesStats.IsSuccess)
                {
                    var salesStatsDyn = responseSalesStats.Data;
                    var responseSalesStatsJSON = JsonConvert.SerializeObject(salesStatsDyn);
                    List<dynamic> salesStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSalesStatsJSON);

                    if (salesStatsList.Count > 0)
                    {
                        string[] transNos = salesStatsList.FirstOrDefault().trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++)
                        {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",", transNos);

                        //populate personal value
                        string queryGetTotalSales = @"select holder_no,sum(trans_amt) as TRANSAMT,extract( month from trans_dt) as TRANSMONTH,extract( year from trans_dt) as TRANSYEAR from uts.holder_ledger where trans_no in (" + transactionsNo + ") and extract( year from trans_dt) = EXTRACT(YEAR FROM sysdate) group by uts.holder_ledger.HOLDER_NO, extract( year from trans_dt), extract( month from trans_dt)";
                        Response responseGetTotalSales = ServicesManager.GetDataByQuery(queryGetTotalSales);
                        if (responseGetTotalSales.IsSuccess)
                        {
                            var getSalesDataDyn = responseGetTotalSales.Data;
                            var responseGetSalesDataJSON = JsonConvert.SerializeObject(getSalesDataDyn);
                            List<dynamic> getSalesDataList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetSalesDataJSON);

                            getSalesDataList.GroupBy(x => new { x.TRANSYEAR, x.TRANSMONTH }).ToList().ForEach(x =>
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().TRANSYEAR,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.FirstOrDefault().TRANSMONTH)), commVal = x.FirstOrDefault().TRANSAMT }).ToList()
                                };

                                var yearIndex = SalesStatsByYear.IndexOf(SalesStatsByYear.Where(i => i.Year == Convert.ToString(x.FirstOrDefault().TRANSYEAR)).First());
                                var monthIndex = SalesStatsByYear[yearIndex].statByMonth.IndexOf(SalesStatsByYear[yearIndex].statByMonth.FirstOrDefault(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.FirstOrDefault().TRANSMONTH))));
                                SalesStatsByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.FirstOrDefault().commVal;

                            });

                            //double salesVal = getSalesDataList.FirstOrDefault().TRANSAMT;
                            return SalesStatsByYear;
                        }
                    }
                }
                return SalesStatsByYear;
            }

            return null;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetAnnualSalesTargetStat()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (user != null)
            {
                //Pre initialize data for years to 0
                List<getSalesByYear> salesByYearList = new List<getSalesByYear>();

                for (int i = 0; i < 2; i++)
                {
                    String years = "";
                    if (i == 0)
                    {
                        years = "Current Total Sales for " + Convert.ToString(DateTime.Now.Year);
                    }
                    if (i == 1)
                    {
                        years = "Amount to achieve sales goal";
                    }

                    getSalesByYear singleYear = new getSalesByYear
                    {
                        Year = years,
                        SalesValue = "0"
                    };
                    salesByYearList.Add(singleYear);
                }

                string querySalesStats = @"select 
                                    GROUP_CONCAT(uo.reject_reason) as trans_no, 
                                    uo.order_type, uo.order_status, uo.consultantID
                                    from user_orders uo
                                    where uo.consultantID = '" + user.AgentCode + "' and uo.order_status = 3 and uo.order_type = 1";
                Response responseSalesStats = GenericService.GetDataByQuery(querySalesStats, 0, 0, false, null, false, null, true);
                if (responseSalesStats.IsSuccess)
                {
                    var salesStatsDyn = responseSalesStats.Data;
                    var responseSalesStatsJSON = JsonConvert.SerializeObject(salesStatsDyn);
                    List<dynamic> salesStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSalesStatsJSON);
                    if (salesStatsList.Count > 0)
                    {
                        string[] transNos = salesStatsList.FirstOrDefault().trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++)
                        {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",", transNos);

                        //populate personal value
                        string queryGetTotalSales = @"select holder_no,sum(trans_amt) as TRANSAMT,extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + transactionsNo + ") and extract( year from trans_dt) = EXTRACT(YEAR FROM sysdate) group by uts.holder_ledger.HOLDER_NO, extract( year from trans_dt)";
                        Response responseGetTotalSales = ServicesManager.GetDataByQuery(queryGetTotalSales);
                        if (responseGetTotalSales.IsSuccess)
                        {
                            var getSalesDataDyn = responseGetTotalSales.Data;
                            var responseGetSalesDataJSON = JsonConvert.SerializeObject(getSalesDataDyn);
                            List<dynamic> getSalesDataList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetSalesDataJSON);

                            getSalesDataList.ForEach(x =>
                            {
                                //curr year
                                salesByYearList[0].SalesValue = Convert.ToString(x.TRANSAMT);


                                //goal
                                if (Convert.ToDouble(x.TRANSAMT) > 30000.00)
                                {
                                    salesByYearList[1].SalesValue = "0";
                                }
                                else {
                                    salesByYearList[1].SalesValue = Convert.ToString((30000.00 - Convert.ToDouble(x.TRANSAMT)));
                                }



                            });

                            //double salesVal = getSalesDataList.FirstOrDefault().TRANSAMT;
                            return salesByYearList;
                        }
                    }
                }
                return salesByYearList;
            }

            return null;

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetAllSalesData()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (user != null) {

                getAllSalesData AllSalesData = new getAllSalesData();

                //annual sales target pre-ini
                List<getSalesByYear> salesByYearList = new List<getSalesByYear>();

                for (int i = 0; i < 2; i++)
                {
                    String years = "";
                    if (i == 0)
                    {
                        years = "Current Total Sales for " + Convert.ToString(DateTime.Now.Year);
                    }
                    if (i == 1)
                    {
                        years = "Amount to achieve sales goal";
                    }

                    getSalesByYear singleYear = new getSalesByYear
                    {
                        Year = years,
                        SalesValue = "0"
                    };
                    salesByYearList.Add(singleYear);
                }

                //Sales Data for monthly tracking
                //Initialize object with 0 values for months and year
                List<getStatsByYear> SalesStatsByYear = new List<getStatsByYear>();
                SalesStatsByYear.Add(new getStatsByYear
                {
                    Year = Convert.ToString(DateTime.Now.Year),
                });

                //To initialize 0 values for agent, who has no commission on a specific month.
                SalesStatsByYear.ForEach(y => {
                    List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                    // take first 3 years assign months, year and commval 0;
                    for (int i = 0; i < 12; i++)
                    {

                        getStatsByMonth statMonth = new getStatsByMonth();
                        statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                        statMonth.commVal = "0";
                        statByMonths.Add(statMonth);

                    }
                    y.statByMonth = statByMonths;
                });

                string querySalesStats = @"select 
                                    GROUP_CONCAT(uo.reject_reason) as trans_no, 
                                    uo.order_type, uo.order_status, uo.consultantID
                                    from user_orders uo
                                    where uo.consultantID = '" + user.AgentCode + "' and uo.order_status = 3 and uo.order_type = 1";
                Response responseSalesStats = GenericService.GetDataByQuery(querySalesStats, 0, 0, false, null, false, null, true);
                if (responseSalesStats.IsSuccess)
                {
                    var salesStatsDyn = responseSalesStats.Data;
                    var responseSalesStatsJSON = JsonConvert.SerializeObject(salesStatsDyn);
                    List<dynamic> salesStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSalesStatsJSON);

                    if (salesStatsList.Count > 0)
                    {
                        string[] transNos = salesStatsList.FirstOrDefault().trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++)
                        {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",", transNos);

                        //populate personal value
                        string queryGetTotalSales = @"select holder_no,sum(trans_amt) as TRANSAMT,extract( month from trans_dt) as TRANSMONTH,extract( year from trans_dt) as TRANSYEAR from uts.holder_ledger where trans_no in (" + transactionsNo + ") and extract( year from trans_dt) = EXTRACT(YEAR FROM sysdate) group by uts.holder_ledger.HOLDER_NO, extract( year from trans_dt), extract( month from trans_dt)";
                        Response responseGetTotalSales = ServicesManager.GetDataByQuery(queryGetTotalSales);
                        if (responseGetTotalSales.IsSuccess)
                        {
                            var getSalesDataDyn = responseGetTotalSales.Data;
                            var responseGetSalesDataJSON = JsonConvert.SerializeObject(getSalesDataDyn);
                            List<dynamic> getSalesDataList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetSalesDataJSON);

                            getSalesDataList.GroupBy(x => new { x.TRANSYEAR, x.TRANSMONTH }).ToList().ForEach(x =>
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().TRANSYEAR,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.FirstOrDefault().TRANSMONTH)), commVal = x.FirstOrDefault().TRANSAMT }).ToList()
                                };

                                var yearIndex = SalesStatsByYear.IndexOf(SalesStatsByYear.Where(i => i.Year == Convert.ToString(x.FirstOrDefault().TRANSYEAR)).First());
                                var monthIndex = SalesStatsByYear[yearIndex].statByMonth.IndexOf(SalesStatsByYear[yearIndex].statByMonth.FirstOrDefault(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.FirstOrDefault().TRANSMONTH))));
                                SalesStatsByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.FirstOrDefault().commVal;

                            });

                            getSalesDataList.Where(x => x.TRANSYEAR == DateTime.Now.Year).ToList().ForEach(x =>
                            {
                                //curr year
                                salesByYearList[0].SalesValue = Convert.ToString(x.TRANSAMT);


                                //goal
                                if (Convert.ToDouble(x.TRANSAMT) > 30000.00)
                                {
                                    salesByYearList[1].SalesValue = "0";
                                }
                                else
                                {
                                    salesByYearList[1].SalesValue = Convert.ToString((30000.00 - Convert.ToDouble(x.TRANSAMT)));
                                }

                            });
                            AllSalesData.getAnnualSalesData = salesByYearList;
                            //double salesVal = getSalesDataList.FirstOrDefault().TRANSAMT;
                            AllSalesData.getMonthlySalesData = SalesStatsByYear;
                            //return SalesStatsByYear;
                        }
                    }
                }

                //Annual Sales Target Data
                //Pre initialize data for years to 0
                //List<getSalesByYear> salesByYearList = new List<getSalesByYear>();

                //for (int i = 0; i < 2; i++)
                //{
                //    String years = "";
                //    if (i == 0)
                //    {
                //        years = "Current Total Sales for " + Convert.ToString(DateTime.Now.Year);
                //    }
                //    if (i == 1)
                //    {
                //        years = "Amount to achieve sales goal";
                //    }

                //    getSalesByYear singleYear = new getSalesByYear
                //    {
                //        Year = years,
                //        SalesValue = "0"
                //    };
                //    salesByYearList.Add(singleYear);
                //}

                //string queryAnnualSales = @"select 
                //                    GROUP_CONCAT(uo.reject_reason) as trans_no, 
                //                    uo.order_type, uo.order_status, uo.consultantID
                //                    from user_orders uo
                //                    where uo.consultantID = '" + user.AgentCode + "' and uo.order_status = 3 and uo.order_type = 1";
                //Response responseAnnualSales = GenericService.GetDataByQuery(queryAnnualSales, 0, 0, false, null, false, null, true);
                //if (responseAnnualSales.IsSuccess)
                //{
                //    var salesStatsDyn = responseAnnualSales.Data;
                //    var responseSalesStatsJSON = JsonConvert.SerializeObject(salesStatsDyn);
                //    List<dynamic> salesStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSalesStatsJSON);
                //    if (salesStatsList.Count > 0)
                //    {
                //        string[] transNos = salesStatsList.FirstOrDefault().trans_no.ToString().Split(',');
                //        for (int i = 0; i < transNos.Length; i++)
                //        {
                //            transNos[i] = "'" + transNos[i] + "'";
                //        }
                //        string transactionsNo = String.Join(",", transNos);

                //        //populate personal value
                //        string queryGetTotalSales = @"select holder_no,sum(trans_amt) as TRANSAMT,extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + transactionsNo + ") and extract( year from trans_dt) = EXTRACT(YEAR FROM sysdate) group by uts.holder_ledger.HOLDER_NO, extract( year from trans_dt)";
                //        Response responseGetTotalSales = ServicesManager.GetDataByQuery(queryGetTotalSales);
                //        if (responseGetTotalSales.IsSuccess)
                //        {
                //            var getSalesDataDyn = responseGetTotalSales.Data;
                //            var responseGetSalesDataJSON = JsonConvert.SerializeObject(getSalesDataDyn);
                //            List<dynamic> getSalesDataList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetSalesDataJSON);

                //            getSalesDataList.ForEach(x =>
                //            {
                //                //curr year
                //                salesByYearList[0].SalesValue = Convert.ToString(x.TRANSAMT);


                //                //goal
                //                if (Convert.ToDouble(x.TRANSAMT) > 30000.00)
                //                {
                //                    salesByYearList[1].SalesValue = "0";
                //                }
                //                else
                //                {
                //                    salesByYearList[1].SalesValue = Convert.ToString((30000.00 - Convert.ToDouble(x.TRANSAMT)));
                //                }



                //            });

                //            //double salesVal = getSalesDataList.FirstOrDefault().TRANSAMT;
                //            AllSalesData.getAnnualSalesData = salesByYearList;
                //            //return salesByYearList;
                //        }
                //    }
                //}
                return AllSalesData;
            }
            else
            {

            }
            return null;
        }

        public class getAllSalesData
        {
            public List<getSalesByYear> getAnnualSalesData { get; set; }
            public List<getStatsByYear> getMonthlySalesData { get; set; }
        }

        public class getSalesByYear {
            public String Year { get; set; }
            public string SalesValue { get; set; }
        }

        public class getStatsByMonth
        {
            public string Month { get; set; }
            public string commVal { get; set; }
        }

        public class getStatsByYear
        {

            public string Year { get; set; }
            public List<getStatsByMonth> statByMonth { get; set; }
        }
    }
}