﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class UtmcFundCorporateActions
    {
        public Int32 Id { get; set; }
        public String Epf_Ipd_Code { get; set; }
        public String Ipd_Fund_Code { get; set; }
        public DateTime Corporate_Action_Date { get; set; }
        public Decimal Distributions { get; set; }
        public Decimal? Unit_Splits { get; set; }
        public DateTime Report_Date { get; set; }
        public Decimal Net_Distribution { get; set; }
    }
}
