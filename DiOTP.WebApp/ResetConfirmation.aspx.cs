﻿using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ResetConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx?redirectUrl=Portfolio.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx?redirectUrl=Portfolio.aspx');", true);
                    }
                }
                else
                {
                    try
                    {
                        User user = (User)Session["user"];
                        UserSecurity userSecurity = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                        string key = userSecurity.Value;
                        Email email = new Email();
                        Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
                        MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                        user.Username = maHolderReg.Name1;
                        email.user = user;
                        EmailService.SendLostGoogleAuthenticatorMail(email, key);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog("ResetConfirmation Page SendEmail: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}