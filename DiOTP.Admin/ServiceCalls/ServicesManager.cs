﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace Admin.ServiceCalls
{
    public class ServicesManager
    {
        public static Response CheckConnection()
        {
            Response response = new Response();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response1 = client.GetAsync("api/HolderLedgerApi/Get?filter=check").Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        //string responseString = response1.Content.ReadAsStringAsync().Result;
                        var modelObject = response1.Content.ReadAsAsync<Response>().Result;
                        return modelObject;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.IsApiAvailable = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static LoginRole GetRole(HttpContext Context)
        {
            Boolean isSuperAdmin = false;
            Boolean isMainAdmin = false;
            Boolean isAccountAdmin = false;
            Boolean isContentAdmin = false;

            Boolean isAdminOfficer = false;
            Boolean isAdminManager = false;
            if (Context.Session["isSuperAdmin"] != null && Context.Session["isSuperAdmin"].ToString() == "1")
                isSuperAdmin = true;
            if (Context.Session["isMainAdmin"] != null && Context.Session["isMainAdmin"].ToString() == "1")
                isMainAdmin = true;
            if (Context.Session["isAccountAdmin"] != null && Context.Session["isAccountAdmin"].ToString() == "1")
                isAccountAdmin = true;
            if (Context.Session["isContentAdmin"] != null && Context.Session["isContentAdmin"].ToString() == "1")
                isContentAdmin = true;
            if (Context.Session["isAdminOfficer"] != null && Context.Session["isAdminOfficer"].ToString() == "1")
                isAdminOfficer = true;
            if (Context.Session["isAdminManager"] != null && Context.Session["isAdminManager"].ToString() == "1")
                isAdminManager = true;
            return new LoginRole
            {
                isSuperAdmin = isSuperAdmin,
                isMainAdmin = isMainAdmin,
                isAccountAdmin = isAccountAdmin,
                isContentAdmin = isContentAdmin,
                isAdminOfficer = isAdminOfficer,
                isAdminManager = isAdminManager
            };
        }


        public static Response responseCustom = new Response();



        public static Response GetMaHolderRegByAccountNo(string accNo, Boolean isAccountOpeningTest = false, string insertQuery = "")
        {
            MaHolderReg maHolderReg = new MaHolderReg();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    filter.Append(" left join UTS.ADD_INFO a on a.INFO_NUMBER = h.HOLDER_NO and a.REF_CODE='09' where h.holder_no = '" + accNo + "'");
                    var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            List<MaHolderReg> maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                            maHolderReg = maHolderRegs.FirstOrDefault();
                            if (maHolderReg != null)
                            {
                                responseCustom.IsSuccess = true;
                                responseCustom.Data = maHolderReg;
                            }
                            else
                            {
                                if (isAccountOpeningTest)
                                {
                                    Response responseInsertMA = InsertRecord(insertQuery);
                                    if (responseInsertMA.IsSuccess)
                                    {
                                        return GetMaHolderRegByAccountNo(accNo);
                                        //responseCustom.IsSuccess = false;
                                        //responseCustom.Message = "Invalid MA No!";
                                    }
                                    else
                                    {
                                        responseCustom = responseInsertMA;
                                    }
                                }
                                else
                                {
                                    responseCustom.IsSuccess = false;
                                    responseCustom.Message = "Invalid MA No!";
                                }
                                //responseCustom.IsSuccess = false;
                                //responseCustom.Message = "Invalid MA No!";
                            }
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetMaHolderRegsByIdNo(string IdNo)
        {
            List<MaHolderReg> maHolderRegs = new List<MaHolderReg>();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    //filter.Append(" and id_no_2 not in ('STA', 'STA  2', 'STA 2', 'STA 2 (10JF)', 'STA 3', 'STA2', 'STA3')");
                    //filter.Append(" and id_no_ol not in ('STA2', '10 JF', 'STA2')");
                    //filter.Append(" and id_no_3 not in ('10JF', 'STA 3', 'STA3')");
                    filter.Append(" left join UTS.ADD_INFO a on a.INFO_NUMBER = h.HOLDER_NO and a.REF_CODE='09' where h.id_no = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old = '" + IdNo + "'");
                    filter.Append(" or h.id_no_2 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_2 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_3 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_3 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_4 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_4 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_5 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_5 = '" + IdNo + "'");
                    var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = maHolderRegs;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static List<UtmcDetailedMemberInvestment> GetMAAccountSummary(string holderNo, List<UtmcFundInformation> UTMCFundInformations)
        {
            List<UtmcDetailedMemberInvestment> objs = new List<UtmcDetailedMemberInvestment>();

            DateTime currentDate = DateTime.Now;
            //DateTime yearsBefore = currentDate.AddYears(-10);
            DateTime yearsBefore = new DateTime(2000, 1, 1);

            List<HolderInv> holderInvs = GetHolderInvByHolderNo(holderNo, UTMCFundInformations).Where(x => x.CurrUnitHldg > 0).ToList();
            if (holderInvs.Count != 0)
            {
                //string filterDate = DateTime.Now.AddYears(-3).ToString("yyyy/MM/dd");
                List<HolderLedger> holderLedgers = GetHolderLedgerByHolderNo(" select * from uts.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and HOLDER_NO='" + holderNo + "' and FUND_ID in (" + String.Join(",", holderInvs.Select(x => "'" + x.FundId + "'").ToArray()) + ") ORDER By TRANS_DT desc ");

                //List<UtmcMemberInvestment> utmcMemberInvestments = GetMemberInvestmentsByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"), String.Join(",", holderInvs.Select(x => "'" + x.FundId + "'").ToArray()));

                //List<UtmcCompositionalTransaction> utmcCompositionalTransactions = GetCompositionalTransactionByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"));
                //List<FundDetailedInformation> fundDetailedInformations = IUtmcFundInformationService.GetFundDetailedInformation();

                foreach (string fundCode in holderInvs.Select(x => x.FundId).ToList())
                {
                    //UTMCFundInformations.ForEach(x=> {
                    //    x.UtmcDailyNavFundsWithChanges.Clear();
                    //    x.UtmcDailyNavFundWithChangeMonthEnd.Clear();
                    //    x.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Clear();
                    //    x.UtmcFundInformation.UtmcFundInformationIdUtmcFundCharges.Clear();
                    //});

                    UtmcDetailedMemberInvestment utmcDetailedMemberInvestment = new UtmcDetailedMemberInvestment
                    {
                        holderInv = holderInvs.Where(x => x.FundId == fundCode).FirstOrDefault(),
                        //utmcMemberInvestments = utmcMemberInvestments.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.EffectiveDate).ToList(),
                        //--utmcCompositionalTransactions = utmcCompositionalTransactions.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.DateOfTransaction).ToList(),
                        //--utmcMemberInvestments = utmcMemberInvestments.Where(x => x.ReportDate == utmcMemberInvestments.Max(y => y.ReportDate)).Where(x => x.IpdFundCode == fundCode).ToList(),
                        utmcFundInformation = UTMCFundInformations.Where(x => x.IpdFundCode == fundCode).FirstOrDefault(),
                        holderLedgers = holderLedgers.Where(x => x.FundID == fundCode).ToList()
                    };
                    objs.Add(utmcDetailedMemberInvestment);
                }
            }
            return objs;
        }

        public static List<HolderInv> GetHolderInvByHolderNo(string holderNo, List<UtmcFundInformation> UTMCFundInformations)
        {
            if (UTMCFundInformations.Count == 0)
            {
                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                }
            }
            string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());
            List<HolderInv> holderInvs = new List<HolderInv>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderInvApi?holderNo=" + holderNo + "&fundIds=" + fundIds).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderInvs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderInv>>(responseFromApi.Data.ToString());
                }
            }

            List<HolderFundDiv> holderFundDivs = GetHolderFundDivByHolderNo(holderNo, UTMCFundInformations);

            holderInvs.ForEach(hi =>
            {
                HolderFundDiv holderFundDiv = holderFundDivs.Where(x => x.FundId == hi.FundId).FirstOrDefault();
                if (holderFundDiv != null)
                {
                    DistributionIns distributionIns = (DistributionIns)(Convert.ToInt32(holderFundDiv.DivPymt));
                    hi.isDisFromOracle = 1;
                    hi.DistributionIns = (Convert.ToInt32(holderFundDiv.DivPymt));
                    hi.DistributionInsString = distributionIns.ToString();
                }
                else
                {
                    hi.isDisFromOracle = 0;
                    hi.DistributionIns = (Int32)(DistributionIns.Reinvest);
                    hi.DistributionInsString = DistributionIns.Reinvest.ToString();
                }
            });
            holderInvs = holderInvs.OrderBy(x => x.FundId).ToList();

            return holderInvs;
        }

        public static Response GetDataByQuery(string query)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/HolderLedgerApi/Get?filter=" + query).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = responseFromAPI.Data;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }
        
        public static Response GetOccupationDescData(String occuCode)
        {
            List<string> excludeList = new List<string> {
                //"H- SENIOR MANAGEMENT",
                //"H-BANKING",
                //"H-CONSTRUCTION",
                //"H-CONSULTANT",
                //"H-ENGINEERING",
                //"H-GAMBLING",
                //"H-GAMING",
                //"H-HEALTHCARE",
                //"H-HOUSEWIFE",
                //"H-IMPORT/EXPORT",
                //"H-JEWEL/GEM DEALER",
                //"H-MONEY CHARGER",
                //"H-PAWN BROKERS",
                //"H-PROFESSIONAL",
                //"H-LECTURER",
                //"H-RETIRER",
                //"H-STUDENT",
                //"H-STOCK BROKER",
                //"H-TRAVEL AGENCIES",
                //"H-TRUCK DEALER",
                //"H-USED AUTOMOBILE",
                //"L-(SE) AGRICULTURE",
                //"L-AGENT",
                //"L-APPAREL/FASHION",
                //"L-AUTOMOTIVE",
                //"L-BANKING",
                //"L-CLERICAL",
                //"L-COMMUNICATION",
                //"L-CONSTRUCTION",
                //"L-CONSULTANCY",
                //"L-DESIGNER",
                //"L-EDUCATION",
                //"L-ENGINEERING",
                //"L-ENTERTAINMENT",
                //"L-FACTORY",
                //"L-FOOD/BEVERAGE",
                //"L-HEALTHCARE",
                //"L-HOUSEKEEPER",
                //"L-HOUSEWIFE",
                //"L-IT & COMPUTER",
                //"L-LECTURER",
                //"L-MANUFACTURAL",
                //"L-MIDDLE MANAGEMENT",
                //"L-ONLINE BUSINESS",
                //"L-OPERATOR",
                //"L-PROFESSIONAL",
                //"L-PROPERTY",
                //"L-PUBLIC WORKER",
                //"L-RETIREE",
                //"L-SALES & MARKETING",
                //"L-SECRETARY",
                //"L-SECURITY GUARD",
                //"L-SELF EMPLOYED",
                //"L-SENIOR MANAGEMENT",
                //"L-STUDENT",
                //"L-SUPPLIER",
                //"L-TECHNICIAN",
                //"L-TRADING",
                //"L-TRAINER/EVENT",
                //"L-TRANSPORTATION",
                "NOT AVAILABLE"
            };
            try
            {
                Response response = GetDataByQuery("select * from uts.occpn where occ_code ='" + occuCode + "'");
                List<OccupationDTO> occupationDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationDTO>>(response.Data.ToString());
                occupationDTOs = occupationDTOs.Where(x => !excludeList.Contains(x.SDESC)).ToList();
                occupationDTOs = occupationDTOs.Where(x => x.SDESC != "L-STUDENT" && x.SDESC != "NOT AVAILABLE" && x.SDESC != "L-RETIRER" && x.SDESC != "H-RETIRER" && x.SDESC != "L-HOUSEWIFE" && x.SDESC != "H-HOUSEWIFE").ToList();
                occupationDTOs.ForEach(x =>
                {
                    x.OCCCODE = x.SDESC.Replace("L-", "").Replace("H-", "").Replace("(SE) ", "");
                });
                occupationDTOs.ForEach(x =>
                {
                    x.OCCCODE = x.OCCCODE.Trim().Capitalize();
                });
                OccupationDTO occupationDTONA = occupationDTOs.FirstOrDefault(x => x.OCCCODE == "Not Available");
                occupationDTOs = occupationDTOs.Where(x => x.OCCCODE != "Not Available").ToList();
                occupationDTOs = occupationDTOs.OrderBy(x => x.OCCCODE).ToList();
                if (occupationDTONA != null)
                    occupationDTOs.Add(occupationDTONA);

                var groupBy = occupationDTOs.GroupBy(x => x.OCCCODE).Select(grp => grp.Count() > 1 ? grp.FirstOrDefault(y => y.SDESC.Contains("L-")) : grp.FirstOrDefault()).ToList();
                responseCustom.IsSuccess = true;
                responseCustom.Data = groupBy.FirstOrDefault();
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }
        public static string UpdateMaHolderRegByHolderNo(MaHolderReg obj)
        {
            MaHolderReg maHolderReg = new MaHolderReg();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpContent content = new StringContent(obj.ToString(), Encoding.UTF8, "application/json");
                var response = client.PostAsJsonAsync("api/MaHolderRegApi/PostSingle", obj).Result;
                string responseString = "";
                if (response.IsSuccessStatusCode)
                {
                    responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    //maHolderReg = Newtonsoft.Json.JsonConvert.DeserializeObject<MaHolderReg>(responseString);
                }
                return responseString;
            }
        }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        public static List<FundDetailedInformation> FundDetailedInformations = new List<FundDetailedInformation>();

        //public static List<UtmcDetailedMemberInvestment> GetMAAccountSummary(string holderNo)
        //{
        //    if (GlobalProperties.FundDetailedInformations == null)
        //    {
        //        FundDetailedInformations = IUtmcFundInformationService.GetFundDetailedInformation();
        //        GlobalProperties.FundDetailedInformations = FundDetailedInformations;
        //    }
        //    List<UtmcDetailedMemberInvestment> objs = new List<UtmcDetailedMemberInvestment>();

        //    DateTime currentDate = DateTime.Now;
        //    DateTime yearsBefore = currentDate.AddYears(-6);

        //    List<UtmcMemberInvestment> utmcMemberInvestments = GetMemberInvestmentsByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"));
        //    List<UtmcCompositionalTransaction> utmcCompositionalTransactions = GetCompositionalTransactionByHolderNo(holderNo);

        //    foreach (string fundCode in utmcMemberInvestments.Select(x => x.IpdFundCode).Distinct().ToList())
        //    {
        //        UtmcDetailedMemberInvestment utmcDetailedMemberInvestment = new UtmcDetailedMemberInvestment
        //        {
        //            utmcMemberInvestments = utmcMemberInvestments.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.EffectiveDate).ToList(),
        //            utmcCompositionalTransactions = utmcCompositionalTransactions.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.DateOfTransaction).ToList(),
        //            //utmcMemberInvestments = utmcMemberInvestments.Where(x => x.ReportDate == utmcMemberInvestments.Max(y => y.ReportDate)).Where(x=>x.IpdFundCode == fundCode).ToList(),
        //            fundDetailedInformation = GlobalProperties.FundDetailedInformations.Where(x => x.UtmcFundInformation.IpdFundCode == fundCode).FirstOrDefault()
        //        };
        //        objs.Add(utmcDetailedMemberInvestment);
        //    }

        //    return objs;
        //}

        public static List<UtmcMemberInvestment> GetMemberInvestmentsByHolderNo(string holderNo, string startDate, string endDate)
        {
            List<UtmcMemberInvestment> utmcMemberInvestments = new List<UtmcMemberInvestment>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcMemberInvestmentApi?holderNo=" + holderNo + "&startDate=" + startDate + "&endDate=" + endDate).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcMemberInvestments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcMemberInvestment>>(responseString);
                }
            }
            return utmcMemberInvestments;
        }

        public static List<UtmcCompositionalTransaction> GetCompositionalTransactionByHolderNo(string holderNo)
        {
            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcCompositionalTransactionApi?holderNo=" + holderNo).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcCompositionalTransactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcCompositionalTransaction>>(responseString);
                }
            }
            return utmcCompositionalTransactions;
        }

        //public static ConsolidatedAccountSummary GetConsolidatedAccountSummary(User user)
        //{
        //    Response responseUFIList = IUtmcFundInformationService.GetData(0, 0, false);
        //    if (responseUFIList.IsSuccess)
        //    {
        //        List<UtmcFundInformation> UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;

        //        Response responseUAList = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
        //        if (responseUAList.IsSuccess)
        //        {
        //            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
        //            userAccounts = userAccounts.Where(x => x.IsVerified == 1 && x.Status == 1).ToList();
        //            List<PortfolioAllocation> PortfolioAllocations = new List<PortfolioAllocation>();
        //            List<FundAllocation> FundAllocations = new List<FundAllocation>();
        //            Decimal consolidatedTotalActualCost = 0;
        //            Decimal consolidatedTotalInvestmentRM = 0;
        //            Decimal consolidatedTotalInvestmentUNITS = 0;
        //            Decimal consolidatedAccountUnrealisedGainRM = 1000;
        //            List<UtmcMemberInvestment> consolidatedInvestments = new List<UtmcMemberInvestment>();
        //            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
        //            DateTime asAtDate = DateTime.Now;
        //            userAccounts.ForEach(uA =>
        //            {
        //                Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
        //                if (responseMA.IsSuccess)
        //                {
        //                    MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
        //                    List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = new List<UtmcDetailedMemberInvestment>();
        //                    //Int32 MAHolderRegID = uA.MaHolderRegId;
        //                    //String propertyName = nameof(UtmcMemberInvestment.IpdMemberAccNo);

        //                    //API
        //                    utmcDetailedMemberInvestments = ServicesManager.GetMAAccountSummary(uA.AccountNo);

        //                    //Local
        //                    //utmcDetailedMemberInvestments = IUtmcMemberInvestmentService.GetMAAccountSummary(propertyName, maHolderReg.HolderNo.ToString()).ToList();


        //                    consolidatedTotalActualCost += utmcDetailedMemberInvestments.Sum(x => (x.utmcMemberInvestments[0].ActualCost));
        //                    consolidatedTotalInvestmentRM += utmcDetailedMemberInvestments.Sum(x => (x.utmcMemberInvestments[0].Units * x.fundDetailedInformation.CurrentUnitPrice));
        //                    consolidatedTotalInvestmentUNITS += utmcDetailedMemberInvestments.Sum(x => x.utmcMemberInvestments[0].Units);
        //                    utmcDetailedMemberInvestments.ForEach(x =>
        //                    {
        //                        consolidatedInvestments.AddRange(x.utmcMemberInvestments);
        //                        utmcCompositionalTransactions.AddRange(x.utmcCompositionalTransactions);
        //                    });
        //                    List<UtmcFundInformation> ExistingUTMCFundInformations = UTMCFundInformations.Where(x => utmcDetailedMemberInvestments.Select(y => y.fundDetailedInformation.UtmcFundInformation.Id).Contains(x.Id)).ToList();
        //                    asAtDate = DateTime.Now.AddDays(-1);
        //                    foreach (UtmcFundInformation fund in ExistingUTMCFundInformations)
        //                    {
        //                        FundAllocations.Add(new FundAllocation
        //                        {
        //                            FundCode = fund.FundCode,
        //                            FundName = fund.FundName.Capitalize(),
        //                            Investment = utmcDetailedMemberInvestments.Where(x => x.fundDetailedInformation.UtmcFundInformation.Id == fund.Id).Sum(x => x.utmcMemberInvestments[0].Units)
        //                        });
        //                    }
        //                    PortfolioAllocations.Add(new PortfolioAllocation
        //                    {
        //                        AccountPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls),
        //                        Investment = utmcDetailedMemberInvestments.Sum(x => x.utmcMemberInvestments[0].Units)
        //                    });
        //                }
        //            });
        //            //groupedByYear
        //            var groupedList = consolidatedInvestments
        //                                               .GroupBy(u => u.EffectiveDate.Year)
        //                                               .Select(grp => grp.ToList())
        //                                               .ToList();

        //            //((u.utmcMemberInvestments[0].Units * u.fundDetailedInformation.CurrentUnitPrice) - (u.utmcMemberInvestments[0].ActualCost)) / (u.utmcMemberInvestments[0].ActualCost)
        //            //                        ) *100

        //            List<InvestmentPerformance> InvestmentPerformances = new List<InvestmentPerformance>();
        //            foreach (List<UtmcMemberInvestment> invests in groupedList)
        //            {
        //                decimal InvestUnits = 0;
        //                decimal InvestMarketValue = 0;
        //                decimal GainLoss = 0;

        //                var groupByFundList = invests.GroupBy(x => x.IpdFundCode).Select(grp => grp.OrderByDescending(y => y.EffectiveDate).ToList());
        //                foreach (List<UtmcMemberInvestment> invests2 in groupByFundList)
        //                {
        //                    UtmcFundInformation uFI = UTMCFundInformations.Where(x => x.IpdFundCode == invests2.FirstOrDefault().IpdFundCode).FirstOrDefault();
        //                    //IUtmcFundInformationService.GetDataByPropertyName(nameof(UtmcFundInformation.IpdFundCode), invests.FirstOrDefault().IpdFundCode, true, 0, 0, false).FirstOrDefault();
        //                    InvestUnits += invests2.FirstOrDefault().Units;
        //                    InvestMarketValue += (invests2.FirstOrDefault().Units * uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice);
        //                    GainLoss += ((invests2.FirstOrDefault().Units * uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice) - invests2.FirstOrDefault().ActualCost);
        //                }

        //                InvestmentPerformances.Add(new InvestmentPerformance
        //                {
        //                    ReportDate = invests.OrderByDescending(x => x.EffectiveDate).FirstOrDefault().EffectiveDate,
        //                    InvestUnits = InvestUnits,
        //                    InvestMarketValue = InvestMarketValue,
        //                    DividendPayout = 0,
        //                    GainLoss = GainLoss
        //                    //GainLoss = ((invests.FirstOrDefault().MarketValue - invests.FirstOrDefault().ActualCost) / invests.FirstOrDefault().ActualCost) * 100
        //                });
        //                decimal a = invests.FirstOrDefault().MarketValue;
        //                decimal b = invests.FirstOrDefault().ActualCost;
        //            }

        //            List<UtmcCompositionalTransaction> YearData = utmcCompositionalTransactions.ToList();

        //            var groupedByYearMonthList = YearData.GroupBy(x => new { x.EffectiveDate.Value.Year, x.EffectiveDate.Value.Month })
        //                                        .Select(grp => grp.ToList())
        //                                        .ToList();
        //            List<PortfolioMarketValueMovement> PortfolioMarketValueMovements = new List<PortfolioMarketValueMovement>();

        //            foreach (List<UtmcCompositionalTransaction> invests in groupedByYearMonthList)
        //            {
        //                decimal InvestUnits = 0;
        //                decimal InvestMarketValue = 0;
        //                decimal RealizedProfit = 0;

        //                var groupByFundList = invests.GroupBy(x => x.IpdFundCode).Select(grp => grp.ToList());
        //                foreach (List<UtmcCompositionalTransaction> invests2 in groupByFundList)
        //                {
        //                    UtmcFundInformation uFI = UTMCFundInformations.Where(x => x.IpdFundCode == invests2.FirstOrDefault().IpdFundCode).FirstOrDefault();
        //                    //IUtmcFundInformationService.GetDataByPropertyName(nameof(UtmcFundInformation.IpdFundCode), invests.FirstOrDefault().IpdFundCode, true, 0, 0, false).FirstOrDefault();
        //                    InvestUnits += invests2.FirstOrDefault().Units.Value;
        //                    InvestMarketValue += (invests2.FirstOrDefault().Units.Value * uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice);
        //                    RealizedProfit += (invests.Sum(x => x.RealisedGainLoss.Value));
        //                }
        //                //UtmcFundInformation uFI = UTMCFundInformations.Where(x => x.IpdFundCode == invests.FirstOrDefault().IpdFundCode).FirstOrDefault();
        //                //IUtmcFundInformationService.GetDataByPropertyName(nameof(UtmcFundInformation.IpdFundCode), invests.FirstOrDefault().IpdFundCode, true, 0, 0, false).FirstOrDefault();
        //                PortfolioMarketValueMovements.Add(new PortfolioMarketValueMovement
        //                {
        //                    ReportDate = invests.OrderByDescending(x => x.EffectiveDate.Value).FirstOrDefault().EffectiveDate.Value,
        //                    InvestUnits = InvestUnits,
        //                    //portfolio marketvalue movement
        //                    InvestMarketValue = InvestMarketValue,
        //                    RealizedProfit = RealizedProfit,
        //                    DividendPayout = 0,
        //                });
        //            }

        //            PortfolioAllocations.ForEach(x =>
        //            {
        //                x.Percent = ((x.Investment != 0 ? x.Investment : 1) / (PortfolioAllocations.Sum(y => y.Investment != 0 ? y.Investment : 1))) * 100;
        //            });
        //            PortfolioAllocations = PortfolioAllocations
        //                .GroupBy(grp => grp.AccountPlan)
        //                .Select(g => new PortfolioAllocation
        //                {
        //                    AccountPlan = g.FirstOrDefault().AccountPlan,
        //                    Investment = g.Sum(x => x.Investment),
        //                    Percent = g.Sum(x => x.Percent)
        //                }).ToList();
        //            var groupedFundAllocations = FundAllocations.GroupBy(x => x.FundName.Capitalize()).Select(grp => grp.ToList()).ToList();
        //            List<FundAllocation> FundAllocationsNew = new List<FundAllocation>();
        //            foreach (List<FundAllocation> fs in groupedFundAllocations)
        //            {
        //                if (fs.Sum(x => x.Investment) != 0)
        //                {
        //                    FundAllocationsNew.Add(new FundAllocation
        //                    {
        //                        FundCode = fs.FirstOrDefault().FundCode,
        //                        FundName = fs.FirstOrDefault().FundName.Capitalize(),
        //                        Investment = fs.Sum(x => x.Investment),
        //                    });
        //                }
        //            }
        //            FundAllocationsNew.ForEach(x =>
        //            {
        //                x.Percent = (x.Investment / (FundAllocationsNew.Sum(y => y.Investment))) * 100;
        //            });
        //            consolidatedAccountUnrealisedGainRM = (((consolidatedTotalInvestmentRM - consolidatedTotalActualCost) != 0 ? (consolidatedTotalInvestmentRM - consolidatedTotalActualCost) : 1) / (consolidatedTotalActualCost != 0 ? consolidatedTotalActualCost : 1)) * 100;
        //            InvestmentPerformances = InvestmentPerformances.OrderBy(x => x.ReportDate).ToList();
        //            PortfolioMarketValueMovements = PortfolioMarketValueMovements.OrderBy(x => x.ReportDate).ToList();

        //            ConsolidatedAccountSummary consolidatedAccountSummary = new ConsolidatedAccountSummary
        //            {
        //                asAtDate = asAtDate,
        //                consolidatedTotalInvestmentRM = consolidatedTotalInvestmentRM,
        //                consolidatedTotalInvestmentUNITS = consolidatedTotalInvestmentUNITS,
        //                NoOfAccounts = userAccounts.Count,
        //                consolidatedAccountUnrealisedGainRM = consolidatedAccountUnrealisedGainRM,
        //                InvestmentPerformances = InvestmentPerformances,
        //                PortfolioMarketValueMovements = PortfolioMarketValueMovements,
        //                PortfolioAllocations = PortfolioAllocations,
        //                FundAllocations = FundAllocationsNew
        //            };

        //            return consolidatedAccountSummary;
        //        }
        //    }
        //    return null;
        //}

        public class ConsolidatedAccountSummary
        {
            public DateTime asAtDate { get; set; }
            public Decimal consolidatedTotalInvestmentRM { get; set; }
            public Decimal consolidatedTotalInvestmentUNITS { get; set; }
            public Int32 NoOfAccounts { get; set; }
            public Decimal consolidatedAccountUnrealisedGainRM { get; set; }
            public List<InvestmentPerformance> InvestmentPerformances { get; set; }
            public List<PortfolioMarketValueMovement> PortfolioMarketValueMovements { get; set; }
            public List<PortfolioAllocation> PortfolioAllocations { get; set; }
            public List<FundAllocation> FundAllocations { get; set; }
        }

        public static List<FundInfoDTO> GetFundInfoByFundIds(string fundIdsString)
        {
            List<FundInfoDTO> fundInfoDTOs = new List<FundInfoDTO>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/FundInfo/Get?fund_ids=" + fundIdsString).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    fundInfoDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FundInfoDTO>>(responseFromApi.Data.ToString());
                }
            }
            return fundInfoDTOs;
        }

        public static List<HolderLedger> GetHolderLedgerByHolderNo(string filter)
        {
            List<HolderLedger> holderLedgers = new List<HolderLedger>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderLedgers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderLedger>>(responseFromApi.Data.ToString());
                }
            }
            return holderLedgers;
        }

        public static List<HolderInv> GetHolderInvByHolderNo(string holderNo)
        {

            List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
            Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
            if (responseUFIList.IsSuccess)
            {
                UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
            }
            string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());
            List<HolderInv> holderInvs = new List<HolderInv>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderInvApi?holderNo=" + holderNo + "&fundIds=" + fundIds).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderInvs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderInv>>(responseFromApi.Data.ToString());
                }
            }

            //List<HolderFundDiv> holderFundDivs = GetHolderFundDivByHolderNo(holderNo);

            //holderInvs.ForEach(hi =>
            //{
            //    HolderFundDiv holderFundDiv = holderFundDivs.Where(x => x.FundId == hi.FundId).FirstOrDefault();
            //    if (holderFundDiv != null)
            //    {
            //        DistributionIns distributionIns = (DistributionIns)(Convert.ToInt32(holderFundDiv.DivPymt));
            //        hi.isDisFromOracle = 1;
            //        hi.DistributionIns = (Convert.ToInt32(holderFundDiv.DivPymt));
            //        hi.DistributionInsString = distributionIns.ToString();
            //    }
            //    else
            //    {
            //        hi.isDisFromOracle = 0;
            //        hi.DistributionIns = (Int32)(DistributionIns.Reinvest);
            //        hi.DistributionInsString = DistributionIns.Reinvest.ToString();
            //    }
            //});


            return holderInvs;
        }

        public static List<HolderFundDiv> GetHolderFundDivByHolderNo(string holderNo, List<UtmcFundInformation> UTMCFundInformations = null)
        {
            if (UTMCFundInformations == null)
            {
                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                }
            }
            string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());
            List<HolderFundDiv> holderFundDivs = new List<HolderFundDiv>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderFundDivApi?holderNo=" + holderNo + "&fundIds=" + fundIds).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderFundDivs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderFundDiv>>(responseFromApi.Data.ToString());
                }
            }
            return holderFundDivs;
        }

        public static List<CurrentNAVDTO> GetcurrentNAV(string fundIdsString)
        {
            List<CurrentNAVDTO> CurrentNAVDTOs = new List<CurrentNAVDTO>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/NAVApi/Get?fundIdsString=" + fundIdsString).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    CurrentNAVDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrentNAVDTO>>(responseFromApi.Data.ToString());
                }
            }
            return CurrentNAVDTOs;
        }

        public static CurrentNAVDTO GetNAVByFundAndDate(string fundIdsString, string dateString)
        {
            string apiURL = "api/NAVApi/GetNAVByFundAndDate?fundIdsString=" + fundIdsString + "&dateString=" + dateString;
            CurrentNAVDTO currentNAVDTO = new CurrentNAVDTO();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(apiURL).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    if (responseFromApi.IsSuccess)
                    {
                        Logger.WriteLog("GetNAVByFundAndDate apiURL: " + apiURL);
                        List<CurrentNAVDTO> currentNAVDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrentNAVDTO>>(responseFromApi.Data.ToString());
                        currentNAVDTO = currentNAVDTOs.FirstOrDefault();
                    }
                    else
                    {
                        Logger.WriteLog("GetNAVByFundAndDate: " + responseFromApi.Message + " - Exception");
                    }
                }
            }
            return currentNAVDTO;
        }

        public static Response GetActiveMAsCount()
        {
            String ActiveMAsCount = "";
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 10);
                    StringBuilder filter = new StringBuilder();
                    //filter.Append(" and id_no_2 not in ('STA', 'STA  2', 'STA 2', 'STA 2 (10JF)', 'STA 3', 'STA2', 'STA3')");
                    //filter.Append(" and id_no_ol not in ('STA2', '10 JF', 'STA2')");
                    //filter.Append(" and id_no_3 not in ('10JF', 'STA 3', 'STA3')");
                    filter.Append(" select count(*) as macount from uts.holder_reg where holder_status='A' ");
                    var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            List<OracleMACountStats> oracleStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OracleMACountStats>>(responseFromAPI.Data.ToString());
                            if (oracleStatsList.Count > 0)
                            {
                                ActiveMAsCount = oracleStatsList.First().MACOUNT;
                            }
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = ActiveMAsCount;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (TaskCanceledException ex)
            {
                // Check ex.CancellationToken.IsCancellationRequested here.
                // If false, it's pretty safe to assume it was a timeout.
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = "API may didn't respond within Timeout. " + ex.Message;
            }
            return responseCustom;
        }

        public static Response GetAssetValue(string holderNos)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    //filter.Append(" and id_no_2 not in ('STA', 'STA  2', 'STA 2', 'STA 2 (10JF)', 'STA 3', 'STA2', 'STA3')");
                    //filter.Append(" and id_no_ol not in ('STA2', '10 JF', 'STA2')");
                    //filter.Append(" and id_no_3 not in ('10JF', 'STA 3', 'STA3')");
                    filter.Append(" select SUM(CURR_UNIT_HLDG * HLDR_AVE_PRICE) as ASSETVALUE from UTS.HOLDER_INV ");
                    if (holderNos != "")
                    {
                        filter.Append(" where HOLDER_NO in (" + holderNos + ") ");
                    }
                    var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            List<AssetStats> assetStats = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AssetStats>>(responseFromAPI.Data.ToString());
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = assetStats;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetRegisteredMAsCountByMTDLastYear()
        {
            try
            {

                StringBuilder filter = new StringBuilder();
                filter.Append(" select YEAR(created_date) as YEAR, MONTH(created_date) as MONTH, count(*) as MACOUNT from user_accounts where YEAR(created_date) in (YEAR(CURDATE()), YEAR(CURDATE() - INTERVAL '1' YEAR)) and is_verified=1 and status=1 group by YEAR(created_date), MONTH(created_date) order by YEAR(created_date) desc, MONTH(created_date) desc ");

                Response response = GenericService.GetDataByQuery(filter.ToString(), 0, 0, false, null, false, null, true);
                if (response.IsSuccess)
                {
                    string json = JsonConvert.SerializeObject(response.Data);
                    List<OracleMACountStats> oracleStatsList = JsonConvert.DeserializeObject<List<OracleMACountStats>>(json);
                    List<OracleMACountStatsByYear> oracleStatsByYears = new List<OracleMACountStatsByYear>();
                    oracleStatsList.GroupBy(x => x.YEAR).ToList().ForEach(x =>
                    {
                        oracleStatsByYears.Add(new OracleMACountStatsByYear
                        {
                            YEAR = x.First().YEAR,
                            mACountByMonths = x.Select(y => new MACountByMonth { Month = new DateTime(2010, Convert.ToInt32(y.MONTH), 1).ToString("MMM", CultureInfo.InvariantCulture), MACOUNT = y.MACOUNT }).ToList()
                        });
                    });
                    oracleStatsByYears = oracleStatsByYears.OrderBy(x => x.YEAR).ToList();
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = oracleStatsByYears;
                }
                else
                {
                    responseCustom = response;
                }
                //using (var client = new HttpClient())
                //{
                //    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                //    client.BaseAddress = new Uri(baseURL);
                //    client.DefaultRequestHeaders.Accept.Clear();
                //    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //    StringBuilder filter = new StringBuilder();
                //    //filter.Append(" and id_no_2 not in ('STA', 'STA  2', 'STA 2', 'STA 2 (10JF)', 'STA 3', 'STA2', 'STA3')");
                //    //filter.Append(" and id_no_ol not in ('STA2', '10 JF', 'STA2')");
                //    //filter.Append(" and id_no_3 not in ('10JF', 'STA 3', 'STA3')");
                //    //
                //    //ORACLE
                //    //filter.Append(" select TO_CHAR(reg_dt, 'YYYY') as year, TO_CHAR(reg_dt, 'MM') as month, count(*) as macount from uts.holder_reg where TO_CHAR(reg_dt, 'YYYY') in (TO_CHAR(SYSDATE, 'YYYY'), TO_CHAR(SYSDATE - INTERVAL '1' YEAR, 'YYYY')) group by TO_CHAR(reg_dt, 'YYYY'), TO_CHAR(reg_dt, 'MM') order by TO_CHAR(reg_dt, 'YYYY') desc, TO_CHAR(reg_dt, 'MM') desc ");
                //    //MYSQL
                //    filter.Append(" select YEAR(created_date) as year, MONTH(created_date) as month, count(*) as macount from user_accounts where YEAR(created_date) in (YEAR(CURDATE()), YEAR(CURDATE() - INTERVAL '1' YEAR)) group by YEAR(created_date), MONTH(created_date) order by YEAR(created_date) desc, MONTH(created_date) desc ");
                //    var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter.ToString()).Result;
                //    if (response.IsSuccessStatusCode)
                //    {
                //        string responseString = response.Content.ReadAsStringAsync().Result;
                //        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                //        if (responseFromAPI.IsSuccess)
                //        {
                //            List<OracleStats> oracleStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OracleStats>>(responseFromAPI.Data.ToString());
                //            List<OracleStatsByYear> oracleStatsByYears = new List<OracleStatsByYear>();
                //            oracleStatsList.GroupBy(x => x.YEAR).ToList().ForEach(x =>
                //            {
                //                oracleStatsByYears.Add(new OracleStatsByYear
                //                {
                //                    YEAR = x.First().YEAR,
                //                    mACountByMonths = x.Select(y => new MACountByMonth { Month = new DateTime(2010, Convert.ToInt32(y.MONTH), 1).ToString("MMM", CultureInfo.InvariantCulture), MACOUNT = y.MACOUNT }).ToList()
                //                });
                //            });
                //            oracleStatsByYears = oracleStatsByYears.OrderBy(x => x.YEAR).ToList();
                //            responseCustom.IsSuccess = true;
                //            responseCustom.Data = oracleStatsByYears;
                //        }
                //        else
                //        {
                //            responseCustom = responseFromAPI;
                //        }
                //        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                //    }
                //    else
                //    {
                //        responseCustom.IsSuccess = false;
                //        responseCustom.Message = response.ReasonPhrase;
                //    }
                //}
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetRegisteredMAsAssetByMTDLastYear()
        {
            try
            {

                StringBuilder filter = new StringBuilder();
                filter.Append(" select YEAR(settlement_date) as YEAR, MONTH(settlement_date) as MONTH, if(SUM(trans_amt) IS NULL, 0, SUM(trans_amt)) as TRANSAMT from user_orders where status=1 and order_status=3 and order_type=1 and YEAR(settlement_date) in (YEAR(CURDATE()), YEAR(CURDATE() - INTERVAL '1' YEAR)) group by YEAR(settlement_date), MONTH(settlement_date) order by YEAR(settlement_date) desc, MONTH(settlement_date) desc ");

                Response response = GenericService.GetDataByQuery(filter.ToString(), 0, 0, false, null, false, null, true);
                if (response.IsSuccess)
                {
                    string json = JsonConvert.SerializeObject(response.Data);
                    List<OracleMAAssetStats> oracleStatsList = JsonConvert.DeserializeObject<List<OracleMAAssetStats>>(json);
                    List<OracleMAAssetStatsByYear> oracleStatsByYears = new List<OracleMAAssetStatsByYear>();
                    oracleStatsList.GroupBy(x => x.YEAR).ToList().ForEach(x =>
                    {
                        oracleStatsByYears.Add(new OracleMAAssetStatsByYear
                        {
                            YEAR = x.First().YEAR,
                            mAAssetByMonths = x.Select(y => new MAAssetByMonth { Month = new DateTime(2010, Convert.ToInt32(y.MONTH), 1).ToString("MMM", CultureInfo.InvariantCulture), TRANSAMT = y.TRANSAMT }).ToList()
                        });
                    });
                    oracleStatsByYears = oracleStatsByYears.OrderBy(x => x.YEAR).ToList();
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = oracleStatsByYears;
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response isProcessedMonthlyCheck(int process_type)
        {
            Response response = new Response();
            try
            {
                StringBuilder filter = new StringBuilder();
                filter.Append(@"SELECT * from ws_processes where process_type=" + process_type + " and MONTH(processed_date) = '" + DateTime.Now.ToString("MM") + "' and YEAR(processed_date) = '" + DateTime.Now.ToString("yyyy") + "' order by ID desc ");
                Response responseDailyProcessRow = GenericService.GetDataByQuery(filter.ToString(), 0, 0, false, null, false, null, true);
                if (responseDailyProcessRow.IsSuccess)
                {
                    var dynamicRow = responseDailyProcessRow.Data;
                    var dynToJSON = JsonConvert.SerializeObject(dynamicRow);
                    List<WS_Process> processList = JsonConvert.DeserializeObject<List<WS_Process>>(dynToJSON);
                    response.IsSuccess = true;
                    response.Data = processList;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Logger.WriteLog("isProcessedDailyCheck: " + ex.Message + " - Exception");
                //return true;
            }
            return response;
        }

        public static Response GenerateStatements(User loginUser)
        {
            Response response = new Response();
            try
            {
                WS_Process wS_Process = new WS_Process
                {
                    is_processed = 1,
                    processed_date = DateTime.Now,
                    process_type = 101,
                    status = 0,
                    message = "Clicked by "+ loginUser.Username
                };
                Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                if (!responsePost.IsSuccess)
                {
                    Logger.WriteLog("GenerateMonthlyReport WS_Process PostData: " + responsePost.Message + " - Exception");
                }

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
        //AO

        public static Response GetMonthlyIncomeData()
        {
            try
            {
                Response response = GetDataByQuery("select * from uts.income");
                List<IncomeDTO> incomeDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IncomeDTO>>(response.Data.ToString());
                responseCustom.IsSuccess = true;
                responseCustom.Data = incomeDTOs;
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetNewMANo()
        {
            Response response = new Response();
            string newMANoQuery = "select HOLDER_NO from UTS.HOLDER_REG where rownum=1 order by HOLDER_NO desc";
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var responseApi = client.GetAsync("api/HolderLedgerApi?filter=" + newMANoQuery).Result;
                if (responseApi.IsSuccessStatusCode)
                {
                    string responseString = responseApi.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    dynamic holdernos = JsonConvert.DeserializeObject<dynamic>(response.Data.ToString());
                    response.Data = (holdernos[0].HOLDERNO + 1);
                }
            }
            return response;
        }
        public static Response InsertRecord(string insertQuery)
        {
            Response response = new Response();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var responseApi = client.GetAsync("api/HolderLedgerApi?filter=" + insertQuery).Result;
                if (responseApi.IsSuccessStatusCode)
                {
                    string responseString = responseApi.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                }
            }
            return response;
        }

        public static Response GetNewTransNo()
        {
            Response response = new Response();
            string newMANoQuery = HttpContext.Current.Server.UrlEncode("select TRANS_NO from (select TRANS_NO from UTS.HOLDER_LEDGER where REGEXP_LIKE(TRANS_NO, '^[[:digit:]]+$') order by TO_NUMBER(TRANS_NO) desc) where rownum = 1");
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var responseApi = client.GetAsync("api/HolderLedgerApi?filter=" + newMANoQuery).Result;
                if (responseApi.IsSuccessStatusCode)
                {
                    string responseString = responseApi.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    dynamic TRANS_NOs = JsonConvert.DeserializeObject<dynamic>(response.Data.ToString());
                    response.Data = (Convert.ToInt32(TRANS_NOs[0].TRANSNO) + 1);
                }
            }
            return response;
        }
    }

    public class OracleMACountStats
    {
        public String YEAR { get; set; }
        public String MONTH { get; set; }
        public String MACOUNT { get; set; }
    }

    public class OracleMAAssetStats
    {
        public String YEAR { get; set; }
        public String MONTH { get; set; }
        public String TRANSAMT { get; set; }
    }

    public class OracleMACountStatsByYear
    {
        public String YEAR { get; set; }
        public List<MACountByMonth> mACountByMonths { get; set; }
    }

    public class MACountByMonth
    {
        public String Month { get; set; }
        public String MACOUNT { get; set; }
    }

    public class AssetStats
    {
        public string ASSETVALUE { get; set; }
    }

    public class OracleMAAssetStatsByYear
    {
        public String YEAR { get; set; }
        public List<MAAssetByMonth> mAAssetByMonths { get; set; }
    }

    public class MAAssetByMonth
    {
        public String Month { get; set; }
        public String TRANSAMT { get; set; }
    }
}