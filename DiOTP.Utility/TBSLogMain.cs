﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("tbs_log_main")]
    public class TBSLogMain
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("table_name"), Display("TableName")]
        public String TableName { get; set; }
        [Database("description"), Display("Description")]
        public String Description { get; set; }
        [Database("task"), Display("Task")]
        public String Task { get; set; }
        [Database("original_ref_id"), Display("OriginalRefId")]
        public String OriginalRefId { get; set; }
        [Database("new_ref_id"), Display("NewRefId")]
        public String NewRefId { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32 UpdatedBy { get; set; }
    }
}
