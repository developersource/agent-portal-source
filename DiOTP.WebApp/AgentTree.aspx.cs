﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentTree : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetAgentHierarchy()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (user != null)
            {
                //Get agentcode from session
                string AgentUserId = user.Id.ToString();
                //string AgentUserId = "561";//591-siti 609-Marven Lim 607-wrights 590- Lee Kian Hing
                MainAgent CurrentAgent = new MainAgent();
                
                //Use agent user ID to get all direct and indirect downlines of the agent tree.
                try
                {
                    string getAgentCodeQuery = @"select arp.name,utd.name as agent_rank,ap.* from users u
												 join user_types ut on ut.user_id = u.id
												join user_types_def utd on utd.id = ut.user_type_id and utd.name in ('WA','WM','GM','SGM')
												 join agent_regs ar on ar.user_id = u.id
                                                 join agent_positions ap on ar.agent_code = ap.agent_code
                                                 join agent_reg_personal arp on arp.agent_reg_id = ar.id
                                                where u.id in ("+AgentUserId+")";
                    Response responseGetAgent = GenericService.GetDataByQuery(getAgentCodeQuery, 0, 0, false, null, false, null, false);
                    if (responseGetAgent.IsSuccess)
                    {
                        var agentDyn = responseGetAgent.Data;
                        var responseJSON = JsonConvert.SerializeObject(agentDyn);
                        List<dynamic> agentUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);

                        CurrentAgent.AgentName = agentUser.FirstOrDefault().Name;
                        CurrentAgent.AgentRank = agentUser.FirstOrDefault().AgentRank;
                        CurrentAgent.AgentCode = agentUser.FirstOrDefault().AgentCode;
                        CurrentAgent.AgentPosition = agentUser.FirstOrDefault().AgentPos;
                        CurrentAgent.AgentGroup = agentUser.FirstOrDefault().AgentGroup;
                    }

                    //string uplineQueryString = "";
                    //if (CurrentAgent.AgentPosition.Length > 3) {
                    //    uplineQueryString = " OR (ap.agent_pos in(";

                    //    for (int i = 3; i < CurrentAgent.AgentPosition.Length; i += 3)
                    //    {
                    //        if ((CurrentAgent.AgentPosition.Length - i) == 3)
                    //        {
                    //            uplineQueryString += "'" + CurrentAgent.AgentPosition.Substring(0, (CurrentAgent.AgentPosition.ToString().Length) - i) + "') and char_length(ap.agent_pos) <" + CurrentAgent.AgentPosition.Length + " )";
                    //        }
                    //        else
                    //        {
                    //            uplineQueryString += "'" + CurrentAgent.AgentPosition.Substring(0, (CurrentAgent.AgentPosition.ToString().Length) - i) + "',";
                    //        }
                    //    }
                    //}


                    //Use Agent Code/Agent IdString to get agent position
                    //          string getAgentsQuery = @"select arp.name,utd.name as agent_rank, arp.Status,ap.id,ap.Upline1,ap.Upline2,ap.Upline3, ap.prev_Upline1,ap.prev_Upline2,ap.prev_Upline3,ap.is_active, ap.status, ap.agent_pos, ap.agent_code,ap.created_date,ar.user_id, ut.id as UTID
                    //                              from agent_positions ap
                    //                              join agent_regs ar on ap.agent_code = ar.agent_code
                    //                              join agent_reg_personal arp on ar.id = arp.agent_reg_id
                    //join user_types ut on ut.user_id = ar.user_id
                    //join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                    //                              where (ap.agent_pos like '" + CurrentAgent.AgentPosition + "%' and char_length(ap.agent_pos) >" + CurrentAgent.AgentPosition.Length + " )"+uplineQueryString+" OR (ap.agent_pos = '" + CurrentAgent.AgentPosition + "') group by arp.id";
                    string getAgentsQuery = @"select arp.name,utd.name as agent_rank, arp.Status,ap.id,ap.Upline1,ap.Upline2,ap.Upline3, ap.prev_Upline1,ap.prev_Upline2,ap.prev_Upline3,ap.is_active, ap.status, ap.agent_pos, ap.agent_code,ap.created_date,ar.user_id, ut.id as UTID
                                        from agent_positions ap
                                        join agent_regs ar on ap.agent_code = ar.agent_code
                                        join agent_reg_personal arp on ar.id = arp.agent_reg_id
										join user_types ut on ut.user_id = ar.user_id
										join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                                        where ap.agent_group = '"+CurrentAgent.AgentGroup+"'group by arp.id";
                    Response responseAgentHierarchy = GenericService.GetDataByQuery(getAgentsQuery, 0, 0, false, null, false, null, true);
                    if (responseAgentHierarchy.IsSuccess)
                    {

                        var agentHierarchyDyn = responseAgentHierarchy.Data;
                        var responseJSON = JsonConvert.SerializeObject(agentHierarchyDyn);
                        List<dynamic> agentHierarchyList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);

                        //Separation of agent categories

                        // 1. Direct Downline Agents
                        List<dynamic> DirectDownlineAgents = agentHierarchyList.Where(x => x.agent_pos.ToString().Length == (CurrentAgent.AgentPosition.ToString().Length + 3)).ToList();

                        // 2. Indirect Downline Agents
                        List<dynamic> IndirectDownlineAgents = agentHierarchyList.Where(x => x.agent_pos.ToString().Length > (CurrentAgent.AgentPosition.ToString().Length + 3)).ToList();

                        // 3. Upline Agent
                        List<dynamic> UplineAgent = agentHierarchyList.Where(x => x.agent_pos.ToString().Length <= (CurrentAgent.AgentPosition.ToString().Length - 3)).ToList();


                        MainTreeBranch MainBranch = new MainTreeBranch {
                            CurrentName = CurrentAgent.AgentName,
                            CurrentRank = CurrentAgent.AgentRank,
                            CurrentPosition = CurrentAgent.AgentPosition,
                            DirectDownlineAgent = new List<DirectDownlineTreeBranch>()
                        };
                        IndirectDownlineTreeBranch DirectUpline = new IndirectDownlineTreeBranch
                        {
                            Name = "",
                            Rank = "",
                            CurrentPosition = ""
                        };
                        List<IndirectDownlineTreeBranch> IndirectUpline = new List<IndirectDownlineTreeBranch>();

                        for (int i = 0; i < 2; i++) {
                            IndirectUpline.Add(new IndirectDownlineTreeBranch {
                                Name = "",
                                Rank = "",
                                CurrentPosition = ""
                            });
                        }
                        MainBranch.DirectUplineAgent = DirectUpline;
                        MainBranch.IndirectUplineAgent = IndirectUpline;
                        //Check if have upline or not first
                        if (UplineAgent.Count == 1)
                        {
                            MainBranch.DirectUplineAgent.Name = UplineAgent.FirstOrDefault().name;
                            MainBranch.DirectUplineAgent.Rank = UplineAgent.FirstOrDefault().agent_rank;
                            MainBranch.DirectUplineAgent.CurrentPosition = UplineAgent.FirstOrDefault().agent_pos;

                        }
                        else if (UplineAgent.Count > 1) {
                            //Sort the List according to length of position. DESC -> direct upline is first or default
                            var sortedUplineAgentAA = UplineAgent.OrderByDescending(x => x.agent_pos.ToString().Length).ThenByDescending(x => Convert.ToInt32(x.agent_pos.ToString().Length)).ToList();

                            MainBranch.DirectUplineAgent.Name = sortedUplineAgentAA.FirstOrDefault().name;
                            MainBranch.DirectUplineAgent.Rank = sortedUplineAgentAA.FirstOrDefault().agent_rank;
                            MainBranch.DirectUplineAgent.CurrentPosition = sortedUplineAgentAA.FirstOrDefault().agent_pos;

                            //Main is indirect upline
                            if (sortedUplineAgentAA.Count == 3)
                            {
                                for (int i = 0; i < 2; i++)
                                {
                                    MainBranch.IndirectUplineAgent[i].Name = sortedUplineAgentAA[i + 1].name;
                                    MainBranch.IndirectUplineAgent[i].Rank = sortedUplineAgentAA[i + 1].agent_rank;
                                    MainBranch.IndirectUplineAgent[i].CurrentPosition = sortedUplineAgentAA[i + 1].agent_pos;
                                }
                            }
                            else {
                                MainBranch.IndirectUplineAgent[1].Name = sortedUplineAgentAA[1].name;
                                MainBranch.IndirectUplineAgent[1].Rank = sortedUplineAgentAA[1].agent_rank;
                                MainBranch.IndirectUplineAgent[1].CurrentPosition = sortedUplineAgentAA[1].agent_pos;
                            }
                            
                        }
                        else {
                            //When no upline
                            MainBranch.CurrentName = CurrentAgent.AgentName;
                            MainBranch.CurrentRank = CurrentAgent.AgentRank;
                            MainBranch.CurrentPosition = CurrentAgent.AgentPosition;
                        }

                        if (DirectDownlineAgents.Count > 0)
                        {
                            //First downline must be direct upline
                            DirectDownlineAgents.ForEach(x =>
                            {
                                DirectDownlineTreeBranch SingleDirect = new DirectDownlineTreeBranch();
                                SingleDirect.Name = x.name;
                                SingleDirect.Rank = x.agent_rank;
                                SingleDirect.CurrentPosition = x.agent_pos;
                                SingleDirect.IndirectDownlineAgent = new List<IndirectDownlineTreeBranch>();
                                if (IndirectDownlineAgents.Count > 0)
                                {
                                    IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.agent_pos.ToString())).ToList().ForEach(z =>
                                    {
                                        IndirectDownlineTreeBranch SingleIndirect = new IndirectDownlineTreeBranch
                                        {
                                            Name = z.name,
                                            Rank = z.agent_rank,
                                            CurrentPosition = z.agent_pos
                                        };
                                        SingleDirect.IndirectDownlineAgent.Add(SingleIndirect);
                                    });
                                }
                                else
                                {
                                    IndirectDownlineTreeBranch SingleIndirect = new IndirectDownlineTreeBranch
                                    {
                                        Name = "",
                                        Rank = "",
                                        CurrentPosition = ""
                                    };
                                    //SingleDirect.IndirectDownlineAgent.Add(SingleIndirect);
                                    SingleDirect.IndirectDownlineAgent.Add(SingleIndirect);
                                }

                                MainBranch.DirectDownlineAgent.Add(SingleDirect);
                            });
                        }
                        else {
                            DirectDownlineTreeBranch SingleDirect = new DirectDownlineTreeBranch();
                            SingleDirect.Name = "";
                            SingleDirect.Rank = "";
                            SingleDirect.CurrentPosition = "";
                            SingleDirect.IndirectDownlineAgent = new List<IndirectDownlineTreeBranch>();

                            IndirectDownlineTreeBranch SingleIndirect = new IndirectDownlineTreeBranch
                            {
                                Name = "",
                                Rank = "",
                                CurrentPosition = ""
                            };
                            SingleDirect.IndirectDownlineAgent.Add(SingleIndirect);
                            MainBranch.DirectDownlineAgent.Add(SingleDirect);
                        }
                        return MainBranch;
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine("Agent Tree Hierarchy Population: " + ex.Message);
                    return false;
                }
            }
            return null;
        }

        public class MainTreeBranch {
            public string CurrentName;
            public string CurrentRank;
            public string CurrentPosition;
            public List<DirectDownlineTreeBranch> DirectDownlineAgent;
            public List<IndirectDownlineTreeBranch> IndirectUplineAgent;
            public IndirectDownlineTreeBranch DirectUplineAgent;
        }

        public class DirectDownlineTreeBranch
        {
            public string Name;
            public string Rank;
            public string CurrentPosition;
            public List<IndirectDownlineTreeBranch> IndirectDownlineAgent;
        }

        public class IndirectDownlineTreeBranch
        {
            public string Name;
            public string Rank;
            public string CurrentPosition;
        }

        public class MainAgent {
            public string AgentName;
            public string AgentCode;
            public string AgentPosition;
            public string AgentRank;
            public string AgentGroup;
        }
    }    
}