using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_statements")]
    public class UserStatement
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("user_statement_category_id"), Display("UserStatementCategoryId")]
        public Int32 UserStatementCategoryId { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("url"), Display("Url")]
        public String Url { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("statement_date"), Display("StatementDate")]
        public DateTime? StatementDate { get; set; }
        public UserStatementCategory UserStatementCategoryIdUserStatementCategory { get; set; }
        public User UserIdUser { get; set; }
    }
}
