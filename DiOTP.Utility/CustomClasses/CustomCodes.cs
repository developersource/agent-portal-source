﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public static class CustomCodes
    {
        public enum DepartmentCodes
        {
            Operation = 1,
            Finance = 2,
            SalesChannel = 3,
            Compliance = 4
        }

        public enum StaffFunctionCodes
        {
            Maker = 1,
            Checker = 2,
            ViewReport = 3,
            GenerateReport = 4
        }

        public class Department_Code : Enumeration
        {
            public static readonly Department_Code Operation = new Department_Code("1", "Operation");
            public static readonly Department_Code Finance = new Department_Code("2", "Finance");
            public static readonly Department_Code SalesChannel = new Department_Code("3", "Sales Channel");
            public static readonly Department_Code Compliance = new Department_Code("4", "Compliance");

            public Department_Code(string id, string name)
                : base(id, name)
            {
            }

            public string GetDesc(int id)
            {
                return "";
            }
        }
    }
}
