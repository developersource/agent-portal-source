﻿using DiOTP.OracleData.ORACLEDBRef;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class GenericRepoChild
    {
        
        public static Response GetSingle(Type type, Int32 Id)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            dynamic obj = Activator.CreateInstance(type);
            string databaseTableName = Converter.GetDatabaseName(type);
            try
            {
                string query = "select * from " + databaseTableName + " where ID=" + Id;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                obj = dt.ToDynamicObject(type).FirstOrDefault();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response GetData(Type type, int skip, int take, bool isOrderByDesc)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            dynamic objs;
            string databaseTableName = Converter.GetDatabaseName(type);
            try
            {
                string query = "select * from " + databaseTableName + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject(type);
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response GetDataByProperty(Type type, string propName, string value, int skip, int take, bool isOrderByDesc)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            dynamic objs;
            string databaseTableName = Converter.GetDatabaseName(type);

            string columnName = Converter.GetColumnNameByPropertyName(type, propName);
            try
            {
                string query = "select * from " + databaseTableName + " where " + columnName + "='" + value + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject(type);
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }
    }
}
