﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;
using static DiOTP.WebApp.AccountOpening;

namespace DiOTP.WebApp
{
    public partial class UploadDocuments : System.Web.UI.Page
    {
        private static string keyOrg = "";

        private static readonly Lazy<IAccountOpeningService> lazyObjAO = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());
        public static IAccountOpeningService IAccountOpeningService { get { return lazyObjAO.Value; } }

        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IAccountOpeningFileService> lazyObjF = new Lazy<IAccountOpeningFileService>(() => new AccountOpeningFileService());
        public static IAccountOpeningFileService IAccountOpeningFileService { get { return lazyObjF.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uploadDocs.Visible = false;
                truliooDoc.Visible = false;
                string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/");
                string key = Request.QueryString["key"];
                keyOrg = key;
                if (!string.IsNullOrEmpty(key))
                {
                    key = key.Replace(" ", "+");
                    key = CustomEncryptorDecryptor.DecryptText(key);
                    string date = key.Split('_')[1];
                    DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);

                    string from = key.Split('_')[2];
                    if (from == "user")
                    {
                        truliooDoc.Visible = true;
                        uploadDocs.Visible = false;

                        string id = key.Split('_')[0];
                        Response responseAO = IAccountOpeningService.GetSingle(Convert.ToInt32(id));
                        if (responseAO.IsSuccess)
                        {
                            DiOTP.Utility.AccountOpening accountOpening = (DiOTP.Utility.AccountOpening)responseAO.Data;
                            if (accountOpening.IsIdentityVerified == 0)
                            {
                                string[] names = accountOpening.Name.Split(' ');
                                String[] dob = accountOpening.Dob.Split('/');
                                Int32 DayOfBirth = Convert.ToInt32(dob[0]);
                                Int32 MonthOfBirth = Convert.ToInt32(dob[1]);
                                Int32 YearOfBirth = Convert.ToInt32(dob[2]);
                                Trulioo.Client.V1.Model.PersonInfo personInfo = new Trulioo.Client.V1.Model.PersonInfo
                                {
                                    FirstGivenName = names[0],
                                    FirstSurName = names.Length > 1 ? names[1] : names[0],
                                    DayOfBirth = DayOfBirth,
                                    MonthOfBirth = MonthOfBirth,
                                    YearOfBirth = YearOfBirth
                                };
                                List<Trulioo.Client.V1.Model.NationalId> NationalIds = new List<Trulioo.Client.V1.Model.NationalId> { new Trulioo.Client.V1.Model.NationalId { Number = accountOpening.IdNo, Type = "nationalid" } };


                                Response resVerify = VerifyeKYC(personInfo, new Trulioo.Client.V1.Model.Location { }, NationalIds, new Trulioo.Client.V1.Model.Document { });
                                if (resVerify.IsSuccess)
                                {
                                    accountOpening.IsIdentityVerified = 1;
                                    accountOpening.IdentityStatus = "success";
                                    IAccountOpeningService.UpdateData(accountOpening);
                                }
                                else
                                {
                                    accountOpening.IsIdentityVerified = 1;
                                    accountOpening.IdentityStatus = resVerify.Message;
                                    IAccountOpeningService.UpdateData(accountOpening);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Identity Verification failed.', '');", true);
                                }
                            }
                            else if (accountOpening.IsIdentityVerified == 1 && accountOpening.IsDocumentsVerified == 0)
                            {
                                //Identity Already Verified
                                if (accountOpening.IdentityStatus != "success")
                                {
                                    uploadDocs.Visible = false;
                                    truliooDoc.Visible = false;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Identity Status: " + accountOpening.IdentityStatus + ". You cannot proceed.', '/Index.aspx');", true);
                                }
                            }
                            else if (accountOpening.IsDocumentsVerified == 1)
                            {
                                uploadDocs.Visible = false;
                                truliooDoc.Visible = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Document Status: " + accountOpening.DocumentsStatus + ". Already verified.', '/Index.aspx');", true);
                            }
                        }
                    }
                    else if (from == "admin")
                    {
                        truliooDoc.Visible = false;
                        uploadDocs.Visible = true;
                    }
                    //if ((DateTime.Now - dateTime).TotalMinutes > Convert.ToInt32(EmailValidityTimeInMinutes))
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Link expired!', 'Index.aspx');", true);
                    //}
                    //else
                    {
                        string id = key.Split('_')[0];
                    }
                }
                else if (Session["AccId"] == null)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Session Expired. Please click the link from Email again.', '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetTransactionResponse(String DocumentType, String DocumentFrontImage, String DocumentBackImage, String LivePhoto, String FirstName, String LastName, String FullName, String CountryCode)
        {
            Response response = new Response { IsSuccess = true };
            try
            {
                string key = keyOrg;
                if (!string.IsNullOrEmpty(key))
                {
                    key = key.Replace(" ", "+");
                    key = CustomEncryptorDecryptor.DecryptText(key);
                    string date = key.Split('_')[1];
                    string from = key.Split('_')[2];
                    DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                    //if ((DateTime.Now - dateTime).TotalMinutes > Convert.ToInt32(EmailValidityTimeInMinutes))
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Link expired!', 'Index.aspx');", true);
                    //}
                    //else
                    {
                        string id = key.Split('_')[0];
                        AccountOpeningFile accountOpeningFile = new AccountOpeningFile { AccountOpeningId = Convert.ToInt32(id) };

                        //Response resTransRec = DigitalizationServiceManager.GetTransactionRecord(ExperienceTransactionId, TransactionRecordId);
                        //if (resTransRec.IsSuccess)
                        //{
                        //    Trulioo.Client.V1.Model.VerifyResult verifyResult = (Trulioo.Client.V1.Model.VerifyResult)resTransRec.Data;
                        //    response.Data = verifyResult;
                        //}
                        //else
                        //{
                        //    return resTransRec;
                        //}
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object UpdateStatus(String ExperienceTransactionId, String TransactionRecordId)
        {
            Response response = new Response { IsSuccess = true };
            try
            {
                string key = keyOrg;
                if (!string.IsNullOrEmpty(key))
                {
                    key = key.Replace(" ", "+");
                    key = CustomEncryptorDecryptor.DecryptText(key);
                    string date = key.Split('_')[1];
                    string from = key.Split('_')[2];
                    DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                    string id = key.Split('_')[0];
                    if (from == "user")
                    {
                        Response responseAO = IAccountOpeningService.GetSingle(Convert.ToInt32(id));
                        if (responseAO.IsSuccess)
                        {
                            DiOTP.Utility.AccountOpening accountOpening = (DiOTP.Utility.AccountOpening)responseAO.Data;
                            accountOpening.ProcessStatus = 1;
                            accountOpening.IsDocumentsVerified = 1;
                            accountOpening.DocumentsStatus = "success";
                            IAccountOpeningService.UpdateData(accountOpening);
                            SendEmail(accountOpening.Email, "", "", 2, accountOpening.MobileNo, accountOpening.IdNo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object SendEmail(string address, string title, string content, int type, string mobileNo, string idno)
        {
            User sessionUser = new Utility.User();
            Response response = new Response();
            DiOTP.Utility.AccountOpening accountOpeningExisting = new DiOTP.Utility.AccountOpening();
            try
            {
                String query = "email_id = '" + address + "'";
                Response responseUserExists = IUserService.GetDataByFilter(query, 0, 0, false);
                if (responseUserExists.IsSuccess)
                {
                    List<User> Users = (List<User>)responseUserExists.Data;
                    if (Users.Count > 0)
                    {
                        response.Message = "This email has been registered";
                        response.IsSuccess = false;
                    }
                    else
                    {
                        String content2 = "";
                        if (Utility.Helper.CustomValidator.IsValidEmail(address))
                        {
                            Email email = new Email();
                            email.user = sessionUser;

                            if (type == 1)
                            {

                                mobileNo = "0" + mobileNo;
                                Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                                if (responseUList.IsSuccess)
                                {
                                    List<DiOTP.Utility.AccountOpening> accountOpenings = (List<DiOTP.Utility.AccountOpening>)responseUList.Data;
                                    if (accountOpenings.Count == 1)
                                    {
                                        accountOpeningExisting = accountOpenings.FirstOrDefault();
                                        content2 = CustomGenerator.GenerateSixDigitPin();
                                        accountOpeningExisting.EmailVerificationCode = Convert.ToInt32(content2);
                                        accountOpeningExisting.IsEmailRequested = 1;
                                        response = IAccountOpeningService.UpdateData(accountOpeningExisting);
                                        if (response.IsSuccess)
                                        {

                                            //accountOpening = (DiOTP.Utility.AccountOpening)responsePost.Data;

                                            //response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                                        }
                                        else
                                        {

                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                                email.user.Username = "Valued Customer";



                            }
                            else if (type == 2)
                            {
                                Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
                                if (responseUList.IsSuccess)
                                {
                                    List<DiOTP.Utility.AccountOpening> accountOpenings = (List<DiOTP.Utility.AccountOpening>)responseUList.Data;
                                    if (accountOpenings.Count == 1)
                                    {
                                        accountOpeningExisting = accountOpenings.FirstOrDefault();
                                        content2 = CustomGenerator.GenerateSixDigitPin();
                                        email.user.Username = accountOpeningExisting.Name;


                                        if (response.IsSuccess)
                                        {

                                            //accountOpening = (DiOTP.Utility.AccountOpening)responsePost.Data;

                                            //response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                                        }
                                        else
                                        {

                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            { }



                            email.user.EmailId = address;
                            bool send = EmailService.SendAccountOpeningMail(email, title, content, DateTime.Now.ToString(), content2, type, 0);
                            if (send == true)
                            {
                                response.Message = "sent" + "," + (Convert.ToInt32("60"));
                            }
                            else
                            { }
                        }
                        else
                        {
                            response.Message = "Invalid Email";
                        }
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = responseUserExists.Message;
                    return response;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Account Opening Page SendEmail: " + ex.Message);
            }
            return response;
        }

        public static Response VerifyeKYC(Trulioo.Client.V1.Model.PersonInfo personInfo, Trulioo.Client.V1.Model.Location location, List<Trulioo.Client.V1.Model.NationalId> NationalIds, Trulioo.Client.V1.Model.Document document)
        {
            //personInfo.FirstGivenName = "Leo";
            //personInfo.FirstSurName = "Moggie";
            //personInfo.DayOfBirth = 1;
            //personInfo.MonthOfBirth = 10;
            //personInfo.YearOfBirth = 1941;
            NationalIds = new List<Trulioo.Client.V1.Model.NationalId> { new Trulioo.Client.V1.Model.NationalId { Number = "411001-13-5027", Type = "nationalid" } };
            Response response = new Response() { IsSuccess = true };
            try
            {

                Response resAuth = DigitalizationServiceManager.TestAuth();
                if (resAuth.IsSuccess)
                {
                    //Response resEntities = DigitalizationServiceManager.GetTestEntities();
                    //if (resEntities.IsSuccess)
                    {
                        Response resConsents = DigitalizationServiceManager.GetConsents();
                        if (resConsents.IsSuccess)
                        {
                            List<string> Consents = (List<string>)resConsents.Data;
                            Response resDocumentTypes = DigitalizationServiceManager.GetDocumentTypes();
                            if (resDocumentTypes.IsSuccess)
                            {
                                DocumentType documentType = (DocumentType)resDocumentTypes.Data;
                                Trulioo.Client.V1.Model.VerifyRequest verifyRequestBody = new Trulioo.Client.V1.Model.VerifyRequest
                                {
                                    AcceptTruliooTermsAndConditions = true,
                                    CleansedAddress = true,
                                    ConfigurationName = "Identity Verification",
                                    ConsentForDataSources = Consents.ToArray(),
                                    CountryCode = "MY",
                                    DataFields = new Trulioo.Client.V1.Model.DataFields
                                    {
                                        PersonInfo = personInfo,
                                        Location = location,
                                        Document = document,
                                        NationalIds = NationalIds.ToArray()
                                    }
                                };
                                Response resVerify = DigitalizationServiceManager.Verify(verifyRequestBody);
                                if (!resVerify.IsSuccess)
                                    return resVerify;
                            }
                        }
                        else
                        {
                            return resConsents;
                        }
                    }
                    //else
                    {
                        //return resEntities;
                    }
                }
                else
                {
                    return resAuth;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Response response = new Response { IsSuccess = true };
            try
            {
                string key = keyOrg;
                if (!string.IsNullOrEmpty(key))
                {
                    key = key.Replace(" ", "+");
                    key = CustomEncryptorDecryptor.DecryptText(key);
                    string date = key.Split('_')[1];
                    string from = key.Split('_')[2];
                    DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                    string id = key.Split('_')[0];
                    if (from == "admin")
                    {
                        Response responseAO = IAccountOpeningService.GetSingle(Convert.ToInt32(id));
                        if (responseAO.IsSuccess)
                        {
                            DiOTP.Utility.AccountOpening accountOpening = (DiOTP.Utility.AccountOpening)responseAO.Data;
                            if (accountOpening.ProcessStatus == (Int32)AOProcessStatus.DocRequiredByAdmin)
                            {
                                if (additionalDocuments.HasFile)
                                {
                                    bool isAllFileTypesValid = true;
                                    if (additionalDocuments.PostedFiles.Count > 1)
                                    {
                                        additionalDocuments.PostedFiles.ToList().ForEach(x =>
                                        {
                                            if (Path.GetExtension(x.FileName).ToLower() == ".jpg" ||
                                                   Path.GetExtension(x.FileName).ToLower() == ".jpeg" ||
                                                   Path.GetExtension(x.FileName).ToLower() == ".png" ||
                                                   Path.GetExtension(x.FileName).ToLower() == ".pdf")
                                            {

                                            }
                                            else
                                            {
                                                isAllFileTypesValid = false;
                                            }
                                        });
                                        if (!isAllFileTypesValid)
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"File type is Invalid. Please make sure all file types are valid.\", '');", true);
                                        }
                                    }
                                    else if (additionalDocuments.PostedFiles.Count == 1)
                                    {
                                        if (Path.GetExtension(additionalDocuments.FileName).ToLower() == ".jpg" ||
                                                   Path.GetExtension(additionalDocuments.FileName).ToLower() == ".jpeg" ||
                                                   Path.GetExtension(additionalDocuments.FileName).ToLower() == ".png" ||
                                                   Path.GetExtension(additionalDocuments.FileName).ToLower() == ".pdf")
                                        {

                                        }
                                        else
                                        {
                                            isAllFileTypesValid = false;
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"File type is Invalid\", '');", true);
                                        }
                                    }
                                    if (isAllFileTypesValid)
                                    {
                                        List<FileInput> fileInputs = new List<FileInput>();
                                        additionalDocuments.PostedFiles.ToList().ForEach(x =>
                                        {
                                            if (Path.GetExtension(x.FileName).ToLower() == ".jpg" ||
                                                   Path.GetExtension(x.FileName).ToLower() == ".jpeg" ||
                                                   Path.GetExtension(x.FileName).ToLower() == ".png" ||
                                                   Path.GetExtension(x.FileName).ToLower() == ".pdf")
                                            {
                                                string path = "/Content/acc-opening-uploads/" + DateTime.Now.ToString("yyyyMMdd") + "/";
                                                if (!Directory.Exists(Server.MapPath(path)))
                                                    Directory.CreateDirectory(Server.MapPath(path));
                                                int fileCount = Directory.GetFiles(Server.MapPath(path)).Count();
                                                fileCount++;

                                                var uploadAdditionalFileName = x.FileName;
                                                var last = uploadAdditionalFileName.LastIndexOf('.');
                                                var butLast = uploadAdditionalFileName.Substring(0, last).Replace(".", "");
                                                uploadAdditionalFileName = butLast + uploadAdditionalFileName.Substring(last);
                                                string uploadAdditionalFileNameNew = uploadAdditionalFileName.Split('.')[0] + fileCount + "." + uploadAdditionalFileName.Split('.')[1];

                                                x.SaveAs(Server.MapPath(path) + uploadAdditionalFileNameNew);
                                                string filePath = path + uploadAdditionalFileNameNew;
                                                AccountOpeningFile accountOpeningFile = new AccountOpeningFile
                                                {
                                                    AccountOpeningId = accountOpening.Id,
                                                    FileType = 7,
                                                    Name = uploadAdditionalFileNameNew,
                                                    Url = filePath,
                                                    CreatedDate = DateTime.Now,
                                                    Status = 1
                                                };
                                                IAccountOpeningFileService.PostData(accountOpeningFile);
                                            }
                                        });

                                        accountOpening.ProcessStatus = 21;
                                        IAccountOpeningService.UpdateData(accountOpening);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Uploaded Successfully. Thank you.\", '/Index.aspx');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Please upload file.\", '');", true);
                                }
                            }
                            else if (accountOpening.ProcessStatus == (Int32)AOProcessStatus.DocRequiredUploaded)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Sorry! You have uploaded already.\", '/Index.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Sorry! You are not authorized to upload now.\", '/Index.aspx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseAO.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Not Authorised\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }



    }
}