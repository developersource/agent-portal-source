﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.Helper;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Admin
{
    public partial class DistributionInstruction : System.Web.UI.Page
    {
        private static readonly Lazy<IDvDistributionIntructionService> lazyDvDistributionIntructionServiceObj = new Lazy<IDvDistributionIntructionService>(() => new DvDistributionIntructionService());
        public static IDvDistributionIntructionService IDvDistributionIntructionService { get { return lazyDvDistributionIntructionServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append("1=1");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameUsername = Converter.GetColumnNameByPropertyName<DvDistributionIntruction>(nameof(DvDistributionIntruction.UserId));
                    filter.Append(" and " + columnNameUsername + " like '%" + Search.Value.Trim() + "%'");
                }

                if (IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "3")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and distribution_intruction in ('" + FilterValue.Value + "')");

                }
                else
                {
                    filter.Append(" and distribution_intruction in ('1', '2')");
                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IDvDistributionIntructionService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseDvDistributionIntructionList = IDvDistributionIntructionService.GetDataByFilter(filter.ToString(), skip, take, true);
                if (responseDvDistributionIntructionList.IsSuccess)
                {
                    List<DvDistributionIntruction> dvDistributionIntructions = ((List<DvDistributionIntruction>)responseDvDistributionIntructionList.Data).ToList();
                    if (dvDistributionIntructions.Count > 0)
                    {
                        StringBuilder asb = new StringBuilder();
                        int index = 1;
                        foreach (DvDistributionIntruction dvDistributionIntruction in dvDistributionIntructions)
                        {
                            Response responseUserAccountList = IUserAccountService.GetSingle(dvDistributionIntruction.UserAccountId);
                            if (responseUserAccountList.IsSuccess)
                            {
                                UserAccount ua = (UserAccount)responseUserAccountList.Data;
                                Response responseUserList = IUserService.GetSingle(dvDistributionIntruction.UserId);
                                if (responseUserList.IsSuccess)
                                {
                                    User u = (User)responseUserList.Data;
                                    Response responseUtmcFundInformationList = IUtmcFundInformationService.GetSingle(dvDistributionIntruction.FundId);
                                    if (responseUtmcFundInformationList.IsSuccess)
                                    {
                                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseUtmcFundInformationList.Data;
                                        asb.Append(@"<tr>
                                                    <td>" + index + @"</td>
                                                    <td>" + dvDistributionIntruction.UserId + " (" + u.Username + ")" + @"</a>
                                                    <td>" + ua.AccountNo + @"</td>
                                                    <td>" + utmcFundInformation.FundName.Capitalize() + @"</td>
                                                    <td>" + (dvDistributionIntruction.DistributionIntruction == 1 ? "Reinvest" : "Payout") + @"</td>
                                                    <td>" + dvDistributionIntruction.UpdatedDate.ToString("dd/MM/yyyy") + @"</td>
                                                  </tr>"
                                        );
                                        index++;
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                    "alert('" + responseUtmcFundInformationList.Message + "');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                "alert('" + responseUserList.Message + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                            "alert('" + responseUserAccountList.Message + "');", true);
                            }
                        }
                        TbodyDistributionInstruction.InnerHtml = asb.ToString();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseDvDistributionIntructionList.Message + "');", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "Distribution_Instructions_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Distribution Instructions");

                var headerRow = new List<string[]>()
            {
                new string[] { "Index", "Username", "MA No", "Fund Name", "Instruction" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Distribution Instructions"];

                string docDetails = "Distribution Instructions";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    if (filter == "3" || filter == null)
                    {
                        filterQuery.Append(" distribution_intruction in ('1', '2') ");
                    }
                    else if (filter == "1")
                    {
                        filterQuery.Append(" distribution_intruction = '1' ");
                    }
                    else if (filter == "2")
                    {
                        filterQuery.Append(" distribution_intruction = '2' ");

                    }
                    Response responseDvDistributionIntructionList = IDvDistributionIntructionService.GetDataByFilter(filterQuery.ToString(), 0, 0, true);
                    if (responseDvDistributionIntructionList.IsSuccess)
                    {
                        List<DvDistributionIntruction> dvDistributionIntructions = ((List<DvDistributionIntruction>)responseDvDistributionIntructionList.Data).ToList();
                        if (dvDistributionIntructions.Count > 0)
                        {
                            StringBuilder asb = new StringBuilder();
                            int index = 1;
                            foreach (DvDistributionIntruction dvDistributionIntruction in dvDistributionIntructions)
                            {
                                Response responseUserAccountList = IUserAccountService.GetSingle(dvDistributionIntruction.UserAccountId);
                                if (responseUserAccountList.IsSuccess)
                                {
                                    UserAccount ua = (UserAccount)responseUserAccountList.Data;
                                    Response responseUserList = IUserService.GetSingle(dvDistributionIntruction.UserId);
                                    if (responseUserList.IsSuccess)
                                    {
                                        User u = (User)responseUserList.Data;
                                        Response responseUtmcFundInformationList = IUtmcFundInformationService.GetSingle(dvDistributionIntruction.FundId);
                                        if (responseUtmcFundInformationList.IsSuccess)
                                        {
                                            UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseUtmcFundInformationList.Data;

                                            worksheet.Cells[row, 1].Value = index;
                                            worksheet.Cells[row, 2].Value = u.Username;
                                            worksheet.Cells[row, 3].Value = ua.AccountNo;
                                            worksheet.Cells[row, 4].Value = utmcFundInformation.FundName.Capitalize();
                                            if (dvDistributionIntruction.DistributionIntruction == 1)
                                            {
                                                worksheet.Cells[row, 5].Value = "Reinvest";
                                                worksheet.Cells[row, 5].Style.Font.Color.SetColor(System.Drawing.Color.White);
                                                worksheet.Cells[row, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                worksheet.Cells[row, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                                            }
                                            else if (dvDistributionIntruction.DistributionIntruction == 2)
                                            {
                                                worksheet.Cells[row, 5].Value = "Payout";
                                                worksheet.Cells[row, 5].Style.Font.Color.SetColor(System.Drawing.Color.White);
                                                worksheet.Cells[row, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                worksheet.Cells[row, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                                            }
                                            row++;
                                            index++;
                                        }
                                        else
                                        {
                                            response.IsSuccess = false;
                                            response.Message = responseUtmcFundInformationList.Message;
                                        }
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        response.Message = responseUserList.Message;
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = responseUserAccountList.Message;
                                }
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "No data";
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseDvDistributionIntructionList.Message;
                    }
                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;

                    // Save this data as a file
                    excel.SaveAs(excelFile);

                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }
    }
}