﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("account_openings")]
    public class AccountOpening
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("account_type"), Display("AccountType")]
        public Int32 AccountType { get; set; }
        [Database("principle_id"), Display("PrincipleId")]
        public String PrincipleId { get; set; }
        [Database("principle_user_id"), Display("PrincipleUserId")]
        public Int32 PrincipleUserId { get; set; }
        [Database("joint_relationship"), Display("JointRelationship")]
        public String JointRelationship { get; set; }
        [Database("us_citizen"), Display("UsCitizen")]
        public Int32 UsCitizen { get; set; }
        [Database("id_no"), Display("IdNo")]
        public String IdNo { get; set; }
        [Database("mobile_no"), Display("MobileNo")]
        public String MobileNo { get; set; }
        [Database("is_mobile_verified"), Display("IsMobileVerified")]
        public Int32 IsMobileVerified { get; set; }
        [Database("is_otp_requested"), Display("IsOtpRequested")]
        public Int32 IsOtpRequested { get; set; }
        [Database("otp_attempts"), Display("OtpAttempts")]
        public Int32 OtpAttempts { get; set; }
        [Database("otp_code"), Display("OtpCode")]
        public Int32 OtpCode { get; set; }
        [Database("is_otp_expired"), Display("IsOtpExpired")]
        public Int32 IsOtpExpired { get; set; }
        [Database("email"), Display("Email")]
        public String Email { get; set; }
        [Database("password"), Display("Password")]
        public String Password { get; set; }
        [Database("email_verification_code"), Display("EmailVerificationCode")]
        public Int32 EmailVerificationCode { get; set; }
        [Database("is_email_requested"), Display("IsEmailRequested")]
        public Int32 IsEmailRequested { get; set; }
        [Database("is_email_verified"), Display("IsEmailVerified")]
        public Int32 IsEmailVerified { get; set; }
        [Database("resident_of_malaysia"), Display("ResidentOfMalaysia")]
        public Int32 ResidentOfMalaysia { get; set; }
        [Database("nationality"), Display("Nationality")]
        public String Nationality { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("dob"), Display("Dob")]
        public String Dob { get; set; }
        [Database("race"), Display("Race")]
        public String Race { get; set; }
        [Database("bumiputra_status"), Display("BumiputraStatus")]
        public String BumiputraStatus { get; set; }
        [Database("marital_status"), Display("MaritalStatus")]
        public String MaritalStatus { get; set; }
        [Database("gender"), Display("Gender")]
        public String Gender { get; set; }
        [Database("salutation"), Display("Salutation")]
        public String Salutation { get; set; }
        [Database("race_description"), Display("RaceDescription")]
        public String RaceDescription { get; set; }

        [Database("are_you_pep"), Display("AreYouPep")]
        public Int32 AreYouPep { get; set; }
        [Database("pep_status"), Display("PepStatus")]
        public String PepStatus { get; set; }
        [Database("f_pep_status"), Display("FPepStatus")]
        public String FPepStatus { get; set; }
        [Database("tax_resident_outside_malaysia"), Display("TaxResidentOutsideMalaysia")]
        public Int32 TaxResidentOutsideMalaysia { get; set; }
        [Database("tax_residency_status"), Display("TaxResidencyStatus")]
        public String TaxResidencyStatus { get; set; }

        [Database("tin"), Display("Tin")]
        public String Tin { get; set; }
        [Database("declaration"), Display("Declaration")]
        public Int32 Declaration { get; set; }
        [Database("started_date"), Display("StartedDate")]
        public DateTime StartedDate { get; set; }
        [Database("last_updated_date"), Display("LastUpdatedDate")]
        public DateTime? LastUpdatedDate { get; set; }
        [Database("submitted_date"), Display("SubmitedDate")]
        public DateTime? SubmittedDate { get; set; }
        [Database("is_additional_required"), Display("IsAdditionalRequired")]
        public Int32 IsAdditionalRequired { get; set; }
        [Database("is_additional_required_2"), Display("IsAdditionalRequired2")]
        public Int32 IsAdditionalRequired2 { get; set; }
        [Database("settlement_date"), Display("SettlementDate")]
        public DateTime? SettlementDate { get; set; }

        [Database("remarks"), Display("Remarks")]
        public String Remarks { get; set; }
        [Database("process_status"), Display("ProcessStatus")]
        public Int32 ProcessStatus { get; set; }

        [Database("is_identity_verified"), Display("IsIdentityVerified")]
        public Int32 IsIdentityVerified { get; set; }

        [Database("identity_status"), Display("IdentityStatus")]
        public String IdentityStatus { get; set; }
        [Database("is_documents_verified"), Display("IsDocumentsVerified")]
        public Int32 IsDocumentsVerified { get; set; }
        [Database("documents_status"), Display("DocumentsStatus")]
        public String DocumentsStatus { get; set; }
        [Database("is_additional_admin"), Display("IsAdditionalAdmin")]
        public Int32 IsAdditionalAdmin { get; set; }
        [Database("additional_desc"), Display("AdditionalDesc")]
        public String AdditionalDesc { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("last_btn_clicked"), Display("LastBtnClicked")]
        public String LastBtnClicked { get; set; }
        [Database("remaind_times"), Display("RemaindTimes")]
        public Int32 RemaindTimes { get; set; }
        [Database("register_ip_address"), Display("RegisterIPAddress")]
        public String RegisterIPAddress { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("email_code_sent_date"), Display("EmailCodeSentDate")]
        public DateTime? EmailCodeSentDate { get; set; }

        public List<AccountOpeningAddress> accountOpeningAddresses { get; set; }
        public AccountOpeningOccupation AccountOpeningOccupation { get; set; }
        public AccountOpeningFinancialProfile AccountOpeningFinancialProfile { get; set; }
        public AccountOpeningBankDetail AccountOpeningBankDetail { get; set; }
        public List<AccountOpeningCRSDetail> accountOpeningCRSDetails { get; set; }
        public List<AccountOpeningFile> accountOpeningFiles { get; set; }
    }
}
