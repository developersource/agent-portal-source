﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgentRequests.aspx.cs" Inherits="Admin.AgentRequests" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="agentRequestsForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully rejected" data-status="Rejected" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Reject" data-isconfirm="1" data-isreason="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Successfully approved" data-status="Approved" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Approve" data-isconfirm="0" data-isreason="0"><i class="fa fa-check"></i></button>
                                                <button data-message="Successfully recommended" data-status="Recommended" data-original-title="Recommend" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Recommend" data-isconfirm="0" data-isreason="1" data-reasonLabel="Recommendation"><i class="fa fa-thumbs-up"></i></button>
                                                <span class="common-divider pull-right"></span>
                                                <button data-message="Successfully reverted" data-status="Link" data-original-title="Revert" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="ResendLink" data-isconfirm="1" data-isreason="1" data-content="Revert to Agent"><i class="fa fa-undo"></i></button>
                                                <button data-original-title="Process Application" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Edit" data-url="AddFiMMDetails.aspx" data-title="Process Application"><i class="fa fa-credit-card"></i></button>
                                                <button id="download" data-message="Success" data-status="Active" data-original-title="Download as excel" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Download" data-isconfirm="0"><i class="fa fa-download"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can search here with Username, Email, and Mobile number</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can Process/Approve/Reject on Agent Applications</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                        <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="0">All</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="2">Pending</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="4">CUTE Passed</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="5">Approved</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="49">Rejected</button>
                                        <small>
                                            <br />
                                            <strong>Note:</strong>
                                            <span>Agent Status Filtering on Agent Applications</span>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable userAccounts" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th class="br-0" data-column-name="username">Agent details</th>
                                            <th class="bl-0"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="usersTbody" runat="server"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderByColumn" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderType" runat="server" ClientIDMode="Static" Value="0" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
