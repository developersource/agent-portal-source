﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="New-Fund-Notices.aspx.cs" Inherits="DiOTP.WebApp.New_Fund_Notices" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-lg">
                        <h5 class="fundName">IMPORTANT NOTICES</h5>
                        <div class="mb-20">
                            <p>Please read the following information before retrieving the electronic prospectus(es) and electronic application forms.</p>
                        </div>
                        <div class="text-center">
                            <div class="btn-group">
                                <asp:LinkButton ID="lnkAgree" runat="server" ClientIDMode="Static" CssClass="btn btn-primary" Text="Agree"></asp:LinkButton>
                                <asp:LinkButton ID="lnkDisagree" runat="server" ClientIDMode="Static" CssClass="btn btn-border ml-10" Text="Disagree"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
