﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Mobile_Register : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyUserSecurityObj = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyUserSecurityObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    btnRequestCode.Visible = false;
                    btnRequestCodeForAdditionalAcc.Visible = true;
                    User user = (User)Session["user"];
                    txtEmail.Text = user.EmailId;
                    txtEmail.ReadOnly = true;
                    txtICNo.Text = user.UserIdUserAccounts.FirstOrDefault().MaHolderRegIdMaHolderReg.IdNo;
                    txtICNo.ReadOnly = true;
                }
                else
                {
                    btnRequestCode.Visible = true;
                    btnRequestCodeForAdditionalAcc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnRequestCode_Click(object sender, EventArgs e)
        {
            String MANumber = txtMANumber.Text;
            String ICNo = txtICNo.Text;
            String Email = txtEmail.Text;
            if (MANumber == "" || ICNo == "" || Email == "")
            {
                divMessage.Attributes.Add("class", "text-danger");
                divMessage.InnerHtml = "<i class='fa fa-close icon'></i> Please enter required(*) fields.";
            }
            if (ICNo.Contains("-"))
            {
                ICNo = ICNo.Replace("-", "");
            }
            if (ICNo.Contains(" "))
            {
                ICNo = ICNo.Replace(" ", "");
            }
            string propertyName = nameof(MaHolderReg.HolderNo);
            MaHolderReg MaHolderRegData = new MaHolderReg();
            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(MANumber);
            if (responseMHR.IsSuccess)
            {
                MaHolderRegData = (MaHolderReg)responseMHR.Data;
                if (MaHolderRegData == null)
                {
                    divMessage.Attributes.Add("class", "text-danger");
                    divMessage.InnerHtml = "<i class='fa fa-close icon'></i> No MA Record found.";
                }
                else
                {
                    if (MaHolderRegData.IdNo.Replace("-", "").Replace(" ", "") == ICNo)
                    {

                        string toMobileNumber = MaHolderRegData.HandPhoneNo;
                        if (toMobileNumber == null || toMobileNumber == "")
                        {
                            divMessage.Attributes.Add("class", "text-danger");
                            divMessage.InnerHtml = "<i class='fa fa-close icon'></i> No Mobile Number for MA. Please contact our Back Office.";
                        }
                        else
                        {
                            if (toMobileNumber.Contains("-"))
                            {
                                toMobileNumber = toMobileNumber.Replace("-", "");
                            }
                            if (toMobileNumber.Contains(" "))
                            {
                                toMobileNumber = toMobileNumber.Replace(" ", "");
                            }

                            User user = new User
                            {
                                EmailId = Email,
                                Password = "",
                                MobileNumber = toMobileNumber,
                                CreatedDate = DateTime.Now,
                                IsActive = 0,
                                IsSecurityChecked = 0,
                                IsSatChecked = 0,
                                Status = 0,
                                RegisterIp = TrackIPAddress.GetUserPublicIP(hdnLocalIPAddress.Value) + ", " + hdnLocalIPAddress.Value,
                                ModifiedBy = 0,
                                VerificationCode = 0
                            };
                            user.UserIdUserAccounts = new List<UserAccount>();
                            user.UserIdUserAccounts.Add(new UserAccount
                            {
                                UserId = user.Id,
                                AccountNo = MANumber,
                                CreatedBy = 0,
                                CreatedDate = DateTime.Now,
                                Status = 1,
                                IsVerified = 0,
                                MaHolderRegId = MaHolderRegData.Id,
                                IsPrimary = 1,
                                ModifiedBy = 0,
                            });
                            Response response = IUserService.PostData(user);
                            User returnUser = (User)response.Data;

                            int length = toMobileNumber.Length;
                            string displayMoileNumber = new String('X', length - 4) + toMobileNumber.Substring(length - 4);
                            divMessage.Attributes.Add("class", "text-success");
                            divMessage.InnerHtml = "<i class='fa fa-check icon'></i> OTP sent to Mobile Number " + displayMoileNumber + ".<br/>Mobile Number Outdated! Please contact our Back Office.";

                        }

                    }
                }
            }
        }

        protected void btnRequestCodeForAdditionalAcc_Click(object sender, EventArgs e)
        {
            String MANumber = txtMANumber.Text;
            String ICNo = txtICNo.Text;
            String Email = txtEmail.Text;

            if (ICNo.Contains("-"))
            {
                ICNo = ICNo.Replace("-", "");
            }
            if (ICNo.Contains(" "))
            {
                ICNo = ICNo.Replace(" ", "");
            }

            string propertyName = nameof(MaHolderReg.HolderNo);

            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(MANumber);
            MaHolderReg MaHolderRegData = new MaHolderReg();
            if (responseMHR.IsSuccess)
            {
                MaHolderRegData = (MaHolderReg)responseMHR.Data;
            }
            if (MaHolderRegData == null)
            {
                divMessage.Attributes.Add("class", "text-danger");
                divMessage.InnerHtml = "<i class='fa fa-close icon'></i> No MA Record found.";
            }
            else
            {
                if (MaHolderRegData.IdNo.Replace("-", "").Replace(" ", "") == ICNo)
                {
                    string toMobileNumber = MaHolderRegData.HandPhoneNo;
                    if (toMobileNumber == null || toMobileNumber == "")
                    {
                        divMessage.Attributes.Add("class", "text-danger");
                        divMessage.InnerHtml = "<i class='fa fa-close icon'></i> No Mobile Number for MA. Please contact our Back Office.";
                    }
                    else
                    {
                        if (toMobileNumber.Contains("-"))
                        {
                            toMobileNumber = toMobileNumber.Replace("-", "");
                        }
                        if (toMobileNumber.Contains(" "))
                        {
                            toMobileNumber = toMobileNumber.Replace(" ", "");
                        }

                        User user = (User)Session["user"];

                        String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                        //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                        string result = "";
                        string title = "MA ACTIVATION";
                        result = SMSService.SendSMS(toMobileNumber, mobileVerificationPin, title);

                        UserAccount uA = new UserAccount
                        {
                            UserId = user.Id,
                            AccountNo = MANumber,
                            CreatedBy = 0,
                            CreatedDate = DateTime.Now,
                            Status = 1,
                            IsVerified = 0,
                            MaHolderRegId = MaHolderRegData.Id,
                            VerificationCode = Convert.ToInt32(mobileVerificationPin),
                            ModifiedBy = 0
                        };

                        IUserAccountService.PostData(uA);

                        int length = toMobileNumber.Length;
                        string displayMoileNumber = new String('X', length - 4) + toMobileNumber.Substring(length - 4);
                        divMessage.Attributes.Add("class", "text-success");
                        divMessage.InnerHtml = "<i class='fa fa-check icon'></i> OTP sent to Mobile Number " + displayMoileNumber + ".<br/>Mobile Number Outdated! Please contact our Back Office.";

                    }
                }
            }

        }
    }
}