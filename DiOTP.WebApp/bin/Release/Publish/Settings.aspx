﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" EnableViewState="true" CodeBehind="Settings.aspx.cs" Inherits="DiOTP.WebApp.Settings" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="stylesheet" />
    </asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

        <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
            <div class="settings-wrapper">

                <div class="pull-right">
                    <ol class="breadcrumb">
                        <li><a href="/Portfolio.aspx">eApexIs</a></li>
                        <li class="active">Account Settings</li>
                    </ol>
                </div>

                <div style="text-align: left;">
                    <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Account Settings</h3>
                </div>
                <div style="margin-bottom: 5px; margin-left: 5px;">
                    <img src="Content/MyImage/INFO.png" style="width: 15px; height: 15.25px; margin-right: 5px;" />
                    <span style="font-weight: 600; color: #A0A0A0; font-size: 13px">You are currently viewing account</span>
                </div>
                <div class="alert" id="divalert" runat="server" style="background-color: #dff0d8">
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-10">
                        <asp:DropDownList CssClass="form-control selectpicker-acc" ID="ddlUserAccountId" runat="server" OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                </div>

                <div class="row" id="settingsDiv">

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="maAddress" runat="server" clientidmode="Static">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Master Account Address</h5>
                                    <p>For correspondence purpose.</p>
                                    <a href="#" class="settings-bind" data-toggle="modal" data-target="#address">View</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="maProfile" runat="server">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Master Account Profile</h5>
                                    <p>For profile update.</p>
                                    <a href="#" class="settings-bind" data-toggle="modal" data-target="#profile">View</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="occupationInfo" runat="server">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Occupation Information</h5>
                                    <p>For viewing occupation information.</p>
                                    <a href="#" class="settings-bind" data-toggle="modal" data-target="#occupation">View</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Bank Details <span></span></h5>
                                    <p>For payment purpose.</p>
                                    <a href="#" class="settings-bind" data-toggle="modal" data-target="#bankDetails">Modify</a>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="alert" runat="server">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Watch List</h5>
                                    <p>For alert notifications.</p>

                                    <a href="#" class="settings-bind" data-toggle="modal" data-target="#alerts">Modify</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Hardcopy Setting</h5>
                                    <p>Activate / Deactivate</p>
                                    <a href="HardcopyRequest.aspx" class="settings-bind">Modify</a>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="satGroupScore" runat="server">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>SAT Group & Score</h5>
                                    <div class="satGroup">
                                        <p id="satScore" class="satScoreClass" runat="server"></p>
                                        <%--<p id="satStatus" class="satStatusClass" runat="server"></p>--%>
                                    </div>                                  
                                    <a id="satATag" runat="server" href="/SAT-Form.aspx?redirectUrl=Settings.aspx" class="settings-bind">View</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Order History</h5>
                                    <p>For transactions history.</p>
                                    <a href="OrderList.aspx" class="settings-bind">View</a>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="distribution" runat="server">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Distribution Instruction</h5>
                                    <p>For distribution instruction update.</p>
                                    <a href="#" class="settings-bind" data-toggle="modal" data-target="#distributionInstruction">View</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="bankDiv" runat="server">
                        <div class="settings-part">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <h5>Bank Details - <small id="bankDetailsBindStatus" runat="server"></small></h5>
                                    <p>Required for transaction activities.</p>
                                    <a href="#" id="abank" runat="server" class="settings-bind" data-toggle="modal" data-target="#bankDetails">View</a>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>


        </div>
        <asp:HiddenField ID="hdnSMSExpirationTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnSMSLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />

        <asp:HiddenField ID="HiddenField3" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hdnMobilePinRequestedTrans" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hdnMobilePinRequestedBank" runat="server" ClientIDMode="Static" Value="0" />


        <asp:HiddenField ID="SettingId" runat="server" ClientIDMode="Static" />
        <asp:Button ID="Delete_Setting" runat="server" ClientIDMode="Static" CssClass="hide" OnClick="Delete_Setting_Click" />
    </asp:Content>



    <asp:Content ID="Content4" runat="server" ContentPlaceHolderID="AccountModal">

        <div class="modal fade mobile-height" id="address">
            <div class="modal-dialog sett">
                <asp:Panel ID="PanelMAAdress" runat="server" DefaultButton="btnAddressSubmit">
                    <div class="modal-content">
                        <div class="modal-header sett-modalhead">
                            <h2 class="text-white mb-0">Address</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-information">
                                        <div class="col-md-3">
                                            <div class="profile-pic text-center">
                                                <img src="/Content/MyImage/loc.png" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row hide">
                                                <div class="col-md-12">
                                                    <h5>Master Account Number :
                                                <asp:Label ID="txtMANumber" runat="server"></asp:Label></h5>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Address Line 1</label><sup class="text-danger" id="addr1">*</sup>
                                                            <asp:TextBox ID="txtAddressLine1" runat="server" CssClass="form-control" placeholder="Address Line 1" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnAddressLine1" runat="server" ClientIDMode="Static" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Address Line 2</label><sup class="text-danger" id="addr2">*</sup>
                                                            <asp:TextBox ID="txtAddressLine2" runat="server" CssClass="form-control" placeholder="Address Line 2" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnAddressLine2" runat="server" ClientIDMode="Static" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Address Line 3</label>
                                                            <asp:TextBox ID="txtAddressLine3" runat="server" CssClass="form-control" placeholder="Address Line 3" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnAddressLine3" runat="server" ClientIDMode="Static" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Address Line 4</label>
                                                            <asp:TextBox ID="txtAddressLine4" runat="server" CssClass="form-control" placeholder="Address Line 4" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnAddressLine4" runat="server" ClientIDMode="Static" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Postcode</label><sup class="text-danger" id="pcode">*</sup>
                                                            <asp:TextBox ID="txtPostCode" runat="server" CssClass="form-control" placeholder="Postcode" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:HiddenField ID="hdnPostCode" runat="server" ClientIDMode="Static" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>State</label><sup class="text-danger" id="state">*</sup>
                                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hdnState" runat="server" ClientIDMode="Static" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Country</label>
                                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 divPassword">
                                                    <div class="profile-desk">
                                                        <div class="form-group mb-10">
                                                            <label>Your Password</label><sup class="text-danger" id="pword">*</sup>
                                                            <asp:TextBox ID="txtCurrentPasswordAddress" TextMode="Password" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter Password"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="fs-18 text-center">
                                Modification of  Country of Residence has been restricted,<br />
                                If your Country of Residence other than Malaysia,<br />
                                please <a href='https://eapexis.apexis.com.my/Contact.aspx' target='_blank'>Contact Us</a> or visit our office.
                            </div>
                            <br />
                            <div id="divOfficeAddress2">
                                <label id="approvalMaAddress" clientidmodel="static" runat="server" visible="false">Approval:</label>
                                <span id="addressnotverified" clientidmodel="static" runat="server" visible="false">
                                    <span class="text-warning"><i class="fa fa-exclamation-circle mr-6"></i>Address Approval is Pending</span><br />
                                </span>
                                <span id="addressverified" runat="server" clientidmodel="static" visible="false">
                                    <span class="text-success"><i class="fa fa-check mr-6"></i>Address is Approved</span><br />
                                </span>
                                <span id="addressdenied" runat="server" clientidmodel="static" visible="false">
                                    <span class="text-danger"><i class="fa fa-times mr-6"></i>Request for change of address is rejected by eApexIs Admin</span><br />
                                </span>
                                <br />
                                <table class="table table-condensed table-cell-pad-5 no-border">
                                    <tbody id="addressdetail" runat="server">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer" style="margin-bottom: 35px;">
                            <asp:Button ID="btnAddressSubmit" ClientIDMode="Static" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClientClick="return clientAddressValidation()" OnClick="btnAddressSubmit_Click" />
                            <button type="button" id="btnEdit" runat="server" clientidmode="Static" onclick="MaAddressEdit()" class="btn btn-sett-confirm">Edit</button>
                            <button type="button" class="btn btn-sett-cancel" id="btnAddressCancel" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>

        <div class="modal fade mobile-height" id="profile">
            <div class="modal-dialog modal-lg">

                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">Master Account Profile</h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-information">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="profile-pic text-center">
                                                <img src="Content/MyImage/9.png" alt="" height="200" />
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <h4 class="text-uppercase text-primary">Account Details</h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Name</div>
                                                        <asp:Label ID="txtSalutation" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        <asp:Label ID="txtName" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        <br>
                                                        <small id="type" runat="server"></small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Account Type</div>
                                                        <asp:Label ID="txtHolderAccType" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        <asp:Label ID="txtEpfType" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Master Account Number</div>
                                                        <asp:Label ID="txtMANumber2" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Registered Date</div>
                                                        <asp:Label ID="txtRegDate" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="display: none;">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Holder Class</div>
                                                        <asp:Label ID="txtHolderClass" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="profile-information">
                                                <h4 class="text-uppercase text-primary">Personal Details</h4>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div class="text-muted mb-5">NRIC</div>
                                                            <asp:Label ID="txtIdNo" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityOldIC" runat="server" class="text-muted mb-5">Old NRIC/Passport/Others</div>
                                                            <asp:Label ID="txtIdNoOld" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div id="telephoneNo" runat="server" class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div class="text-muted mb-5">Telephone No</div>
                                                            <asp:Label ID="txtTelNo" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityHandPhoneNo" runat="server" class="text-muted mb-5">Handphone No</div>
                                                            <asp:Label ID="txtHandphoneNo" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="IsNotPrinciple" runat="server">
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visivilityBirthDate" runat="server" class="text-muted mb-5">Birth Date</div>
                                                            <asp:Label ID="txtBirthDate" runat="server" CssClass="mb-10 d-ib"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityGender" runat="server" class="text-muted mb-5">Gender</div>
                                                            <asp:Label ID="txtSex" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityNationality" runat="server" class="text-muted mb-5">Nationality</div>
                                                            <asp:Label ID="txtNationality" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="Div1" runat="server" class="text-muted mb-5">Nature of Business</div>
                                                            <asp:Label ID="Label1" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div class="form-group mb-10">
                                                                <label>Occupation</label><sup class="text-danger">*</sup>
                                                                <asp:DropDownList ID="ddlOccupation" runat="server" CssClass="form-control" placeholder="Occupation" ClientIDMode="Static"></asp:DropDownList>
                                                                <asp:HiddenField ID="hdnOccupation" runat="server" ClientIDMode="Static" />
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityOccupation" runat="server" class="text-muted mb-5">Occupation</div>
                                                            <asp:Label ID="txtOccupation" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div class="form-group mb-10">
                                                                <label>Nature of Business</label><sup class="text-danger">*</sup>
                                                                <asp:DropDownList ID="ddlNatureOfBusiness" runat="server" CssClass="form-control" placeholder="Occupation" ClientIDMode="Static"></asp:DropDownList>
                                                                <asp:HiddenField ID="hdnNatureOfBusiness" runat="server" ClientIDMode="Static" />
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div class="form-group mb-10">
                                                                <label>Name of Employer / Name of Company for self employed</label><sup class="text-danger">*</sup>
                                                                <asp:TextBox ID="txtEmployerName" runat="server" CssClass="form-control" placeholder="Occupation" ClientIDMode="Static"></asp:TextBox>
                                                                <asp:HiddenField ID="hdnEmployerName" runat="server" ClientIDMode="Static" />
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityRace" runat="server" class="text-muted mb-5">Race</div>
                                                            <asp:Label ID="txtRace" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div class="form-group mb-10">
                                                                <label>Monthly Income</label><sup class="text-danger">*</sup>
                                                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" placeholder="Occupation" ClientIDMode="Static"></asp:DropDownList>
                                                                <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" />
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div id="monthlyIncome" runat="server" class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityIncome" runat="server" class="text-muted mb-5">Monthly Income</div>
                                                            <asp:Label ID="txtIncome" runat="server" CssClass="mb-10 d-ib text-capitalize" ClientIDMode="Static"></asp:Label>
                                                </div>
                                            </div>--%>

                                                    <%--<div class="col-md-4">
                                                        <div class="profile-desk">
                                                            <div id="visibilityNoOfDependants" runat="server" class="text-muted mb-5">No Of Dependants</div>
                                                            <asp:Label ID="txtNoOfDependants" runat="server" CssClass="mb-10 d-ib" ClientIDMode="Static"></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                            </div>
                                            <div id="corpContactPerson" runat="server" visible="false" class="profile-information">
                                                <h4 class="text-uppercase text-primary">Contact details</h4>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Contact Person</div>
                                                        <asp:Label ID="txtContactPerson" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-desk">
                                                        <div class="text-muted mb-5">Position held</div>
                                                        <asp:Label ID="positionHeld" runat="server" CssClass="mb-10 d-ib text-capitalize"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnSubmitEditProfile" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" ClientIDMode="Static" OnClick="btnSubmitEditProfile_Click" />
                        <button type="button" id="btnEditProfile" runat="server" clientidmode="Static" onclick="MaProfileEdit()" class="btn btn-sett-confirm hide">Edit</button>
                        <button type="button" id="btnProfileCancel" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade mobile-height" id="occupation" style="margin-bottom: 30px;">
            <div class="modal-dialog sett">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAddressSubmit">
                    <div class="modal-content">
                        <div class="modal-header sett-modalhead">
                            <h2 class="text-white mb-0">Occupation Information</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-information">

                                        <div class="col-md-12">
                                            <div class="row hide">
                                                <div class="col-md-12">
                                                    <h5>:
                                                    <asp:Label ID="Label1" runat="server"></asp:Label></h5>
                                                </div>
                                            </div>
                                            <style>
                                                input[type=checkbox] {
                                                    margin-right: 10px;
                                                }
                                            </style>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-3">
                                                    <label for="employedOption" id="lblEmployment" class="hide">Employment Status</label>
                                                </div>
                                                <div id="employedOption" class="col-md-9 checkbox-group-2 empChecks hide">
                                                    <div class="col-md-4">
                                                        <asp:CheckBox ID="Employed" runat="server" Text="Employed" ClientIDMode="Static" Style="" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:CheckBox ID="Unemployed" runat="server" Text="Unemployed" ClientIDMode="Static" Style="margin-right: 10px;" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row occupation-field">
                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="form-group row">
                                                        <div class="col-md-3">
                                                            <label for="ddlOccupation" id="lblOccupation">Occupation</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div id="lblViewOccupation" class="form-control" runat="server" clientidmode="static"></div>
                                                            <select class="form-control hide" id="ddlOccupation" runat="server" clientidmode="static" readonly="readonly">

                                                                <option value="Others">Others (Please Specify)</option>
                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 occupation-others hide" id="occupationOthers">
                                                        <br />
                                                        <div class="form-group row">
                                                            <div class="col-md-3">
                                                                <label for="txtOccupationDesc">Please Specify</label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" name="OccupationDesc" id="txtOccupationDesc" runat="server" class="form-control" placeholder="Enter Occupation" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="employed-fields">
                                                            <br />

                                                            <div class="col-md-3">
                                                                <label for="EmployerName">Name of Employer / Name of Company for self employed</label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div id="lblEmployerName" class="form-control" runat="server" clientidmode="static"></div>
                                                                <input type="text" name="EmployerName" id="txtEmployerName" runat="server" class="form-control hide" clientidmode="static" placeholder="Name of Employer/Name of Company for self employed" readonly="readonly" />

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="employed-fields">
                                                        <br />

                                                        <div class="form-group row">
                                                            <div class="">
                                                                <div class="">
                                                                    <div class="col-md-3">
                                                                        <label for="ddlNatureOfBusiness">Nature of Business</label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <div id="txtNatureOfBusiness" runat="server" class="form-control" clientidmode="static"></div>
                                                                        <select class="form-control hide" name="ddlNatureOfBusiness" id="ddlNatureOfBusiness" runat="server" clientidmode="static" disabled="disabled">
                                                                            <option value="">Select Nature of Business</option>
                                                                            <option value="Academician">Academician</option>
                                                                            <option value="Actuarial">Actuarial</option>
                                                                            <option value="Admin/Human Resources">Admin/Human Resources</option>
                                                                            <option value="Advertising">Advertising</option>
                                                                            <option value="Advisory Firm">Advisory Firm</option>
                                                                            <option value="Agriculture & Plantation">Agriculture & Plantation</option>
                                                                            <option value="Audit & Taxation">Audit & Taxation</option>
                                                                            <option value="Audit Firm">Audit Firm</option>
                                                                            <option value="Aviation">Aviation</option>
                                                                            <option value="Banking/Financial">Banking/Financial</option>
                                                                            <option value="Beauty Treatment Centres / Parlours">Beauty Treatment Centres / Parlours</option>
                                                                            <option value="Biotechnology">Biotechnology</option>
                                                                            <option value="Building and Construction">Building and Construction</option>
                                                                            <option value="Business in Low Density Goods (e.g: Moblie Phones/Clothing)">Business in Low Density Goods (e.g: Moblie Phones/Clothing)</option>
                                                                            <option value="Business Centres">Business Centres</option>
                                                                            <option value="Casino/Betting/Gambling Related">Casino/Betting/Gambling Related</option>
                                                                            <option value="Cable /DTH services">Cable /DTH services</option>
                                                                            <option value="Cash Intensive Business (e.g: Convenient Store)">Cash Intensive Business (e.g: Convenient Store)</option>
                                                                            <option value="Chemical Engineering">Chemical Engineering</option>
                                                                            <option value="Chemistry">Chemistry</option>
                                                                            <option value="Clerical/Administrative">Clerical/Administrative</option>
                                                                            <option value="Coaching classes/ Training Institutes">Coaching classes/ Training Institutes</option>
                                                                            <option value="Construction Agencies  / Contractors">Construction Agencies  / Contractors</option>
                                                                            <option value="Corporate Finance/Investment">Corporate Finance/Investment</option>
                                                                            <option value="Courier Services">Courier Services</option>
                                                                            <option value="Customer Service">Customer Service</option>
                                                                            <option value="Education">Education</option>
                                                                            <option value="Educational Institutions">Educational Institutions</option>
                                                                            <option value="Electricity Generation">Electricity Generation</option>
                                                                            <option value="Electronics Engineering">Electronics Engineering</option>
                                                                            <option value="Engineering">Engineering</option>
                                                                            <option value="Entertainment Outlets/Karaoke/Spa Massage/Internet Café">Entertainment Outlets/Karaoke/Spa Massage/Internet Café</option>
                                                                            <option value="Environmental Consultant">Environmental Consultant</option>
                                                                            <option value="Environmental Engineering">Environmental Engineering</option>
                                                                            <option value="Fashion Designing">Fashion Designing</option>
                                                                            <option value="Film / TV Production Agency">Film / TV Production Agency</option>
                                                                            <option value="Financial Consultant">Financial Consultant</option>
                                                                            <option value="Financial Institutions">Financial Institutions</option>
                                                                            <option value="Food Tech/Nutritionist">Food Tech/Nutritionist</option>
                                                                            <option value="Food/Beverage">Food/Beverage</option>
                                                                            <option value="Licensed Gaming Outlet">Licensed Gaming Outlet</option>
                                                                            <option value="General Work">General Work</option>
                                                                            <option value="General/Cost Accounting">General/Cost Accounting</option>
                                                                            <option value="Geology/Geophysics">Geology/Geophysics</option>
                                                                            <option value="Government Body">Government Body</option>
                                                                            <option value="Gymkhana">Gymkhana</option>
                                                                            <option value="Health Clinics / Fitness Centres">Health Clinics / Fitness Centres</option>
                                                                            <option value="Healthcare">Healthcare</option>
                                                                            <option value="Hospitals or Nursing  Homes">Hospitals or Nursing  Homes</option>
                                                                            <option value="Hotel/Tourism">Hotel/Tourism</option>
                                                                            <option value="Hotels / Boarding / Lodging">Hotels / Boarding / Lodging</option>
                                                                            <option value="House Keeping Services">House Keeping Services</option>
                                                                            <option value="Human Resources">Human Resources</option>
                                                                            <option value="Industrial Engineering">Industrial Engineering</option>
                                                                            <option value="Information Technology">Information Technology</option>
                                                                            <option value="Insurance Services">Insurance Services</option>
                                                                            <option value="Investment Banker">Investment Banker</option>
                                                                            <option value="Investment Company">Investment Company</option>
                                                                            <option value="Journalists/Editors">Journalists/Editors</option>
                                                                            <option value="Law/Legal Services">Law/Legal Services</option>
                                                                            <option value="Licensed Money Lending">Licensed Money Lending</option>
                                                                            <option value="Logistics/Supply Chains">Logistics/Supply Chains</option>
                                                                            <option value="Manpower providers / Labour Contractors">Manpower providers / Labour Contractors</option>
                                                                            <option value="Manufacturing">Manufacturing</option>
                                                                            <option value="Marine">Marine</option>
                                                                            <option value="Marketing Services / Agencies">Marketing Services / Agencies</option>
                                                                            <option value="Mechanical/Automotive Engineering">Mechanical/Automotive Engineering</option>
                                                                            <option value="Media and event management Companies">Media and event management Companies</option>
                                                                            <option value="Medical">Medical</option>
                                                                            <option value="Military Transactions">Military Transactions</option>
                                                                            <option value="Money Remittance">Money Remittance</option>
                                                                            <option value="Money Lender">Money Lender</option>
                                                                            <option value="Money Services Business / Money Changer">Money Services Business / Money Changer</option>
                                                                            <option value="Non-Government Organizarion (NGO) /Charitable body">Non-Government Organizarion (NGO) /Charitable body</option>
                                                                            <option value="Offshore Banking/Offshore Trust/Offshore Corporations">Offshore Banking/Offshore Trust/Offshore Corporations</option>
                                                                            <option value="Oil/Gas Engineering">Oil/Gas Engineering</option>
                                                                            <option value="Optician">Optician</option>
                                                                            <option value="Pawn Brokers/Stock Brokers">Pawn Brokers/Stock Brokers</option>
                                                                            <option value="Personal Care">Personal Care</option>
                                                                            <option value="Pest Control Services">Pest Control Services</option>
                                                                            <option value="Plantations">Plantations</option>
                                                                            <option value="Police Force">Police Force</option>
                                                                            <option value="Printing Press / Printing Agencies">Printing Press / Printing Agencies</option>
                                                                            <option value="Precious Stone/Metal Dealer (e.g. Gold/Jewelry)">Precious Stone/Metal Dealer (e.g. Gold/Jewelry)</option>
                                                                            <option value="Process Design & Control">Process Design & Control</option>
                                                                            <option value="Producer">Producer</option>
                                                                            <option value="Property/Real Estate">Property/Real Estate</option>
                                                                            <option value="Public relation">Public relation</option>
                                                                            <option value="Publisher">Publisher</option>
                                                                            <option value="Reseller(includes Wholesalers)">Reseller(includes Wholesalers)</option>
                                                                            <option value="Restaurants / Bar">Restaurants / Bar</option>
                                                                            <option value="Retailer">Retailer</option>
                                                                            <option value="Sales & Marketing">Sales & Marketing</option>
                                                                            <option value="Science & Technology">Science & Technology</option>
                                                                            <option value="Secretarial">Secretarial</option>
                                                                            <option value="Security and Detective Agencies">Security and Detective Agencies</option>
                                                                            <option value="Security/Armed Forces">Security/Armed Forces</option>
                                                                            <option value="Service Centres / Maintenance Agencies">Service Centres / Maintenance Agencies</option>
                                                                            <option value="Sports">Sports</option>
                                                                            <option value="Tech & Helpdesk Support">Tech & Helpdesk Support</option>
                                                                            <option value="Telecommunication Services">Telecommunication Services</option>
                                                                            <option value="Tour and Travel Services">Tour and Travel Services</option>
                                                                            <option value="Training and Placement Service Centre">Training and Placement Service Centre</option>
                                                                            <option value="Transport">Transport</option>
                                                                            <option value="Vehicle Rental Services">Vehicle Rental Services</option>
                                                                            <option value="Veterinarian">Veterinarian</option>
                                                                            <option value="Youth movement">Youth movement</option>
                                                                            <option value="Used automobile/truck/machines part dealer">Used automobile/truck/machines part dealer</option>
                                                                            <option value="Import/Export Companies">Import/Export Companies</option>
                                                                            <option value="Professional service providers acting as intermediaries (Lawyer, accountants)">Professional service providers acting as intermediaries (Lawyer, accountants)</option>
                                                                            <%--<option value="Audit/Accounting/Tax/Legal/Company Secretary">Audit/Accounting/Tax/Legal/Company Secretary</option>
                                                                <option value="Building/Construction Related">Building/Construction Related</option>
                                                                <option value="Business in Low Density Goods">Business in Low Density Goods (e.g: Moblie Phones/Clothing)</option>
                                                                <option value="Cash Intensive Business">Cash Intensive Business (e.g: Restaurant/Convenient Store)</option>
                                                                <option value="Casino/Betting/Gambling Related">Casino/Betting/Gambling Related</option>
                                                                <option value="Education">Education</option>
                                                                <option value="Engineering">Engineering</option>
                                                                <option value="Entertainment Outlets/Karaoke/Spa Massage/Internet Café">Entertainment Outlets/Karaoke/Spa Massage/Internet Café</option>
                                                                <option value="Farming/Fishing/Foresting">Farming/Fishing/Foresting</option>
                                                                <option value="Financial/Capital Market Institution/Intermediary">Financial/Capital Market Institution/Intermediary</option>
                                                                <option value="Government/Regulatory Authority">Government/Regulatory Authority</option>
                                                                <option value="Hotel/Travel Services">Hotel/Travel Services</option>
                                                                <option value="Medical/Health/Science">Medical/Health/Science</option>
                                                                <option value="Money Services Business">Money Services Business (e.g. Remittance Agent/Non-Bank Money Lender)</option>
                                                                <option value="Non-profit organization/Charity">Non-profit organization/Charity</option>
                                                                <option value="Offshore Banking/Offshore Trust">Offshore Banking/Offshore Trust</option>
                                                                <option value="Pawn Brokers/Stock Brokers">Pawn Brokers/Stock Brokers</option>
                                                                <option value="Precious Stone/Metal Dealer">Precious Stone/Metal Dealer (e.g. Gold/Jewelry)</option>
                                                                <option value="Real Estate/Property">Real Estate/Property</option>
                                                                <option value="Telecommunication">Telecommunication</option>
                                                                <option value="Import/Export Companies">Import/Export Companies</option>
                                                                <option value="Military Transactions">Military Transactions</option>
                                                                <option value="Used automobile/truck/machines part dealer">Used automobile/truck/machines part dealer</option>--%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="employed-fields">
                                                            <div class="form-group row">
                                                                <br />
                                                                <div class="">
                                                                    <div class="col-md-3">
                                                                        <label for="ddlMonthlyIncome">Monthly Income</label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <asp:Label ID="txtIncome" runat="server" CssClass="mb-10 form-control text-capitalize" ClientIDMode="Static"></asp:Label>
                                                                        <select class="form-control hide" id="ddlMonthlyIncome" runat="server" clientidmode="static" readonly="readonly">
                                                                            <option value="">Select Monthly Income</option>
                                                                            <option value="Below 500 MYR">Below 500 MYR</option>
                                                                            <option value="MYR 500 to MYR 1,000 MYR">500 MYR to 1,000 MYR</option>
                                                                            <option value="MYR 1,001 to MYR 2,000 MYR">1,001 MYR to 2,000 MYR</option>
                                                                            <option value="MYR 2,001 to MYR 5,000 MYR">2,001 MYR to 5,000 MYR</option>
                                                                            <option value="MYR 5,001 to MYR 10,000 MYR">5,001 MYR to 10,000 MYR</option>
                                                                            <option value="MYR 10,001 and above">10,001 MYR and above</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="employed-fields">
                                                        <h3 class="correspondenceAddress text-dark  font-weight-bold">Office Address</h3>
                                                        <div class="row">
                                                            <br />
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <%--<label for="txtOfficeAddress1">Addr1</label>--%>
                                                                    <input type="text" name="Addr1" id="txtOfficeAddress1" runat="server" class="form-control" placeholder="Address Line 1" clientidmode="static" readonly="readonly" />
                                                                    <input type="text" name="Addr2" id="txtOfficeAddress2" runat="server" class="form-control" placeholder="Address Line 2" clientidmode="static" readonly="readonly" />
                                                                    <input type="text" name="Addr3" id="txtOfficeAddress3" runat="server" class="form-control" placeholder="Address Line 3" clientidmode="static" readonly="readonly" />
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="txtOfficePostCode">Post Code</label>
                                                                    <input type="text" name="Post Code" id="txtOfficePostCode" runat="server" class="form-control" placeholder="Enter Post Code" clientidmode="static" readonly="readonly" />
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="txtCity">City</label>
                                                                    <input type="text" name="City" id="txtCity" runat="server" class="form-control" placeholder="Enter City" clientidmode="static" readonly="readonly" />
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="ddlOfficeState">State</label>
                                                                    <%--<select class="form-control" id="ddlState" runat="server" clientidmode="static">

                                                        <option value="Select">Select State</option>
                                                    </select>--%>
                                                                    <input type="text" id="ddlOfficeState" runat="server" name="state" class="form-control" placeholder="Enter State" clientidmode="static" readonly="readonly" />
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group row">
                                                                    <label for="ddlCountry">Country</label>
                                                                    <select class="form-control" id="txtOfficeCountry" runat="server" clientidmode="static" disabled="disabled">
                                                                        <option value="">Select Country</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="txtTelephone">Office Tel. No.</label>
                                                                    <div>
                                                                        <span class="position-absolute mobile-prefix" style="width: 75px;">
                                                                            <select class="form-control" id="officeFrontNumber" runat="server" clientidmode="static" disabled="disabled" style="padding: .375rem .75rem;">
                                                                                <option value="60">+60</option>
                                                                                <option value="1">+1</option>
                                                                                <option value="20">+20</option>
                                                                                <option value="211">+211</option>
                                                                                <option value="212">+212</option>
                                                                                <option value="213">+213</option>
                                                                                <option value="216">+216</option>
                                                                                <option value="218">+218</option>
                                                                                <option value="220">+220</option>
                                                                                <option value="221">+221</option>
                                                                                <option value="222">+222</option>
                                                                                <option value="223">+223</option>
                                                                                <option value="224">+224</option>
                                                                                <option value="225">+225</option>
                                                                                <option value="226">+226</option>
                                                                                <option value="227">+227</option>
                                                                                <option value="228">+228</option>
                                                                                <option value="229">+229</option>
                                                                                <option value="230">+230</option>
                                                                                <option value="231">+231</option>
                                                                                <option value="232">+232</option>
                                                                                <option value="233">+233</option>
                                                                                <option value="234">+234</option>
                                                                                <option value="235">+235</option>
                                                                                <option value="236">+236</option>
                                                                                <option value="237">+237</option>
                                                                                <option value="238">+238</option>
                                                                                <option value="239">+239</option>
                                                                                <option value="240">+240</option>
                                                                                <option value="241">+241</option>
                                                                                <option value="242">+242</option>
                                                                                <option value="243">+243</option>
                                                                                <option value="244">+244</option>
                                                                                <option value="245">+245</option>
                                                                                <option value="246">+246</option>
                                                                                <option value="247">+247</option>
                                                                                <option value="248">+248</option>
                                                                                <option value="249">+249</option>
                                                                                <option value="250">+250</option>
                                                                                <option value="251">+251</option>
                                                                                <option value="252">+252</option>
                                                                                <option value="253">+253</option>
                                                                                <option value="254">+254</option>
                                                                                <option value="255">+255</option>
                                                                                <option value="256">+256</option>
                                                                                <option value="257">+257</option>
                                                                                <option value="258">+258</option>
                                                                                <option value="260">+260</option>
                                                                                <option value="261">+261</option>
                                                                                <option value="262">+262</option>
                                                                                <option value="263">+263</option>
                                                                                <option value="264">+264</option>
                                                                                <option value="265">+265</option>
                                                                                <option value="266">+266</option>
                                                                                <option value="267">+267</option>
                                                                                <option value="268">+268</option>
                                                                                <option value="269">+269</option>
                                                                                <option value="27">+27</option>
                                                                                <option value="290">+290</option>
                                                                                <option value="291">+291</option>
                                                                                <option value="297">+297</option>
                                                                                <option value="298">+298</option>
                                                                                <option value="299">+299</option>
                                                                                <option value="30">+30</option>
                                                                                <option value="31">+31</option>
                                                                                <option value="32">+32</option>
                                                                                <option value="33">+33</option>
                                                                                <option value="34">+34</option>
                                                                                <option value="350">+350</option>
                                                                                <option value="351">+351</option>
                                                                                <option value="352">+352</option>
                                                                                <option value="353">+353</option>
                                                                                <option value="354">+354</option>
                                                                                <option value="355">+355</option>
                                                                                <option value="356">+356</option>
                                                                                <option value="357">+357</option>
                                                                                <option value="358">+358</option>
                                                                                <option value="359">+359</option>
                                                                                <option value="36">+36</option>
                                                                                <option value="370">+370</option>
                                                                                <option value="371">+371</option>
                                                                                <option value="372">+372</option>
                                                                                <option value="373">+373</option>
                                                                                <option value="374">+374</option>
                                                                                <option value="375">+375</option>
                                                                                <option value="376">+376</option>
                                                                                <option value="377">+377</option>
                                                                                <option value="378">+378</option>
                                                                                <option value="379">+379</option>
                                                                                <option value="380">+380</option>
                                                                                <option value="381">+381</option>
                                                                                <option value="382">+382</option>
                                                                                <option value="385">+385</option>
                                                                                <option value="386">+386</option>
                                                                                <option value="387">+387</option>
                                                                                <option value="388">+388</option>
                                                                                <option value="389">+389</option>
                                                                                <option value="39">+39</option>
                                                                                <option value="40">+40</option>
                                                                                <option value="41">+41</option>
                                                                                <option value="420">+420</option>
                                                                                <option value="421">+421</option>
                                                                                <option value="423">+423</option>
                                                                                <option value="43">+43</option>
                                                                                <option value="44">+44</option>
                                                                                <option value="45">+45</option>
                                                                                <option value="46">+46</option>
                                                                                <option value="47">+47</option>
                                                                                <option value="48">+48</option>
                                                                                <option value="49">+49</option>
                                                                                <option value="500">+500</option>
                                                                                <option value="501">+501</option>
                                                                                <option value="502">+502</option>
                                                                                <option value="503">+503</option>
                                                                                <option value="504">+504</option>
                                                                                <option value="505">+505</option>
                                                                                <option value="506">+506</option>
                                                                                <option value="507">+507</option>
                                                                                <option value="508">+508</option>
                                                                                <option value="509">+509</option>
                                                                                <option value="51">+51</option>
                                                                                <option value="52">+52</option>
                                                                                <option value="53">+53</option>
                                                                                <option value="54">+54</option>
                                                                                <option value="55">+55</option>
                                                                                <option value="56">+56</option>
                                                                                <option value="57">+57</option>
                                                                                <option value="58">+58</option>
                                                                                <option value="590">+590</option>
                                                                                <option value="591">+591</option>
                                                                                <option value="592">+592</option>
                                                                                <option value="593">+593</option>
                                                                                <option value="594">+594</option>
                                                                                <option value="595">+595</option>
                                                                                <option value="596">+596</option>
                                                                                <option value="597">+597</option>
                                                                                <option value="598">+598</option>
                                                                                <option value="599">+599</option>
                                                                                <option value="61">+61</option>
                                                                                <option value="62">+62</option>
                                                                                <option value="63">+63</option>
                                                                                <option value="64">+64</option>
                                                                                <option value="65">+65</option>
                                                                                <option value="66">+66</option>
                                                                                <option value="670">+670</option>
                                                                                <option value="672">+672</option>
                                                                                <option value="673">+673</option>
                                                                                <option value="674">+674</option>
                                                                                <option value="675">+675</option>
                                                                                <option value="676">+676</option>
                                                                                <option value="677">+677</option>
                                                                                <option value="678">+678</option>
                                                                                <option value="679">+679</option>
                                                                                <option value="680">+680</option>
                                                                                <option value="681">+681</option>
                                                                                <option value="682">+682</option>
                                                                                <option value="683">+683</option>
                                                                                <option value="685">+685</option>
                                                                                <option value="686">+686</option>
                                                                                <option value="687">+687</option>
                                                                                <option value="688">+688</option>
                                                                                <option value="689">+689</option>
                                                                                <option value="690">+690</option>
                                                                                <option value="691">+691</option>
                                                                                <option value="692">+692</option>
                                                                                <option value="7">+7</option>
                                                                                <option value="800">+800</option>
                                                                                <option value="808">+808</option>
                                                                                <option value="81">+81</option>
                                                                                <option value="82">+82</option>
                                                                                <option value="84">+84</option>
                                                                                <option value="850">+850</option>
                                                                                <option value="852">+852</option>
                                                                                <option value="853">+853</option>
                                                                                <option value="855">+855</option>
                                                                                <option value="856">+856</option>
                                                                                <option value="86">+86</option>
                                                                                <option value="870">+870</option>
                                                                                <option value="878">+878</option>
                                                                                <option value="880">+880</option>
                                                                                <option value="881">+881</option>
                                                                                <option value="882">+882</option>
                                                                                <option value="883">+883</option>
                                                                                <option value="886">+886</option>
                                                                                <option value="888">+888</option>
                                                                                <option value="90">+90</option>
                                                                                <option value="91">+91</option>
                                                                                <option value="92">+92</option>
                                                                                <option value="93">+93</option>
                                                                                <option value="94">+94</option>
                                                                                <option value="95">+95</option>
                                                                                <option value="960">+960</option>
                                                                                <option value="961">+961</option>
                                                                                <option value="962">+962</option>
                                                                                <option value="963">+963</option>
                                                                                <option value="964">+964</option>
                                                                                <option value="965">+965</option>
                                                                                <option value="966">+966</option>
                                                                                <option value="967">+967</option>
                                                                                <option value="968">+968</option>
                                                                                <option value="970">+970</option>
                                                                                <option value="971">+971</option>
                                                                                <option value="972">+972</option>
                                                                                <option value="973">+973</option>
                                                                                <option value="974">+974</option>
                                                                                <option value="975">+975</option>
                                                                                <option value="976">+976</option>
                                                                                <option value="977">+977</option>
                                                                                <option value="979">+979</option>
                                                                                <option value="98">+98</option>
                                                                                <option value="991">+991</option>
                                                                                <option value="992">+992</option>
                                                                                <option value="993">+993</option>
                                                                                <option value="994">+994</option>
                                                                                <option value="995">+995</option>
                                                                                <option value="996">+996</option>
                                                                                <option value="998">+998</option>

                                                                            </select></span>
                                                                        <input type="text" name="Telephone" id="txtTelephone" runat="server" class="form-control" placeholder="Telephone No. (Office)" clientidmode="static" readonly="readonly" style="padding-left: 30%;" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 divOccupationPassword">
                                                                <div class="profile-desk">
                                                                    <div class="form-group mb-10">
                                                                        <label>Your Password</label><sup class="text-danger">*</sup>
                                                                        <asp:TextBox ID="txtOccupationPassword" TextMode="Password" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter Password"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div />


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div id="divAddress2">
                                            <label id="Label2" clientidmodel="static" runat="server" visible="false">Approval:</label>
                                            <span id="Span1" clientidmodel="static" runat="server" visible="false">
                                                <span class="text-warning"><i class="fa fa-exclamation-circle mr-6"></i>Address Approval is Pending</span><br />
                                            </span>
                                            <span id="Span2" runat="server" clientidmodel="static" visible="false">
                                                <span class="text-success"><i class="fa fa-check mr-6"></i>Address is Approved</span><br />
                                            </span>
                                            <span id="Span3" runat="server" clientidmodel="static" visible="false">
                                                <span class="text-danger"><i class="fa fa-times mr-6"></i>Request for change of address is rejected by eApexIs Admin</span><br />
                                            </span>
                                            <br />
                                            <table class="table table-condensed table-cell-pad-5 no-border">
                                                <tbody id="Tbody1" runat="server">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="">
                                        <asp:Button ID="btnSubmitEditOccupation" ClientIDMode="Static" runat="server" CssClass="btn btn-sett-confirm hide" Text="Submit" OnClientClick="return clientOccupationValidation" OnClick="btnSubmitEditOccupation_Click" />
                                        <button type="button" id="btnOccupationEdit" runat="server" clientidmode="Static" onclick="OccupationEdit()" class="btn btn-sett-confirm hides">Edit</button>
                                        <button type="button" class="btn btn-sett-cancel" id="btnOccupationCancel" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>

        <div class="modal fade mobile-height" id="bankDetails">
            <div class="modal-dialog sett two">
                <asp:Panel ID="Panel1BankDetails" runat="server" DefaultButton="">
                    <div class="modal-content">
                        <div class="modal-header sett-modalhead">
                            <h2 class="text-white mb-0">Account Settings: Bank Details</h2>
                        </div>
                        <div class="modal-body" style="height: calc(100vh - 200px); overflow-y: scroll;">
                            <div class="row">

                                <asp:HiddenField ID="hdnBindBankId" runat="server" ClientIDMode="Static" />
                                <asp:Button ID="btnBindBank" CssClass="hide" runat="server" ClientIDMode="Static" OnClick="btnBindBank_Click" />
                                <asp:Button ID="btnUnbindBank" CssClass="hide" runat="server" ClientIDMode="Static" OnClick="btnUnbindBank_Click" />
                                <div class="col-md-7">
                                    <div class="table-responsive" style="border: none!important;">
                                        <table class="table table-font-size-13 table-cell-pad-5 table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>MA No</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Number</th>
                                                    <th>File</th>
                                                    <th>Binded Date</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyUserBankAccounts" runat="server">
                                                <tr>
                                                    <td colspan="6" class="text-center">No records found.
                                                    Please proceed to add and bind your bank details in your <a href="UserSettings.aspx?isPopup=1&Popup=openPopup('bankDetails')">User Settings.</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="col-md-7 hide">

                                    <div class="form-group mb-10">

                                        <div class="col-md-6">
                                            <label>Account :</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" disabled="disabled" value="1024 Cash" />
                                        </div>

                                    </div>

                                    <div class="form-group mb-10">
                                        <label>Bank Name <sup class="text-danger">*</sup></label>
                                        <asp:HiddenField ID="hdnMaHolderBankId" runat="server" ClientIDMode="Static" Value="0" />
                                        <asp:DropDownList ID="ddlBank" ClientIDMode="Static" runat="server" CssClass="form-control" EnableViewState="true">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hdnBankNoFormat" runat="server" ClientIDMode="Static" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-10">
                                                <label>Account Name <sup class="text-danger">*</sup></label>
                                                <asp:DropDownList ID="ddlAccountname" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-10">
                                                <label>Account Number <sup class="text-danger">*</sup></label>
                                                <asp:HiddenField ID="hdnAccountNumber" runat="server" ClientIDMode="Static" Value="0" />
                                                <asp:TextBox ID="txtAccountNumber" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter Account Number"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div id="divBankAttachment" class="form-group mb-10">
                                                <label>Attachment file <sup class="text-danger">*</sup></label>
                                                <asp:FileUpload ID="FileUpload1" CssClass="userKYC" runat="server" ClientIDMode="Static" accept=".jpeg,.jpg,.png,.pdf" />
                                            </div>
                                            <div id="divBankAttachment1" class="form-group mb-10">
                                                <label>Attachment file :</label>
                                                <a href="#" target="_blank" id="attachemtFile">
                                                    <asp:Label ID="lblBankAttachment" runat="server" ClientIDMode="Static" CssClass="hide"></asp:Label>
                                                    Click here to view
                                                </a>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group mb-10">
                                        <label>Authentication<sup class="text-danger">*</sup></label>
                                        <div class="row mb-5">
                                            <div class="col-md-6">
                                                <asp:RadioButton ID="rdnGoogleBank" runat="server" Text="Google Authentication" GroupName="pac" ClientIDMode="Static" Checked="true" />
                                            </div>
                                            <div class="col-md-6">
                                                <asp:RadioButton ID="rdnMobileBank" runat="server" Text="Mobile Authentication" GroupName="pac" ClientIDMode="Static" />
                                            </div>
                                        </div>
                                        <div class="form-group mb-5">
                                            <asp:TextBox ID="txtPacNoBank" runat="server" CssClass="form-control" placeholder="" ClientIDMode="Static"></asp:TextBox>
                                            <button type="button" id="btnPACBank" runat="server" class="btn btn-sm btn-primary hide" clientidmode="static" style="position: absolute; margin-top: -31px; right: 19px;">Request OTP</button>
                                        </div>
                                        <small class="" id="lblInfoBank" runat="server" clientidmode="static"></small>
                                        <br />
                                        <small class="" id="countdownLabelTextBank" runat="server" clientidmode="static"></small>
                                        <small class="" id="countDownTimeBank" runat="server" clientidmode="static"></small>
                                        <div id="divBank2" class="col-md-12">
                                            <label id="approvalBank" runat="server" visible="false">Approval:</label>
                                            <div id="banknotverified" runat="server" visible="false">
                                                <a class="text-warning"><i class="fa fa-exclamation-circle mr-6"></i>Bank Detail Approval is Pending</a><br />
                                            </div>
                                            <div id="bankverified" runat="server" visible="false">
                                                <a class="text-success"><i class="fa fa-check mr-6"></i>Bank Detail is Approved</a><br />
                                            </div>
                                            <div id="bankdenied" runat="server" visible="false">
                                                <a class="text-danger"><i class="fa fa-times mr-6"></i>Bank Detail is Denied</a><br />
                                            </div>
                                            <table>
                                                <tbody id="bankdetail" runat="server">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group mb-10">
                                        <p><b>Notes</b></p>
                                        <ul>
                                            <li>Your bank account number registered will be maintained by Apex for crediting of all future payments(distribution, redemption proceeds and other monies payable) to you.</li>
                                            <li>Only one  bank account number is allowed to be binded on each Master Account in our record. </li>
                                            <li>Remittance amount that you receive may be lesser than your initial redemption amount due to correspondence bank charges or intermediate bank charges (if any), which will be borne by investor and deducted directly from the redemption proceeds.</li>
                                            <li>For security purpose, Google Code/ Mobile OTP must be keyed in for registration of bank account.</li>
                                            <li>All payments will be made to the MAIN account holder. We will not make any payments to third-parties</li>
                                            <li>In the event that  any payment to your bank account is  unsuccessful, Apex will issue a cheque and send to your mailing address.</li>
                                            <li>To update your existing bank account details, kindly attach a copy of your latest bank statement (not later than 3 months).</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <%--<asp:Button ID="btnBankDetailsSubmit" ClientIDMode="Static" OnClientClick="return clientBankValidation()" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnBankDetailsSubmit_Click" />
                            <button type="button" id="btnBankEdit" class="btn btn-sett-confirm" onclick="EditBank()">Edit</button>--%>
                            <button type="button" class="btn btn-sett-cancel" onclick="return clearBank()" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </asp:Panel>
            </div>

        </div>

        <div class="modal fade" id="alerts">
            <div class="modal-dialog modal-lg" style="width: 70%;">
                <asp:Panel ID="alertsPanel" runat="server" DefaultButton="btnSaveAlert">
                    <div class="modal-content">
                        <div class="modal-header sett-modalhead">
                            <h2 class="text-white mb-0">Watch List</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-information">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div id="notificationSettings" runat="server" class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group mb-12">
                                                            <div class="col-md-5">
                                                                <label>
                                                                    Fund Price Target 
                                                                <span class="alert-percent-span">(MYR)</span>
                                                                    <br />
                                                                    <small class="fs-12">(Get email notification when reach fund price target)</small>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:TextBox ID="txtFundPriceTargetText" placeholder="Eg. 0.1234" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlFundPriceTargetFund" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-12">

                                                <div class="form-group mb-12">
                                                    <div class="col-md-5">
                                                        <label>
                                                            Fund Performance Target <span class="alert-percent-span">(%)</span><br />
                                                            <small class="fs-12">(Get email notification when reach fund performance target)</small>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:TextBox ID="txtFundPerformanceTarget" placeholder="Eg. 12%" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlFundPerformanceTargetFund" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                        <div class="row mt-10" id="watchListDiv" runat="server">
                                            <div class="col-md-12">
                                                <table class="table table-condensed table-bordered dataTable table-cell-pad-5">
                                                    <thead>
                                                        <tr style="width: 35.70%">
                                                            <th>Fund Name</th>
                                                            <th style="width: 32.20%">Fund Price Target (MYR)</th>
                                                            <th style="width: 32.20%">Fund Performance Target (%)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbodyWatchList" runat="server" clientidmode="static">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSaveAlert" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnSaveAlert_Click" ClientIDMode="Static" OnClientClick="return ValidateWatchList()" />
                            <%--<button type="button" id="SaveAlert" class="btn btn-sett-confirm">Save</button>--%>
                            <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>

        <%--    <div class="modal fade" id="bankDetails">
            <div class="modal-dialog sett two">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">Bank Details</h2>
                    </div>
                    <div class="modal-body" style="height: calc(100vh - 200px); overflow-y: scroll;">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group mb-10">
                                    <label>Bank Name <sup class="text-danger">*</sup></label>
                                    <asp:HiddenField ID="hdnMaHolderBankId" runat="server" ClientIDMode="Static" Value="0" />
                                    <asp:DropDownList ID="ddlBank" ClientIDMode="Static" runat="server" CssClass="form-control" EnableViewState="true">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnBankNoFormat" runat="server" ClientIDMode="Static" />
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-10">
                                            <label>Account Name <sup class="text-danger">*</sup></label>
                                            <asp:DropDownList ID="ddlAccountname" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mb-10">
                                            <label>Account Number <sup class="text-danger">*</sup></label>
                                            <asp:HiddenField ID="hdnAccountNumber" runat="server" ClientIDMode="Static" Value="0" />
                                            <asp:TextBox ID="txtAccountNumber" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter Account Number"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div id="divBankAttachment" class="form-group mb-10">
                                            <label>Attachment file <sup class="text-danger">*</sup></label>
                                            <asp:FileUpload ID="FileUpload1" CssClass="userKYC" runat="server" ClientIDMode="Static" accept=".jpeg,.jpg,.png,.pdf" />
                                        </div>
                                        <div id="divBankAttachment1" class="form-group mb-10">
                                            <label>Attachment file :</label>
                                            <a href="#" target="_blank" id="attachemtFile">
                                                <asp:Label ID="lblBankAttachment" runat="server" ClientIDMode="Static" CssClass="hide" ></asp:Label> Click here to view
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group mb-10">
                                    <label>Authentication<sup class="text-danger">*</sup></label>
                                    <div class="row mb-5">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnGoogleBank" runat="server" Text="Google Authentication" GroupName="pac" ClientIDMode="Static" Checked="true" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnMobileBank" runat="server" Text="Mobile Authentication" GroupName="pac" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <div class="form-group mb-5">
                                        <asp:TextBox ID="txtPacNoBank" runat="server" CssClass="form-control" placeholder="" ClientIDMode="Static"></asp:TextBox>
                                        <button type="button" id="btnPACBank" runat="server" class="btn btn-sm btn-primary hide" clientidmode="static" style="position: absolute; margin-top: -31px; right: 19px;">Request OTP</button>
                                    </div>
                                    <small class="" id="lblInfoBank" runat="server" clientidmode="static"></small>
                                    <br />
                                    <small class="" id="countdownLabelTextBank" runat="server" clientidmode="static"></small>
                                    <small class="" id="countDownTimeBank" runat="server" clientidmode="static"></small>
                                    <div id="divBank2" class="col-md-12">
                                        <label id="approvalBank" runat="server" visible="false">Approval:</label>
                                        <div id="banknotverified" runat="server" visible="false">
                                            <a class="text-warning"><i class="fa fa-exclamation-circle mr-6"></i>Bank Detail Approval is Pending</a><br />
                                        </div>
                                        <div id="bankverified" runat="server" visible="false">
                                            <a class="text-success"><i class="fa fa-check mr-6"></i>Bank Detail is Approved</a><br />
                                        </div>
                                        <div id="bankdenied" runat="server" visible="false">
                                            <a class="text-danger"><i class="fa fa-times mr-6"></i>Bank Detail is Denied</a><br />
                                        </div>
                                        <table>
                                            <tbody id="bankdetail" runat="server">
                                            </tbody>
                                        </table>
                                        <div><small><strong>Note: If you choose Google Authentication, please make sure Google Authenticator is binded.</strong></small></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group mb-10">
                                    <p><b>Notes</b></p>
                                    <ul>
                                        <li>Your bank account number registered will be maintained by Apex for crediting of all future payments(distribution, redemption proceeds and other monies payable) to you.</li>
                                        <li>The updated bank account number will replace/supersede the existing bank account (if any) in our record.</li>
                                        <li>For security purpose, Google key/ Mobile pin must be keyed in for registration of bank account.</li>
                                        <li>Please ensure that the bank account is in your name. In the event any payment to your bank account is not successful, Apex will issue a cheque and send to your mailing address.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnBankDetailsSubmit" ClientIDMode="Static" OnClientClick="return clientBankValidation()" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnBankDetailsSubmit_Click" />
                        <button type="button" id="btnBankEdit" class="btn btn-sett-confirm" onclick="EditBank()">Edit</button>
                        <button type="button" class="btn btn-sett-cancel" onclick="return clearBank()" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>--%>

        <div class="modal fade" id="ORDERHISTORY">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">Order History</h2>
                    </div>
                    <div class="modal-body" style="height: calc(100vh - 200px); overflow-y: scroll;">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="OrderList.aspx">Only display latest 10 transaction records, click to view all transaction.</a>
                                <table class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><a href="/OrderConfirmation.aspx">Order Number</a></th>
                                            <th>Transaction Type</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="OrderHistory" runat="server"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade mobile-height" id="distributionInstruction">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">Distribution Instruction</h2>
                    </div>
                    <div class="modal-body" style="height: calc(100vh - 200px); overflow-y: scroll;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" style="border: none !important;">
                                    <table class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>MA Account</th>
                                                <th>Fund Name</th>
                                                <th>Instruction</th>
                                            </tr>
                                        </thead>
                                        <tbody id="TbodyDistributionInstruction" runat="server"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--<asp:Button ID="btnDistributionInstruction" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnDistributionInstruction_Click" />--%>
                        <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnMobilePinRequested" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hdnUnemployedOccupation" runat="server" ClientIDMode="Static" Value="0" />
    </asp:Content>




    <asp:Content ID="Content3" ContentPlaceHolderID="AccountScripts" runat="server">

        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

        <script type="text/javascript">
            var hdnMobilePinRequestedBank = parseInt($('#hdnMobilePinRequestedBank').val());
            function ValidateWatchList() {
                var isValid = true;
                var d1 = $('#ddlFundPriceTargetFund').val();
                var t1 = $('#txtFundPriceTargetText').val();

                var d2 = $('#ddlFundPerformanceTargetFund').val();
                var t2 = $('#txtFundPerformanceTarget').val();

                if (d1 == '' && t1 == '' && d2 == '' && t2 == '') {
                    ShowCustomMessage("Warning", "Please select atleast one fund and enter a value to Save!", "");
                    isValid = false;
                }
                else {
                    if (d1 != '' & t1 == '') {
                        $('#txtFundPriceTargetText').css({
                            "borderBottom": "1px solid red",
                        });
                        $('#ddlFundPriceTargetFund').css({
                            "borderBottom": "",
                        });
                        isValid = false;
                    }
                    if (d1 == '' && t1 != '') {
                        $('#ddlFundPriceTargetFund').css({
                            "borderBottom": "1px solid red",
                        });
                        $('#txtFundPriceTargetText').css({
                            "borderBottom": "",
                        });
                        isValid = false;
                    }
                    if (d1 != '' && t1 != '') {
                        $('#ddlFundPriceTargetFund').css({
                            "borderBottom": "",
                        });
                        $('#txtFundPriceTargetText').css({
                            "borderBottom": "",
                        });
                    }
                    if (d2 != '' & t2 == '') {
                        $('#txtFundPerformanceTarget').css({
                            "borderBottom": "1px solid red",
                        });
                        $('#ddlFundPerformanceTargetFund').css({
                            "borderBottom": "",
                        });
                        isValid = false;
                    }
                    if (d2 == '' && t2 != '') {
                        $('#ddlFundPerformanceTargetFund').css({
                            "borderBottom": "1px solid red",
                        });
                        $('#txtFundPerformanceTarget').css({
                            "borderBottom": "",
                        });
                        isValid = false;
                    }
                    if (d2 != '' && t2 != '') {
                        $('#ddlFundPerformanceTargetFund').css({
                            "borderBottom": "",
                        });
                        $('#txtFundPerformanceTarget').css({
                            "borderBottom": "",
                        });
                    }
                }
                return isValid;
            }

            function hideButton() {

                $.ajax({

                    url: "Settings.aspx/HideButton",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: function (data) {
                        return data;
                    },
                    success: function (data) {
                        if (data.d == true) {

                            $('.bindBank').hide();
                        }
                        else {

                        }

                    }

                });
            }

            $(document).ready(function () {

                //$('.satStatusClass').insertAfter('.satScoreClass');

                $('#addr1').hide();
                $('#addr2').hide();
                $('#pcode').hide();
                $('#state').hide();
                $('#pword').hide();

                $('#txtFundPriceTargetText').inputFilter(function (value) {
                    return /^(\d*\.)?\d*$/.test(value);
                    //return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
                });
                $('#txtFundPerformanceTarget').inputFilter(function (value) {
                    return /^(\d*\.)?\d*$/.test(value);
                    //return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
                });

                $('#btnAddressCancel').click(function () {
                    $('#addr1').hide();
                    $('#addr2').hide();
                    $('#pcode').hide();
                    $('#state').hide();
                    $('#pword').hide();
                });

                $('#ddlFundPriceTargetFund').change(function () {
                    $('#txtFundPriceTargetText').attr('placeholder', 'Eg. 0.1234');
                    $('#txtFundPriceTargetText').attr('readonly', 'readonly');
                    if ($('#ddlBank').val() != "") {
                        $.ajax({
                            url: "Settings.aspx/GetNAVPrice",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            async: true,
                            data: { 'fundId': JSON.stringify($('#ddlFundPriceTargetFund').val()) },
                            success: function (data) {
                                $('#txtFundPriceTargetText').removeAttr('readonly');
                                if (data.d.IsSuccess == true) {
                                    $('#txtFundPriceTargetText').attr('placeholder', 'Eg. ' + (data.d.Data).toFixed(4));
                                    //$('#txtFundPriceTargetText').val((data.d.Data).toFixed(4));
                                }
                                else {
                                    ShowCustomMessage('Alert', data.d.Message, '');
                                }
                            }
                        });
                    }
                });

                $('#ddlOccupation').change(function () {
                    $('#hdnUnemployedOccupation').val($('#ddlOccupation').val());
                    if ($(this).val() == 'Others') {

                        $('.occupation-others').removeClass('hide');

                    }
                    else {
                        $('.occupation-others').addClass('hide');
                    }
                });

                $('#address').on('shown.bs.modal', function () {
                    $('#txtAddressLine1').focus();
                });

                $('.bindBank').click(function () {
                    var id = $(this).data('id');
                    $('#hdnBindBankId').val(id);
                    $('#btnBindBank').click();
                });
                $('.unbindBank').click(function () {
                    var id = $(this).data('id');
                    $('#hdnBindBankId').val(id);
                    $('#btnUnbindBank').click();
                });
                hideButton();



                //$('#txtAddressLine1').prop('readonly', true);
                //$('#txtPostCode').prop('readonly', true);
                //$('#txtAddressLine2').prop('readonly', true);
                //$('#ddlState').prop('readonly', true);
                //$('#txtAddressLine3').prop('readonly', true);
                //$('#txtAddressLine4').prop('readonly', true);
                addresssClear();
                $('#btnAddressCancel').click(function () {
                    addresssClear();
                });
                profileClear();
                $('#btnProfileCancel').click(function () {
                    profileClear();
                });
                occupationClear();
                $('#btnOccupationCancel').click(function () {
                    occupationClear();
                });

                $("#txtPostCode").inputFilter(function (value) {
                    if (value.length >= 6) {
                        return false;
                    }
                    else {
                        return /^\d*$/.test(value);
                    }
                });



                bankReadonly();

                $('#ddlBank').change(function () {
                    $('#txtAccountNumber').val('');
                });

                $('#txtAccountNumber').inputFilter(function (value) {
                    var bankcount = parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                    if (value.length > bankcount) {
                        return false;
                    }
                    else {
                        return /^\d*$/.test(value);
                    }
                    //return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
                });
                //$('.checkbox-group-2 input:checkbox').click(function () {
                //    $('.checkbox-group-2 input:checkbox').not(this).prop('checked', false);
                //    $('.loader-bg').fadeIn();
                //    if ($(this).attr('id') == 'Employed') {
                //        document.getElementById("lblOccupation").innerHTML = "Occupation";
                //        if ($(this).is(':checked')) {
                //            $('.occupation-field').removeClass('hide');
                //            $('.employed-fields').removeClass('hide');
                //            $('#occupationOthers').removeClass('hide');
                //            if (!$('.occupation-others').hasClass('hide'))
                //                $('.occupation-others').addClass('hide');
                //            //ddlOccOptions = $('#ddlOccupation option');
                //            $('#ddlOccupation option').remove();
                //            $.ajax({
                //                url: "AccountOpening.aspx/GetOccupations",
                //                contentType: 'application/json; charset=utf-8',
                //                type: "GET",
                //                dataType: "JSON",
                //                //data: $('form').serializeArray(),
                //                success: function (data) {
                //                    console.log(data);
                //                    if (data.d.IsSuccess) {
                //                        var json = data.d.Data;
                //                        var option = document.createElement("option");
                //                        option.text = "Select";
                //                        option.value = "";
                //                        $('#ddlOccupation').append(option);
                //                        $.each(json, function (idx, obj) {
                //                            var option = document.createElement("option");
                //                            option.text = obj.OCCCODE;
                //                            option.value = obj.SDESC;
                //                            $('#ddlOccupation').append(option);
                //                        });
                //                        $('#ddlOccupation').val('');
                //                    }
                //                    else {
                //                        ShowCustomMessage("Alert", data.d.Message, "");
                //                    }
                //                }
                //            });


                //        }
                //        else {
                //            $('.occupation-field').addClass('hide');
                //            $('.employed-fields').addClass('hide');
                //        }

                //    }
                //    else if ($(this).attr('id') == 'Unemployed') {
                //        if ($(this).is(':checked')) {
                //            $('.occupation-field').removeClass('hide');
                //            $('.employed-fields').addClass('hide');
                //            if (!$('.occupation-others').hasClass('hide'))
                //                $('.occupation-others').addClass('hide');
                //            ddlOccOptions = $('#ddlOccupation option');
                //            document.getElementById("lblOccupation").innerHTML = "Status";
                //            $('#ddlOccupation option').remove();
                //            $('#ddlOccupation').append('<option value="">Select Status</option>');
                //            $('#ddlOccupation').append('<option value="Student">Student</option>');
                //            $('#ddlOccupation').append('<option value="Retired">Retired</option>');
                //            $('#ddlOccupation').append('<option value="Housewife">Housewife</option>');
                //            $('#ddlOccupation').append('<option value="Others">Others</option>');
                //        }
                //        else {
                //            $('.occupation-others').addClass('hide');
                //            $('.occupation-field').addClass('hide');
                //            $('.employed-fields').addClass('hide');
                //            //$('.occupation-others').addClass('hide');
                //        }
                //    }
                //    $('.loader-bg').fadeOut();
                //});

            });

            function addresssClear() {
                $('#btnAddressSubmit').hide();
                $('#btnEdit').show();
                $('#txtAddressLine1,#txtAddressLine2,#txtCurrentPasswordAddress, #txtPostCode,#txtAddressLine3,#txtAddressLine4').prop('readonly', true);
                $('#ddlState').prop('disabled', true);
                $('.divPassword').hide();
                $('#txtAddressLine1').val($('#hdnAddressLine1').val());
                $('#txtAddressLine2').val($('#hdnAddressLine2').val());
                $('#txtAddressLine3').val($('#hdnAddressLine3').val());
                $('#txtAddressLine4').val($('#hdnAddressLine4').val());
                $('#txtPostCode').val($('#hdnPostCode').val());
                $('#ddlState').val($('#hdnState').val());
                $('#divAddress2').show();

            }
            function profileClear() {
                $('#btnSubmitEditProfile').hide();
                $('#btnEditProfile').show();
                //$('#txtAddressLine1,#txtAddressLine2,#txtCurrentPasswordAddress, #txtPostCode,#txtAddressLine3,#txtAddressLine4').prop('readonly', true);
                //$('#ddlState').prop('disabled', true);
                //$('.divPassword').hide();
                //$('#txtAddressLine1').val($('#hdnAddressLine1').val());
                //$('#txtAddressLine2').val($('#hdnAddressLine2').val());
                //$('#txtAddressLine3').val($('#hdnAddressLine3').val());
                //$('#txtAddressLine4').val($('#hdnAddressLine4').val());
                //$('#txtPostCode').val($('#hdnPostCode').val());
                //$('#ddlState').val($('#hdnState').val());
                //$('#divAddress2').show();

            }
            function occupationClear() {
                $('#btnOccupationSubmit').addClass('hide');
                $('#btnOccupationEdit').removeClass('hide');
                $('#lblEmployment').addClass('hide');
                $('#employedOption').addClass('hide');
                $('#btnSubmitEditOccupation').addClass('hide');

                $('#ddlOccupation').addClass('hide');
                $('#lblViewOccupation').removeClass('hide');
                $('#txtEmployerName').addClass('hide');
                $('#lblEmployerName').removeClass('hide');
                $('#txtEmployerName').attr('readonly', 'readonly');
                $('#ddlNatureOfBusiness').attr('readonly', 'readonly');
                $('#txtNatureOfBusiness').removeClass('hide');
                $('#ddlNatureOfBusiness').addClass('hide');
                $('#ddlNatureOfBusiness').attr('disabled', 'disabled');
                $('#txtIncome').removeClass('hide');
                $('#ddlMonthlyIncome').addClass('hide');
                $('#ddlMonthlyIncome').attr('readonly', 'readonly');
                $('#ddlOccupation').attr('readonly', 'readonly');
                $('#txtOfficeAddress1').attr('readonly', 'readonly');
                $('#txtOfficeAddress2').attr('readonly', 'readonly');
                $('#txtOfficeAddress3').attr('readonly', 'readonly');
                $('#txtOfficePostCode').attr('readonly', 'readonly');
                $('#txtCity').attr('readonly', 'readonly');
                $('#ddlOfficeState').attr('readonly', 'readonly');
                $('#txtOfficeCountry').attr('disabled', 'disabled');
                $('#officeFrontNumber').attr('disabled', 'disabled');
                $('#txtTelephone').attr('readonly', 'readonly');
                $('.divOccupationPassword').addClass('hide');

            }
            function OccupationEdit() {
                $('#Employed').prop('checked', true);
                $('#lblEmployment').removeClass('hide');
                $('#employedOption').removeClass('hide');
                $('#btnSubmitEditOccupation').removeClass('hide');
                $('#btnOccupationEdit').addClass('hide');
                $('#ddlOccupation').removeClass('hide');
                $('#lblViewOccupation').addClass('hide');
                $('#txtEmployerName').removeClass('hide');
                $('#lblEmployerName').addClass('hide');
                $('#txtEmployerName').removeAttr('readonly');
                $('#ddlNatureOfBusiness').removeAttr('readonly');
                $('#txtNatureOfBusiness').addClass('hide');
                $('#ddlNatureOfBusiness').removeClass('hide');
                $('#ddlNatureOfBusiness').removeAttr('disabled');
                $('#txtIncome').addClass('hide');
                $('#ddlMonthlyIncome').removeClass('hide');
                $('#ddlMonthlyIncome').removeAttr('readonly');
                $('#ddlOccupation').removeAttr('readonly');
                $('#txtOfficeAddress1').removeAttr('readonly');
                $('#txtOfficeAddress2').removeAttr('readonly');
                $('#txtOfficeAddress3').removeAttr('readonly');
                $('#txtOfficePostCode').removeAttr('readonly');
                $('#txtCity').removeAttr('readonly');
                $('#ddlOfficeState').removeAttr('readonly');
                $('#txtOfficeCountry').removeAttr('disabled');
                $('#officeFrontNumber').removeAttr('disabled');
                $('#txtTelephone').removeAttr('readonly');

                //$('#divAddress2').hide();
                $('.divOccupationPassword').removeClass('hide');
                $('#ddlState').prop('disabled', false);
                $('#txtAddressLine1,#txtAddressLine2,#txtCurrentPasswordAddress, #txtPostCode,#txtAddressLine3,#txtAddressLine4').prop('readonly', false);
            }
            function MaAddressEdit() {
                $('#btnAddressSubmit').show();
                $('#btnEdit').hide();
                $('#divAddress2').hide();
                $('.divPassword').show();
                $('#ddlState').prop('disabled', false);
                $('#txtAddressLine1,#txtAddressLine2,#txtCurrentPasswordAddress, #txtPostCode,#txtAddressLine3,#txtAddressLine4').prop('readonly', false);

                $('#addr1').show();
                $('#addr2').show();
                $('#pcode').show();
                $('#state').show();
                $('#pword').show();
            }
            function MaProfileEdit() {
                $('#btnSubmitEditProfile').show();
                $('#btnEditProfile').hide();
                $('#divProfile2').hide();
                $('.divPassword2').show();
                //$('#ddlState').prop('disabled', false);
                //$('#txtAddressLine1,#txtAddressLine2,#txtCurrentPasswordAddress, #txtPostCode,#txtAddressLine3,#txtAddressLine4').prop('readonly', false);
            }
            function CheckNumeric(e) {

                if (window.event) // IE 
                {
                    if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 45) {
                        console.log("asadsadsa");
                        event.returnValue = false;
                        return false;
                    }
                }
                else { // Fire Fox
                    if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 45) {
                        console.log("asadsadsa");
                        event.returnValue = false;
                        return false;
                    }
                }
            }
            function openDvModal() {
                $('#distributionInstruction').modal('show');
            }
            function openPopup(popupid) {
                $('a[data-target="#' + popupid + '"]').click();
            }
            function bankReadonly() {
                $('#txtAccountNumber').prop('readonly', true);
                $('#txtPacNoBank').prop('readonly', true);
                $('#ddlAccountname').prop('disabled', true);
                $('#ddlBank').prop('disabled', true);
                $('#btnBankDetailsSubmit').hide();
                $('#btnBankEdit').show();
                $('#divBankAttachment').hide();
                $('#divBankAttachment1').show();
                $('#attachemtFile').attr('href', $('#lblBankAttachment').text())
                $('#rdnGoogleBank').attr('disabled', true);
                $('#rdnMobileBank').attr('disabled', true)
            }

            function clearBank() {
                $('#ddlBank').val('0');
                $('#txtAccountNumber').val('');
                $('#txtPacNoBank').val('');
                $('#FileUpload1').val('');
                if ($('#hdnMaHolderBankId').val() != '') {
                    $('#ddlBank').val($('#hdnMaHolderBankId').val());
                }
                $('#txtAccountNumber').val($('#hdnAccountNumber').val());
                $('#divBank2').show();
                bankReadonly();
            }
            function EditBank() {
                $('#txtAccountNumber').prop('readonly', false);
                $('#txtPacNoBank').prop('readonly', false);
                $('#ddlAccountname').prop('disabled', false);
                $('#ddlBank').prop('disabled', false);
                $('#btnBankDetailsSubmit').show();
                $('#btnBankEdit').hide();
                $('#divBankAttachment').show();
                $('#divBankAttachment1').hide();
                $('#divBank2').hide();
                $('#rdnGoogleBank').attr('disabled', false);
                $('#rdnMobileBank').attr('disabled', false)
            }

            function clientBankValidation() {
                var isValid = true;
                if ($('#ddlBank').val() == '' || $('#ddlBank').val() == '0') {
                    $('#ddlBank').css({
                        "borderBottom": "1px solid red",
                    });
                    //ShowCustomMessage('Alert', 'Please Select bank', '');
                    isValid = false;
                }
                else {
                    $('#ddlBank').css({
                        "borderBottom": "",
                    });
                }
                if (validation('txtAccountNumber') == false) {
                    $('#txtAccountNumber').css({
                        "borderBottom": "1px solid red",
                    });
                    //ShowCustomMessage('Alert', 'Please enter Account Number', '');
                    isValid = false;
                }
                else {
                    $('#txtAccountNumber').css({
                        "borderBottom": "",
                    });
                    var accountLength = $('#txtAccountNumber').val().length;
                    if ($('option:selected', '#ddlBank').attr('accountnumlength') != undefined) {
                        var bankcount = parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                        if (accountLength != bankcount) {
                            $('#txtAccountNumber').css({
                                "borderBottom": "1px solid red",
                            });
                            ShowCustomMessage('Alert', 'Account number Invalid!', '');
                            isValid = false;
                        }
                        else {
                            $('#txtAccountNumber').css({
                                "borderBottom": "",
                            });
                        }
                    }
                }
                if ($('#FileUpload1').val() == "") {
                    $('#FileUpload1').css({
                        "borderBottom": "1px solid red",
                    });
                    //ShowCustomMessage('Alert', 'Please choose file', '');
                    isValid = false;
                }
                else {
                    $('#FileUpload1').css({
                        "borderBottom": "",
                    });
                }
                if ($('#rdnGoogleBank').is(':checked') || $('#rdnMobileBank').is(':checked')) {
                    if ($('#txtPacNoBank').val() == "") {
                        $('#txtPacNoBank').css({
                            "borderBottom": "1px solid red",
                        });
                        isValid = false;
                    }
                }
                if (isValid) {
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
                return isValid;
            }

            function validation(test) {
                if ($('#' + test).val() == '' || $('#' + test).val() == null) {
                    return false;
                }
                return true;
            }
            var ispasswordValid = false;

            function clientAddressValidation() {
                var isValid = true;
                $('#txtAddressLine1,#txtAddressLine2,#txtCurrentPasswordAddress, #ddlState, #txtPostCode').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "borderBottom": "1px solid red",
                            //"background": "#FFCECE"
                        });
                    }
                    else {
                        $(this).css({
                            "borderBottom": "",
                            //"background": "LightBlue"
                        });
                        var id = $(this).attr('id');
                        if (id == 'txtPostCode') {
                            var value = $(this).val();
                            if (value.length != 5) {
                                $(this).css({
                                    "borderBottom": "1px solid red",
                                    //"background": "#FFCECE"
                                });
                                isValid = false;
                            }
                            else if (!$.isNumeric(value)) {
                                $(this).css({
                                    "borderBottom": "1px solid red",
                                    //"background": "#FFCECE"
                                });
                                isValid = false;
                            }
                        }
                    }
                });

                if (isValid == false) {
                    return false;
                }
                else {
                    if (!ispasswordValid) {
                        var password = $('#txtCurrentPasswordAddress').val();
                        var userdetails = { 'password': password }
                        $.ajax({
                            url: "Settings.aspx/PasswordVerify",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            async: true,
                            data: { 'password': JSON.stringify(password) },
                            success: function (data) {
                                if (data.d == true) {
                                    ispasswordValid = true;
                                    $('#btnAddressSubmit').click();
                                }
                                else {
                                    ispasswordValid = false;
                                    ShowCustomMessage('Alert', 'Invalid password', '')
                                }
                            }
                        });
                        return false;
                    }
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
            }

            function clientOccupationValidation() {
                var isValid = true;
                $('#ddlOccupation, #txtEmployerName, #ddlNatureOfBusiness, #ddlMonthlyIncome, #txtOfficeAddress1, #txtTelephone, #txtCity, #txtOccupationPassword, #ddlOfficeState, #txtOfficePostCode').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "borderBottom": "1px solid red",
                            //"background": "#FFCECE"
                        });
                    }
                    else {
                        $(this).css({
                            "borderBottom": "",
                            //"background": "LightBlue"
                        });
                        var id = $(this).attr('id');
                        if (id == 'txtOfficePostCode') {
                            var value = $(this).val();
                            if (value.length != 5) {
                                $(this).css({
                                    "borderBottom": "1px solid red",
                                    //"background": "#FFCECE"
                                });
                                isValid = false;
                            }
                            else if (!$.isNumeric(value)) {
                                $(this).css({
                                    "borderBottom": "1px solid red",
                                    //"background": "#FFCECE"
                                });
                                isValid = false;
                            }
                        }
                    }
                });

                if (isValid == false) {
                    return false;
                }
                else {
                    if (!ispasswordValid) {
                        var password = $('#txtOccupationPassword').val();
                        var userdetails = { 'password': password }
                        $.ajax({
                            url: "Settings.aspx/PasswordVerify",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            async: true,
                            data: { 'password': JSON.stringify(password) },
                            success: function (data) {
                                if (data.d == true) {
                                    ispasswordValid = true;
                                    $('#btnSubmitEditOccupation').click();
                                }
                                else {
                                    ispasswordValid = false;
                                    ShowCustomMessage('Alert', 'Invalid password', '')
                                }
                            }
                        });
                        return false;
                    }
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
            }

            $(document).ready(function () {
                if ($('#ddlUserAccountId').val() == '') {
                    $('#settingsDiv').addClass('hide');
                }
                else {
                    $('#settingsDiv').removeClass('hide');
                }

                $('.checkbox-group-2 input:checkbox').click(function () {
                    $('.checkbox-group-2 input:checkbox').not(this).prop('checked', false);
                    $('.loader-bg').fadeIn();
                    if ($(this).attr('id') == 'Employed') {
                        document.getElementById("lblOccupation").innerHTML = "Occupation";
                        if ($(this).is(':checked')) {
                            $('.occupation-field').removeClass('hide');
                            $('.employed-fields').removeClass('hide');
                            $('#occupationOthers').removeClass('hide');
                            if (!$('.occupation-others').hasClass('hide'))
                                $('.occupation-others').addClass('hide');
                            //ddlOccOptions = $('#ddlOccupation option');
                            $('#ddlOccupation option').remove();
                            $.ajax({
                                url: "AccountOpening.aspx/GetOccupations",
                                contentType: 'application/json; charset=utf-8',
                                type: "GET",
                                dataType: "JSON",
                                //data: $('form').serializeArray(),
                                success: function (data) {
                                    console.log(data);
                                    if (data.d.IsSuccess) {
                                        var json = data.d.Data;
                                        var option = document.createElement("option");
                                        option.text = "Select";
                                        option.value = "";
                                        $('#ddlOccupation').append(option);
                                        $.each(json, function (idx, obj) {
                                            var option = document.createElement("option");
                                            option.text = obj.OCCCODE;
                                            option.value = obj.SDESC;
                                            $('#ddlOccupation').append(option);
                                        });
                                        $('#ddlOccupation').val('');
                                    }
                                    else {
                                        ShowCustomMessage("Alert", data.d.Message, "");
                                    }
                                }
                            });


                        }
                        else {
                            $('.occupation-field').addClass('hide');
                            $('.employed-fields').addClass('hide');
                        }

                    }
                    else if ($(this).attr('id') == 'Unemployed') {
                        if ($(this).is(':checked')) {
                            $('.occupation-field').removeClass('hide');
                            $('.employed-fields').addClass('hide');
                            if (!$('.occupation-others').hasClass('hide'))
                                $('.occupation-others').addClass('hide');
                            ddlOccOptions = $('#ddlOccupation option');
                            document.getElementById("lblOccupation").innerHTML = "Status";
                            $('#ddlOccupation option').remove();
                            $('#ddlOccupation').append('<option value="">Select Status</option>');
                            $('#ddlOccupation').append('<option value="Student">Student</option>');
                            $('#ddlOccupation').append('<option value="Retired">Retired</option>');
                            $('#ddlOccupation').append('<option value="Housewife">Housewife</option>');
                            $('#ddlOccupation').append('<option value="Others">Others</option>');
                        }
                        else {
                            $('.occupation-others').addClass('hide');
                            $('.occupation-field').addClass('hide');
                            $('.employed-fields').addClass('hide');
                            //$('.occupation-others').addClass('hide');
                             $('.Unemployed').not(this).prop('checked', false);
                        }
                    }
                    $('.loader-bg').fadeOut();
                });

                //$('#ddlUserAccountId').change(function () {
                //    window.location.href = "/settings.aspx?accountId=" + $('#ddlUserAccountId').val();
                //});

                $('#rdnMobileBank').change(function () {
                    if ($(this).is(':checked')) {
                        $('#txtPacNoBank').attr('placeholder', 'Enter OTP');
                        $('#btnPACBank').removeClass('hide');
                    }
                });
                $('#rdnGoogleBank').change(function () {
                    if ($(this).is(':checked')) {
                        $('#txtPacNoBank').attr('placeholder', 'Enter Code');
                        $('#btnPACBank').addClass('hide');
                    }
                });

                $('#btnPACBank').click(function () {
                    //alert(hdnMobilePinRequestedBank);
                    debugger;
                    if (hdnMobilePinRequestedBank == 0) {
                        $.ajax({
                            url: "UserSettings.aspx/BankPhoneVerify",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            data: { title: "-" },
                            success: function (data) {
                                var json = data.d;
                                //console.log(json);
                                $('#hdnMobilePinRequestedBank').val(1);
                                if (json == "sent") {
                                    $('#lblInfoBank').html("OTP sent to mobile successfully.");
                                    $('#countdownLabelTextBank').html('Please Enter your OTP in');
                                    $('#btnPACBank').attr('disabled', 'disabled');
                                    StartTimerBank();
                                }
                                else if (json == "already sent") {
                                    $('#lblInfoBank').html("OTP already sent.");
                                    $('#btnPACBank').attr('disabled', 'disabled');
                                    StartTimerBank();
                                }
                                else if (json == "sms locked") {
                                    $('#lblInfoBank').html("Too many attempts. Please try later.");
                                    $('#countdownLabelTextBank').html('You can request new OTP in ');
                                    //$('#requestNewPinLinkDiv').addClass('hide');
                                    //$('#btnRequestNewPin').attr('disabled', 'disabled');
                                    $('#btnPACBank').attr('disabled', 'disabled');
                                    $('#hdnMobilePinRequestedBank').val(0);
                                    SMSLockStartTimerBank();
                                }
                                else {
                                    $('#lblInfoBank').html("No Account Registered with this Mobile Number.");
                                }

                            }
                        });
                    }
                    else {

                    }
                });



                //$('#table-2').DataTable();
                $('#ddlCountry').change(function () {
                    //$.ajax({
                    //    url: "Settings.aspx/GetStatesByCountryId",
                    //    async: true,
                    //    contentType: 'application/json; charset=utf-8',
                    //    type: "GET",
                    //    dataType: "JSON",
                    //    data: { 'Id': id },
                    //    success: function (data) {
                    //        var json = data.d;
                    //        console.log(json);
                    //        $('#cartTotalAmount').html(json);
                    //        $(that).parents('tr').remove();
                    //    }
                    //});
                });

                //$('#ddlBank').change(function () {
                //    var id = $('#ddlBank').val();
                //    $.ajax({
                //        url: "Settings.aspx/GetBank",
                //        async: true,
                //        contentType: 'application/json; charset=utf-8',
                //        type: "GET",
                //        dataType: "JSON",
                //        data: { 'id': id },
                //        success: function (data) {
                //            $('#hdnBankNoFormat').val(data.d.NoFormat);
                //        }
                //    });
                //});

                // Restricts input for each element in the set of matched elements to the given inputFilter.
            });



            function StartTimerBank() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
                //var timer2 = "5:01";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        $('#countdownLabelTextBank').html('Pin Expired. Please Request Again.<br/>');
                        $('#countDownTimeBank').html('');
                        $('#countDownTimeBank').addClass('hide');
                        //$('#requestNewPinLinkDiv').removeClass('hide');
                        //$('#btnRequestNewPin').removeAttr('disabled');
                        $('#btnPACBank').removeAttr('disabled');
                        $('#hdnMobilePinRequestedBank').val(0);
                    }
                    else {
                        $('#countdownLabelTextBank').html('You can request new OTP in ');
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTimeBank').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                        $('#countDownTimeBank').removeClass('hide');
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }

            function StartTimerGoogle() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        $('#countdownLabelTextGoogle').html('Pin Expired. Please Request Again.<br/>');
                        $('#countDownTimeGoogle').html('');
                        $('#countDownTimeGoogle').addClass('hide');
                        $('#btnRequest').removeAttr('disabled');
                        $('#hdnMobilePinRequestedGoogle').val(0);
                    }
                    else {
                        $('#countdownLabelTextGoogle').html('You can request new OTP in ');
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTimeGoogle').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                        $('#countDownTimeGoogle').removeClass('hide');
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }

            function SMSLockStartTimer() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
                //var timer2 = "5:01";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        //$('.countdown').html('Your Can Request Now.');
                        $('#countdownLabelText').html('You Can Request Now.<br/>');
                        $('#countDownTime').html('');
                        $('#countDownTime').addClass('hide');
                        $('#btnPAC').removeAttr('disabled');
                        $('#hdnMobilePinRequested').val(0);
                    }
                    else {
                        $('#countdownLabelText').html('You can request new OTP in ');
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTime').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                        $('#countDownTime').removeClass('hide');
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }

            function SMSLockStartTimerTrans() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
                //var timer2 = "5:01";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        //$('.countdown').html('Your Can Request Now.');
                        $('#countdownLabelTextTrans').html('You Can Request Now.<br/>');
                        $('#countDownTimeTrans').html('');
                        $('#countDownTimeTrans').addClass('hide');
                        $('#btnPACTrans').removeAttr('disabled');
                        $('#hdnMobilePinRequestedTrans').val(0);
                    }
                    else {
                        $('#countdownLabelTextTrans').html('You can request new OTP in ');
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTimeTrans').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                        $('#countDownTimeTrans').removeClass('hide');
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }

            function SMSLockStartTimerBank() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
                //var timer2 = "5:01";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        //$('.countdown').html('Your Can Request Now.');
                        $('#countdownLabelTextBank').html('You Can Request Now.<br/>');
                        $('#countDownTimeBank').html('');
                        $('#countDownTimeBank').addClass('hide');
                        //$('#requestNewPinLinkDiv').removeClass('hide');
                        //$('#btnRequestNewPin').removeAttr('disabled');
                        $('#btnPACBank').removeAttr('disabled');
                        $('#hdnMobilePinRequestedBank').val(0);
                    }
                    else {
                        $('#countdownLabelTextBank').html('You can request new OTP in ');
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTimeBank').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                        $('#countDownTimeBank').removeClass('hide');
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }

            function SMSLockStartTimerGoogle() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        $('#countdownLabelTextGoogle').html('You Can Request Now.<br/>');
                        $('#countDownTimeGoogle').html('');
                        $('#countDownTimeGoogle').addClass('hide');
                        $('#btnRequest').removeAttr('disabled');
                        $('#hdnMobilePinRequestedGoogle').val(0);
                    }
                    else {
                        $('#countdownLabelTextGoogle').html('You can request new OTP in ');
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTimeGoogle').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                        $('#countDownTimeGoogle').removeClass('hide');
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }




            (function ($) {
                $.fn.inputFilter = function (inputFilter) {
                    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                        if (inputFilter(this.value)) {
                            this.oldValue = this.value;
                            this.oldSelectionStart = this.selectionStart;
                            this.oldSelectionEnd = this.selectionEnd;
                        } else if (this.hasOwnProperty("oldValue")) {
                            this.value = this.oldValue;
                            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                        }
                    });
                };
            }(jQuery));
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js "></script>
        <script>
            $(document).ready(function () {

                $('[data-target="#ORDERHISTORY"]').click(function () {
                    setTimeout(function () {
                        if (!$.fn.DataTable.isDataTable('.dataTable')) {
                            $('.dataTable').DataTable({
                                dom: '',
                                responsive: true,
                            });
                        }
                    }, 200);
                });

                $('#rdnGoogle').change(function () {
                    if ($(this).is(':checked')) {
                        $('#txtPacNo').attr('placeholder', 'Enter Code');
                        $('#btnPAC').addClass('hide');
                    }
                });
                $('#rdnMobile').change(function () {
                    if ($(this).is(':checked')) {
                        $('#txtPacNo').attr('placeholder', 'Enter OTP');
                        $('#btnPAC').removeClass('hide');
                    }
                });

            });

        </script>
        <script type="text/javascript">
            function openPopup(popupid) {
                $('a[data-target="#' + popupid + '"]').click();
            }
            function secondsTimeSpanToHMS(s) {
                var h = Math.floor(s / 3600); //Get whole hours
                s -= h * 3600;
                var m = Math.floor(s / 60); //Get remaining minutes
                s -= m * 60;
                return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
            }
            var hdnSMSExpirationTimeInSeconds = parseInt($('#hdnSMSExpirationTimeInSeconds').val());
            var hdnSMSLockTimeInSeconds = parseInt($('#hdnSMSLockTimeInSeconds').val());
            function StartTimer() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
                //var timer2 = "5:01";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        $('#countdownLabelText').html('Pin Expired. Please Request Again.');
                        $('#countDownTime').html('');
                        //$('#requestNewPinLinkDiv').removeClass('hide');
                        //$('#btnRequestNewPin').removeAttr('disabled');
                        $('#btnPAC').removeAttr('disabled');
                        $('#hdnMobilePinRequested').val(0);
                    }
                    else {
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }
            function SMSLockStartTimer() {
                var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
                //var timer2 = "5:01";
                var interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var hours = parseInt(timer[0], 10);
                    var minutes = parseInt(timer[1], 10);
                    var seconds = parseInt(timer[2], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    hours = (minutes < 0) ? --hours : hours;
                    if (hours < 0) {
                        clearInterval(interval);
                        //$('.countdown').html('Your Can Request Now.');
                        $('#countdownLabelText').html('You Can Request Now.');
                        $('#countDownTime').html('');
                        //$('#requestNewPinLinkDiv').removeClass('hide');
                        //$('#btnRequestNewPin').removeAttr('disabled');
                        $('#btnPAC').removeAttr('disabled');
                        $('#hdnMobilePinRequested').val(0);
                    }
                    else {
                        seconds = (seconds < 0) ? 59 : seconds;
                        seconds = (seconds < 10) ? '0' + seconds : seconds;
                        minutes = (minutes < 0) ? 59 : minutes;
                        minutes = (minutes < 10) ? '0' + minutes : minutes;
                        hours = (hours < 10) ? '0' + hours : hours;
                        $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                        timer2 = hours + ':' + minutes + ':' + seconds;
                    }
                }, 1000);
            }
            $(document).ready(function () {
                // alert message hide
                setTimeout(function () { $('.alert').fadeOut(2000); }, 5000);

                // setTimeout(function () { $('.alert').fadeOut(2000); }, 5000);


                var hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());

                $('#btnPAC').click(function () {
                    if (hdnMobilePinRequested == 0) {
                        $.ajax({
                            url: "Settings.aspx/PhoneVerify",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            data: { title: "-" },
                            success: function (data) {
                                var json = data.d;
                                //console.log(json);
                                $('#hdnMobilePinRequested').val(1);
                                if (json == "sent") {
                                    $('#lblInfo').html("OTP sent to mobile successfully.");
                                    $('#countdownLabelText').html('Please Enter your OTP in');
                                    $('#btnPAC').attr('disabled', 'disabled');
                                    StartTimer();
                                }
                                else if (json == "already sent") {
                                    $('#lblInfo').html("OTP already sent.");
                                    $('#btnPAC').attr('disabled', 'disabled');
                                    StartTimer();
                                }
                                else if (json == "sms locked") {
                                    $('#lblInfo').html("Too many attempts. Please try later.");
                                    $('#countdownLabelText').html('You can request new OTP in ');
                                    //$('#requestNewPinLinkDiv').addClass('hide');
                                    //$('#btnRequestNewPin').attr('disabled', 'disabled');
                                    $('#btnPAC').attr('disabled', 'disabled');
                                    $('#hdnMobilePinRequested').val(0);
                                    SMSLockStartTimer();
                                }
                                else {
                                    $('#lblInfo').html("No Account Registered with this Mobile Number.");
                                }

                            }
                        });
                    }
                    else {

                    }
                });

                $('.delete-notify').click(function () {
                    var id = $(this).data('id');
                    $('#SettingId').val(id);
                    $('#Delete_Setting').click();
                });

            });

        </script>
    </asp:Content>
