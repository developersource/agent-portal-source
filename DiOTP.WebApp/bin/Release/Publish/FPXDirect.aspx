﻿<%@ Page ValidateRequest="false" Language="C#" Debug="true" %>

<%@ Import Namespace="System" %>
                    <%@ Import Namespace="System.IO" %>
                    <%@ Import Namespace="System.Text" %>
                    <%@ Import Namespace="System.Security.Cryptography" %>
                    <%@ Import Namespace="System.Security.Cryptography.X509Certificates" %>
                    <%@ Import Namespace="System.Globalization" %>
                    <%@ Import Namespace="DiOTP.WebApp.FPXLibary" %>

<%
    Controller c = new Controller();
    String fpx_buyerBankBranch = Request.Form["fpx_buyerBankBranch"];
    String fpx_buyerBankId = Request.Form["fpx_buyerBankId"];
    String fpx_buyerIban = Request.Form["fpx_buyerIban"];
    String fpx_buyerId = Request.Form["fpx_buyerId"];
    String fpx_buyerName = Request.Form["fpx_buyerName"];
    String fpx_creditAuthCode = Request.Form["fpx_creditAuthCode"];
    String fpx_creditAuthNo = Request.Form["fpx_creditAuthNo"];
    String fpx_debitAuthCode = Request.Form["fpx_debitAuthCode"];
    String fpx_debitAuthNo = Request.Form["fpx_debitAuthNo"];
    String fpx_fpxTxnId = Request.Form["fpx_fpxTxnId"];
    String fpx_fpxTxnTime = Request.Form["fpx_fpxTxnTime"];
    String fpx_makerName = Request.Form["fpx_makerName"];
    String fpx_msgToken = Request.Form["fpx_msgToken"];
    String fpx_msgType = Request.Form["fpx_msgType"];
    String fpx_sellerExId = Request.Form["fpx_sellerExId"];
    String fpx_sellerExOrderNo = Request.Form["fpx_sellerExOrderNo"];
    String fpx_sellerId = Request.Form["fpx_sellerId"];
    String fpx_sellerOrderNo = Request.Form["fpx_sellerOrderNo"];
    String fpx_sellerTxnTime = Request.Form["fpx_sellerTxnTime"];
    String fpx_txnAmount = Request.Form["fpx_txnAmount"];
    String fpx_txnCurrency = Request.Form["fpx_txnCurrency"];
    String fpx_checkSum = Request.Form["fpx_checkSum"];
    String fpx_checkSumString = "";
    fpx_checkSumString = fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|" + fpx_creditAuthCode + "|" + fpx_creditAuthNo + "|" + fpx_debitAuthCode + "|" + fpx_debitAuthNo + "|" + fpx_fpxTxnId + "|" + fpx_fpxTxnTime + "|" + fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|";
    fpx_checkSumString += fpx_sellerExId + "|" + fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency;

    String mesg_type = Request.Form["mesg_type"];
    String mesgFromFpx = Request.Form["mesgFromFpx"];
    String mesg_token = Request.Form["mesg_token"];
    String key_type = Request.Form["key_type"];
    String seller_ex_desc = Request.Form["seller_ex_desc"];
    String seller_ex_id = Request.Form["seller_ex_id"];
    String order_no = Request.Form["order_no"];
    String seller_txn_time = Request.Form["seller_txn_time"];
    String seller_order_no = Request.Form["seller_order_no"];
    String seller_id = Request.Form["seller_id"];
    String seller_fpx_bank_code = Request.Form["seller_fpx_bank_code"];
    String buyer_mail_id = Request.Form["buyer_mail_id"];
    String txn_amt = Request.Form["txn_amt"];
    String checksum = Request.Form["checksum"];
    String debit_auth_code = Request.Form["debit_auth_code"];
    String debit_auth_no = Request.Form["debit_auth_no"];
    String buyer_bank = Request.Form["buyer_bank"];
    String buyer_bank_branch = Request.Form["buyer_bank_branch"];
    String buyer_name = Request.Form["buyer_name"];
    String FPX_TXN_ID = Request.Form["FPX_TXN_ID"];
    String finalVerifiMsg = "ERROR";
    String xmlMessage = "";
    xmlMessage = fpx_checkSumString;
    finalVerifiMsg = c.nvl_VerifiMsg(fpx_checkSumString, fpx_checkSum, Request.PhysicalApplicationPath); //Certificate Path
    //string Destfile = @"D:\Gowtham\FPX\Patch\Dotnet\NON IDE\AC_" + fpx_fpxTxnId + ".txt"; //path for writing response into a text file 
    //using (StreamWriter writer = new StreamWriter(Destfile, true))
    //{
    //    writer.WriteLine(fpx_checkSumString);
    //    writer.WriteLine(finalVerifiMsg);
    //}

%>
OK