using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class SiteContentRepo : ISiteContentRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(SiteContent obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<SiteContent>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<SiteContent> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<SiteContent>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(SiteContent obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<SiteContent>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<SiteContent> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<SiteContent>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public SiteContent DeleteData(Int32 Id)
        {
            Response responseSC = GetSingle(Id);
            if (responseSC.IsSuccess)
            {
                SiteContent obj = (SiteContent)responseSC.Data;
                try
                {
                    string query = obj.ObjectToQuery<SiteContent>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseSC = GetSingle(Id);
                    if (responseSC.IsSuccess)
                    {
                        SiteContent obj = (SiteContent)responseSC.Data;
                        query += obj.ObjectToQuery<SiteContent>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            SiteContent obj = new SiteContent();
            try
            {
                string query = "select * from site_content where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new SiteContent
                       {
                           Id = x.Field<Int32>("id"),
                           SiteContentTypeId = x.Field<Int32?>("site_content_type_id") == null ? 0 : x.Field<Int32>("site_content_type_id"),
                           Priority = x.Field<Int32>("priority"),
                           IsDisplay = x.Field<Int32>("is_display"),
                           IsContentDisplay = x.Field<Int32>("is_content_display"),
                           Title = x.Field<String>("title"),
                           TitleColor = x.Field<String>("title_color"),
                           SubTitle = x.Field<String>("sub_title"),
                           ShortDisplayContent = x.Field<String>("short_display_content"),
                           Content = x.Field<String>("content"),
                           ContentColor = x.Field<String>("content_color"),
                           ImageUrl = x.Field<String>("image_url"),
                           PublishDate = x.Field<DateTime>("publish_date"),
                           Status = x.Field<Int32>("status"),
                           CreatedBy = x.Field<Int32>("created_by"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                           UpdatedDate = x.Field<DateTime?>("updated_date"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<SiteContent> objs = new List<SiteContent>();
            try
            {
                string query = "select * from site_content";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new SiteContent
                        {
                            Id = x.Field<Int32>("id"),
                            SiteContentTypeId = x.Field<Int32?>("site_content_type_id") == null ? 0 : x.Field<Int32>("site_content_type_id"),
                            Priority = x.Field<Int32>("priority"),
                            IsDisplay = x.Field<Int32>("is_display"),
                            IsContentDisplay = x.Field<Int32>("is_content_display"),
                            Title = x.Field<String>("title"),
                            TitleColor = x.Field<String>("title_color"),
                            SubTitle = x.Field<String>("sub_title"),
                            ShortDisplayContent = x.Field<String>("short_display_content"),
                            Content = x.Field<String>("content"),
                            ContentColor = x.Field<String>("content_color"),
                            ImageUrl = x.Field<String>("image_url"),
                            PublishDate = x.Field<DateTime>("publish_date"),
                            Status = x.Field<Int32>("status"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from site_content";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<SiteContent>(propertyName);
            List<SiteContent> objs = new List<SiteContent>();
            try
            {
                string query = "select * from site_content where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from site_content where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new SiteContent
                        {
                            Id = x.Field<Int32>("id"),
                            SiteContentTypeId = x.Field<Int32?>("site_content_type_id") == null ? 0 : x.Field<Int32>("site_content_type_id"),
                            Priority = x.Field<Int32>("priority"),
                            IsDisplay = x.Field<Int32>("is_display"),
                            IsContentDisplay = x.Field<Int32>("is_content_display"),
                            Title = x.Field<String>("title"),
                            TitleColor = x.Field<String>("title_color"),
                            SubTitle = x.Field<String>("sub_title"),
                            ShortDisplayContent = x.Field<String>("short_display_content"),
                            Content = x.Field<String>("content"),
                            ContentColor = x.Field<String>("content_color"),
                            ImageUrl = x.Field<String>("image_url"),
                            PublishDate = x.Field<DateTime>("publish_date"),
                            Status = x.Field<Int32>("status"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<SiteContent>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from site_content where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from site_content where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<SiteContent> objs = new List<SiteContent>();
            try
            {
                string query = "select * from site_content where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new SiteContent
                        {
                            Id = x.Field<Int32>("id"),
                            SiteContentTypeId = x.Field<Int32?>("site_content_type_id") == null ? 0 : x.Field<Int32>("site_content_type_id"),
                            Priority = x.Field<Int32>("priority"),
                            IsDisplay = x.Field<Int32>("is_display"),
                            IsContentDisplay = x.Field<Int32>("is_content_display"),
                            Title = x.Field<String>("title"),
                            TitleColor = x.Field<String>("title_color"),
                            SubTitle = x.Field<String>("sub_title"),
                            ShortDisplayContent = x.Field<String>("short_display_content"),
                            Content = x.Field<String>("content"),
                            ContentColor = x.Field<String>("content_color"),
                            ImageUrl = x.Field<String>("image_url"),
                            PublishDate = x.Field<DateTime>("publish_date"),
                            Status = x.Field<Int32>("status"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
