﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentClients : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        public static List<UtmcFundInformation> UTMCFundInformations = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                }
            }
            if (Session["isVerified"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
            }
            if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
            {
                if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "1")
                {
                    User user = (User)(Session["user"]);
                    if (UTMCFundInformations == null)
                    {
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" 1=1 ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                        }
                    }
                    string query = (@" select u.* from agent_clients ac 
                                join users u on u.id = ac.user_id 
                                where ac.agent_user_id = '" + user.Id + "' and ac.is_active=1 and ac.status=1 ");

                    Response responseList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);
                    if (responseList.IsSuccess)
                    {
                        var UsersDyn = responseList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                        List<User> clients = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(responseJSON);
                        StringBuilder stringBuilder = new StringBuilder();
                        int index = 1;
                        if (clients.Count != 0)
                        {
                            clients.ForEach(x =>
                            {
                                Response responseUA = IUserAccountService.GetDataByFilter(" user_id='" + x.Id + "' ", 0, 0, false);
                                if (responseUA.IsSuccess)
                                {
                                    Decimal consolidatedTotalInvestmentRM = 0;
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUA.Data;
                                    userAccounts.ForEach(uA =>
                                    {
                                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                                        MaHolderReg maHolderReg = new MaHolderReg();
                                        if (responseMHR.IsSuccess)
                                        {
                                            maHolderReg = (MaHolderReg)responseMHR.Data;
                                            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = new List<UtmcDetailedMemberInvestment>();
                                            utmcDetailedMemberInvestments = ServicesManager.GetMAAccountSummary(uA.AccountNo, UTMCFundInformations);
                                            if (utmcDetailedMemberInvestments.Count > 0)
                                            {
                                                consolidatedTotalInvestmentRM += utmcDetailedMemberInvestments.Where(udmi => udmi.holderInv.CurrUnitHldg > 0).Sum(udmi => (udmi.holderInv.CurrUnitHldg * udmi.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice));
                                            }
                                        }
                                    });

                                    //Table Design

                                    stringBuilder.Append(
                                                    @"
                                            <tr class='" + "trTextFormat" + @"'>
                                            <td><b>" + index + @"</b></td>
                                            <td><b>" + x.Username + @"</b></td>
                                            <td><b>" + x.IdNo + @"</b></td>
                                            <td><b>" + x.MobileNumber + @"</b></td>
                                            <td><b class='currencyFormat'>" + consolidatedTotalInvestmentRM + @"</b></td>
                                            <td><a href='/ViewClient.aspx?id=" + x.Id + @"' class='btn btn-infos1'>View</a></td>
                                        </tr>");
                                    index++;
                                    //String MANos = String.Join(", ", userAccounts.Select(uA => uA.AccountNo).ToArray());
                                    //stringBuilder.Append(@"<div class='col-md-3 col-sm-12 col-xs-12'>
                                    //                        <div class='card hovercard'>
                                    //                            <div class='cardheader'>
                                    //                            </div>
                                    //                            <div class='avatar'>
                                    //                                <img alt='' src='/Content/MyImage/9.png'>
                                    //                            </div>
                                    //                            <div class='info'>
                                    //                                <div class='title'>
                                    //                                    <a target='_blank' href='javascript:;'>" + x.Username + @"</a>
                                    //                                </div>
                                    //                                <div class='desc'>" + x.IdNo + @"</div>
                                    //                                <div class='desc'>" + x.EmailId + @"</div>
                                    //                                <div class='desc'>" + x.MobileNumber + @"</div>
                                    //                                <div class='desc'>" + (x.Status == 1 ? "<span class='text text-success'>Active</span>" : "<span class='text text-danger'>Inactive</span>") + @"</div>
                                    //                                <div class='desc'><a href='/ViewClient.aspx?id=" + x.Id + @"' class='btn btn-infos1'>View</a></div>
                                    //                            </div>
                                    //                            <div class='bottom'>
                                    //                                <h5>Total Current Investment Value</h5>
                                    //                                <div><strong><span class='currencyFormat'>" + consolidatedTotalInvestmentRM + @"</span></strong></div>
                                    //                            </div>
                                    //                        </div>
                                    //                    </div>");
                                }
                            });
                        }
                        else {
                            stringBuilder.Append(" <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='6'><b>No data available for display.</b></td></tr>");
                        }
                        
                        tbodyAgentClientList.InnerHtml = stringBuilder.ToString();
                        //divClientsHTML.InnerHtml = stringBuilder.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseList.Message + "\", '');", true);
                    }
                }
            }
        }
    }
}