﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.WebApp.AccountOpening;

namespace DiOTP.AgentApp
{
    public partial class CompleteSignup : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAgentSignupService> lazyAgentSignupServiceObj = new Lazy<IAgentSignupService>(() => new AgentSignupService());
        public static IAgentSignupService IAgentSignupService { get { return lazyAgentSignupServiceObj.Value; } }

        private static readonly Lazy<IAgentPositionService> lazyAgentPositionObj = new Lazy<IAgentPositionService>(() => new AgentPositionService());
        public static IAgentPositionService IAgentPositionService { get { return lazyAgentPositionObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());
        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
            {
                if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "1")
                {
                    string status = Request.QueryString["status"];
                    string userId = Request.QueryString["userId"];
                    string agentId = Request.QueryString["agentId"];
                    string viewonly = Request.QueryString["viewonly"];
                    if (status != null && userId != null)
                    {
                        if (!IsPostBack)
                        {
                            string queryRegion = (@" select * from regions where is_active=1 and status=1 ");
                            Response responseRegionList = GenericService.GetDataByQuery(queryRegion, 0, 0, false, null, false, null, true);
                            if (responseRegionList.IsSuccess)
                            {
                                var RegionsDyn = responseRegionList.Data;
                                var responseRegionsJSON = JsonConvert.SerializeObject(RegionsDyn);
                                List<Region> regionsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Region>>(responseRegionsJSON);

                                ddlRegion.DataTextField = "name";
                                ddlRegion.DataValueField = "id";
                                ddlRegion.DataSource = regionsList;
                                ddlRegion.DataBind();

                                ddlRegion.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRegionList.Message + "\", '');", true);
                            }

                            string queryAO = (@" select * from agent_offices where is_active=1 and status=1 ");
                            Response responseAOList = GenericService.GetDataByQuery(queryAO, 0, 0, false, null, false, null, true);
                            if (responseAOList.IsSuccess)
                            {
                                var AOsDyn = responseAOList.Data;
                                var responseAOsJSON = JsonConvert.SerializeObject(AOsDyn);
                                List<AgentOffice> aosList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentOffice>>(responseAOsJSON);

                                aosList.ForEach(x =>
                                {
                                    ListItem listItem = new ListItem();
                                    listItem.Text = x.name;
                                    listItem.Value = x.id.ToString();
                                    listItem.Attributes.Add("region_id", x.region_id);
                                    ddlOffice.Items.Add(listItem);
                                });

                                //ddlOffice.DataTextField = "name";
                                //ddlOffice.DataValueField = "id";
                                //ddlOffice.DataSource = aosList;
                                //ddlOffice.DataBind();

                                ddlOffice.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAOList.Message + "\", '');", true);
                            }

                            Response resRaces = ServicesManager.GetRaceData();
                            if (resRaces.IsSuccess)
                            {
                                List<RaceDTO> raceDTOs = (List<RaceDTO>)resRaces.Data;
                                ddlRace.DataTextField = "SDESC";
                                ddlRace.DataValueField = "RACECODE";
                                ddlRace.DataSource = raceDTOs;
                                ddlRace.DataBind();

                                ddlRace.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + resRaces.Message + "\", '');", true);
                            }

                            Response responseCDList = ICountriesDefService.GetData(0, 0, false);
                            if (responseCDList.IsSuccess)
                            {
                                List<CountriesDef> countries = (List<CountriesDef>)responseCDList.Data;
                                ddlCountry.DataTextField = "Name";
                                ddlCountry.DataValueField = "Code";
                                ddlCountry.DataSource = countries;
                                ddlCountry.DataBind();

                                ddlCountry.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCDList.Message + "\", '');", true);
                            }

                            Response responseStates = IStatesDefService.GetData(0, 0, false);
                            if (responseStates.IsSuccess)
                            {
                                List<StatesDef> stateList = (List<StatesDef>)responseStates.Data;

                                ddlState.DataTextField = "Name";
                                ddlState.DataValueField = "Code";
                                ddlState.DataSource = stateList;
                                ddlState.DataBind();

                                ddlState.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseStates.Message + "\", '');", true);
                            }

                            Response responseNDList = INationalityDefService.GetData(0, 0, true);
                            if (responseNDList.IsSuccess)
                            {
                                List<NationalityDef> nds = (List<NationalityDef>)responseNDList.Data;
                                ddlNationality.DataTextField = "Name";
                                ddlNationality.DataValueField = "Code";
                                ddlNationality.DataSource = nds;
                                ddlNationality.DataBind();

                                ddlNationality.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNDList.Message + "\", '');", true);
                            }

                            Response resCurrencies = ServicesManager.GetCurrencyData();
                            if (resCurrencies.IsSuccess)
                            {
                                List<CurrencyDTO> currencyDTOs = (List<CurrencyDTO>)resCurrencies.Data;
                                ddlCurrency.DataTextField = "SDESC";
                                ddlCurrency.DataValueField = "CURRENCY";
                                ddlCurrency.DataSource = currencyDTOs;
                                ddlCurrency.DataBind();

                                ddlCurrency.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + resCurrencies.Message + "\", '');", true);
                            }

                            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + userId + "' and status='1' ", 0, 0, false);
                            Response responseUser = IUserService.GetSingle(Convert.ToInt32(userId));
                            if (responseUser.IsSuccess)
                            {
                                User agentUser = (User)responseUser.Data;

                                if (responseUAList.IsSuccess)
                                {
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                    UserAccount primaryAcc = userAccounts.FirstOrDefault(x => x.IsPrimary == 1);
                                    if (primaryAcc != null && primaryAcc.Id != 0)
                                    {
                                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                                        if (responseMHR.IsSuccess)
                                        {
                                            MaHolderReg maHolderReg = (MaHolderReg)responseMHR.Data;
                                            txtEmail.Text = agentUser.EmailId;
                                            txtAgentName.Text = maHolderReg.Name1;
                                            txtIDNo.Text = agentUser.IdNo;
                                            DateTime BDT = DateTime.ParseExact(maHolderReg.BirthDt, "M/d/yyyy", CultureInfo.InvariantCulture);
                                            txtDOB.Text = BDT.ToString("dd/MM/yyyy");
                                            ddlSex.SelectedValue = (maHolderReg.Sex == "M" ? "M" : (maHolderReg.Sex == "F" ? "F" : ""));
                                            ddlRace.SelectedValue = maHolderReg.Race;
                                            txtAddrLine1.Text = maHolderReg.Addr1;
                                            txtAddrLine2.Text = maHolderReg.Addr2;
                                            txtAddrLine3.Text = maHolderReg.Addr3;
                                            txtAddrLine4.Text = maHolderReg.Addr4;
                                            txtPostCode.Text = maHolderReg.Postcode.HasValue ? maHolderReg.Postcode.Value.ToString() : "";
                                            ddlCountry.SelectedValue = maHolderReg.CountryRes;
                                            ddlState.SelectedValue = maHolderReg.StateCode.HasValue ? maHolderReg.StateCode.Value.ToString() : "";
                                            txtTelNoOffice.Text = maHolderReg.OffPhoneNo;
                                            txtTelNoHome.Text = maHolderReg.TelNo;
                                            txtHandPhoneNo.Text = maHolderReg.HandPhoneNo;
                                            txtContactPerson.Text = maHolderReg.ContactPerson;
                                            txtIncomeTaxNo.Text = maHolderReg.IncomeTaxNo;
                                            ddlNationality.SelectedValue = maHolderReg.Nationality;
                                            ddlCurrency.SelectedValue = "RM";
                                            if (CustomValues.GetAccounPlan(primaryAcc.HolderClass) == "EPF")
                                            {
                                                txtEPFNo.Text = maHolderReg.IdNo2;
                                            }
                                            else
                                            {
                                                UserAccount epfAccount = userAccounts.FirstOrDefault(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF");
                                                if (epfAccount != null)
                                                {
                                                    Response responseMHREPF = ServicesManager.GetMaHolderRegByAccountNo(epfAccount.AccountNo);
                                                    if (responseMHREPF.IsSuccess)
                                                    {
                                                        MaHolderReg maHolderRegEPF = (MaHolderReg)responseMHREPF.Data;
                                                        txtEPFNo.Text = maHolderRegEPF.IdNo2;
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHREPF.Message + "\", '');", true);
                                                    }
                                                }
                                            }
                                            if (CustomValues.GetAccounPlan(primaryAcc.HolderClass) == "JOINT" && maHolderReg.JointRelationCode == "05")
                                            {
                                                ddlMaritalStatus.SelectedValue = "Married";
                                                txtSpouseName.Text = maHolderReg.Name2;
                                                txtSpouseIDNo.Text = maHolderReg.IdNo2;
                                            }
                                            else
                                            {
                                                UserAccount jointAccount = userAccounts.FirstOrDefault(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT");
                                                if (jointAccount != null)
                                                {
                                                    Response responseMHRJ = ServicesManager.GetMaHolderRegByAccountNo(jointAccount.AccountNo);
                                                    if (responseMHRJ.IsSuccess)
                                                    {
                                                        MaHolderReg maHolderRegJ = (MaHolderReg)responseMHRJ.Data;
                                                        ddlMaritalStatus.SelectedValue = "Married";
                                                        txtSpouseName.Text = maHolderRegJ.Name2;
                                                        txtSpouseIDNo.Text = maHolderRegJ.IdNo2;
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHRJ.Message + "\", '');", true);
                                                    }
                                                }
                                            }
                                            //txtEPFNo.Text = maHolderReg.epf
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Something went wrong.\", '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
                            }
                            //OLD


                            AgentReg agentRegExisting = new AgentReg();
                            string queryAgentReg = (@" select * from agent_regs where user_id=" + userId + " and process_status in (1, 2, 3, 19, 29, 39) and status=1 ");
                            Response responseAgentRegList = GenericService.GetDataByQuery(queryAgentReg, 0, 0, false, null, false, null, true);
                            if (responseAgentRegList.IsSuccess)
                            {
                                var agentsDyn = responseAgentRegList.Data;
                                var responseARegJSON = JsonConvert.SerializeObject(agentsDyn);
                                List<AgentReg> agentRegs = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseARegJSON);
                                if (agentRegs.Count == 1)
                                {
                                    agentRegExisting = agentRegs.FirstOrDefault();
                                    if (agentRegExisting.process_status == 2)
                                    {
                                        txtRecommendation.Enabled = false;
                                        btnSubmit.Visible = false;
                                    }
                                    Session["AgentRegExisting"] = agentRegExisting;
                                    chkIsCUTEPassedYes.Checked = (agentRegExisting.is_new_agent == 0);
                                    chkIsCUTEPassedNo.Checked = (agentRegExisting.is_new_agent == 1);

                                    chkRecommendedAgentYes.Checked = (agentRegExisting.introducer_code != "");
                                    chkRecommendedAgentNo.Checked = (agentRegExisting.introducer_code == "");
                                    txtIntroCode.Text = agentRegExisting.introducer_code;

                                    chkRegTypeUTS.Checked = (agentRegExisting.reg_type_uts == 1);
                                    chkRegTypePRS.Checked = (agentRegExisting.reg_type_prs == 1);

                                    ddlJobType.SelectedValue = agentRegExisting.job_type_id.ToString();
                                    ddlOffice.SelectedValue = agentRegExisting.office_id.ToString();
                                    ddlRegion.SelectedValue = ddlOffice.SelectedItem.Attributes["region_id"];



                                    string queryAgentRegPersonal = (@" select * from agent_reg_personal where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                                    Response responseAgentRegPersonalList = GenericService.GetDataByQuery(queryAgentRegPersonal, 0, 0, false, null, false, null, true);
                                    if (responseAgentRegPersonalList.IsSuccess)
                                    {
                                        var agentregPersonalsDyn = responseAgentRegPersonalList.Data;
                                        var responseAgentRegPersonalJSON = JsonConvert.SerializeObject(agentregPersonalsDyn);
                                        List<AgentRegPersonal> agentRegPersonals = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPersonal>>(responseAgentRegPersonalJSON);
                                        if (agentRegPersonals.Count == 1)
                                        {
                                            agentRegExisting.AgentRegPersonal = agentRegPersonals.FirstOrDefault();

                                            txtAgentName.Text = agentRegExisting.AgentRegPersonal.name;
                                            txtIDNo.Text = agentRegExisting.AgentRegPersonal.id_no;
                                            txtDOB.Text = agentRegExisting.AgentRegPersonal.dob.HasValue ? agentRegExisting.AgentRegPersonal.dob.Value.ToString("dd/MM/yyyy") : "";
                                            txtMotherMaidenName.Text = agentRegExisting.AgentRegPersonal.mother_maiden_name;
                                            ddlSex.SelectedValue = agentRegExisting.AgentRegPersonal.sex;
                                            ddlRace.SelectedValue = agentRegExisting.AgentRegPersonal.race;
                                            ddlReligion.SelectedValue = agentRegExisting.AgentRegPersonal.religion;
                                            ddlEducation.SelectedValue = agentRegExisting.AgentRegPersonal.highest_education;
                                            txtYearsOfExperience.Text = agentRegExisting.AgentRegPersonal.years_of_experience.ToString();
                                            txtAddrLine1.Text = agentRegExisting.AgentRegPersonal.mail_addr1;
                                            txtAddrLine2.Text = agentRegExisting.AgentRegPersonal.mail_addr2;
                                            txtAddrLine3.Text = agentRegExisting.AgentRegPersonal.mail_addr3;
                                            txtAddrLine4.Text = agentRegExisting.AgentRegPersonal.mail_addr4;
                                            txtPostCode.Text = agentRegExisting.AgentRegPersonal.post_code;
                                            ddlState.SelectedValue = agentRegExisting.AgentRegPersonal.state;
                                            ddlCountry.SelectedValue = agentRegExisting.AgentRegPersonal.country;
                                            txtTelNoOffice.Text = agentRegExisting.AgentRegPersonal.tel_no_office;
                                            txtTelNoHome.Text = agentRegExisting.AgentRegPersonal.tel_no_home;
                                            txtHandPhoneNo.Text = agentRegExisting.AgentRegPersonal.hand_phone;
                                            txtEmail.Text = agentRegExisting.AgentRegPersonal.email;
                                            txtContactPerson.Text = agentRegExisting.AgentRegPersonal.contact_person;
                                            txtIncomeTaxNo.Text = agentRegExisting.AgentRegPersonal.income_tax_no;
                                            txtEPFNo.Text = agentRegExisting.AgentRegPersonal.epf_no;
                                            txtSOCSO.Text = agentRegExisting.AgentRegPersonal.socso;
                                            ddlLanguage.SelectedValue = agentRegExisting.AgentRegPersonal.language;
                                            ddlNationality.SelectedValue = agentRegExisting.AgentRegPersonal.nationality;
                                            ddlMaritalStatus.SelectedValue = agentRegExisting.AgentRegPersonal.marital_status;
                                            chkBankruptcyYes.Checked = (agentRegExisting.AgentRegPersonal.bankruptcy_declaration != "");
                                            chkBankruptcyNo.Checked = (agentRegExisting.AgentRegPersonal.bankruptcy_declaration == "");
                                            txtBankruptcyDesc.Text = (chkBankruptcyYes.Checked ? agentRegExisting.AgentRegPersonal.bankruptcy_declaration : "");
                                            txtSpouseName.Text = agentRegExisting.AgentRegPersonal.spouse_name;
                                            txtSpouseIDNo.Text = agentRegExisting.AgentRegPersonal.spouse_id_no;

                                            string queryAgentRegPayment = (@" select * from agent_reg_payment_insurance where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                                            Response responseAgentRegPaymentList = GenericService.GetDataByQuery(queryAgentRegPayment, 0, 0, false, null, false, null, true);
                                            if (responseAgentRegPaymentList.IsSuccess)
                                            {
                                                var agentregPaymentsDyn = responseAgentRegPaymentList.Data;
                                                var responseAgentRegPaymentJSON = JsonConvert.SerializeObject(agentregPaymentsDyn);
                                                List<AgentRegPaymentInsurance> agentRegPaymentInsurances = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPaymentInsurance>>(responseAgentRegPaymentJSON);
                                                if (agentRegPaymentInsurances.Count == 1)
                                                {
                                                    agentRegExisting.AgentRegPaymentInsurance = agentRegPaymentInsurances.FirstOrDefault();

                                                    txtBeneficiaryName.Text = agentRegExisting.AgentRegPaymentInsurance.insurance_beneficiary_name;
                                                    txtBeneficiaryNRIC.Text = agentRegExisting.AgentRegPaymentInsurance.insurance_beneficiary_NRIC;


                                                    string queryAgentRegPaymentBanks = (@" select * from agent_reg_banks where agent_reg_payment_id=" + agentRegExisting.AgentRegPaymentInsurance.id + " and status=1 ");
                                                    Response responseAgentRegPaymentBanksList = GenericService.GetDataByQuery(queryAgentRegPaymentBanks, 0, 0, false, null, false, null, true);
                                                    if (responseAgentRegPaymentBanksList.IsSuccess)
                                                    {
                                                        var agentregPaymentBanksDyn = responseAgentRegPaymentList.Data;
                                                        var responseAgentRegPaymentBanksJSON = JsonConvert.SerializeObject(agentregPaymentBanksDyn);
                                                        List<AgentRegBank> agentRegPaymentBanks = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegBank>>(responseAgentRegPaymentBanksJSON);
                                                        agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks = agentRegPaymentBanks;

                                                        AgentRegBank agentRegBank = agentRegPaymentBanks.FirstOrDefault(x => x.bank_type == 1);
                                                        if (agentRegBank != null)
                                                        {
                                                            ddlBankAccountType.SelectedValue = agentRegBank.bank_account_type;
                                                            txtBankName.Text = agentRegBank.bank_name;
                                                            txtBankAccNo.Text = agentRegBank.account_no;
                                                            ddlPaymentMode.SelectedValue = agentRegBank.payment_mode;
                                                        }

                                                        AgentRegBank agentRegBankI = agentRegPaymentBanks.FirstOrDefault(x => x.bank_type == 2);
                                                        if (agentRegBankI != null)
                                                        {
                                                            ddlCurrencyI.SelectedValue = agentRegBankI.currency;
                                                            txtPaymentICI.Text = agentRegBankI.id_no;
                                                            ddlBankAccountTypeI.SelectedValue = agentRegBankI.bank_account_type;
                                                            txtBankNameI.Text = agentRegBankI.bank_name;
                                                            txtBankAccNoI.Text = agentRegBankI.account_no;
                                                            ddlPaymentModeI.SelectedValue = agentRegBankI.payment_mode;
                                                        }
                                                    }
                                                    else
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPaymentBanksList.Message + "\", '/Index.aspx');", true);


                                                    string queryAgentExp = (@" select * from agent_reg_exps where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                                                    Response responseAgentExpList = GenericService.GetDataByQuery(queryAgentExp, 0, 0, false, null, false, null, true);
                                                    if (responseAgentExpList.IsSuccess)
                                                    {
                                                        var agentExpDyn = responseAgentExpList.Data;
                                                        var responseAgentExpsJSON = JsonConvert.SerializeObject(agentExpDyn);
                                                        List<AgentRegExp> agentRegPExps = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegExp>>(responseAgentExpsJSON);
                                                        if (agentRegPExps.Count == 1)
                                                        {
                                                            agentRegExisting.AgentRegExp = agentRegPExps.FirstOrDefault();

                                                            txtPreviousEmployerName.Text = agentRegExisting.AgentRegExp.employer_name;
                                                            txtPositionHeld.Text = agentRegExisting.AgentRegExp.position_held;
                                                            txtLengthOfService.Text = agentRegExisting.AgentRegExp.length_of_service_yrs.ToString();
                                                            ddlAnnualIncome.SelectedValue = agentRegExisting.AgentRegExp.annual_income;
                                                            txtReasonEmp.Text = agentRegExisting.AgentRegExp.reason_for_leaving;
                                                        }

                                                        string query = " select * from agent_reg_files where agent_reg_id = '" + agentRegExisting.id + "' and status = 1 ";
                                                        Response responseARFist = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                                                        if (responseARFist.IsSuccess)
                                                        {
                                                            var agentFilesDyn = responseARFist.Data;
                                                            var responseAgentFilesJSON = JsonConvert.SerializeObject(agentFilesDyn);
                                                            List<Utility.AgentRegFile> agentRegFiles = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegFile>>(responseAgentFilesJSON);

                                                            AgentRegFile agentRegFileNRICFront = agentRegFiles.FirstOrDefault(x => x.file_type == 1);
                                                            AgentRegFile agentRegFileNRICBack = agentRegFiles.FirstOrDefault(x => x.file_type == 2);
                                                            AgentRegFile agentRegFileSelfie = agentRegFiles.FirstOrDefault(x => x.file_type == 3);
                                                            AgentRegFile agentRegFilePOP = agentRegFiles.FirstOrDefault(x => x.file_type == 4);
                                                            AgentRegFile agentRegFileQualification = agentRegFiles.FirstOrDefault(x => x.file_type == 5);
                                                            AgentRegFile agentRegFileFiMMCard = agentRegFiles.FirstOrDefault(x => x.file_type == 6);
                                                            AgentRegFile agentRegFileResult = agentRegFiles.FirstOrDefault(x => x.file_type == 7);
                                                            if (agentRegFileNRICFront != null)
                                                            {
                                                                uploadNRICFrontPageImage.InnerHtml = "<img src='" + agentRegFileNRICFront.url + "' alt='Selfie with NRIC' title='" + agentRegFileNRICFront.name + "' style='height: 100%'/>";
                                                                uploadNRICFrontPageImageLink.InnerHtml = "<a href='" + agentRegFileNRICFront.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileNRICFront.name + "</a>";
                                                            }
                                                            if (agentRegFileNRICBack != null)
                                                            {
                                                                uploadNRICBackPageImage.InnerHtml = "<img src='" + agentRegFileNRICBack.url + "' alt='Selfie with NRIC' title='" + agentRegFileNRICBack.name + "' style='height: 100%'/>";
                                                                uploadNRICBackPageImageLink.InnerHtml = "<a href='" + agentRegFileNRICBack.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileNRICBack.name + "</a>";
                                                            }
                                                            if (agentRegFileSelfie != null)
                                                            {
                                                                uploadSelfieImage.InnerHtml = "<img src='" + agentRegFileSelfie.url + "' alt='Selfie with NRIC' title='" + agentRegFileSelfie.name + "' style='height: 100%'/>";
                                                                uploadSelfieImageLink.InnerHtml = "<a href='" + agentRegFileSelfie.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileSelfie.name + "</a>";
                                                            }
                                                            if (agentRegFilePOP != null)
                                                            {
                                                                uploadPOPImage.InnerHtml = "<img src='" + agentRegFilePOP.url + "' alt='Selfie with NRIC' title='" + agentRegFilePOP.name + "' style='height: 100%'/>";
                                                                uploadPOPImageLink.InnerHtml = "<a href='" + agentRegFilePOP.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFilePOP.name + "</a>";
                                                            }
                                                            if (agentRegFileQualification != null)
                                                            {
                                                                uploadQualificationImage.InnerHtml = "<img src='" + agentRegFileQualification.url + "' alt='Selfie with NRIC' title='" + agentRegFileQualification.name + "' style='height: 100%'/>";
                                                                uploadQualificationImageLink.InnerHtml = "<a href='" + agentRegFileQualification.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileQualification.name + "</a>";
                                                            }
                                                            if (agentRegFileFiMMCard != null)
                                                            {
                                                                uploadFiMMCardImage.InnerHtml = "<img src='" + agentRegFileFiMMCard.url + "' alt='Selfie with NRIC' title='" + agentRegFileFiMMCard.name + "' style='height: 100%'/>";
                                                                uploadFiMMCardImageLink.InnerHtml = "<a href='" + agentRegFileFiMMCard.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileFiMMCard.name + "</a>";
                                                            }
                                                            if (agentRegFileResult != null)
                                                            {
                                                                uploadResultImage.InnerHtml = "<img src='" + agentRegFileResult.url + "' alt='Selfie with NRIC' title='" + agentRegFileResult.name + "' style='height: 100%'/>";
                                                                uploadResultImageLink.InnerHtml = "<a href='" + agentRegFileResult.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileResult.name + "</a>";
                                                            }

                                                            txtRecommendation.Text = agentRegExisting.recommendation;

                                                        }
                                                        else
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseARFist.Message + "\", '/Index.aspx');", true);

                                                    }
                                                    else
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentExpList.Message + "\", '/Index.aspx');", true);
                                                }
                                            }
                                            else
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPaymentList.Message + "\", '/Index.aspx');", true);
                                        }
                                    }
                                    else
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPersonalList.Message + "\", '/Index.aspx');", true);
                                }
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegList.Message + "\", '/Index.aspx');", true);
                        }
                    }
                }
                else if (Session["isVerified"].ToString() != "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Not verified.\", '/AgentSignups.aspx');", true);
                }
                else if (Session["isAgent"].ToString() != "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Not agent.\", '/AgentSignups.aspx');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Session expired.\", '/Index.aspx');", true);
            }
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetSessionDetails(Int32 SessionId)
        {
            Response response = new Response() { IsSuccess = true };
            try
            {
                string mainQ = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status,group_concat(exam_datetime) as exam_datetimes FROM agent_exam_schedules where exam_datetime > '" + DateTime.Now.ToString("yyyy-MM-dd") + "' and is_active=1 and status=1 and id=" + SessionId + " ");
                Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 30, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var examsDyn = responseUList.Data;
                    var responseexamsDynJSON = JsonConvert.SerializeObject(examsDyn);
                    List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseexamsDynJSON);
                    CustomAgentExamSchedule customAgentExamSchedule = agentExamSchedules.FirstOrDefault();
                    response.Data = customAgentExamSchedule;
                }
                else
                {
                    response = responseUList;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object AddExamAttended(Int32 SessionId, Int32 FirstTime, String ExamResitDateAttended, String ExamResultAttended)
        {
            User user = (User)(HttpContext.Current.Session["user"]);
            AgentReg agentReg = (AgentReg)HttpContext.Current.Session["AgentRegExisting"];
            Response response = new Response() { IsSuccess = true };
            try
            {
                if (user != null && agentReg != null)
                {
                    string mainQ = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status,group_concat(exam_datetime) as exam_datetimes FROM agent_exam_schedules where is_active=1 and status=1 and id=" + SessionId + " ");
                    Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 30, false, null, false, null, true);
                    if (responseUList.IsSuccess)
                    {
                        var examsDyn = responseUList.Data;
                        var responseexamsDynJSON = JsonConvert.SerializeObject(examsDyn);
                        List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseexamsDynJSON);
                        CustomAgentExamSchedule customAgentExamSchedule = agentExamSchedules.FirstOrDefault();

                        AgentRegExam agentRegExam = new AgentRegExam
                        {
                            session_id = SessionId.ToString(),
                            agent_reg_id = agentReg.id,
                            language = customAgentExamSchedule.language,
                            location = customAgentExamSchedule.location,
                            exam_date = customAgentExamSchedule.exam_datetime,
                            is_first_time = FirstTime,
                            resit_date = ExamResitDateAttended == "" ? default(DateTime) : DateTime.ParseExact(ExamResitDateAttended, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                            result = ExamResultAttended,
                            is_active = 1,
                            created_by = user.Id,
                            created_date = DateTime.Now,
                            status = 1
                        };

                        Response responsePost = GenericService.PostData<AgentRegExam>(agentRegExam);
                        if (responsePost.IsSuccess)
                        {
                            agentRegExam = (AgentRegExam)responsePost.Data;

                            string queryAgentRegExam = (@" select * from agent_reg_exams where agent_reg_id=" + agentReg.id + " and status=1 ");
                            Response responseAgentRegExamList = GenericService.GetDataByQuery(queryAgentRegExam, 0, 0, false, null, false, null, true);
                            if (responseAgentRegExamList.IsSuccess)
                            {
                                var agentregExamDyn = responseAgentRegExamList.Data;
                                var responseAgentRegExamsJSON = JsonConvert.SerializeObject(agentregExamDyn);
                                List<AgentRegExam> agentRegExams = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegExam>>(responseAgentRegExamsJSON);
                                StringBuilder stringBuilder = new StringBuilder();
                                if (agentRegExams.Count > 0)
                                {
                                    int index = 1;
                                    agentRegExams.ForEach(are =>
                                    {
                                        stringBuilder.Append(@"<tr>
                                                                    <td><span style='line-height: 44px;'>" + index + @"</span></td>
                                                                    <td>" + are.session_id + @"</td>
                                                                    <td>" + are.language + @"</td>
                                                                    <td>" + are.location + @"</td>
                                                                    <td>" + (are.exam_date.HasValue ? are.exam_date.Value.ToString("dd/MM/yyyy HH:mm:ss") : "") + @"</td>
                                                                    <td>" + (are.is_first_time.HasValue ? (are.is_first_time.Value == 0 ? "No" : "Yes") : "-") + @"</td>
                                                                    <td>" + (are.resit_date.HasValue ? are.resit_date.Value.ToString("dd/MM/yyyy HH:mm:ss") : "") + @"</td>
                                                                    <td>" + are.result + @"</td>
                                                                    <td>-</td>
                                                                </tr>");
                                        index++;
                                    });
                                }
                                else
                                {
                                    stringBuilder.Append(@"<tr>
                                                                                                            <td colspan='9'>No data</td>
                                                                                                        </tr>");
                                }
                                response.Data = stringBuilder.ToString();
                            }
                            else
                            {
                                response = responseAgentRegExamList;
                            }
                        }
                        else
                        {
                            response = responsePost;
                        }
                    }
                    else
                    {
                        response = responseUList;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired!";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)(Session["user"]);
                AgentReg agentReg = (AgentReg)Session["AgentRegExisting"];
                if (user != null && agentReg != null)
                {
                    Response responseSignupUser = IUserService.GetSingle(agentReg.user_id);
                    if (responseSignupUser.IsSuccess)
                    {
                        User signupUser = (User)responseSignupUser.Data;

                        Response resGet = GenericService.GetSingle<AgentReg>(agentReg.id, true, null, false);
                        if (resGet.IsSuccess)
                        {
                            AgentReg agentRegDB = (AgentReg)resGet.Data;
                            agentRegDB.recommendation = txtRecommendation.Text;
                            agentRegDB.recommendation_date = DateTime.Now;
                            agentRegDB.process_status = 2;
                            agentRegDB.updated_date = DateTime.Now;
                            agentRegDB.updated_by = user.Id;
                            Response responseAgentreg = GenericService.UpdateData<AgentReg>(agentRegDB);
                            if (responseAgentreg.IsSuccess)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Recommendation submitted successfull\", '/AgentSignups.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentreg.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + resGet.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseSignupUser.Message + "\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Session Expired!\", '');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}