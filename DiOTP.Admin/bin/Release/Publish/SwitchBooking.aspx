﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SwitchBooking.aspx.cs" Inherits="Admin.SwitchBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="redemptionBookingForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Switch Booking Rejected" data-status="Reject" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Deactivate" data-isconfirm="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Switch Booking Approved" data-status="Approve" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Activate" data-isconfirm="0" data-isreason="1" data-reasonLabel="Start Date" data-reasonInput="date"><i class="fa fa-check"></i></button>
                                                <span class="common-divider pull-right"></span>
                                                <button data-original-title="Add" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Add" data-url="AddSwitchBooking.aspx" data-title="Add Switch Booking"><i class="fa fa-plus"></i></button>
                                                <button data-original-title="Edit" data-trigger="hover" data-placement="bottom" data-content="Select one record" type="button" class="popovers btn action-button pull-right" data-action="Edit" data-url="AddSwitchBooking.aspx" data-title="Edit Switch Booking"><i class="fa fa-edit"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can search here with MA Acc or Agent Code</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can Edit, Add, Approve and Reject</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />                                        
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="All">All</button>
                                        <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="2">Pending</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="1">Approved</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="0">Rejected</button>
                                        <small>
                                            <br />
                                            <strong>Note:</strong>
                                            <span>Status Filtering on Bookings</span>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table  id="bookingTable" class="table table-bordered dataTable nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <%--<div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>--%>
                                                S.No
                                            </th>
                                            <th>MA Acc</th>
                                            <th>Client Name</th>
                                            <th>Scheme</th>
                                            <th>Gross Amt</th>
                                            <th>Sales Charge</th>
                                            <th>Nett Amt</th>
                                            <th>NAV</th>
                                            <th>Units</th>
                                            <th>Servicing Agent</th>
                                            <th>Remarks</th>
                                            <th>Payment Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bookingTbody" runat="server"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <script>

    </script>
</body>
</html>
