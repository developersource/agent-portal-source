﻿using DiOTP.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility;
using DiOTP.Data.IRepo;
using DiOTP.Data;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Service
{
    public class SampleService : ISampleService
    {
        public int DeleteBulkData(List<int> Ids)
        {
            throw new NotImplementedException();
        }

        public User DeleteData(int Id)
        {
            throw new NotImplementedException();
        }

        public int GetCount()
        {
            throw new NotImplementedException();
        }

        public int GetCountByFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public int GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            throw new NotImplementedException();
        }

        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            throw new NotImplementedException();
        }

        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            throw new NotImplementedException();
        }
        
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            throw new NotImplementedException();
        }

        public Response GetSingle(int Id)
        {
            throw new NotImplementedException();
        }

        public int PostBulkData(List<User> objs)
        {
            throw new NotImplementedException();
        }

        public Response PostData(User obj)
        {
            throw new NotImplementedException();
        }

        public int UpdateBulkData(List<User> objs)
        {
            throw new NotImplementedException();
        }

        public Response UpdateData(User obj)
        {
            throw new NotImplementedException();
        }
    }
}
