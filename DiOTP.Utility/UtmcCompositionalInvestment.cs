﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_compositional_investment")]
    public class UtmcCompositionalInvestment
    {
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("member_epf_no"), Display("MemberEPFNo")]
        public String MemberEpfNo { get; set; }
        [Database("ipd_member_acc_no"), Display("IpdMemberAccNo")]
        public String IpdMemberAccNo { get; set; }
        [Database("effective_date"), Display("EffectiveDate")]
        public DateTime? EffectiveDate { get; set; }
        [Database("opening_balance_units"), Display("OpeningBalanceUnits")]
        public Decimal? OpeningBalanceUnits { get; set; }
        [Database("opening_balance_cost_rm"), Display("OpeningBalanceCostRm")]
        public Decimal? OpeningBalanceCostRm { get; set; }
        [Database("opening_balance_date"), Display("OpeningBalanceDate")]
        public DateTime OpeningBalanceDate { get; set; }
        [Database("net_cumulative_closing_balance_units"), Display("NetCumulativeClosingBalanceUnits")]
        public Decimal? NetCumulativeClosingBalanceUnits { get; set; }
        [Database("net_cumulative_closing_balance_cost_rm"), Display("NetCumulativeClosingBalanceCostRm")]
        public Decimal? NetCumulativeClosingBalanceCostRm { get; set; }
        [Database("net_cumulative_closing_balance_date"), Display("NetCumulativeClosingBalanceDate")]
        public DateTime? NetCumulativeClosingBalanceDate { get; set; }
        [Database("market_price_nav"), Display("MarketPriceNav")]
        public Decimal? MarketPriceNav { get; set; }
        [Database("market_price_effective_date"), Display("MarketPriceEffectiveDate")]
        public DateTime? MarketPriceEffectiveDate { get; set; }
        [Database("unrealised_gain_loss_rm"), Display("UnrealisedGainLossRm")]
        public Decimal? UnrealisedGainLossRm { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime? ReportDate { get; set; }
        [Database("report_key"), Display("ReportKey")]
        public String ReportKey { get; set; }
        [Database("isactive"), Display("Isactive")]
        public String Isactive { get; set; }
    }
}
