using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_fund_categories_def")]
    public class UtmcFundCategoriesDef
    {
        public List<UtmcFundInformation> UtmcFundCategoriesDefIdUtmcFundInformations { get; set; }
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
