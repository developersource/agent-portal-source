﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class AgentRequest
    {
        public String Name { get; set; }
        public String IdNo { get; set; }
        public Int32 JobTypeID { get; set; }
        public String AgentOfficeBranch { get; set; }
        public DateTime AgentStatusDate { get; set; }
        public DateTime RankDate { get; set; }
        public DateTime UTMCJoiningDate { get; set; }
        public Int32 AgentStatus { get; set; }
        public String Region { get; set; }
        public String NRICNew { get; set; }
        public String NRICOld { get; set; }
        public DateTime DateofBirth { get; set; }
        public String MotherMaidenName { get; set; }
        public String Sex { get; set; }
        public String Race { get; set; }
        public String Religion { get; set; }
        public String HighestEducation { get; set; }
        public String OfficeTelephone { get; set; }
        public String TelephoneNo { get; set; }
        public String HandphoneNo { get; set; }
        public String Email { get; set; }
        public String ContactPerson { get; set; }
        public String IncomeTaxNo { get; set; }
        public String EPFNo { get; set; }
        public String SOCSO { get; set; }
        public String EntryDate { get; set; }
        public String Language { get; set; }
        public String Nationality { get; set; }
        public String MaritalStatus { get; set; }
        public String BankruptcyDeclaration { get; set; }
        public String SpouseName { get; set; }
        public String SpouseICNew { get; set; }
        public String SpouseICOld { get; set; }
        public String Currency { get; set; }
        public String InsuranceId { get; set; }
        public String IC { get; set; }
        public String BankName { get; set; }
        public String BranchName { get; set; }
        public Int32 AccountType { get; set; }
        public String AccountNo { get; set; }
        public String PaymentMode { get; set; }
        public String InterCurrency { get; set; }
        public String InterPassport { get; set; }
        public String InterBankName { get; set; }
        public String InterBranchName { get; set; }
        public Int32 InterAccountType { get; set; }
        public String InterAccountNo { get; set; }
        public String InterPaymentMode { get; set; }
        public String InsuranceBeneficiaryName { get; set; }
        public String InsuranceBeneficiaryNRIC { get; set; }
        public String PreviousEmployerName { get; set; }
        public String PositionHeld { get; set; }
        public Int32 LengthOfService { get; set; }
        public String AnnualIncome { get; set; }
        public String ReasonForLeaving { get; set; }
    }
}
