﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class Login : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static User user = new User();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["rmbMe"] != null)
                {
                    txtUsername.Text = Request.Cookies["rmbMe"].Value;
                    rememberMe.Checked = true;
                }
                else {
                    rememberMe.Checked = false;
                }
            }
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUsername.Text) && !string.IsNullOrEmpty(txtPassword.Text))
            {
                Response responseUList = new Response();
                if (DiOTP.Utility.Helper.CustomValidator.IsValidEmail(txtUsername.Text))
                {
                    responseUList = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.EmailId), txtUsername.Text, true, 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        user = ((List<User>)responseUList.Data).FirstOrDefault();
                    }
                    else
                    {
                        Response.Write("<script>alert(\"" + responseUList.Message + "\");</script>");
                    }
                }
                else
                {
                    responseUList = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.Username), txtUsername.Text, true, 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        user = ((List<User>)responseUList.Data).FirstOrDefault();
                    }
                    else
                    {
                        Response.Write("<script>alert(\"" + responseUList.Message + "\");</script>");
                    }
                }                
               
                if (responseUList.IsSuccess) {                  
                    LoginUser(user, txtPassword.Text, Request, hdnLocalIPAddress.Value);
                }

            }
        }

        public void LoginUser(User user, string password, HttpRequest Request, String localIP)
        {
            if (user != null)
            {
                if (user.Password == CustomEncryptorDecryptor.EncryptPassword(password))
                {
                    if (user.UserRoleId == 1 || user.UserRoleId == 2)
                    {
                        Response responseUTList = IUserTypeService.GetDataByPropertyName(nameof(UserType.UserId), user.Id.ToString(), true, 0, 0, false);
                        if (responseUTList.IsSuccess)
                        {
                            List<UserType> userTypes = (List<UserType>)responseUTList.Data;
                            int isSuperAdmin = 0;
                            if (user.UserRoleId == 1)
                                isSuperAdmin = 1;
                            int isMainAdmin = 0;
                            if (user.UserRoleId == 2)
                            {
                                if (userTypes.Count > 0)
                                {
                                    UserType userType = userTypes.FirstOrDefault();
                                    if (userType.UserTypeId == 3)
                                    {
                                        isMainAdmin = 1;
                                    }
                                }

                            }
                            int isInternalStaff = 0;

                            int isContentAdmin = 0;
                            int isAccountAdmin = 0;

                            int isAdminOfficer = 0;
                            int isAdminManager = 0;

                            if (userTypes.Count == 1)
                            {
                                UserType userType = userTypes.FirstOrDefault();
                                if (userType.UserTypeId == 3)
                                    isMainAdmin = 1;
                                if (userType.UserTypeId == 4)
                                    isAccountAdmin = 1;
                                if (userType.UserTypeId == 5)
                                    isContentAdmin = 1;
                                if (userType.UserTypeId == 10)
                                    isAdminOfficer = 1;
                                if (userType.UserTypeId == 11)
                                    isAdminManager = 1;
                                if (userType.UserTypeId == 13)//include other staff except for admin
                                    isInternalStaff = 1;
                            }
                            else if (userTypes.Count == 2)
                            {
                                UserType userType1 = userTypes[0];
                                UserType userType2 = userTypes[1];
                                if (userType1.UserTypeId == 3)
                                    isMainAdmin = 1;
                                if (userType1.UserTypeId == 4)
                                    isAccountAdmin = 1;
                                if (userType1.UserTypeId == 5)
                                    isContentAdmin = 1;
                                if (userType1.UserTypeId == 10)
                                    isAdminOfficer = 1;
                                if (userType1.UserTypeId == 11)
                                    isAdminManager = 1;
                            }
                            if (isSuperAdmin == 1 || isMainAdmin == 1 || isContentAdmin == 1 || isAccountAdmin == 1 || isAdminOfficer == 1 || isAdminManager == 1)
                            {
                                Session["admin"] = user;
                                Session["isSuperAdmin"] = isSuperAdmin;
                                Session["isMainAdmin"] = isMainAdmin;
                                Session["isContentAdmin"] = isContentAdmin;
                                Session["isAccountAdmin"] = isAccountAdmin;
                                Session["isAdminOfficer"] = isAdminOfficer;
                                Session["isAdminManager"] = isAdminManager;
                                Session["LastLoginIP"] = user.LastLoginIp;
                                Session["LastLoginTime"] = user.LastLoginDate;
                                User updateUser = user;
                                updateUser.IsLoginLocked = 0;
                                updateUser.LoginAttempts = 0;
                                updateUser.LastLoginDate = DateTime.Now;
                                updateUser.LastLoginIp = TrackIPAddress.GetUserPublicIP(localIP) + ", " + localIP;
                                IUserService.UpdateData(updateUser);
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Admin Login Successful",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = user.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }

                                //create cookie upon succesful login - each succesful login will change cookie username value based on logged in user
                                if (rememberMe.Checked)
                                {
                                    Response.Cookies["rmbMe"].Expires = DateTime.Now.AddDays(99999);
                                    Response.Cookies["rmbMe"].Value = txtUsername.Text.Trim();
                                }
                                else {
                                    //removes cookie when remember me checkbox is unchecked
                                    Response.Cookies["rmbMe"].Expires = DateTime.Now.AddDays(-99998);

                                }

                                //Audit Log Ends here
                                Response.Redirect("Default.aspx");
                            }
                            else if(isInternalStaff == 1 || isSuperAdmin == 1)
                            {
                                Session["Staff"] = user;
                                Session["isInternalStaff"] = isInternalStaff;
                                Session["LastLoginIP"] = user.LastLoginIp;
                                Session["LastLoginTime"] = user.LastLoginDate;
                                Session["isSuperAdmin"] = isSuperAdmin;

                                User updateUser = user;
                                updateUser.IsLoginLocked = 0;
                                updateUser.LoginAttempts = 0;
                                updateUser.LastLoginDate = DateTime.Now;
                                updateUser.LastLoginIp = TrackIPAddress.GetUserPublicIP(localIP) + ", " + localIP;
                                IUserService.UpdateData(updateUser);
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Internal Staff Login Successful",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = user.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }

                                //create cookie upon succesful login - each succesful login will change cookie username value based on logged in user
                                if (rememberMe.Checked)
                                {
                                    Response.Cookies["rmbMe"].Expires = DateTime.Now.AddDays(99999);
                                    Response.Cookies["rmbMe"].Value = txtUsername.Text.Trim();
                                }
                                else
                                {
                                    //removes cookie when remember me checkbox is unchecked
                                    Response.Cookies["rmbMe"].Expires = DateTime.Now.AddDays(-99998);

                                }

                                //Audit Log Ends here
                                Response.Redirect("DefaultInternalStaff.aspx");
                            }
                            else
                            {
                                Response.Write("<script>alert('User Type not Authorized.');</script>");
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert(\"" + responseUTList.Message + "\");</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Not Authorized.');</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Password incorrect.');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Wrong username or password.');</script>");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                WebControl.DisabledCssClass = "";
                if (txtEmail.Text != null && txtEmail.Text != "")
                {
                    String emailId = txtEmail.Text;
                    string siteURL = ConfigurationManager.AppSettings["siteURL"];
                    Response responseUList = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.EmailId), emailId, true, 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        User user = ((List<User>)responseUList.Data).FirstOrDefault();
                        if (user != null && user.UserRoleId == 2)
                        {
                            Email email = new Email();
                            email.user = user;

                            var unique_code = CustomGenerator.GenerateSixDigitPin();
                            user.EmailCode = unique_code;

                            user.IsSecurityChecked = 1;
                            user.ModifiedDate = DateTime.Now;
                            IUserService.UpdateData(user);
                            
                            email.link = siteURL + "Admin/SetAdminPassword.aspx";
                            EmailService.SendResetPasswordLink(email, unique_code);
                            lblInfo.InnerHtml = "Reset link send to Email successfully.";
                            btnSubmit.Enabled = false;
                            btnCancel.Enabled = false;
                            btnSubmit.Attributes.Add("class", "btn btn-success btn-disabled");
                            btnCancel.Attributes.Add("class", "btn btn-default btn-disabled");
                        }
                        else
                            lblInfo.InnerHtml = "No Account Registered with this Email.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "OpenFPModal();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUList.Message + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "OpenFPModal();", true);
                    lblInfo.InnerHtml = "Email cannot be empty";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("ForgotPassword Page btnEmailSubmit_Click: " + ex.Message);
            }
        }

    }
}