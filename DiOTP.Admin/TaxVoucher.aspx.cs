﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class TaxVoucher : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserOrderStatementService> lazyUserStatementServiceObj = new Lazy<IUserOrderStatementService>(() => new UserOrderStatementService());
        public static IUserOrderStatementService IUserOrderStatementService { get { return lazyUserStatementServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append(" 1=1 ");
                filter.Append(" and status=1 ");
                filter.Append(" and user_order_statement_type_id=4");
                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IUserOrderStatementService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                StringBuilder asb = new StringBuilder();
                int index = 1;
                Response responseUOSList = IUserOrderStatementService.GetDataByFilter("status=1 and user_order_statement_type_id=4", 0, 0, true);
                if (responseUOSList.IsSuccess)
                {
                    List<UserOrderStatement> userorderstatements = ((List<UserOrderStatement>)responseUOSList.Data).ToList();

                    foreach (UserOrderStatement uos in userorderstatements)
                    {
                        Response response = IUserService.GetSingle(uos.UserId);
                        if (response.IsSuccess)
                        {
                            User user = (User)response.Data;

                            Response response2 = IUserAccountService.GetSingle(uos.UserAccountId);
                            if (response2.IsSuccess)
                            {
                                UserAccount userAccount = (UserAccount)response2.Data;

                                asb.Append(@"<tr>
                                            <td>" + index + @"</td>
                                            <td>" + user.Username + @"</td>
                                            <td>" + userAccount.AccountNo + @"</td>
                                            <td>" + DateTime.ParseExact(uos.RefNo, "yyyyMMdd", null).ToString("dd/MM/yyyy") + @"</td>
                                            <td>" + uos.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                            <td><a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text - info' href='" + uos.Url + @"' target='_blank'><i class='fa fa-eye'></i></a></td>
                                         </tr>
                                     ");
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + response2.Message + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + response.Message + "');", true);
                        }
                        index++;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert(\"" + responseUOSList.Message + "\");", true);
                }
                index++;
                taxVoucherTbody.InnerHtml = asb.ToString();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object Upload(TaxVouchers[] statements)
        {
            Response response = new Response();
            List<string> responseMsgs = new List<string>();

            try
            {
                foreach (TaxVouchers tv in statements)
                {
                    string ex = Path.GetExtension(tv.Name).ToUpper();
                    if (ex == ".PDF")
                    {
                        string[] keys = tv.Name.Split('_');
                        int index = 3;

                        if (index == keys.Length)
                        {
                            if (keys[0] != "" && keys[1] != "" && keys[2] != "")
                            {
                                string accNo = keys[0];
                                string fileType = keys[1].ToUpper();
                                string timestamp = keys[2];

                                Response responseUA = IUserAccountService.GetDataByFilter(" account_no ='" + accNo + "' and is_verified = 1", 0, 0, false);
                                if (responseUA.IsSuccess)
                                {
                                    UserAccount userAcc = ((List<UserAccount>)responseUA.Data).FirstOrDefault();
                                    if (userAcc != null)
                                    {
                                        if (accNo == userAcc.AccountNo)
                                        {
                                            if (fileType.Equals("TV"))
                                            {
                                                DateTime date;
                                                bool isDate = DateTime.TryParseExact(timestamp.Remove(timestamp.IndexOf(".pdf")), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                                                if (isDate)
                                                {
                                                    Response responseUOSList = IUserOrderStatementService.GetDataByFilter("status=1", 0, 0, true);
                                                    if (responseUOSList.IsSuccess)
                                                    {
                                                        List<UserOrderStatement> userorderstatement = (List<UserOrderStatement>)responseUOSList.Data;

                                                        UserOrderStatement userOrderStatement = userorderstatement.Where(x => x.Name == tv.Name).FirstOrDefault();
                                                        UserOrderStatement newUserOrderStatement = new UserOrderStatement();

                                                        byte[] result = Convert.FromBase64String(tv.Base64String.Replace("data:application/pdf;base64,", ""));
                                                        File.WriteAllBytes(HttpContext.Current.Server.MapPath("/Content/Statements/") + Path.GetFileName(tv.Name), result);

                                                        Response response1 = IUserService.GetSingle(userAcc.UserId);
                                                        if (response1.IsSuccess)
                                                        {
                                                            User user = (User)response1.Data;

                                                            if (userOrderStatement != null)
                                                            {
                                                                //update existing record as old file.
                                                                userOrderStatement.Status = 0;
                                                                IUserOrderStatementService.UpdateData(userOrderStatement);

                                                                newUserOrderStatement.RefNo = timestamp.Remove(timestamp.IndexOf(".pdf"));
                                                                newUserOrderStatement.Name = tv.Name;
                                                                newUserOrderStatement.Url = "/Content/Statements/" + Path.GetFileName(tv.Name);
                                                                newUserOrderStatement.CreatedBy = 0;
                                                                newUserOrderStatement.CreatedDate = DateTime.Now;
                                                                newUserOrderStatement.UpdatedBy = 0;
                                                                newUserOrderStatement.Status = 1;
                                                                newUserOrderStatement.UserId = userAcc.UserId;
                                                                newUserOrderStatement.UserOrderStatementTypeId = 4;
                                                                newUserOrderStatement.UserAccountId = userAcc.Id;
                                                                IUserOrderStatementService.PostData(newUserOrderStatement);

                                                                response.IsSuccess = true;
                                                                responseMsgs.Add("[" + tv.Name + "] updates successfully");

                                                                Email email = new Email();
                                                                email.user = user;
                                                                EmailService.SendUpdateMail(email, "Tax Voucher Notification", "Tax Voucher is updated", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                                            }
                                                            else
                                                            {
                                                                newUserOrderStatement.RefNo = timestamp.Remove(timestamp.IndexOf(".pdf"));
                                                                newUserOrderStatement.Name = tv.Name;
                                                                newUserOrderStatement.Url = "/Content/Statements/" + Path.GetFileName(tv.Name);
                                                                newUserOrderStatement.CreatedBy = 0;
                                                                newUserOrderStatement.CreatedDate = DateTime.Now;
                                                                newUserOrderStatement.UpdatedBy = 0;
                                                                newUserOrderStatement.Status = 1;
                                                                newUserOrderStatement.UserId = userAcc.UserId;
                                                                newUserOrderStatement.UserOrderStatementTypeId = 4;
                                                                newUserOrderStatement.UserAccountId = userAcc.Id;
                                                                IUserOrderStatementService.PostData(newUserOrderStatement);

                                                                response.IsSuccess = true;
                                                                responseMsgs.Add("[" + tv.Name + "] uploads successfully");

                                                                Email email = new Email();
                                                                email.user = user;
                                                                EmailService.SendUpdateMail(email, "Tax Voucher Notification", "Tax Voucher is uploaded", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            response.IsSuccess = false;
                                                            responseMsgs.Add(response1.Message);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        response.IsSuccess = false;
                                                        responseMsgs.Add(responseUOSList.Message);
                                                    }
                                                }
                                                else
                                                {
                                                    response.IsSuccess = false;
                                                    responseMsgs.Add("Date of [" + tv.Name + "] is invalid");
                                                }
                                            }
                                            else
                                            {
                                                response.IsSuccess = false;
                                                responseMsgs.Add("Statement Type of [" + tv.Name + "] contains invalid name");
                                            }
                                        }
                                        else
                                        {
                                            response.IsSuccess = false;
                                            responseMsgs.Add("Account No of [" + tv.Name + "] is not exist");
                                        }
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        responseMsgs.Add("Account No of [" + tv.Name + "] is not exist");
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    responseMsgs.Add(responseUA.Message);
                                }
                            }
                            else
                            {
                                response.IsSuccess = false;
                                responseMsgs.Add("File name of [" + tv.Name + "] is invalid");
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            responseMsgs.Add("File name of [" + tv.Name + "] is invalid");
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        responseMsgs.Add("File type of [" + tv.Name + "] is invalid");
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                responseMsgs.Add(ex.Message);
            }
            response.Message = String.Join(", ", responseMsgs.ToArray());
            return response;
        }

        public class TaxVouchers
        {
            public string Base64String { get; set; }
            public string Name { get; set; }
        }
    }
}