using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("dv_distribution_intructions")]
    public class DvDistributionIntruction
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("fund_id"), Display("FundId")]
        public Int32 FundId { get; set; }
        [Database("distribution_intruction"), Display("DistributionIntruction")]
        public Int32 DistributionIntruction { get; set; }
        [Database("consultant_id"), Display("ConsultantId")]
        public string ConsultantId { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        public UtmcFundInformation FundIdUtmcFundInformation { get; set; }
        public UserAccount UserAccountIdUserAccount { get; set; }
        public User UserIdUser { get; set; }
    }
}
