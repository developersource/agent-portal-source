﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("payment_details")]
    public class PaymentDetail
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("transaction_id"), Display("TransactionId")]
        public String TransactionId { get; set; }
        [Database("msg_type"), Display("MsgType")]
        public String MsgType { get; set; }
        [Database("msg_token"), Display("MsgToken")]
        public String MsgToken { get; set; }
        [Database("seller_ex_id"), Display("SellerExId")]
        public String SellerExId { get; set; }
        [Database("seller_ex_order_no"), Display("SellerExOrderNo")]
        public String SellerExOrderNo { get; set; }
        [Database("seller_order_no"), Display("SellerOrderNo")]
        public String SellerOrderNo { get; set; }
        [Database("seller_txn_time"), Display("SellerTxnTime")]
        public String SellerTxnTime { get; set; }
        [Database("fpx_txn_time"), Display("FpxTxnTime")]
        public String FpxTxnTime { get; set; }
        [Database("seller_id"), Display("SellerId")]
        public String SellerId { get; set; }
        [Database("seller_bank_code"), Display("SellerBankCode")]
        public String SellerBankCode { get; set; }
        [Database("txn_currency"), Display("TxnCurrency")]
        public String TxnCurrency { get; set; }
        [Database("txn_amount"), Display("TxnAmount")]
        public String TxnAmount { get; set; }
        [Database("buyer_email"), Display("BuyerEmail")]
        public String BuyerEmail { get; set; }
        [Database("buyer_id"), Display("BuyerId")]
        public String BuyerId { get; set; }
        [Database("buyer_name"), Display("BuyerName")]
        public String BuyerName { get; set; }
        [Database("buyer_bank_id"), Display("BuyerBankId")]
        public String BuyerBankId { get; set; }
        [Database("buyer_bank_branch"), Display("BuyerBankBranch")]
        public String BuyerBankBranch { get; set; }
        [Database("buyer_acc_no"), Display("BuyerAccNo")]
        public String BuyerAccNo { get; set; }
        [Database("maker_name"), Display("MakerName")]
        public String MakerName { get; set; }
        [Database("buyer_Iban"), Display("BuyerIban")]
        public String BuyerIban { get; set; }
        [Database("product_desc"), Display("ProductDesc")]
        public String ProductDesc { get; set; }
        [Database("version"), Display("Version")]
        public String Version { get; set; }
        [Database("created_on"), Display("CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}
