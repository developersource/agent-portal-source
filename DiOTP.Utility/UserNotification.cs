using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_notifications")]
    public class UserNotification
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_notification_setting_id"), Display("UserNotificationSettingId")]
        public Int32 UserNotificationSettingId { get; set; }
        [Database("is_read"), Display("IsRead")]
        public Int32 IsRead { get; set; }
        [Database("is_delete"), Display("IsDelete")]
        public Int32 IsDelete { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("modified_by"), Display("ModifiedBy")]
        public Int32? ModifiedBy { get; set; }
        [Database("modified_date"), Display("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
