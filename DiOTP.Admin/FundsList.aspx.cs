﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using KellermanSoftware.CompareNetObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class FundsList : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyfdObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyfdObj.Value; } }

        private static readonly Lazy<IUtmcFundChargeService> lazyUtmcFundChargeServiceObj = new Lazy<IUtmcFundChargeService>(() => new UtmcFundChargeService());

        public static IUtmcFundChargeService IUtmcFundChargeService { get { return lazyUtmcFundChargeServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> lazyUtmcFundFileServiceObj = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IUtmcFundFileService { get { return lazyUtmcFundFileServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        public static int idString;

        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append(" 1=1 ");
                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameFundCode = Converter.GetColumnNameByPropertyName<UtmcFundInformation>(nameof(UtmcFundInformation.FundCode));
                    filter.Append(" and (" + columnNameFundCode + " like '%" + Search.Value.Trim() + "%'");
                    string columnNameFundName = Converter.GetColumnNameByPropertyName<UtmcFundInformation>(nameof(UtmcFundInformation.FundName));
                    filter.Append(" or " + columnNameFundName + " like '%" + Search.Value.Trim() + "%')");
                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IUtmcFundInformationService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(filter.ToString(), skip, take, false);
                if (responseUFIList.IsSuccess)
                {
                    List<UtmcFundInformation> UtmcFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;

                    Response responseFIList = IFundInfoService.GetData(0, 0, false);
                    if (responseFIList.IsSuccess)
                    {
                        List<FundInfo> FundInfos = (List<FundInfo>)responseFIList.Data;
                        StringBuilder sb = new StringBuilder();
                        int index = 1;
                        foreach (UtmcFundInformation utmcFundInformation in UtmcFundInformations)
                        {
                            Response responseUFDList = IUtmcFundDetailService.GetDataByPropertyName(nameof(UtmcFundDetail.UtmcFundInformationId), utmcFundInformation.Id.ToString(), true, 0, 0, false);
                            if (responseUFDList.IsSuccess)
                            {
                                UtmcFundDetail ufd = ((List<UtmcFundDetail>)responseUFDList.Data).FirstOrDefault();
                                FundInfo fDI = FundInfos.Where(x => x.FundCode == utmcFundInformation.IpdFundCode).FirstOrDefault();
                                sb.Append(@"<tr class='fs-12'>
                                    <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + utmcFundInformation.Id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (utmcFundInformation.Status == "Active" ? "<span class='label label-success'>Active</span>" : (utmcFundInformation.Status == "Inactive" ? "<span class='label label-danger'>Inactive</span" : "<span class='label label-danger'>Suspended</span>")) + @"</span>
                                    </td>
                                    <td><a href='javascript:;' data-id='" + utmcFundInformation.Id + @"' data-original-title='View details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='FundDetails.aspx' data-title='View details' data-action='View'><span class='fundName'>" + utmcFundInformation.FundCode + @"</span> <span class='ml-10'>" + (utmcFundInformation.IsRetail == 0 ? "<img src='/Content/MyImage/12.png' height='16' style='margin-top:-5px;' />" : "") + @"</span><span class='fundType'>" + (ufd != null && ufd.IsEpfApproved == 1 ? "EPF-MIS" : "") + @"</span> <br><span class='fundFullname'>" + utmcFundInformation.FundName.Capitalize() + @"</span>&nbsp;<i class='fa fa-eye'></i></a>
                                    <td>" + String.Format("{0:0.0000}", fDI != null ? fDI.CurrentUnitPrice : 0) + @"</td>
                                    <td><span class='" + (fDI != null && fDI.ChangePrice > 0 ? "fund-success-text" : (fDI != null && fDI.ChangePrice == 0 ? "fund-warning-text" : "fund-danger-text")) + @"'>" + String.Format("{0:0.0000}", fDI != null ? fDI.ChangePrice : 0) + @"</span></td>
                                    <td><span class='" + (fDI != null && fDI.ChangePer > 0 ? "fund-success" : (fDI != null && fDI.ChangePer == 0 ? "fund-warning" : "fund-danger")) + @"'>" + String.Format("{0:0.00}", fDI != null ? fDI.ChangePer : 0) + @" %</span></td>
                                    <td>" + utmcFundInformation.FundBaseCurrency + @"</td>
                                    <td>" + utmcFundInformation.LipperCategoryOfFund + @"</td>
                                    <td>" + (fDI != null ? fDI.CurrentNavDate.ToString("dd/MM/yyyy") : "") + @" </td>
                                    <td class='hide'>" + utmcFundInformation.FundCls + @"</td>
                                    <td class='hide'>" + utmcFundInformation.IsRetail + @"</td>
                                </tr>");
                                index++;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseUFDList.Message + "');", true);
                            }
                        }
                        fundListTableBody.InnerHtml = sb.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseFIList.Message + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseUFIList.Message + "');", true);
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            try
            {
                string idString = String.Join(",", ids);
                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    String tempStatus = "";
                    List<UtmcFundInformation> funds = (List<UtmcFundInformation>)responseUFIList.Data;
                    if (action == "Suspend")
                    {
                        funds.ForEach(x =>
                        {
                            tempStatus = x.Status;
                            x.Status = "Suspended";
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.FundName + " Fund updated",
                                TableName = "`utmc_fund_information`",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "Status",
                                ValueOld = tempStatus,
                                ValueNew = "Suspended",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        IUtmcFundInformationService.UpdateBulkData(funds);
                    }
                    if (action == "Deactivate")
                    {
                        funds.ForEach(x =>
                        {
                            tempStatus = x.Status;
                            x.Status = "Inactive";
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.FundName + " Fund updated",
                                TableName = "`utmc_fund_information`",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "Status",
                                ValueOld = tempStatus,
                                ValueNew = "Inactive",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        IUtmcFundInformationService.UpdateBulkData(funds);
                    }
                    if (action == "Activate")
                    {
                        funds.ForEach(x =>
                        {
                            tempStatus = x.Status;
                            x.Status = "Active";
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.FundName + " Fund updated",
                                TableName = "`utmc_fund_information`",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "Status",
                                ValueOld = tempStatus,
                                ValueNew = "Active",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        IUtmcFundInformationService.UpdateBulkData(funds);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fund list action: " + ex.Message);
                return false;
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(FundDetailsClass obj)
        {
            
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            try
            {

                if (obj.Id == 0)
                {

                }
                else
                {
                    idString = (int)HttpContext.Current.Session["idString"];
                    Response responseGet = IUtmcFundInformationService.GetSingle(idString);
                    string fundName = "";
                    if (responseGet.IsSuccess)
                    {
                        UtmcFundInformation utmcFundInfo = (UtmcFundInformation)responseGet.Data;
                        fundName = utmcFundInfo.FundName;

                        UtmcFundInformation utmcFundInfoDB = new UtmcFundInformation();
                        foreach (var propertyInfo in utmcFundInfoDB.GetType().GetProperties())
                        {
                            propertyInfo.SetValue(utmcFundInfoDB, propertyInfo.GetValue(utmcFundInfo, null), null);
                        }

                        utmcFundInfo.Conventional = !string.IsNullOrEmpty(obj.Conventional) ? obj.Conventional : utmcFundInfo.Conventional;
                        
                        utmcFundInfo.ForeignFund = !string.IsNullOrEmpty(obj.ForeignFund) ? obj.ForeignFund : utmcFundInfo.ForeignFund;
                        utmcFundInfo.FundBaseCurrency = !string.IsNullOrEmpty(obj.FundBaseCurrency) ? obj.FundBaseCurrency : utmcFundInfo.FundBaseCurrency;
                        utmcFundInfo.FundCode = !string.IsNullOrEmpty(obj.FundCode) ? obj.FundCode : utmcFundInfo.FundCode;
                        
                        utmcFundInfo.IsEmis = obj.IsEpfApproved;
                        utmcFundInfo.IsRetail = (obj.IsRetail != null) ? obj.IsRetail : utmcFundInfo.IsRetail;
                        utmcFundInfo.LipperCategoryOfFund = !string.IsNullOrEmpty(obj.LipperCategoryOfFund) ? obj.LipperCategoryOfFund : utmcFundInfo.LipperCategoryOfFund;
                        
                        utmcFundInfo.SatGroup = !string.IsNullOrEmpty(obj.SatGroup) ? obj.SatGroup : utmcFundInfo.SatGroup;
                        utmcFundInfo.IsRsp = (obj.IsRsp != null) ? obj.IsRsp : utmcFundInfo.IsRsp;
                        utmcFundInfo.Status = !string.IsNullOrEmpty(obj.Status) ? obj.Status : utmcFundInfo.Status;
                        utmcFundInfo.UtmcFundCategoriesDefId = obj.UtmcFundCategoriesDefId != 0 ? obj.UtmcFundCategoriesDefId : utmcFundInfo.UtmcFundCategoriesDefId;
                        if (utmcFundInfo.UtmcFundCategoriesDefId == 1 || utmcFundInfo.UtmcFundCategoriesDefId == 2 || utmcFundInfo.UtmcFundCategoriesDefId == 5 || utmcFundInfo.UtmcFundCategoriesDefId == 6)
                        {
                            utmcFundInfo.FundCls = "EE";
                        }
                        else if (utmcFundInfo.UtmcFundCategoriesDefId == 3 || utmcFundInfo.UtmcFundCategoriesDefId == 4)
                        {
                            utmcFundInfo.FundCls = "MA";
                        }
                        else if (utmcFundInfo.UtmcFundCategoriesDefId == 7 || utmcFundInfo.UtmcFundCategoriesDefId == 8)
                        {
                            utmcFundInfo.FundCls = "MM";
                        }
                        

                        
                        Response responseU = IUtmcFundInformationService.UpdateData(utmcFundInfo);
                        if (!responseU.IsSuccess)
                        {
                            return responseU;
                        }
                        else
                        {
                            AdminLogMain alm = new AdminLogMain()
                            {
                                TableName = "utmc_fund_information",
                                Description = fundName + " info change successful",
                                UserId = loginUser.Id,
                                UpdatedDate = DateTime.Now,
                            };

                            Response responseALM = IAdminLogMainService.PostData(alm);
                            alm = (AdminLogMain)responseALM.Data;

                            int propertyCount = typeof(UtmcFundInformation).GetProperties().Length;

                            CompareLogic basicComparison = new CompareLogic()
                            { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                            List<Difference> diffs = basicComparison.Compare(utmcFundInfoDB, utmcFundInfo).Differences;
                            int noOfDifferent = diffs.Count();

                            foreach (Difference diff in diffs)
                            {
                                string columnName = Converter.GetColumnNameByPropertyName<UtmcFundInformation>(diff.PropertyName);

                                AdminLogSub x = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = columnName,
                                    ValueOld = diff.Object1Value,
                                    ValueNew = diff.Object2Value,
                                };
                                Response response1 = IAdminLogSubService.PostData(x);
                                x = (AdminLogSub)response1.Data;
                            }
                        }
                    }
                    else
                    {
                        return responseGet;
                    }

                    Response responseUFD = IUtmcFundDetailService.GetDataByFilter(" utmc_fund_information_id='" + obj.Id + "'", 0, 0, false);
                    if (responseUFD.IsSuccess)
                    {
                        List<UtmcFundDetail> utmcFundDetails = (List<UtmcFundDetail>)responseUFD.Data;
                        if (utmcFundDetails.Count > 0)
                        {
                            UtmcFundDetail utmcFundDetailDB = utmcFundDetails.First();

                            UtmcFundDetail UtmcFundDetailDBOrg = new UtmcFundDetail();
                            foreach (var propertyInfo in UtmcFundDetailDBOrg.GetType().GetProperties())
                            {
                                propertyInfo.SetValue(UtmcFundDetailDBOrg, propertyInfo.GetValue(utmcFundDetailDB, null), null);
                            }
                            utmcFundDetailDB.RelaunchDate = obj.RelaunchDate;
                            utmcFundDetailDB.CollingOffPeriod = obj.CollingOffPeriod;
                            utmcFundDetailDB.DistributionPolicy = obj.DistributionPolicy;
                            utmcFundDetailDB.InvestmentObjective = obj.InvestmentObjective;
                            utmcFundDetailDB.InvestmentStrategyAndPolicy = obj.InvestmentStrategyAndPolicy;
                            utmcFundDetailDB.IsEpfApproved = obj.IsEpfApproved;
                            utmcFundDetailDB.HistoricalIncomeDistribution = obj.HistoricalIncomeDistribution;
                            utmcFundDetailDB.FundSizeRm = obj.FundSizeRm;
                            utmcFundDetailDB.MinHoldingUnits = obj.MinHoldingUnits;
                            utmcFundDetailDB.MinInitialInvestmentCash = obj.MinInitialInvestmentCash;
                            utmcFundDetailDB.MinInitialInvestmentEpf = obj.MinInitialInvestmentEpf;
                            utmcFundDetailDB.MinRedAmountUnits = obj.MinRedAmountUnits;
                            utmcFundDetailDB.MinRspInvestmentAdditionalRm = obj.MinRspInvestmentAdditionalRm;
                            utmcFundDetailDB.MinRspInvestmentInitialRm = obj.MinRspInvestmentInitialRm;
                            utmcFundDetailDB.MinSubsequentInvestmentCash = obj.MinSubsequentInvestmentCash;
                            utmcFundDetailDB.MinSubsequentInvestmentEpf = obj.MinSubsequentInvestmentEpf;
                            utmcFundDetailDB.PricingBasis = obj.PricingBasis;
                            
                            utmcFundDetailDB.RiskRating = obj.RiskRating;
                            utmcFundDetailDB.ShariahCompliant = obj.ShariahCompliant;
                            
                            
                            Response responseUpdateUtmcFundDetails = IUtmcFundDetailService.UpdateData(utmcFundDetailDB);

                            //Audit Log starts here
                            if (!responseUpdateUtmcFundDetails.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    TableName = "utmc_fund_details",
                                    Description = fundName + " details change successful",
                                    UserId = loginUser.Id,
                                    UpdatedDate = DateTime.Now,
                                };

                                Response responseALM = IAdminLogMainService.PostData(alm);
                                alm = (AdminLogMain)responseALM.Data;

                                int propertyCount = typeof(UtmcFundDetail).GetProperties().Length;

                                CompareLogic basicComparison = new CompareLogic()
                                { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                List<Difference> diffs = basicComparison.Compare(UtmcFundDetailDBOrg, utmcFundDetailDB).Differences;
                                int noOfDifferent = diffs.Count();

                                foreach (Difference diff in diffs)
                                {
                                    string columnName = Converter.GetColumnNameByPropertyName<UtmcFundDetail>(diff.PropertyName);

                                    AdminLogSub x = new AdminLogSub()
                                    {
                                        AdminLogMainId = alm.Id,
                                        ColumnName = columnName,
                                        ValueOld = diff.Object1Value,
                                        ValueNew = diff.Object2Value,
                                    };
                                    Response response1 = IAdminLogSubService.PostData(x);
                                    x = (AdminLogSub)response1.Data;
                                }
                            }
                            

                        }
                    }

                    Response responseUFC = IUtmcFundChargeService.GetDataByFilter(" utmc_fund_information_id='" + obj.Id + "'", 0, 0, false);
                    if (responseUFC.IsSuccess)
                    {
                        List<UtmcFundCharge> utmcFundCharges = (List<UtmcFundCharge>)responseUFC.Data;
                        if (utmcFundCharges.Count > 0)
                        {
                            UtmcFundCharge utmcFundChargeDB = utmcFundCharges.First();

                            UtmcFundCharge utmcFundChargeDBOrg = new UtmcFundCharge();
                            foreach (var propertyInfo in utmcFundChargeDBOrg.GetType().GetProperties())
                            {
                                propertyInfo.SetValue(utmcFundChargeDBOrg, propertyInfo.GetValue(utmcFundChargeDB, null), null);
                            }

                            utmcFundChargeDB.AnnualManagementChargePercent = obj.AnnualManagementChargePercent;
                            utmcFundChargeDB.GstPercent = obj.GstPercent;
                            utmcFundChargeDB.InitialSalesChargesPercent = obj.InitialSalesChargesPercent;
                            utmcFundChargeDB.EpfSalesChargesPercent = obj.EpfSalesChargesPercent;
                            utmcFundChargeDB.OtherSignificantFeeRm = obj.OtherSignificantFeeRm;
                            utmcFundChargeDB.RedemptionFeePercent = obj.RedemptionFeePercent;
                            utmcFundChargeDB.SwitchingFeePercent = obj.SwitchingFeePercent;
                            utmcFundChargeDB.TransferFeeRm = obj.TransferFeeRm;
                            utmcFundChargeDB.TrusteeFeePercent = obj.TrusteeFeePercent;
                            
                            Response responseUtmcFundCharges = IUtmcFundChargeService.UpdateData(utmcFundChargeDB);
                            //Audit Log starts here
                            if (!responseUtmcFundCharges.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    TableName = "utmc_fund_charges",
                                    Description = fundName + " charges change successful",
                                    UserId = loginUser.Id,
                                    UpdatedDate = DateTime.Now,
                                };

                                Response responseALM = IAdminLogMainService.PostData(alm);
                                alm = (AdminLogMain)responseALM.Data;

                                int propertyCount = typeof(UtmcFundCharge).GetProperties().Length;

                                CompareLogic basicComparison = new CompareLogic()
                                { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                List<Difference> diffs = basicComparison.Compare(utmcFundChargeDBOrg, utmcFundChargeDB).Differences;
                                int noOfDifferent = diffs.Count();

                                foreach (Difference diff in diffs)
                                {
                                    string columnName = Converter.GetColumnNameByPropertyName<UtmcFundCharge>(diff.PropertyName);

                                    AdminLogSub x = new AdminLogSub()
                                    {
                                        AdminLogMainId = alm.Id,
                                        ColumnName = columnName,
                                        ValueOld = diff.Object1Value,
                                        ValueNew = diff.Object2Value,
                                    };
                                    Response response1 = IAdminLogSubService.PostData(x);
                                    x = (AdminLogSub)response1.Data;
                                }
                            }
                            
                        }
                        else
                        {
                            UtmcFundCharge utmcFundChargeDB = new UtmcFundCharge();
                            utmcFundChargeDB.UtmcFundInformationId = obj.Id;
                            utmcFundChargeDB.AnnualManagementChargePercent = obj.AnnualManagementChargePercent;
                            utmcFundChargeDB.GstPercent = obj.GstPercent;
                            utmcFundChargeDB.InitialSalesChargesPercent = obj.InitialSalesChargesPercent;
                            utmcFundChargeDB.EpfSalesChargesPercent = obj.EpfSalesChargesPercent;
                            utmcFundChargeDB.OtherSignificantFeeRm = obj.OtherSignificantFeeRm;
                            utmcFundChargeDB.RedemptionFeePercent = obj.RedemptionFeePercent;
                            utmcFundChargeDB.SwitchingFeePercent = obj.SwitchingFeePercent;
                            utmcFundChargeDB.TransferFeeRm = obj.TransferFeeRm;
                            utmcFundChargeDB.TrusteeFeePercent = obj.TrusteeFeePercent;
                            Response responseUtmcFundCharges = IUtmcFundChargeService.PostData(utmcFundChargeDB);

                            //Audit Log starts here
                            if (!responseUtmcFundCharges.IsSuccess)
                            {

                            }
                            else
                            {
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    TableName = "utmc_fund_charges",
                                    Description = fundName + " charges added successful",
                                    UserId = loginUser.Id,
                                    UpdatedDate = DateTime.Now,
                                };
                                Response responseALM = IAdminLogMainService.PostData(alm);
                            }

                        }
                    }
                    
                }
                response.IsSuccess = true;
                response.Message = "Success";
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Upload(UtmcFundFile obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            try
            {
                Int32 LoginUserId = 0;
                if (HttpContext.Current.Session["admin"] != null)
                {
                    User sessionUser = (User)HttpContext.Current.Session["admin"];
                    LoginUserId = sessionUser.Id;
                }
                if (obj.Id != 0)
                {
                    Response responseUFF = IUtmcFundFileService.GetSingle(obj.Id);
                    if (responseUFF.IsSuccess)
                    {
                        UtmcFundFile utmcFundFile = (UtmcFundFile)responseUFF.Data;
                        utmcFundFile.Name = obj.Name;
                        utmcFundFile.Url = obj.Url;
                        utmcFundFile.UpdatedDate = DateTime.Now;
                        utmcFundFile.UpdatedBy = LoginUserId;
                        utmcFundFile.Status = obj.Status;
                        IUtmcFundFileService.UpdateData(utmcFundFile);
                        response.IsSuccess = true;
                        response.Message = "Updated successfully";

                        Response responseF = IUtmcFundInformationService.GetSingle(utmcFundFile.UtmcFundInformationId);
                        if (responseF.IsSuccess)
                        {
                            UtmcFundInformation objF = (UtmcFundInformation)responseF.Data;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = objF.FundName + " Fund file (" + utmcFundFile.Name + ") uploaded",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,

                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                AdminLogMain almFail = new AdminLogMain()
                                {
                                    Description = objF.FundName + " Fund file (" + utmcFundFile.Name + ") uploading failed",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,

                                };
                                //Audit log failed
                            }
                            else
                            {

                            }
                        }
                        //Audit Log Ends here
                    }
                    else
                    {
                        return responseUFF;
                    }
                }
                else
                {
                    obj.CreatedDate = DateTime.Now;
                    obj.CreatedBy = LoginUserId;
                    obj.UpdatedDate = default(DateTime);
                    obj.UpdatedBy = LoginUserId;
                    IUtmcFundFileService.PostData(obj);
                    response.IsSuccess = true;
                    response.Message = "Uploaded successfully";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = obj.Id + " Fund file (" + obj.Name + ") updated",
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,

                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object ConfigureFund(dynamic obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string fundId = obj["fundId"];

            Response checkFundsResponse = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code='" + fundId + "' ", 0, 0, false);
            if (checkFundsResponse.IsSuccess)
            {
                List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)checkFundsResponse.Data;
                if (utmcFundInformations.Count > 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Fund code already configured.";
                }
                else
                {
                    List<FundInfoDTO> fundInfos = ServicesManager.GetFundInfoByFundIds("'" + fundId + "'");
                    if (fundInfos.Count == 0)
                    {
                        response.IsSuccess = false;
                        response.Message = "Fund code doesn't exist.";
                    }
                    else
                    {
                        FundInfoDTO fundInfo = fundInfos.FirstOrDefault();

                        UtmcFundInformation utmcFundInformation = new UtmcFundInformation
                        {
                            EpfIpdCode = "IPD033",
                            IpdFundCode = fundInfo.FUND_ID,
                            FundCode = "",
                            FundName = fundInfo.L_NAME,
                            UtmcFundCategoriesDefId = 0,
                            EffectiveDate = fundInfo.COMMENCE_DT,
                            LipperCategoryOfFund = "",
                            Conventional = "",
                            Status = "Inactive",
                            ForeignFund = "",
                            ReportDate = DateTime.Now,
                            FundBaseCurrency = "",
                            IsEmis = 0,
                            FundCls = "",
                            IsRetail = 1,
                            SatGroup = "",
                            IsRsp = 0,

                        };
                        Response postUFIData = IUtmcFundInformationService.PostData(utmcFundInformation);
                        if (postUFIData.IsSuccess)
                        {
                            utmcFundInformation = (UtmcFundInformation)postUFIData.Data;

                            UtmcFundDetail utmcFundDetail = new UtmcFundDetail
                            {
                                UtmcFundInformationId = utmcFundInformation.Id,
                                LaunchDate = fundInfo.COMMENCE_DT,
                                RelaunchDate = default(DateTime),
                                LaunchPrice = fundInfo.UP_SELL,
                                PricingBasis = "",
                                InvestmentObjective = "",
                                InvestmentStrategyAndPolicy = "",
                                LatestNavPrice = fundInfo.UP_SELL,
                                HistoricalIncomeDistribution = 1,
                                IsEpfApproved = 0,
                                ShariahCompliant = 0,
                                RiskRating = "",
                                FundSizeRm = "",
                                MinInitialInvestmentCash = 0,
                                MinInitialInvestmentEpf = 0,
                                MinSubsequentInvestmentCash = 0,
                                MinSubsequentInvestmentEpf = 0,
                                MinRspInvestmentInitialRm = 0,
                                MinRspInvestmentAdditionalRm = 0,
                                MinRedAmountUnits = 0,
                                MinHoldingUnits = 0,
                                CollingOffPeriod = "",
                                DistributionPolicy = ""
                            };
                            Response postUFDData = IUtmcFundDetailService.PostData(utmcFundDetail);
                            if (postUFDData.IsSuccess)
                            {
                                response.IsSuccess = true;
                                response.Message = "Successfully configured";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = utmcFundInformation.FundName + " New Fund retrived",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,

                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }

                                //Audit Log Ends here
                            }
                            else
                            {
                                response = postUFDData;
                            }
                        }
                        else
                        {
                            response = postUFIData;
                        }
                    }
                }
            }
            else
            {
                response = checkFundsResponse;
            }
            return response;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object SyncNAV()
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response responseR = new Response();
            try
            {
                Response responseUTMCFundInfo = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_information ", 0, 0, false, null, false, null, false);
                var DynUTMCFundInfoString = responseUTMCFundInfo.Data;
                var responseUTMCFundInfoJSON = JsonConvert.SerializeObject(DynUTMCFundInfoString);
                List<UtmcFundInformation> utmcFundInformations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundInformation>>(responseUTMCFundInfoJSON);
                string fundIdsString = String.Join(",", utmcFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToArray());
                List<CurrentNAVDTO> currentNAVDTOs = ServicesManager.GetcurrentNAV(fundIdsString);
                List<Response> responses = new List<Response>();
                currentNAVDTOs.ForEach(currentNAV =>
                {
                    CurrentNAVDTO prev1DayNAVDTO = ServicesManager.GetNAVByFundAndDate("'" + currentNAV.FUND_ID + "'", currentNAV.TRANS_DT.ToString("yyyy-MM-dd") + "prev");

                    var responseCJSON = JsonConvert.SerializeObject(currentNAV);
                    Logger.WriteLog("CheckNAV currentNAV " + responseCJSON);
                    Response response = GenericService.GetDataByQuery(" SELECT * FROM utmc_daily_nav_fund where IPD_Fund_Code='" + currentNAV.FUND_ID + "' and CAST(Daily_NAV_Date AS DATE) ='" + currentNAV.TRANS_DT.ToString("yyyy-MM-dd") + "' ", 0, 0, false, null, false, null, false);
                    var DynString = response.Data;
                    var responseJSON = JsonConvert.SerializeObject(DynString);
                    List<UtmcDailyNavFund> utmcDailyNavFunds = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcDailyNavFund>>(responseJSON);
                    if (utmcDailyNavFunds.Count == 0)
                    {
                        UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund
                        {
                            EpfIpdCode = "IPD000033",
                            IpdFundCode = currentNAV.FUND_ID,
                            DailyNavDate = currentNAV.TRANS_DT,
                            DailyNav = currentNAV.UNIT_IN_ISSUE_PRICE,
                            ReportDate = currentNAV.TRANS_DT,
                            DailyUnitCreated = currentNAV.UNIT_IN_ISSUE,
                            DailyNavEpf = 0.2M,
                            DailyUnitCreatedEpf = 0.4M,
                            DailyUnitPrice = currentNAV.UP_SELL,
                            DailyUnitCreatedEpfAdj = 0.0M,
                        };
                        Response resDNF = GenericService.PostData<UtmcDailyNavFund>(utmcDailyNavFund);
                        responses.Add(resDNF);
                        if (!resDNF.IsSuccess)
                        {
                            Logger.WriteLog("CheckNAV UtmcDailyNavFund " + utmcDailyNavFund.IpdFundCode + " PostData: " + resDNF.Message + " - Exception");
                        }

                        UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(x => x.IpdFundCode == currentNAV.FUND_ID);

                        Response responseUFD = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_details where utmc_fund_information_id='" + utmcFundInformation.Id + "' ", 0, 0, false, null, false, null, false);
                        var DynUFDString = responseUFD.Data;
                        var responseUFDJSON = JsonConvert.SerializeObject(DynUFDString);
                        List<UtmcFundDetail> utmcFundDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundDetail>>(responseUFDJSON);
                        UtmcFundDetail utmcFundDetail = utmcFundDetails.FirstOrDefault();
                        utmcFundDetail.LatestNavPrice = currentNAV.UP_SELL;
                        utmcFundDetail.LatestNavDate = currentNAV.TRANS_DT;
                        Response r1 = GenericService.UpdateData<UtmcFundDetail>(utmcFundDetail);
                        responses.Add(r1);
                        if (!r1.IsSuccess)
                        {
                            Logger.WriteLog("DailyNAV r1 UtmcFundDetail UpdateData: " + r1.Message + " - Exception");
                        }

                        Response responseFI = GenericService.GetDataByQuery(" SELECT * FROM fund_info where FUND_CODE='" + currentNAV.FUND_ID + "' ", 0, 0, false, null, false, null, false);
                        var DynFIString = responseFI.Data;
                        var responseFIJSON = JsonConvert.SerializeObject(DynFIString);
                        List<FundInfo> fundInfos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FundInfo>>(responseFIJSON);
                        FundInfo fundInfo = fundInfos.FirstOrDefault();
                        fundInfo.ChangePrice = (currentNAV.UP_SELL - prev1DayNAVDTO.UP_SELL);//fundDetailedInformation.Change;
                        fundInfo.ChangePer = (fundInfo.ChangePrice / prev1DayNAVDTO.UP_SELL) * 100;//fundDetailedInformation.ChangePercent;
                        fundInfo.CurrentUnitPrice = currentNAV.UP_SELL;
                        fundInfo.CurrentNavDate = currentNAV.TRANS_DT;
                        Response r2 = GenericService.UpdateData<FundInfo>(fundInfo);
                        responses.Add(r2);
                        if (!r2.IsSuccess)
                        {
                            Logger.WriteLog("CheckNAV r2 FundInfo UpdateData: " + r2.Message + " - Exception");
                        }
                        responses.Add(new Response { IsSuccess = true, Message = currentNAV.FUND_ID + " sync success with price: " + currentNAV.UP_SELL.ToString("N4") });
                        //Audit Log starts here
                        AdminLogMain alm = new AdminLogMain()
                        {
                            Description = "Fund List successfully downloaded",
                            TableName = "users",
                            UpdatedDate = DateTime.Now,
                            UserId = loginUser.Id,

                        };
                        Response responseLog = IAdminLogMainService.PostData(alm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                        }
                        else
                        {

                        }

                        //Audit Log Ends here
                    }
                    else
                    {
                        responses.Add(new Response { IsSuccess = true, Message = currentNAV.FUND_ID + " already in sync!" });
                    }
                });

                bool isExecute = false;
                if (responses.Count > 0)
                {
                    if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                    {
                        isExecute = true;
                        responseR.IsSuccess = true;
                        responseR.Message = String.Join(",<br/>", responses.Where(x => x.Message != null).Select(x => x.Message).ToArray());
                        return responseR;
                    }
                    else
                    {
                        responseR.IsSuccess = false;
                        responseR.Message = String.Join(",<br/>", responses.Where(x => x.Message != null).Select(x => x.Message).ToArray());
                        return responseR;
                    }
                }
                else
                {
                    isExecute = true;
                    responseR.IsSuccess = true;
                    responseR.Message = String.Join(",<br/>", responses.Where(x => x.Message != null).Select(x => x.Message).ToArray());
                    return responseR;
                }
            }
            catch (Exception ex)
            {
                responseR.IsSuccess = false;
                responseR.Message = ex.Message;
                return responseR;
            }
        }

    }


}