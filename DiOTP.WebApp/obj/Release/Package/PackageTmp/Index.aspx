﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="DiOTP.WebApp.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .required-input:after {
            content: "*";
            color: red;
            font-size: 1.15em;
            position: absolute;
            margin-top: -8px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        function loginValidation() {
            var isValid = true;
            var txtEmailOrUsername = $('#txtEmailOrUsername').val();
            var txtPassword = $('#txtPassword').val();
            if (!txtEmailOrUsername) {
                $('#txtEmailOrUsername').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
                isValid = false;
            } else {
                $('#txtEmailOrUsername').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });

            }
            if (!txtPassword) {
                $('#txtPassword').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
                isValid = false;
            } else {
                $('#txtPassword').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });

            }
            if (isValid) {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            return isValid;
        }
        function clientValidation() {
            var isValid = true;
            var txtICNo = $('#txtICNo').val();
            if (!txtICNo) {
                $('#divIC').addClass("required-input");
                isValid = false;
            } else {
                $('#divIC').removeClass("required-input");
            }
            var txtMANumber = $('#txtMANumber').val();
            if (!txtMANumber) {
                $('#divMA').addClass("required-input");
                isValid = false;
            } else {
                $('#divMA').removeClass("required-input");
            }
            var txtEmail = $('#txtEmail').val();
            if (!txtEmail) {
                $('#divEmail').addClass("required-input");
                isValid = false;
            } else {
                $('#divEmail').removeClass("required-input");
            }
            if (!isValid) {
                $('#msg').addClass('text-danger').html("<i class='fa fa-exclamation-triangle'></i> Please enter required(*) fields.");

            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            return isValid;
        }
    </script>

    <section class="announcement">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="announcement-wrapper" id="announcementWrapper" runat="server">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="login">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-5">
                    <div class="welcome-section hide" id="IndexWelcomeSection" runat="server" clientidmode="static">
                        <div>
                            <h3 class="welcome-note">Welcome to eApexIs</h3>
                        </div>
                        <div class="mb-30">
                            <h4 class="welcome-note-2">CREATE WEALTH BY INVESTING.</h4>
                            <h5 class="welcome-note-3">Begin with the right fund manager.</h5>
                        </div>
                        <div>
                            <a href="CorporateAccounts.aspx" class="btn btn-infos1">Open an Account</a>
                            <asp:LinkButton ID="btnLoginOpen" runat="server" ClientIDMode="Static" OnClick="btnLoginOpen_Click" Text="Login/Register" CssClass="btn btn-infos1"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="login-section" id="IndexLoginSection" runat="server" clientidmode="static">
                        <div class="login-head">
                            <h4><strong>LOGIN</strong></h4>
                        </div>
                        <div class="login-content">

                            <div id="divMessage" runat="server" clientidmode="static"></div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtEmailOrUsername" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter your Email ID"></asp:TextBox>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtPassword" runat="server"  TextMode="Password" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your password"></asp:TextBox>
                            </div>
                            <div class="form-group mb-5">
                                <asp:Button ClientIDMode="Static" ID="btnLogin" runat="server" CssClass="btn login-btn btn-block" Text="Login" OnClick="btnLogin_Click" OnClientClick="return loginValidation()" />
                            </div>
                            <div class="mt-5 countdown text-center text-white hide">

                                <label class="label label-warning" id="countDownTime" runat="server" clientidmode="static"></label>
                            </div>
                            <div class="login-fp mb-5" style="height: 20px">
                                <a href="/ForgotPassword.aspx" style="font-size: 12px;"><strong>Forgot Email ID/Password?</strong></a>
                            </div>

                            <%--<%= GetApplicationVersion() %>--%>
                        </div>

                        <div class="login-bottom" style="display: none">
                            <p class="fs-14 mb-2"><a href="#" style="color: #e89928;"><i class="fa fa-caret-right mr-6"></i>General information.</a></p>
                            <p class="fs-14 mb-2"><a href="#" style="color: #e89928;"><i class="fa fa-caret-right mr-6"></i>Whats new here?</a></p>
                            <p class="fs-14 mb-2"><a href="#" style="color: #e89928;"><i class="fa fa-caret-right mr-6"></i>Apex Thank you rewards.</a></p>
                        </div>
                        <div class="login-apply">
                            <div>
                                <p style="color: #fff; font-size: 14px;" class="mb-0"></p>
                                <a href="/OTP-Activation.aspx" style="color: #cca100; width: 105px; text-align: center; font-size: 14px" title="Join OTP">
                                    <b>Don't have an account? Register here</b>
                                </a>

                            </div>
                            <div style="height: 25px;">
                                <a href="/PrivacyPolicy.aspx" style="font-size: 13px">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="slider">
        <div class="container-fluid">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" id="carouselInner" runat="server" clientidmode="static">
                    <div class="item active" style="height: calc(100vh - 42px) !important">
                        <img src="/Content/Banner/office-583841_1920.jpg" height:86% weight:100%/>
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3 style="color : white;">Account Opening</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp" style="color: black;">
                                    You can open Account online. <br />
                                    We enabled Digital Onboarding to open account online.
                                </p>
                                <a class="btn btn-readmore" href="CorporateAccounts.aspx">Open now</a>
                            </div>
                        </div>

                    </div>
                    <%--<div class="item active" style="height: calc(100vh - 42px) !important">
                        <img src="Content/Banner/14.png" />
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3>Launch OTP</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp">
                                    Open your world of trading with the grand launching of
                                    eApexIs, Online Trading Platform.
                                    Gear up, start trading beginning Mid 2018.<br />

                                </p>
                                <button type="button" class="btn btn-readmore" onclick="javascript:window.location.href='News-Content.aspx'">Read more</button>
                            </div>
                        </div>

                    </div>
                    <div class="item" style="height: calc(100vh - 42px) !important">
                        <img src="Content/Banner/BANNER-2-Banner.jpg" />
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3>Launch FPX</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp" style="color: #000;">
                                    Trading never been so easy with FPX,
                                    Real time payment partner.
                                    All you need is your Internet Banking Account.
                                    
                                </p>
                                <button type="button" class="btn btn-readmore">Read more</button>

                            </div>

                        </div>
                    </div>
                    <div class="item" style="height: calc(100vh - 42px) !important">
                        <img src="/Content/Banner/9.jpg" />
                        <div class="carousel-caption hidden-lg hidden-md hidden-sm">
                            <div class="banner-head">
                                <h3>Security</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp">
                                    Taking encrypted security to the next level,
                                    with Two level Authentication, via SMS & Google.
                                    SO you vault over any threat.
                                </p>
                                <button type="button" class="btn btn-readmore">Read more</button>

                            </div>
                        </div>
                    </div>
                    <div class="item" style="height: calc(100vh - 42px) !important">
                        <img src="/Content/Banner/2.jpg" />
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3 style="color: #000;">Trading</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp" style="color: #000;">
                                    Powerful analysis tool with round the clock updates.
                                    View charts, compare funds, review prices and manymore.
                                </p>
                                <button type="button" class="btn btn-readmore">Read more</button>

                            </div>
                        </div>
                    </div>--%>
                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

                <ul class="nav nav-pills nav-justified nav-my" id="carouselThumbs" runat="server">
                    <li data-target="#myCarousel" data-slide-to="0" class="active">
                        <a href="#">
                            <span class="tab-head">Account Opening</span><br />
                            <span class="tab-cont">You can open Account online.</span>
                        </a>
                    </li>
                    <%--<li data-target="#myCarousel" data-slide-to="0" class="active">
                        <a href="#">
                            <span class="tab-head">Launch OTP</span><br />
                            <span class="tab-cont">Open your world of trading with the grand launching of eApexIs.</span>
                        </a></li>
                    <li data-target="#myCarousel" data-slide-to="1">
                        <a href="#">
                            <span class="tab-head">Launch FPX</span><br />
                            <span class="tab-cont">Trading never been so easy with FPx, your real time payment partner.</span>
                        </a></li>
                    <li data-target="#myCarousel" data-slide-to="2">
                        <a href="#">
                            <span class="tab-head">Security</span><br />
                            <span class="tab-cont">Taking encrypted security to the next level with Two Level Authentication.</span>
                        </a></li>
                    <li data-target="#myCarousel" data-slide-to="3">
                        <a href="#">
                            <span class="tab-head">Trading</span><br />
                            <span class="tab-cont">Powerful analysis tool with round the clock updates.</span>
                        </a></li>--%>
                </ul>
            </div>
        </div>
    </section>



    <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnLoginLocked" runat="server" Value="0" ClientIDMode="Static" />
    <%--<asp:HiddenField ID="hdnLoginLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />--%>
    <asp:HiddenField ID="hdnLoginAttempts" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="modalPlace" ID="modalHolder" runat="server">
    <div class="modal fade" id="modalAccountLockedPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document" style="width: 350px">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Unlock Your Account</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center text-danger mb-10">
                        <small id="msg" runat="server" clientidmode="static"><i class='fa fa-close icon'></i>Your account is locked.</small>
                    </div>
                    <div id="unlockMessage" runat="server" clientidmode="static">
                        <div class="mb-10">
                            Too many invalid login attempts. Unlock your account to Proceed.
                        </div>
                        <div class="text-center mb-5">
                            <small>You can click 'Proceed' below to unlock.</small>
                        </div>
                        <button id="unlock-btn" type="button" class="btn btn-primary btn-block">Proceed</button>
                    </div>
                    <div class="row hide" id="unlock-input">
                        <div class="col-md-12">
                            <div id="divIC" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtICNo" runat="server" CssClass="form-control" placeholder="NRIC / OLD NRIC / PASSPORT *" ClientIDMode="Static"></asp:TextBox>
                            </div>

                            <div id="divMA" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtMANumber" runat="server" CssClass="form-control" placeholder="Enter your MasterAccount Number *" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            <div id="divEmail" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Enter your Email *" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            <div class="form-group mb-10">
                                <br />
                                <div id="captcha" class="g-recaptcha text-center" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>
                                <br />
                                <asp:Button ID="btnRequestCode" runat="server" CssClass="btn login-btn btn-block" OnClientClick="return clientValidation()" OnClick="btnRequestCode_Click" Text="Submit" Enabled="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnAccountLockedPop" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
    <script>
        function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        //var hdnLoginLockTimeInSeconds = parseInt($('#hdnLoginLockTimeInSeconds').val());
        var hdnAccountLockedPop = parseInt($('#hdnAccountLockedPop').val());
        var hdnLoginAttempts = parseInt($('#hdnLoginAttempts').val());
        var hdnLoginLocked = parseInt($('#hdnLoginLocked').val());
        //function StartTimer() {
        //    //var timer2 = secondsTimeSpanToHMS(hdnLoginLockTimeInSeconds);
        //    //var timer2 = "5:01";
        //    var interval = setInterval(function () {
        //        //var timer = timer2.split(':');
        //        //by parsing integer, I avoid all extra string processing
        //        var hours = parseInt(timer[0], 10);
        //        var minutes = parseInt(timer[1], 10);
        //        var seconds = parseInt(timer[2], 10);
        //        --seconds;
        //        minutes = (seconds < 0) ? --minutes : minutes;
        //        hours = (minutes < 0) ? --hours : hours;
        //        if (hours < 0) {
        //            clearInterval(interval);
        //            $('.countdown').removeClass('hide');
        //            $('.countdown').html('Your Can Login Now.');
        //            $('#btnLogin').removeAttr('disabled');
        //            $('#divMessage').html("");
        //            $('#hdnMobilePinRequested').val(0);
        //        }
        //        else {
        //            seconds = (seconds < 0) ? 59 : seconds;
        //            seconds = (seconds < 10) ? '0' + seconds : seconds;
        //            minutes = (minutes < 0) ? 59 : minutes;
        //            minutes = (minutes < 10) ? '0' + minutes : minutes;
        //            hours = (hours < 10) ? '0' + hours : hours;
        //            $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
        //            timer2 = hours + ':' + minutes + ':' + seconds;
        //        }
        //    }, 1000);
        //}


        $(document).ready(function () {
            $("#txtPassword").keyup(function () {
                $("#divMessage").html("");
            });

            $('#unlock-btn').click(function () {
                $('#msg').html('');
                $('#unlockMessage').addClass('hide');
                $('#unlock-input').removeClass('hide');
            });

            if (hdnAccountLockedPop == 1) {
                $('#modalAccountLockedPop').modal({
                    keyboard: false,
                    backdrop: "static"
                });
            }
            if (hdnLoginAttempts >= 3 && hdnLoginLocked == 1) {
                //$('#btnLogin').attr('disabled', 'disabled');
                $('#divMessage').html("<i class='fa fa-close icon'></i> Too many attempts. Account is Locked.");
                //StartTimer();
            }
            $('#myCarousel').carousel({
                interval: 3000
            });

            var clickEvent = false;
            $('#myCarousel').on('click', '.nav-my a', function () {
                clickEvent = true;
                $('.nav-my li').removeClass('active');
                $(this).parent().addClass('active');
            }).on('slid.bs.carousel', function (e) {
                if (!clickEvent) {
                    var count = $('.nav-my').children().length - 1;
                    var current = $('.nav-my li.active');
                    current.removeClass('active').next().addClass('active');
                    var id = parseInt(current.data('slide-to'));
                    if (count == id) {
                        $('.nav-my li').first().addClass('active');
                    }
                }
                clickEvent = false;
            });
            //announcement
            $('.announcement-wrapper .announcement-slidetext:gt(0)').hide();
            setInterval(function () {
                console.log($('.announcement-wrapper .announcement-slidetext').length);
                if ($('.announcement-wrapper .announcement-slidetext').length > 1) {
                    $('.announcement-wrapper .announcement-slidetext:first-child').fadeOut(3000).next('.announcement-slidetext').fadeIn(3000)
                        .end().appendTo('.announcement-wrapper');
                }
                else {
                    $('.announcement-wrapper .announcement-slidetext:first-child').fadeOut(3000);
                    $('.announcement-wrapper .announcement-slidetext:first-child').fadeIn(3000)
                    //.end().appendTo('.announcement-wrapper');
                }
            }, 5000);
            //setTimeout(fadeOutAnnouncement, 5000000);
        });
        //function fadeOutAnnouncement() {
        //    $(".announcement").fadeOut(1000000);
        //    setTimeout(fadeInAnnouncement, 4000000);
        //}
        //function fadeInAnnouncement() {
        //    $(".announcement").fadeIn(100000000);
        //    setTimeout(fadeOutAnnouncement, 400000000);
        //}
        /**
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        */
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });
    </script>

</asp:Content>
