﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class TransferFunds : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IMaHolderClassService> lazyMaHolderClassServiceObj = new Lazy<IMaHolderClassService>(() => new MaHolderClassService());

        public static IMaHolderClassService IMaHolderClassService { get { return lazyMaHolderClassServiceObj.Value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            String fundCode = Request.QueryString["fundCode"];
            if (fundCode == null || fundCode == "")
            {
                if (Session["transFundCode"] != null)
                {
                    fundCode = Session["transFundCode"].ToString();
                }
            }
            else
            {
                Session["transFundCode"] = fundCode;
            }
            if (Session["user"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=TransferFunds.aspx'", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx?redirectUrl=TransferFunds.aspx'", true);
                }
            }
            else
            {
                Online_Transactions ot = new Online_Transactions();
                bool isSATUpdated = ot.CheckIfSATUpdated();
                if (!isSATUpdated)
                {
                    RunScript(fundCode);
                }
            }
            if (Session["isVerified"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=TransferFunds.aspx'", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx?redirectUrl=TransferFunds.aspx'", true);
                }
            }
            else
            {
                String isVerified = (Session["isVerified"].ToString());
                if (isVerified == "0")
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=TransferFunds.aspx'", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx?redirectUrl=TransferFunds.aspx'", true);
                    }
                }
            }
            UtmcFundInformation utmcFundInformation = null;
            if (fundCode != null)
            {
                String propName = nameof(UtmcFundInformation.FundCode);
                utmcFundInformation = IUtmcFundInformationService.GetDataByPropertyName(propName, fundCode, true, 0, 0, false).FirstOrDefault();
                //propName = nameof(FundInfo.FundCode);
                //FundInfo fundInfo = IFundInfoService.GetDataByPropertyName(propName, utmcFundInformation.IpdFundCode).FirstOrDefault();
            }
            if (!IsPostBack)
            {
                Session["cartItems"] = null;
                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                    UserAccount primaryAcc = new UserAccount();
                    if (Session["SelectedAccountHolderId"] != null)
                    {
                        primaryAcc = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == Session["SelectedAccountHolderId"].ToString()).FirstOrDefault();
                        hdnAccountPlan.Value = CustomValues.isEPF(primaryAcc.MaHolderRegIdMaHolderReg.HolderCls) ? "EPF" : "CASH";
                    }
                    maAccNumber.InnerHtml = primaryAcc.AccountNo;
                    accName.InnerHtml = primaryAcc.MaHolderRegIdMaHolderReg.Name1;
                    accType.InnerHtml = CustomValues.isEPF(primaryAcc.MaHolderRegIdMaHolderReg.HolderCls) ? "EPF" : "CASH";

                    List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = (List<UtmcDetailedMemberInvestment>)Portfolio.GetMAAccountUtmcMemberInvestmentsTrans(primaryAcc.MaHolderRegId.ToString());
                    StringBuilder sb = new StringBuilder();
                    foreach (UtmcDetailedMemberInvestment utmcDetailedMemberInvestment in utmcDetailedMemberInvestments)
                    {
                        FundDetailedInformation fundDetails = utmcDetailedMemberInvestment.fundDetailedInformation;
                        sb.Append(@"<tr>
                                        <td>" + fundDetails.UtmcFundInformation.FundCode + @" - " + fundDetails.UtmcFundInformation.FundName + @"</td>
                                        <td class='unitFormat'>" + utmcDetailedMemberInvestment.utmcMemberInvestments[0].Units + @"</td>
                                        <td class='currencyFormat'>" + utmcDetailedMemberInvestment.utmcMemberInvestments[0].MarketValue + @"</td>
                                        <td class='unitFormatNoSymbol'>" + fundDetails.CurrentUnitPrice + @"</td>
                                    </tr>");
                    }
                    tbodyAccountFunds.InnerHtml = sb.ToString();

                    List<UtmcFundInformation> UTMCFundInformations = IUtmcFundInformationService.GetData(0, 0, false);
                    ddlFundsList.Items.Add(new ListItem
                    {
                        Text = "Select Fund",
                        Value = ""
                    });
                    if (CustomValues.isEPF(primaryAcc.MaHolderRegIdMaHolderReg.HolderCls))
                    {
                        UTMCFundInformations = UTMCFundInformations.Where(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().IsEpfApproved == 1).ToList();
                    }
                    Online_Transactions ot = new Online_Transactions();

                    string userAccSATGroupByScore = ot.GetGroupByScore(primaryAcc.SatScore);
                    int group = userAccSATGroupByScore[1];

                    //List<UtmcFundInformation> UTMCFundInformationsForAcc = UTMCFundInformations.Where(x => utmcDetailedMemberInvestments.Select(y => y.fundDetailedInformation.UtmcFundInformation.Id).Contains(x.Id)).ToList();
                    //List<UtmcFundInformation> UTMCFundInformationsNotInAcc = UTMCFundInformations.Except(UTMCFundInformationsForAcc).ToList();

                    UTMCFundInformations = UTMCFundInformations.Where(x => utmcDetailedMemberInvestments.Select(y => y.fundDetailedInformation.UtmcFundInformation.Id).Contains(x.Id)).ToList();

                    foreach (UtmcFundInformation uFI in UTMCFundInformations)
                    {
                        ListItem listItem = new ListItem
                        {
                            Text = uFI.FundCode + " - " + uFI.FundName,
                            Value = uFI.Id.ToString(),
                        };
                        listItem.Attributes.Add("data-holding-units", utmcDetailedMemberInvestments.Where(x => x.fundDetailedInformation.UtmcFundInformation.Id == uFI.Id).FirstOrDefault().utmcMemberInvestments[0].Units.ToString());
                        if (utmcFundInformation != null)
                        {
                            if (uFI.Id == utmcFundInformation.Id)
                            {
                                listItem.Selected = true;
                                ddlFundsList.Items.Add(listItem);
                            }
                            else
                            {
                                ddlFundsList.Items.Add(listItem);
                            }
                        }
                        else
                        {
                            ddlFundsList.Items.Add(listItem);
                        }
                    }
                    //BindCart();
                }
            }
        }

        public void BindCart()
        {
            StringBuilder sb = new StringBuilder();
            List<Cart> cartItems = GetAllCartItems();
            foreach (Cart cartItem in cartItems)
            {
                sb.Append(@"<tr>
                                        <td>" + cartItem.UtmcFundInformation.FundCode + @" - " + cartItem.UtmcFundInformation.FundName + @"</td>
                                        <td class='unitFormat'>" + cartItem.Units + @"</td>
                                        <td><a href='javascript:;' 
                                                data-fundId=" + cartItem.UtmcFundInformation.Id + @" 
                                                class='btn btn-sm removeFund' 
                                                data-toggle='tooltip' 
                                                title='Remove'><i class='fa fa-times'></i></a></td>
                                    </tr>");
            }
            if (cartItems.Count == 0)
            {
                sb.Append("<tr><td colspan='3'>No Funds Selected</td></tr>");
            }
            tbodySelectedFunds.InnerHtml = sb.ToString();
            tbodySelectedFunds1.InnerHtml = sb.ToString();
            Decimal totalSales = cartItems.Sum(x => x.Units);
            tdTotalSwitchingUnits.InnerHtml = totalSales.ToString();
            tdTotalSwitchingUnits1.InnerHtml = totalSales.ToString();
        }

        public void RunScript(string fundCode)
        {
            if (fundCode != null && fundCode != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "if(confirm('Please Proceed to Update SAT.')) window.location.href='SAT-Form.aspx?redirectUrl=TransferFunds.aspx'; else window.location.href='Funds-listing.aspx'", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "if(confirm('Please Proceed to Update SAT.')) window.location.href='SAT-Form.aspx?redirectUrl=TransferFunds.aspx'; else window.location.href='Index.aspx'", true);
            }
        }

        public static List<Cart> GetAllCartItems()
        {
            List<Cart> cartItems = new List<Cart>();
            if (HttpContext.Current.Session["user"] != null)
            {
                if (HttpContext.Current.Session["cartItems"] != null)
                {
                    cartItems = (List<Cart>)HttpContext.Current.Session["cartItems"];
                }
            }
            return cartItems;
        }

        public static void UpdateCart(List<Cart> cartItems)
        {
            HttpContext.Current.Session["cartItems"] = cartItems;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetAccountDetails(string accNo)
        {
            int isError = 0;
            String ErrorMessage = "";
            User user = (User)System.Web.HttpContext.Current.Session["user"];
            List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
            UserAccount primaryAcc = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();
            UserAccount userAccount = new UserAccount();
            MaHolderClass holderClass = new MaHolderClass();
            if (primaryAcc.AccountNo == accNo.Trim())
            {
                isError = 1;
                ErrorMessage = "Cannot transfer to your own Account";
            }
            else
            {
                userAccount = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.AccountNo), accNo, true, 0, 0, false).FirstOrDefault();
                if (userAccount != null)
                {
                    holderClass = IMaHolderClassService.GetDataByPropertyName(nameof(MaHolderClass.Code), userAccount.MaHolderRegIdMaHolderReg.HolderCls, true, 0, 0, false).FirstOrDefault();
                }
                else
                {
                    isError = 1;
                    ErrorMessage = "Account Not found";
                }
            }
            return new { userAccount = userAccount, holderClass = holderClass, isError = isError, ErrorMessage = ErrorMessage };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object BindFunds()
        {
            User user = (User)HttpContext.Current.Session["user"];
            List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
            UserAccount primaryAcc = new UserAccount();
            if (HttpContext.Current.Session["SelectedAccountHolderId"] != null)
            {
                primaryAcc = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == HttpContext.Current.Session["SelectedAccountHolderId"].ToString()).FirstOrDefault();
            }
            List<Cart> cartItems = GetAllCartItems().Where(x => x.userAccount.Id == primaryAcc.Id).ToList();
            int id = 1;
            cartItems.ForEach(x => {
                x.Id = id;
                id++;
            });
            UpdateCart(cartItems);
            Decimal totalSales = cartItems.Sum(x => x.Units);
            return new
            {
                CartItems = cartItems,
                TotalUnits = totalSales
            };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object AddFund(Int32 fundId, Int32 ToAccountId, Decimal units)
        {
            User user = (User)HttpContext.Current.Session["user"];
            List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
            UserAccount primaryAcc = new UserAccount();
            if (HttpContext.Current.Session["SelectedAccountHolderId"] != null)
            {
                primaryAcc = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == HttpContext.Current.Session["SelectedAccountHolderId"].ToString()).FirstOrDefault();
            }
            List<Cart> cartItems = GetAllCartItems();
            bool isAddNow = false;
            if (cartItems.Count > 0)
            {
                Cart matchedItem = cartItems.Where(x => x.UtmcFundInformation.Id == fundId).FirstOrDefault();
                if (matchedItem != null)
                {
                    isAddNow = false;
                }
                else
                {
                    isAddNow = true;
                }
            }
            else
            {
                isAddNow = true;
            }
            if (isAddNow)
            {
                cartItems.Add(new Cart
                {
                    UtmcFundInformation = IUtmcFundInformationService.GetSingle(fundId),
                    userAccount2 = IUserAccountService.GetSingle(ToAccountId),
                    Units = units,
                    Type = 4,
                    userAccount = primaryAcc
                });
            }
            else
            {
                cartItems.ForEach(x =>
                {
                    if (x.UtmcFundInformation.Id == fundId)
                        x.Units = units;
                });
            }
            UpdateCart(cartItems);
            return BindFunds();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object RemoveFund(Int32 Id)
        {
            User user = (User)HttpContext.Current.Session["user"];
            List<UserAccount> userAccounts = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
            UserAccount primaryAcc = new UserAccount();
            if (HttpContext.Current.Session["SelectedAccountHolderId"] != null)
            {
                primaryAcc = userAccounts.Where(x => x.MaHolderRegIdMaHolderReg.Id.ToString() == HttpContext.Current.Session["SelectedAccountHolderId"].ToString()).FirstOrDefault();
            }
            List<Cart> cartItems = GetAllCartItems();
            if (cartItems.Count > 0)
            {
                List<Cart> remainingItems = cartItems.Where(x => x.Id != Id).ToList();
                cartItems = remainingItems;
            }
            UpdateCart(cartItems);
            return BindFunds();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("Show-Cart.aspx");
        }
    }
}