﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace Admin
{
    public partial class Enrollments : System.Web.UI.Page
    {
        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT ate.id, ate.is_reported, att.Id, att.is_online, att.topic,att.synopsis, u.id, u.username as AgentName, utd.name as AgentType, u.agent_code, ate.created_date FROM");
            string mainQCount = (@"select count(*) from ");
            filter.Append(@" agent_training_enrollments ate 
                                join agent_training_topics att on ate.agent_training_topic_id = att.id 
                                join users u on ate.user_id = u.id 
                                join user_types ut on ut.user_id = u.ID 
                                join user_types_def utd on ut.user_type_id = utd.id and utd.id != '1' 
                                where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (att.topic like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or att.synopsis like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or AgentType like '%" + Search.Value.Trim() + "%'");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                        filter.Append(" and ate.is_reported in ('" + FilterValue.Value + "')");
                    else
                        filter.Append(@" and ate.is_reported in ('0','1') ");
                }
                else
                {
                    filter.Append(@"and ate.is_reported in ('0','1') ");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ddlCourseList"]))
                {
                    ddlCourseList.SelectedValue = Request.QueryString["ddlCourseList"].ToString();
                    var selectedCourse = Request.QueryString["ddlCourseList"].ToString(); ;
                    filter.Append(" and att.Id in ('" + selectedCourse + "')");
                }



                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                {
                    if (hdnOrderByColumn.Value == "" || string.IsNullOrEmpty(Request.QueryString["hdnOrderByColumn"]))
                    {
                        filter.Append("order by ate.created_date desc ");
                    }
                    else
                    {
                        filter.Append("order by ate.created_date desc ");
                    }
                }


                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                //bind course list data into drop down list
                string queryCourseList = (@" select * from agent_training_topics where is_active=1 and status=1 ");
                Response responseCourseList = GenericService.GetDataByQuery(queryCourseList, 0, 0, false, null, false, null, true);
                if (responseCourseList.IsSuccess)
                {
                    var CourseListDyn = responseCourseList.Data;
                    var responseCourseListJSON = JsonConvert.SerializeObject(CourseListDyn);
                    List<DiOTP.Utility.CourseListing> courseList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CourseListing>>(responseCourseListJSON);

                    ddlCourseList.DataTextField = "Topic";
                    ddlCourseList.DataValueField = "Id";
                    ddlCourseList.DataSource = courseList;
                    ddlCourseList.DataBind();

                    ddlCourseList.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCourseList.Message + "\", '');", true);
                }

                Response responseMAHolderRegList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseMAHolderRegList.IsSuccess)
                {
                    var AddressDyn = responseMAHolderRegList.Data;
                    var responseJSON = JsonConvert.SerializeObject(AddressDyn);
                    List<DiOTP.Utility.Enrollments> enrollments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Enrollments>>(responseJSON);


                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (DiOTP.Utility.Enrollments a in enrollments)
                    {
                        asb.Append(@"<tr>
                                                <td class='icheck'>
                                                    <label>" + index + @"</label><br/>
                                                    </span>
                                                </td>
                                                <td>" + a.Topic + @"</td>
                                                <td>" + (a.IsOnline == 1 ? "Online" : (a.IsOnline == 0 ? "Offline" : "")) + @"</td>
                                                <td>" + a.Synopsis + @"</td>
                                                <td>" + a.AgentCode + @"</td>
                                                <td>" + a.AgentName + @"</td>
                                                <td>" + a.AgentType + @"</td>
                                                <td>" + a.CreatedDate + @"</td>
                                                <td>" + (a.IsReported == 1 ? "Processed" : (a.IsReported == 0 ? "Unprocessed" : "")) + @"</td>
                                                </tr>"
                                    );
                        index++;


                    }
                    usersTbody.InnerHtml = asb.ToString();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert(\"" + responseMAHolderRegList.Message + "\");", true);
                }
            }
        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {

            string mainFilter = filter.Split(',')[0];
            string ddlFilter = String.IsNullOrEmpty(filter)? "":filter.Split(',')[1];

            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {

                string tempPath = Path.GetTempPath() + "Training Enrollment_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Training Enrollment List");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Course Name", "Course Type", "Course Synopsis", "Agent Code", "Agent Name", "Agent Type", "Enrollment Date", "Attended", "Passed" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Training Enrollment List"];

                string docDetails = "Training Enrollment List";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    StringBuilder jfilter = new StringBuilder();


                    filterQuery.Append(@"SELECT ate.id, ate.is_attended , ate.is_passed, ate.is_reported , att.Id, att.is_online, att.topic,att.synopsis, u.id, u.username as agent_name, utd.name as agent_type, u.agent_code, ate.created_date, att.is_assessment FROM
                                            agent_training_enrollments ate 
                                            join agent_training_topics att on ate.agent_training_topic_id = att.id 
                                            join users u on ate.user_id = u.id 
                                            join user_types ut on ut.user_id = u.ID 
                                            join user_types_def utd on ut.user_type_id = utd.id and utd.id != '1' 
                                            where 1=1 ");


                    if (!string.IsNullOrEmpty(mainFilter) && mainFilter != "")
                    {

                        docDetails += " | Status: " + (mainFilter == "0" ? "Unprocessed" : (mainFilter == "1" ? "Processed" : "-")) + " ";
                        if (mainFilter != "All")
                            filterQuery.Append(" and ate.is_reported in ('" + mainFilter + "')");
                        else
                            filterQuery.Append(@" and ate.is_reported in ('0','1') ");
                    }
                    else
                    {
                        docDetails += " | Status: All";
                        filterQuery.Append(@"and ate.is_reported in ('0','1') ");

                    }

                    if (!string.IsNullOrEmpty(ddlFilter) && ddlFilter != "") {
                        filterQuery.Append("and att.Id in ('" + ddlFilter + "')");
                    }

                    int skip = 0, take = 20;
                    filterQuery.Append(@"order by ate.created_date desc ");
                    
                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseAddress = GenericService.GetDataByQuery(filterQuery.ToString(), skip, take, false, null, false, null, false);
                    if (responseAddress.IsSuccess)
                    {
                        var EnrollmentsDyn = responseAddress.Data;
                        var responseJSON = JsonConvert.SerializeObject(EnrollmentsDyn);
                        List<DiOTP.Utility.Enrollments> Enrollments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Enrollments>>(responseJSON);

                        StringBuilder asb = new StringBuilder();

                        int index = 1;

                        foreach (DiOTP.Utility.Enrollments e in Enrollments)
                        {

                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = e.Topic;
                            worksheet.Cells[row, 3].Value = (e.IsOnline == 1 ? "Online" : (e.IsOnline == 0 ? "Offline" : "-"));
                            worksheet.Cells[row, 4].Value = e.Synopsis;
                            worksheet.Cells[row, 5].Value = e.AgentCode;
                            worksheet.Cells[row, 6].Value = e.AgentName;
                            worksheet.Cells[row, 7].Value = e.AgentType;
                            worksheet.Cells[row, 8].Value = e.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");


                            var range = ExcelRange.GetAddress(row, 9);
                            var values = worksheet.DataValidations.AddListValidation(range);
                            values.Formula.Values.Add("Select");
                            values.Formula.Values.Add("Yes");
                            values.Formula.Values.Add("No");
                            
                            if (e.IsReported == 1)
                            {
                                //if pass data before
                                //worksheet.Cells[row, 9].Value ;
                                worksheet.Cells[row, 9].Value = e.isAttended == 1 ? "Yes" : "No";

                                if (e.IsAssessment == 1) {
                                    var range2 = ExcelRange.GetAddress(row, 10);
                                    var values2 = worksheet.DataValidations.AddListValidation(range2);
                                    values2.Formula.Values.Add("Select");
                                    values2.Formula.Values.Add("Yes");
                                    values2.Formula.Values.Add("No");
                                    worksheet.Cells[row, 10].Value = e.isPassed == 1 ? "Yes" : "No";
                                }
                                else
                                {
                                    worksheet.Cells[row, 10].Value = "N/A";
                                    worksheet.Cells[row, 10].Style.Locked = true;
                                    worksheet.Cells[row, 10].Style.Fill.PatternType = ExcelFillStyle.Gray125;
                                }
                            }
                            else {
                                worksheet.Cells[row, 9].Value = "Select";

                                if (e.IsAssessment == 1)
                                {
                                    var range2 = ExcelRange.GetAddress(row, 10);
                                    var values2 = worksheet.DataValidations.AddListValidation(range2);
                                    values2.Formula.Values.Add("Select");
                                    values2.Formula.Values.Add("Yes");
                                    values2.Formula.Values.Add("No");
                                    worksheet.Cells[row, 10].Value = "Select";
                                }
                                else
                                {
                                    worksheet.Cells[row, 10].Value = "N/A";
                                    worksheet.Cells[row, 10].Style.Locked = true;
                                    worksheet.Cells[row, 10].Style.Fill.PatternType = ExcelFillStyle.Gray125;
                                }
                            }
                            
                            worksheet.Cells[row, 11].Value = e.Id;
                            worksheet.Cells[row, 11].Style.Font.Color.SetColor(Color.White);
                            row++;
                            index++;
                        }

                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Training Enrollment List downloaded",
                        TableName = "agent_enrollments",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    // Release COM objects (very important!)

                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "eApexIs_training_enrollment_list" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object Upload(UploadFile[] statements)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            List<string> responseMsgs = new List<string>();
            int idx = 1;
            try
            {
                foreach (UploadFile f in statements)
                {
                    string ex = Path.GetExtension(f.Name).ToUpper();
                    if (ex == ".XLSX" || ex == ".XLS")
                    {
                        string dirPath = HttpContext.Current.Server.MapPath("/Content/EnrollmentUploads/");
                        string filename = Path.GetFileName(f.Name);
                        //dirPath += accNo + "/" + statementDate + "/";
                        string filePath = dirPath + filename;
                        Directory.CreateDirectory(dirPath);
                        if (File.Exists(filePath))
                            File.Delete(filePath);
                        byte[] result = Convert.FromBase64String(f.Base64String.Replace("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,", ""));
                        File.WriteAllBytes(filePath, result);

                        FileInfo existingFile = new FileInfo(filePath);
                        using (ExcelPackage package = new ExcelPackage(existingFile))
                        {
                            //get the first worksheet in the workbook
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                            int colCount = worksheet.Dimension.End.Column;  //get Column Count
                            int rowCount = worksheet.Dimension.End.Row;     //get row count
                            for (int row = 3; row <= rowCount; row++)
                            {
                                //for (int col = 1; col <= colCount; col++)
                                //{
                                //    Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + worksheet.Cells[row, col].Value?.ToString().Trim());
                                //}
                                String HiddenId = worksheet.Cells[row, 11].Value.ToString();

                                object index = worksheet.Cells[row, 1].Value;
                                object Topic = worksheet.Cells[row, 2].Value;
                                object type = worksheet.Cells[row, 3].Value;
                                object Synopsis = worksheet.Cells[row, 4].Value;
                                object AgentCode = worksheet.Cells[row, 5].Value;
                                object AgentName = worksheet.Cells[row, 6].Value;
                                object AgentType = worksheet.Cells[row, 7].Value;
                                object CreatedDate = worksheet.Cells[row, 8].Value;
                                String Attended = worksheet.Cells[row, 9].Value.ToString();
                                String Passed = worksheet.Cells[row, 10].Value.ToString();

                                StringBuilder filterQuery = new StringBuilder();
                                filterQuery.Append(@"SELECT ate.*, att.is_assessment FROM
                                            agent_training_enrollments ate 
                                            join agent_training_topics att on ate.agent_training_topic_id = att.id 
                                            join users u on ate.user_id = u.id 
                                            join user_types ut on ut.user_id = u.ID 
                                            join user_types_def utd on ut.user_type_id = utd.id and utd.id != '1' 
                                            where ate.id = '" + HiddenId + @"' ");
                                Response responseEnrollment = GenericService.GetDataByQuery(filterQuery.ToString(), 0, 0, false, null, false, null, false);
                                if (responseEnrollment.IsSuccess)
                                {
                                    var EnrollmentsDyn = responseEnrollment.Data;
                                    var responseJSON = JsonConvert.SerializeObject(EnrollmentsDyn);
                                    List<AgentTrainingEnrollment> Enrollments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentTrainingEnrollment>>(responseJSON);
                                    if (Enrollments.Count == 1)
                                    {
                                        AgentTrainingEnrollment enrollment = Enrollments.FirstOrDefault();
                                        bool allPassed = true;
                                        if (Attended == "Select")
                                        {
                                            allPassed = false;
                                            response.IsSuccess = false;
                                            responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + AgentCode + "</td><td>Attendance not selected</td></tr>");
                                        }
                                        else if (Attended != "Yes" && Attended != "No")
                                        {
                                            allPassed = false;
                                            response.IsSuccess = false;
                                            responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + AgentCode + "</td><td> Invalid Attended data!</td></tr>");
                                        }
                                        else
                                        {
                                            if (enrollment.IsAssessment == 1)
                                            {
                                                if (Passed == "Select")
                                                {
                                                    allPassed = false;
                                                    response.IsSuccess = false;
                                                    responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + AgentCode + "</td><td>Passed data not selected</td></tr>");
                                                }
                                                else if (Passed != "Yes" && Passed != "No")
                                                {
                                                    allPassed = false;
                                                    response.IsSuccess = false;
                                                    responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + AgentCode + "</td><td> Invalid Passed data!</td></tr>");
                                                }
                                            }
                                        }
                                        if (allPassed)
                                        {
                                            enrollment.IsAttended = (Attended == "Yes" ? 1 : 0);
                                            if (enrollment.IsAssessment == 1)
                                                enrollment.IsPassed = (Passed == "Yes" ? 1 : 0);
                                            enrollment.IsReported = 1;
                                            Response responsePost = GenericService.UpdateData<AgentTrainingEnrollment>(enrollment);
                                            if (responsePost.IsSuccess)
                                            {
                                                responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-success'><i class='fa fa-check'></i></label></td><td><strong class='text-success'>" + AgentCode + "</strong></td><td> <strong class='text-success'>" + Attended + @"</strong></td><td> <strong class='text-success'>" + Passed + @"</strong></td></tr>");
                                            }
                                            else
                                            {
                                                response.IsSuccess = false;
                                                responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + AgentCode + "</td><td> <strong class='text-success'>" + Attended + @"</strong></td><td> <strong class='text-success'>" + Passed + @"</strong></td><td> " + responsePost.Message + @"!</td></tr>");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + AgentCode + "</td><td> <strong class='text-success'>" + Attended + @"</strong></td><td> <strong class='text-success'>" + Passed + @"</strong></td><td> " + responseEnrollment.Message + @"!</td></tr>");
                                }
                                idx++;
                            }
                        }


                        //responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-success'><i class='fa fa-check'></i></label></td><td><strong class='text-success'>" + f.Name + "</strong></td><td> <strong class='text-success'>Successfully uploaded!</strong></td></tr>");
                        //Audit Log starts here
                        //AdminLogMain alm = new AdminLogMain()
                        //{
                        //    Description = f.Name + " succesfully uploaded",
                        //    TableName = "user_order_statements",
                        //    UpdatedDate = DateTime.Now,
                        //    UserId = loginUser.Id,
                        //};
                        //Response responseLog = IAdminLogMainService.PostData(alm);
                        //if (!responseLog.IsSuccess)
                        //{
                        //    //Audit log failed
                        //}
                        //else
                        //{

                        //}

                        //Audit Log Ends here
                        idx++;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + f.Name + "</td><td> File type is invalid!</td></tr>");
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                responseMsgs.Add("<tr><td colspan='4' class='text-center'><label class='label label-danger'><i class='fa fa-times'></i></label>" + ex.Message + "</td></tr>");
            }
            response.Message = String.Join("<br/>", responseMsgs.ToArray());
            return response;
        }

        public class UploadFile
        {
            public string Base64String { get; set; }
            public string Name { get; set; }
        }
    }
}