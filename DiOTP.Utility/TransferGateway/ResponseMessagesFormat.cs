﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.TransferGateway
{
    public class ResponseMessagesFormat
    {
    }

    public class ResponseMessagesOrderListFormatBodyTCTASC
    {
        public String TRXNCODE { get; set; }
        public String TRXNNO { get; set; }
        public DateTime VALIDATEDTIMESTAMP { get; set; }
        public Decimal WDWAMOUNT { get; set; }
        public List<THIRDPARTY> THIRDPARTYS { get; set; }
    }

    public class ResponseMessagesOrderListFormatBodySR
    {
        public String RESERVE1 { get; set; }
        public String RESERVE2 { get; set; }
        public String RESERVE3 { get; set; }
    }
}
