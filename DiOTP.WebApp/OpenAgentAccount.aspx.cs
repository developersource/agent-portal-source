﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class OpenAgentAccount : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAgentSignupService> lazyAgentSignupServiceObj = new Lazy<IAgentSignupService>(() => new AgentSignupService());
        public static IAgentSignupService IAgentSignupService { get { return lazyAgentSignupServiceObj.Value; } }

        private static readonly Lazy<IAgentPositionService> lazyAgentPositionObj = new Lazy<IAgentPositionService>(() => new AgentPositionService());
        public static IAgentPositionService IAgentPositionService { get { return lazyAgentPositionObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());
        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());
        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }
        public static List<BanksDef> bankdefs = new List<BanksDef>();

        protected void Page_Load(object sender, EventArgs e)
        {
            User sessionUser = (User)Session["user"];
            if (sessionUser != null)
            {
                if (!IsPostBack)
                {

                    string queryRegion = (@" select * from regions where is_active=1 and status=1 ");
                    Response responseRegionList = GenericService.GetDataByQuery(queryRegion, 0, 0, false, null, false, null, true);
                    if (responseRegionList.IsSuccess)
                    {
                        var RegionsDyn = responseRegionList.Data;
                        var responseRegionsJSON = JsonConvert.SerializeObject(RegionsDyn);
                        List<Region> regionsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Region>>(responseRegionsJSON);

                        ddlRegion.DataTextField = "name";
                        ddlRegion.DataValueField = "id";
                        ddlRegion.DataSource = regionsList;
                        ddlRegion.DataBind();

                        ddlRegion.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRegionList.Message + "\", '');", true);
                    }

                    string queryAO = (@" select * from agent_offices where is_active=1 and status=1 ");
                    Response responseAOList = GenericService.GetDataByQuery(queryAO, 0, 0, false, null, false, null, true);
                    if (responseAOList.IsSuccess)
                    {
                        var AOsDyn = responseAOList.Data;
                        var responseAOsJSON = JsonConvert.SerializeObject(AOsDyn);
                        List<AgentOffice> aosList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentOffice>>(responseAOsJSON);

                        aosList.ForEach(x =>
                        {
                            ListItem listItem = new ListItem();
                            listItem.Text = x.name;
                            listItem.Value = x.id.ToString();
                            listItem.Attributes.Add("region_id", x.region_id);
                            ddlOffice.Items.Add(listItem);
                        });

                        //ddlOffice.DataTextField = "name";
                        //ddlOffice.DataValueField = "id";
                        //ddlOffice.DataSource = aosList;
                        //ddlOffice.DataBind();

                        ddlOffice.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAOList.Message + "\", '');", true);
                    }

                    Response resRaces = ServicesManager.GetRaceData();
                    if (resRaces.IsSuccess)
                    {
                        List<RaceDTO> raceDTOs = (List<RaceDTO>)resRaces.Data;
                        ddlRace.DataTextField = "SDESC";
                        ddlRace.DataValueField = "RACECODE";
                        ddlRace.DataSource = raceDTOs;
                        ddlRace.DataBind();

                        ddlRace.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + resRaces.Message + "\", '');", true);
                    }

                    Response responseCDList = ICountriesDefService.GetData(0, 0, false);
                    if (responseCDList.IsSuccess)
                    {
                        List<CountriesDef> countries = (List<CountriesDef>)responseCDList.Data;
                        ddlCountry.DataTextField = "Name";
                        ddlCountry.DataValueField = "Code";
                        ddlCountry.DataSource = countries;
                        ddlCountry.DataBind();

                        ddlCountry.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCDList.Message + "\", '');", true);
                    }

                    Response responseStates = IStatesDefService.GetData(0, 0, false);
                    if (responseStates.IsSuccess)
                    {
                        List<StatesDef> stateList = (List<StatesDef>)responseStates.Data;

                        ddlState.DataTextField = "Name";
                        ddlState.DataValueField = "Code";
                        ddlState.DataSource = stateList;
                        ddlState.DataBind();

                        ddlState.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseStates.Message + "\", '');", true);
                    }

                    Response responseNDList = INationalityDefService.GetData(0, 0, true);
                    if (responseNDList.IsSuccess)
                    {
                        List<NationalityDef> nds = (List<NationalityDef>)responseNDList.Data;
                        ddlNationality.DataTextField = "Name";
                        ddlNationality.DataValueField = "Code";
                        ddlNationality.DataSource = nds;
                        ddlNationality.DataBind();

                        ddlNationality.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNDList.Message + "\", '');", true);
                    }

                    //Response resCurrencies = ServicesManager.GetCurrencyData();
                    //if (resCurrencies.IsSuccess)
                    //{
                    //    List<CurrencyDTO> currencyDTOs = (List<CurrencyDTO>)resCurrencies.Data;
                    //    ddlCurrency.DataTextField = "SDESC";
                    //    ddlCurrency.DataValueField = "CURRENCY";
                    //    ddlCurrency.DataSource = currencyDTOs;
                    //    ddlCurrency.DataBind();

                    //    ddlCurrency.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    //}
                    //else
                    //{                       
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + resCurrencies.Message + "\", '');", true);
                    //}

                    Response responseBDList = IBanksDefService.GetDataByFilter(" status = 1 ", 0, 0, true);
                    if (responseBDList.IsSuccess)
                    {
                        bankdefs = (List<BanksDef>)responseBDList.Data;
                        bankdefs = bankdefs.OrderBy(x => x.Name).ToList();
                        foreach (BanksDef x in bankdefs)
                        {
                            ListItem listItem = new ListItem
                            {
                                Text = x.Name,
                                Value = x.Id.ToString()
                            };
                            //listItem.Attributes.Add("accountnumlength", x.NoFormat);
                            ddlBankList.Items.Add(listItem);
                            ddlBankNameI.Items.Add(listItem);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseBDList.Message + "\", '');", true);
                    }

                    
                    

                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + sessionUser.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount primaryAcc = userAccounts.FirstOrDefault(x => x.IsPrimary == 1);
                        if (primaryAcc != null && primaryAcc.Id != 0)
                        {
                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                            if (responseMHR.IsSuccess)
                            {
                                MaHolderReg maHolderReg = (MaHolderReg)responseMHR.Data;
                                txtEmail.Text = sessionUser.EmailId;
                                txtAgentName.Text = maHolderReg.Name1;
                                txtIDNo.Text = sessionUser.IdNo;
                                DateTime BDT = DateTime.ParseExact(maHolderReg.BirthDt, "M/d/yyyy", CultureInfo.InvariantCulture);
                                txtDOB.Text = BDT.ToString("dd/MM/yyyy");
                                ddlSex.SelectedValue = (maHolderReg.Sex == "M" ? "M" : (maHolderReg.Sex == "F" ? "F" : ""));
                                ddlRace.SelectedValue = maHolderReg.Race;
                                txtAddrLine1.Text = maHolderReg.Addr1;
                                txtAddrLine2.Text = maHolderReg.Addr2;
                                txtAddrLine3.Text = maHolderReg.Addr3;
                                txtAddrLine4.Text = maHolderReg.Addr4;
                                txtPostCode.Text = maHolderReg.Postcode.HasValue ? maHolderReg.Postcode.Value.ToString() : "";
                                ddlCountry.SelectedValue = maHolderReg.CountryRes;
                                ddlState.SelectedValue = maHolderReg.StateCode.HasValue ? maHolderReg.StateCode.Value.ToString() : "";
                                txtTelNoOffice.Text = maHolderReg.OffPhoneNo;
                                txtTelNoHome.Text = maHolderReg.TelNo;
                                txtHandPhoneNo.Text = maHolderReg.HandPhoneNo;
                                //txtContactPerson.Text = maHolderReg.ContactPerson;
                                txtIncomeTaxNo.Text = maHolderReg.IncomeTaxNo;
                                ddlNationality.SelectedValue = maHolderReg.Nationality;
                                ddlCurrency.SelectedValue = "RM";
                                if (CustomValues.GetAccounPlan(primaryAcc.HolderClass) == "EPF")
                                {
                                    txtEPFNo.Text = maHolderReg.IdNo2;
                                }
                                else
                                {
                                    UserAccount epfAccount = userAccounts.FirstOrDefault(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF");
                                    if (epfAccount != null)
                                    {
                                        Response responseMHREPF = ServicesManager.GetMaHolderRegByAccountNo(epfAccount.AccountNo);
                                        if (responseMHREPF.IsSuccess)
                                        {
                                            MaHolderReg maHolderRegEPF = (MaHolderReg)responseMHREPF.Data;
                                            txtEPFNo.Text = maHolderRegEPF.IdNo2;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHREPF.Message + "\", '');", true);
                                        }
                                    }
                                }
                                if (CustomValues.GetAccounPlan(primaryAcc.HolderClass) == "JOINT" && maHolderReg.JointRelationCode == "05")
                                {
                                    ddlMaritalStatus.SelectedValue = "Married";
                                    txtSpouseName.Text = maHolderReg.Name2;
                                    txtSpouseIDNo.Text = maHolderReg.IdNo2;
                                }
                                else
                                {
                                    UserAccount jointAccount = userAccounts.FirstOrDefault(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT");
                                    if (jointAccount != null)
                                    {
                                        Response responseMHRJ = ServicesManager.GetMaHolderRegByAccountNo(jointAccount.AccountNo);
                                        if (responseMHRJ.IsSuccess)
                                        {
                                            MaHolderReg maHolderRegJ = (MaHolderReg)responseMHRJ.Data;
                                            ddlMaritalStatus.SelectedValue = "Married";
                                            txtSpouseName.Text = maHolderRegJ.Name2;
                                            txtSpouseIDNo.Text = maHolderRegJ.IdNo2;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHRJ.Message + "\", '');", true);
                                        }
                                    }
                                }
                                //txtEPFNo.Text = maHolderReg.epf
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Something went wrong.\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                    }
                    //OLD


                    AgentReg agentRegExisting = new AgentReg();
                    string queryAgentReg = (@" select * from agent_regs where user_id=" + sessionUser.Id + " and process_status in (0,1,2,3,4,5,22,49,59) and status=1 ");
                    Response responseAgentRegList = GenericService.GetDataByQuery(queryAgentReg, 0, 0, false, null, false, null, true);
                    if (responseAgentRegList.IsSuccess)
                    {
                        var agentsDyn = responseAgentRegList.Data;
                        var responseARegJSON = JsonConvert.SerializeObject(agentsDyn);
                        List<AgentReg> agentRegs = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseARegJSON);
                        if (agentRegs.Count == 1)
                        {
                            agentRegExisting = agentRegs.FirstOrDefault();

                            //incomplete or rejected 
                            if (agentRegExisting.process_status == 0 || agentRegExisting.process_status == 59)
                            {
                                chkIsCUTEPassedYes.Checked = (agentRegExisting.is_new_agent == 0);
                                chkIsCUTEPassedNo.Checked = (agentRegExisting.is_new_agent == 1);

                                chkRecommendedAgentYes.Checked = (agentRegExisting.introducer_code != "");
                                chkRecommendedAgentNo.Checked = (agentRegExisting.introducer_code == "");
                                txtIntroCode.Text = agentRegExisting.introducer_code;

                                chkRegTypeUTS.Checked = (agentRegExisting.reg_type_uts == 1);
                                chkRegTypePRS.Checked = (agentRegExisting.reg_type_prs == 1);

                                ddlJobType.SelectedValue = agentRegExisting.job_type_id.ToString();
                                ddlOffice.SelectedValue = agentRegExisting.office_id.ToString();
                                ddlRegion.SelectedValue = ddlOffice.SelectedItem.Attributes["region_id"];



                                string queryAgentRegPersonal = (@" select * from agent_reg_personal where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                                Response responseAgentRegPersonalList = GenericService.GetDataByQuery(queryAgentRegPersonal, 0, 0, false, null, false, null, true);
                                if (responseAgentRegPersonalList.IsSuccess)
                                {
                                    var agentregPersonalsDyn = responseAgentRegPersonalList.Data;
                                    var responseAgentRegPersonalJSON = JsonConvert.SerializeObject(agentregPersonalsDyn);
                                    List<AgentRegPersonal> agentRegPersonals = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPersonal>>(responseAgentRegPersonalJSON);
                                    if (agentRegPersonals.Count == 1)
                                    {
                                        agentRegExisting.AgentRegPersonal = agentRegPersonals.FirstOrDefault();

                                        txtAgentName.Text = agentRegExisting.AgentRegPersonal.name;
                                        txtIDNo.Text = agentRegExisting.AgentRegPersonal.id_no;
                                        txtDOB.Text = agentRegExisting.AgentRegPersonal.dob.HasValue ? agentRegExisting.AgentRegPersonal.dob.Value.ToString("dd/MM/yyyy") : "";
                                        txtMotherMaidenName.Text = agentRegExisting.AgentRegPersonal.mother_maiden_name;
                                        ddlSex.SelectedValue = agentRegExisting.AgentRegPersonal.sex;
                                        ddlRace.SelectedValue = agentRegExisting.AgentRegPersonal.race;
                                        ddlReligion.SelectedValue = agentRegExisting.AgentRegPersonal.religion;
                                        txtReligionDesc.Text = agentRegExisting.AgentRegPersonal.religion_others;
                                        ddlEducation.SelectedValue = agentRegExisting.AgentRegPersonal.highest_education;
                                        txtYearsOfExperience.Text = agentRegExisting.AgentRegPersonal.years_of_experience.ToString();
                                        txtAddrLine1.Text = agentRegExisting.AgentRegPersonal.mail_addr1;
                                        txtAddrLine2.Text = agentRegExisting.AgentRegPersonal.mail_addr2;
                                        txtAddrLine3.Text = agentRegExisting.AgentRegPersonal.mail_addr3;
                                        txtAddrLine4.Text = agentRegExisting.AgentRegPersonal.mail_addr4;
                                        txtPostCode.Text = agentRegExisting.AgentRegPersonal.post_code;
                                        ddlState.SelectedValue = agentRegExisting.AgentRegPersonal.state;
                                        ddlCountry.SelectedValue = agentRegExisting.AgentRegPersonal.country;
                                        txtTelNoOffice.Text = agentRegExisting.AgentRegPersonal.tel_no_office;
                                        txtTelNoHome.Text = agentRegExisting.AgentRegPersonal.tel_no_home;
                                        txtHandPhoneNo.Text = agentRegExisting.AgentRegPersonal.hand_phone;
                                        txtEmail.Text = agentRegExisting.AgentRegPersonal.email;
                                        txtContactPerson.Text = agentRegExisting.AgentRegPersonal.contact_person;
                                        txtContactNumber.Text = agentRegExisting.AgentRegPersonal.contact_no;
                                        txtIncomeTaxNo.Text = agentRegExisting.AgentRegPersonal.income_tax_no;
                                        txtEPFNo.Text = agentRegExisting.AgentRegPersonal.epf_no;
                                        txtSOCSO.Text = agentRegExisting.AgentRegPersonal.socso;
                                        ddlLanguage.SelectedValue = agentRegExisting.AgentRegPersonal.language;
                                        ddlNationality.SelectedValue = agentRegExisting.AgentRegPersonal.nationality;
                                        ddlMaritalStatus.SelectedValue = agentRegExisting.AgentRegPersonal.marital_status;
                                        chkBankruptcyYes.Checked = (agentRegExisting.AgentRegPersonal.bankruptcy_declaration != "");
                                        chkBankruptcyNo.Checked = (agentRegExisting.AgentRegPersonal.bankruptcy_declaration == "");
                                        txtBankruptcyDesc.Text = (chkBankruptcyYes.Checked ? agentRegExisting.AgentRegPersonal.bankruptcy_declaration : "");
                                        txtSpouseName.Text = agentRegExisting.AgentRegPersonal.spouse_name;
                                        txtSpouseIDNo.Text = agentRegExisting.AgentRegPersonal.spouse_id_no;

                                        string queryAgentRegPayment = (@" select * from agent_reg_payment_insurance where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                                        Response responseAgentRegPaymentList = GenericService.GetDataByQuery(queryAgentRegPayment, 0, 0, false, null, false, null, true);
                                        if (responseAgentRegPaymentList.IsSuccess)
                                        {
                                            var agentregPaymentsDyn = responseAgentRegPaymentList.Data;
                                            var responseAgentRegPaymentJSON = JsonConvert.SerializeObject(agentregPaymentsDyn);
                                            List<AgentRegPaymentInsurance> agentRegPaymentInsurances = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPaymentInsurance>>(responseAgentRegPaymentJSON);
                                            if (agentRegPaymentInsurances.Count == 1)
                                            {
                                                agentRegExisting.AgentRegPaymentInsurance = agentRegPaymentInsurances.FirstOrDefault();

                                                txtBeneficiaryName.Text = agentRegExisting.AgentRegPaymentInsurance.insurance_beneficiary_name;
                                                txtBeneficiaryNRIC.Text = agentRegExisting.AgentRegPaymentInsurance.insurance_beneficiary_NRIC;


                                                string queryAgentRegPaymentBanks = (@" select * from agent_reg_banks where agent_reg_payment_id=" + agentRegExisting.AgentRegPaymentInsurance.id + " and status=1 ");
                                                Response responseAgentRegPaymentBanksList = GenericService.GetDataByQuery(queryAgentRegPaymentBanks, 0, 0, false, null, false, null, true);
                                                if (responseAgentRegPaymentBanksList.IsSuccess)
                                                {
                                                    var agentregPaymentBanksDyn = responseAgentRegPaymentList.Data;
                                                    var responseAgentRegPaymentBanksJSON = JsonConvert.SerializeObject(agentregPaymentBanksDyn);
                                                    List<AgentRegBank> agentRegPaymentBanks = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegBank>>(responseAgentRegPaymentBanksJSON);
                                                    agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks = agentRegPaymentBanks;

                                                    AgentRegBank agentRegBank = agentRegPaymentBanks.FirstOrDefault(x => x.bank_type == 1);
                                                    if (agentRegBank != null)
                                                    {
                                                        ddlBankAccountType.SelectedValue = agentRegBank.bank_account_type;
                                                        ddlBankList.Items.FindByValue(agentRegBank.bank_name).Selected = true;
                                                        //ddlBankList.SelectedValue = agentRegBank.bank_name;
                                                        //ddlBankList.SelectedIndex = ddlBankList.Items.IndexOf(ddlBankList.Items.FindByValue(agentRegBank.bank_name));
                                                        //ddlBankList.SelectedItem.Text = ddlBankList.Items.FindByValue(agentRegBank.bank_name).ToString();
                                                        //ddlBankList.SelectedItem.Text = ddlBankList.Items.FindByValue(agentRegBank.bank_name).Selected.ToString();
                                                        txtBankAccNo.Text = agentRegBank.account_no;
                                                        ddlPaymentMode.SelectedValue = agentRegBank.payment_mode;
                                                    }

                                                    AgentRegBank agentRegBankI = agentRegPaymentBanks.FirstOrDefault(x => x.bank_type == 2);
                                                    if (agentRegBankI != null)
                                                    {
                                                        ddlCurrencyI.SelectedValue = agentRegBankI.currency;
                                                        txtPaymentICI.Text = agentRegBankI.id_no;
                                                        ddlBankAccountTypeI.SelectedValue = agentRegBankI.bank_account_type;
                                                        ddlBankNameI.Value = agentRegBankI.bank_name;
                                                        txtBankAccNoI.Text = agentRegBankI.account_no;
                                                        ddlPaymentModeI.SelectedValue = agentRegBankI.payment_mode;
                                                    }
                                                }
                                                else
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPaymentBanksList.Message + "\", '/Index.aspx');", true);


                                                string queryAgentExp = (@" select * from agent_reg_exps where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                                                Response responseAgentExpList = GenericService.GetDataByQuery(queryAgentExp, 0, 0, false, null, false, null, true);
                                                if (responseAgentExpList.IsSuccess)
                                                {
                                                    var agentExpDyn = responseAgentExpList.Data;
                                                    var responseAgentExpsJSON = JsonConvert.SerializeObject(agentExpDyn);
                                                    List<AgentRegExp> agentRegPExps = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegExp>>(responseAgentExpsJSON);
                                                    if (agentRegPExps.Count == 1)
                                                    {
                                                        agentRegExisting.AgentRegExp = agentRegPExps.FirstOrDefault();

                                                        txtPreviousEmployerName.Text = agentRegExisting.AgentRegExp.employer_name;
                                                        txtPositionHeld.Text = agentRegExisting.AgentRegExp.position_held;
                                                        txtLengthOfService.Text = agentRegExisting.AgentRegExp.length_of_service_yrs.ToString();
                                                        ddlAnnualIncome.SelectedValue = agentRegExisting.AgentRegExp.annual_income;
                                                        txtReasonEmp.Text = agentRegExisting.AgentRegExp.reason_for_leaving;
                                                    }

                                                    string query = " select * from agent_reg_files where agent_reg_id = '" + agentRegExisting.id + "' and status = 1 ";
                                                    Response responseARFist = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                                                    if (responseARFist.IsSuccess)
                                                    {
                                                        var agentFilesDyn = responseARFist.Data;
                                                        var responseAgentFilesJSON = JsonConvert.SerializeObject(agentFilesDyn);
                                                        List<Utility.AgentRegFile> agentRegFiles = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegFile>>(responseAgentFilesJSON);

                                                        AgentRegFile agentRegFileNRICFront = agentRegFiles.FirstOrDefault(x => x.file_type == 1);
                                                        AgentRegFile agentRegFileNRICBack = agentRegFiles.FirstOrDefault(x => x.file_type == 2);
                                                        AgentRegFile agentRegFileSelfie = agentRegFiles.FirstOrDefault(x => x.file_type == 3);
                                                        AgentRegFile agentRegFilePOP = agentRegFiles.FirstOrDefault(x => x.file_type == 4);
                                                        AgentRegFile agentRegFileQualification = agentRegFiles.FirstOrDefault(x => x.file_type == 5);
                                                        AgentRegFile agentRegFileFiMMCard = agentRegFiles.FirstOrDefault(x => x.file_type == 6);
                                                        AgentRegFile agentRegFileResult = agentRegFiles.FirstOrDefault(x => x.file_type == 7);
                                                        if (agentRegFileNRICFront != null)
                                                        {
                                                            uploadNRICFrontPageImage.InnerHtml = "<img src='" + agentRegFileNRICFront.url + "' alt='Selfie with NRIC' title='" + agentRegFileNRICFront.name + "' style='height: 100%'/>";
                                                            uploadNRICFrontPageImageLink.InnerHtml = "<a href='" + agentRegFileNRICFront.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileNRICFront.name +"</a>";
                                                        }
                                                        if (agentRegFileNRICBack != null)
                                                        {
                                                            uploadNRICBackPageImage.InnerHtml = "<img src='" + agentRegFileNRICBack.url + "' alt='Selfie with NRIC' title='" + agentRegFileNRICBack.name + "' style='height: 100%'/>";
                                                            uploadNRICBackPageImageLink.InnerHtml = "<a href='" + agentRegFileNRICBack.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileNRICBack.name + "</a>";
                                                        }
                                                        if (agentRegFileSelfie != null)
                                                        {
                                                            uploadSelfieImage.InnerHtml = "<img src='" + agentRegFileSelfie.url + "' alt='Selfie with NRIC' title='" + agentRegFileSelfie.name + "' style='height: 100%'/>";
                                                            uploadSelfieImageLink.InnerHtml = "<a href='" + agentRegFileSelfie.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileSelfie.name + "</a>";
                                                        }
                                                        if (agentRegFilePOP != null)
                                                        {
                                                            uploadPOPImage.InnerHtml = "<img src='" + agentRegFilePOP.url + "' alt='Selfie with NRIC' title='" + agentRegFilePOP.name + "' style='height: 100%'/>";
                                                            uploadPOPImageLink.InnerHtml = "<a href='" + agentRegFilePOP.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFilePOP.name + "</a>";
                                                        }
                                                        if (agentRegFileQualification != null)
                                                        {
                                                            uploadQualificationImage.InnerHtml = "<img src='" + agentRegFileQualification.url + "' alt='Selfie with NRIC' title='" + agentRegFileQualification.name + "' style='height: 100%'/>";
                                                            uploadQualificationImageLink.InnerHtml = "<a href='" + agentRegFileQualification.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileQualification.name + "</a>";
                                                        }
                                                        if (agentRegFileFiMMCard != null)
                                                        {
                                                            uploadFiMMCardImage.InnerHtml = "<img src='" + agentRegFileFiMMCard.url + "' alt='Selfie with NRIC' title='" + agentRegFileFiMMCard.name + "' style='height: 100%'/>";
                                                            uploadFiMMCardImageLink.InnerHtml = "<a href='" + agentRegFileFiMMCard.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileFiMMCard.name + "</a>";
                                                        }
                                                        if (agentRegFileResult != null)
                                                        {
                                                            uploadResultImage.InnerHtml = "<img src='" + agentRegFileResult.url + "' alt='Selfie with NRIC' title='" + agentRegFileResult.name + "' style='height: 100%'/>";
                                                            uploadResultImageLink.InnerHtml = "<a href='" + agentRegFileResult.url + "' alt='Selfie with NRIC' style='height: 100%' target='_blank'/>" + agentRegFileResult.name + "</a>";
                                                        }

                                                    }
                                                    else
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseARFist.Message + "\", '/Index.aspx');", true);
                                                }
                                                else
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentExpList.Message + "\", '/Index.aspx');", true);
                                            }
                                        }
                                        else
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPaymentList.Message + "\", '/Index.aspx');", true);
                                    }
                                }
                                else
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPersonalList.Message + "\", '/Index.aspx');", true);
                            }
                            else {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Request has been submitted before.\", '/AccountOpeningType.aspx');", true);
                            }
                        }
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegList.Message + "\", '/Index.aspx');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Session expired.\", '/Index.aspx');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object AgentVerify(string agentCode) {
            Response response = new Response();

            string invalid = "Invalid Introducer Code";
            string noInput = "Please Enter Introducer Code";
            string agentRankInvalid = "Invalid Introducer Code: Introducer rank should be higher than wealth Associate";
            string agentName;
            try {
                //check agent code
                //if input is not empty or null => get agent name.
                if (!String.IsNullOrEmpty(agentCode))
                {
                    string queryAgentCode = @"SELECT ar.id, ar.user_id, ar.agent_code, utd.name as agent_rank, arp.name from agent_regs ar 
                                                left join agent_reg_personal arp on ar.id = arp.agent_reg_id 
                                                left join user_types ut on ar.user_id = ut.user_id
                                                left join user_types_def utd on ut.user_type_id = utd.id
                                                where ar.agent_code = '"+ agentCode+"' and utd.name NOT in ('Trader')";
                    Response responseAgentCode = GenericService.GetDataByQuery(queryAgentCode, 0, 0, false, null, false, null, false);
                    if (responseAgentCode.IsSuccess)
                    {
                        var agentCodeDyn = responseAgentCode.Data;
                        var responseJSON = JsonConvert.SerializeObject(agentCodeDyn);
                        List<dynamic> agentCodeDynamicList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);

                        if (agentCodeDynamicList.Count != 0 && agentCodeDynamicList.FirstOrDefault().AgentRank != "WA")
                        {
                            dynamic agentCodeDynamic = agentCodeDynamicList.First();
                            agentName = agentCodeDynamic.Name;
                            String[] AgentNameArray = agentName.Split(' ');
                            String maskFirstName = "";
                            for (int i = 0; i < AgentNameArray[0].Length; i++)
                            {

                                maskFirstName += "x";
                            }
                            String MaskedAgentName = maskFirstName + agentName.Substring(AgentNameArray[0].Length);
                            return MaskedAgentName;
                        }
                        else if (agentCodeDynamicList.Count != 0 && agentCodeDynamicList.FirstOrDefault().AgentRank == "WA")
                        {
                            return agentRankInvalid;
                        }
                        else {
                            return invalid;
                        }                        
                    }
                    else {
                        response.Message = "Invalid Introducer Code";
                        response.IsSuccess = false;
                        return response;
                    }
                }
            }
            catch (Exception ex) {
                Logger.WriteLog("Open Agent Account Page verify agent: " + ex.Message);
            }
            return noInput;
        }


        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object btnNext_Click(List<NameValuePairs> formInputs)
        {
            Response response = new Response() { IsSuccess = true };
            List<CustomValidation> customValidations = new List<CustomValidation>();
            var defaultDateTime = default(DateTime);
            var defaultDateTimeStr = defaultDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var chkIsCUTEPassedYes = "";
                var chkIsCUTEPassedNo = "";
                User user = (User)(HttpContext.Current.Session["user"]);

                ////check agent code
                //var agentCode = formInputs.FirstOrDefault(x => x.name == "txtIntroCode").value;
                ////if input is not empty or null => get agent name.
                //if (!String.IsNullOrEmpty(agentCode)) {
                //    string queryAgentCode = @"SELECT ar.id, ar.agent_code , arp.name from agent_regs ar left join agent_reg_personal arp on ar.id = arp.agent_reg_id where ar.agent_code = '" + agentCode + "'";
                //    Response responseAgentCode = GenericService.GetDataByQuery(queryAgentCode, 0, 0, false, null, false, null, false);
                //    if (responseAgentCode.IsSuccess)
                //    {
                //        var agentCodeDyn = responseAgentCode.Data;
                //        var responseJSON = JsonConvert.SerializeObject(agentCodeDyn);
                //        List<dynamic> agentCodeDynamicList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);

                //        if (agentCodeDynamicList.Count != 0)
                //        {
                //            dynamic agentCodeDynamic = agentCodeDynamicList.First();
                //            string agentName = agentCodeDynamic.Name;
                //        }
                //    }
                //}


                var hdnOpenAgentId = formInputs.FirstOrDefault(x => x.name == "hdnOpenAgentId").value;
                AgentReg agentRegExisting = new AgentReg();
                string queryAgentReg = (@" select * from agent_regs where user_id=" + user.Id + " and process_status=0 and status=1 ");
                Response responseAgentRegList = GenericService.GetDataByQuery(queryAgentReg, 0, 0, false, null, false, null, true);
                if (responseAgentRegList.IsSuccess)
                {
                    var agentsDyn = responseAgentRegList.Data;
                    var responseARegJSON = JsonConvert.SerializeObject(agentsDyn);
                    List<AgentReg> agentRegs = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseARegJSON);
                    if (agentRegs.Count == 1)
                    {
                        agentRegExisting = agentRegs.FirstOrDefault();
                        HttpContext.Current.Session["AgentRegExisting"] = agentRegExisting;
                    }
                }
                else
                    return responseAgentRegList;                
                var panelId = formInputs.FirstOrDefault(x => x.name == "panelId").value;
                if (panelId == "agency")
                {
                    chkIsCUTEPassedYes = formInputs.FirstOrDefault(x => x.name == "chkIsCUTEPassedYes").value;
                    chkIsCUTEPassedNo = formInputs.FirstOrDefault(x => x.name == "chkIsCUTEPassedNo").value;
                    var chkRecommendedAgentYes = formInputs.FirstOrDefault(x => x.name == "chkRecommendedAgentYes").value;
                    var chkRecommendedAgentNo = formInputs.FirstOrDefault(x => x.name == "chkRecommendedAgentNo").value;
                    var txtIntroCode = formInputs.FirstOrDefault(x => x.name == "txtIntroCode").value;
                    var chkRegTypeUTS = formInputs.FirstOrDefault(x => x.name == "chkRegTypeUTS") != null ? formInputs.FirstOrDefault(x => x.name == "chkRegTypeUTS").value : "";
                    var chkRegTypePRS = formInputs.FirstOrDefault(x => x.name == "chkRegTypePRS") != null ? formInputs.FirstOrDefault(x => x.name == "chkRegTypePRS").value : "";
                    var ddlJobType = formInputs.FirstOrDefault(x => x.name == "ddlJobType").value;
                    var ddlOffice = formInputs.FirstOrDefault(x => x.name == "ddlOffice").value;
                    var ddlRegion = formInputs.FirstOrDefault(x => x.name == "ddlRegion").value;
                    
                    if ((chkIsCUTEPassedYes == null && chkIsCUTEPassedNo == null) || (chkRecommendedAgentYes == null && chkRecommendedAgentNo == null) || (chkRegTypeUTS == null && chkRegTypePRS == null)
                        || ddlJobType == "" || ddlOffice == "" || ddlRegion == "" || (chkRecommendedAgentYes == "on" && txtIntroCode == ""))
                    {
                        if (chkIsCUTEPassedYes == null && chkIsCUTEPassedNo == null)
                            customValidations.Add(new CustomValidation { Name = "chkIsCUTEPassedYes", Message = "This is required" });
                        if (chkRecommendedAgentYes == null && chkRecommendedAgentNo == null)
                            customValidations.Add(new CustomValidation { Name = "chkRecommendedAgentYes", Message = "Introducer Option is required" });
                        if (chkRegTypeUTS == null && chkRegTypePRS == null)
                            customValidations.Add(new CustomValidation { Name = "chkRegTypeUTS", Message = "Registration Type is required" });
                        if (ddlJobType == "")
                            customValidations.Add(new CustomValidation { Name = "ddlJobType", Message = "Agent Code is required" });
                        if (ddlOffice == "")
                            customValidations.Add(new CustomValidation { Name = "ddlOffice", Message = "Office is required" });
                        if (ddlRegion == "")
                            customValidations.Add(new CustomValidation { Name = "ddlRegion", Message = "Region is required" });
                        if (chkRecommendedAgentYes == "on" && txtIntroCode == "")
                            customValidations.Add(new CustomValidation { Name = "txtIntroCode", Message = "Introducer Code is required" });
                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    else if (chkRecommendedAgentYes == "on" && (formInputs.FirstOrDefault(x => x.name == "introCodeIsValid").value == "0" || formInputs.FirstOrDefault(x => x.name == "introCodeIsValid").value == ""))
                    {
                        customValidations.Add(new CustomValidation { Name = "txtIntroCode", Message = "Invalid Agent Code: Introducer rank should be higher than Wealth Associate" });
                        response.IsSuccess = false;
                        response.Message = "Please fill in valid details";
                    }
                    else
                    {

                        AgentReg agentReg = new AgentReg
                        {
                            id = agentRegExisting.id,
                            user_id = user.Id,
                            is_new_agent = chkIsCUTEPassedYes != null && chkIsCUTEPassedYes != "" && chkIsCUTEPassedYes == "on" ? 0 : chkIsCUTEPassedNo != null && chkIsCUTEPassedNo != "" && chkIsCUTEPassedNo == "on" ? 1 : 0,
                            reg_type_uts = chkRegTypeUTS != null && chkRegTypeUTS != "" && chkRegTypeUTS == "on" ? 1 : 0,
                            reg_type_prs = chkRegTypePRS != null && chkRegTypePRS != "" && chkRegTypePRS == "on" ? 1 : 0,
                            introducer_code = txtIntroCode,
                            job_type_id = Convert.ToInt32(ddlJobType),
                            office_id = Convert.ToInt32(ddlOffice),
                            process_status = 0,
                            recommendation_date = defaultDateTime,
                            is_active = 1,
                            created_date = DateTime.Now,
                            created_by = user.Id,
                            updated_date = DateTime.Now,
                            updated_by = user.Id,
                            status = 1
                        };
                        Response resPost = new Utility.CustomClasses.Response();
                        if (agentReg.id == 0)
                        {
                            resPost = GenericService.PostData<AgentReg>(agentReg);
                        }
                        else
                        {
                            resPost = GenericService.UpdateData<AgentReg>(agentReg);
                        }
                        if (resPost.IsSuccess)
                        {
                            agentRegExisting = (AgentReg)resPost.Data;

                            //string queryAgentRegPersonal = (@" select * from agent_reg_personal where agent_reg_id=" + agentReg.id + " and status=1 ");
                            //Response responseAgentRegPersonalList = GenericService.GetDataByQuery(queryAgentRegPersonal, 0, 0, false, null, false, null, true);
                            //if (responseAgentRegPersonalList.IsSuccess)
                            //{
                            //    var agentregPersonalsDyn = responseAgentRegPersonalList.Data;
                            //    var responseAgentRegPersonalJSON = JsonConvert.SerializeObject(agentregPersonalsDyn);
                            //    List<AgentRegPersonal> agentRegPersonals = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPersonal>>(responseAgentRegPersonalJSON);
                            //    if (agentRegPersonals.Count == 1)
                            //    {
                            //        agentRegExisting.AgentRegPersonal = agentRegPersonals.FirstOrDefault();
                            //    }
                            //}
                            //else
                            //    return responseAgentRegPersonalList;
                        }
                        else
                            return resPost;
                    }
                }
                if (panelId == "personal")
                {
                    string queryAgentRegPersonal = (@" select * from agent_reg_personal where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                    Response responseAgentRegPersonalList = GenericService.GetDataByQuery(queryAgentRegPersonal, 0, 0, false, null, false, null, true);
                    if (responseAgentRegPersonalList.IsSuccess)
                    {
                        var agentregPersonalsDyn = responseAgentRegPersonalList.Data;
                        var responseAgentRegPersonalJSON = JsonConvert.SerializeObject(agentregPersonalsDyn);
                        List<AgentRegPersonal> agentRegPersonals = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPersonal>>(responseAgentRegPersonalJSON);
                        if (agentRegPersonals.Count == 1)
                        {
                            agentRegExisting.AgentRegPersonal = agentRegPersonals.FirstOrDefault();
                        }
                    }
                    else
                        return responseAgentRegPersonalList;
                    var txtAgentName = formInputs.FirstOrDefault(x => x.name == "txtAgentName").value;
                    var txtIDNo = formInputs.FirstOrDefault(x => x.name == "txtIDNo").value;
                    var txtDOB = formInputs.FirstOrDefault(x => x.name == "txtDOB").value;
                    var txtMotherMaidenName = formInputs.FirstOrDefault(x => x.name == "txtMotherMaidenName").value;
                    var ddlSex = formInputs.FirstOrDefault(x => x.name == "ddlSex").value;
                    var ddlRace = formInputs.FirstOrDefault(x => x.name == "ddlRace").value;
                    var txtRaceDesc = formInputs.FirstOrDefault(x => x.name == "txtRaceDesc").value;
                    var ddlReligion = formInputs.FirstOrDefault(x => x.name == "ddlReligion").value;
                    var txtReligionDesc = formInputs.FirstOrDefault(x => x.name == "txtReligionDesc").value;
                    var ddlEducation = formInputs.FirstOrDefault(x => x.name == "ddlEducation").value;
                    var txtEducationDesc = formInputs.FirstOrDefault(x => x.name == "txtEducationDesc").value;
                    var txtYearsOfExperience = formInputs.FirstOrDefault(x => x.name == "txtYearsOfExperience").value;
                    var txtAddrLine1 = formInputs.FirstOrDefault(x => x.name == "txtAddrLine1").value;
                    var txtAddrLine2 = formInputs.FirstOrDefault(x => x.name == "txtAddrLine2").value;
                    var txtAddrLine3 = formInputs.FirstOrDefault(x => x.name == "txtAddrLine3").value;
                    var txtAddrLine4 = formInputs.FirstOrDefault(x => x.name == "txtAddrLine4").value;
                    var txtPostCode = formInputs.FirstOrDefault(x => x.name == "txtPostCode").value;
                    var ddlCountry = formInputs.FirstOrDefault(x => x.name == "ddlCountry").value;
                    var ddlState = formInputs.FirstOrDefault(x => x.name == "ddlState").value;
                    var txtTelNoOffice = formInputs.FirstOrDefault(x => x.name == "txtTelNoOffice").value;
                    var txtTelNoHome = formInputs.FirstOrDefault(x => x.name == "txtTelNoHome").value;
                    var txtHandPhoneNo = formInputs.FirstOrDefault(x => x.name == "txtHandPhoneNo").value;
                    var txtEmail = formInputs.FirstOrDefault(x => x.name == "txtEmail").value;
                    var txtContactPerson = formInputs.FirstOrDefault(x => x.name == "txtContactPerson").value;
                    var txtContactNumber = formInputs.FirstOrDefault(x => x.name == "txtContactNumber").value;
                    var txtIncomeTaxNo = formInputs.FirstOrDefault(x => x.name == "txtIncomeTaxNo").value;
                    var txtEPFNo = formInputs.FirstOrDefault(x => x.name == "txtEPFNo").value;
                    var txtSOCSO = formInputs.FirstOrDefault(x => x.name == "txtSOCSO").value;
                    var ddlLanguage = formInputs.FirstOrDefault(x => x.name == "ddlLanguage").value;
                    var ddlNationality = formInputs.FirstOrDefault(x => x.name == "ddlNationality").value;
                    var ddlMaritalStatus = formInputs.FirstOrDefault(x => x.name == "ddlMaritalStatus").value;
                    var txtMaritalDesc = formInputs.FirstOrDefault(x => x.name == "txtMaritalDesc").value;
                    var chkBankruptcyYes = formInputs.FirstOrDefault(x => x.name == "chkBankruptcyYes") != null ? formInputs.FirstOrDefault(x => x.name == "chkBankruptcyYes").value : null;
                    var chkBankruptcyNo = formInputs.FirstOrDefault(x => x.name == "chkBankruptcyNo") != null ? formInputs.FirstOrDefault(x => x.name == "chkBankruptcyNo").value : null;
                    var txtBankruptcyDesc = formInputs.FirstOrDefault(x => x.name == "txtBankruptcyDesc").value;
                    var txtSpouseName = formInputs.FirstOrDefault(x => x.name == "txtSpouseName").value;
                    var txtSpouseIDNo = formInputs.FirstOrDefault(x => x.name == "txtSpouseIDNo").value;

                    if (txtDOB == "" || txtMotherMaidenName == "" || ddlSex == "" || ddlRace == "" || txtContactPerson == "" || txtContactNumber == "" || txtIncomeTaxNo == "" || txtSOCSO == "" || ddlReligion == "" || ddlEducation == "" || txtYearsOfExperience == ""
                        || txtAgentName == "" || txtIDNo == "" || txtAddrLine1 == "" || txtAddrLine2 == "" || txtAddrLine3 == "" || txtAddrLine4 == "" || txtPostCode == "" || ddlState == "" || ddlCountry == "" || txtHandPhoneNo == "" || txtTelNoHome == "" || txtEmail == "" || ddlCountry == "" || ddlLanguage == "" || ddlMaritalStatus == "" || txtTelNoOffice == "" || txtEPFNo == "" || (chkBankruptcyYes == "" && chkBankruptcyNo == "")
                        || (chkBankruptcyYes == "on" && txtBankruptcyDesc == "") || (ddlRace == "4" && txtRaceDesc == "") || (ddlReligion == "4" && txtReligionDesc == "") || (ddlMaritalStatus == "4" && txtMaritalDesc == "") || (ddlEducation == "6" && txtEducationDesc == "")) //|| txtSpouseNRICOld == ""
                    {
                        if (txtAgentName == "")
                            customValidations.Add(new CustomValidation { Name = "txtAgentName", Message = "Agent Name is required" });
                        if (txtIDNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtIDNo", Message = "NRIC/Passport No is required" });
                        if (txtDOB == "")
                            customValidations.Add(new CustomValidation { Name = "txtDOB", Message = "Date of Birth is required" });
                        if (txtMotherMaidenName == "")
                            customValidations.Add(new CustomValidation { Name = "txtMotherMaidenName", Message = "Mother's Maiden Name is required" });
                        if (ddlSex == "")
                            customValidations.Add(new CustomValidation { Name = "ddlSex", Message = "Applicant's Sex is required" });
                        if (ddlRace == "")
                            customValidations.Add(new CustomValidation { Name = "ddlRace", Message = "Race is required" });
                        if (txtAddrLine1 == "")
                            customValidations.Add(new CustomValidation { Name = "txtAddrLine1", Message = "Address Line 1 is required" });
                        if (txtAddrLine2 == "")
                            customValidations.Add(new CustomValidation { Name = "txtAddrLine2", Message = "Address Line 2 is required" });
                        if (txtAddrLine3 == "")
                            customValidations.Add(new CustomValidation { Name = "txtAddrLine3", Message = "Address Line 3 is required" });
                        if (txtAddrLine4 == "")
                            customValidations.Add(new CustomValidation { Name = "txtAddrLine4", Message = "Address Line 4 is required" });
                        if (txtPostCode == "")
                            customValidations.Add(new CustomValidation { Name = "txtPostCode", Message = "Post Code is required" });
                        if (ddlState == "")
                            customValidations.Add(new CustomValidation { Name = "ddlState", Message = "State is required" });
                        if (ddlCountry == "")
                            customValidations.Add(new CustomValidation { Name = "ddlCountry", Message = "Country is required" });
                        if (txtHandPhoneNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtHandPhoneNo", Message = "Handphone No is required" });
                        if (txtEmail == "")
                            customValidations.Add(new CustomValidation { Name = "txtEmail", Message = "Email is required" });
                        if (txtTelNoHome == "")
                            customValidations.Add(new CustomValidation { Name = "txtTelNoHome", Message = "Home Tel No is required" });
                        if (txtContactPerson == "")
                            customValidations.Add(new CustomValidation { Name = "txtContactPerson", Message = "Emergency Contact Person is required" });
                        if (txtContactNumber == "")
                            customValidations.Add(new CustomValidation { Name = "txtContactNumber", Message = "Emergency Contact Number is required" });
                        if (txtIncomeTaxNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtIncomeTaxNo", Message = "Income Tax No is required" });
                        if (txtSOCSO == "")
                            customValidations.Add(new CustomValidation { Name = "txtSOCSO", Message = "SOSCO is required" });
                        if (ddlLanguage == "")
                            customValidations.Add(new CustomValidation { Name = "ddlLanguage", Message = "Language is required" });
                        if (ddlMaritalStatus == "")
                            customValidations.Add(new CustomValidation { Name = "ddlMaritalStatus", Message = "Marital Status is required" });
                        if (ddlMaritalStatus == "Married")
                        {
                            if (txtSpouseName == "")
                                customValidations.Add(new CustomValidation { Name = "txtSpouseName", Message = "Spouse Name is required" });
                            if (txtSpouseIDNo == "")
                                customValidations.Add(new CustomValidation { Name = "txtSpouseIDNo", Message = "New Spouse NRIC is required" });
                        }
                        if (txtTelNoOffice == "")
                            customValidations.Add(new CustomValidation { Name = "txtTelNoOffice", Message = "Office Telephone number is required" });
                        if (txtEPFNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtEPFNo", Message = "EPF No is required" });
                        if (chkBankruptcyYes == null && chkBankruptcyNo == null)
                            customValidations.Add(new CustomValidation { Name = "chkBankruptcyYes", Message = "Bankruptcy Declaration status is required" });
                        if (ddlReligion == "")
                            customValidations.Add(new CustomValidation { Name = "ddlReligion", Message = "Religion is required" });
                        if (ddlEducation == "")
                            customValidations.Add(new CustomValidation { Name = "ddlEducation", Message = "Highest Education is required" });
                        if (txtYearsOfExperience == "")
                            customValidations.Add(new CustomValidation { Name = "txtYearsOfExperience", Message = "Years of Experience is required" });

                        //"Others" validation
                        if (chkBankruptcyYes == "on" && txtBankruptcyDesc == "")
                            customValidations.Add(new CustomValidation { Name = "txtBankruptcyDesc", Message = "Bankruptcy Declaration text is required" });
                        if (ddlRace == "O" && txtRaceDesc == "")
                            customValidations.Add(new CustomValidation { Name = "txtRaceDesc", Message = "Race is required" });
                        if (ddlReligion == "4" && txtReligionDesc == "")
                            customValidations.Add(new CustomValidation { Name = "txtReligionDesc", Message = "Religion is required" });
                        if (ddlMaritalStatus == "Others" && txtMaritalDesc == "")
                            customValidations.Add(new CustomValidation { Name = "txtMaritalDesc", Message = "Marital Status is required" });
                        if (ddlEducation == "6" && txtEducationDesc == "")
                            customValidations.Add(new CustomValidation { Name = "txtEducationDesc", Message = "Highest Education is required" });
                        //if (txtSpouseNRICOld == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtSpouseNRICOld", Message = "Old NRIC Spouse is required" });

                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    else
                    {
                        AgentRegPersonal agentRegPersonal = new AgentRegPersonal
                        {
                            agent_reg_id = agentRegExisting.id,
                            name = txtAgentName,
                            id_no = txtIDNo,
                            dob = DateTime.ParseExact(txtDOB, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                            mother_maiden_name = txtMotherMaidenName,
                            sex = ddlSex,
                            race = (ddlRace == "O" ? "" : ddlRace.ToString()),
                            religion = (ddlReligion == "O" ? "" : ddlReligion.ToString()),
                            religion_others = (ddlReligion == "4" ? txtReligionDesc : ""),
                            highest_education = (ddlEducation == "6" ? txtEducationDesc : ddlEducation),
                            years_of_experience = (txtYearsOfExperience != "" ? Convert.ToInt32(txtYearsOfExperience) : 0),
                            bankruptcy_declaration = (chkBankruptcyYes == "on" ? txtBankruptcyDesc : ""), //might have to make changes here
                            mail_addr1 = txtAddrLine1,
                            mail_addr2 = txtAddrLine2,
                            mail_addr3 = txtAddrLine3,
                            mail_addr4 = txtAddrLine4,
                            post_code = txtPostCode,
                            state = ddlState,
                            country = ddlCountry,
                            tel_no_office = txtTelNoOffice,
                            tel_no_home = txtTelNoHome,
                            hand_phone = txtHandPhoneNo,
                            email = txtEmail,
                            contact_person = txtContactPerson,
                            contact_no = txtContactNumber,
                            income_tax_no = txtIncomeTaxNo,
                            epf_no = txtEPFNo,
                            socso = txtSOCSO,
                            language = ddlLanguage,
                            nationality = ddlNationality,
                            marital_status = (ddlMaritalStatus == "Others" ? txtMaritalDesc : ddlMaritalStatus),
                            spouse_name = txtSpouseName,
                            spouse_id_no = txtSpouseIDNo,
                            is_active = 1,
                            created_date = DateTime.Now,
                            created_by = user.Id,
                            status = 1
                        };
                        Response resPostPersonal = new Response();
                        if (agentRegExisting.AgentRegPersonal == null)
                        {
                            resPostPersonal = GenericService.PostData<AgentRegPersonal>(agentRegPersonal);
                        }
                        else
                        {
                            agentRegPersonal.id = agentRegExisting.AgentRegPersonal.id;
                            resPostPersonal = GenericService.UpdateData<AgentRegPersonal>(agentRegPersonal);
                        }
                        if (resPostPersonal.IsSuccess)
                        {
                            agentRegPersonal = (AgentRegPersonal)resPostPersonal.Data;
                            agentRegExisting.AgentRegPersonal = agentRegPersonal;

                            string queryAgentRegPayment = (@" select * from agent_reg_payment_insurance where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                            Response responseAgentRegPaymentList = GenericService.GetDataByQuery(queryAgentRegPayment, 0, 0, false, null, false, null, true);
                            if (responseAgentRegPaymentList.IsSuccess)
                            {
                                var agentregPaymentsDyn = responseAgentRegPaymentList.Data;
                                var responseAgentRegPaymentJSON = JsonConvert.SerializeObject(agentregPaymentsDyn);
                                List<AgentRegPaymentInsurance> agentRegPaymentInsurances = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPaymentInsurance>>(responseAgentRegPaymentJSON);
                                if (agentRegPaymentInsurances.Count == 1)
                                {
                                    agentRegExisting.AgentRegPaymentInsurance = agentRegPaymentInsurances.FirstOrDefault();
                                }
                            }
                            else
                                return responseAgentRegPaymentList;
                        }
                        else
                            return resPostPersonal;
                    }
                }
                if (panelId == "payment")
                {
                    bool continueFlag = false;
                    string queryAgentRegPayment = (@" select * from agent_reg_payment_insurance where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                    Response responseAgentRegPaymentList = GenericService.GetDataByQuery(queryAgentRegPayment, 0, 0, false, null, false, null, true);
                    if (responseAgentRegPaymentList.IsSuccess)
                    {
                        var agentregPaymentsDyn = responseAgentRegPaymentList.Data;
                        var responseAgentRegPaymentJSON = JsonConvert.SerializeObject(agentregPaymentsDyn);
                        List<AgentRegPaymentInsurance> agentRegPaymentInsurances = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegPaymentInsurance>>(responseAgentRegPaymentJSON);
                        if (agentRegPaymentInsurances.Count == 1)
                        {
                            agentRegExisting.AgentRegPaymentInsurance = agentRegPaymentInsurances.FirstOrDefault();

                            string queryAgentRegPaymentBanks = (@" select * from agent_reg_banks where agent_reg_payment_id=" + agentRegExisting.AgentRegPaymentInsurance.id + " and status=1 ");
                            Response responseAgentRegPaymentBanksList = GenericService.GetDataByQuery(queryAgentRegPaymentBanks, 0, 0, false, null, false, null, true);
                            if (responseAgentRegPaymentBanksList.IsSuccess)
                            {
                                var agentregPaymentBanksDyn = responseAgentRegPaymentList.Data;
                                var responseAgentRegPaymentBanksJSON = JsonConvert.SerializeObject(agentregPaymentBanksDyn);
                                List<AgentRegBank> agentRegPaymentBanks = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegBank>>(responseAgentRegPaymentBanksJSON);
                                agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks = agentRegPaymentBanks;
                            }
                        }
                    }
                    else
                        return responseAgentRegPaymentList;

                    var ddlCurrency = formInputs.FirstOrDefault(x => x.name == "ddlCurrency").value;
                    var ddlBankAccountType = formInputs.FirstOrDefault(x => x.name == "ddlBankAccountType").value;
                    var ddlBankList = formInputs.FirstOrDefault(x => x.name == "ddlBankList").value;
                    var txtBankAccNo = formInputs.FirstOrDefault(x => x.name == "txtBankAccNo").value;
                    var ddlPaymentMode = formInputs.FirstOrDefault(x => x.name == "ddlPaymentMode").value;

                    var ddlCurrencyI = formInputs.FirstOrDefault(x => x.name == "ddlCurrencyI").value;
                    var txtPaymentICI = formInputs.FirstOrDefault(x => x.name == "txtPaymentICI").value;
                    var ddlBankAccountTypeI = formInputs.FirstOrDefault(x => x.name == "ddlBankAccountTypeI").value;
                    var ddlBankNameI = formInputs.FirstOrDefault(x => x.name == "ddlBankNameI").value;
                    var txtBankAccNoI = formInputs.FirstOrDefault(x => x.name == "txtBankAccNoI").value;
                    var ddlPaymentModeI = formInputs.FirstOrDefault(x => x.name == "ddlPaymentModeI").value;

                    var txtBeneficiaryName = formInputs.FirstOrDefault(x => x.name == "txtBeneficiaryName").value;
                    var txtBeneficiaryNRIC = formInputs.FirstOrDefault(x => x.name == "txtBeneficiaryNRIC").value;
                    //var txtOfficeSubsidyComm = formInputs.FirstOrDefault(x => x.name == "txtOfficeSubsidyComm").value;
                    //var txtOfficeSubsidyAmount = formInputs.FirstOrDefault(x => x.name == "txtOfficeSubsidyAmount").value;

                    //

                    //Nothing is filled, Show Null Message 1
                    if (((ddlCurrency == "" && ddlBankList == "" && ddlBankAccountType == "" && txtBankAccNo == "" && ddlCurrencyI == ""
                        && txtPaymentICI == "" && ddlBankNameI == "" && ddlBankAccountTypeI == "" && txtBankAccNoI == "")))
                    {
                        continueFlag = false;
                        response.Message = "Please fill in bank details";
                        response.IsSuccess = false;
                    }



                    //If any of A is blank when A is filled, highlight A scenario 5 6
                    else if ((ddlCurrency != "" || ddlBankList != "" || ddlBankAccountType != "" || txtBankAccNo != "")
                        && (ddlCurrency == "" || ddlBankList == "" || ddlBankAccountType == "" || txtBankAccNo == "")
                        && ((ddlCurrencyI == "" && txtPaymentICI == "" && ddlBankNameI == "" && ddlBankAccountTypeI == "" && txtBankAccNoI == "")
                        || (ddlCurrencyI != "" && txtPaymentICI != "" && ddlBankNameI != "" && ddlBankAccountTypeI != "" && txtBankAccNoI != ""))) //|| txtOfficeSubsidyComm == "" || txtOfficeSubsidyAmount == ""
                    {

                        if (ddlCurrency == "")
                            customValidations.Add(new CustomValidation { Name = "ddlCurrency", Message = "Currency is required" });
                        if (ddlBankList == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankList", Message = "Bank Name is required" });
                        if (ddlBankAccountType == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankAccountType", Message = "Bank Account Type is required" });
                        if (txtBankAccNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtBankAccNo", Message = "Bank Account Number is required" });
                        if (ddlPaymentMode == "")
                            customValidations.Add(new CustomValidation { Name = "ddlPaymentMode", Message = "Payment Mode is required" });
                        if (txtBeneficiaryName == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryName", Message = "Beneficiary Name is required" });
                            response.IsSuccess = false;
                        }
                        if (txtBeneficiaryNRIC == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryNRIC", Message = "Beneficiary NRIC is required" });
                            response.IsSuccess = false;
                        }
                        //if (ddlPaymentModeI == "")
                        //    customValidations.Add(new CustomValidation { Name = "ddlPaymentModeI", Message = "International Payment Mode is required" });
                        //if (txtOfficeSubsidyComm == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtOfficeSubsidyComm", Message = "Office Subsidy Commission is required" });
                        //if (txtOfficeSubsidyAmount == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtOfficeSubsidyAmount", Message = "Office Subsidy Amount is required" });

                        continueFlag = false;
                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    //If any of B is blank when B is filled, highlight B scenario 7 8
                    else if ((ddlCurrencyI != "" || txtPaymentICI != "" || ddlBankNameI != "" || ddlBankAccountTypeI != "" || txtBankAccNoI != "")
                        && (ddlCurrencyI == "" || txtPaymentICI == "" || ddlBankNameI == "" || ddlBankAccountTypeI == "" || txtBankAccNoI == "")
                        && ((ddlCurrency == "" && ddlBankList == "" && ddlBankAccountType == "" && txtBankAccNo == "")
                        || (ddlCurrency != "" && ddlBankList != "" && ddlBankAccountType != "" && txtBankAccNo != "")))
                    {
                        if (ddlCurrencyI == "")
                            customValidations.Add(new CustomValidation { Name = "ddlCurrencyI", Message = "International Currency is required" });
                        if (txtPaymentICI == "")
                            customValidations.Add(new CustomValidation { Name = "txtPaymentICI", Message = "International Payment IC is required" });
                        if (ddlBankNameI == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankNameI", Message = "International Bank Name is required" });
                        if (ddlBankAccountTypeI == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankAccountTypeI", Message = "International Bank Account Type is required" });
                        if (txtBankAccNoI == "")
                            customValidations.Add(new CustomValidation { Name = "txtBankAccNoI", Message = "International Bank Account Number is required" });
                        if (txtBeneficiaryName == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryName", Message = "Beneficiary Name is required" });
                            response.IsSuccess = false;
                        }
                        if (txtBeneficiaryNRIC == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryNRIC", Message = "Beneficiary NRIC is required" });
                            response.IsSuccess = false;
                        }
                        //if (ddlPaymentModeI == "")
                        //    customValidations.Add(new CustomValidation { Name = "ddlPaymentModeI", Message = "International Payment Mode is required" });

                        //if (txtOfficeSubsidyComm == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtOfficeSubsidyComm", Message = "Office Subsidy Commission is required" });
                        //if (txtOfficeSubsidyAmount == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtOfficeSubsidyAmount", Message = "Office Subsidy Amount is required" });

                        continueFlag = false;
                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    //Highlight all 9
                    else if ((ddlCurrency == "" || ddlBankList == "" || ddlBankAccountType == "" || txtBankAccNo == "")
                        && (ddlCurrencyI == "" || txtPaymentICI == "" || ddlBankNameI == "" || ddlBankAccountTypeI == "" || txtBankAccNoI == ""))
                    {
                        if (ddlCurrency == "")
                            customValidations.Add(new CustomValidation { Name = "ddlCurrency", Message = "Currency is required" });
                        if (ddlBankList == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankName", Message = "Bank Name is required" });
                        if (ddlBankAccountType == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankAccountType", Message = "Bank Account Type is required" });
                        if (txtBankAccNo == "")
                            customValidations.Add(new CustomValidation { Name = "txtBankAccNo", Message = "Bank Account Number is required" });
                        if (ddlPaymentMode == "")
                            customValidations.Add(new CustomValidation { Name = "ddlPaymentMode", Message = "Payment Mode is required" });
                        if (ddlCurrencyI == "")
                            customValidations.Add(new CustomValidation { Name = "ddlCurrencyI", Message = "International Currency is required" });
                        if (txtPaymentICI == "")
                            customValidations.Add(new CustomValidation { Name = "txtPaymentICI", Message = "International Payment IC is required" });
                        if (ddlBankNameI == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankNameI", Message = "International Bank Name is required" });
                        if (ddlBankAccountTypeI == "")
                            customValidations.Add(new CustomValidation { Name = "ddlBankAccountTypeI", Message = "International Bank Account Type is required" });
                        if (txtBankAccNoI == "")
                            customValidations.Add(new CustomValidation { Name = "txtBankAccNoI", Message = "International Bank Account Number is required" });
                        if (txtBeneficiaryName == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryName", Message = "Beneficiary Name is required" });
                            response.IsSuccess = false;
                        }
                        if (txtBeneficiaryNRIC == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryNRIC", Message = "Beneficiary NRIC is required" });
                            response.IsSuccess = false;
                        }
                        continueFlag = false;
                        response.Message = "Please fill in details";
                        response.IsSuccess = false;
                    }
                    //Pass scenario 4 3 2
                    else if (((ddlCurrency == "" && ddlBankList == "" && ddlBankAccountType == "" && txtBankAccNo == "")
                    && (ddlCurrencyI != "" && txtPaymentICI != "" && ddlBankNameI != "" && ddlBankAccountTypeI != "" && txtBankAccNoI != ""))
                    || ((ddlCurrencyI == "" && txtPaymentICI == "" && ddlBankNameI == "" && ddlBankAccountTypeI == "" && txtBankAccNoI == "")
                    && (ddlCurrency != "" && ddlBankList != "" && ddlBankAccountType != "" && txtBankAccNo != ""))
                    || (ddlCurrency != "" && ddlBankList != "" && ddlBankAccountType != "" && txtBankAccNo != ""
                    && ddlCurrencyI != "" && txtPaymentICI != "" && ddlBankNameI != "" && ddlBankAccountTypeI != "" && txtBankAccNoI != ""))
                    {
                        continueFlag = true;
                        if (txtBeneficiaryName == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryName", Message = "Beneficiary Name is required" });
                            response.Message = "Please fill in details";
                            response.IsSuccess = false;
                        }
                        if (txtBeneficiaryNRIC == "")
                        {
                            continueFlag = false;
                            customValidations.Add(new CustomValidation { Name = "txtBeneficiaryNRIC", Message = "Beneficiary NRIC is required" });
                            response.Message = "Please fill in details";
                            response.IsSuccess = false;
                        }
                    }
                    else
                    { }

                    if (continueFlag)
                    {
                        AgentRegPaymentInsurance agentRegPaymentInsurance = new AgentRegPaymentInsurance
                        {
                            agent_reg_id = agentRegExisting.id,
                            insurance_beneficiary_name = txtBeneficiaryName,
                            insurance_beneficiary_NRIC = txtBeneficiaryNRIC,
                            is_active = 1,
                            created_by = user.Id,
                            created_date = DateTime.Now,
                            status = 1
                        };
                        Response resPostPayment = new Response();
                        if (agentRegExisting.AgentRegPaymentInsurance == null)
                        {
                            resPostPayment = GenericService.PostData<AgentRegPaymentInsurance>(agentRegPaymentInsurance);
                        }
                        else
                        {
                            agentRegPaymentInsurance.id = agentRegExisting.AgentRegPaymentInsurance.id;
                            resPostPayment = GenericService.UpdateData<AgentRegPaymentInsurance>(agentRegPaymentInsurance);
                        }
                        if (resPostPayment.IsSuccess)
                        {
                            agentRegPaymentInsurance = (AgentRegPaymentInsurance)resPostPayment.Data;
                            if (agentRegExisting.AgentRegPaymentInsurance == null)
                                agentRegExisting.AgentRegPaymentInsurance = agentRegPaymentInsurance;
                            AgentRegBank agentRegBank = new AgentRegBank
                            {
                                agent_reg_payment_id = agentRegPaymentInsurance.id,
                                bank_type = 1,
                                currency = ddlCurrency,
                                bank_name = ddlBankList,
                                bank_account_type = ddlBankAccountType,
                                account_no = txtBankAccNo,
                                payment_mode = ddlPaymentMode,
                                is_active = 1,
                                created_date = DateTime.Now,
                                created_by = user.Id,
                                status = 1
                            };
                            Response resPostPaymentBank = new Response();
                            if (agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks == null || (agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks != null && agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks.FirstOrDefault(x => x.bank_type == 1) == null))
                            {
                                resPostPaymentBank = GenericService.PostData<AgentRegBank>(agentRegBank);
                            }
                            else if (agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks != null && agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks.FirstOrDefault(x => x.bank_type == 1) != null)
                            {
                                agentRegBank.id = agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks.FirstOrDefault(x => x.bank_type == 1).id;
                                resPostPaymentBank = GenericService.UpdateData<AgentRegBank>(agentRegBank);
                            }
                            List<AgentRegBank> AgentRegBanks = new List<AgentRegBank>();
                            if (resPostPaymentBank.IsSuccess)
                            {
                                agentRegBank = (AgentRegBank)resPostPaymentBank.Data;
                                AgentRegBanks.Add(agentRegBank);
                            }
                            else
                                return resPostPaymentBank;
                            AgentRegBank agentRegBankI = new AgentRegBank
                            {
                                agent_reg_payment_id = agentRegPaymentInsurance.id,
                                bank_type = 2,
                                currency = ddlCurrencyI,
                                id_no = txtPaymentICI,
                                bank_name = ddlBankNameI,
                                bank_account_type = ddlBankAccountTypeI,
                                account_no = txtBankAccNoI,
                                payment_mode = ddlPaymentModeI,
                                is_active = 1,
                                created_date = DateTime.Now,
                                created_by = user.Id,
                                status = 1
                            };
                            Response resPostPaymentBankI = new Response();
                            if (agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks == null || (agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks != null && agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks.FirstOrDefault(x => x.bank_type == 2) == null))
                            {
                                resPostPaymentBankI = GenericService.PostData<AgentRegBank>(agentRegBankI);
                            }
                            else if (agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks != null && agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks.FirstOrDefault(x => x.bank_type == 2) != null)
                            {
                                agentRegBankI.id = agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks.FirstOrDefault(x => x.bank_type == 2).id;
                                resPostPaymentBankI = GenericService.UpdateData<AgentRegBank>(agentRegBankI);
                            }
                            if (resPostPaymentBankI.IsSuccess)
                            {
                                agentRegBankI = (AgentRegBank)resPostPaymentBankI.Data;
                                AgentRegBanks.Add(agentRegBankI);
                            }
                            else
                                return resPostPaymentBankI;

                            agentRegExisting.AgentRegPaymentInsurance = agentRegPaymentInsurance;
                            agentRegExisting.AgentRegPaymentInsurance.AgentRegBanks = AgentRegBanks;
                        }
                        else
                            return resPostPayment;
                    }
                }
                if (panelId == "work")
                {
                    string queryAgentExp = (@" select * from agent_reg_exps where agent_reg_id=" + agentRegExisting.id + " and status=1 ");
                    Response responseAgentExpList = GenericService.GetDataByQuery(queryAgentExp, 0, 0, false, null, false, null, true);
                    if (responseAgentExpList.IsSuccess)
                    {
                        var agentExpDyn = responseAgentExpList.Data;
                        var responseAgentExpsJSON = JsonConvert.SerializeObject(agentExpDyn);
                        List<AgentRegExp> agentRegPExps = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegExp>>(responseAgentExpsJSON);
                        if (agentRegPExps.Count == 1)
                        {
                            agentRegExisting.AgentRegExp = agentRegPExps.FirstOrDefault();
                        }
                    }
                    else
                        return responseAgentExpList;
                    var txtPreviousEmployerName = formInputs.FirstOrDefault(x => x.name == "txtPreviousEmployerName").value;
                    var txtPositionHeld = formInputs.FirstOrDefault(x => x.name == "txtPositionHeld").value;
                    var txtLengthOfService = formInputs.FirstOrDefault(x => x.name == "txtLengthOfService").value;
                    var ddlAnnualIncome = formInputs.FirstOrDefault(x => x.name == "ddlAnnualIncome").value;
                    var txtReasonEmp = formInputs.FirstOrDefault(x => x.name == "txtReasonEmp").value;
                    //var txtPreviousEmployerName2 = formInputs.FirstOrDefault(x => x.name == "txtPreviousEmployerName2").value;
                    //var txtPositionHeld2 = formInputs.FirstOrDefault(x => x.name == "txtPositionHeld2").value;
                    //var txtLengthOfService2 = formInputs.FirstOrDefault(x => x.name == "txtLengthOfService2").value;
                    //var ddlAnnualIncome2 = formInputs.FirstOrDefault(x => x.name == "ddlAnnualIncome2").value;
                    //var txtReasonEmp2 = formInputs.FirstOrDefault(x => x.name == "txtReasonEmp2").value;
                    if (txtPreviousEmployerName == "" || txtPositionHeld == "" || txtLengthOfService == "" || ddlAnnualIncome == "" || txtReasonEmp == ""
                        //|| txtPreviousEmployerName2 == "" || txtPositionHeld2 == "" || txtLengthOfService2 == "" || ddlAnnualIncome2 == ""|| txtReasonEmp2 == ""
                        )
                    {
                        if (txtPreviousEmployerName == "")
                            customValidations.Add(new CustomValidation { Name = "txtPreviousEmployerName", Message = "Previous Employer Name is required" });
                        if (txtPositionHeld == "")
                            customValidations.Add(new CustomValidation { Name = "txtPositionHeld", Message = "Position Held is required" });
                        if (txtLengthOfService == "")
                            customValidations.Add(new CustomValidation { Name = "txtLengthOfService", Message = "Length of Service is required" });
                        if (ddlAnnualIncome == "")
                            customValidations.Add(new CustomValidation { Name = "ddlAnnualIncome", Message = "Annual Income is required" });
                        if (txtReasonEmp == "")
                            customValidations.Add(new CustomValidation { Name = "txtReasonEmp", Message = "Reason for leaving is required" });
                        //if (txtPreviousEmployerName2 == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtPreviousEmployerName2", Message = "Contact Person is required" });
                        //if (txtPositionHeld2 == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtPositionHeld2", Message = "Income Tax No is required" });
                        //if (txtLengthOfService2 == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtLengthOfService2", Message = "SOSCO is required" });
                        //if (ddlAnnualIncome2 == "")
                        //    customValidations.Add(new CustomValidation { Name = "ddlAnnualIncome2", Message = "Entry Date is required" });
                        //if (txtReasonEmp2 == "")
                        //    customValidations.Add(new CustomValidation { Name = "txtReasonEmp2", Message = "Language is required" });

                        response.IsSuccess = false;
                        response.Message = "Please fill in details";
                    }
                    else
                    {
                        AgentRegExp agentRegExp = new AgentRegExp
                        {
                            agent_reg_id = agentRegExisting.id,
                            employer_name = txtPreviousEmployerName,
                            position_held = txtPositionHeld,
                            length_of_service_yrs = Convert.ToInt32(txtLengthOfService),
                            annual_income = ddlAnnualIncome,
                            reason_for_leaving = txtReasonEmp,
                            is_active = 1,
                            created_by = user.Id,
                            created_date = DateTime.Now,
                            status = 1
                        };
                        Response resPostExp = new Response();
                        if (agentRegExisting.AgentRegExp == null)
                        {
                            resPostExp = GenericService.PostData<AgentRegExp>(agentRegExp);
                        }
                        else
                        {
                            agentRegExp.id = agentRegExisting.AgentRegExp.id;
                            resPostExp = GenericService.UpdateData<AgentRegExp>(agentRegExp);
                        }
                        if (resPostExp.IsSuccess)
                        {
                            agentRegExp = (AgentRegExp)resPostExp.Data;
                        }
                        else
                            return resPostExp;
                    }
                }
                if (panelId == "docs")
                {
                    List<string> ValidFileTypes = new List<string> { "data:image/png", "data:image/jpg", "data:image/jpeg", "data:application/pdf" };
                    List<string> ValidSelfieFileTypes = new List<string> { "data:image/png", "data:image/jpg", "data:image/jpeg" };
                    string path = "/Content/acc-opening-uploads/" + DateTime.Now.ToString("yyyyMMdd") + "/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(path)))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
                    int fileCount = Directory.GetFiles(HttpContext.Current.Server.MapPath(path)).Count();
                    fileCount++;

                    //string query = " select * from agent_reg_files where agent_reg_id = '" + agentRegExisting.id + "' and status = 1 ";
                    string query = "Select ar.is_new_agent , arf.* from agent_reg_files  arf join agent_regs ar ON ar.id = arf.agent_reg_id where arf.agent_reg_id = '" + agentRegExisting.id + "' and arf.status = 1 ";
                    Response responseARFist = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                    if (responseARFist.IsSuccess)
                    {
                        var agentFilesDyn = responseARFist.Data;
                        var responseAgentFilesJSON = JsonConvert.SerializeObject(agentFilesDyn);
                        //List<Utility.AgentRegFile> agentRegFiles = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegFile>>(responseAgentFilesJSON);
                        List<dynamic> agentRegFiles = JsonConvert.DeserializeObject<List<dynamic>>(responseAgentFilesJSON);
                        List<FileInput> fileInputs = new List<FileInput>();

                        chkIsCUTEPassedYes = (agentRegExisting.is_new_agent == 0 ? "on" : "off");
                        chkIsCUTEPassedNo = (agentRegExisting.is_new_agent == 1 ? "on" : "off");


                        if (formInputs.FirstOrDefault(x => x.name == "uploadNRICFrontPage") != null) //___________________________________________NRIC FRONT PAGE VALIDATION
                        {
                            var uploadNRICFrontPage = formInputs.FirstOrDefault(x => x.name == "uploadNRICFrontPage").value;
                            if (ValidFileTypes.FirstOrDefault(x => uploadNRICFrontPage.Contains(x)) != null)
                            {
                                var uploadNRICFrontPageFileName = formInputs.FirstOrDefault(x => x.name == "uploadNRICFrontPageFileName").value;

                                var last = uploadNRICFrontPageFileName.LastIndexOf('.');
                                var butLast = uploadNRICFrontPageFileName.Substring(0, last).Replace(".", "");
                                uploadNRICFrontPageFileName = butLast + uploadNRICFrontPageFileName.Substring(last);
                                string uploadNRICFrontPageFileNameNew = uploadNRICFrontPageFileName.Split('.')[0] + fileCount + "." + uploadNRICFrontPageFileName.Split('.')[1];
                                fileCount++;
                                fileInputs.Add(new FileInput
                                {
                                    Type = 1,
                                    Base64String = uploadNRICFrontPage,
                                    FileName = uploadNRICFrontPageFileName,
                                    FileNameNew = uploadNRICFrontPageFileNameNew
                                });
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach valid file type for NRIC Front Page";
                                return response;
                            }
                        }
                        else if (agentRegFiles.FirstOrDefault(x => x.file_type == 1) == null)
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach NRIC Front Page";
                            return response;
                        }
                        if (formInputs.FirstOrDefault(x => x.name == "uploadNRICBackPage") != null) //_________________________NRIC BACK PAGE VALIDATION
                        {
                            var uploadNRICBackPage = formInputs.FirstOrDefault(x => x.name == "uploadNRICBackPage").value;
                            if (ValidFileTypes.FirstOrDefault(x => uploadNRICBackPage.Contains(x)) != null)
                            {
                                var uploadNRICBackPageFileName = formInputs.FirstOrDefault(x => x.name == "uploadNRICBackPageFileName").value;
                                var last = uploadNRICBackPageFileName.LastIndexOf('.');
                                var butLast = uploadNRICBackPageFileName.Substring(0, last).Replace(".", "");
                                uploadNRICBackPageFileName = butLast + uploadNRICBackPageFileName.Substring(last);
                                string uploadNRICBackPageFileNameNew = uploadNRICBackPageFileName.Split('.')[0] + fileCount + "." + uploadNRICBackPageFileName.Split('.')[1];
                                fileCount++;
                                fileInputs.Add(new FileInput { Type = 2, Base64String = uploadNRICBackPage, FileName = uploadNRICBackPageFileName, FileNameNew = uploadNRICBackPageFileNameNew });
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach valid file type for NRIC Back Page";
                                return response;
                            }
                        }
                        else if (agentRegFiles.FirstOrDefault(x => x.file_type == 2) == null)
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach NRIC Back Page";
                            return response;
                        }
                        if (formInputs.FirstOrDefault(x => x.name == "uploadSelfie") != null) //_____________________________________________UPLOAD SELFIE VALIDATION
                        {
                            var uploadSelfie = formInputs.FirstOrDefault(x => x.name == "uploadSelfie").value;
                            if (uploadSelfie != "")
                            {
                                if (ValidFileTypes.FirstOrDefault(x => uploadSelfie.Contains(x)) != null)
                                {
                                    var uploadSelfieFileName = formInputs.FirstOrDefault(x => x.name == "uploadSelfieFileName").value;
                                    var last = uploadSelfieFileName.LastIndexOf('.');
                                    var butLast = uploadSelfieFileName.Substring(0, last).Replace(".", "");
                                    uploadSelfieFileName = butLast + uploadSelfieFileName.Substring(last);
                                    string uploadSelfieFileNameNew = uploadSelfieFileName.Split('.')[0] + fileCount + "." + uploadSelfieFileName.Split('.')[1];
                                    fileCount++;
                                    fileInputs.Add(new FileInput { Type = 3, Base64String = uploadSelfie, FileName = uploadSelfieFileName, FileNameNew = uploadSelfieFileNameNew });
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Please attach valid file type for Selfie";
                                    return response;
                                }
                            }
                            else if (agentRegFiles.FirstOrDefault(x => x.file_type == 3) == null)
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach Selfie";
                                return response;
                            }
                        }
                        else if (agentRegFiles.FirstOrDefault(x => x.file_type == 3) == null)
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach Selfie";
                            return response;
                        }
                        //Upload Proof of Payment
                        if (formInputs.FirstOrDefault(x => x.name == "uploadPOP") != null)
                        {
                            var uploadPOP = formInputs.FirstOrDefault(x => x.name == "uploadPOP").value;
                            if (ValidFileTypes.FirstOrDefault(x => uploadPOP.Contains(x)) != null)
                            {
                                var uploadPOPFileName = formInputs.FirstOrDefault(x => x.name == "uploadPOPFileName").value;

                                var last = uploadPOPFileName.LastIndexOf('.');
                                var butLast = uploadPOPFileName.Substring(0, last).Replace(".", "");
                                uploadPOPFileName = butLast + uploadPOPFileName.Substring(last);
                                string uploadPOPFileNameNew = uploadPOPFileName.Split('.')[0] + fileCount + "." + uploadPOPFileName.Split('.')[1];
                                fileCount++;
                                fileInputs.Add(new FileInput
                                {
                                    Type = 4,
                                    Base64String = uploadPOP,
                                    FileName = uploadPOPFileName,
                                    FileNameNew = uploadPOPFileNameNew
                                });
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach valid file type for Proof of Payment";
                                return response;
                            }
                        }
                        else if (agentRegFiles.FirstOrDefault(x => x.file_type == 4) == null)
                        {
                            response.IsSuccess = false;
                            response.Message = "Please attach Proof of Payment";
                            return response;
                        }

                        // Upload Qualification
                        if (chkIsCUTEPassedNo == "on" )
                        {
                            if (formInputs.FirstOrDefault(x => x.name == "uploadQualification") != null)
                            {
                                var uploadQ = formInputs.FirstOrDefault(x => x.name == "uploadQualification").value;
                                if (ValidFileTypes.FirstOrDefault(x => uploadQ.Contains(x)) != null)
                                {
                                    var uploadQFileName = formInputs.FirstOrDefault(x => x.name == "uploadQualificationFileName").value;

                                    var last = uploadQFileName.LastIndexOf('.');
                                    var butLast = uploadQFileName.Substring(0, last).Replace(".", "");
                                    uploadQFileName = butLast + uploadQFileName.Substring(last);
                                    string uploadQFileNameNew = uploadQFileName.Split('.')[0] + fileCount + "." + uploadQFileName.Split('.')[1];
                                    fileCount++;
                                    fileInputs.Add(new FileInput
                                    {
                                        Type = 5,
                                        Base64String = uploadQ,
                                        FileName = uploadQFileName,
                                        FileNameNew = uploadQFileNameNew
                                    });
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Please attach valid file type for Qualification";
                                    return response;
                                }
                            }
                            else if (agentRegFiles.FirstOrDefault(x => x.file_type == 5) == null)
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach Qualification";
                                return response;
                            }
                        }
                        // Upload FiMM
                        if (chkIsCUTEPassedYes == "on")
                        {
                            if (formInputs.FirstOrDefault(x => x.name == "uploadFiMMCard") != null)
                            {
                                var uploadFiMMCard = formInputs.FirstOrDefault(x => x.name == "uploadFiMMCard").value;
                                if (ValidFileTypes.FirstOrDefault(x => uploadFiMMCard.Contains(x)) != null)
                                {
                                    var uploadFiMMFileName = formInputs.FirstOrDefault(x => x.name == "uploadFiMMCardFileName").value;

                                    var last = uploadFiMMFileName.LastIndexOf('.');
                                    var butLast = uploadFiMMFileName.Substring(0, last).Replace(".", "");
                                    uploadFiMMFileName = butLast + uploadFiMMFileName.Substring(last);
                                    string uploadFiMMFileNameNew = uploadFiMMFileName.Split('.')[0] + fileCount + "." + uploadFiMMFileName.Split('.')[1];
                                    fileCount++;
                                    fileInputs.Add(new FileInput
                                    {
                                        Type = 6,
                                        Base64String = uploadFiMMCard,
                                        FileName = uploadFiMMFileName,
                                        FileNameNew = uploadFiMMFileNameNew
                                    });
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Please attach valid file type for FiMM Card";
                                    return response;
                                }
                            }
                            else if (agentRegFiles.FirstOrDefault(x => x.file_type == 6) == null)
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach FiMM Card";
                                return response;
                            }
                            // Upload Result
                            if (formInputs.FirstOrDefault(x => x.name == "uploadResult") != null)
                            {
                                var uploadResult = formInputs.FirstOrDefault(x => x.name == "uploadResult").value;
                                if (ValidFileTypes.FirstOrDefault(x => uploadResult.Contains(x)) != null)
                                {
                                    var uploadResultFileName = formInputs.FirstOrDefault(x => x.name == "uploadResultFileName").value;

                                    var last = uploadResultFileName.LastIndexOf('.');
                                    var butLast = uploadResultFileName.Substring(0, last).Replace(".", "");
                                    uploadResultFileName = butLast + uploadResultFileName.Substring(last);
                                    string uploadResultFileNameNew = uploadResultFileName.Split('.')[0] + fileCount + "." + uploadResultFileName.Split('.')[1];
                                    fileCount++;
                                    fileInputs.Add(new FileInput
                                    {
                                        Type = 7,
                                        Base64String = uploadResult,
                                        FileName = uploadResultFileName,
                                        FileNameNew = uploadResultFileNameNew
                                    });
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Please attach valid file type for Result";
                                    return response;
                                 }
                            }
                            else if (agentRegFiles.FirstOrDefault(x => x.file_type == 7) == null)
                            {
                                response.IsSuccess = false;
                                response.Message = "Please attach Result";
                                return response;
                            }
                        }
                        foreach (FileInput x in fileInputs)
                        //fileInputs.ForEach(x =>
                        {
                            string Base64 = x.Base64String.Split(',')[1];
                            byte[] bytes = Convert.FromBase64String(Base64);
                            File.WriteAllBytes(HttpContext.Current.Server.MapPath(path) + x.FileNameNew, bytes);

                            string filePath = path + x.FileNameNew;

                            //Response responseAOFist = IAccountOpeningFileService.GetDataByFilter(" account_opening_id = '" + accountOpeningExisting.Id + "' and file_type='" + x.Type + "' and status = 1 ", 0, 0, false);
                            //if (responseAOFist.IsSuccess)
                            {
                                //List<Utility.AccountOpeningFile> accountOpeningFs = (List<Utility.AccountOpeningFile>)responseAOFist.Data;
                                AgentRegFile agentRegFile = new AgentRegFile
                                {
                                    agent_reg_id = agentRegExisting.id,
                                    file_type = x.Type,
                                    name = x.FileNameNew,
                                    url = filePath,
                                    status = 1,
                                    created_date = DateTime.Now,
                                    updated_date = DateTime.Now
                                };
                                Response resPostFile = new Response();
                                if (agentRegFiles.FirstOrDefault(y => y.file_type == x.Type && y.status == 1) != null)
                                {
                                    AgentRegFile agentRegFileOld = agentRegFiles.FirstOrDefault(y => y.file_type == x.Type && y.status == 1);
                                    agentRegFileOld.status = 0;

                                    if (agentRegExisting.AgentRegExp == null)
                                    {
                                        resPostFile = GenericService.PostData<AgentRegFile>(agentRegFile);
                                    }
                                    else
                                    {
                                        agentRegFile.id = agentRegExisting.AgentRegFiles.FirstOrDefault(y => y.file_type == x.Type && y.status == 1).id;
                                        resPostFile = GenericService.UpdateData<AgentRegFile>(agentRegFile);
                                    }

                                    if (resPostFile.IsSuccess)
                                    {
                                        agentRegFile = (AgentRegFile)resPostFile.Data;
                                        Response responsePost = GenericService.UpdateData<AgentRegFile>(agentRegFileOld);
                                    }
                                    else
                                        response = resPostFile;
                                }
                                else
                                {
                                    resPostFile = GenericService.PostData<AgentRegFile>(agentRegFile);
                                    if (resPostFile.IsSuccess)
                                    {
                                    }
                                    else
                                    {
                                        response = resPostFile;
                                    }
                                }
                            }
                        }//);

                    }
                    else
                    {
                        return responseARFist;
                    }

                }

                response.Data = agentRegExisting;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Logger.WriteLog(ex.ToString());
                Logger.WriteLog(ex.Message);
                Logger.WriteLog(ex.StackTrace.ToString());
            }
            response.CustomValidations = customValidations;
            return response;
        }

        public class NameValuePairs
        {
            public String name { get; set; }
            public String value { get; set; }
        }
        public class FileInput
        {
            public Int32 Type { get; set; }
            public String Base64String { get; set; }
            public String FileName { get; set; }
            public String FileNameNew { get; set; }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            User user = (User)(HttpContext.Current.Session["user"]);
            if (user != null)
            {
                Response response = new Response() { IsSuccess = true };
                List<CustomValidation> customValidations = new List<CustomValidation>();
                bool isChecked = chkDeclare.Checked;
                if (isChecked)
                {
                    AgentReg agentRegExisting = new AgentReg();
                    string queryAgentReg = (@" select * from agent_regs where user_id=" + user.Id + " and process_status=0 and status=1 ");
                    Response responseAgentRegList = GenericService.GetDataByQuery(queryAgentReg, 0, 0, false, null, false, null, true);
                    if (responseAgentRegList.IsSuccess)
                    {
                        var agentsDyn = responseAgentRegList.Data;
                        var responseARegJSON = JsonConvert.SerializeObject(agentsDyn);
                        List<AgentReg> agentRegs = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseARegJSON);
                        if (agentRegs.Count == 1)
                        {
                            agentRegExisting = agentRegs.FirstOrDefault();
                            agentRegExisting.process_status = 1;
                            agentRegExisting.updated_date = DateTime.Now;
                            Response responsePost = GenericService.UpdateData<AgentReg>(agentRegExisting);
                            if (responsePost.IsSuccess)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Request submitted successfully\", '/Portfolio.aspx');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responsePost.Message + "\", '');", true);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegList.Message + "\", '');", true);
                    }
                }
                else {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Terms and Conditions is required. \", '');", true);

                }
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Session expired.\", '/Index.aspx');", true);
            }
        }

    }
}