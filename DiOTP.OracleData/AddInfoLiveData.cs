﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class AddInfoLiveData
    {

        public DataTable GenerateData(string Query)
        {
            DataManager DB = new DataManager();
            DataTable ds = new DataTable();
            string ConnectionString = DB.GetOracleConnection();
            OracleConnection conn = new OracleConnection(ConnectionString);
            try
            {
                string mysql = "";
                OracleCommand cmd = new OracleCommand(mysql, conn);
                conn.Open();
                cmd.CommandType = CommandType.Text;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
            }
            finally
            {
                conn.Close();
            }
            return ds;

        }
    }
}
