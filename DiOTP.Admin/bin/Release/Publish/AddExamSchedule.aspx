﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddExamSchedule.aspx.cs" Inherits="Admin.AddExamSchedule" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="addExamScheduleForm" runat="server">
        <div class="row" id="FormId" data-value="addExamScheduleForm">
            <div class="col-md-12">
                
                 <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Location:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="Location" id="Location" runat="server" clientidmode="static" class="form-control" placeholder="Enter Location" />
                    </div>
                </div>

                 <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Language:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="Language" id="Language" runat="server" clientidmode="static" class="form-control" placeholder="Enter Language" />
                    </div>
                </div>

                 <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Exam date:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="ExamDate" id="ExamDate" runat="server" clientidmode="static" class="form-control" placeholder="Select date" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Timing 1:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="SessionTime1" id="SessionTime1" runat="server" clientidmode="static" class="form-control" placeholder="Select time" disabled="disabled" />
                    </div>
                </div>
                
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Timing 2:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="SessionTime2" id="SessionTime2" runat="server" clientidmode="static" class="form-control" placeholder="Select time" disabled="disabled" />
                    </div>
                </div>
                
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Timing 3:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="SessionTime3" id="SessionTime3" runat="server" clientidmode="static" class="form-control" placeholder="Select time" disabled="disabled" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $ui(document).ready(function () {
            $ui('#ExamDate').datepicker({
                format: 'dd/mm/yyyy',
                pickTime: false,
                todayBtn: true,
                todayHighlight: true,
                autoclose: true,
            }).on('changeDate', function (ev) {
                console.log(ev.date);
                $ui('#SessionTime1, #SessionTime2, #SessionTime3').datetimepicker({
                    format: 'HH:ii P',
                    autoclose: true,
                    startView: 1,
                    minView: 0,
                    maxView: 1,
                    showMeridian: true,
                    minuteStep: 30,
                    initialDate: ev.date
                });
                $ui('#SessionTime1, #SessionTime2, #SessionTime3').removeAttr('disabled')
            });

            

            //$ui('#ToDate').datepicker({
            //    format: 'mm/dd/yyyy',
            //    pickTime: false,
            //    autoclose: true
            //});
        });
    </script>
</body>
</html>
