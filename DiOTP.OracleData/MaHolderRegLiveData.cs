﻿using System;
using System.Collections.Generic;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Data;
using DiOTP.Utility;
using MySql.Data.MySqlClient;
using System.Reflection;
using KellermanSoftware.CompareNetObjects;
using DiOTP.Utility.Helper;
using System.Globalization;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.OracleData
{
    public class MaHolderRegLiveData
    {
        public static Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            DataManager DB = new DataManager();
            Response response = new Response();
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                string query = "select a.REF_VALUE as NOB, j.TEL_NO as JOINT_TEL_NO, j.ADDR_1 as JOINT_ADDR_1, j.ADDR_2 as JOINT_ADDR_2, j.ADDR_3 as JOINT_ADDR_3, j.ADDR_4 as JOINT_ADDR_4, j.POSTCODE as JOINT_POSTCODE, j.REGION as JOINT_REGION, j.STATE as JOINT_STATE, j.COUNTRY_RES as JOINT_COUNTRY_RES, j.NATIONALITY as JOINT_NATIONALITY, j.RACE as JOINT_RACE, j.OCC_CODE as JOINT_OCC_CODE, j.BIRTH_DT as JOINT_BIRTH_DT, j.SEX as JOINT_SEX, j.INCOME_CODE as JOINT_INCOME_CODE, j.RELATION_CODE as JOINT_RELATION_CODE, j.SIGNATURE_IND as JOINT_SIGNATURE_IND, j.NO_OF_DPNDNT as JOINT_NO_OF_DPNDNT, j.DOWNLOAD_IND as JOINT_DOWNLOAD_IND, h.* from uts.holder_reg h left join UTS.joint_reg j on j.ID_NO = h.ID_NO_2 and h.holder_no = j.holder_no " + filter;
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                //MySqlCommand cmd = new MySqlCommand(query, /*mySQLDBConnect.connection*/);
                //mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                response = GenerateDataTable(query);
                //MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                //dA.Fill(dt);
                if (response.IsSuccess)
                {
                    dt = (DataTable)response.Data;
                    objs = (from x in dt.AsEnumerable()
                            select new MaHolderReg
                            {
                                //Id = x.Field<Int32>("id"),
                                IdNo = x.Field<String>("id_no"),
                                IdNoOld = x.Field<String>("id_no_old"),
                                IdNo2 = x.Field<String>("id_no_2"),
                                IdNoOld2 = x.Field<String>("id_no_old_2"),
                                IdNo3 = x.Field<String>("id_no_3"),
                                IdNoOld3 = x.Field<String>("id_no_old_3"),
                                IdNo4 = x.Field<String>("id_no_4"),
                                IdNoOld4 = x.Field<String>("id_no_old_4"),
                                IdNo5 = x.Field<String>("id_no_5"),
                                IdNoOld5 = x.Field<String>("id_no_old_5"),
                                HolderNo = x.Field<Int32?>("holder_no") == null ? 0 : x.Field<Int32>("holder_no"),
                                Title = x.Field<String>("title"),
                                Name1 = x.Field<String>("name_1"),
                                Name2 = x.Field<String>("name_2"),
                                Name3 = x.Field<String>("name_3"),
                                Name4 = x.Field<String>("name_4"),
                                Name5 = x.Field<String>("name_5"),
                                Addr1 = x.Field<String>("addr_1"),
                                Addr2 = x.Field<String>("addr_2"),
                                Addr3 = x.Field<String>("addr_3"),
                                Addr4 = x.Field<String>("addr_4"),
                                Postcode = Convert.ToInt32(x.Field<String>("postcode") != "" ? x.Field<String>("postcode") : "0"),
                                TelNo = x.Field<String>("tel_no"),
                                RegionCode = Convert.ToInt32(x.Field<String>("region_code") == "" ? "0" : x.Field<String>("region_code")),

                                StateCode = Convert.ToInt32(x.Field<String>("state_code") == "" ? "0" : x.Field<String>("state_code")),
                                CountryIssue = x.Field<String>("country_issue"),
                                CountryIssue1 = x.Field<String>("country_issue1"),
                                CountryIssue2 = x.Field<String>("country_issue2"),
                                CountryIssue3 = x.Field<String>("country_issue3"),
                                CountryRes = x.Field<String>("country_res"),
                                CountryIncorp = x.Field<String>("country_incorp"),
                                Nationality = x.Field<String>("nationality"),
                                Race = x.Field<String>("race"),
                                OccCode = x.Field<String>("occ_code"),
                                CorpStatus = x.Field<String>("corp_status"),
                                BusinessType = x.Field<String>("business_type"),
                                ContactPerson = x.Field<String>("contact_person"),
                                PositionHeld = x.Field<String>("position_held"),
                                AccountType = x.Field<String>("account_type"),
                                DivPymt = x.Field<String>("div_pymt"),
                                HolderCls = x.Field<String>("holder_cls"),
                                HolderInd = x.Field<String>("holder_ind"),
                                HolderStatus = x.Field<String>("holder_status"),
                                RegDt = (x.Field<DateTime?>("reg_dt") != null ? x.Field<DateTime>("reg_dt").ToString("M/dd/yyyy") : ""),
                                RegBrn = x.Field<String>("reg_brn"),
                                BirthDt = (x.Field<DateTime?>("birth_dt") != null ? x.Field<DateTime>("birth_dt").ToString("M/dd/yyyy") : ""),
                                Sex = x.Field<String>("sex"),
                                TelNo2 = x.Field<String>("tel_no_2"),
                                AgentCode = x.Field<String>("agent_code") == "" ? "0" : x.Field<String>("agent_code"),
                                AgentType = x.Field<String>("agent_type"),
                                AgentId = Convert.ToInt32(x.Field<String>("agent_id") == "" ? "0" : x.Field<String>("agent_id")),
                                IncomeTaxNo = x.Field<String>("income_tax_no"),
                                HandPhoneNo = x.Field<String>("hand_phone_no"),
                                OffPhoneNo = x.Field<String>("off_phone_no"),
                                PrmntAddr1 = x.Field<String>("prmnt_addr_1"),
                                PrmntAddr2 = x.Field<String>("prmnt_addr_2"),
                                PrmntAddr3 = x.Field<String>("prmnt_addr_3"),
                                PrmntAddr4 = x.Field<String>("prmnt_addr_4"),
                                PrmntPostCode = Convert.ToInt32(x.Field<String>("prmnt_post_code") == "" ? "0" : x.Field<String>("prmnt_post_code")),
                                PrmntRegion = Convert.ToInt32(x.Field<String>("prmnt_region") == "" ? "0" : x.Field<String>("prmnt_region")),
                                PrmntState = x.Field<String>("prmnt_state"),
                                Salutation = x.Field<String>("salutation"),

                                HAccType = x.Field<String>("h_acc_type"),
                                Marital = x.Field<String>("marital"),

                                NoOfDpndnt = x.Field<Int16?>("no_of_dpndnt") == null ? 0 : x.Field<Int16>("no_of_dpndnt"),

                                RcvMaterial = x.Field<String>("rcv_material"),
                                MaterialLan = x.Field<String>("material_lan"),

                                Field1 = x.Field<String>("field_1"),
                                Field2 = x.Field<String>("field_2"),
                                Field3 = x.Field<String>("field_3"),
                                Field4 = x.Field<String>("field_4"),
                                Field5 = x.Field<String>("field_5"),
                                Field6 = x.Field<String>("field_6"),
                                Field7 = x.Field<String>("field_7"),
                                Field8 = x.Field<String>("field_8"),
                                Field9 = x.Field<String>("field_9"),
                                Field10 = x.Field<String>("field_10"),
                                FieldDesc1 = x.Field<String>("field_desc_1"),
                                FieldDesc2 = x.Field<String>("field_desc_2"),
                                FieldDesc3 = x.Field<String>("field_desc_3"),
                                FieldDesc4 = x.Field<String>("field_desc_4"),
                                FieldDesc5 = x.Field<String>("field_desc_5"),
                                FieldDesc6 = x.Field<String>("field_desc_6"),
                                FieldDesc7 = x.Field<String>("field_desc_7"),
                                FieldDesc8 = x.Field<String>("field_desc_8"),
                                FieldDesc9 = x.Field<String>("field_desc_9"),
                                FieldDesc10 = x.Field<String>("field_desc_10"),

                                LastDisbDt = (x.IsNull("last_disb_dt") ? default(DateTime) : (x.Field<DateTime>("last_disb_dt"))),
                                NextDisbDt = (x.IsNull("next_disb_dt") ? default(DateTime) : (x.Field<DateTime>("next_disb_dt"))),
                                PendDisbDt = (x.IsNull("pend_disb_dt") ? default(DateTime) : (x.Field<DateTime>("pend_disb_dt"))),

                                DownloadInd = x.Field<String>("download_ind"),
                                IdentityInd = x.Field<String>("identity_ind"),
                                EpfIStatus = x.Field<String>("epf_i_status"),

                                EpfIEffDt = (x.IsNull("epf_i_eff_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_eff_dt"))),
                                EpfILstUpdDt = (x.IsNull("epf_i_lst_upd_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_lst_upd_dt"))),
                                EntDt = (x.IsNull("ent_dt") ? default(DateTime) : (x.Field<DateTime>("ent_dt"))),
                                EntTime = (x.IsNull("ent_time") ? default(DateTime) : (x.Field<DateTime>("ent_time"))),

                                EntTerm = x.Field<String>("ent_term"),
                                EntBy = x.Field<String>("ent_by"),

                                ChgDt = (x.IsNull("chg_dt") ? default(DateTime) : (x.Field<DateTime>("chg_dt"))),
                                ChgTime = (x.IsNull("chg_time") ? default(DateTime) : (x.Field<DateTime>("chg_time"))),

                                ChgTerm = x.Field<String>("chg_term"),
                                ChgBy = x.Field<String>("chg_by"),
                                //LastDisbDt = (x.Field<DateTime?>("last_disb_dt") != null ? x.Field<DateTime>("last_disb_dt").ToString("M/dd/yyyy") : ""),
                                //NextDisbDt = (x.Field<DateTime?>("next_disb_dt") != null ? x.Field<DateTime>("next_disb_dt").ToString("M/dd/yyyy") : ""),
                                //PendDisbDt = (x.Field<DateTime?>("pend_disb_dt") != null ? x.Field<DateTime>("pend_disb_dt").ToString("M/dd/yyyy") : ""),
                                //DownloadInd = x.Field<String>("download_ind"),
                                //IdentityInd = x.Field<String>("identity_ind"),
                                //EpfIStatus = x.Field<String>("epf_i_status"),
                                //EpfIEffDt = (x.Field<DateTime?>("epf_i_eff_dt") != null ? x.Field<DateTime>("epf_i_eff_dt").ToString("M/dd/yyyy") : ""),
                                //EpfILstUpdDt = (x.Field<DateTime?>("epf_i_lst_upd_dt") != null ? x.Field<DateTime>("epf_i_lst_upd_dt").ToString("M/dd/yyyy") : ""),
                                //EntDt = (x.Field<DateTime?>("ent_dt") != null ? x.Field<DateTime>("ent_dt").ToString("M/dd/yyyy") : ""),
                                //EntTime = (x.Field<DateTime?>("ent_time") != null ? x.Field<DateTime>("ent_time").ToString("M/dd/yyyy") : ""),
                                //EntTerm = x.Field<String>("ent_term"),
                                //EntBy = x.Field<String>("ent_by"),
                                //ChgDt = (x.Field<DateTime?>("chg_dt") != null ? x.Field<DateTime>("chg_dt").ToString("M/dd/yyyy") : ""),
                                //ChgTime = (x.Field<DateTime?>("chg_time") != null ? x.Field<DateTime>("chg_time").ToString("M/dd/yyyy") : ""),
                                //ChgTerm = x.Field<String>("chg_term"),
                                //ChgBy = x.Field<String>("chg_by"),

                                JointTelNo = x.Field<String>("joint_tel_no"),
                                JointAddr1 = x.Field<String>("joint_addr_1"),
                                JointAddr2 = x.Field<String>("joint_addr_2"),
                                JointAddr3 = x.Field<String>("joint_addr_3"),
                                JointAddr4 = x.Field<String>("joint_addr_4"),
                                JointPostcode = x.Field<String>("joint_postcode"),
                                JointRegion = x.Field<String>("joint_region"),
                                JointState = x.Field<String>("joint_state"),
                                JointCountryRes = x.Field<String>("joint_country_res"),
                                JointNationality = x.Field<String>("joint_nationality"),
                                JointRace = x.Field<String>("joint_race"),
                                JointOccCode = x.Field<String>("joint_occ_code"),
                                JointBirthDt = (x.Field<DateTime?>("joint_birth_dt") != null ? x.Field<DateTime>("joint_birth_dt").ToString("M/dd/yyyy") : ""),
                                JointSex = x.Field<String>("joint_sex"),
                                JointIncomeCode = x.Field<String>("joint_income_code"),
                                JointRelationCode = x.Field<String>("joint_relation_code"),
                                JointSignatureInd = x.Field<String>("joint_signature_ind"),
                                JointNoOfDpndnt = x.Field<Int16?>("joint_no_of_dpndnt") == null ? 0 : x.Field<Int16>("joint_no_of_dpndnt"),
                                JointDownloadInd = x.Field<String>("joint_download_ind"),

                            }).ToList();
                    response.IsSuccess = true;
                    response.Data = objs;
                }
                else
                {
                    return response;
                }
                //mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetSingle(Int32 holder_no)
        {
            Response response = new Response();
            Console.WriteLine("Holder: " + holder_no);
            MaHolderReg obj = new MaHolderReg();
            try
            {
                string query = "select * from uts.holder_reg where holder_no = '" + holder_no + "'";
                //MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                //mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                //MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                //dA.Fill(dt);
                response = GenerateDataTable(query);
                if (response.IsSuccess)
                {
                    dt = (DataTable)response.Data;
                    obj = (from x in dt.AsEnumerable()
                           select new MaHolderReg
                           {
                               //Id = x.Field<Int32>("id"),
                               IdNo = x.Field<String>("id_no"),
                               IdNoOld = x.Field<String>("id_no_old"),
                               IdNo2 = x.Field<String>("id_no_2"),
                               IdNoOld2 = x.Field<String>("id_no_old_2"),
                               IdNo3 = x.Field<String>("id_no_3"),
                               IdNoOld3 = x.Field<String>("id_no_old_3"),
                               IdNo4 = x.Field<String>("id_no_4"),
                               IdNoOld4 = x.Field<String>("id_no_old_4"),
                               IdNo5 = x.Field<String>("id_no_5"),
                               IdNoOld5 = x.Field<String>("id_no_old_5"),
                               HolderNo = x.Field<Int32?>("holder_no") == null ? 0 : x.Field<Int32>("holder_no"),
                               Title = x.Field<String>("title"),
                               Name1 = x.Field<String>("name_1"),
                               Name2 = x.Field<String>("name_2"),
                               Name3 = x.Field<String>("name_3"),
                               Name4 = x.Field<String>("name_4"),
                               Name5 = x.Field<String>("name_5"),
                               Addr1 = x.Field<String>("addr_1"),
                               Addr2 = x.Field<String>("addr_2"),
                               Addr3 = x.Field<String>("addr_3"),
                               Addr4 = x.Field<String>("addr_4"),
                               Postcode = Convert.ToInt32(x.Field<String>("postcode") != "" ? x.Field<String>("postcode") : "0"),
                               TelNo = x.Field<String>("tel_no"),
                               RegionCode = Convert.ToInt32(x.Field<String>("region_code") == "" ? "0" : x.Field<String>("region_code")),
                               StateCode = Convert.ToInt32(x.Field<String>("state_code") == "" ? "0" : x.Field<String>("state_code")),
                               CountryIssue = x.Field<String>("country_issue"),
                               CountryIssue1 = x.Field<String>("country_issue1"),
                               CountryIssue2 = x.Field<String>("country_issue2"),
                               CountryIssue3 = x.Field<String>("country_issue3"),
                               CountryRes = x.Field<String>("country_res"),
                               CountryIncorp = x.Field<String>("country_incorp"),
                               Nationality = x.Field<String>("nationality"),
                               Race = x.Field<String>("race"),
                               OccCode = x.Field<String>("occ_code"),
                               CorpStatus = x.Field<String>("corp_status"),
                               BusinessType = x.Field<String>("business_type"),
                               ContactPerson = x.Field<String>("contact_person"),
                               PositionHeld = x.Field<String>("position_held"),
                               AccountType = x.Field<String>("account_type"),
                               DivPymt = x.Field<String>("div_pymt"),
                               HolderCls = x.Field<String>("holder_cls"),
                               HolderInd = x.Field<String>("holder_ind"),
                               HolderStatus = x.Field<String>("holder_status"),
                               RegDt = (x.Field<DateTime?>("reg_dt") != null ? x.Field<DateTime>("reg_dt").ToString("M/dd/yyyy") : ""),
                               RegBrn = x.Field<String>("reg_brn"),
                               BirthDt = (x.Field<DateTime?>("birth_dt") != null ? x.Field<DateTime>("birth_dt").ToString("M/dd/yyyy") : ""),
                               Sex = x.Field<String>("sex"),
                               TelNo2 = x.Field<String>("tel_no_2"),
                               AgentCode = x.Field<String>("agent_code") == "" ? "0" : x.Field<String>("agent_code"),
                               AgentType = x.Field<String>("agent_type"),
                               AgentId = Convert.ToInt32(x.Field<String>("agent_id") == "" ? "0" : x.Field<String>("agent_id")),
                               IncomeTaxNo = x.Field<String>("income_tax_no"),
                               HandPhoneNo = x.Field<String>("hand_phone_no"),
                               OffPhoneNo = x.Field<String>("off_phone_no"),
                               PrmntAddr1 = x.Field<String>("prmnt_addr_1"),
                               PrmntAddr2 = x.Field<String>("prmnt_addr_2"),
                               PrmntAddr3 = x.Field<String>("prmnt_addr_3"),
                               PrmntAddr4 = x.Field<String>("prmnt_addr_4"),
                               PrmntPostCode = Convert.ToInt32(x.Field<String>("prmnt_post_code") == "" ? "0" : x.Field<String>("prmnt_post_code")),
                               PrmntRegion = Convert.ToInt32(x.Field<String>("prmnt_region") == "" ? "0" : x.Field<String>("prmnt_region")),
                               PrmntState = x.Field<String>("prmnt_state"),
                               Salutation = x.Field<String>("salutation"),
                               HAccType = x.Field<String>("h_acc_type"),
                               Marital = x.Field<String>("marital"),
                               NoOfDpndnt = x.Field<Int32?>("no_of_dpndnt") == null ? 0 : x.Field<Int32>("no_of_dpndnt"),
                               RcvMaterial = x.Field<String>("rcv_material"),
                               MaterialLan = x.Field<String>("material_lan"),
                               Field1 = x.Field<String>("field_1"),
                               Field2 = x.Field<String>("field_2"),
                               Field3 = x.Field<String>("field_3"),
                               Field4 = x.Field<String>("field_4"),
                               Field5 = x.Field<String>("field_5"),
                               Field6 = x.Field<String>("field_6"),
                               Field7 = x.Field<String>("field_7"),
                               Field8 = x.Field<String>("field_8"),
                               Field9 = x.Field<String>("field_9"),
                               Field10 = x.Field<String>("field_10"),
                               FieldDesc1 = x.Field<String>("field_desc_1"),
                               FieldDesc2 = x.Field<String>("field_desc_2"),
                               FieldDesc3 = x.Field<String>("field_desc_3"),
                               FieldDesc4 = x.Field<String>("field_desc_4"),
                               FieldDesc5 = x.Field<String>("field_desc_5"),
                               FieldDesc6 = x.Field<String>("field_desc_6"),
                               FieldDesc7 = x.Field<String>("field_desc_7"),
                               FieldDesc8 = x.Field<String>("field_desc_8"),
                               FieldDesc9 = x.Field<String>("field_desc_9"),
                               FieldDesc10 = x.Field<String>("field_desc_10"),
                               LastDisbDt = (x.IsNull("last_disb_dt") ? default(DateTime) : (x.Field<DateTime>("last_disb_dt"))),
                               NextDisbDt = (x.IsNull("next_disb_dt") ? default(DateTime) : (x.Field<DateTime>("next_disb_dt"))),
                               PendDisbDt = (x.IsNull("pend_disb_dt") ? default(DateTime) : (x.Field<DateTime>("pend_disb_dt"))),
                               DownloadInd = x.Field<String>("download_ind"),
                               IdentityInd = x.Field<String>("identity_ind"),
                               EpfIStatus = x.Field<String>("epf_i_status"),
                               EpfIEffDt = (x.IsNull("epf_i_eff_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_eff_dt"))),
                               EpfILstUpdDt = (x.IsNull("epf_i_lst_upd_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_lst_upd_dt"))),
                               EntDt = (x.IsNull("ent_dt") ? default(DateTime) : (x.Field<DateTime>("ent_dt"))),
                               EntTime = (x.IsNull("ent_time") ? default(DateTime) : (x.Field<DateTime>("ent_time"))),
                               EntTerm = x.Field<String>("ent_term"),
                               EntBy = x.Field<String>("ent_by"),
                               ChgDt = (x.IsNull("chg_dt") ? default(DateTime) : (x.Field<DateTime>("chg_dt"))),
                               ChgTime = (x.IsNull("chg_time") ? default(DateTime) : (x.Field<DateTime>("chg_time"))),
                               ChgTerm = x.Field<String>("chg_term"),
                               ChgBy = x.Field<String>("chg_by"),

                               //OtpActSt = x.Field<String>("otp_act_st"),
                               //OtpEmailAdd = x.Field<String>("otp_email_add"),
                               //OtpMobileNo = x.Field<String>("otp_mobile_no"),
                               //OtpExpiryDate = x.Field<DateTime?>("otp_expiry_date"),
                               //OtpVersion = x.Field<String>("otp_version"),
                               //OtpEntBy = x.Field<String>("otp_ent_by"),
                               //OtpEntDt = x.Field<DateTime?>("otp_ent_dt"),
                               //OtpTacCd = x.Field<String>("otp_tac_cd"),
                               JointTelNo = x.Field<String>("joint_tel_no"),
                               JointAddr1 = x.Field<String>("joint_addr_1"),
                               JointAddr2 = x.Field<String>("joint_addr_2"),
                               JointAddr3 = x.Field<String>("joint_addr_3"),
                               JointAddr4 = x.Field<String>("joint_addr_4"),
                               JointPostcode = x.Field<String>("joint_postcode"),
                               JointRegion = x.Field<String>("joint_region"),
                               JointState = x.Field<String>("joint_state"),
                               JointCountryRes = x.Field<String>("joint_country_res"),
                               JointNationality = x.Field<String>("joint_nationality"),
                               JointRace = x.Field<String>("joint_race"),
                               JointOccCode = x.Field<String>("joint_occ_code"),
                               JointBirthDt = x.Field<String>("joint_birth_dt"),
                               JointSex = x.Field<String>("joint_sex"),
                               JointIncomeCode = x.Field<String>("joint_income_code"),
                               JointRelationCode = x.Field<String>("joint_relation_code"),
                               JointSignatureInd = x.Field<String>("joint_signature_ind"),
                               JointNoOfDpndnt = x.Field<Int16?>("joint_no_of_dpndnt") == null ? 0 : x.Field<Int16>("joint_no_of_dpndnt"),
                               JointDownloadInd = x.Field<String>("joint_download_ind"),

                           }).ToList().FirstOrDefault();
                    response.IsSuccess = true;
                    response.Data = obj;
                }
                else
                {
                    return response;
                }
                //mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Boolean PostDataSingle(MaHolderReg obj)
        {
            DataManager DB = new DataManager();
            var success = false;
            Response response1 = GetSingle(Convert.ToInt32(obj.HolderNo));
            if (response1.IsSuccess)
            {
                MaHolderReg ExistingRecord = (MaHolderReg)response1.Data;

                StringBuilder sb = new StringBuilder();
                int propertyCount = typeof(MaHolderReg).GetProperties().Length;

                CompareLogic basicComparison = new CompareLogic()
                { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                List<Difference> diffs = basicComparison.Compare(ExistingRecord, obj).Differences;
                int noOfDifferent = diffs.Count();

                foreach (Difference diff in diffs)
                {
                    string columnName = Converter.GetColumnNameByPropertyName<MaHolderReg>(diff.PropertyName);
                    if (noOfDifferent != 1)
                        sb.Append(columnName + "='" + diff.Object2Value + "',");
                    else
                        sb.Append(columnName + "='" + diff.Object2Value + "'");
                    //sb.AppendLine("Property name:" + diff.PropertyName);
                    //sb.AppendLine("Column name:" + columnName);
                    //sb.AppendLine("Old value:" + diff.Object1Value);
                    //sb.AppendLine("New value:" + diff.Object2Value + "\n");
                    noOfDifferent--;
                }


                string ConnectionString = DB.GetOracleConnection();
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    OracleCommand cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "update uts.holder_reg set " + sb + " where holder_no = " + obj.HolderNo;
                    cmd.Parameters.Add(":holder_no", obj.HolderNo);
                    int rowsupdate = cmd.ExecuteNonQuery();
                    if (rowsupdate > 0) success = true;
                    else
                    {
                        success = false;
                    }
                }

            }
            return success;
        }

        public static MaHolderReg UpdateDataSingle(MaHolderReg obj)
        {
            return obj;
        }

        public static Response GenerateDataTable(string Query)
        {
            DataManager DB = new DataManager();
            Response response = new Response();
            DataTable ds = new DataTable();
            string ConnectionString = DB.GetOracleConnection();
            OracleConnection conn = new OracleConnection(ConnectionString);
            try
            {
                string mysql = Query;

                OracleCommand cmd = new OracleCommand(mysql, conn);
                conn.Open();
                cmd.CommandType = CommandType.Text;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(ds);
                response.IsSuccess = true;
                response.Data = ds;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Logger.WriteLog(ex.Message);
                //Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
            }
            finally
            {
                conn.Close();
            }
            return response;

        }
    }
}
