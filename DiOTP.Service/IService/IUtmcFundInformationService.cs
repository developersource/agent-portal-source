using DiOTP.Utility.Helper;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Service.IService
{
    public interface IUtmcFundInformationService : IDefaultInterface<UtmcFundInformation>
    {
        List<FundDetailedInformation> GetFundDetailedInformation();
        List<FundChartInformation> GenerateFundChartInfo(Int32[] selectedFunds, Int32 dataValue);
    }
}
