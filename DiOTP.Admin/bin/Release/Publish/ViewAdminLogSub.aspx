﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAdminLogSub.aspx.cs" Inherits="Admin.ViewAdminLogSub" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <table class="display responsive nowrap table table-bordered mb-30" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Index</th>
                                <th>Column Name</th>
                                <th>Old Value</th>
                                <th>New Value</th>
                            </tr>
                        </thead>
                        <tbody id="userTbody" runat="server">
                            <tr>
                                <td>Index</td>
                                <td>Column Name</td>
                                <td>Old Value</td>
                                <td>New Value</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
