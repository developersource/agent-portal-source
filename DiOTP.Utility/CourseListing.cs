﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_training_topics")]
    public class CourseListing
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("topic"), Display("Topic")]
        public String Topic { get; set; }
        [Database("synopsis"), Display("Synopsis")]
        public String Synopsis { get; set; }
        [Database("outcome"), Display("Outcome")]
        public String Outcome { get; set; }
        [Database("is_online"), Display("IsOnline")]
        public Int32 IsOnline { get; set; }
        [Database("venue"), Display("Venue")]
        public String Venue { get; set; }
        [Database("license_type"), Display("LicenseType")]
        public String LicenseType { get; set; }
    
        
        [Database("attendies"), Display("Attendies")]
        public String Attendies { get; set; }
        [Database("duration_type"), Display("DurationType")]
        public Int32 DurationType { get; set; }
        [Database("duration"), Display("Duration")]
        public String Duration { get; set; }
        [Database("duration_note"), Display("DurationNote")]
        public String DurationNote { get; set; }
        [Database("enroll_end_date"), Display("EnrollEndDate")]
        public DateTime EnrollEndDate { get; set; }
        [Database("cpd_points"), Display("CpdPoints")]
        public Int32 CpdPoints { get; set; }
        [Database("special_note"), Display("SpecialNote")]
        public String SpecialNote { get; set; }
        [Database("start_date"), Display("StartDate")]
        public DateTime StartDate { get; set; }
        [Database("start_assigned_by"), Display("StartAssignedBy")]
        public Int32 StartAssignedBy { get; set; }
        [Database("course_file_url"), Display("CourseFileUrl")]
        public String CourseFileUrl { get; set; }
        [Database("start_url"), Display("StartUrl")]
        public String StartUrl { get; set; }
        [Database("join_url"), Display("JoinUrl")]
        public String JoinUrl { get; set; }
        [Database("instructor_id"), Display("Instructor")]
        public Int32 InstructorId { get; set; }
        [Database("capacity"), Display("Capacity")]
        public Int32 Capacity { get; set; }
        [Database("is_assessment"), Display("IsAssessment")]
        public Int32 IsAssessment { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32 UpdatedBy { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }

    public class CourseListingCustom
    {
        public Int32 Id { get; set; }
        public String Topic { get; set; }
        public String Synopsis { get; set; }
        public String Outcome { get; set; }
        public String IsOnline { get; set; }
        public String Venue { get; set; }
        public String Capacity { get; set; }
        public Int32 LicenseType { get; set; }
        public String UTS { get; set; }
        public String PRS { get; set; }
        public String notApplicable { get; set; }
        public String NewJoiners { get; set; }
        public String SGM { get; set; }
        public String GM { get; set; }
        public Int32 Attendies { get; set; }
        public String IsAssessment { get; set; }
        public String InstructorId { get; set; }
        public String InstructorName { get; set; }
        public Int32 DurationType { get; set; }
        public String Duration { get; set; }
        public String Day { get; set; }
        public String Hour { get; set; }
        public String Minute { get; set; }
        public String DurationNote { get; set; }
        public DateTime EnrollEndDate { get; set; }
        public String CpdPoints { get; set; }
        public String SpecialNote { get; set; }
        public String CourseFileUrl { get; set; }
        public String StartUrl { get; set; }
        public String JoinUrl { get; set; }
    }

    public class CourseListingView
    {
        public Int32 Id { get; set; }
        public String Topic { get; set; }
        public String Synopsis { get; set; }
        public String Outcome { get; set; }
        public Int32 IsOnline { get; set; }
        public String Venue { get; set; }
        public String LicenseType { get; set; }
        public String Attendies { get; set; }
        public Int32 DurationType { get; set; }
        public String Duration { get; set; }
        public String DurationNote { get; set; }
        public String CourseFileUrl { get; set; }
        public DateTime EnrollEndDate { get; set; }
        public Int32 CpdPoints { get; set; }
        public String SpecialNote { get; set; }
        public DateTime StartDate { get; set; }
        public Int32 StartAssignedBy { get; set; }
        public String StartUrl { get; set; }
        public String JoinUrl { get; set; }
        public Int32 InstructorId { get; set; }
        public String InstructorName { get; set; }
        public Int32 Capacity { get; set; }
        public Int32 IsAssessment { get; set; }
        public Int32 IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Int32 UpdatedBy { get; set; }
        public Int32 Status { get; set; }
        public DateTime EnrolledDate { get; set; }
    }
}
