﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using Newtonsoft.Json;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KellermanSoftware.CompareNetObjects;

namespace Admin
{
    public partial class Admins : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserTypesDefService> lazyUserTypesDefServiceObj = new Lazy<IUserTypesDefService>(() => new UserTypesDefService());
        public static IUserTypesDefService IUserTypesDefService { get { return lazyUserTypesDefServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT u.id,u.username,t.user_type_id,d.name as user_type,u.mobile_number,u.email_id,u.last_login_date,u.created_date,u.status from ");
            string mainQCount = (@"SELECT count(*) from ");
            filter.Append(@" users u
                            join user_types t on u.id = t.user_id
                            join user_types_def d on t.user_type_id = d.id where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.email_id like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.mobile_number like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]) && Request.QueryString["FilterValue"].Trim() != "0")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and t.user_type_id in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(@" and t.user_type_id in ('3','4','5','10','11') ");
                }

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by u.status desc, u.created_date desc");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }


                Response responseAdmins = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, true);


                if (responseAdmins.IsSuccess)
                {


                    var AdminsDyn = responseAdmins.Data;
                    var responseJSON = JsonConvert.SerializeObject(AdminsDyn);
                    List<DiOTP.Utility.Admins> Admins = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Admins>>(responseJSON);

                    StringBuilder asb = new StringBuilder();


                    int index = 1;
                    {
                        Boolean isCurrentUser = false;
                        Boolean isCurrentUserType = false;

                        Admins.ForEach(x =>
                        {
                            if (user.Id == x.Id)
                                isCurrentUser = true;
                            if (user.UserRoleId == x.user_type_id)
                            {
                                Response responseUT = IUserTypeService.GetDataByFilter(" user_id='" + x.Id + "'", 0, 0, false);
                                if (responseUT.IsSuccess)
                                {
                                    UserType userType = ((List<UserType>)responseUT.Data).Where(y => y.UserTypeId == loginUserType.UserTypeId).FirstOrDefault();
                                    if (userType != null)
                                    { isCurrentUserType = true; }
                                }
                            }
                            asb.Append(@"<tr>
                                    <td class='" + (isCurrentUserType ? "" : "icheck") + @"'>
                                        " + (isCurrentUserType ? "" : @"<div class='square single-row'>
                                            <div class='checkbox'>
                                                <input type='checkbox' name='checkRow' class='checkRow' value='" + x.Id + @"'" + @" /> <label>" + index + @"</label><br/>
                                            </div>
                                        </div>") + @"
                                        <span class='row-status'>" + (x.status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Username</td>
                                                    <td><strong>" + x.username + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>User type</td>
                                                    <td><strong>" + x.user_type + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><strong>" + x.email_id + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table class='table'>
                                            <tbody>
                                                <tr>
                                                    <td>Mobile number</td>
                                                    <td><strong>" + x.mobile_number + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td style='width:50%;'>Created date</td>
                                                    <td><strong>" + x.created_date + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Last login</td>
                                                    <td><strong>" + (x.last_login_date != null && x.last_login_date.ToString("yyyy") != "0001" ? x.last_login_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>");
                            index++;

                        });
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                               "alert(\"" + responseAdmins.Message + "\");", true);
                }


            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            try
            {
                string idString = String.Join(",", ids);
                Response responseUList = IUserService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUList.Data;
                    if (action == "Deactivate")
                    {
                        users.ForEach(x =>
                        {
                            x.Status = 0;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = (x.Username) + "(" + (x.UserRoleId == 3 ? "Main" : x.UserRoleId == 4 ? "Account" : (x.UserRoleId == 5 ? "Content" : "")) + ")" + " Admin deactivation Successful",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                    if (action == "Activate")
                    {
                        users.ForEach(x =>
                        {
                            x.Status = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = (x.Username) + "(" + (x.UserRoleId == 3 ? "Main" : x.UserRoleId == 4 ? "Account" : (x.UserRoleId == 5 ? "Content" : "")) + ")" + " Admin activation Successful",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here



                        });
                        IUserService.UpdateBulkData(users);
                    }
                    if (action == "ResendLink")
                    {
                        users.ForEach(x =>
                        {
                            var unique_code = CustomGenerator.GenerateSixDigitPin();
                            x.EmailCode = unique_code;
                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                            Email email = new Email
                            {
                                user = x,
                                link = siteURL + "Admin/SetAdminPassword.aspx",
                            };
                            EmailService.SendVerificationLink(email, unique_code);
                        });
                        IUserService.UpdateBulkData(users);
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("User accounts action: " + ex.Message);
                return false;
            }
        }

        public static Response IsValidPhoneNumber(string phoneNumber)
        {
            Response response = new Response();
            try
            {
                //will match +61 or +66- or 0 or nothing followed by a nine digit number
                bool isValid = Regex.Match(phoneNumber,
                    @"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$").Success;
                //to vary this, replace 61 with an international code of your choice 
                //or remove [\+]?61[-]? if international code isn't needed
                //{8} is the number of digits in the actual phone number less one
                if (isValid)
                {
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid mobile number";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(User obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            if (obj.LoginAttempts == 0)
            {
                response.IsSuccess = false;
                response.Message = "Select the Type";
                return response;
            }
            if (obj.Username == "")
            {
                response.IsSuccess = false;
                response.Message = "Username cannot be empty";
                return response;
            }

            if (obj.EmailId == "")
            {
                response.IsSuccess = false;
                response.Message = "Email cannot be empty";
                return response;
            }
            else
            {
                if (!CustomValidator.IsValidEmail(obj.EmailId))
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid email";
                    return response;
                }
            }
            if (obj.MobileNumber == "")
            {
                response.IsSuccess = false;
                response.Message = "Mobile number cannot be empty";
                return response;
            }
            else
            {
                response = IsValidPhoneNumber(obj.MobileNumber);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }
            Int32 LoginUserId = 0;
            if (HttpContext.Current.Session["admin"] != null)
            {
                User sessionUser = (User)HttpContext.Current.Session["admin"];
                LoginUserId = sessionUser.Id;
            }
            try
            {
                User user = new User();
                User userDB = new User();
                bool isEdit = false;
                if (obj.Id == 0)
                {
                    Response responseUM = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.Username), obj.Username, true, 0, 0, false);
                    if (responseUM.IsSuccess)
                    {
                        List<User> userMatches = (List<User>)responseUM.Data;
                        if (userMatches.Count > 0)
                        {
                            response.IsSuccess = false;
                            response.Message = "Username is taken";
                            return response;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseUM.Message;
                        return response;
                    }

                    Response responseEmail = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.EmailId), obj.EmailId, true, 0, 0, false);
                    if (responseEmail.IsSuccess)
                    {
                        List<User> userMatches = (List<User>)responseEmail.Data;
                        if (userMatches.Count > 0)
                        {
                            response.IsSuccess = false;
                            response.Message = "Account with this email already exists.";
                            return response;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseEmail.Message;
                        return response;
                    }

                    obj.UniqueKey = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    obj.IdNo = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    obj.Password = "default123";
                    obj.TransPwd = "default123";
                    obj.CreatedBy = LoginUserId;
                    obj.CreatedDate = DateTime.Now;
                    int userTypeId = obj.LoginAttempts;
                    obj.LoginAttempts = 0;
                    user = obj;

                    Response responsePost = IUserService.PostDataAdmin(obj);
                    if (responsePost.IsSuccess)
                    {
                        user = (User)responsePost.Data;
                        if (user.Id != 0)
                        {
                            UserType userType = new UserType
                            {
                                UserTypeId = userTypeId,
                                UserId = user.Id,
                                IsVerified = 0,
                                Status = 1
                            };
                            IUserTypeService.PostData(userType);
                        }
                        response.IsSuccess = true;
                        response.Message = "Success";
                    }
                    else
                    {
                        return responsePost;
                    }
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = (obj.LoginAttempts == 3 ? "Main" : obj.LoginAttempts == 4 ? "Account" : (obj.LoginAttempts == 5 ? "Content" : "")) + " Admin Added - " + user.Username,
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                else
                {
                    isEdit = true;
                    Response responseGet = IUserService.GetSingle(obj.Id);
                    if (responseGet.IsSuccess)
                    {
                        user = (User)responseGet.Data;
                        foreach (var propertyInfo in userDB.GetType().GetProperties())
                        {
                            propertyInfo.SetValue(userDB, propertyInfo.GetValue(user, null), null);
                        }
                        if (obj.Username == user.Username && obj.Id == user.Id && obj.EmailId == user.EmailId)
                        {
                            obj.CreatedBy = user.CreatedBy;
                            obj.CreatedDate = user.CreatedDate;
                            obj.LastLoginDate = user.LastLoginDate;
                            obj.ModifiedDate = DateTime.Now;
                            obj.ModifiedBy = LoginUserId;
                            IUserService.UpdateData(obj);

                            StringBuilder filter = new StringBuilder();
                            filter.Append(" 1=1 and user_id = " + obj.Id);
                            Response responseGet1 = IUserTypeService.GetDataByFilter(filter.ToString(), 0, 0, false);
                            if (responseGet1.IsSuccess)
                            {
                                UserType userType = ((List<UserType>)responseGet1.Data).FirstOrDefault();
                                UserType UserTypeDB = new UserType();
                                foreach (var propertyInfo in userType.GetType().GetProperties())
                                {
                                    propertyInfo.SetValue(UserTypeDB, propertyInfo.GetValue(userType, null), null);
                                }
                                if (userType.UserTypeId != obj.LoginAttempts)
                                {
                                    userType.UserTypeId = obj.LoginAttempts;
                                    IUserTypeService.UpdateData(userType);
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        TableName = "user_types",
                                        Description = obj.Username + " type change successful",
                                        UserId = loginUser.Id,
                                        UpdatedDate = DateTime.Now,
                                    };
                                    Response responseALM = IAdminLogMainService.PostData(alm);
                                    alm = (AdminLogMain)responseALM.Data;



                                    int propertyCount = typeof(UserType).GetProperties().Length;

                                    CompareLogic basicComparison = new CompareLogic()
                                    { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                    List<Difference> diffs = basicComparison.Compare(UserTypeDB, userType).Differences;
                                    int noOfDifferent = diffs.Count();

                                    foreach (Difference diff in diffs)
                                    {
                                        string columnName = Converter.GetColumnNameByPropertyName<UserType>(diff.PropertyName);
                                        if (diff.PropertyName != nameof(DiOTP.Utility.User.LoginAttempts))
                                        {
                                            AdminLogSub x = new AdminLogSub()
                                            {
                                                AdminLogMainId = alm.Id,
                                                ColumnName = columnName,
                                                ValueOld = diff.Object1Value,
                                                ValueNew = diff.Object2Value,
                                            };
                                            Response response1 = IAdminLogSubService.PostData(x);
                                            x = (AdminLogSub)response1.Data;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                return responseGet1;
                            }
                            response.IsSuccess = true;
                            response.Message = "Success";

                            if (isEdit && response.IsSuccess)
                            {
                                try
                                {
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        TableName = "users",
                                        Description = obj.Username + " info change successful",
                                        UserId = loginUser.Id,
                                        UpdatedDate = DateTime.Now,
                                    };

                                    Response responseALM = IAdminLogMainService.PostData(alm);
                                    alm = (AdminLogMain)responseALM.Data;


                                    int propertyCount = typeof(User).GetProperties().Length;

                                    CompareLogic basicComparison = new CompareLogic()
                                    { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                    List<Difference> diffs = basicComparison.Compare(userDB, obj).Differences;
                                    int noOfDifferent = diffs.Count();

                                    foreach (Difference diff in diffs)
                                    {
                                        string columnName = Converter.GetColumnNameByPropertyName<User>(diff.PropertyName);
                                        if (diff.PropertyName != nameof(DiOTP.Utility.User.LoginAttempts) && diff.Object2Value != "(null)")
                                        {
                                            AdminLogSub x = new AdminLogSub()
                                            {
                                                AdminLogMainId = alm.Id,
                                                ColumnName = columnName,
                                                ValueOld = diff.Object1Value,
                                                ValueNew = diff.Object2Value,
                                            };
                                            Response response1 = IAdminLogSubService.PostData(x);
                                            x = (AdminLogSub)response1.Data;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Success. Audit log failed! " + ex.Message;
                                    return response;
                                }
                            }

                        }
                        else
                        {
                            if (obj.Username != user.Username)
                            {
                                Response responseUM = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.Username), obj.Username, true, 0, 0, false);
                                if (responseUM.IsSuccess)
                                {
                                    List<User> userMatches = (List<User>)responseUM.Data;
                                    if (userMatches.Count > 0)
                                    {
                                        response.IsSuccess = false;
                                        response.Message = "Username is taken";
                                        return response;
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = responseUM.Message;
                                    return response;
                                }
                            }
                            if (obj.EmailId != user.EmailId)
                            {
                                Response responseEmail = IUserService.GetDataByPropertyName(nameof(DiOTP.Utility.User.EmailId), obj.EmailId, true, 0, 0, false);
                                if (responseEmail.IsSuccess)
                                {
                                    List<User> userMatchesEmail = (List<User>)responseEmail.Data;
                                    if (userMatchesEmail.Count > 0)
                                    {
                                        response.IsSuccess = false;
                                        response.Message = "Account with this email already exists.";
                                        return response;
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = responseEmail.Message;
                                    return response;
                                }
                            }


                            obj.CreatedBy = user.CreatedBy;
                            obj.CreatedDate = user.CreatedDate;
                            obj.LastLoginDate = user.LastLoginDate;
                            obj.ModifiedDate = DateTime.Now;
                            obj.ModifiedBy = LoginUserId;

                            IUserService.UpdateData(obj);

                            StringBuilder filter = new StringBuilder();
                            filter.Append(" 1=1 and user_id = " + obj.Id);
                            Response responseGet1 = IUserTypeService.GetDataByFilter(filter.ToString(), 0, 0, false);
                            if (responseGet1.IsSuccess)
                            {
                                UserType userType = ((List<UserType>)responseGet1.Data).FirstOrDefault();
                                if (userType.UserTypeId != obj.LoginAttempts)
                                {
                                    UserType UserTypeDB = new UserType();
                                    foreach (var propertyInfo in userType.GetType().GetProperties())
                                    {
                                        propertyInfo.SetValue(UserTypeDB, propertyInfo.GetValue(userType, null), null);
                                    }

                                    userType.UserTypeId = obj.LoginAttempts;
                                    IUserTypeService.UpdateData(userType);
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        TableName = "user_types",
                                        Description = obj.Username + " type change successful",
                                        UserId = loginUser.Id,
                                        UpdatedDate = DateTime.Now,
                                    };
                                    Response responseALM = IAdminLogMainService.PostData(alm);
                                    alm = (AdminLogMain)responseALM.Data;



                                    int propertyCount = typeof(UserType).GetProperties().Length;

                                    CompareLogic basicComparison = new CompareLogic()
                                    { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                    List<Difference> diffs = basicComparison.Compare(UserTypeDB, userType).Differences;
                                    int noOfDifferent = diffs.Count();

                                    foreach (Difference diff in diffs)
                                    {
                                        string columnName = Converter.GetColumnNameByPropertyName<UserType>(diff.PropertyName);
                                        if (diff.PropertyName != nameof(DiOTP.Utility.User.LoginAttempts))
                                        {
                                            AdminLogSub x = new AdminLogSub()
                                            {
                                                AdminLogMainId = alm.Id,
                                                ColumnName = columnName,
                                                ValueOld = diff.Object1Value,
                                                ValueNew = diff.Object2Value,
                                            };
                                            Response response1 = IAdminLogSubService.PostData(x);
                                            x = (AdminLogSub)response1.Data;
                                        }
                                    }

                                }

                            }
                            else
                            {
                                return responseGet1;
                            }
                            response.IsSuccess = true;
                            response.Message = "Success";


                            if (isEdit && response.IsSuccess)
                            {
                                try
                                {
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        TableName = "users",
                                        Description = obj.Username + " info change successful",
                                        UserId = loginUser.Id,
                                        UpdatedDate = DateTime.Now,
                                    };

                                    Response responseALM = IAdminLogMainService.PostData(alm);
                                    alm = (AdminLogMain)responseALM.Data;


                                    int propertyCount = typeof(User).GetProperties().Length;

                                    CompareLogic basicComparison = new CompareLogic()
                                    { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                    List<Difference> diffs = basicComparison.Compare(userDB, obj).Differences;
                                    int noOfDifferent = diffs.Count();

                                    foreach (Difference diff in diffs)
                                    {
                                        string columnName = Converter.GetColumnNameByPropertyName<User>(diff.PropertyName);
                                        if (diff.PropertyName != nameof(DiOTP.Utility.User.LoginAttempts) && diff.Object2Value != "(null)")
                                        {
                                            AdminLogSub x = new AdminLogSub()
                                            {
                                                AdminLogMainId = alm.Id,
                                                ColumnName = columnName,
                                                ValueOld = diff.Object1Value,
                                                ValueNew = diff.Object2Value,
                                            };
                                            Response response1 = IAdminLogSubService.PostData(x);
                                            x = (AdminLogSub)response1.Data;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Success. Audit log failed! " + ex.Message;
                                    return response;
                                }
                            }

                        }
                    }
                    else
                    {
                        return responseGet;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add admin action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = string.Empty;
            try
            {


                string tempPath = Path.GetTempPath() + "Admins_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Admins");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No.", "Status", "Username", "User type", "Email", "Mobile No", "Created date", "Last login date" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Admins"];

                string docDetails = "Admins";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    StringBuilder jfilter = new StringBuilder();


                    filterQuery.Append(@"SELECT u.id,u.username,t.user_type_id,d.name as user_type,u.mobile_number,u.email_id,u.last_login_date,u.created_date,u.status from users u
                            join user_types t on u.id = t.user_id
                            join user_types_def d on t.user_type_id = d.id where 1=1 ");
                    
                    if (!string.IsNullOrEmpty(filter) && filter != "0")
                    {
                        docDetails += " | Type: " + (filter == "3" ? "Main Admin" : (filter == "4" ? "Account Admin" : (filter == "5" ? "Content Admin" : "-"))) + " ";
                        filterQuery.Append(" and t.user_type_id in ('" + filter + "')");
                    }
                    else
                    {
                        docDetails += " | Type: All (Main Admin, Account Admin, Content Admin)";
                        filterQuery.Append(@" and t.user_type_id in ('3','4','5') ");
                    }
                    int skip = 0, take = 20;
                    filterQuery.Append(@"order by u.status desc, u.created_date desc");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseAdmins = GenericService.GetDataByQuery(filterQuery.ToString(), skip, take, false, null, false, null, true);
                    if (responseAdmins.IsSuccess)
                    {
                        var AdminsDyn = responseAdmins.Data;
                        var responseJSON = JsonConvert.SerializeObject(AdminsDyn);
                        List<DiOTP.Utility.Admins> Admins = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Admins>>(responseJSON);

                        StringBuilder asb = new StringBuilder();


                        int index = 1;

                        Admins.ForEach(x =>
                        {
                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = (x.status == 1 ? "Active" : "Inactive");
                            worksheet.Cells[row, 3].Value = x.username;
                            worksheet.Cells[row, 4].Value = x.user_type;
                            worksheet.Cells[row, 5].Value = x.email_id;
                            worksheet.Cells[row, 6].Value = x.mobile_number;
                            worksheet.Cells[row, 7].Value = (x.created_date.ToString("dd/MM/yyyy HH:mm:ss"));
                            worksheet.Cells[row, 8].Value = (x.last_login_date != null && x.last_login_date.ToString("yyyy") != "0001" ? x.last_login_date.ToString("dd/MM/yyyy HH:mm:ss") : "-");
                            row++;
                            index++;
                        });

                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Admin List downloaded",
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Admin_list" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

    }
}