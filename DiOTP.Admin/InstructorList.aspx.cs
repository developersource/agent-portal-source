﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace Admin
{
    public partial class InstructorList : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoginRole loginRole = ServicesManager.GetRole(Context);

            StringBuilder filter = new StringBuilder();
            string mainQ = (@"select *
                                from ");
            string mainQCount = (@"SELECT count(*) from ");
            filter.Append(@" instructors
                                where status=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];

                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (name like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or email_id like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mobile_number like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and is_active in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(@" and is_active in (0, 1) ");
                }

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by created_date desc");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseList = GenericService.GetDataByQuery(mainQ + filter.ToString(), 0, 0, false, null, false, null, false);
                if (responseList.IsSuccess)
                {
                    var UsersDyn = responseList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.Instructor> instructors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Instructor>>(responseJSON);
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    instructors.ForEach(x =>
                    {
                        string checkBox = string.Empty;
                        checkBox = @"<div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + x.Id + @"'" + @" /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>";
                        asb.Append(@"<tr>
                                    <td class='icheck'>
                                        " + checkBox + @"
                                        <span class='row-status'>" + (x.IsActive == 1 ? "<span class='label label-success'>Active</span>" : (x.IsActive == 0 ? "<span class='label label-danger'>Inactive</span>" : "")) + @"</span>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td><strong><a href='javascript:;' data-id='" + x.Id + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='AgentRequestsDetails.aspx' data-title='View details' data-action='View'>   " + x.Name + @"&nbsp;<i class='fa fa-eye'></i></a></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><strong>" + x.EmailId + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Mobile number</td>
                                                    <td><strong>" + x.MobileNumber + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table class='table'>
                                            <tbody>
                                                <tr>
                                                    <td>Created on</td>
                                                    <td><strong>" + x.CreatedDate.ToString("dd/MM/yyyy") + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Updated on</td>
                                                    <td><strong>" + (x.UpdatedDate != default(DateTime) ? x.UpdatedDate.ToString("dd/MM/yyyy") : "-") + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>");
                        index++;
                    });
                    usersTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                               "alert(\"" + responseList.Message + "\");", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, string rejectReason = "")
        {
            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                User loginUser = (User)HttpContext.Current.Session["admin"];
                if (loginUser == null)
                {
                    response1.IsSuccess = false;
                    response1.Message = "Session Expired!";
                }
                else
                {
                    LoginRole loginRole = ServicesManager.GetRole(HttpContext.Current);
                    string idString = String.Join(",", ids);
                    string queryAReg = (@" SELECT * FROM instructors where id in (" + idString + ") ");
                    Response responseIList = GenericService.GetDataByQuery(queryAReg, 0, 0, false, null, false, null, false);
                    if (responseIList.IsSuccess)
                    {
                        var instructorsDyn = responseIList.Data;
                        var responseJSON = JsonConvert.SerializeObject(instructorsDyn);
                        List<DiOTP.Utility.Instructor> instructors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Instructor>>(responseJSON);

                        if (action == "Activate")
                        {
                            instructors.ForEach(x =>
                            {
                                if (x.IsActive == 0)
                                {
                                    x.IsActive = 1;
                                    Response resUpdate = GenericService.UpdateData<Instructor>(x);
                                    if (resUpdate.IsSuccess)
                                    {
                                        response1.IsSuccess = true;
                                        responseMsgs.Add("Instructor " + x.Name + " activated.");
                                    }
                                    else
                                    {
                                        response1.IsSuccess = false;
                                        responseMsgs.Add(resUpdate.Message);
                                    }
                                }
                                else
                                {
                                    response1.IsSuccess = false;
                                    responseMsgs.Add("Instructor " + x.Name + " already active");
                                }
                            });
                        }
                        if (action == "Deactivate")
                        {
                            instructors.ForEach(x =>
                            {
                                if (x.IsActive == 1)
                                {
                                    x.IsActive = 0;
                                    Response resUpdate = GenericService.UpdateData<Instructor>(x);
                                    if (resUpdate.IsSuccess)
                                    {
                                        response1.IsSuccess = true;
                                        responseMsgs.Add("Instructor " + x.Name + " deactivated.");
                                    }
                                    else
                                    {
                                        response1.IsSuccess = false;
                                        responseMsgs.Add(resUpdate.Message);
                                    }
                                }
                                else
                                {
                                    response1.IsSuccess = false;
                                    responseMsgs.Add("Instructor " + x.Name + " already inactive");
                                }
                            });
                        }
                    }
                    else
                    {
                        response1.IsSuccess = false;
                        responseMsgs.Add(responseIList.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add("Action: " + ex.Message + " - Exception");
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        public static Response IsValidPhoneNumber(string phoneNumber)
        {
            Response response = new Response();
            try
            {
                //will match +61 or +66- or 0 or nothing followed by a nine digit number
                bool isValid = Regex.Match(phoneNumber,
                    @"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$").Success;
                //to vary this, replace 61 with an international code of your choice 
                //or remove [\+]?61[-]? if international code isn't needed
                //{8} is the number of digits in the actual phone number less one
                if (isValid)
                {
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid mobile number";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(InstructorCustom obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            if (obj.Name == "")
            {
                response.IsSuccess = false;
                response.Message = "Username cannot be empty";
                return response;
            }
            if (obj.EmailId == "")
            {
                response.IsSuccess = false;
                response.Message = "Email cannot be empty";
                return response;
            }
            else
            {
                if (!CustomValidator.IsValidEmail(obj.EmailId))
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid email";
                    return response;
                }
            }
            if (obj.MobileNumber == "")
            {
                response.IsSuccess = false;
                response.Message = "Mobile number cannot be empty";
                return response;
            }
            else
            {
                response = IsValidPhoneNumber(obj.MobileNumber);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }
            Int32 LoginUserId = 0;
            if (HttpContext.Current.Session["admin"] != null)
            {
                User sessionUser = (User)HttpContext.Current.Session["admin"];
                LoginUserId = sessionUser.Id;
            }
            try
            {
                if (obj.Id == 0)
                {
                    Instructor instructor = new Instructor
                    {
                        Name = obj.Name,
                        EmailId = obj.EmailId,
                        MobileNumber = obj.MobileNumber,
                        IsActive = 0,
                        CreatedBy = LoginUserId,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = 0,
                        UpdatedDate = default(DateTime),
                        Status = 1
                    };
                    Response resUpdate = GenericService.PostData<Instructor>(instructor);
                    if (!resUpdate.IsSuccess)
                    {
                        response = resUpdate;
                    }
                }
                else
                {
                    string queryAReg = (@" SELECT * FROM instructors where id='" + obj.Id + "' ");
                    Response responseIList = GenericService.GetDataByQuery(queryAReg, 0, 0, false, null, false, null, false);
                    if (responseIList.IsSuccess)
                    {
                        var instructorsDyn = responseIList.Data;
                        var responseJSON = JsonConvert.SerializeObject(instructorsDyn);
                        List<DiOTP.Utility.Instructor> instructors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Instructor>>(responseJSON);
                        if (instructors.Count == 1)
                        {
                            Instructor instructor = instructors.FirstOrDefault();
                            instructor.Name = obj.Name;
                            instructor.EmailId = obj.EmailId;
                            instructor.MobileNumber = obj.MobileNumber;
                            instructor.UpdatedBy = LoginUserId;
                            instructor.UpdatedDate = DateTime.Now;
                            Response resUpdate = GenericService.UpdateData<Instructor>(instructor);
                            if (!resUpdate.IsSuccess)
                            {
                                response = resUpdate;
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Instructor not found";
                        }
                    }
                    else
                    {
                        response = responseIList;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}