using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_order_files")]
    public class UserOrderFile
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("order_file_type_id"), Display("OrderFileTypeId")]
        public Int32 OrderFileTypeId { get; set; }
        [Database("order_no"), Display("OrderNo")]
        public String OrderNo { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("url"), Display("Url")]
        public String Url { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public User UpdatedByUser { get; set; }
        public User CreatedByUser { get; set; }
        public UserAccount UserAccountIdUserAccount { get; set; }
        public User UserIdUser { get; set; }
        public UserOrder UserOrderIdUserOrder { get; set; }
    }
}
