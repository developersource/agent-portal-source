﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class TrainingSession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String type = Request.QueryString["type"];
                BindCourses(type);
            }
            else {
                BindCourses(Request.QueryString["type"]);
            }
        }

        public void BindCourses(string type)
        {
            User user = (User)(Session["user"]);
            if (user != null)
            {
                learningTitle.Text = type == "0" ? "Classroom Training" : "Online Training";
                if (type == "0" && !IsPostBack) {
                    ddlLocation.Items.Clear();
                    ddlLocation.Items.Insert(0, new ListItem("Select", "Select"));
                    ddlLocation.Items.Insert(1, new ListItem("Petaling Jaya", "Petaling Jaya"));
                    ddlLocation.Items.Insert(2, new ListItem("Cheras", "Cheras"));
                    ddlLocation.Items.Insert(3, new ListItem("Kuala Lumpur", "Kuala Lumpur"));
                    ddlLocation.Items.Insert(4, new ListItem("Puchong", "Puchong"));
                }
                if (type == "1" && !IsPostBack) {
                    ddlLocation.Items.Clear();
                    ddlLocation.Items.Insert(0, new ListItem("Select", "Select"));
                    ddlLocation.Items.Insert(1, new ListItem("Zoom", "Zoom"));
                }
                
                
                // Online or Offline content
                StringBuilder filter = new StringBuilder();
                string mainQ = (@"SELECT t.*, i.name as instructor_name ");
                filter.Append(@" FROM agent_training_topics t
                            left join instructors i on i.id = t.instructor_id
                             where 1=1 ");
                //if (Session["user"] == null)
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
                //else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
                //else
                //{
                filter.Append("and t.is_online = " + type + " and t.is_active=1 ");

                // > -1 = selected, index start from 0
                if (ddlLocation.SelectedIndex > 0) {
                    filter.Append("and t.venue in ('"+ ddlLocation.Value.ToString() +"')");
                }
                if (ddlDate.SelectedIndex > 0) {
                    filter.Append("and MONTH(t.enroll_end_date) = '" + ddlDate.Value.ToString() + "' ");
                }

                filter.Append("order by t.id desc");
                int skip = 0, take = 20;
                Response responseSCList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseSCList.IsSuccess)
                {
                    var CourseDyn = responseSCList.Data;
                    var responseJSON = JsonConvert.SerializeObject(CourseDyn);
                    List<DiOTP.Utility.CourseListingView> courselisting = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CourseListingView>>(responseJSON);

                    StringBuilder filterE = new StringBuilder();
                    string mainQE = (@"SELECT * ");
                    filterE.Append(@" FROM agent_training_enrollments
                             where 1=1 ");
                    filterE.Append("and is_active=1 and user_id='" + user.Id + "' ");
                    filterE.Append("order by id desc");

                    List<AgentTrainingEnrollment> agentTrainingEnrollments = new List<AgentTrainingEnrollment>();

                    Response responseEList = GenericService.GetDataByQuery(mainQE + filterE.ToString(), 0, 0, false, null, false, null, false);
                    if (responseEList.IsSuccess)
                    {
                        var CourseEDyn = responseEList.Data;
                        var responseEJSON = JsonConvert.SerializeObject(CourseEDyn);
                        agentTrainingEnrollments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTrainingEnrollment>>(responseEJSON);



                        StringBuilder bsb = new StringBuilder();
                        StringBuilder asb = new StringBuilder();
                        int index = 1;
                        foreach (DiOTP.Utility.CourseListingView c in courselisting)
                        {
                            string mainQECount = (@"SELECT count(*) FROM agent_training_enrollments
                             where agent_training_topic_id = " + c.Id);
                            Response responseECount = GenericService.GetCountByQuery(mainQECount);
                            int registeredCount = Convert.ToInt32(responseECount.Data);
                            String EnrollString = "";
                            String CancelString = "";
                            Boolean isEnrolled = false;

                            //Exclude if already exceed end-date
                            if (DateTime.Compare(c.EnrollEndDate, DateTime.Now) > 0)
                            {
                                //Move Code here for allowing courses thath has dates that are later than today's date to be displayed.

                                if (agentTrainingEnrollments.FirstOrDefault(x => x.AgentTrainingTopicId == c.Id) != null)
                                {
                                    //Enrolled
                                    EnrollString = "<a href='javascript:;' class='btn btn-success btn-disabled' style='padding: 10px 10px 10px 10px; width: 100%;text-align:center;'>Enrolled</a>";
                                    CancelString = "<a href='javascript:;' class='btn btn-warning btn-disabled btnWithdrawNow' data-id='"+ c.Id+ "' style='padding: 10px 10px 10px 10px; width: 100%;text-align:center;'>Withdraw Enrollment</a>";
                                    isEnrolled = true;
                                }
                                else
                                {
                                    //Not Enrolled
                                    EnrollString = "<a href='javascript:;' class='btn btn-infos1 btnEnrollNow' data-id='" + c.Id + @"' style='padding: 10px 10px 10px 10px; width: 100%;'>Enroll now</a>";
                                    isEnrolled = false;
                                }

                                bsb.Append(@"<div class='col-md-3 mb-20'>
                                                <div class='iconbox'>
                                                    <div class='box-header'>
                                                        <div class='box-icon'>
                                                            <i class='fa " + (isEnrolled ? "fa-check-square-o" : "fa-square-o") + @"'></i>
                                                        </div>
                                                        <div class='box-title'>
                                                            <h5>" + c.Topic + @"</h5>
                                                            <h5>Training Date: " + c.StartDate.ToString("dd/MM/yyyy") + @"</h5>
                                                            <h5>Venue: " + c.Venue + @"</h5>
                                                        </div>
                                                    </div>
                                                    <div class='box-content'>
                                                        <div class='synopsis'>
                                                            " + (c.Synopsis.Length > 130 ? c.Synopsis.Substring(0, 130) + "..." : c.Synopsis) + @"
                                                        </div>
                                                        <p class='box-readmore'>
                                                            <a href='#' class='' data-toggle='modal' data-target='#detailsPanel" + index + @"'>Learn more</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>");


                                //bsb.Append((index % 3 == 0 ? spacing : "") + @"<div class='col-md-4'>
                                //            <div class='iconbox' style='height: 200px; padding: 40px 40px 30px 40px; transition: all 0.3s ease-in-out; background: #f3f3f3;'>
                                //                <a href='#' class='' data-toggle='modal' data-target='#detailsPanel" + index + @"' style='color: black;'>
                                //                    <div style='text-align: center;'>
                                //                        <img src='Content/MyImage/9.png' style='height: 120px; width: 120px;' />
                                //                    </div>
                                //                    <div style = 'text-align: center; font-size: 20px;'>
                                //                        " + c.Topic + @"
                                //                    </div >
                                //                </a>
                                //            </div>
                                //        </div>" +
                                //                (index % 3 == 0 ? spacingEnd : "")
                                //                );
                                String[] licenseType = c.LicenseType.Split(',');
                                asb.Append(
                        @"<div class='modal fade' id='detailsPanel" + index + @"' tabindex='-" + index + @"' role='dialog' aria-labelledby='myModalLabel'>
	                        <div id='' runat='server' class='modal-dialog sett two'>
		                        <div class='modal-content'>
			                        <div class='modal-header sett-modalhead'>
				                        <h4>" + c.Topic + @"</h4>
			                        </div>
			                        <div class='modal-body intro-section'>
				                        <div class='row clearfix'>
					                        <!-- Content Column -->
					                        <div class='col-lg-8 md-mb-50'>
						                        <!-- Intro Info Tabs-->
						                        <div class='intro-info-tabs' style='background: #ffffff;box-shadow: 0 0 30px #eee;'>
							                        <ul class='nav nav-tabs intro-tabs tabs-box' id='myTab-" + index + @"' role='tablist'>
								                        <li class='nav-item tab-btns active'>
									                        <a class='nav-link tab-btn' id='prod-overview-tab-" + index + @"' data-toggle='tab' href='#prod-overview-" + index + @"' role='tab' aria-controls='prod-overview-" + index + @"' aria-selected='true'>Details</a>
								                        </li>
								                        <!--<li class='nav-item tab-btns'>
									                        <a class='nav-link tab-btn' id='prod-instructor-tab-" + index + @"' data-toggle='tab' href='#prod-instructor-" + index + @"' role='tab' aria-controls='prod-instructor-" + index + @"' aria-selected='false'>Instructor</a>
								                        </li>-->
							                        </ul>
							                        <div class='tab-content tabs-content' id='myTabContent'>
								                        <div class='tab-pane tab fade active in' id='prod-overview-" + index + @"' role='tabpanel' aria-labelledby='prod-overview-tab-" + index + @"'>
									                        <div class='content white-bg pt-30'>
										                        <!-- Cource Overview -->
										                        <div class='course-overview'>
											                        <div class='inner-box'>
												                        <h4>Synopsis</h4>
												                        <p>" + (String.IsNullOrEmpty(c.Synopsis) ? "-N/A-" : c.Synopsis) + @"</p>
												                        <h4>Learning Outcome</h4>
												                        <p>" + (String.IsNullOrEmpty(c.Outcome) ? "-N/A-" : c.Outcome) + @"</p>
												                        <h4>Special Notes</h4>
												                        <p>" + (String.IsNullOrEmpty(c.SpecialNote) ? "-N/A-" : c.SpecialNote) + @"</p>
                                                                        
											                        ");
                                                                //online
                                                                if (type == "1" && isEnrolled) {
                                                                    asb.Append(@"<h4>Downloadable Material</h4>
												                                 <p><a href='" + (String.IsNullOrEmpty(c.CourseFileUrl) ? "" : c.CourseFileUrl) + "' target='_blank'> Course Material </a>" + @"</p>");
                                                                }

                                                 asb.Append(@"</div>
                                                                </div>

                                                            </div>

                                                        </div>

                                                        <!-- <div class='tab-pane fade' id='prod-instructor-" + index + @"' role='tabpanel' aria-labelledby='prod-instructor-tab-" + index + @"'>
									                        <div class='content pt-30 pb-30 pl-30 pr-30 white-bg'>
										                        <h3 class='instructor-title'>Instructors</h3>
										                        <div class='row rs-team style1 orange-color transparent-bg clearfix'>
											                        <div class='col-lg-6 col-md-6 col-sm-12 sm-mb-30'>
												                        <div class='team-item'>
													                        <div class='content-part'>
														                        <h4 class='name'><a href = '#' > Jhon Pedrocas</a></h4>
														                        <span class='designation'>Professor</span>
													                        </div>
												                        </div>
											                        </div>
											                        <div class='col-lg-6 col-md-6 col-sm-12'>
												                        <div class='team-item'>
													                        <div class='content-part'>
														                        <h4 class='name'><a href = '#' > Jhon Pedrocas</a></h4>
														                        <span class='designation'>Professor</span>
														                        <ul class='social-links'>
															                        <li><a href = '#' >< i class='fa fa-facebook'></i></a></li>
															                        <li><a href = '#' >< i class='fa fa-twitter'></i></a></li>
															                        <li><a href = '#' >< i class='fa fa-linkedin'></i></a></li>
															                        <li><a href = '#' >< i class='fa fa-google-plus'></i></a></li>
														                        </ul>
													                        </div>
												                        </div>
											                        </div>
										                        </div>
									                        </div>
								                        </div> -->
							                        </div>
						                        </div>
					                        </div>

					                        <!-- Video Column -->
					                        <div class='video-column col-lg-4'>
						                        <div class='inner-column'>
							                        <div class='course-features-info'>
								                        <ul style = 'padding-left: 0;' >

                                                            <li class='license-feature'>
										                        <i class='fa fa-id-card'></i>
										                        <span class='label'>License</span>
										                        <span class='value'>" + c.LicenseType + @"</span>
									                        </li>
									                        <li class='attendies-feature'>
										                        <i class='fa fa-users'></i>
										                        <span class='label'>Attendies</span>
										                        <span class='value'>" + c.Attendies + @"</span>
									                        </li>
									                        <li class='close-date-feature'>
										                        <i class='fa fa-calendar'></i>
										                        <span class='label'>Closing Date</span>
										                        <span class='value'>" + c.EnrollEndDate.ToString("dd/MM/yyyy") + @"</span>
									                        </li>
									                        <li class='lectures-feature'>
										                        <i class='fa fa-briefcase'></i>
										                        <span class='label'>Instructor</span>
										                        <span class='value'>" + c.InstructorName + @"</span>
									                        </li>
									                        <li class='students-feature'>
										                        <i class='fa fa-dot-circle-o'></i>
										                        <span class='label'>Capacity</span>
										                        <span class='value'>" + registeredCount + "/" + c.Capacity + @"</span>
									                        </li>
									                        <li class='duration-feature'>
										                        <i class='fa fa-clock-o'></i>
										                        <span class='label'>Duration</span>
										                        <span class='value'>" + c.Duration + @" <span id = 'durtionNote' style='color: #bcbcbc;'>" + (c.DurationNote != "" ? "(" + c.DurationNote + ")" : "") + @"</span></span>
									                        </li>
									                        <li class='quizzes-feature'>
										                        <i class='fa fa-puzzle-piece'></i>
										                        <span class='label'>Assessment</span>
										                        <span class='value'>" + (c.IsAssessment == 0 ? "Not Required" : "Required") + @"</span>
									                        </li>
									                        <li class='assessments-feature'>
										                        <i class='fa fa-globe'></i>
										                        <span class='label'>" + (c.IsOnline == 0 ? "Venue" : "App") + @"</span>
										                        <span class='value'>" + c.Venue + @"</span>
									                        </li>
									                        " +
                                                            (c.IsOnline == 1 && isEnrolled?
                                                            @"<li class='join-feature'>
										                        <i class='fa fa-globe'></i>
										                        <span class='label'>Join link</span>
										                        <span class='value'><a href='" + c.JoinUrl + @"' target='_blank'>Click here</a></span>
									                        </li>" :
                                                            "") + @"
								                        </ul>
							                        </div>

							                        <div class='btn-part'>
								                        <a href='#' class='btn btn-default'>" + c.CpdPoints + @" (CPD Points)</a>
								                        " + EnrollString + CancelString + @"
							                        </div>
						                        </div>
					                        </div>
				                        </div>
			                        </div>
			                        <div class='modal-footer'>
				                        <button type='button' class='btn btn-infos1' data-dismiss='modal'>Close</button>
			                        </div>
		                        </div>
	                        </div>
                        </div>");
                                index++;

                            }
                            else {
                                //If no available courses upcoming

                            }
                        }

                        if (bsb.Length != 0 && asb.Length != 0)
                        {
                            selection.InnerHtml = bsb.ToString();
                            tabContent.InnerHtml = asb.ToString();
                        }
                        else {
                            selection.InnerHtml = "";
                            tabContent.InnerHtml = "";
                        }
                        
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseEList.Message + "\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseSCList.Message + "\", '');", true);
                }
            }
        }

        protected void btnEnroll_Click(object sender, EventArgs e)
        {
            User user = (User)(Session["user"]);
            String CourseId = hdnCourseId.Value;
            if (CourseId != "")
            {
                StringBuilder filter = new StringBuilder();
                string mainQ = (@"SELECT * ");
                filter.Append(@" FROM agent_training_topics
                             where 1=1 ");
                filter.Append("and is_active=1 and id='" + CourseId + "' ");
                filter.Append("order by id desc");

                int skip = 0, take = 20;
                Response responseSCList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);


                if (responseSCList.IsSuccess)
                {
                    var CourseDyn = responseSCList.Data;
                    var responseJSON = JsonConvert.SerializeObject(CourseDyn);
                    List<DiOTP.Utility.CourseListing> courselistings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CourseListing>>(responseJSON);
                    if (courselistings.Count == 1)
                    {
                        DiOTP.Utility.CourseListing courseListing = courselistings.FirstOrDefault();

                        StringBuilder filterE = new StringBuilder();
                        string mainQE = (@"SELECT * ");
                        filterE.Append(@" FROM agent_training_enrollments
                             where 1=1 ");
                        filterE.Append("and is_active=1 and agent_training_topic_id='" + CourseId + "' ");
                        filterE.Append("order by id desc");

                        Response responseEList = GenericService.GetDataByQuery(mainQE + filterE.ToString(), 0, 0, false, null, false, null, false);
                        if (responseEList.IsSuccess)
                        {
                            var CourseEDyn = responseEList.Data;
                            var responseEJSON = JsonConvert.SerializeObject(CourseEDyn);
                            List<AgentTrainingEnrollment> agentTrainingEnrollments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTrainingEnrollment>>(responseEJSON);
                            if (agentTrainingEnrollments.Count > courseListing.Capacity)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Class is full.\");", true);
                            }
                            else
                            {
                                AgentTrainingEnrollment agentTrainingEnrollment = new AgentTrainingEnrollment
                                {
                                    AgentTrainingTopicId = courseListing.Id,
                                    UserId = user.Id,
                                    IsActive = 1,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = user.Id,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedBy = user.Id,
                                    Status = 1
                                };
                                Response responsePost = GenericService.PostData<AgentTrainingEnrollment>(agentTrainingEnrollment);
                                if (responsePost.IsSuccess)
                                {
                                    hdnCourseId.Value = "";
                                    BindCourses(courseListing.IsOnline == 0 ? "1" : "2");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Enrolled successfully\", 'TrainingSession.aspx?type=" + courseListing.IsOnline + "');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responsePost.Message + "\", '');", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseEList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Course not found\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseSCList.Message + "\", '');", true);
                }

            }
        }

        protected void btnWithdraw_Click(object sender, EventArgs e)
        {
            User user = (User)(Session["user"]);
            String CourseId = hdnCourseId.Value;
            if (CourseId != "")
            {
                String type = Request.QueryString["type"];
                StringBuilder filter = new StringBuilder();

                string mainQ = (@"DELETE");
                filter.Append(@" FROM agent_training_enrollments
                             where 1=1 ");
                filter.Append("and user_id='"+ user.Id+"' and agent_training_topic_id='" + CourseId + "' ");

                Response responseSCList = GenericService.GetDataByQuery(mainQ + filter.ToString(), 0, 0, false, null, false, null, false);
                if (responseSCList.IsSuccess)
                {
                    //If successfully delete, use alert to notify that they have withdrawn from the enrollment.

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Enrollment Withdrawn Successfully\", 'TrainingSession.aspx?type=" + type  + "');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseSCList.Message + "\", '');", true);
                }

            }
        }
    }
}