﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddBanner : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazySiteContentServiceObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazySiteContentServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
            CreatedBy.Value = "0";
            UpdatedBy.Value = "0";
            CreatedDate.Value = currentDateString;
            UpdatedDate.Value = currentDateString;
            string idString = Request.QueryString["id"];
            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);
                Response response = ISiteContentService.GetSingle(id);
                if (response.IsSuccess)
                {
                    SiteContent siteContent = (SiteContent)response.Data;

                    Id.Value = siteContent.Id.ToString();
                    SiteContentTypeId.Value = siteContent.SiteContentTypeId.ToString();
                    Priority.Value = siteContent.Priority.ToString();
                    IsDisplay.Value = siteContent.IsDisplay.ToString();
                    IsContentDisplay.Value = siteContent.IsContentDisplay.ToString();
                    Title.Value = siteContent.Title;
                    TitleColor.Value = siteContent.TitleColor;
                    SubTitle.Value = siteContent.SubTitle;
                    ShortDisplayContent.Value = siteContent.ShortDisplayContent;
                    Content.Value = siteContent.Content;
                    ContentColor.Value = siteContent.ContentColor;

                    bannerImage.Src = siteContent.ImageUrl;
                    ImageUrl.Value = siteContent.ImageUrl;

                    PublishDate.Value = siteContent.PublishDate.ToString("MM/dd/yyyy");
                    Status.Value = siteContent.Status.ToString();
                    CreatedBy.Value = siteContent.CreatedBy.ToString();
                    CreatedDate.Value = siteContent.CreatedDate.ToString("MM/dd/yyyy");
                    UpdatedBy.Value = siteContent.UpdatedBy.ToString();
                    UpdatedDate.Value = siteContent.UpdatedDate == null ? DateTime.Now.ToString("MM/dd/yyyy") : siteContent.UpdatedDate.Value.ToString("MM/dd/yyyy");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}