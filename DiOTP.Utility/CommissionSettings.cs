﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomAttributes;

namespace DiOTP.Utility
{
    [Database("commission_settings")]
    public class CommissionSettings
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("code"), Display("code")]
        public String code { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 isActive { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime createdDate { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32 createdBy { get; set; }
        [Database("updated_date"), Display("updated_date")]
        public DateTime updatedDate { get; set; }
        [Database("updated_by"), Display("updated_by")]
        public Int32 updatedBy { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }

    public class CommissionSettingsDetails {
        public Int32 id { get; set; }
        public String code { get; set; }
        public Int32 isActive { get; set; }
        public DateTime createdDate { get; set; }
        public Int32 createdBy { get; set; }
        public DateTime updatedDate { get; set; }
        public Int32 updatedBy { get; set; }
        public Int32 status { get; set; }

        public String definitionCode { get; set; }
        public String definitionCodeName { get; set; }
    }
}
