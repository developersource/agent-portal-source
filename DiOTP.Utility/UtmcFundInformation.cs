using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_fund_information")]
    public class UtmcFundInformation
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("fund_code"), Display("FundCode")]
        public String FundCode { get; set; }
        [Database("fund_name"), Display("FundName")]
        public String FundName { get; set; }
        [Database("utmc_fund_categories_def_id"), Display("UtmcFundCategoriesDefId")]
        public Int32 UtmcFundCategoriesDefId { get; set; }
        [Database("effective_date"), Display("EffectiveDate")]
        public DateTime EffectiveDate { get; set; }
        [Database("lipper_category_of_fund"), Display("LipperCategoryOfFund")]
        public String LipperCategoryOfFund { get; set; }
        [Database("conventional"), Display("Conventional")]
        public String Conventional { get; set; }
        [Database("status"), Display("Status")]
        public String Status { get; set; }
        [Database("foreign_fund"), Display("ForeignFund")]
        public String ForeignFund { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime ReportDate { get; set; }
        [Database("fund_base_currency"), Display("FundBaseCurrency")]
        public String FundBaseCurrency { get; set; }
        [Database("is_emis"), Display("IsEmis")]
        public Int32? IsEmis { get; set; }
        [Database("fund_cls"), Display("FundCls")]
        public String FundCls { get; set; }
        [Database("is_retail"), Display("IsRetail")]
        public Int32? IsRetail { get; set; }
        [Database("sat_group"), Display("SatGroup")]
        public String SatGroup { get; set; }
        [Database("is_rsp"), Display("IsRsp")]
        public Int32? IsRsp { get; set; }
        public UtmcFundCategoriesDef UtmcFundCategoriesDefIdUtmcFundCategoriesDef { get; set; }
        public List<UtmcFundFile> UtmcFundInformationIdUtmcFundFiles { get; set; }
        public List<UtmcFundDetail> UtmcFundInformationIdUtmcFundDetails { get; set; }
        public List<UtmcFundCharge> UtmcFundInformationIdUtmcFundCharges { get; set; }
    }
}
