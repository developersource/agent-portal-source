﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAnnouncement.aspx.cs" Inherits="Admin.AddAnnouncement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addAnnouncementForm" runat="server">
        <div id="FormId" data-value="addAnnouncementForm">
            <div class="row">
                <div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Title:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="hidden" name="SiteContentTypeId" value="1" id="SiteContentTypeId" runat="server" clientidmode="static" />
                            <input type="hidden" name="Priority" value="1" id="Priority" runat="server" clientidmode="static" />
                            <input type="hidden" name="IsDisplay" value="1" id="IsDisplay" runat="server" clientidmode="static" />
                            <input type="hidden" name="IsContentDisplay" value="1" id="IsContentDisplay" runat="server" clientidmode="static" />
                            <input type="hidden" name="TitleColor" id="TitleColor" runat="server" clientidmode="static" />
                            <input type="hidden" name="SubTitle" id="SubTitle" runat="server" clientidmode="static" />
                            <input type="hidden" name="ContentColor" id="ContentColor" runat="server" clientidmode="static" />
                            <input type="hidden" name="ImageUrl" id="ImageUrl" runat="server" clientidmode="static" />
                            <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                            <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                            <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                            <input type="hidden" name="UpdatedBy" value="0" id="UpdatedBy" runat="server" clientidmode="static" />
                            <input type="hidden" name="UpdatedDate" value="0" id="UpdatedDate" runat="server" clientidmode="static" />

                            <input type="text" name="Title" id="Title" runat="server" clientidmode="static" class="form-control" placeholder="Enter Title" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Date of Publish:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="PublishDate" id="PublishDate" runat="server" clientidmode="static" class="form-control default-date-picker" placeholder="Select Date" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Short Display Content:</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="ShortDisplayContent" id="ShortDisplayContent" runat="server" clientidmode="static" class="form-control" placeholder="Enter Short Display Content" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Content:</label>
                        </div>
                        <div class="col-md-9">
                            <textarea placeholder="Enter Content" rows="5" class="form-control" name="Content" id="Content" runat="server"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $ui(document).ready(function () {
                    $ui('.default-date-picker').datepicker({
                        format: 'mm/dd/yyyy',
                        pickTime: false,
                        autoclose: true
                    });
                });
            </script>
        </div>
    </form>
</body>
</html>
