﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="InternetRisk.aspx.cs" Inherits="DiOTP.WebApp.InternetRisk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Internet Risk</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Internet Risk</h3>
                    </div>
                    <p>Apex Investment Services Berhad (“AISB”) is committed to maintain high security measures to ensure information provided are secure, but there are risks beyond the Company’s control in Internet communication. Please note that communication over the Internet is subjected to the common usual hazards of Internet communications, nor can it be guaranteed that this communication has not been the subject of unauthorised interception of modification.</p>
                    <p>You may be exposed to risks associated with hardware and software failure. Therefore, you should be aware and acknowledge the risks involved of sending confidential information through internet site when performing any transactions or enquiries online. As a precaution, you are advised to ensure your personal computer and mobile devices are protected from unauthorised access and/or virus that may reveal confidential information to a third party. You should also be wary of email scams and attachments that may attempt to obtain personal information via illicit means.</p>
                    <p>You are also required to ensure your PC and mobile devices are protected with an effective anti-virus / anti-malware software.</p>
                </div>
            </div>
        </div>
    </section>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
