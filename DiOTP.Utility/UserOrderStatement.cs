using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_order_statements")]
    public class UserOrderStatement
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("ref_no"), Display("RefNo")]
        public String RefNo { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("url"), Display("Url")]
        public String Url { get; set; }
        [Database("user_order_statement_type_id"), Display("UserOrderStatementTypeId")]
        public Int32 UserOrderStatementTypeId { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        public UserOrderStatementType UserOrderStatementTypeIdUserOrderStatementType { get; set; }
        public UserOrder RefNoUserOrder { get; set; }
    }
}
