﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class FundDetailedInformation
    {
        public List<UtmcFundCorporateAction> UtmcFundCorporateActions { get; set; }
        public UtmcFundInformation UtmcFundInformation { get; set; }
        //public List<UtmcDailyNavFund> UtmcDailyNavFunds { get; set; }
        public List<UtmcDailyNavFundWithChange> UtmcDailyNavFundsWithChanges { get; set; }
        //public List<UtmcDailyNavFund> UtmcDailyNavFundsMonthEnd { get; set; }
        public List<UtmcDailyNavFundWithChange> UtmcDailyNavFundWithChangeMonthEnd { get; set; }
        public DateTime CurrentNavDate { get; set; }
        public Decimal CurrentUnitPrice { get; set; }
        public Decimal CurrentPrice { get; set; }
        public Decimal Change { get; set; }
        public Decimal ChangePercent { get; set; }
        public FundDetailsAfterDividend FundDetailsAfterDividend { get; set; }
    }

    public class UtmcDailyNavFundAfterDividend
    {
        public Int32 Days { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime DailyNavDate { get; set; }
        public Decimal DailyNavUnitPrice { get; set; }
        public Decimal Distribution { get; set; }
        public Decimal NavAfterDividend { get; set; }
        public Decimal Changes { get; set; }
        public UtmcDailyNavFundAfterDividend()
        {
            Distribution = 1;
        }

    }

    public class FundDetailsAfterDividend
    {
        public List<UtmcDailyNavFundAfterDividend> utmcDailyNavFundAfterDividends { get; set; }
        public Decimal Return { get; set; }
        public Decimal Average { get; set; }
    }

    public class FundChartInformation
    {
        public Int32 fundId { get; set; }
        public string fundName { get; set; }
        public string fundCode { get; set; }
        public Decimal totalReturns { get; set; }
        public Decimal average { get; set; }
        public string threeYearAnnualisedPercent { get; set; }
        public Decimal minChange { get; set; }
        public Decimal maxChange { get; set; }
        public Decimal minNav { get; set; }
        public Decimal maxNav { get; set; }
        public Decimal currentNav { get; set; }
        public string currentNavDate { get; set; }
        public List<string> Labels { get; set; }
        public List<string> Values { get; set; }
        public List<string> NavPrices { get; set; }
        public List<string> NavDates { get; set; }
    }

    public enum DisplayChartBy
    {
        YTD = 1,
        LastYear = 2,
        OneWeek = 3,
        OneMonth = 4,
        ThreeMonth = 5,
        SixMonth = 6,
        OneYear = 7,
        TwoYear = 8,
        ThreeYear = 9,
        FiveYear = 10,
        TenYear = 11
    };
}
