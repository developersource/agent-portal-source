﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Show-Cart.aspx.cs" Inherits="DiOTP.WebApp.Show_Cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Cart</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head" id="cartType" runat="server">Cart</h3>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 5px; margin-left: 5px;">
                <img src="Content/MyImage/INFO.png" style="width: 15px; height: 15.25px; margin-right: 5px;" />
                <span style="font-weight: 600; color: #A0A0A0; font-size: 13px">You are currently viewing account</span>
            </div>
            <div class="row">
                <asp:DropDownList CssClass="form-control" ID="ddlUserAccountId" runat="server" ClientIDMode="Static" Style="width: 33.33%; margin-left: 18px;"></asp:DropDownList>

                <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4 mb-10" style="margin-left: 0px;">

                    <asp:HiddenField ID="isUserAccountChange" Value="false" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-20">
                    <%--<div class="table-head" visible="false">
                        
                    </div>--%>
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13 table-cart">
                            <tr>
                                <th id="fund1TD" runat="server" style="background-color: rgba(5, 156, 206, 1); color: white;">Fund
                                <br />
                                    Name</th>
                                <th id="fund2TD" runat="server" visible="false" style="background-color: rgba(5, 156, 206, 1); color: white;">Fund2</th>
                                <th style="background-color: rgba(5, 156, 206, 1); color: white;">Prospectus</th>
                                <th style="background-color: rgba(5, 156, 206, 1); color: white;">Product Highlights Sheets</th>
                                <th style="background-color: rgba(5, 156, 206, 1); color: white;">Information Memorandum</th>
                                <th style="background-color: rgba(5, 156, 206, 1); color: white;">Order Date</th>
                                <th class="text-right" id="thSalesCharge" runat="server" style="background-color: rgba(5, 156, 206, 1); color: white;">Sales Charge
                                <br />
                                    %</th>
                                <th class="text-right" id="transType" runat="server" style="background-color: rgba(5, 156, 206, 1); color: white;">Investment/ Redemption / Switching</th>
                                <th class="text-right" id="subscriptionFee" runat="server" visible="false" style="background-color: rgba(5, 156, 206, 1); color: white;">Subscription Fee MYR</th>
                            </tr>
                            <tbody id="tbodyCart" runat="server">
                            </tbody>
                            <tfoot id="totalTfoot" runat="server">
                                <tr>
                                    <th colspan="5" class="text-right" id="totalTfootTH" runat="server">Total</th>
                                    <th class="text-right" id="cartTotalAmount" runat="server" clientidmode="static">0</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="bankDetailsDiv" runat="server">
                    <h5>Bank Details</h5>
                    <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Acc Name</th>
                                <th>Acc No</th>
                            </tr>
                        </thead>
                        <tbody id="bankDetailsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-10" id="mobile">
                    <p class="mb-5">The above funds will be <span class="mb-5" id="transactionType" runat="server">Purchased/Direct Debited</span> using this investment account <strong id="maAccNo" runat="server"></strong></p>
                    <p class="mb-5">All orders received are irrevocable.</p>
                    <p class="mb-5">Important: Click <a class="text-info" href="#" data-toggle="modal" data-target="#righttocancel">here</a> for information about your right to cancel.</p>
                    <%--<p class="text-muted mb-5">Only information which forms part of the Prospectus should be relied upon. Printed copies of the Prospectus can also be obtained at our office and you may request for them by calling our helpline at +(603) 2095 9999.</p>
                    <p class="mb-10">Please note that the unit trusts are offered solely on the basis of the information contained in the electronic prospectus and any other information found elsewhere does not form part of the electronic prospectus.</p>--%>


                    <div class="mb-10">
                        <div class="checkbox">
                            <%--<sup class="text-danger" style="font-size: 20px; top: 3px;">*</sup>--%>
                            <asp:CheckBox ID="chkAgree" runat="server" ClientIDMode="Static" CssClass="checkbox-inline" Text="" />
                            <label style="margin-top: -10px">
                                <label for="chkAgree" class="text-justify">I confirm that I have read, understood and accepted the terms and conditions in the relevant Prospectus/Information Memorandum, Product Highlights Sheets and eApexIs website and have read and understood the <a class="text-info" href="#" data-toggle="modal" data-target="#riskDisclosureStatement">Unit Trust Loan Financing Risk Disclosure Statement</a>. I agree to be bound by the <a id="TCofTransaction" runat="server" clientidmode="static" data-toggle="modal" data-target="#myModal">Terms & Conditions of this investment</a>.</label>
                            </label>
                            <div class="text-left mb-10" id="rspNote" runat="server" visible="false">
                                <div>
                                    <p class="mb-5" style="font-family: 'Montserrat', sans-serif; margin-left: 40px;">
                                        I fully understand and agree to the <a target="_blank" href="Content/MyImage/[Form] DD T&C_FINAL.pdf">Terms & Conditions</a> of Direct Debit services and I acknowledge that upon successful completion of this online application, RM1.00 shall be debited from my selected account <b>to ensure the said account is active</b> for the purpose of application for this service and the amount shall be refunded to me by Apex Investment Services Berhad.
                                    </p>
                                </div><%--<iframe id="TCofRSP" runat="server" class="help-pop-img" src="Content/MyImage/[Form] DD T&C_FINAL.pdf" style="width: 100%; height: calc(100vh - 200px);"></iframe>--%>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-10 mt-10" id="buyPayDiv" runat="server" clientidmode="static">
                        <div class="col-md-3" style="width: 0%;">
                        </div>
                        <div class="col-md-3">

                            <asp:DropDownList ID="ddlPaymentMethodsList" runat="server" CssClass="form-control" ClientIDMode="Static">
                                <asp:ListItem Value="" Text="Select Payment Method"></asp:ListItem>
                                <asp:ListItem Value="1" Text="FPX Payment"></asp:ListItem>
                                <%-- <asp:ListItem Value="2" Text="Cheque"></asp:ListItem>--%>
                                <asp:ListItem Value="2" Text="Proof of Payment"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:;" id="guideProof" runat="server" class="test" clientidmode="static" data-toggle="tooltip" title="Click here to view guideline for attaching bank details file" style="position: absolute; right: 23px; margin-top: 4px;">
                                <div onclick="helpBank();"><i class="fa fa-info-circle text-info fs-16"></i></div>
                            </a>
                            <div class="form-control" id="lblPaymentMethodSelect" style="opacity: 0.4;">
                                Select payment method
                            </div>

                            <div class="mb-10">

                                <asp:FileUpload ID="fuPaymentMethod" runat="server" CssClass="form-control hide" ClientIDMode="Static" accept=".jpeg,.jpg,.png,.pdf" />
                                <sup id="fuPaymentAlert" class="text-danger">* <span style="font-size: 11px; font-weight: 100; top: 2px;">( jpeg / jpg / png / pdf )</span></sup>
                                <asp:DropDownList ID="ddlBankList" runat="server" ClientIDMode="Static" CssClass="form-control hide"></asp:DropDownList>

                            </div>
                            <div id="helpBank" class="bank-attachment-modal overlay hide">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="guide-popup">

                                            <p class="text-justify">
                                                Our Designated Accounts:
                                                <br />
                                                Bank Name: Maybank Islamic Berhad<br />
                                                Account Name: APEX INVESTMENT SERVICES BHD - CLIENTS TRUST ACCOUNT<br />
                                                Account Number: 5640 1662 7254<br />

                                                Make a bank transfer through Online Transfer, ATM and Cash Deposit to our designated bank accounts. Please make sure that the payment receipt is clear and consists of date, time, Apex Investment Services Berhad’s bank account and amount. For recipient reference, please fill up your Order Number, Payment Details
                                                <br />

                                                Take a colour photo of your document, ensure it’s in focus and we can read all the information as per the sample of proof is shown below: 
                                            </p>

                                            <!-- The Modals -->
                                            <div id="myModal1" class="modal-mobile hide">
                                                <span class="close help-pop-img-close">&times;</span>
                                                <img id="img01" class="modal-mobile-content" src="Content/MyImage/Guide_for_Proof_of_Payment.PNG" />
                                            </div>

                                            <img id="myimg1" class="help-pop-img" src="Content/MyImage/Guide_for_Proof_of_Payment.PNG" />
                                            <img id="myimg2" class="help-pop-img" src="Content/MyImage/Guide_for_Proof_of_Payment_2.PNG" />
                                            <img id="myimg3" class="help-pop-img" src="Content/MyImage/Guide_for_Proof_of_Payment_3.PNG" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span id="fpx_logo_span" runat="server" clientidmode="static" class="hide">&nbsp;&nbsp;&nbsp;via&nbsp;&nbsp;&nbsp;
                                <img src="Content/MyImage/FPX_logo.png" style="width: 80px; margin-top: -5px;" />
                                <span style="display:block;">
                                    By clicking "Confirm" button, you agree to <a target="_blank" href="https://uat.mepsfpx.com.my/FPXMain/termsAndConditions.jsp">FPX's Terms & Conditions</a>.
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="text-center mb-10">
                        <p style="text-align: left; margin-bottom: 0px;">
                            Please enter transaction password for verification
                            <sup class="text-danger">*</sup>:
                            <asp:TextBox TextMode="Password" ID="txtTransPassword" runat="server" CssClass="form-control" Style="width: 200px; display: inline;" ClientIDMode="Static"></asp:TextBox>

                        </p>
                        <div>
                            <p style="text-align: left;"><sup class="text-danger" style="font-size: 20px; top: 3px;">*</sup>
                                <small><strong>Note:</strong>
                                    <span id="warningLabel" style="color: red;">By default, transaction password will be your login password.</span>
                                </small>
                            </p>
                        </div>
                    </div>


                    <div class="text-right" style="margin-bottom: 10px;">
                        <div class="mt-5 mb-5">
                            <small class="text-danger" id="errorMessage" runat="server" clientidmode="static"></small>
                        </div>
                        <asp:HiddenField ID="lblUserIC" runat="server" />
                        <asp:Button ID="btnConfirm" runat="server" CssClass="btn btn-infos1" ClientIDMode="Static" Text="Confirm" OnClick="btnConfirm_Click" />
                        <asp:Button ID="btnPayLater" CausesValidation="false" OnClick="btnPayLater_Click" CssClass="btn btn-infos1" runat="server" Text="Pay Later" Style="margin-left: 10px;" />
                        <asp:Button ID="btnPayCanCel" OnClick="btnPayCanCel_Click" CausesValidation="false" CssClass="btn btn-infos1" runat="server" Text="Cancel" Style="margin-left: 10px;" />


                        <%--<div class="mt-10 mb-5">
                            
                        </div>--%>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
    <div class="modal fade" id="riskDisclosureStatement">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Unit Trust Loan Financing Risk Disclosure Statement</h4>
                </div>
                <div class="modal-body">
                    <p>Investing in unit trust fund with borrowed money is more risky than investing with your own savings.</p>
                    <p><b>You should access if loan financing is suitable for you</b> in light of your objectives, attitude to risk and financial circumstances. You should be aware of risks, which would include the following:</p>
                    <ul>
                        <li>The higher the margin of financing (that is, the amount of money you borrow for every ringgit of your own money which you put in as deposit or down payment), the greater the loss or gain on your investment;</li>
                        <li>You should access whether you have the ability to service the repayments on the proposal loan. If your loan is a variable rate loan, and if interest rates rise, your total repayment amount will be increased;</li>
                        <li>If unit prices fall beyond a certain level, you may be asked to provide additional acceptable collateral (where units are used as collateral) or pay additional amounts on top of your normal installments. If you fail to comply within the time prescribed, your units may be sold towards the settlement of your loan;</li>
                        <li>Returns on unit trusts are not guaranteed and maynot be earned evenly over time. this means that there may be some years where returns are high and other years where losses are experienced. Whether you eventually realise a gain or loss may be affected by the timing of the sale of your units. The value of units may fall just when you want your money back even though the investment may have done well in the past.</li>
                    </ul>
                    <p><b>This brief statement cannot disclose all the risks and other aspects of loan financing. You should therefore careful study the terms and conditions before you decide to take the loan. If you are in doubt in respect of any aspect of this risk disclosure statement or the terms of the loan financing, you should consult the institution offering loan.</b></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="righttocancel">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Rights to cancel</h4>
                </div>
                <div class="modal-body">

                    <p class="" style="color: forestgreen !important;">
                        The cooling off period is applicable to all first-time individual investors except for a staff of the Manager and persons registered with a body approved by the SC to deal in unit trusts. The cooling off right must be exercised within six (6) Business Days commencing from the date of receipt of the application for Units by the Manager.
                        <br />
                        The refund for every Unit held by the investor pursuant to exercising his cooling off right shall be sum of the NAV per Unit on the day the Units were first purchased and sales charge per Unit originally imposed on the day the Units were purchased. Switching is strictly during cooling off period.
                    </p>
                    <div style="font-size: 11px !important;">
                        <label>*Note: Cooling-off right is not applicable to ALL EPF-MIS scheme.</label>
                        <p>To exercise your cooling-off rights, please contact our Customer Service Representative +603 2095 9999 for assistance.</p>
                    </div>
                    <%--<p>Please contact our customer service at +603 2095 9999 for further information and assistance.</p>--%>
                    <%-- <p>Investing in unit trust fund with borrowed money is more risky than investing with your own savings.</p>
                    <p><b>You should access if loan financing is suitable for you</b> in light of your objectives, attitude to risk and financial circumstances. You should be aware of risks, which would include the following:</p>
                    <ul>
                        <li>The higher the margin of financing (that is, the amount of money you borrow for every ringgit of your own money which you put in as deposit or down payment), the greater the loss or gain on your investment;</li>
                        <li>You should access whether you have the ability to service the repayments on the proposal loan. If your loan is a variable rate loan, and if interest rates rise, your total repayment amount will be increased;</li>
                        <li>If unit prices fall beyond a certain level, you may be asked to provide additional acceptable collateral (where units are used as collateral) or pay additional amounts on top of your normal installments. If you fail to comply within the time prescribed, your units may be sold towards the settlement of your loan;</li>
                        <li>Returns on unit trusts are not guaranteed and maynot be earned evenly over time. this means that there may be some years where returns are high and other years where losses are experienced. Whether you eventually realise a gain or loss may be affected by the timing of the sale of your units. The value of units may fall just when you want your money back even though the investment may have done well in the past.</li>
                    </ul>
                    <p><b>This brief statement cannot disclose all the risks and other aspects of loan financing. You should therefore careful study the terms and conditions before you decide to take the loan. If you are in doubt in respect of any aspect of this risk disclosure statement or the terms of the loan financing, you should consult the institution offering loan.</b></p>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 75%;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Terms & Conditions</h4>
                </div>
                <div class="modal-body">
                    
                    <div id="TCofBuy" runat="server" clientidmode="static" visible="false">
                        
                        
                        <p>
                            1. Any person(s) 18 years of age and above can invest in unit trust funds. You may have a Joint Holder on your account. A designated account can be opened for a Joint Holder above 18 years of age only.
                        </p>
                        <p>
                            2. The Principle Holder will be solely the Authority to operate Account to effect all the transactions including buy, sell and switching in eApexIs Online Portal.
                        </p>
                        <p>
                            3. A sales charge of up to 5.2632% will be levied for investment into equity/mixed asset/balanced funds. No sales charge will be levied for investment into money market funds. Sales charge that is to be levied on the purchase of units by investors is not incorporated in the quoted NAV per unit of the funds and will be computed separately and deducted from the amount paid.
                        </p>
                        <p>
                            4. The minimum initial investment for all funds is RM 1,000.00 except for ADAK which is RM100.
                        </p>
                        <p>
                            5. The minimum additional investment for all funds is RM100.
                        </p>
                        <p>
                            6. Investments transacted before or at 4:00 pm, on any business day will be processed based on the price determined for the same business day; whilst investments transacted after 4:00 pm will be processed based on the price determined for the next business day. Any investment transacted on a non-business day will be treated as investments transacted on the following business day.
                        </p>
                        <p>
                            7. For a Money Market fund, investment transaction should be before or at 11.00 am on any business day.
                        </p>
                        <p>
                            8. In the event of early cut-off time due to Half-Day Trading Session by Bursa Malaysia or by the Manager, investments and transaction requests made after the cut-off time will be processed based on the price determined for the next business day. Notice on early cut-off time will be posted on eApexIs Online Homepage.
                        </p>
                        <p>
                            9. Distributions of less than RM 100.00 will be automatically reinvested in the purchase of additional units.
                        </p>
                        <p>
                            10. The unitholder shall fully indemnify and keep Apex Investment Services Bhd ("AISB") fully indemnified against any actions, proceedings, claims, losses, damages, costs and expenses which may be brought against, suffered or incurred and/or to which may be suffered or incurred by AISB arising either directly or indirectly out of or in connection with AISB accepting, relying on or failing to act on any instructions given by the unitholder unless due to the willful default or negligence of AISB.
                        </p>
                        <p>
                            11. AISB reserves the right to accept or reject any investment in whole or in part without assigning any reason.
                        </p>
                            
                    </div>
                    <div id="TCofSell" runat="server" clientidmode="static" visible="false">
                        <p>
                            1. Redemption of units is a withdrawal of investment and will be processed based on the NAV per unit at the close of the business day.
                        </p>
                        <p>
                            2. Redemption transacted before or at 4:00 pm on any business day will be processed based on the price determined for the same business day; whilst redemption transacted after 4:00 pm will be processed based on the price determined for the next business day. Any redemption transacted on a non-business day will be treated as redemption transacted on the following business day.
                        </p>
                        <p>
                            3. In the event of early cut-off time due to Half-Day Trading Session by Bursa Malaysia or by the Manager, investments and transaction requests made after the cut-off time will be processed based on the price determined for the next business day. Notice on early cut-off time will be posted on Apex Investment Services Berhad Website and eApexIs Online Homepage.
                        </p>
                        <p>
                            4. Redemption proceeds will be paid within 10 days of your redemption request.
                        </p>
                        <p>
                            5. Redemption proceeds will be banked-in to your bank account registered in eApexIs Online Portal.
                        </p>
                        <p>
                            6. For partial redemption, please leave the minimum balance of units required to maintain your account.
                        </p>
                        <p>
                            7. Upon your full redemption, please advise the bank to terminate your Direct Debit Authorisation (DDA) of your Regular Saving Plan if you do not wish to continue your DDA. Any DDA payment received by Apex Investment after your full redemption may be processed into your account(s). To withdraw the amount, you are required to submit Redemption request.
                        </p>
                        <p>
                            8. The unitholder shall fully indemnify and keep Apex Investment Services Berhad (“AISB”) fully indemnified against any actions, proceedings, claims, losses, damages, costs and expenses which may be brought against, suffered or incurred and/or to which may be suffered or incurred by AISB arising either directly or indirectly out of or in connection with AISB accepting, relying on or failing to act on any instructions given by the unitholder unless due to the wilful default or negligence of AISB.
                        </p>
                        <p>
                            9. AISB reserves the right to accept or reject the redemption request in whole or in part without assigning any reason.
                        </p>
                    </div>
                    <div id="TCofSwitch" runat="server" clientidmode="static" visible="false">
                        <p>
                            1. Switching of units is considered a withdrawal/redemption of investment from a fund and an application to purchase units of another fund.
                       
                        </p>
                        <p>
                            2. Switching of zero-load, low-load funds to higher load fund will be subject to sales charge of up to 5.2632%.
                           
                        </p>
                        <p>
                            3. Three free switches per account are allowed in each calendar year. Subsequent switches will be charged at 1% of redemption fee for administrative purpose. Switching fee is deducted from the redemption proceeds and the net proceeds will be processed into the 'switch to' accounts based on the NAV per unit at the close of the business day.
                           
                        </p>
                        <p>
                            4. Switching transacted before or at 4:00 pm on any business day will be processed based on the price determined for the same business day; whilst switching transacted after 4:00 pm will be processed based on the price determined for the next business day. Any switching transacted on a non-business day will be treated as switching transacted on the following business day.
                        </p>
                        <p>
                            5. For a Money Market fund, switching transaction should be before or at 10.00 am on any business day
                        </p>
                        <p>
                            6. Switching requests for foreign funds received by Apex Investment Services Bhd ("AISB") during non-business days of the funds will be processed based on pricing determined at the close of the next business day when both the switch out and switch to funds are opened for trading.
                        </p>
                        <p>
                            7. In the event of early cut-off time due to Half-Day Trading Session by Bursa Malaysia or by the Manager, investments and transaction requests made after the cut-off time will be processed based on the price determined for the next business day. Notice on early cut-off time will be posted on eApexIs Online Homepage.
                        </p>
                        <p>
                            8. AISB reserves the right to reject any switching request that it regards as disruptive to efficient portfolio management of the fund; or if deemed by AISB to be contrary to the best interest of the fund. Switching requests which are rejected by AISB would be treated as a redemption of units from a fund and a rejection of application to purchase units in the intended fund. As such, unitholders will receive payment of redemption proceeds in the event of a rejection in switching requests.
                        </p>
                        <p>
                            9. For partial switching, please leave the minimum balance of units required to maintain your account.
                        </p>
                        <p>
                            10. Upon your full switching, please advise the bank to terminate your Direct Debit Authorisation (DDA) if you do not wish to continue your DDA. Any DDA payment received by AISB after your full switching may be processed into your accounts. To withdraw the amount, you are required to submit Redemption Form.
                        </p>
                        <p>
                            11. The unitholder shall fully indemnify and keep AISB fully indemnified against any actions, proceedings, claims, losses, damages, costs and expenses which may be brought against, suffered or incurred and/or to which may be suffered or incurred by AISB arising either directly or indirectly out of or in connection with AISB accepting, relying on or failing to act on any instructions given by the unitholder unless due to the willful default or negligence of AISB.
                        </p>
                        <p>
                            12. AISB reserves the right to accept or reject the switching request in whole or in part without assigning any reason.
                        </p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script>

        $(document).ready(function () {

            $('body').on('click', '.help-pop-img', function () {
                $("#myModal1").css('display', 'block');
                $("#myModal1").removeClass('hide');
                $("#img01").attr('src', $(this).attr('src'));
            });
            $('body').on('click', '.help-pop-img-close', function () {
                $("#myModal1").css('display', 'none');
                $("#myModal1").addClass('hide');
            });

            $('#ddlPaymentMethodsList').change(function () {
                var ddlPayMode = $(this).val();
                if (ddlPayMode == "") {
                    $('#fuPaymentMethod').addClass('hide');
                    $('#fuPaymentAlert').addClass('hide');
                    $('#guideProof').addClass('hide');
                    $('#ddlBankList').addClass('hide');
                    $('#fpx_logo_span').addClass('hide');
                    $('#lblPaymentMethodSelect').removeClass('hide');
                    $('#btnConfirm').val('Confirm');
                }
                else if (ddlPayMode == "1") {
                    $('#ddlBankList').removeClass('hide');
                    $('#fuPaymentMethod').addClass('hide');
                    $('#fuPaymentAlert').addClass('hide');
                    $('#guideProof').addClass('hide');
                    $('#fpx_logo_span').removeClass('hide');
                    $('#lblPaymentMethodSelect').addClass('hide');
                    //$('#btnConfirm').val('Confirm to Pay');
                }
                else if (ddlPayMode == "2") {
                    $('#ddlBankList').addClass('hide');
                    $('#fuPaymentMethod').removeClass('hide');
                    $('#fuPaymentAlert').removeClass('hide');
                    $('#guideProof').removeClass('hide');
                    $('#fpx_logo_span').addClass('hide');
                    $('#lblPaymentMethodSelect').addClass('hide');
                    //$('#btnConfirm').val('Confirm');
                }
            });
            $('#ddlPaymentMethodsList').change();
            $('#btnConfirm').click(function () {
                var agree = $('#chkAgree').is(':checked');
                //var agree2 = $('#chkAgree2').is('checked');
                var transPwd = $('#txtTransPassword').val();
                var ddlPayMode = $('#ddlPaymentMethodsList').val();
                var ddlBank = $('#ddlBankList').val();
                var popFile = $('#fuPaymentMethod').val();
                if (!$('#buyPayDiv').hasClass('hide')) {
                    var transAmt = parseFloat($('#cartTotalAmount').attr('value'));
                    if (ddlPayMode == "") {
                        ShowCustomMessage('Alert', 'Please fill the required fields.', '');
                        return false;
                    }
                    else if (ddlPayMode == "1" && ddlBank == "") {
                        ShowCustomMessage('Alert', 'Please select bank.', '');
                        return false;
                    }
                    else if (ddlPayMode == "3" && popFile == "") {
                        ShowCustomMessage('Alert', 'Please choose file.', '');
                        return false;
                    }
                    else if (transPwd == "") {
                        ShowCustomMessage('Alert', 'Please enter transaction password.', '');
                        return false;
                    }
                    else if (!agree) {
                        ShowCustomMessage('Alert', 'Please agree to proceed.', '');
                        return false;
                    }
                    if (ddlPayMode == "1") {
                        if (transAmt < 1) {
                            ShowCustomMessage('Alert', 'Transaction Amount is Lower than the Minimum Limit RM 1.00', '');
                            return false;
                        }
                        else if (transAmt > 30000) {
                            ShowCustomMessage('Alert', 'Maximum Transaction Limit Exceeded RM 30,000.00', '');
                            return false;
                        }
                        else {
                            setTimeout(function () {
                                $('.loader-bg').fadeIn();
                            }, 500);
                            return true;
                        }
                    }
                    //else if (!agree2) {
                    //ShowCustomMessage('Alert', 'Please agree to proceed.', '');
                    //return false;
                    //}
                    else {
                        setTimeout(function () {
                            $('.loader-bg').fadeIn();
                        }, 500);
                        return true;
                    }
                }
                else {
                    if (transPwd == "") {
                        ShowCustomMessage('Alert', 'Please enter transaction password.', '');
                        return false;
                    }
                    else if (!agree) {
                        ShowCustomMessage('Alert', 'Please agree to proceed.', '');
                        return false;
                    }
                    else {
                        setTimeout(function () {
                            $('.loader-bg').fadeIn();
                        }, 500);
                        return true;
                    }
                }
            });

            $('#guideProof').addClass('hide');


        });




        function helpBank() {
            ShowCustomGuideMessage("Proof of Payment", $(".guide-popup").html(), "");
        }
        function closeHelpBank() {
            $('#helpBank').addClass('hide');
        }

        function checkTypeofPayment() {
            alert('test');
            if ($('#ddlPaymentMethodsList').index = 1) {
                $('#guideProof').addClass('hide');
            }
            else if ($('#ddlPaymentMethodsList').index = 2) {
                $('#guideProof').removeClass('hide');
            }
            else if ($('#ddlPaymentMethodsList').index = 3) {
                $('#guideProof').removeClass('hide');
            }
            document.getElementById('ddlPaymentMethodsList');
        }

        function checkTypeofTransaction() {

            $.ajax({
                url: "Show-Cart.aspx/CheckTransactionType",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { fundId: fundId, amount: amount },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalAmount = data.d.TotalAmount;
                    BindCart();
                    if (isModify == 1) {
                        $('#successMessage').html('Successfully Modified with your confirmation.');
                        InsertIntoOrderCard(fundId, amount, distributionID, consultantID);
                    }
                    else {
                        $('#successMessage').html('Successfully Added.');
                        InsertIntoOrderCard(fundId, amount, distributionID, consultantID);
                    }
                    $('#cart-table').removeClass('hide');
                }
            });
        }

        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: ' UNITS', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
        }
        FormatAllUnit();
    </script>
</asp:Content>
