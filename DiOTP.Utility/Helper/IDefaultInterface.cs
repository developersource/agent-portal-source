using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.Helper
{
    public interface IDefaultInterface<T>
    {
        Response PostData(T obj);
        Int32 PostBulkData(List<T> objs);
        Response UpdateData(T obj);
        Int32 UpdateBulkData(List<T> objs);
        T DeleteData(Int32 Id);
        Int32 DeleteBulkData(List<Int32> Ids);
        Response GetSingle(Int32 Id);
        Response GetData(int skip, int take, bool isOrderByDesc);
        Int32 GetCount();
        Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc);
        Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual);
        Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc);
        Int32 GetCountByFilter(string filter);
    }
}
