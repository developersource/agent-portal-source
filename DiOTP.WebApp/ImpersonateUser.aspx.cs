﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ImpersonateUser : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String idString = Request.QueryString["id"];
                if (!string.IsNullOrEmpty(idString))
                {
                    idString = idString.Replace(" ", "+");
                    try
                    {
                        idString = CustomEncryptorDecryptor.DecryptText(idString);
                        idString = idString.Split('_')[0];
                        int id = 0;
                        if (int.TryParse(idString, out id))
                        {
                            Response response = IUserService.GetSingle(id);
                            if (response.IsSuccess)
                            {
                                User user = (User)response.Data;
                                Session["user"] = user;
                                Session["IsImpersonated"] = true;
                                Session["LastLoginIP"] = user.LastLoginIp;
                                Session["LastLoginTime"] = user.LastLoginDate;
                                Session["CurrentLoginTime"] = DateTime.Now;

                                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id='" + user.Id + "' and status='1'", 0, 0, false);
                                if (responseUAList.IsSuccess)
                                {
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                    UserAccount primaryAccount = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();
                                    if (primaryAccount == null)
                                    {
                                        primaryAccount = userAccounts.FirstOrDefault();
                                    }
                                    //Session["SelectedAccountHolderNo"] = primaryAccount.AccountNo;
                                }
                                Response.Redirect("/Portfolio.aspx", false);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'error on int.TryParse', '/');", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + ex.Message + "\", '/');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'idString is null', '/');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}