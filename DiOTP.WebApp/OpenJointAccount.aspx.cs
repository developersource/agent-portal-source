﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class OpenJointAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (ddlRelationship.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Please select Relationship.\", '');", true);
            }
            else
            {
                Session["IsJointAccountOpening"] = 1;
                Session["JointRelationship"] = ddlRelationship.SelectedValue;
                Response.Redirect("/AccountOpening.aspx");
            }
        }
    }
}