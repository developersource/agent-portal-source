﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class MABanks : System.Web.UI.Page
    {
        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyHolderBankServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT us.username,ua.id_no,ua.account_no as ma_no,b.name as bank_name,m.account_name,m.bank_account_no, m.image, u.updated_date from ");
            string mainQCount = (@"SELECT count(*) from ");
            filter.Append(@"ma_holder_bank m
join user_account_banks u on u.ma_holder_bank_id = m.id and u.status=1
join banks_def b on b.id = m.bank_def_id
join user_accounts ua on ua.id = u.user_account_id
join users us on us.id = ua.user_id where u.status=1 and b.status=1 ");
            
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (us.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ua.id_no like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ua.account_no like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ua.account_no like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or m.bank_account_no like '%" + Search.Value.Trim() + "%')");
                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }
                Response responseBDList = IBanksDefService.GetData(0, 0, true);
                List<BanksDef> bankdefs = new List<BanksDef>();
                if (responseBDList.IsSuccess)
                {
                    bankdefs = (List<BanksDef>)responseBDList.Data;
                }
                filter.Append("order by u.updated_date desc");
                Response responseMABanks = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, true);

                if (responseMABanks.IsSuccess)
                {
                    var mABankBindingsDyn = responseMABanks.Data;
                    var responseJSON = JsonConvert.SerializeObject(mABankBindingsDyn);
                    List<MABankBinding> mABankBindings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MABankBinding>>(responseJSON);

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    mABankBindings.ForEach(x =>
                    {
                        string image = !string.IsNullOrEmpty(x.image) ? "Image <i class='fa fa-eye'></i>" : "";
                        asb.Append(@"<tr>
                                        <td>
                                            " + index + @"
                                        </td>
                                        <td>" + x.ma_no + @"
                                        </td>
                                        <td>
                                            " + x.updated_date.ToString("dd/MM/yyyy") + @"
                                        </td>
                                        <td>" + x.username + @"</td>
                                        <td>" + x.id_no + @"</td>
                                        <td>
                                                <div><label>Bank name</label>: <span>" + x.bank_name + @"</span></div>
                                                <div><label>Account name</label>: <span>" + x.account_name + @"</span></div>
                                                <div><label>Account number</label>: <span>" + x.bank_account_no + @"</span></div>
                                        </td>
                                        <td><a target='_blank' href=" + x.image + @">" + image + @"</a></td>
                                    </tr>");
                        index++;
                    });
                    bankDetailsTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                               "alert(\\'"+ responseMABanks.Message + "\\');", true);
                }


                                
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "MA_Bank_Binding_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("MA Banks");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No.", "MA.No", "Binded date", "Username", "ID No", "Bank name", "Account name", "Account.No", "Image URL" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["MA Banks"];

                string docDetails = "MA Banks";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    filterQuery.Append(@"SELECT us.username,ua.id_no,ua.account_no as ma_no,b.name as bank_name,m.account_name,m.bank_account_no, m.image, u.updated_date from ma_holder_bank m
                        join user_account_banks u on u.ma_holder_bank_id = m.id and u.status=1
                        join banks_def b on b.id = m.bank_def_id
                        join user_accounts ua on ua.id = u.user_account_id
                        join users us on us.id = ua.user_id where u.status=1 ");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;
                    filterQuery.Append(@"order by u.updated_date desc");
                    Response responseMABanks = GenericService.GetDataByQuery(filterQuery.ToString(), 0, 0, false, null, false, null, true);

                    if (responseMABanks.IsSuccess)
                    {
                        var mABankBindingsDyn = responseMABanks.Data;
                        var responseJSON = JsonConvert.SerializeObject(mABankBindingsDyn);
                        List<MABankBinding> mABankBindings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MABankBinding>>(responseJSON);
                        int index = 1;
                        mABankBindings.ForEach(x =>
                        {
                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = x.ma_no;
                            worksheet.Cells[row, 3].Value = x.updated_date.ToString("dd/MM/yyyy");
                            worksheet.Cells[row, 4].Value = x.username;
                            worksheet.Cells[row, 5].Value = x.id_no;
                            worksheet.Cells[row, 6].Value = x.bank_name;
                            worksheet.Cells[row, 7].Value = x.account_name;
                            worksheet.Cells[row, 8].Value = x.bank_account_no;
                            worksheet.Cells[row, 9].Value = x.image;
                            row++;
                            index++;
                        });
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "User Address Change Request List downloaded",
                        TableName = "banks_def",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "MA_Bank_Binding" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }
    }
}