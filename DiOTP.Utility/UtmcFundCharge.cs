using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_fund_charges")]
    public class UtmcFundCharge
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("utmc_fund_information_id"), Display("UtmcFundInformationId")]
        public Int32 UtmcFundInformationId { get; set; }
        [Database("initial_sales_charges_percent"), Display("InitialSalesChargesPercent")]
        public Decimal InitialSalesChargesPercent { get; set; }
        [Database("annual_management_charge_percent"), Display("AnnualManagementChargePercent")]
        public Decimal AnnualManagementChargePercent { get; set; }
        [Database("trustee_fee_percent"), Display("TrusteeFeePercent")]
        public Decimal TrusteeFeePercent { get; set; }
        [Database("switching_fee_percent"), Display("SwitchingFeePercent")]
        public Decimal SwitchingFeePercent { get; set; }
        [Database("redemption_fee_percent"), Display("RedemptionFeePercent")]
        public Decimal RedemptionFeePercent { get; set; }
        [Database("transfer_fee_rm"), Display("TransferFeeRm")]
        public Decimal TransferFeeRm { get; set; }
        [Database("other_significant_fee_rm"), Display("OtherSignificantFeeRm")]
        public Decimal OtherSignificantFeeRm { get; set; }
        [Database("epf_sales_charges_percent"), Display("EpfSalesChargesPercent")]
        public Decimal EpfSalesChargesPercent { get; set; }
        [Database("gst_percent"), Display("GstPercent")] 
        public Decimal GstPercent { get; set; }
        public UtmcFundInformation UtmcFundInformationIdUtmcFundInformation { get; set; }
    }
}
