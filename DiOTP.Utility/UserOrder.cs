using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_orders")]
    public class UserOrder
    {
        public UserOrder()
        {
            TransAmt = 0;
            TransUnits = 0;
            AppFee = 0;
            ExitFee = 0;
            FeeAmt = 0;
            NetAmt = 0;
            TransPr = 0;
        }

        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("ref_no"), Display("RefNo")]
        public String RefNo { get; set; }
        [Database("order_type"), Display("OrderType")]
        public Int32 OrderType { get; set; }
        [Database("fund_id"), Display("FundId")]
        public Int32 FundId { get; set; }
        [Database("to_fund_id"), Display("ToFundId")]
        public Int32 ToFundId { get; set; }
        [Database("to_account_id"), Display("ToAccountId")]
        public Int32 ToAccountId { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("order_no"), Display("OrderNo")]
        public String OrderNo { get; set; }
        [Database("payment_method"), Display("PaymentMethod")]
        public String PaymentMethod { get; set; }
        [Database("amount"), Display("Amount")]
        public Decimal Amount { get; set; }
        [Database("units"), Display("Units")]
        public Decimal Units { get; set; }
        [Database("trans_id"), Display("TransId")]
        public Int32 TransId { get; set; }
        [Database("trans_no"), Display("TransNo")]
        public String TransNo { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32? CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("consultantID"), Display("ConsultantId")]
        public string ConsultantId { get; set; }
        [Database("is_tax_invoice"), Display("IsTaxInvoice")]
        public Int32 IsTaxInvoice { get; set; }
        [Database("is_cas"), Display("IsCas")]
        public Int32 IsCas { get; set; }
        [Database("is_credit_note"), Display("IsCreditNote")]
        public Int32 IsCreditNote { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("reject_reason"), Display("RejectReason")]
        public string RejectReason { get; set; }
        [Database("order_status"), Display("OrderStatus")]
        public Int32 OrderStatus { get; set; }
        [Database("fpx_transaction_id"), Display("FpxTransactionId")]
        public string FpxTransactionId { get; set; }
        [Database("fpx_status"), Display("FpxStatus")]
        public string FpxStatus { get; set; }
        [Database("fpx_bank_code"), Display("BankCode")]
        public string BankCode { get; set; }
        [Database("payment_date"), Display("PaymentDate")]
        public DateTime? PaymentDate { get; set; }
        [Database("settlement_date"), Display("SettlementDate")]
        public DateTime? SettlementDate { get; set; }
        [Database("confirmation_date"), Display("ConfirmationDate")]
        public DateTime? ConfirmationDate { get; set; }
        [Database("distribution_instruction"), Display("DistributionInstruction")]
        public Int32 DistributionInstruction { get; set; }

        [Database("bank_id"), Display("BankId")]
        public Int32 BankId { get; set; }


        [Database("trans_amt"), Display("TransAmt")]
        public Decimal? TransAmt { get; set; }

        [Database("trans_units"), Display("TransUnits")]
        public Decimal? TransUnits { get; set; }

        [Database("app_fee"), Display("AppFee")]
        public Decimal? AppFee { get; set; }

        [Database("exit_fee"), Display("ExitFee")]
        public Decimal? ExitFee { get; set; }

        [Database("fee_amt"), Display("FeeAmt")]
        public Decimal? FeeAmt { get; set; }

        [Database("net_amt"), Display("NetAmt")]
        public Decimal? NetAmt { get; set; }

        [Database("trans_pr"), Display("TransPr")]
        public Decimal? TransPr { get; set; }

        public List<UserOrderFile> UserOrderIdUserOrderFiles { get; set; }

        public List<UserOrderStatement> RefNoUserOrderStatements { get; set; }
    }



    public class UserOrderCustom
    {
        public Int32 Id { get; set; }
        public String RefNo { get; set; }
        public String Username { get; set; }
        public String AccountNo { get; set; }
        public Int32 OrderType { get; set; }
        public Int32 FundId { get; set; }
        public Int32 ToFundId { get; set; }
        public Int32 ToAccountId { get; set; }
        public String IdNo { get; set; }
        public Int32 UserId { get; set; }
        public Int32 UserAccountId { get; set; }
        public String OrderNo { get; set; }
        public String PaymentMethod { get; set; }
        public Decimal Amount { get; set; }
        public Decimal SumAmount { get; set; }
        public Decimal Units { get; set; }
        public Decimal SumUnits { get; set; }
        public Int32 TransId { get; set; }
        public String TransNo { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ConsultantId { get; set; }
        public Int32 IsTaxInvoice { get; set; }
        public Int32 IsCas { get; set; }
        public Int32 IsCreditNote { get; set; }
        public Int32 Status { get; set; }
        public string RejectReason { get; set; }
        public Int32 OrderStatus { get; set; }
        public string FpxTransactionId { get; set; }
        public string FpxStatus { get; set; }
        public string BankCode { get; set; }
        public string FpxBankCode { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public DateTime? ConfirmationDate { get; set; }
        public Int32 DistributionInstruction { get; set; }
        public String HolderClass { get; set; }
        public Int32 BankId { get; set; }
        public Int32 NoOfItems { get; set; }

        public string FundNames { get; set; }


        public List<UserOrderFile> UserOrderIdUserOrderFiles { get; set; }

        public List<UserOrderStatement> RefNoUserOrderStatements { get; set; }
    }
}
