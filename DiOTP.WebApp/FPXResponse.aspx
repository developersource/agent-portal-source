﻿<%@ Page ValidateRequest="false" Language="C#" MasterPageFile="~/AccountMaster.Master" AutoEventWireup="true" CodeBehind="FPXResponse.aspx.cs" Inherits="DiOTP.WebApp.FPXResponse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        .mobile-help {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li><a href="javascript:;">Transactions</a></li>
                    <li class="active">FPX Response</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">FPX Response</h3>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <%@ Import Namespace="System" %>
                    <%@ Import Namespace="System.IO" %>
                    <%@ Import Namespace="System.Text" %>
                    <%@ Import Namespace="System.Security.Cryptography" %>
                    <%@ Import Namespace="System.Security.Cryptography.X509Certificates" %>
                    <%@ Import Namespace="System.Globalization" %>
                    <%@ Import Namespace="DiOTP.WebApp.FPXLibary" %>
                    <%@ Import Namespace="DiOTP.Utility.CustomClasses" %>

                    <%
                        Controller c = new Controller();
                        String fpx_buyerBankBranch = Request.Form["fpx_buyerBankBranch"];
                        String fpx_buyerBankId = Request.Form["fpx_buyerBankId"];
                        String fpx_buyerIban = Request.Form["fpx_buyerIban"];
                        String fpx_buyerId = Request.Form["fpx_buyerId"];
                        String fpx_buyerName = Request.Form["fpx_buyerName"];
                        String fpx_creditAuthCode = Request.Form["fpx_creditAuthCode"];
                        String fpx_creditAuthNo = Request.Form["fpx_creditAuthNo"];
                        String fpx_debitAuthCode = Request.Form["fpx_debitAuthCode"];
                        String fpx_debitAuthNo = Request.Form["fpx_debitAuthNo"];
                        String fpx_fpxTxnId = Request.Form["fpx_fpxTxnId"];
                        String fpx_fpxTxnTime = Request.Form["fpx_fpxTxnTime"];
                        String fpx_makerName = Request.Form["fpx_makerName"];
                        String fpx_msgToken = Request.Form["fpx_msgToken"];
                        String fpx_msgType = Request.Form["fpx_msgType"];
                        String fpx_sellerExId = Request.Form["fpx_sellerExId"];
                        String fpx_sellerExOrderNo = Request.Form["fpx_sellerExOrderNo"];
                        String fpx_sellerId = Request.Form["fpx_sellerId"];
                        String fpx_sellerOrderNo = Request.Form["fpx_sellerOrderNo"];
                        String fpx_sellerTxnTime = Request.Form["fpx_sellerTxnTime"];
                        String fpx_txnAmount = Convert.ToDecimal(Request.Form["fpx_txnAmount"], CultureInfo.CurrentCulture).ToString("N2");
                        String fpx_txnCurrency = Request.Form["fpx_txnCurrency"];
                        String fpx_checkSum = Request.Form["fpx_checkSum"];
                        String fpx_checkSumString = "";
                        fpx_checkSumString = fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|" + fpx_creditAuthCode + "|" + fpx_creditAuthNo + "|" + fpx_debitAuthCode + "|" + fpx_debitAuthNo + "|" + fpx_fpxTxnId + "|" + fpx_fpxTxnTime + "|" + fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|";
                        fpx_checkSumString += fpx_sellerExId + "|" + fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency;

                        String mesg_type = Request.Form["mesg_type"];
                        String mesgFromFpx = Request.Form["mesgFromFpx"];
                        String mesg_token = Request.Form["mesg_token"];
                        String key_type = Request.Form["key_type"];
                        String seller_ex_desc = Request.Form["seller_ex_desc"];
                        String seller_ex_id = Request.Form["seller_ex_id"];
                        String order_no = Request.Form["order_no"];
                        String seller_txn_time = Request.Form["seller_txn_time"];
                        String seller_order_no = Request.Form["seller_order_no"];
                        String seller_id = Request.Form["seller_id"];
                        String seller_fpx_bank_code = Request.Form["seller_fpx_bank_code"];
                        String buyer_mail_id = Request.Form["buyer_mail_id"];
                        String txn_amt = Request.Form["txn_amt"];
                        String checksum = Request.Form["checksum"];
                        String debit_auth_code = Request.Form["debit_auth_code"];
                        String debit_auth_no = Request.Form["debit_auth_no"];
                        String buyer_bank = Request.Form["buyer_bank"];
                        String buyer_bank_branch = Request.Form["buyer_bank_branch"];
                        String buyer_name = Request.Form["buyer_name"];
                        String FPX_TXN_ID = Request.Form["FPX_TXN_ID"];
                        String finalVerifiMsg = "ERROR";
                        String xmlMessage = "";
                        xmlMessage = fpx_checkSumString;
                        finalVerifiMsg = c.nvl_VerifiMsg(xmlMessage, fpx_checkSum, Request.PhysicalApplicationPath); //Certificate Path

                        Type t = Type.GetType("FPXBank");
                        if (t == null)
                        {
                            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                            {
                                t = a.GetType("DiOTP.Utility.CustomClasses." + "FPXBank");
                                if (t != null)
                                    break;
                            }
                        }
                        FPXBank b = null;
                        if(t != null)
                            b = FPXEnumeration.GetByBankId(t, fpx_buyerBankId);

                    %>
                    <input type="hidden" name="fpx_buyerBankBranch" value='<%=fpx_buyerBankBranch%>' />
                    <input type="hidden" name="fpx_buyerBankId" value='<%=fpx_buyerBankId%>' />
                    <input type="hidden" name="fpx_buyerIban" value='<%=fpx_buyerIban%>' />
                    <input type="hidden" name="fpx_buyerId" value='<%=fpx_buyerId%>' />
                    <input type="hidden" name="fpx_buyerName" value='<%=fpx_buyerName%>' />
                    <input type="hidden" name="fpx_creditAuthCode" value='<%=fpx_creditAuthCode%>' />
                    <input type="hidden" name="fpx_creditAuthNo" value='<%=fpx_creditAuthNo%>' />
                    <input type="hidden" name="fpx_debitAuthCode" value='<%=fpx_debitAuthCode%>' />
                    <input type="hidden" name="fpx_debitAuthNo" value='<%=fpx_debitAuthNo%>' />
                    <input type="hidden" name="fpx_fpxTxnId" value='<%=fpx_fpxTxnId%>' />
                    <input type="hidden" name="fpx_fpxTxnTime" value='<%=fpx_fpxTxnTime%>' />
                    <input type="hidden" name="fpx_makerName" value='<%=fpx_makerName%>' />
                    <input type="hidden" name="fpx_msgToken" value='<%=fpx_msgToken%>' />
                    <input type="hidden" name="fpx_msgType" value='<%=fpx_msgType%>' />
                    <input type="hidden" name="fpx_sellerExId" value='<%=fpx_sellerExId%>' />
                    <input type="hidden" name="fpx_sellerExOrderNo" value='<%=fpx_sellerExOrderNo%>' />
                    <input type="hidden" name="fpx_sellerId" value='<%=fpx_sellerId%>' />
                    <input type="hidden" name="fpx_sellerOrderNo" value='<%=fpx_sellerOrderNo%>' />
                    <input type="hidden" name="fpx_sellerTxnTime" value='<%=fpx_sellerTxnTime%>' />
                    <input type="hidden" name="fpx_txnAmount" value='<%=fpx_txnAmount%>' />
                    <input type="hidden" name="fpx_txnCurrency" value='<%=fpx_txnCurrency%>' />
                    <input type="hidden" name="fpx_checkSum" value='<%=fpx_checkSum%>' />
                    <input type="hidden" name="xmlMessage" value='<%=xmlMessage%>' />


                    <table class="table no-border <% if (!fpx_sellerOrderNo.Contains("RS"))
                        { %>hide<% } %>"
                        id="rspEnrollmentDetails">
                        <thead>
                            <tr>
                                <th colspan="2">Enrollment Details
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Account Number
                                </th>
                                <td>: <span id="accNo" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Bank Name
                                </th>
                                <td>: <span id="bankName" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Seller Order No
                                </th>
                                <td>: <span id="orderNo" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Deduction Date
                                </th>
                                <td>: 1<sup>st</sup> of each month
                                </td>
                            </tr>
                            <tr>
                                <th>RSP Amount (MYR)
                                </th>
                                <td>: <span id="amount" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Transaction Date/Time
                                </th>
                                <td>: <span id="transDate" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Transaction Description
                                </th>
                                <td>: <span id="transDesc" runat="server"></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table no-border <% if (!fpx_sellerOrderNo.Contains("RS"))
                        { %>hide<% } %>"
                        id="fpxPaymentDetails">
                        <thead>
                            <tr>
                                <th colspan="2">FPX Payment Details
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>e-manddate Bank Name
                                </th>
                                <td>: <span id="bankName2" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>FPX  ID
                                </th>
                                <td>: <span id="transID" runat="server"></span>
                                </td>
                            </tr>
                            <tr class="<% if (fpx_debitAuthCode.CompareTo("00") != 0 && fpx_debitAuthCode.CompareTo("09") != 0 && fpx_debitAuthCode.CompareTo("99") != 0)
                                { %>hide<% } %>">
                                <th>Charges (MYR)
                                </th>
                                <td>: <span id="charges" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>: 
                                     <%if (fpx_debitAuthCode.CompareTo("00") == 0)
                                         {%>
                                    <%="Success"%>
                                    <%}
                                        else if (fpx_debitAuthCode.CompareTo("09") == 0)
                                        {%>
                                    <%="Pending Authorization"%>
                                    <%}
                                        else if (fpx_debitAuthCode.CompareTo("99") == 0)
                                        {%>
                                    <%="Pending Authorization"%>
                                    <%}
                                        else
                                        {%>
                                    <%="Failed"%>
                                    <span id="Span1" runat="server"></span>
                                    <%}%>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <table class="table <% if (fpx_sellerOrderNo.Contains("RS"))
                        { %>hide<% } %>"
                        id="fpxDetails">
                        <tbody>
                            <tr>
                                <td>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p id="txtStatus" runat="server" class="normal">Thank you for investment! </p>
                                                    <p class="normal"><b>TRANSACTION DETAILS</b></p>
                                                    <!-- Display details for Receipt -->
                                                    <table class="table no-border">
                                                        <tr>
                                                            <td class="text-left">Transaction Status</td>
                                                            <td>:</td>
                                                            <td class="text-left">
																<b>
																	<%if (fpx_debitAuthCode.CompareTo("00") == 0)
																		{%>
																	<%="Success"%>
																	<%}
																		else if (fpx_debitAuthCode.CompareTo("09") == 0)
																		{%>
																	<%="Pending Authorization"%>
																	<%}
																		else if (fpx_debitAuthCode.CompareTo("99") == 0)
																		{%>
																	<%="Pending Authorization"%>
																	<%}
																		else
																		{%>
																	<%="Failed"%>
																	<span id="fpxResponseCodeDesc" runat="server"></span>
																	<%}%>
																</b>
																<!--<strong>
																	Comparing Debit Auth Code and Credit Auth Code to cater SUCCESSFUL and UNSUCCESSFUL result 
																	<%=finalVerifiMsg%>
																</strong>-->
															</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">FPX Txn ID</td>
                                                            <td>:</td>
                                                            <td class="text-left"><%=fpx_fpxTxnId%></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Seller Order Number</td>
                                                            <td>:</td>
                                                            <td class="text-left"><%=fpx_sellerOrderNo%></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Buyer Bank</td>
                                                            <td>:</td>
                                                            <td class="text-left"><%=(b == null ? fpx_buyerBankId :  b.DisplayName)%></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Transaction Date</td>
                                                            <td>:</td>
                                                            <td class="text-left"><%=DateTime.ParseExact(fpx_fpxTxnTime, "yyyyMMddHHmmss", null).ToString("dd/MM/yyyy HH:mm")%></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Transaction Amount</td>
                                                            <td>:</td>
                                                            <td class="text-left">RM<%=fpx_txnAmount%></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" id="extraMsgForDevEx" runat="server"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- footer //-->
                        </tbody>
                    </table>

                    <p class="infoBelow hide">This parameter should be hidden from customer </p>
                    <table class="table hide">
                        <tr>
                            <td>
                                <table class="table">
                                    <tr>
                                        <td>1. Message From FPX:
                                            <textarea cols="80" rows="4"><%=fpx_checkSum%></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>2. Message From Here:</p>
                                            <textarea cols="80" rows="4"><%=xmlMessage%></textarea>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-right" style="margin-bottom: 10px;">
                    <asp:HiddenField ID="lblOrderType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="lblaccNo" runat="server" ClientIDMode="Static" />
                    <a href="javascript:;" onclick="ViewOrder();" class="btn btn-primary" style="margin-bottom: 5px;">View orders</a>
                    <a href="javascript:;" onclick="AnotherTransaction()" class="btn btn-primary" style="margin-bottom: 5px;">Click to do another transaction</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script>
        $(document).ready(function () {
        });
        function ViewOrder() {
            if ($('#lblOrderType').val() == '6') {
                window.location.href = "RSPList.aspx?type=" + $('#lblOrderType').val() + "&accNo=" + $('#lblaccNo').val();
            } else {
                window.location.href = "OrderList.aspx?type=" + $('#lblOrderType').val() + "&accNo=" + $('#lblaccNo').val();
            }

        }
        function AnotherTransaction() {
            if ($('#lblOrderType').val() == '6') {
                window.location.href = "RegularSavingsPlan.aspx?AccountNo=" + $('#lblaccNo').val();
            } else {
                var url = "BuyFunds.aspx?AccountNo=" + $('#lblaccNo').val();
                //var tcAcceptBuySession = sessionStorage['tcAcceptBuy'];
                //if (tcAcceptBuySession != undefined && tcAcceptBuySession != null && tcAcceptBuySession != "") {
                //    url += "&isTermsModalPopup=0";
                //}
                window.location.href = url;
            }
        }
    </script>
</asp:Content>
