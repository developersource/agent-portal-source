using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_types")]
    public class UserType
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_type_id"), Display("UserTypeId")]
        public Int32 UserTypeId { get; set; }
        [Database("is_verified"), Display("IsVerified")]
        public Int32 IsVerified { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public UserTypesDef UserTypeIdUserTypesDef { get; set; }
        public User UserIdUser { get; set; }
    }
}
