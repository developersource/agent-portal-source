using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UtmcMemberInvestmentService : IUtmcMemberInvestmentService
    {
        private static readonly Lazy<IUtmcMemberInvestmentRepo> lazy = new Lazy<IUtmcMemberInvestmentRepo>(() => new UtmcMemberInvestmentRepo());
        public static IUtmcMemberInvestmentRepo IUtmcMemberInvestmentRepo { get { return lazy.Value; } }

        private static readonly Lazy<IUtmcCompositionalTransactionRepo> lazyIUtmcCompositionalTransactionRepo = new Lazy<IUtmcCompositionalTransactionRepo>(() => new UtmcCompositionalTransactionRepo());
        public static IUtmcCompositionalTransactionRepo IUtmcCompositionalTransactionRepo { get { return lazyIUtmcCompositionalTransactionRepo.Value; } }

        private static readonly Lazy<ITransactionCodeRepo> lazyTransactionCodeRepo = new Lazy<ITransactionCodeRepo>(() => new TransactionCodeRepo());
        public static ITransactionCodeRepo ITransactionCodeRepo { get { return lazyTransactionCodeRepo.Value; } }

        public Response PostData(UtmcMemberInvestment obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcMemberInvestmentRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcMemberInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcMemberInvestmentRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcMemberInvestment obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcMemberInvestmentRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcMemberInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcMemberInvestmentRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcMemberInvestment DeleteData(Int32 Id)
        {
            UtmcMemberInvestment obj = new UtmcMemberInvestment();
            try
            {
                obj = IUtmcMemberInvestmentRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcMemberInvestmentRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcMemberInvestment obj = new UtmcMemberInvestment();
            try
            {
                response = IUtmcMemberInvestmentRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                response = IUtmcMemberInvestmentRepo.GetData(skip, take, isOrderByDesc);
                response.Data = ((List<UtmcMemberInvestment>)response.Data).Where(x => x.ReportDate == objs.Max(y => y.ReportDate)).OrderBy(x => x.IpdFundCode).ToList();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUtmcMemberInvestmentRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                response = IUtmcMemberInvestmentRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
                response.Data = ((List<UtmcMemberInvestment>)response.Data).Where(x => x.ReportDate == objs.Max(y => y.ReportDate)).OrderBy(x => x.IpdFundCode).ToList();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcMemberInvestmentRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                response = IUtmcMemberInvestmentRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
                response.Data = ((List<UtmcMemberInvestment>)response.Data).Where(x => x.ReportDate == objs.Max(y => y.ReportDate)).OrderBy(x => x.IpdFundCode).ToList();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcMemberInvestmentRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }


        //public List<UtmcDetailedMemberInvestment> GetMAAccountSummary(string propertyName, string propertyValue)
        //{
        //    List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
        //    Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
        //    if (responseUFIList.IsSuccess)
        //    {
        //        UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
        //    }

        //    List<UtmcDetailedMemberInvestment> objs = new List<UtmcDetailedMemberInvestment>();
        //    try
        //    {
        //        Response responseUMIList = IUtmcMemberInvestmentRepo.GetDataByPropertyName(propertyName, propertyValue, true, 0, 0, false);
        //        if (responseUMIList.IsSuccess)
        //        {
        //            List<UtmcMemberInvestment> utmcMemberInvestments = ((List<UtmcMemberInvestment>)responseUMIList.Data).OrderBy(x => x.IpdFundCode).ToList();
        //            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
        //            DateTime currentDate = DateTime.Now;
        //            DateTime yearsBefore = currentDate.AddYears(-6);
        //            StringBuilder filter = new StringBuilder();
        //            string columnNameIpdMemberAccNo = Converter.GetColumnNameByPropertyName<UtmcCompositionalTransaction>(nameof(UtmcCompositionalTransaction.IpdMemberAccNo));
        //            string columnNameReportDate = Converter.GetColumnNameByPropertyName<UtmcCompositionalTransaction>(nameof(UtmcCompositionalTransaction.ReportDate));
        //            filter.Append(" " + columnNameIpdMemberAccNo + " = '" + propertyValue + "'");
        //            filter.Append(" and " + columnNameReportDate + " > str_to_date('" + yearsBefore.ToString("yyyy-MM-dd") + "', '%Y-%m-%d')");
        //            Response responseUCTList = IUtmcCompositionalTransactionRepo.GetDataByFilter(filter + "", 0, 0, false);
        //            if (responseUCTList.IsSuccess)
        //            {
        //                utmcCompositionalTransactions = (List<UtmcCompositionalTransaction>)responseUCTList.Data;
        //                //IUtmcCompositionalTransactionRepo.GetDataByPropertyName(propertyName, propertyValue, true, 0, 0, false);
        //                //utmcCompositionalTransactions.ForEach(x=> {
        //                //    x.TransactionCode = ITransactionCodeRepo.GetDataByPropertyName(nameof(TransactionCode.Code), x.TransactionCode, true, 0, 0, false).FirstOrDefault().Name;
        //                //});

        //                foreach (string fundCode in utmcMemberInvestments.Select(x => x.IpdFundCode).Distinct().ToList())
        //                {
        //                    UtmcDetailedMemberInvestment utmcDetailedMemberInvestment = new UtmcDetailedMemberInvestment
        //                    {
        //                        utmcMemberInvestments = utmcMemberInvestments.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.EffectiveDate).ToList(),
        //                        utmcCompositionalTransactions = utmcCompositionalTransactions.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.DateOfTransaction).ToList(),
        //                        //utmcMemberInvestments = utmcMemberInvestments.Where(x => x.ReportDate == utmcMemberInvestments.Max(y => y.ReportDate)).Where(x=>x.IpdFundCode == fundCode).ToList(),


        //                        fundDetailedInformation = GlobalProperties.FundDetailedInformations.Where(x => x.UtmcFundInformation.IpdFundCode == fundCode).FirstOrDefault()
        //                    };
        //                    objs.Add(utmcDetailedMemberInvestment);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Write(ex.ToString());
        //    }
        //    return objs;
        //}
        //public List<UtmcMemberInvestment> GetDataByPaging(int take, int skip, out int filteredResultsCount, out int totalResultsCount)
        //{
        //    totalResultsCount = filteredResultsCount = 0;
        //    List<UtmcMemberInvestment> list = new List<UtmcMemberInvestment>();
        //    try
        //    {
        //        totalResultsCount = GetData().Count();
        //        if (take == -1)
        //        {
        //            take = totalResultsCount;
        //        }
        //        list = GetData().Skip(skip).Take(take).ToList();
        //        filteredResultsCount = list.Count();
        //        return list;
        //    }
        //    catch(Exception ex)
        //    {
        //        Console.Write(ex.ToString());
        //    }
        //    return list;
        //}

    }
}
