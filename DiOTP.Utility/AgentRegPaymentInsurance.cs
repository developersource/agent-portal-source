﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_payment_insurance")]
    public class AgentRegPaymentInsurance
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_reg_id"), Display("agent_reg_id")]
        public Int32 agent_reg_id { get; set; }
        [Database("insurance_beneficiary_name"), Display("insurance_beneficiary_name")]
        public String insurance_beneficiary_name { get; set; }
        [Database("insurance_beneficiary_NRIC"), Display("insurance_beneficiary_NRIC")]
        public String insurance_beneficiary_NRIC { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32? created_by { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
        public List<AgentRegBank> AgentRegBanks { get; set; }

    }
}
