﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddBank.aspx.cs" Inherits="Admin.AddBank" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <div id="addBankForm" runat="server">
        <div class="row" id="FormId" data-value="addBankForm">
            <div class="col-md-12">
                    <div class="row row mb-6 hide">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">ISIN</label>
                        </div>  
                        <div class="col-md-8">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                        
                            <input type="text" name="ISIN" id="ISIN" runat="server" clientidmode="static" class="form-control" placeholder="Enter ISIN" />                        
                        </div>
                    </div>

                    <div class="row mb-6 hide">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Code:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Code" id="Code" runat="server" clientidmode="static" class="form-control" placeholder="Enter Code" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Short Name:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="ShortName" id="ShortName" runat="server" clientidmode="static" class="form-control" placeholder="Enter ShortName" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Name:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Name" id="Name" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        </div>
                    </div>

                    <%--<div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">No of Format:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="NoFormat" id="NoFormat" runat="server" clientidmode="static" class="form-control" placeholder="Enter No of Format" />
                        </div>
                    </div>--%>
                </div>
            </div>
        </div>
</body>
</html>
