using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UserService : IUserService
    {
        private static readonly Lazy<IUserRepo> lazy = new Lazy<IUserRepo>(() => new UserRepo());
        public static IUserRepo IUserRepo { get { return lazy.Value; } }

        private static readonly Lazy<IUserTypeRepo> lazyIUserTypeRepo = new Lazy<IUserTypeRepo>(() => new UserTypeRepo());
        public static IUserTypeRepo IUserTypeRepo { get { return lazyIUserTypeRepo.Value; } }

        private static readonly Lazy<IUserAccountRepo> lazyIUserAccountRepo = new Lazy<IUserAccountRepo>(() => new UserAccountRepo());
        public static IUserAccountRepo IUserAccountRepo { get { return lazyIUserAccountRepo.Value; } }

        private static readonly Lazy<IUserSecurityRepo> lazyIUserSecurityRepo = new Lazy<IUserSecurityRepo>(() => new UserSecurityRepo());
        public static IUserSecurityRepo IUserSecurityRepo { get { return lazyIUserSecurityRepo.Value; } }

        public Response PostData(User user)
        {
            Response response = new Response();
            try
            {

                string siteURL = ConfigurationManager.AppSettings["siteURL"];
                if (user.Username == "" || user.Username == null)
                {
                    user.Username = user.EmailId.Split('@')[0];
                }
                //Response responseUsernameMatches = IUserRepo.GetDataByFilter(" username like '" + user.Username + "%'", 0, 0, false);
                //if (responseUsernameMatches.IsSuccess)
                //{
                //    List<User> usernameMatches = (List<User>)responseUsernameMatches.Data;
                //    user.Username = user.Username + (usernameMatches.Count == 0 ? "" : usernameMatches.Count.ToString());
                //}
                user.UniqueKey = CustomGenerator.GenerateGoogleAuthenticationUniqueKeyForEachUser(user.Username);
                if (user.Password != "")
                {
                    user.Password = CustomEncryptorDecryptor.EncryptPassword(user.Password);
                    user.TransPwd = user.Password;
                }
                if (user.UserRoleId == 0)
                    user.UserRoleId = 3;
                user.CreatedDate = DateTime.Now;

                //String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                string toMobileNumber = user.MobileNumber;
                //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                //string result = "";
                //result = SMSService.SendSMS(toMobileNumber, mobileVerificationPin);

                List<UserAccount> uAs = user.UserIdUserAccounts;
                var unique_code = CustomGenerator.GenerateSixDigitPin();
                user.EmailCode = unique_code;
                response = IUserRepo.PostData(user);
                if (response.IsSuccess)
                {
                    user = (User)response.Data;
                    IUserTypeRepo.PostData(new UserType
                    {
                        UserId = user.Id,
                        UserTypeId = 1,
                        IsVerified = user.Password == "" ? 0 : 1,
                        Status = 1
                    });

                    Email email = new Email();
                    email.user = user;
                    if (user.UserRoleId == 2)
                        siteURL = siteURL + "Admin/";
                    email.link = siteURL + "ActivateAccount.aspx";
                    if (user.CreatedBy == 0 || user.Password == "")
                        EmailService.SendVerificationLink(email, unique_code);

                    IUserSecurityRepo.PostData(new UserSecurity
                    {
                        CreatedDate = DateTime.Now,
                        IsVerified = user.Password == "" ? 0 : 1,
                        Status = 1,
                        UserId = user.Id,
                        Value = user.EmailId,
                        UserSecurityTypeId = 1,
                        VerificationPin = 0
                    });

                    IUserSecurityRepo.PostData(new UserSecurity
                    {
                        CreatedDate = DateTime.Now,
                        IsVerified = user.IsSecurityChecked == 1 ? 1 : 0,
                        Status = 1,
                        UserId = user.Id,
                        Value = user.MobileNumber,
                        UserSecurityTypeId = 2,
                        VerificationPin = 0
                    });
                    //int idx = 0;
                    uAs.ForEach(uA =>
                    {
                        uA.VerificationCode = 0;
                        uA.UserId = user.Id;
                        //if (idx == 0)
                        //    uA.IsPrimary = 1;
                        //idx++;
                        Response resUA = IUserAccountRepo.PostData(uA);
                        if (resUA.IsSuccess) {
                            UserAccount userAccount = (UserAccount)resUA.Data;
                            uA.Id = userAccount.Id;
                        }
                    });
                    user.UserIdUserAccounts = uAs;
                    //String propNameHolderNo = nameof(MaHolderReg.HolderNo);
                    response.Data = user;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public Response PostDataAdmin(User user)
        {
            Response response = new Response();
            try
            {

                string siteURL = ConfigurationManager.AppSettings["siteURL"];
                if (user.Username == "" || user.Username == null)
                {
                    user.Username = user.EmailId.Split('@')[0];
                }
                user.UniqueKey = CustomGenerator.GenerateGoogleAuthenticationUniqueKeyForEachUser(user.Username);
                if (user.Password != "")
                {
                    user.Password = CustomEncryptorDecryptor.EncryptPassword(user.Password);
                    user.TransPwd = user.Password;
                }
                if (user.UserRoleId == 0)
                    user.UserRoleId = 3;
                user.CreatedBy = user.CreatedBy;
                user.CreatedDate = DateTime.Now;

                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                string toMobileNumber = user.MobileNumber;
                //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                //string result = "";
                //result = SMSService.SendSMS(toMobileNumber, mobileVerificationPin);

                List<UserAccount> uAs = user.UserIdUserAccounts;
                var unique_code = CustomGenerator.GenerateSixDigitPin();
                user.EmailCode = unique_code;
                response = IUserRepo.PostData(user);
                user = (User)response.Data;

                Email email = new Email();
                email.user = user;
                if (user.UserRoleId == 2)
                    siteURL = siteURL + "Admin/";
                email.link = siteURL + "SetAdminPassword.aspx";
                EmailService.SendVerificationLink(email, unique_code);

                IUserSecurityRepo.PostData(new UserSecurity
                {
                    CreatedDate = DateTime.Now,
                    IsVerified = 0,
                    Status = 1,
                    UserId = user.Id,
                    Value = user.EmailId,
                    UserSecurityTypeId = 1,
                    VerificationPin = 0
                });

                IUserSecurityRepo.PostData(new UserSecurity
                {
                    CreatedDate = DateTime.Now,
                    IsVerified = 0,
                    Status = 1,
                    UserId = user.Id,
                    Value = user.MobileNumber,
                    UserSecurityTypeId = 2,
                    VerificationPin = 0
                });

                //IUserAccountRepo.PostBulkData(uAs);

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public Int32 PostBulkData(List<User> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUserRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(User obj)
        {
            Response response = new Response();
            try
            {
                if (obj.UserRoleId == 2)
                {
                    Response responseGet = IUserRepo.GetSingle(obj.Id);
                    if (responseGet.IsSuccess)
                    {
                        User user = (User)responseGet.Data;
                        if(user.EmailId != obj.EmailId)
                        {
                            var unique_code = CustomGenerator.GenerateSixDigitPin();
                            user.EmailCode = unique_code;
                            IUserRepo.UpdateData(user);
                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                            Email email = new Email();
                            email.user = obj;
                            if (obj.UserRoleId == 2)
                                siteURL = siteURL + "Admin/";
                            email.link = siteURL + "SetAdminPassword.aspx";
                            EmailService.SendVerificationLink(email, unique_code);
                        }
                    }
                }
                response = IUserRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<User> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUserRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public User DeleteData(Int32 Id)
        {
            User obj = new User();
            try
            {
                obj = IUserRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUserRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            User obj = new User();
            try
            {
                response = IUserRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                response = IUserRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUserRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                response = IUserRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUserRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                response = IUserRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUserRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }

        public Response GetDataByMA(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                response = IUserRepo.GetDataByMA(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByMA(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUserRepo.GetCountByMA(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }

        public Response GetDataByUT(string filter, int skip, int take, bool isOrderByDesc, string jfilter, string jtable)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                response = IUserRepo.GetDataByUT(filter, skip, take, isOrderByDesc, jfilter, jtable);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByUT(string filter, string jfilter, string jtable)
        {
            Int32 count = 0;
            try
            {
                count = IUserRepo.GetCountByUT(filter, jfilter, jtable);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
