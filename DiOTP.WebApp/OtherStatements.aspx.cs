﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class OtherStatements : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        public List<UserAccount> userAccounts = new List<UserAccount>();
        public static List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (!IsPostBack)
                {
                    if (Session["user"] != null)
                    {
                        User user = (User)Session["user"];
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            userAccounts = (List<UserAccount>)responseUAList.Data;
                            string all = String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToArray());
                            ddlFundAccount.Items.Add(new ListItem("All Accounts", all));
                            
                            foreach (UserAccount userAccount in userAccounts)
                            {
                                ddlFundAccount.Items.Add(new ListItem(CustomValues.GetAccounPlan(userAccount.HolderClass) + " - " + userAccount.AccountNo, userAccount.AccountNo));
                            }
                            
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }

                        DateTime now = DateTime.Now;
                        DateTime lastYears = now.AddMonths(-12);
                        ddlFromMonth.Items.Add(new ListItem { Text = "Select", Value = "" });
                        ddlToMonth.Items.Add(new ListItem { Text = "Select", Value = "" });
                        while (lastYears < now)
                        {
                            ddlFromMonth.Items.Add(new ListItem { Text = CustomValues.GetMonthNameByNumber(lastYears.Month) + ", " + lastYears.Year, Value = lastYears.Year + "_" + lastYears.Month });
                            ddlToMonth.Items.Add(new ListItem { Text = CustomValues.GetMonthNameByNumber(lastYears.Month) + ", " + lastYears.Year, Value = lastYears.Year + "_" + lastYears.Month });
                            lastYears = lastYears.AddMonths(1);
                        }

                        List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
                        userAccounts.ForEach(uA =>
                        {
                            List<HolderInv> holderInvs = new List<HolderInv>();

                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                            MaHolderReg maHolderReg = new MaHolderReg();
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;

                                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                                if (responseUFIList.IsSuccess)
                                {
                                    UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                                }

                                holderInvs = ServicesManager.GetHolderInvByHolderNo(uA.AccountNo, UTMCFundInformations);
                                holderInvs.ForEach(x =>
                                {
                                    Response responseUFI = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code = '" + x.FundId + "' ", 0, 0, true);
                                    if (responseUFI.IsSuccess)
                                    {
                                        List<UtmcFundInformation> Funds = (List<UtmcFundInformation>)responseUFI.Data;
                                        if (Funds.Count > 0)
                                        {
                                            UtmcFundInformation Fund = Funds.FirstOrDefault();
                                            Fund.SatGroup = uA.AccountNo;
                                            Int32 fundId = Fund.Id;
                                            if (x.CurrUnitHldg > 0)
                                            {
                                                if (UTMCFundInformations.Count > 0)
                                                {
                                                    if (UTMCFundInformations.Where(y => y.Id == fundId).Count() == 0)
                                                        UTMCFundInformations.Add(Fund);
                                                    else
                                                        UTMCFundInformations.Add(Fund);
                                                }
                                                else
                                                    UTMCFundInformations.Add(Fund);

                                            }
                                        }
                                    }

                                });
                            }
                        });
                        UTMCFundInformations = UTMCFundInformations.GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
                        ddlFundList.Items.Add(new ListItem("All", "all"));
                        UTMCFundInformations.ForEach(x =>
                        {
                            ddlFundList.Items.Add(new ListItem(x.FundName.Capitalize(), x.IpdFundCode));
                        });
                        string type = "all";
                        if (!String.IsNullOrEmpty(Request.QueryString["type"]))
                        {
                            type = Request.QueryString["type"].ToString();
                        }

                        ddlStatementType.SelectedValue = type;
                        spanUsername.InnerHtml = user.Username;
                        statementTypeBC.InnerHtml = (type == "ID" ? "Income Distribution" : (type == "US" ? "Unit Split" : "-"));
                        statementTypeH.InnerHtml = (type == "ID" ? "Income Distribution" : (type == "US" ? "Unit Split" : "-"));
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void BindStatements(string type)
        {
            User user = (User)Session["user"];


            if (ddlFromMonth.SelectedValue == "" || ddlToMonth.SelectedValue == "")
            {
                if (ddlFromMonth.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select a Month(From)', '');", true);
                }
                else if (ddlToMonth.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select a Month(To)', '');", true);
                }
            }
            else
            {
                DateTime startMonth = (ddlFromMonth.SelectedValue != "" ? new DateTime(Convert.ToInt32(ddlFromMonth.SelectedValue.Split('_')[0]), Convert.ToInt32(ddlFromMonth.SelectedValue.Split('_')[1]), 1) : default(DateTime));
                DateTime endMonth = (ddlToMonth.SelectedValue != "" ? new DateTime(Convert.ToInt32(ddlToMonth.SelectedValue.Split('_')[0]), Convert.ToInt32(ddlToMonth.SelectedValue.Split('_')[1]), 1).AddMonths(1) : default(DateTime));

                if (ddlStatementType.SelectedValue.ToString() == "1" && (startMonth >= endMonth))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Month(From) cannot be greater than Month(To)', '');", true);
                }
                else
                {


                    string fundid = ddlFundList.SelectedValue;

                    string dirPath = HttpContext.Current.Server.MapPath("/Content/Statements/Others/");
                    List<FileInfo> fileInfosList = new List<FileInfo>();
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' " + (ddlFundAccount.SelectedItem.Text.ToLower() != "all" ? " and account_no in (" + ddlFundAccount.SelectedValue + ") " : "") + " and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        userAccounts = (List<UserAccount>)responseUAList.Data;
                    }

                    DateTime lastyears = DateTime.Now.AddYears(-10);

                    List<string> monthYears = new List<string>();
                    while (startMonth < endMonth)
                    {
                        monthYears.Add(startMonth.Year + "" + startMonth.Month.ToString().PadLeft(2, '0'));
                        startMonth = startMonth.AddMonths(1);
                    }

                    userAccounts.ForEach(x =>
                    {
                        if (type == "all")
                        {
                            if (Directory.Exists(dirPath + "/" + x.AccountNo))
                            {
                                var dirInfosub = new DirectoryInfo(dirPath + "/" + x.AccountNo);
                                var ydirs = dirInfosub.GetDirectories().Where(y => monthYears.Contains(y.Name.Substring(0, 6))).ToList();
                                ydirs.ForEach(z =>
                                {
                                    fileInfosList.AddRange(z.GetFiles(x.AccountNo + "_*_ID_*", SearchOption.AllDirectories).ToList());
                                    fileInfosList.AddRange(z.GetFiles(x.AccountNo + "_*_US_*", SearchOption.AllDirectories).ToList());
                                });
                            }
                        }
                        else
                        {
                            if (Directory.Exists(dirPath + "/" + x.AccountNo))
                            {
                                var dirInfosub = new DirectoryInfo(dirPath + "/" + x.AccountNo);
                                var ydirs = dirInfosub.GetDirectories().Where(y => monthYears.Contains(y.Name.Substring(0, 6))).ToList();
                                ydirs.ForEach(z =>
                                {
                                    fileInfosList.AddRange(z.GetFiles(x.AccountNo + "_*_" + type.ToUpper() + "_*", SearchOption.AllDirectories).ToList());
                                });
                            }
                        }
                    });

                    List<UtmcFundInformation> UTMCFundInformations1 = new List<UtmcFundInformation>();
                    userAccounts.ForEach(uA =>
                    {
                        List<HolderInv> holderInvs = new List<HolderInv>();

                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;
                            Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                            if (responseUFIList.IsSuccess)
                            {
                                UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                            }
                            holderInvs = ServicesManager.GetHolderInvByHolderNo(uA.AccountNo, UTMCFundInformations);
                            holderInvs.ForEach(x =>
                            {
                                Response responseUFI = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code = '" + x.FundId + "' ", 0, 0, true);
                                if (responseUFI.IsSuccess)
                                {
                                    List<UtmcFundInformation> Funds = (List<UtmcFundInformation>)responseUFI.Data;
                                    if (Funds.Count > 0)
                                    {
                                        UtmcFundInformation Fund = Funds.FirstOrDefault();
                                        Fund.SatGroup = uA.AccountNo;
                                        Int32 fundId = Fund.Id;
                                        if (x.CurrUnitHldg > 0)
                                        {
                                            if (UTMCFundInformations1.Count > 0)
                                            {
                                                if (UTMCFundInformations1.Where(y => y.Id == fundId).Count() == 0)
                                                    UTMCFundInformations1.Add(Fund);
                                                else
                                                    UTMCFundInformations1.Add(Fund);
                                            }
                                            else
                                                UTMCFundInformations1.Add(Fund);

                                        }
                                    }
                                }

                            });
                        }
                    });

                    UTMCFundInformations1.ForEach(x =>
                    {
                        if (fundid != "all")
                        {
                            fileInfosList = fileInfosList.Where(y => y.Name.Contains("_" + fundid + "_")).ToList();
                        }
                    });


                    fileInfosList = fileInfosList.Where(x => monthYears.Contains(x.Name.Split('_')[3].Substring(0, 6)))
                        .OrderByDescending(f => DateTime.ParseExact(f.Name.Replace(".PDF", "").Replace(".pdf", "").Split('_')[3], "yyyyMMdd", CultureInfo.InvariantCulture))
                        .ToList();
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    fileInfosList.ForEach(x =>
                            {
                                string fileName = x.Name.Replace(".PDF", "").Replace(".pdf", "");
                                string[] keys = fileName.Split('_');

                                string accNo = keys[0];
                                string fundId = keys[1].ToUpper();
                                string statementType = keys[2].ToUpper();
                                string statementDate = keys[3].ToUpper();
                                string statementTag = "";
                                if (keys.Length == 5)
                                    statementTag = keys[4].ToUpper();

                                UtmcFundInformation utmcFundInformation = UTMCFundInformations.FirstOrDefault(y => y.IpdFundCode == fundId);

                                DateTime date;
                                bool isDate = DateTime.TryParseExact(statementDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

                                asb.Append(@"<tr>
                                            <td>" + index + @"</td>
                                            <td>" + accNo + @"</td>
                                            <td>" + (utmcFundInformation == null ? fundId : utmcFundInformation.FundName.Capitalize()) + @"</td>
                                            <td>" + (isDate ? date.ToString("dd/MM/yyyy") : "-") + @"</td>
                                            <td>" + (statementType == "ID" ? "Income Distribution " + (statementTag == "C" ? "(Letter)" : (statementTag == "D" ? "(Statement)" : "")) + "" : ((statementType == "US" ? "Unit Split " + (statementTag == "C" ? "(Letter)" : (statementTag == "D" ? "(Statement)" : "")) + "" : "-"))) + @"</td>
                                            <td><a href='/Content/Statements/Others/" + accNo + "/" + statementDate + "/" + x.Name + "' download>" + x.Name + @"</a></td>
                                        </tr>
                                    ");
                                index++;

                            });
                    tbodyStatementDownload.InnerHtml = asb.ToString();
                }
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            BindStatements(ddlStatementType.SelectedValue);
        }
    }
}