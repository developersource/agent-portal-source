﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="OrderConfirmation.aspx.cs" Inherits="DiOTP.WebApp.Invoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section invoice-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">Home</a></li>
                            <%--<li><a href="/Portfolio.aspx">MyApex</a></li>--%>
                            <li class="active">Order confirmation</li>
                        </ol>
                    </div>
                    <div class="text-left">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="invoice-title">
                        <h3 class="mb-0 pb-0 d-ib mob-fund-head tab-fund-head">Order details <small>(<span id="orderstatuslabel" runat="server"></span>)</small></h3>
                        <h3 class="mb-0 pb-0 d-ib mob-fund-head tab-fund-head pull-right">Order # <span id="orderNoLabel" runat="server">12345</span></h3>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Billed To:</strong><br />
                                <span id="orderByUserLabel" runat="server">Sachin</span><br />
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Account Plan:</strong><br />
                                <span id="accountplan" runat="server">epf</span>
                            </address>
                        </div>
                        <div class="col-xs-6 text-right" id="agentIdDiv" runat="server">
                            <address>
                                <strong>Servicing Agent:</strong><br />
                                <span id="agentID" runat="server">AG000001</span>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Payment Type:</strong><br />
                                <span id="orderPaymentMethodLabel" runat="server">Net Banking</span>
                            </address>
                        </div>
                        <div class="col-xs-6 text-right">
                            <address>
                                <strong>Order Date:</strong><br />
                                <span id="orderDateLabel" runat="server">May 25, 2018</span>
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Order summary</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th><strong>Fund</strong></th>
                                            <th class="text-right"><strong id="transType" runat="server">Investment</strong></th>
                                            <th class="text-right" id="thFees" runat="server"><strong>Fee
                                                <br />
                                                %</strong></th>
                                            <th class="text-right"><strong>Total
                                                <br />
                                                <asp:Label ID="txtUnit" runat="server"></asp:Label></strong></th>
                                        </tr>
                                    </thead>
                                    <tbody id="ordersTbody" runat="server">
                                        <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                        <tr>
                                            <td>BS-200</td>
                                            <td class="text-center">$10.99</td>
                                            <td class="text-right">$1</td>
                                            <td class="text-right">$11.99</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td id="tFootTD" runat="server" colspan="2" class="no-line"></td>
                                            <td class="no-line text-right"><strong>Total</strong></td>
                                            <td class="no-line text-right" id="orderTotalLabel" runat="server">$11.99</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script>
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
        }
        FormatAllUnit();
    </script>
</asp:Content>
