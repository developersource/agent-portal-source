﻿using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.OracleDTOs;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class UtmcMemberInvestmentLiveData
    {
        static DataManager DB = new DataManager();

        public static List<UtmcMemberInvestment> GetData(string holderNo, string startDate, string endDate, string fundIds)
        {
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                UTMC15NEW utmc = new UTMC15NEW();
                objs = utmc.Read_MemberInvestment(holderNo, startDate, endDate, fundIds);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return objs;
        }


        public static List<UtmcMemberInvestment> GetDataByHolderNo(string holderNo)
        {
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                string query = "select S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO,"
                                + " S1.TRANS_UNITS,S1.TRANS_AMT, S4.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO,"
                                + " s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR, (S4.UP_SELL * s1.CUR_UNIT_HLDG) AS MARKET_VALUE,"
                                + " s1.TRANS_NO, s1.ACC_TYPE, (s1.TRANS_UNITS * s1.TRANS_PR) as SMALL, s1.ENTRY_DT, s1.CUM_VALUE_HLDG "
                                + " from UTS.HOLDER_LEDGER s1 inner join  UTS.HOLDER_REG s3 on "
                                + "  s1.HOLDER_NO = s3.HOLDER_NO "
                                + " INNER JOIN UTS.PRICE_HISTORY S4 ON S1.FUND_ID = S4.FUND_ID "
                                + " AND S4.PR_STATUS = 'C' INNER JOIN "    //ADJUSTMENT ONLY COMPLETE STATUS FOR PRICE HISTORY
                                + " (   select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID,    HOLDER_NO "
                                + "  from UTS.HOLDER_LEDGER "
                                + "  WHERE "
                                + "  HOLDER_NO IN ('" + holderNo + "')  "  //adjusment
                                + "  group by HOLDER_NO,FUND_ID  "
                                + "  ) s2 "
                                + " on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ "  // adjustment .. AND s1.TRANS_DT = s2.TRANS_DT"
                                + " where s1.CUR_UNIT_HLDG > 0 "
                                + " ORDER BY S1.FUND_ID, S1.HOLDER_NO, S4.TRANS_DT desc";
                objs = GenerateData(query);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return objs;
        }

        public static List<UtmcMemberInvestment> GenerateData(string Query)
        {
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            DataTable ds = new DataTable();
            string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"].ToString();

            string ConnectionString = DB.GetOracleConnection();
            try
            {
                string mysql = Query;
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string IPD_Fund_Code = "";
                                string IPD_Member_Acc_No = "";

                                string Member_EPF_No = rdr[0].ToString();

                                IPD_Fund_Code = rdr[1].ToString();
                                IPD_Member_Acc_No = rdr[2].ToString();
                                string Actual_Transferred_From_EPF_RM = rdr[4].ToString();
                                string Units = rdr[6].ToString();
                                string Book_Value = rdr[4].ToString();
                                string Market_Value = rdr[11].ToString();


                                // adjustment 2016 09


                                DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                                string TransDate = dt.ToString("yyyy-MM-dd");

                                DateTime now = Convert.ToDateTime(rdr[5].ToString());
                                var startDate = new DateTime(now.Year, now.Month, 1);
                                var endDate = DateTime.Now;


                                if (endDate.ToString("yyyy/MM/dd") == "2016/07/31")
                                {
                                    if (IPD_Fund_Code == "06")
                                    {
                                        decimal Market_Value_TT = 0.00M;
                                        decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                        Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                        Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                        Market_Value = Convert.ToString(Market_Value_TT.ToString());
                                    }
                                }

                                //string Effective_Date = End_date;

                                // Query = @"Insert into utmc_member_investment
                                // (Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,
                                // Book_Value,Market_Value,Effective_Date,Report_Date) "
                                //+ @" values('" +
                                // Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + @"',
                                // '" + Book_Value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";


                                objs.Add(new UtmcMemberInvestment()
                                {
                                    MemberEpfNo = Member_EPF_No,
                                    EpfIpdCode = EPF_IPD_Code,
                                    IpdFundCode = IPD_Fund_Code,
                                    IpdMemberAccNo = IPD_Member_Acc_No,
                                    ActualTransferredFromEpfRm = Convert.ToDecimal(Actual_Transferred_From_EPF_RM == "" ? "0" : Actual_Transferred_From_EPF_RM),
                                    Units = Convert.ToDecimal(Units == "" ? "0" : Units),
                                    BookValue = Convert.ToDecimal(Book_Value == "" ? "0" : Book_Value),
                                    MarketValue = Convert.ToDecimal(Market_Value == "" ? "0" : Market_Value),
                                    EffectiveDate = endDate,
                                    ReportDate = endDate,
                                    //ActualCost = Actual_Cost,
                                    //ActualTransferredFromEpfRm = Actual_transfer_value,
                                    //BookValue = Book_value,
                                    //RedemptionCost = UnitCostRDfinal,
                                    TransString = rdr[12].ToString(),
                                    TransType = rdr[9].ToString().Trim(),
                                    AccType = rdr[13].ToString(),
                                    EntryDt = rdr[15].ToString(),
                                    TransDt = rdr[5].ToString(),
                                    TransUnits = rdr[3].ToString(),
                                    TransAmt = rdr[4].ToString(),
                                    TransUnitsXTransPR = rdr[14].ToString(),
                                    CurUnitHldg = rdr[6].ToString(),
                                    CumValueHldg = rdr[16].ToString(),
                                });

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
            }

            decimal Actual_transfer_value = 0.0000M;
            decimal Book_value = 0.0000M;
            decimal Unit_Holding_value = 0.0000M;
            string Trans_Type = "";
            decimal Total_Redemption_cost = 0.0000M;
            decimal UnitCostRD = 0.0000M;
            decimal UnitCostRDfinal = 0.0000M;
            string AccountType = "";
            decimal Actual_Cost = 0.0000M;

            int Count = 0; string[] numb;
            List<UtmcMemberInvestment> objsNew = objs.GroupBy(x => x.TransString).Select(grp => grp.FirstOrDefault()).ToList();
            numb = new string[objsNew.Count];
            //numb = new string[objs.GroupBy(x=>x.TransString).Where(grp=>grp.FirstOrDefault()).Count];
            objsNew.ForEach(obj =>
            {
                numb[Count++] = obj.TransString;
            });
            Array.Resize(ref numb, Count);
            Count = 0;
            string Trans_String;
            objs.ForEach(obj =>
                {
                    Trans_String = obj.TransString;
                    AccountType = obj.AccType;
                    bool TTT = false;

                    // check reversal
                    for (int i = Count; i < numb.Length; i++)
                    {
                        if (Trans_String + "X" == numb[i].ToString())
                        {
                            // rdr.NextResult();
                            TTT = true;
                            break;

                        }

                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                        if (Check_reversed == 'X')
                        {
                            TTT = true;

                            Unit_Holding_value = Convert.ToDecimal(obj.CurUnitHldg);
                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                            UnitCostRD = Convert.ToDecimal(obj.TransUnits);
                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                            break;
                        }

                    }

                    Count++;

                    if (TTT == false)
                    {
                        Trans_Type = obj.TransType.Trim();
                        if (Trans_Type == "SA")
                        {
                            string Trans_no = obj.TransString;
                            char Check_reversed = Trans_no[Trans_no.Length - 1];
                            if (Check_reversed == 'X')
                            {
                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                Actual_transfer_value -= Convert.ToDecimal(obj.TransAmt);
                                Book_value -= Convert.ToDecimal(obj.TransUnitsXTransPR);
                                Book_value = decimal.Round(Book_value, 2);
                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);
                                // new actual transfer
                                if (!(AccountType == "SW"))
                                {
                                    Actual_Cost -= Convert.ToDecimal(obj.TransAmt);
                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                }
                            }
                            else
                            {
                                Actual_transfer_value += Convert.ToDecimal(obj.TransAmt);
                                Book_value += Convert.ToDecimal(obj.TransUnitsXTransPR);
                                Book_value = decimal.Round(Book_value, 2);
                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);
                                // new actual transfer
                                if (!(AccountType == "SW"))
                                {
                                    Actual_Cost += Convert.ToDecimal(obj.TransAmt);
                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                }
                            }
                        }
                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                        {
                            Unit_Holding_value = Convert.ToDecimal(obj.CurUnitHldg);
                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);
                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))
                            {
                                Book_value += Convert.ToDecimal(obj.TransUnitsXTransPR);
                                // rounding
                                Book_value = decimal.Round(Book_value, 2);
                            }
                        }
                        if (Trans_Type == "RD" || Trans_Type == "TR")
                        {
                            string Trans_noRD = obj.TransString;
                            char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];
                            Total_Redemption_cost = Convert.ToDecimal(obj.CurUnitHldg);
                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                            UnitCostRD = Convert.ToDecimal(obj.TransUnits);
                            UnitCostRD = decimal.Round(UnitCostRD, 4);
                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                * UnitCostRD);
                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);
                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                            {
                                if (!(Check_reversedRD == 'X'))                             // Check reversal
                                {
                                    Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                               * Total_Redemption_cost);
                                    Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                              * Total_Redemption_cost);
                                    Book_value = decimal.Round(Book_value, 2);
                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);
                                    // new actual transfer
                                    if (!(AccountType == "SW"))
                                    {
                                        Actual_Cost -= Convert.ToDecimal(obj.TransAmt);
                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                    }
                                }
                                else  // reversal for RD and TR
                                {
                                    // new actual transfer
                                    if (!(AccountType == "SW"))
                                    {
                                        Actual_Cost += Convert.ToDecimal(obj.TransAmt);
                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                    }
                                }
                            }
                            else
                            {
                                Actual_transfer_value = 0.0000M;
                                Book_value = 0.0000M;
                                //UnitCostRDfinal = 0.0000M;
                                Unit_Holding_value = 0.0000M;
                                // new actual transfer
                                if (!(AccountType == "SW"))
                                {
                                    Actual_Cost -= Convert.ToDecimal(obj.TransAmt);
                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                }
                            }
                        }
                    }
                    objs.ForEach(x => {
                        if (x.IpdFundCode == obj.IpdFundCode && x.IpdMemberAccNo == obj.IpdMemberAccNo && x.ReportDate == obj.ReportDate) {
                            x.ActualCost = Actual_Cost;
                            x.ActualTransferredFromEpfRm = Actual_transfer_value;
                            x.BookValue = Book_value;
                            x.RedemptionCost = UnitCostRDfinal;
                        }
                    });
                    //obj.ActualCost = Actual_Cost;
                    //obj.ActualTransferredFromEpfRm = Actual_transfer_value;
                    //obj.BookValue = Book_value;
                    //obj.RedemptionCost = UnitCostRDfinal;
                });



            //List<UtmcMemberInvestment> objsNew = ReadHolderNoforutmc15(objs);
            return objs;

        }

        protected static List<UtmcMemberInvestment> ReadHolderNoforutmc15(List<UtmcMemberInvestment> objs)
        {
            List<UtmcMemberInvestment> objsNew = new List<UtmcMemberInvestment>();
            objsNew = objs;
            try
            {
                foreach (UtmcMemberInvestment u in objs)
                {

                    string EPF_No = u.MemberEpfNo;
                    string IPD_fund_Code = u.IpdFundCode;
                    objsNew = Get_Redumptiondate(EPF_No, IPD_fund_Code, objsNew);
                }

            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read Holder No From utmc_member_investment failed--- timeout", "");
            }
            return objsNew;
        }

        protected static List<UtmcMemberInvestment> Get_Redumptiondate(string Holder_No, string Fund_ID, List<UtmcMemberInvestment> objs, bool IsInsert = false)
        {
            List<UtmcMemberInvestment> objsNew = new List<UtmcMemberInvestment>();
            objsNew = objs;
            string Sqlconnstring = DB.GetOracleConnection();
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                string AccountType = "";
                decimal Actual_Cost = 0.0000M;



                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                 + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                 + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG "
                                 + " FROM UTS.HOLDER_LEDGER "
                                 + " WHERE "
                                 + " HOLDER_NO='" + Holder_No + "' AND "
                                 + " FUND_ID='" + Fund_ID + "' AND "
                                 + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                 + " "
                                 + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";

                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();



                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {
                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);
                            numb = new string[dataTable.Rows.Count];
                            foreach (System.Data.DataRow row in dataTable.Rows)
                                numb[Count++] = row[0].ToString();
                            Array.Resize(ref numb, Count);
                            Count = 0;
                            string Trans_String;
                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {
                                    Trans_String = row[0].ToString();
                                    AccountType = row[2].ToString();
                                    bool TTT = false;
                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }


                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }



                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);


                                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))
                                            {
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);

                                            }

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {
                                            string Trans_noRD = row[0].ToString();
                                            char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];

                                            // Total_Redemption_cost_forRD = Convert.ToDecimal(row[5].ToString());
                                            // Total_Redemption_cost_forRD = decimal.Round(Total_Redemption_cost, 4);

                                            //if (!(Trans_Type == "TR"))
                                            //{
                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);
                                            // }


                                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);
                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);

                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {
                                                if (!(Check_reversedRD == 'X'))                             // Check reversal
                                                {


                                                    Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                               * Total_Redemption_cost);
                                                    Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                              * Total_Redemption_cost);



                                                    Book_value = decimal.Round(Book_value, 2);
                                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }



                                                    // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                                                }
                                                else  // reversal for RD and TR
                                                {
                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }

                                                }

                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                //UnitCostRDfinal = 0.0000M;
                                                Unit_Holding_value = 0.0000M;

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                            }

                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString() + "---Get Redemption Update failed--- NAV", "");
                                }
                            }
                            objsNew = Actual_transfer_BookValue_update(Holder_No, Fund_ID, Actual_Cost, Actual_transfer_value, Book_value, UnitCostRDfinal, objsNew, IsInsert, Unit_Holding_value.ToString());
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Redemption Update failed--- NAV", "");
            }

            return objsNew;
        }

        protected static List<UtmcMemberInvestment> Actual_transfer_BookValue_update(string HolderNo, string IPD_fund_Code, decimal Actual_Cost, decimal Actual_Transfer, decimal Book_value, decimal Redumption_cost, List<UtmcMemberInvestment> objs, bool IsInsert = false, string Units = "0.00")
        {
            List<UtmcMemberInvestment> objsNew = new List<UtmcMemberInvestment>();
            objsNew = objs;
            string Query = "";
            try
            {
                var endDate = new DateTime();
                if (IsInsert)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"].ToString();
                    string Member_EPF_No = "";
                    // string Units = "0.00";
                    string Market_Value = "0.00";
                    string OracleSql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                                     + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS FROM  "
                                                     + " UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO='" + HolderNo + "'";

                    string ConnectionString = DB.GetOracleConnection();
                    using (OracleConnection conn = new OracleConnection(ConnectionString))
                    {
                        conn.Open();
                        using (OracleCommand comm = new OracleCommand(OracleSql, conn))
                        {
                            using (OracleDataReader rdr = comm.ExecuteReader())
                            {
                                while (rdr.Read())
                                {
                                    string Passport_No = rdr[1].ToString();
                                    string EPF_No = rdr[2].ToString();
                                    Member_EPF_No = EPF_No;
                                    //string NIC = "";
                                    // string NIC_Date = "";
                                    DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                    string BirthDate = dt.ToString("yyyy-MM-dd");
                                    string Gender = rdr[4].ToString();

                                    DateTime now = DateTime.Now;
                                    var startDate = new DateTime(now.Year, now.Month, 1);
                                    endDate = startDate.AddMonths(1).AddDays(-1);
                                    string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                    // Effective

                                    DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                    string Release_Date = dt1.ToString("yyyy-MM-dd");
                                    string year = DateTime.Now.Year.ToString();
                                    Release_Date = Release_Date.Remove(0, 4).Insert(0, year);

                                    //Query =
                                    //  "Insert into eppa_wbr1.utmc_member_information(Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                    // + " values('" + EPF_IPD_Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "');";
                                }
                            }
                        }
                    }

                    objsNew.Add(new UtmcMemberInvestment()
                    {
                        MemberEpfNo = Member_EPF_No,
                        EpfIpdCode = EPF_IPD_Code,
                        IpdFundCode = IPD_fund_Code,
                        IpdMemberAccNo = HolderNo,
                        ActualTransferredFromEpfRm = Convert.ToDecimal(Actual_Transfer),
                        Units = Convert.ToDecimal(Units == "" ? "0" : Units),
                        BookValue = Convert.ToDecimal(Book_value),
                        MarketValue = Convert.ToDecimal(Market_Value == "" ? "0" : Market_Value),
                        EffectiveDate = endDate,
                        ReportDate = endDate
                    });
                    //   Query += "\n" + "Insert into eppa_wbr1.utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                    //+ " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_fund_Code + "', '" + HolderNo + "','" + Actual_Transfer + "','" + Units + "','" + Book_value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";


                }

                else
                {
                    //Query = @"UPDATE eppa_wbr1.utmc_member_investment SET Actual_Cost = '" + Actual_Cost + @"', 
                    //Actual_Transferred_From_EPF_RM='" + Actual_Transfer + @"', Book_Value='" + Book_value + @"', 
                    //Redemption_Cost='" + Redumption_cost + @"' where IPD_Member_Acc_No='" + HolderNo + @"' AND 
                    //IPD_Fund_Code= '" + IPD_fund_Code + "' AND Report_Date='" + End_date + "' ";

                    objsNew.ForEach(x =>
                    {
                        if (x.IpdMemberAccNo == HolderNo && x.IpdFundCode == IPD_fund_Code && x.ReportDate == endDate)
                        {
                            x.ActualCost = Actual_Cost;
                        }
                    });

                }

            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "/n" + Query, "");
            }

            return objsNew;
        }
    }
}
