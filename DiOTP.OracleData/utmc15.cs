﻿using DiOTP.OracleData;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class utmc15
    {
        string Start_date = "";
        string End_date = "";

        string TransDateUpdate = "";
        string BirthDateupdate = "";
        static DataManager DB = new DataManager();
        public void Generate_Report15_local(string HolderNo)
        {

            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";

            try
            {
                DataTable DS = new DataTable();
                string Sqlconnstring = GetSqlConnection();


                string NAV_PricePU_Dt = End_date;
                NAV_PricePU_Dt = NAV_PricePU_Dt.Replace("/", "-");
                BirthDateupdate = BirthDateupdate.Replace("/", "-");
                Log.Write("UTMC_015 :Processing ", End_date);



                DS =Get_Member_Invesment(NAV_PricePU_Dt, TransDateUpdate, BirthDateupdate, "L",HolderNo);

                Int64 value = 0;
                try
                {
                    value = DS.Rows.Count;
                }
                catch (Exception ex)
                {
                    value = -1;
                    Log.Write("UTMC_015 :Done " + value + "---------" + ex.ToString(), End_date);
                }

                Log.Write("UTMC_015 :Done ", End_date);

                string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];


                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    foreach (DataRow rdr in DS.Rows)
                    {
                        string Member_EPF_No = rdr[0].ToString();

                        IPD_Fund_Code = rdr[1].ToString();
                        IPD_Member_Acc_No = rdr[2].ToString();
                        string Actual_Transferred_From_EPF_RM = rdr[4].ToString();
                        string Units = rdr[6].ToString();
                        string Book_Value = rdr[4].ToString();
                        string Market_Value = rdr[11].ToString();
                        string CurrentUnitValue = rdr[6].ToString();

                        // adjustment 2016 09

                        if (End_date == "2016/07/31")
                        {
                            if (IPD_Fund_Code == "06")
                            {

                                decimal Market_Value_TT = 0.00M;
                                decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                Market_Value = Convert.ToString(Market_Value_TT.ToString());

                            }
                        }

                        // adjustment 2016 09

                        DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                        string TransDate = dt.ToString("yyyy-MM-dd");

                        DateTime now = Convert.ToDateTime(rdr[5].ToString());
                        var startDate = new DateTime(now.Year, now.Month, 1);
                        var endDate = startDate.AddMonths(1).AddDays(-1);
                        string Effective_Date = End_date;

                        string Query = "Insert into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date,Current_Unit_Holding_Value) "
                       + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + TransDate + "','" + End_date + "','" + CurrentUnitValue + "')";

                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {

                         
                                mysql_comm.ExecuteNonQuery();
                            Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);

                            //System.Threading.Thread.Sleep(20);
                        }
                    }

                    mysql_conn.Close();
                }
                //System.Threading.Thread.Sleep(2000);

                ReadHolderNoforutmc15();
            }

            catch (Exception ex)
            {
                Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Member_Acc_No + " - Insert Failed", End_date);

                ex.ToString();
            }
        }

        public DataTable Get_Member_Invesment(string NAV_PricePU_Dt, string TransDateUpdate, string BirthDateupdate, string FundType,string HolderNo)
        {
           
               string FundCode = " '01','03','04','05','06','07','08','09' ";

            string Cash_HolderClass = "'RN','BI','NI','BC','NC','FI', 'FC','B1','B2','B3','N1','N2','N3','H1','H2','SB','SF','SN','SS','MS','ER','ES', 'RC', 'EZ' ";
            string EPF_HolderClass = "'EB','EN','ER','ES','EZ'";

            String holdderClass = Cash_HolderClass +","+ EPF_HolderClass;
            Log.Write("UTMC15 Datatable processing", "UTMC15 Query");
            DataTable ds = new DataTable();
            string Query = "select S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO,"
                                + " S1.TRANS_UNITS,S1.TRANS_AMT, s1.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO,"
                                + " s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR, (S4.UP_SELL * s1.CUR_UNIT_HLDG) AS MARKET_VALUE"
                                + " from UTS.HOLDER_LEDGER s1 inner join  UTS.HOLDER_REG s3 on   s1.HOLDER_NO = s3.HOLDER_NO "
                            + " inner join UTS.PRICE_HISTORY s4 on s1.FUND_ID = s4.FUND_ID and s1.TRANS_DT = s4.TRANS_DT "
                             + "     where s1.HOLDER_NO = '"+HolderNo+"' and s3.HOLDER_CLS IN "

                               + "    (" + holdderClass + ")  "
                                + "    AND S3.BIRTH_DT > to_date('"+BirthDateupdate+"', 'yyyy/mm/dd') "
                                + "   AND S1.FUND_ID IN ("+FundCode+") "
                                + "   and s1.TRANS_DT >= to_date('"+ TransDateUpdate + "', 'yyyy/mm/dd') "
                                 + "   and s1.TRANS_DT <= to_date('"+NAV_PricePU_Dt+ "', 'yyyy/mm/dd') "
                                  + "    ORDER BY S1.FUND_ID, S1.HOLDER_NO ";

            // and s1.CUR_UNIT_HLDG > 0

            Log.Write(Query, "UTMC15 Query");
            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }

        protected void ReadHolderNoforutmc15()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT IPD_Member_Acc_No,IPD_Fund_Code,Effective_Date FROM utmc_member_investment  order by IPD_Fund_Code,IPD_Member_Acc_No,Effective_Date   ";  //
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();




                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {



                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string EPF_No = row[0].ToString();
                                string IPD_fund_Code = row[1].ToString();
                                DateTime ef = Convert.ToDateTime(row[2].ToString());
                                string TransDate = ef.ToString("yyyy-MM-dd");
                                Get_Redumptiondate(EPF_No, IPD_fund_Code,TransDate);

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read Holder No From utmc_member_investment failed--- timeout", End_date);
            }

        }

        public void SelectDate(string StartDate, string EndDate)
        {
            Start_date = "2008/08/09";
            End_date = "2018/08/09";

            DateTime date = Convert.ToDateTime(EndDate);
            var nextMonth = new DateTime(date.Year, date.Month, 1).AddMonths(1);

            string[] datePartsDOB = Start_date.Split('/');

            int upper = datePartsDOB.GetUpperBound(0);
            if (upper == 2)
            {
                int tmpYear = Convert.ToInt32(datePartsDOB[0]) - 55;
                int tmpMonth = Convert.ToInt32(datePartsDOB[1]) - 1;
                if (tmpMonth == 0)
                {
                    tmpMonth = 12;
                    tmpYear = tmpYear - 1;
                }
                if (tmpMonth == 13)
                {
                    tmpMonth = 1;
                    tmpYear = tmpYear + 1;
                }
                int tmpDay = 1;

                DateTime dtLastTemp = new
                    DateTime(tmpYear, tmpMonth, tmpDay);
                BirthDateupdate = GetLastDayOfMonth(dtLastTemp).ToString("yyyy-MM-dd");

            }
            if (Convert.ToInt32(BirthDateupdate.Replace("-", "")) < 19610831)
            {
                BirthDateupdate = "1961-08-31";
            }


            TransDateUpdate = nextMonth.ToString("yyyy-MM-dd");

            // update for diotp and e-MIS



            DateTime datestart1 = Convert.ToDateTime(StartDate);
            Start_date = datestart1.AddDays(-1).ToString(("yyyy-MM-dd"));

            DateTime dateend = Convert.ToDateTime(EndDate);
            End_date = dateend.ToString(("yyyy-MM-dd"));

            //End_date = FindHoliday(End_date);
          
            TransDateUpdate = Convert.ToDateTime(Start_date).AddDays(1).ToString("yyyy-MM-dd");


        }

        private DateTime GetLastDayOfMonth(DateTime dtDate)
        {

            DateTime dtTo = dtDate;
            dtTo = dtTo.AddMonths(1);
            dtTo = dtTo.AddDays(-(dtTo.Day));
            return dtTo;
        }

        protected void Get_Redumptiondate(string Holder_No, string Fund_ID, string TransDate, bool IsInsert = false)
        {

            string Sqlconnstring = GetOracleConnection();
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                string AccountType = "";
                decimal Actual_Cost = 0.0000M;



                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                 + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                 + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG "
                                 + " FROM UTS.HOLDER_LEDGER "
                                 + " WHERE "
                                 + " HOLDER_NO='" + Holder_No + "' AND "
                                 + " FUND_ID='" + Fund_ID + "' AND "
                                 + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                 + " AND  TRANS_DT <= to_date('" + TransDate + "', 'yyyy/mm/dd') "
                                 + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";

                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();



                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }

                            Array.Resize(ref numb, Count);
                            Count = 0;
                            string Trans_String;
                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    AccountType = row[2].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }


                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }



                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);


                                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))
                                            {
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);

                                            }

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {
                                            string Trans_noRD = row[0].ToString();
                                            char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];

                                            // Total_Redemption_cost_forRD = Convert.ToDecimal(row[5].ToString());
                                            // Total_Redemption_cost_forRD = decimal.Round(Total_Redemption_cost, 4);

                                            //if (!(Trans_Type == "TR"))
                                            //{
                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);
                                            // }


                                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);
                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);

                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {
                                                if (!(Check_reversedRD == 'X'))                             // Check reversal
                                                {


                                                    Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                               * Total_Redemption_cost);
                                                    Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                              * Total_Redemption_cost);



                                                    Book_value = decimal.Round(Book_value, 2);
                                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }



                                                    // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                                                }
                                                else  // reversal for RD and TR
                                                {
                                                    // new actual transfer
                                                    if (!(AccountType == "SW"))
                                                    {
                                                        Actual_Cost += Convert.ToDecimal(row[6].ToString());
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }

                                                }

                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                //UnitCostRDfinal = 0.0000M;
                                                Unit_Holding_value = 0.0000M;

                                                // new actual transfer
                                                if (!(AccountType == "SW"))
                                                {
                                                    Actual_Cost -= Convert.ToDecimal(row[6].ToString());
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                            }

                                        }





                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString() + "---Get Redemption Update failed--- NAV", End_date);
                                }




                            }
                            Actual_transfer_BookValue_update(Holder_No, Fund_ID, Actual_Cost, Actual_transfer_value, Book_value, UnitCostRDfinal, Unit_Holding_value.ToString(),TransDate);
                            mysql_conn.Close();

                        }
                    }
                }
            }



            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Redemption Update failed--- NAV", End_date);
            }
        }

        public void Actual_transfer_BookValue_update(string HolderNo, string IPD_fund_Code, decimal Actual_Cost, decimal Actual_Transfer, decimal Book_value, decimal Redumption_cost, string Units, string TransDate, bool IsInsert = false)
        {
            string Query = "";

            string Sqlconnstring = GetSqlConnection();
            try
            {


                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    if (IsInsert)
                    {

                        string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                        string Member_EPF_No = "";



                        string Market_Value = "0.00";


                        string OracleSql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                                         + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS FROM  "
                                                         + " UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO='" + HolderNo + "'";

                        string ConnectionString = GetOracleConnection();


                        using (OracleConnection conn = new OracleConnection(ConnectionString))
                        {
                            conn.Open();

                            using (OracleCommand comm = new OracleCommand(OracleSql, conn))
                            {
                                using (OracleDataReader rdr = comm.ExecuteReader())
                                {
                                    while (rdr.Read())
                                    {
                                        string Passport_No = rdr[1].ToString();
                                        string EPF_No = rdr[2].ToString();
                                        Member_EPF_No = EPF_No;
                                        string NIC = "";

                                        DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                        string BirthDate = dt.ToString("yyyy-MM-dd");
                                        string Gender = rdr[4].ToString();

                                        DateTime now = DateTime.Now;
                                        var startDate = new DateTime(now.Year, now.Month, 1);
                                        var endDate = startDate.AddMonths(1).AddDays(-1);
                                        string EndofMonth = endDate.ToString("yyyy-MM-dd");


                                        DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                        string Release_Date = dt1.ToString("yyyy-MM-dd");
                                        string year = DateTime.Now.Year.ToString();
                                        Release_Date = Release_Date.Remove(0, 4).Insert(0, year);



                                        Query =
                                          "Insert into utmc_member_information(Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                         + " values('" + EPF_IPD_Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "');";

                                    }
                                }
                            }
                        }

                        Query += "\n" + "Insert into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                     + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_fund_Code + "', '" + HolderNo + "','" + Actual_Transfer + "','" + Units + "','" + Book_value + "','" + Market_Value + "','" + TransDate + "','" + End_date + "')";


                    }

                    else
                    {
                        Query = "UPDATE utmc_member_investment SET Actual_Cost = '" + Actual_Cost + "', Actual_Transferred_From_EPF_RM='" + Actual_Transfer + "', Book_Value='" + Book_value + "', Redemption_Cost='" + Redumption_cost + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' AND Effective_Date='" + TransDate + "' ";


                    }

                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {



                        mysql_comm.ExecuteNonQuery();

                        //System.Threading.Thread.Sleep(20);
                        Log.Write(Query, End_date);
                    }
                    mysql_conn.Close();
                }
                //  }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "/n" + Query, End_date);
            }

        }

        public DataTable GenerateDataTable(string Query, string DataBaseType)
        {


            DataTable ds = new DataTable();
       
            if (DataBaseType == "Oracle")
            {
                string ConnectionString =GetOracleConnection();
                OracleConnection conn = new OracleConnection(ConnectionString);
                try
                {
                    string mysql = Query;

                    OracleCommand cmd = new OracleCommand(mysql, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.Text;
                    OracleDataAdapter da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
                catch (Exception ex)
                {
                    Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
                }
                finally
                {
                    conn.Close();
                }
            }
            else if (DataBaseType == "MySQL")
            {
                string ConnectionString = GetSqlConnection();
                MySqlConnection conn = new MySqlConnection(ConnectionString);
                try
                {
                    string mysql = Query;

                    MySqlCommand cmd = new MySqlCommand(mysql, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.Text;
                    MySqlDataAdapter da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
                catch (Exception ex)
                {
                    Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
                }
                finally
                {
                    conn.Close();
                }
            }
            return ds;

        }

        private string GetOracleConnection()
        {


            string ConnectionString = DB.GetOracleConnection();
            return ConnectionString;

        }

        private string GetSqlConnection()
        {

            string ConnectionString = DB.GetSqlConnection();
            return ConnectionString;
        }

        public void SQL_ExcuteQuery(string Query)
        {


            MySqlConnection connection = new MySqlConnection(GetSqlConnection());
            MySqlCommand mycommand = connection.CreateCommand();
            mycommand.CommandText = Query;
            try
            {
                connection.Open();
                mycommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), "DB_Layer");
            }
            finally
            {
                connection.Close();
            }

        }


        public List<UtmcMemberInvestment> Read_MemberInvestment(string HolderAccNo,string startdate, string enddate)
        {


            SQL_ExcuteQuery("delete from utmc_member_investment where IPD_Member_Acc_No = '"+ HolderAccNo + "'");

            SelectDate(startdate, enddate);
            Generate_Report15_local(HolderAccNo);

            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT * FROM utmc_member_investment where IPD_Member_Acc_No ='" + HolderAccNo + "'";  //
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                objs.Add(new UtmcMemberInvestment()
                                {
                                    MemberEpfNo = row[1].ToString(),
                                    IpdFundCode = row[3].ToString(),
                                    ActualTransferredFromEpfRm = Convert.ToDecimal(row[5].ToString()),
                                    ActualCost = Convert.ToDecimal(row[6].ToString()),
                                    Units = Convert.ToDecimal(row[7].ToString()),
                                    BookValue = Convert.ToDecimal(row[8].ToString()),
                                    MarketValue = Convert.ToDecimal(row[9].ToString()),
                                    CurUnitHldg = row[14].ToString(),
                                    EffectiveDate = Convert.ToDateTime(row[10].ToString())
                                });

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read Holder No From utmc_member_investment failed--- timeout", End_date);
            }
            return objs;

        }
     
    }

    public class UtmcMemberInvestment
    {

        public Int32 Id { get; set; }

        public String MemberEpfNo { get; set; }

        public String EpfIpdCode { get; set; }

        public String IpdFundCode { get; set; }

        public String IpdMemberAccNo { get; set; }

        public Decimal ActualTransferredFromEpfRm { get; set; }

        public Decimal ActualCost { get; set; }

        public Decimal Units { get; set; }

        public Decimal BookValue { get; set; }

        public Decimal MarketValue { get; set; }

        public DateTime EffectiveDate { get; set; }

        public DateTime ReportDate { get; set; }

        public Decimal? RedemptionCost { get; set; }

        public String Isactive { get; set; }


        public String TransString { get; set; }
        public String TransType { get; set; }
        public String AccType { get; set; }
        public String EntryDt { get; set; }
        public String TransDt { get; set; }
        public String TransUnits { get; set; }
        public String TransAmt { get; set; }
        public String TransPR { get; set; }
        public String TransMarketValue { get; set; }
        public String TransUnitsXTransPR { get; set; }
        public String CurUnitHldg { get; set; }
        public String CumValueHldg { get; set; }
    }


}
