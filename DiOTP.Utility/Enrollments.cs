﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class Enrollments
    {
        public Int32 Id { get; set; }
        public Int32 isAttended { get; set; }
        public Int32 isPassed { get; set; }
        public Int32 IsOnline { get; set; }
        public String Topic { get; set; }
        public String Synopsis { get; set; }
        public Int32 AgentId { get; set; }
        public String AgentName { get; set; }
        public String AgentType { get; set; }
        public String AgentCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 IsAssessment { get; set; }
        public Int32 IsReported { get; set; }
    }
}
