﻿using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Admin
{
    public class ZoomIntegrationService
    {
        public static string baseURL = "https://api.zoom.us";
        public static string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IjJtdHgtTGwyUlVDdFU4MDVUNXhOSWciLCJleHAiOjE2MTc4NTI5OTYsImlhdCI6MTYxNzI0ODE5Nn0.WTpJQ-GQOojnwbS4ZWNs3Fn-871OLqooksqOSUZfteA";
        public static Response CallApi(string url, HttpMethod httpMethod, StringContent stringContent = null)
        {
            url = "/v2/users?status=active&page_size=30&page_number=1";
            Response response = new Response();
            try
            {
                //var client = new RestClient("https://api.zoom.us/v2/users?status=active&page_size=30&page_number=1");
                //var request = new RestRequest(Method.GET);
                //request.AddHeader("content-type", "application/json");
                //request.AddHeader("authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IjJtdHgtTGwyUlVDdFU4MDVUNXhOSWciLCJleHAiOjE2MTcwODg1MDcsImlhdCI6MTYxNzA4MzEwN30.VkEPGBwBp_Hi1ce8fLWosuvF30uou8LKYPCy98Ioj74");
                //IRestResponse response1 = client.Execute(request);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("authorization", "Bearer " + token);
                    var responseApi = new HttpResponseMessage();
                    if (httpMethod == HttpMethod.Get)
                        responseApi = client.GetAsync(url).Result;
                    else if (httpMethod == HttpMethod.Post)
                        responseApi = client.PostAsync(url, stringContent).Result;
                    if (responseApi.IsSuccessStatusCode)
                    {
                        string responseString = responseApi.Content.ReadAsStringAsync().Result;
                        response.IsSuccess = true;
                        response.Data = responseString;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseApi.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response CreateMeeting(Meeting meeting, string url)
        {
            String requestJson = JsonConvert.SerializeObject(meeting);
            //requestJson = "{\"topic\":\"New\",\"type\":\"2\",\"start_time\":\"2021-03-31T16:20:00\",\"duration\":\"5\",\"schedule_for\":\"\",\"timezone\":\"\",\"password\":\"12345\",\"agenda\":\"string\",\"recurrence\":{\"type\":\"1\",\"repeat_interval\":\"1\",\"weekly_days\":\"string\",\"monthly_day\":\"integer\",\"monthly_week\":\"integer\",\"monthly_week_day\":\"integer\",\"end_times\":\"2\",\"end_date_time\":\"string[date-time]\"},\"settings\":{\"host_video\":\"true\",\"participant_video\":\"true\",\"cn_meeting\":\"false\",\"in_meeting\":\"false\",\"join_before_host\":\"true\",\"mute_upon_entry\":\"true\",\"watermark\":\"true\",\"use_pmi\":\"true\",\"approval_type\":\"0\",\"registration_type\":\"2\",\"audio\":\"both\",\"auto_recording\":\"none\",\"enforce_login\":\"false\",\"enforce_login_domains\":\"\",\"alternative_hosts\":\"\",\"global_dial_in_countries\":[\"\"],\"registrants_email_notification\":\"true\"}}";
            //stringContent = new StringContent(requestJson);
            //url = "/v2/users/me/meetings?status=active&page_size=30&page_number=1";
            Response response = new Response();
            try
            {
                var client = new RestClient(url);//"https://api.zoom.us/v2/users/me/meetings?status=active&page_size=30&page_number=1"
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + token);
                //"{\"topic\":\"New\",\"type\":\"1\",\"start_time\":\"2021-03-30T17:15:00\",\"duration\":\"5\",\"schedule_for\":\"\",\"timezone\":\"\",\"password\":\"12345\",\"agenda\":\"Agenda\",\"recurrence\":{\"type\":\"1\",\"repeat_interval\":\"1\",\"weekly_days\":\"\",\"monthly_day\":\"0\",\"monthly_week\":\"0\",\"monthly_week_day\":\"0\",\"end_times\":\"1\",\"end_date_time\":\"\"},\"settings\":{\"host_video\":\"true\",\"participant_video\":\"true\",\"cn_meeting\":\"false\",\"in_meeting\":\"false\",\"join_before_host\":\"true\",\"mute_upon_entry\":\"false\",\"watermark\":\"false\",\"use_pmi\":\"false\",\"approval_type\":\"2\",\"registration_type\":\"1\",\"audio\":\"string\",\"auto_recording\":\"string\",\"enforce_login\":\"true\",\"enforce_login_domains\":\"\",\"alternative_hosts\":\"\",\"global_dial_in_countries\":[\"\"],\"registrants_email_notification\":\"true\", \"meeting_authentication\":\"true\"}}"
                request.AddParameter("application/json", requestJson, ParameterType.RequestBody);
                IRestResponse response1 = client.Execute(request);
                MeetingResponse meetingResponse = JsonConvert.DeserializeObject<MeetingResponse>(response1.Content);
                response.IsSuccess = true;
                response.Data = meetingResponse;


                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(baseURL);
                //    client.DefaultRequestHeaders.Accept.Clear();
                //    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //    client.DefaultRequestHeaders.Add("authorization", "Bearer " + token);
                //    var responseApi = new HttpResponseMessage();
                //    if (httpMethod == HttpMethod.Get)
                //        responseApi = client.GetAsync(url).Result;
                //    else if (httpMethod == HttpMethod.Post)
                //        responseApi = client.PostAsync(url, stringContent).Result;
                //    if (responseApi.IsSuccessStatusCode)
                //    {
                //        string responseString = responseApi.Content.ReadAsStringAsync().Result;
                //        response.IsSuccess = true;
                //        response.Data = responseString;
                //    }
                //    else
                //    {
                //        string responseString = responseApi.Content.ReadAsStringAsync().Result;
                //        response.IsSuccess = false;
                //        response.Message = responseApi.ReasonPhrase;
                //    }
                //}
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            //try
            //{
            //    MeetingResponse meetingResponse = (MeetingResponse)response.Data;
            //    var client = new RestClient("https://api.zoom.us/v2/meetings/"+ meetingResponse.id + "/registrants?occurrence_ids=<string>");
            //    client.Timeout = -1;
            //    var request = new RestRequest(Method.POST);
            //    request.AddHeader("content-type", "application/json");
            //    request.AddHeader("authorization", "Bearer " + token);
            //    request.AddParameter("application/json", "{\"email\":\"sachin@petraware.com\",\"first_name\":\"Sachin\",\"last_name\":\"Reddy\",\"address\":\"KL\",\"city\":\"KL\",\"country\":\"MY\",\"zip\":\"50470\",\"state\":\"KL\",\"phone\":\"601151179079\",\"industry\":\"IT\",\"org\":\"Petraware\",\"job_title\":\"SSE\",\"purchasing_time_frame\":\"MoreThan6Months\",\"role_in_purchase_process\":\"Influencer\",\"no_of_employees\":\"1-20\",\"comments\":\"Excitedtohostyou.\",\"custom_questions\":[{\"title\":\"FavoritethingaboutZoom\",\"value\":\"MeetHappy\"}]}", ParameterType.RequestBody);
            //    IRestResponse response2 = client.Execute(request);
            //    Console.WriteLine(response2.Content);
            //}
            //catch(Exception ex){

            //}
            return response;
        }
    }
}