using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("fund_info")]
    public class FundInfo
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("fund_code"), Display("FundCode")]
        public String FundCode { get; set; }
        [Database("current_nav_date"), Display("CurrentNavDate")]
        public DateTime CurrentNavDate { get; set; }
        [Database("current_unit_price"), Display("CurrentUnitPrice")]
        public Decimal CurrentUnitPrice { get; set; }
        [Database("change_price"), Display("ChangePrice")]
        public Decimal ChangePrice { get; set; }
        [Database("change_per"), Display("ChangePer")]
        public Decimal ChangePer { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
    }
}
