﻿using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UserLogMainMainRepo : IUserLogMainRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UserLogMain obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserLogMain>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                //obj = GetDataByPropertyName(nameof(UserLogMain.UpdatedDate), obj.UpdatedDate.ToString("yyyy-MM-dd HH:mm:ss"), true, 0, 0, false).FirstOrDefault();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserLogMain> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserLogMain>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserLogMain obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserLogMain>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserLogMain> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserLogMain>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserLogMain DeleteData(Int32 Id)
        {
            Response responseULM = GetSingle(Id);
            if (responseULM.IsSuccess)
            {
                UserLogMain obj = (UserLogMain)responseULM.Data;
                try
                {
                    string query = obj.ObjectToQuery<UserLogMain>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseULM = GetSingle(Id);
                    if (responseULM.IsSuccess)
                    {
                        UserLogMain obj = (UserLogMain)responseULM.Data;
                        query += obj.ObjectToQuery<UserLogMain>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserLogMain obj = new UserLogMain();
            try
            {
                string query = "select * from user_log_main where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UserLogMain
                       {
                           Id = x.Field<Int32>("id"),
                           StatusType = x.Field<Int32>("status_type"),
                           UserId = x.Field<Int32>("user_id"),
                           UserAccountId = x.Field<Int32>("user_account_id"),
                           Description = x.Field<string>("description"),
                           TableName = x.Field<string>("table_name"),
                           RefId = x.Field<Int32>("ref_id"),
                           RefValue = x.Field<string>("ref_value"),
                           UpdatedDate = x.Field<DateTime>("updated_date")
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserLogMain> objs = new List<UserLogMain>();
            try
            {
                string query = "select * from user_log_main";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserLogMain
                        {
                            Id = x.Field<Int32>("id"),
                            StatusType = x.Field<Int32>("status_type"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            Description = x.Field<string>("description"),
                            TableName = x.Field<string>("table_name"),
                           RefId = x.Field<Int32>("ref_id"),
                            RefValue = x.Field<string>("ref_value"),
                            UpdatedDate = x.Field<DateTime>("updated_date")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_log_main";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UserLogMain>(propertyName);
            List<UserLogMain> objs = new List<UserLogMain>();
            try
            {
                string query = "select * from user_log_main where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from user_log_main where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserLogMain
                        {
                            Id = x.Field<Int32>("id"),
                            StatusType = x.Field<Int32>("status_type"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            Description = x.Field<string>("description"),
                            TableName = x.Field<string>("table_name"),
                           RefId = x.Field<Int32>("ref_id"),
                           RefValue = x.Field<string>("ref_value"),
                            UpdatedDate = x.Field<DateTime>("updated_date")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UserLogMain>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_log_main where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from user_log_main where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserLogMain> objs = new List<UserLogMain>();
            try
            {
                string query = "select * from user_log_main where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserLogMain
                        {
                            Id = x.Field<Int32>("id"),
                            StatusType = x.Field<Int32>("status_type"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            Description = x.Field<string>("description"),
                            TableName = x.Field<string>("table_name"),
                           RefId = x.Field<Int32>("ref_id"),
                           RefValue = x.Field<string>("ref_value"),
                            UpdatedDate = x.Field<DateTime>("updated_date")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_log_main where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
