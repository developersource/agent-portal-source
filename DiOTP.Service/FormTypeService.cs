using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class FormTypeService : IFormTypeService
    {
        private static readonly Lazy<IFormTypeRepo> lazy = new Lazy<IFormTypeRepo>(() => new FormTypeRepo());
        public static IFormTypeRepo IFormTypeRepo { get { return lazy.Value; } }
        public Response PostData(FormType obj)
        {
            Response response = new Response();
            try
            {
                response = IFormTypeRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FormType> objs)
        {
            Int32 result = 0;
            try
            {
                result = IFormTypeRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FormType obj)
        {
            Response response = new Response();
            try
            {
                response = IFormTypeRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FormType> objs)
        {
            Int32 result = 0;
            try
            {
                result = IFormTypeRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FormType DeleteData(Int32 Id)
        {
            FormType obj = new FormType();
            try
            {
                obj = IFormTypeRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IFormTypeRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FormType obj = new FormType();
            try
            {
                response = IFormTypeRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormType> objs = new List<FormType>();
            try
            {
                response = IFormTypeRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IFormTypeRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormType> objs = new List<FormType>();
            try
            {
                response = IFormTypeRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IFormTypeRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormType> objs = new List<FormType>();
            try
            {
                response = IFormTypeRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IFormTypeRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
