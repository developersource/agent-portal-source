﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class UserRspeModel
    {
        public Int32 Id { get; set; }
        public Int32 Fund_ID { get; set; }
        public Int32 User_Id { get; set; }
        public decimal Rsp_Amount { get; set; }
        public Int32 Fund_Category { get; set; }
        public Int32 Ma_Holder_bankId { get; set; }
        public DateTime Rsp_Date { get; set; }
        public Int32 Status { get; set; }
        public int Durations { get; set; }
        public int PaymentMethod { get; set; }
        public string UserAccount { get; set; }
        public string UserName { get; set; }
        public string IDNo { get; set; }
        public int UserAccountId { get; set; }
    }
}
