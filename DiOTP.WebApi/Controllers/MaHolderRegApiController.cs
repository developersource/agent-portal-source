﻿using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.OracleData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.WebApi.Controllers
{
    public class MaHolderRegApiController : ApiController
    {
        //private static readonly Lazy<>

        //private static readonly Lazy<IMaHolderRegRepo> lazy = new Lazy<IMaHolderRegRepo>(() => new MaHolderRegRepo());
        //public static IMaHolderRegRepo IMaHolderRegRepo { get { return lazy.Value; } }

        //GET api/<controller>
        public Response Get(string filter)
        {
            Response response = new Response();
            response = MaHolderRegLiveData.GetDataByFilter(filter, 0, 0, false);
            return response;
        }

        // GET api/<controller>/5
        //public MaHolderReg Get(int id)
        //{
        //    Console.WriteLine("API Holder: " + id);
        //    return MaHolderRegLiveData.GetSingle(id);
        //}

        // POST api/<controller>
        public Boolean PostSingle(MaHolderReg obj)
        {
            Boolean success = MaHolderRegLiveData.PostDataSingle(obj);
            return success;
        }

        // PUT api/<controller>
        public MaHolderReg Put(MaHolderReg obj)
        {
            MaHolderReg newObj = MaHolderRegLiveData.UpdateDataSingle(obj);
            return newObj;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}