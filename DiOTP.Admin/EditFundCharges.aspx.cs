﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class EditFundCharges : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundChargeService> lazyUtmcFundChargeServiceObj = new Lazy<IUtmcFundChargeService>(() => new UtmcFundChargeService());

        public static IUtmcFundChargeService IUtmcFundChargeService { get { return lazyUtmcFundChargeServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                string idString = Request.QueryString["id"];
                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);
                    Response response = IUtmcFundInformationService.GetSingle(id);
                    if (response.IsSuccess)
                    {
                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)response.Data;

                        Response responseUFCList = IUtmcFundChargeService.GetDataByPropertyName(nameof(UtmcFundCharge.UtmcFundInformationId), utmcFundInformation.Id.ToString(), true, 0, 0, false);
                        if (responseUFCList.IsSuccess) {
                            UtmcFundCharge ufc = ((List<UtmcFundCharge>)responseUFCList.Data).FirstOrDefault();

                            txtFundDiscountedInitialSalesCharge.Text = ufc.InitialSalesChargesPercent.ToString();
                            txtFundAnnualManagementCharge.Text = ufc.AnnualManagementChargePercent.ToString();
                            txtFundTrusteeFee.Text = ufc.TrusteeFeePercent.ToString();
                            txtFundSwitchingFee.Text = ufc.SwitchingFeePercent.ToString();
                            txtFundRedemptionFee.Text = ufc.RedemptionFeePercent.ToString();
                            txtFundTransferFee.Text = ufc.TransferFeeRm.ToString();
                            txtFundOtherSignificantFee.Text = ufc.OtherSignificantFeeRm.ToString();
                            txtFundGSTFee.Text = ufc.GstPercent.ToString();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUFCList.Message + "');", true);
                        }
                    }
                }
            }
        }
    }
}