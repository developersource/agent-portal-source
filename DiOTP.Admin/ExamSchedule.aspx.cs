﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class ExamSchedule : System.Web.UI.Page
    {
        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status,group_concat(exam_datetime) as exam_datetimes FROM ");
            string mainQCount = (@"select count(*) as count from (");
            filter.Append(@" agent_exam_schedules where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (ao.name COLLATE UTF8_GENERAL_CI like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.email COLLATE UTF8_GENERAL_CI like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.id_no COLLATE UTF8_GENERAL_CI like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.mobile_no like '%" + Search.Value.Trim() + "%')");
                }
                if (!string.IsNullOrEmpty(Request.QueryString["DateType"]))
                {
                    DateType.Value = Request.QueryString["DateType"].ToString();
                    if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                    {
                        FromDate.Value = Request.QueryString["FromDate"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ToDate"]))
                    {
                        ToDate.Value = Request.QueryString["ToDate"].ToString();
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value == "0")
                    {
                        filter.Append(@"and is_active in ('0') ");
                    }
                    else if (FilterValue.Value == "1")
                    {
                        filter.Append(@"and is_active in ('1') ");
                    }
                    else
                    {

                    }
                }
                else
                {
                    filter.Append(@"and is_active in ('0', '1') ");
                }

                if (!string.IsNullOrEmpty(DateType.Value))
                {
                    if (!string.IsNullOrEmpty(FromDate.Value) && !string.IsNullOrEmpty(ToDate.Value))
                    {
                        DateTime from = DateTime.ParseExact(FromDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime to = DateTime.ParseExact(ToDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        if (FromDate.Value.Trim() == ToDate.Value.Trim())
                        {
                            to = to.AddDays(1);
                        }
                        else if (from > to)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                    "alert('From date cannot be greater than To date.');", true);
                        }
                        if (DateType.Value == "1")
                            filter.Append(" and exam_datetime between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                    }
                }

                filter.Append(" group by DATE(exam_datetime) ");

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by exam_datetime desc ");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") a").Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", skip, take, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var UsersDyn = responseUList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseJSON);


                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (DiOTP.Utility.CustomAgentExamSchedule aes in agentExamSchedules)
                    {
                        string times = String.Join(", ", aes.exam_datetimes.Split(',').ToList().Select(x=> DateTime.Parse(x).ToString("hh:mm tt")));
                        asb.Append(@"<tr>
                                <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + aes.id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (aes.is_active == 1 ? "<span class='label label-success'>Active</span>" :
                                 aes.is_active == 0 ? "<span class='label label-danger'>Inactive</span>" : "") + @"</span>
                                </td>
                                <td>
                                    " + aes.language + @"
                                </td>
                                <td>
                                    " + aes.location + @"
                                </td>
                                <td>
                                    " + aes.exam_datetime.ToString("dd/MM/yyyy") + @"
                                </td>
                                <td>
                                    " + times + @"
                                </td>
                            </tr>");
                        index++;
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseUList.Message + "\");", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(dynamic obj)
        {
            Response response = new Response();
            try
            {
                if (HttpContext.Current.Session["admin"] != null)
                {
                    User loginUser = (User)HttpContext.Current.Session["admin"];
                    String Language = obj["Language"];
                    String Location = obj["Location"];
                    String ExamDate = obj["ExamDate"];
                    String SessionTime1 = obj["SessionTime1"];
                    String SessionTime2 = obj["SessionTime2"];
                    String SessionTime3 = obj["SessionTime3"];
                    DateTime examdate1 = DateTime.ParseExact(ExamDate + " " + SessionTime1, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                    DateTime examdate2 = DateTime.ParseExact(ExamDate + " " + SessionTime2, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                    DateTime examdate3 = DateTime.ParseExact(ExamDate + " " + SessionTime3, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                    AgentExamSchedule agentExamSchedule1 = new AgentExamSchedule
                    {
                        session_id = Language + " - " + Location + " - " + examdate1.ToString("dd/MM/yyyy hh:mm tt"),
                        language = Language,
                        location = Location,
                        exam_datetime = examdate1,
                        is_active = 1,
                        created_date = DateTime.Now,
                        created_by = loginUser.Id,
                        updated_date = DateTime.Now,
                        updated_by = loginUser.Id,
                        status = 1
                    };
                    List<AgentExamSchedule> agentExamSchedules = new List<AgentExamSchedule>();
                    agentExamSchedules.Add(agentExamSchedule1);
                    AgentExamSchedule agentExamSchedule2 = new AgentExamSchedule
                    {
                        session_id = Language + " - " + Location + " - " + examdate2.ToString("dd/MM/yyyy hh:mm tt"),
                        language = Language,
                        location = Location,
                        exam_datetime = examdate2,
                        is_active = 1,
                        created_date = DateTime.Now,
                        created_by = loginUser.Id,
                        updated_date = DateTime.Now,
                        updated_by = loginUser.Id,
                        status = 1
                    };
                    agentExamSchedules.Add(agentExamSchedule2);
                    AgentExamSchedule agentExamSchedule3 = new AgentExamSchedule
                    {
                        session_id = Language + " - " + Location + " - " + examdate3.ToString("dd/MM/yyyy hh:mm tt"),
                        language = Language,
                        location = Location,
                        exam_datetime = examdate3,
                        is_active = 1,
                        created_date = DateTime.Now,
                        created_by = loginUser.Id,
                        updated_date = DateTime.Now,
                        updated_by = loginUser.Id,
                        status = 1
                    };
                    agentExamSchedules.Add(agentExamSchedule3);
                    Response responsePost = GenericService.PostBulkData<AgentExamSchedule>(agentExamSchedules);
                    if (responsePost.IsSuccess)
                    {
                        response.IsSuccess = true;
                        response.Message = "Successfully added";
                    }
                    else
                    {
                        response = responsePost;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired!";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}