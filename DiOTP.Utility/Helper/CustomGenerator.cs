﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.Helper
{
    public static class CustomGenerator
    {
        public static string GenerateSixDigitPin()
        {
            Random generator = new Random();
            return generator.Next(0, 1000000).ToString().PadLeft(6, '0');
        }
        public static string GenerateGoogleAuthenticationUniqueKeyForEachUser(string username)
        {
            
            string key = DateTime.Now.ToString("yyyyMMddhhmmssfff");
            return key;
        }

        public static List<int> SplitNumber(int minAmount, int maxAmount, int maxPerGroup)
        {
            List<int> result = new List<int>();

            int minNo = minAmount;
            int interval = (maxAmount - minAmount) / maxPerGroup;
            for(int i = 0; i < maxPerGroup; i++)
            {
                result.Add(minNo);
                minNo = minNo + interval;
            }
            result.Add(maxAmount);
            return result;
        }
    }
}
