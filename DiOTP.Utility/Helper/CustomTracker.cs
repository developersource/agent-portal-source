﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DiOTP.Utility.Helper
{
    class CustomTracker
    {
    }

    public static class TrackIPAddress
    {
        

        public static string GetUserPublicIP(String ip)
        {
            HttpRequest currentRequest = HttpContext.Current.Request;
            string ipAddress = currentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipAddress == null || ipAddress.ToLower() == "unknown")
            {
                ipAddress = currentRequest.ServerVariables["REMOTE_ADDR"];
                if (ipAddress == "::1" || ipAddress == ip)
                {
                    string strIP = String.Empty;
                    WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
                    using (WebResponse response = request.GetResponse())
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        strIP = sr.ReadToEnd();
                    }
                    //scrape ip from the html
                    int i1 = strIP.IndexOf("Address: ") + 9;
                    int i2 = strIP.LastIndexOf("</body>");
                    strIP = strIP.Substring(i1, i2 - i1);
                    return strIP;
                }
                return ipAddress;
            }
            return ipAddress;
        }
    }


    public class Logger
    {

        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = ConfigurationManager.AppSettings["logFilePath"].ToString();
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("dd-MM-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            strLog = strLog + " - DateTime : " + DateTime.Now;
            log.WriteLine(strLog);
            log.Close();
        }
    }
}
