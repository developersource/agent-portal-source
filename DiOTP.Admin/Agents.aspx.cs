﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class Agents : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserTypesDefService> lazyUserTypesDefServiceObj = new Lazy<IUserTypesDefService>(() => new UserTypesDefService());
        public static IUserTypesDefService IUserTypesDefService { get { return lazyUserTypesDefServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT u.id,u.username,t.user_type_id,d.name as user_type,u.mobile_number,u.email_id,u.last_login_date,u.created_date,u.status from ");
            string mainQCount = (@"SELECT count(*) from ");
            filter.Append(@" users u
                            join user_types t on u.id = t.user_id
                            join user_types_def d on t.user_type_id = d.id where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.email_id like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.mobile_number like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]) && Request.QueryString["FilterValue"].Trim() != "0")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and t.user_type_id in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(@" and t.user_type_id in ('6','7','8', '9') ");
                }

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by u.status desc, u.created_date desc");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }


                Response responseAdmins = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, true);


                if (responseAdmins.IsSuccess)
                {


                    var AdminsDyn = responseAdmins.Data;
                    var responseJSON = JsonConvert.SerializeObject(AdminsDyn);
                    List<DiOTP.Utility.Admins> Admins = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Admins>>(responseJSON);

                    StringBuilder asb = new StringBuilder();


                    int index = 1;
                    {
                        Boolean isCurrentUser = false;
                        Boolean isCurrentUserType = false;

                        Admins.ForEach(x =>
                        {
                            if (user.Id == x.Id)
                                isCurrentUser = true;
                            if (user.UserRoleId == x.user_type_id)
                            {
                                Response responseUT = IUserTypeService.GetDataByFilter(" user_id='" + x.Id + "'", 0, 0, false);
                                if (responseUT.IsSuccess)
                                {
                                    UserType userType = ((List<UserType>)responseUT.Data).Where(y => y.UserTypeId == loginUserType.UserTypeId).FirstOrDefault();
                                    if (userType != null)
                                    { isCurrentUserType = true; }
                                }
                            }
                            asb.Append(@"<tr>
                                    <td class='" + (isCurrentUserType ? "" : "icheck") + @"'>
                                        " + (isCurrentUserType ? "" : @"<div class='square single-row'>
                                            <div class='checkbox'>
                                                <input type='checkbox' name='checkRow' class='checkRow' value='" + x.Id + @"'" + @" /> <label>" + index + @"</label><br/>
                                            </div>
                                        </div>") + @"
                                        <span class='row-status'>" + (x.status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Username</td>
                                                    <td><strong>" + x.username + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>User type</td>
                                                    <td><strong>" + x.user_type + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><strong>" + x.email_id + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table class='table'>
                                            <tbody>
                                                <tr>
                                                    <td>Mobile number</td>
                                                    <td><strong>" + x.mobile_number + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td style='width:50%;'>Created date</td>
                                                    <td><strong>" + x.created_date + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Last login</td>
                                                    <td><strong>" + (x.last_login_date != null && x.last_login_date.ToString("yyyy") != "0001" ? x.last_login_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>");
                            index++;

                        });
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                               "alert(\"" + responseAdmins.Message + "\");", true);
                }


            }
        }
    }
}