using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class MaHolderRegRepo : IMaHolderRegRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(MaHolderReg obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<MaHolderReg>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<MaHolderReg> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<MaHolderReg>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(MaHolderReg obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<MaHolderReg>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<MaHolderReg> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<MaHolderReg>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public MaHolderReg DeleteData(Int32 Id)
        {
            Response responseMHR = GetSingle(Id);
            if (responseMHR.IsSuccess)
            {
                MaHolderReg obj = (MaHolderReg)responseMHR.Data;
                try
                {
                    string query = obj.ObjectToQuery<MaHolderReg>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseMHR = GetSingle(Id);
                    if (responseMHR.IsSuccess)
                    {
                        MaHolderReg obj = (MaHolderReg)responseMHR.Data;
                        query += obj.ObjectToQuery<MaHolderReg>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            MaHolderReg obj = new MaHolderReg();
            try
            {
                string query = "select * from ma_holder_reg where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new MaHolderReg
                       {
                           Id = x.Field<Int32>("id"),
                           IdNo = x.Field<String>("id_no"),
                           IdNoOld = x.Field<String>("id_no_old"),
                           IdNo2 = x.Field<String>("id_no_2"),
                           IdNoOld2 = x.Field<String>("id_no_old_2"),
                           IdNo3 = x.Field<String>("id_no_3"),
                           IdNoOld3 = x.Field<String>("id_no_old_3"),
                           IdNo4 = x.Field<String>("id_no_4"),
                           IdNoOld4 = x.Field<String>("id_no_old_4"),
                           IdNo5 = x.Field<String>("id_no_5"),
                           IdNoOld5 = x.Field<String>("id_no_old_5"),
                           HolderNo = x.Field<Int32?>("holder_no") == null ? 0 : x.Field<Int32>("holder_no"),
                           Title = x.Field<String>("title"),
                           Name1 = x.Field<String>("name_1"),
                           Name2 = x.Field<String>("name_2"),
                           Name3 = x.Field<String>("name_3"),
                           Name4 = x.Field<String>("name_4"),
                           Name5 = x.Field<String>("name_5"),
                           Addr1 = x.Field<String>("addr_1"),
                           Addr2 = x.Field<String>("addr_2"),
                           Addr3 = x.Field<String>("addr_3"),
                           Addr4 = x.Field<String>("addr_4"),
                           Postcode = x.Field<Int32?>("postcode") == null ? 0 : x.Field<Int32>("postcode"),
                           TelNo = x.Field<String>("tel_no"),
                           RegionCode = x.Field<Int32?>("region_code") == null ? 0 : x.Field<Int32>("region_code"),
                           StateCode = x.Field<Int32?>("state_code") == null ? 0 : x.Field<Int32>("state_code"),
                           CountryIssue = x.Field<String>("country_issue"),
                           CountryIssue1 = x.Field<String>("country_issue1"),
                           CountryIssue2 = x.Field<String>("country_issue2"),
                           CountryIssue3 = x.Field<String>("country_issue3"),
                           CountryRes = x.Field<String>("country_res"),
                           CountryIncorp = x.Field<String>("country_incorp"),
                           Nationality = x.Field<String>("nationality"),
                           Race = x.Field<String>("race"),
                           OccCode = x.Field<String>("occ_code"),
                           CorpStatus = x.Field<String>("corp_status"),
                           BusinessType = x.Field<String>("business_type"),
                           ContactPerson = x.Field<String>("contact_person"),
                           PositionHeld = x.Field<String>("position_held"),
                           AccountType = x.Field<String>("account_type"),
                           DivPymt = x.Field<String>("div_pymt"),
                           HolderCls = x.Field<String>("holder_cls"),
                           HolderInd = x.Field<String>("holder_ind"),
                           HolderStatus = x.Field<String>("holder_status"),
                           RegDt = x.Field<String>("reg_dt"),
                           RegBrn = x.Field<String>("reg_brn"),
                           BirthDt = x.Field<String>("birth_dt"),
                           Sex = x.Field<String>("sex"),
                           TelNo2 = x.Field<String>("tel_no_2"),
                           AgentCode = x.Field<String>("agent_code") == null ? "" : x.Field<String>("agent_code"),
                           AgentType = x.Field<String>("agent_type"),
                           AgentId = x.Field<Int32?>("agent_id") == null ? 0 : x.Field<Int32>("agent_id"),
                           IncomeTaxNo = x.Field<String>("income_tax_no"),
                           HandPhoneNo = x.Field<String>("hand_phone_no"),
                           OffPhoneNo = x.Field<String>("off_phone_no"),
                           PrmntAddr1 = x.Field<String>("prmnt_addr_1"),
                           PrmntAddr2 = x.Field<String>("prmnt_addr_2"),
                           PrmntAddr3 = x.Field<String>("prmnt_addr_3"),
                           PrmntAddr4 = x.Field<String>("prmnt_addr_4"),
                           PrmntPostCode = x.Field<Int32?>("prmnt_post_code") == null ? 0 : x.Field<Int32>("prmnt_post_code"),
                           PrmntRegion = x.Field<Int32?>("prmnt_region") == null ? 0 : x.Field<Int32>("prmnt_region"),
                           PrmntState = x.Field<String>("prmnt_state"),
                           Salutation = x.Field<String>("salutation"),
                           HAccType = x.Field<String>("h_acc_type"),
                           Marital = x.Field<String>("marital"),
                           NoOfDpndnt = x.Field<Int32?>("no_of_dpndnt") == null ? 0 : x.Field<Int32>("no_of_dpndnt"),
                           RcvMaterial = x.Field<String>("rcv_material"),
                           MaterialLan = x.Field<String>("material_lan"),
                           Field1 = x.Field<String>("field_1"),
                           Field2 = x.Field<String>("field_2"),
                           Field3 = x.Field<String>("field_3"),
                           Field4 = x.Field<String>("field_4"),
                           Field5 = x.Field<String>("field_5"),
                           Field6 = x.Field<String>("field_6"),
                           Field7 = x.Field<String>("field_7"),
                           Field8 = x.Field<String>("field_8"),
                           Field9 = x.Field<String>("field_9"),
                           Field10 = x.Field<String>("field_10"),
                           FieldDesc1 = x.Field<String>("field_desc_1"),
                           FieldDesc2 = x.Field<String>("field_desc_2"),
                           FieldDesc3 = x.Field<String>("field_desc_3"),
                           FieldDesc4 = x.Field<String>("field_desc_4"),
                           FieldDesc5 = x.Field<String>("field_desc_5"),
                           FieldDesc6 = x.Field<String>("field_desc_6"),
                           FieldDesc7 = x.Field<String>("field_desc_7"),
                           FieldDesc8 = x.Field<String>("field_desc_8"),
                           FieldDesc9 = x.Field<String>("field_desc_9"),
                           FieldDesc10 = x.Field<String>("field_desc_10"),
                           LastDisbDt = (x.IsNull("last_disb_dt") ? default(DateTime) : (x.Field<DateTime>("last_disb_dt"))),
                           NextDisbDt = (x.IsNull("next_disb_dt") ? default(DateTime) : (x.Field<DateTime>("next_disb_dt"))),
                           PendDisbDt = (x.IsNull("pend_disb_dt") ? default(DateTime) : (x.Field<DateTime>("pend_disb_dt"))),
                           DownloadInd = x.Field<String>("download_ind"),
                           IdentityInd = x.Field<String>("identity_ind"),
                           EpfIStatus = x.Field<String>("epf_i_status"),
                           EpfIEffDt = (x.IsNull("epf_i_eff_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_eff_dt"))),
                           EpfILstUpdDt = (x.IsNull("epf_i_lst_upd_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_lst_upd_dt"))),
                           EntDt = (x.IsNull("ent_dt") ? default(DateTime) : (x.Field<DateTime>("ent_dt"))),
                           EntTime = (x.IsNull("ent_time") ? default(DateTime) : (x.Field<DateTime>("ent_time"))),
                           EntTerm = x.Field<String>("ent_term"),
                           EntBy = x.Field<String>("ent_by"),
                           ChgDt = (x.IsNull("chg_dt") ? default(DateTime) : (x.Field<DateTime>("chg_dt"))),
                           ChgTime = (x.IsNull("chg_time") ? default(DateTime) : (x.Field<DateTime>("chg_time"))),
                           ChgTerm = x.Field<String>("chg_term"),
                           ChgBy = x.Field<String>("chg_by"),
                           OtpActSt = x.Field<String>("otp_act_st"),
                           OtpEmailAdd = x.Field<String>("otp_email_add"),
                           OtpMobileNo = x.Field<String>("otp_mobile_no"),
                           OtpExpiryDate = x.Field<DateTime?>("otp_expiry_date"),
                           OtpVersion = x.Field<String>("otp_version"),
                           OtpEntBy = x.Field<DateTime?>("otp_ent_by"),
                           OtpEntDt = x.Field<DateTime?>("otp_ent_dt"),
                           OtpTacCd = x.Field<DateTime?>("otp_tac_cd"),
                           NameOfEmployer = x.Field<String>("name_of_employer"),
                           OfficeAddress1 = x.Field<String>("office_address_1"),
                           OfficeAddress2 = x.Field<String>("office_address_2"),
                           OfficeAddress3 = x.Field<String>("office_address_3"),
                           OfficePostCode = x.Field<String>("office_postcode"),
                           OfficeCity = x.Field<String>("office_city"),
                           OfficeState = x.Field<String>("office_state"),
                           OfficeCountry = x.Field<String>("office_country"),
                           OfficeTelNo = x.Field<String>("office_tel_no"),
                           Nob = x.Field<String>("nob"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                string query = "select * from ma_holder_reg";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new MaHolderReg
                        {
                            Id = x.Field<Int32>("id"),
                            IdNo = x.Field<String>("id_no"),
                            IdNoOld = x.Field<String>("id_no_old"),
                            IdNo2 = x.Field<String>("id_no_2"),
                            IdNoOld2 = x.Field<String>("id_no_old_2"),
                            IdNo3 = x.Field<String>("id_no_3"),
                            IdNoOld3 = x.Field<String>("id_no_old_3"),
                            IdNo4 = x.Field<String>("id_no_4"),
                            IdNoOld4 = x.Field<String>("id_no_old_4"),
                            IdNo5 = x.Field<String>("id_no_5"),
                            IdNoOld5 = x.Field<String>("id_no_old_5"),
                            HolderNo = x.Field<Int32?>("holder_no") == null ? 0 : x.Field<Int32>("holder_no"),
                            Title = x.Field<String>("title"),
                            Name1 = x.Field<String>("name_1"),
                            Name2 = x.Field<String>("name_2"),
                            Name3 = x.Field<String>("name_3"),
                            Name4 = x.Field<String>("name_4"),
                            Name5 = x.Field<String>("name_5"),
                            Addr1 = x.Field<String>("addr_1"),
                            Addr2 = x.Field<String>("addr_2"),
                            Addr3 = x.Field<String>("addr_3"),
                            Addr4 = x.Field<String>("addr_4"),
                            Postcode = x.Field<Int32?>("postcode") == null ? 0 : x.Field<Int32>("postcode"),
                            TelNo = x.Field<String>("tel_no"),
                            RegionCode = x.Field<Int32?>("region_code") == null ? 0 : x.Field<Int32>("region_code"),
                            StateCode = x.Field<Int32?>("state_code") == null ? 0 : x.Field<Int32>("state_code"),
                            CountryIssue = x.Field<String>("country_issue"),
                            CountryIssue1 = x.Field<String>("country_issue1"),
                            CountryIssue2 = x.Field<String>("country_issue2"),
                            CountryIssue3 = x.Field<String>("country_issue3"),
                            CountryRes = x.Field<String>("country_res"),
                            CountryIncorp = x.Field<String>("country_incorp"),
                            Nationality = x.Field<String>("nationality"),
                            Race = x.Field<String>("race"),
                            OccCode = x.Field<String>("occ_code"),
                            CorpStatus = x.Field<String>("corp_status"),
                            BusinessType = x.Field<String>("business_type"),
                            ContactPerson = x.Field<String>("contact_person"),
                            PositionHeld = x.Field<String>("position_held"),
                            AccountType = x.Field<String>("account_type"),
                            DivPymt = x.Field<String>("div_pymt"),
                            HolderCls = x.Field<String>("holder_cls"),
                            HolderInd = x.Field<String>("holder_ind"),
                            HolderStatus = x.Field<String>("holder_status"),
                            RegDt = x.Field<String>("reg_dt"),
                            RegBrn = x.Field<String>("reg_brn"),
                            BirthDt = x.Field<String>("birth_dt"),
                            Sex = x.Field<String>("sex"),
                            TelNo2 = x.Field<String>("tel_no_2"),
                            AgentCode = x.Field<String>("agent_code") == null ? "" : x.Field<String>("agent_code"),
                            AgentType = x.Field<String>("agent_type"),
                            AgentId = x.Field<Int32?>("agent_id") == null ? 0 : x.Field<Int32>("agent_id"),
                            IncomeTaxNo = x.Field<String>("income_tax_no"),
                            HandPhoneNo = x.Field<String>("hand_phone_no"),
                            OffPhoneNo = x.Field<String>("off_phone_no"),
                            PrmntAddr1 = x.Field<String>("prmnt_addr_1"),
                            PrmntAddr2 = x.Field<String>("prmnt_addr_2"),
                            PrmntAddr3 = x.Field<String>("prmnt_addr_3"),
                            PrmntAddr4 = x.Field<String>("prmnt_addr_4"),
                            PrmntPostCode = x.Field<Int32?>("prmnt_post_code") == null ? 0 : x.Field<Int32>("prmnt_post_code"),
                            PrmntRegion = x.Field<Int32?>("prmnt_region") == null ? 0 : x.Field<Int32>("prmnt_region"),
                            PrmntState = x.Field<String>("prmnt_state"),
                            Salutation = x.Field<String>("salutation"),
                            HAccType = x.Field<String>("h_acc_type"),
                            Marital = x.Field<String>("marital"),
                            NoOfDpndnt = x.Field<Int32?>("no_of_dpndnt") == null ? 0 : x.Field<Int32>("no_of_dpndnt"),
                            RcvMaterial = x.Field<String>("rcv_material"),
                            MaterialLan = x.Field<String>("material_lan"),
                            Field1 = x.Field<String>("field_1"),
                            Field2 = x.Field<String>("field_2"),
                            Field3 = x.Field<String>("field_3"),
                            Field4 = x.Field<String>("field_4"),
                            Field5 = x.Field<String>("field_5"),
                            Field6 = x.Field<String>("field_6"),
                            Field7 = x.Field<String>("field_7"),
                            Field8 = x.Field<String>("field_8"),
                            Field9 = x.Field<String>("field_9"),
                            Field10 = x.Field<String>("field_10"),
                            FieldDesc1 = x.Field<String>("field_desc_1"),
                            FieldDesc2 = x.Field<String>("field_desc_2"),
                            FieldDesc3 = x.Field<String>("field_desc_3"),
                            FieldDesc4 = x.Field<String>("field_desc_4"),
                            FieldDesc5 = x.Field<String>("field_desc_5"),
                            FieldDesc6 = x.Field<String>("field_desc_6"),
                            FieldDesc7 = x.Field<String>("field_desc_7"),
                            FieldDesc8 = x.Field<String>("field_desc_8"),
                            FieldDesc9 = x.Field<String>("field_desc_9"),
                            FieldDesc10 = x.Field<String>("field_desc_10"),
                            LastDisbDt = (x.IsNull("last_disb_dt") ? default(DateTime) : (x.Field<DateTime>("last_disb_dt"))),
                            NextDisbDt = (x.IsNull("next_disb_dt") ? default(DateTime) : (x.Field<DateTime>("next_disb_dt"))),
                            PendDisbDt = (x.IsNull("pend_disb_dt") ? default(DateTime) : (x.Field<DateTime>("pend_disb_dt"))),
                            DownloadInd = x.Field<String>("download_ind"),
                            IdentityInd = x.Field<String>("identity_ind"),
                            EpfIStatus = x.Field<String>("epf_i_status"),
                            EpfIEffDt = (x.IsNull("epf_i_eff_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_eff_dt"))),
                            EpfILstUpdDt = (x.IsNull("epf_i_lst_upd_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_lst_upd_dt"))),
                            EntDt = (x.IsNull("ent_dt") ? default(DateTime) : (x.Field<DateTime>("ent_dt"))),
                            EntTime = (x.IsNull("ent_time") ? default(DateTime) : (x.Field<DateTime>("ent_time"))),
                            EntTerm = x.Field<String>("ent_term"),
                            EntBy = x.Field<String>("ent_by"),
                            ChgDt = (x.IsNull("chg_dt") ? default(DateTime) : (x.Field<DateTime>("chg_dt"))),
                            ChgTime = (x.IsNull("chg_time") ? default(DateTime) : (x.Field<DateTime>("chg_time"))),
                            ChgTerm = x.Field<String>("chg_term"),
                            ChgBy = x.Field<String>("chg_by"),
                            OtpActSt = x.Field<String>("otp_act_st"),
                            OtpEmailAdd = x.Field<String>("otp_email_add"),
                            OtpMobileNo = x.Field<String>("otp_mobile_no"),
                            OtpExpiryDate = x.Field<DateTime?>("otp_expiry_date"),
                            OtpVersion = x.Field<String>("otp_version"),
                            OtpEntBy = x.Field<DateTime?>("otp_ent_by"),
                            OtpEntDt = x.Field<DateTime?>("otp_ent_dt"),
                            OtpTacCd = x.Field<DateTime?>("otp_tac_cd"),
                            NameOfEmployer = x.Field<String>("name_of_employer"),
                            OfficeAddress1 = x.Field<String>("office_address_1"),
                            OfficeAddress2 = x.Field<String>("office_address_2"),
                            OfficeAddress3 = x.Field<String>("office_address_3"),
                            OfficePostCode = x.Field<String>("office_postcode"),
                            OfficeCity = x.Field<String>("office_city"),
                            OfficeState = x.Field<String>("office_state"),
                            OfficeCountry = x.Field<String>("office_country"),
                            OfficeTelNo = x.Field<String>("office_tel_no"),
                            Nob = x.Field<String>("nob"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ma_holder_reg";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<MaHolderReg>(propertyName);
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                string query = "select * from ma_holder_reg where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from ma_holder_reg where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new MaHolderReg
                        {
                            Id = x.Field<Int32>("id"),
                            IdNo = x.Field<String>("id_no"),
                            IdNoOld = x.Field<String>("id_no_old"),
                            IdNo2 = x.Field<String>("id_no_2"),
                            IdNoOld2 = x.Field<String>("id_no_old_2"),
                            IdNo3 = x.Field<String>("id_no_3"),
                            IdNoOld3 = x.Field<String>("id_no_old_3"),
                            IdNo4 = x.Field<String>("id_no_4"),
                            IdNoOld4 = x.Field<String>("id_no_old_4"),
                            IdNo5 = x.Field<String>("id_no_5"),
                            IdNoOld5 = x.Field<String>("id_no_old_5"),
                            HolderNo = x.Field<Int32?>("holder_no") == null ? 0 : x.Field<Int32>("holder_no"),
                            Title = x.Field<String>("title"),
                            Name1 = x.Field<String>("name_1"),
                            Name2 = x.Field<String>("name_2"),
                            Name3 = x.Field<String>("name_3"),
                            Name4 = x.Field<String>("name_4"),
                            Name5 = x.Field<String>("name_5"),
                            Addr1 = x.Field<String>("addr_1"),
                            Addr2 = x.Field<String>("addr_2"),
                            Addr3 = x.Field<String>("addr_3"),
                            Addr4 = x.Field<String>("addr_4"),
                            Postcode = x.Field<Int32?>("postcode") == null ? 0 : x.Field<Int32>("postcode"),
                            TelNo = x.Field<String>("tel_no"),
                            RegionCode = x.Field<Int32?>("region_code") == null ? 0 : x.Field<Int32>("region_code"),
                            StateCode = x.Field<Int32?>("state_code") == null ? 0 : x.Field<Int32>("state_code"),
                            CountryIssue = x.Field<String>("country_issue"),
                            CountryIssue1 = x.Field<String>("country_issue1"),
                            CountryIssue2 = x.Field<String>("country_issue2"),
                            CountryIssue3 = x.Field<String>("country_issue3"),
                            CountryRes = x.Field<String>("country_res"),
                            CountryIncorp = x.Field<String>("country_incorp"),
                            Nationality = x.Field<String>("nationality"),
                            Race = x.Field<String>("race"),
                            OccCode = x.Field<String>("occ_code"),
                            CorpStatus = x.Field<String>("corp_status"),
                            BusinessType = x.Field<String>("business_type"),
                            ContactPerson = x.Field<String>("contact_person"),
                            PositionHeld = x.Field<String>("position_held"),
                            AccountType = x.Field<String>("account_type"),
                            DivPymt = x.Field<String>("div_pymt"),
                            HolderCls = x.Field<String>("holder_cls"),
                            HolderInd = x.Field<String>("holder_ind"),
                            HolderStatus = x.Field<String>("holder_status"),
                            RegDt = x.Field<String>("reg_dt"),
                            RegBrn = x.Field<String>("reg_brn"),
                            BirthDt = x.Field<String>("birth_dt"),
                            Sex = x.Field<String>("sex"),
                            TelNo2 = x.Field<String>("tel_no_2"),
                            AgentCode = x.Field<String>("agent_code") == null ? "" : x.Field<String>("agent_code"),
                            AgentType = x.Field<String>("agent_type"),
                            AgentId = x.Field<Int32?>("agent_id") == null ? 0 : x.Field<Int32>("agent_id"),
                            IncomeTaxNo = x.Field<String>("income_tax_no"),
                            HandPhoneNo = x.Field<String>("hand_phone_no"),
                            OffPhoneNo = x.Field<String>("off_phone_no"),
                            PrmntAddr1 = x.Field<String>("prmnt_addr_1"),
                            PrmntAddr2 = x.Field<String>("prmnt_addr_2"),
                            PrmntAddr3 = x.Field<String>("prmnt_addr_3"),
                            PrmntAddr4 = x.Field<String>("prmnt_addr_4"),
                            PrmntPostCode = x.Field<Int32?>("prmnt_post_code") == null ? 0 : x.Field<Int32>("prmnt_post_code"),
                            PrmntRegion = x.Field<Int32?>("prmnt_region") == null ? 0 : x.Field<Int32>("prmnt_region"),
                            PrmntState = x.Field<String>("prmnt_state"),
                            Salutation = x.Field<String>("salutation"),
                            HAccType = x.Field<String>("h_acc_type"),
                            Marital = x.Field<String>("marital"),
                            NoOfDpndnt = x.Field<Int32?>("no_of_dpndnt") == null ? 0 : x.Field<Int32>("no_of_dpndnt"),
                            RcvMaterial = x.Field<String>("rcv_material"),
                            MaterialLan = x.Field<String>("material_lan"),
                            Field1 = x.Field<String>("field_1"),
                            Field2 = x.Field<String>("field_2"),
                            Field3 = x.Field<String>("field_3"),
                            Field4 = x.Field<String>("field_4"),
                            Field5 = x.Field<String>("field_5"),
                            Field6 = x.Field<String>("field_6"),
                            Field7 = x.Field<String>("field_7"),
                            Field8 = x.Field<String>("field_8"),
                            Field9 = x.Field<String>("field_9"),
                            Field10 = x.Field<String>("field_10"),
                            FieldDesc1 = x.Field<String>("field_desc_1"),
                            FieldDesc2 = x.Field<String>("field_desc_2"),
                            FieldDesc3 = x.Field<String>("field_desc_3"),
                            FieldDesc4 = x.Field<String>("field_desc_4"),
                            FieldDesc5 = x.Field<String>("field_desc_5"),
                            FieldDesc6 = x.Field<String>("field_desc_6"),
                            FieldDesc7 = x.Field<String>("field_desc_7"),
                            FieldDesc8 = x.Field<String>("field_desc_8"),
                            FieldDesc9 = x.Field<String>("field_desc_9"),
                            FieldDesc10 = x.Field<String>("field_desc_10"),
                            LastDisbDt = (x.IsNull("last_disb_dt") ? default(DateTime) : (x.Field<DateTime>("last_disb_dt"))),
                            NextDisbDt = (x.IsNull("next_disb_dt") ? default(DateTime) : (x.Field<DateTime>("next_disb_dt"))),
                            PendDisbDt = (x.IsNull("pend_disb_dt") ? default(DateTime) : (x.Field<DateTime>("pend_disb_dt"))),
                            DownloadInd = x.Field<String>("download_ind"),
                            IdentityInd = x.Field<String>("identity_ind"),
                            EpfIStatus = x.Field<String>("epf_i_status"),
                            EpfIEffDt = (x.IsNull("epf_i_eff_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_eff_dt"))),
                            EpfILstUpdDt = (x.IsNull("epf_i_lst_upd_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_lst_upd_dt"))),
                            EntDt = (x.IsNull("ent_dt") ? default(DateTime) : (x.Field<DateTime>("ent_dt"))),
                            EntTime = (x.IsNull("ent_time") ? default(DateTime) : (x.Field<DateTime>("ent_time"))),
                            EntTerm = x.Field<String>("ent_term"),
                            EntBy = x.Field<String>("ent_by"),
                            ChgDt = (x.IsNull("chg_dt") ? default(DateTime) : (x.Field<DateTime>("chg_dt"))),
                            ChgTime = (x.IsNull("chg_time") ? default(DateTime) : (x.Field<DateTime>("chg_time"))),
                            ChgTerm = x.Field<String>("chg_term"),
                            ChgBy = x.Field<String>("chg_by"),
                            OtpActSt = x.Field<String>("otp_act_st"),
                            OtpEmailAdd = x.Field<String>("otp_email_add"),
                            OtpMobileNo = x.Field<String>("otp_mobile_no"),
                            OtpExpiryDate = x.Field<DateTime?>("otp_expiry_date"),
                            OtpVersion = x.Field<String>("otp_version"),
                            OtpEntBy = x.Field<DateTime?>("otp_ent_by"),
                            OtpEntDt = x.Field<DateTime?>("otp_ent_dt"),
                            OtpTacCd = x.Field<DateTime?>("otp_tac_cd"),
                            NameOfEmployer = x.Field<String>("name_of_employer"),
                            OfficeAddress1 = x.Field<String>("office_address_1"),
                            OfficeAddress2 = x.Field<String>("office_address_2"),
                            OfficeAddress3 = x.Field<String>("office_address_3"),
                            OfficePostCode = x.Field<String>("office_postcode"),
                            OfficeCity = x.Field<String>("office_city"),
                            OfficeState = x.Field<String>("office_state"),
                            OfficeCountry = x.Field<String>("office_country"),
                            OfficeTelNo = x.Field<String>("office_tel_no"),
                            Nob = x.Field<String>("nob"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<MaHolderReg>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ma_holder_reg where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from ma_holder_reg where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                string query = "select * from ma_holder_reg where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new MaHolderReg
                        {
                            Id = x.Field<Int32>("id"),
                            IdNo = x.Field<String>("id_no"),
                            IdNoOld = x.Field<String>("id_no_old"),
                            IdNo2 = x.Field<String>("id_no_2"),
                            IdNoOld2 = x.Field<String>("id_no_old_2"),
                            IdNo3 = x.Field<String>("id_no_3"),
                            IdNoOld3 = x.Field<String>("id_no_old_3"),
                            IdNo4 = x.Field<String>("id_no_4"),
                            IdNoOld4 = x.Field<String>("id_no_old_4"),
                            IdNo5 = x.Field<String>("id_no_5"),
                            IdNoOld5 = x.Field<String>("id_no_old_5"),
                            HolderNo = x.Field<Int32?>("holder_no") == null ? 0 : x.Field<Int32>("holder_no"),
                            Title = x.Field<String>("title"),
                            Name1 = x.Field<String>("name_1"),
                            Name2 = x.Field<String>("name_2"),
                            Name3 = x.Field<String>("name_3"),
                            Name4 = x.Field<String>("name_4"),
                            Name5 = x.Field<String>("name_5"),
                            Addr1 = x.Field<String>("addr_1"),
                            Addr2 = x.Field<String>("addr_2"),
                            Addr3 = x.Field<String>("addr_3"),
                            Addr4 = x.Field<String>("addr_4"),
                            Postcode = x.Field<Int32?>("postcode") == null ? 0 : x.Field<Int32>("postcode"),
                            TelNo = x.Field<String>("tel_no"),
                            RegionCode = x.Field<Int32?>("region_code") == null ? 0 : x.Field<Int32>("region_code"),
                            StateCode = x.Field<Int32?>("state_code") == null ? 0 : x.Field<Int32>("state_code"),
                            CountryIssue = x.Field<String>("country_issue"),
                            CountryIssue1 = x.Field<String>("country_issue1"),
                            CountryIssue2 = x.Field<String>("country_issue2"),
                            CountryIssue3 = x.Field<String>("country_issue3"),
                            CountryRes = x.Field<String>("country_res"),
                            CountryIncorp = x.Field<String>("country_incorp"),
                            Nationality = x.Field<String>("nationality"),
                            Race = x.Field<String>("race"),
                            OccCode = x.Field<String>("occ_code"),
                            CorpStatus = x.Field<String>("corp_status"),
                            BusinessType = x.Field<String>("business_type"),
                            ContactPerson = x.Field<String>("contact_person"),
                            PositionHeld = x.Field<String>("position_held"),
                            AccountType = x.Field<String>("account_type"),
                            DivPymt = x.Field<String>("div_pymt"),
                            HolderCls = x.Field<String>("holder_cls"),
                            HolderInd = x.Field<String>("holder_ind"),
                            HolderStatus = x.Field<String>("holder_status"),
                            RegDt = x.Field<String>("reg_dt"),
                            RegBrn = x.Field<String>("reg_brn"),
                            BirthDt = x.Field<String>("birth_dt"),
                            Sex = x.Field<String>("sex"),
                            TelNo2 = x.Field<String>("tel_no_2"),
                            AgentCode = x.Field<String>("agent_code") == null ? "" : x.Field<String>("agent_code"),
                            AgentType = x.Field<String>("agent_type"),
                            AgentId = x.Field<Int32?>("agent_id") == null ? 0 : x.Field<Int32>("agent_id"),
                            IncomeTaxNo = x.Field<String>("income_tax_no"),
                            HandPhoneNo = x.Field<String>("hand_phone_no"),
                            OffPhoneNo = x.Field<String>("off_phone_no"),
                            PrmntAddr1 = x.Field<String>("prmnt_addr_1"),
                            PrmntAddr2 = x.Field<String>("prmnt_addr_2"),
                            PrmntAddr3 = x.Field<String>("prmnt_addr_3"),
                            PrmntAddr4 = x.Field<String>("prmnt_addr_4"),
                            PrmntPostCode = x.Field<Int32?>("prmnt_post_code") == null ? 0 : x.Field<Int32>("prmnt_post_code"),
                            PrmntRegion = x.Field<Int32?>("prmnt_region") == null ? 0 : x.Field<Int32>("prmnt_region"),
                            PrmntState = x.Field<String>("prmnt_state"),
                            Salutation = x.Field<String>("salutation"),
                            HAccType = x.Field<String>("h_acc_type"),
                            Marital = x.Field<String>("marital"),
                            NoOfDpndnt = x.Field<Int32?>("no_of_dpndnt") == null ? 0 : x.Field<Int32>("no_of_dpndnt"),
                            RcvMaterial = x.Field<String>("rcv_material"),
                            MaterialLan = x.Field<String>("material_lan"),
                            Field1 = x.Field<String>("field_1"),
                            Field2 = x.Field<String>("field_2"),
                            Field3 = x.Field<String>("field_3"),
                            Field4 = x.Field<String>("field_4"),
                            Field5 = x.Field<String>("field_5"),
                            Field6 = x.Field<String>("field_6"),
                            Field7 = x.Field<String>("field_7"),
                            Field8 = x.Field<String>("field_8"),
                            Field9 = x.Field<String>("field_9"),
                            Field10 = x.Field<String>("field_10"),
                            FieldDesc1 = x.Field<String>("field_desc_1"),
                            FieldDesc2 = x.Field<String>("field_desc_2"),
                            FieldDesc3 = x.Field<String>("field_desc_3"),
                            FieldDesc4 = x.Field<String>("field_desc_4"),
                            FieldDesc5 = x.Field<String>("field_desc_5"),
                            FieldDesc6 = x.Field<String>("field_desc_6"),
                            FieldDesc7 = x.Field<String>("field_desc_7"),
                            FieldDesc8 = x.Field<String>("field_desc_8"),
                            FieldDesc9 = x.Field<String>("field_desc_9"),
                            FieldDesc10 = x.Field<String>("field_desc_10"),
                            LastDisbDt = (x.IsNull("last_disb_dt") ? default(DateTime) : (x.Field<DateTime>("last_disb_dt"))),
                            NextDisbDt = (x.IsNull("next_disb_dt") ? default(DateTime) : (x.Field<DateTime>("next_disb_dt"))),
                            PendDisbDt = (x.IsNull("pend_disb_dt") ? default(DateTime) : (x.Field<DateTime>("pend_disb_dt"))),
                            DownloadInd = x.Field<String>("download_ind"),
                            IdentityInd = x.Field<String>("identity_ind"),
                            EpfIStatus = x.Field<String>("epf_i_status"),
                            EpfIEffDt = (x.IsNull("epf_i_eff_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_eff_dt"))),
                            EpfILstUpdDt = (x.IsNull("epf_i_lst_upd_dt") ? default(DateTime) : (x.Field<DateTime>("epf_i_lst_upd_dt"))),
                            EntDt = (x.IsNull("ent_dt") ? default(DateTime) : (x.Field<DateTime>("ent_dt"))),
                            EntTime = (x.IsNull("ent_time") ? default(DateTime) : (x.Field<DateTime>("ent_time"))),
                            EntTerm = x.Field<String>("ent_term"),
                            EntBy = x.Field<String>("ent_by"),
                            ChgDt = (x.IsNull("chg_dt") ? default(DateTime) : (x.Field<DateTime>("chg_dt"))),
                            ChgTime = (x.IsNull("chg_time") ? default(DateTime) : (x.Field<DateTime>("chg_time"))),
                            ChgTerm = x.Field<String>("chg_term"),
                            ChgBy = x.Field<String>("chg_by"),
                            OtpActSt = x.Field<String>("otp_act_st"),
                            OtpEmailAdd = x.Field<String>("otp_email_add"),
                            OtpMobileNo = x.Field<String>("otp_mobile_no"),
                            OtpExpiryDate = x.Field<DateTime?>("otp_expiry_date"),
                            OtpVersion = x.Field<String>("otp_version"),
                            OtpEntBy = x.Field<DateTime?>("otp_ent_by"),
                            OtpEntDt = x.Field<DateTime?>("otp_ent_dt"),
                            OtpTacCd = x.Field<DateTime?>("otp_tac_cd"),
                            NameOfEmployer = x.Field<String>("name_of_employer"),
                            OfficeAddress1 = x.Field<String>("office_address_1"),
                            OfficeAddress2 = x.Field<String>("office_address_2"),
                            OfficeAddress3 = x.Field<String>("office_address_3"),
                            OfficePostCode = x.Field<String>("office_postcode"),
                            OfficeCity = x.Field<String>("office_city"),
                            OfficeState = x.Field<String>("office_state"),
                            OfficeCountry = x.Field<String>("office_country"),
                            OfficeTelNo = x.Field<String>("office_tel_no"),
                            Nob = x.Field<String>("nob"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ma_holder_reg where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
