﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentPerformanceStatistics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetComparisonSeries(string[] selectedTransType, Int32 selectedPeriod)
        {
            User user = (User)HttpContext.Current.Session["user"];
            //if (user != null)
            //{
            //    string groupedTransactionTypes = "";
            //    bool isCommissionSelected = false;
            //    //split string data for sales,switch,redemption - commission
            //    for (int i = 0; i < selectedTransType.Length; i++)
            //    {

            //        if (selectedTransType[i] == "SA" || selectedTransType[i] == "RD" || selectedTransType[i] == "SW")
            //        {
            //            if (!String.IsNullOrEmpty(groupedTransactionTypes))
            //            {
            //                groupedTransactionTypes += ",";
            //            }
            //            groupedTransactionTypes = "'" + selectedTransType[i] + "'";
            //        }

            //        if (selectedTransType[i] == "COM")
            //        {
            //            isCommissionSelected = true;
            //        }
            //    }

            //    List<getStatsByYear> statByYearCommission = new List<getStatsByYear>();
            //    List<getStatsByYear> statByYearSales = new List<getStatsByYear>();
            //    List<getStatsByYear> statByYearRD = new List<getStatsByYear>();
            //    List<getComparisonData> comparisonDataByYear = new List<getComparisonData>();
            //    if (selectedPeriod == 1)
            //    {
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });
            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });
            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //    }
            //    if (selectedPeriod == 2)
            //    {
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });
            //    }
            //    if (selectedPeriod == 3)
            //    {
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });
            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });
            //    }
            //    if (selectedPeriod == 4)
            //    {
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });
            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //        });
            //    }
            //    if (selectedPeriod == 5)
            //    {
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //        });
            //        statByYearCommission.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //        });
            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });
            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //        });
            //        statByYearSales.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(DateTime.Now.Year),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //        });

            //        statByYearRD.Add(new getStatsByYear
            //        {
            //            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //        });
            //    }

            //    //To initialize 0 values for agent, who has no commission on a specific month.
            //    statByYearCommission.ForEach(y =>
            //    {
            //        List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
            //        // take first 3 years assign months, year and commval 0;
            //        for (int i = 0; i < 12; i++)
            //        {

            //            getStatsByMonth statMonth = new getStatsByMonth();
            //            statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
            //            statMonth.commVal = "0";
            //            statByMonths.Add(statMonth);

            //        }
            //        y.statByMonth = statByMonths;
            //    });

            //    //To get commission Data 
            //    if (isCommissionSelected)
            //    {
            //        string CommissionQueryString = @"select sum(comm_psc) as comm_psc, sum(comm_oc) as comm_oc, YEAR(updated_date) as updated_year, Month(updated_date)as updated_month from comm_ledger 
            //                        where agent_code = '" + user.AgentCode + "' group by month(updated_date) order by month(updated_date) asc";

            //        Response responseAgentCommission = GenericService.GetDataByQuery(CommissionQueryString, 0, 0, false, null, false, null, true);
            //        if (responseAgentCommission.IsSuccess)
            //        {
            //            var agentCommissionDyn = responseAgentCommission.Data;
            //            var responseAgentCommissionJSON = JsonConvert.SerializeObject(agentCommissionDyn);
            //            List<dynamic> agentCommissionList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentCommissionJSON);

            //            agentCommissionList.GroupBy(x => x.updated_year).ToList().ForEach(x =>
            //            {
            //                if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
            //                {
            //                    getStatsByYear singleStatByYear = new getStatsByYear
            //                    {
            //                        Year = x.First().updated_year,
            //                        statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = (Convert.ToDouble(x.First().comm_psc) + Convert.ToDouble(x.First().comm_oc)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
            //                    };

            //                    var yearIndex = statByYearCommission.IndexOf(statByYearCommission.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
            //                    var monthIndex = statByYearCommission[yearIndex].statByMonth.IndexOf(statByYearCommission[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
            //                    statByYearCommission[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
            //                }
            //            });
            //            statByYearCommission = statByYearCommission.OrderByDescending(x => x.Year).ToList();
            //        }

            //        //To get Buy, Redemption, Switch Data of Agent Clients.
            //        string querySR = @"select GROUP_CONCAT(uo.reject_reason) as trans_no, uo.order_type, uo.consultantId, uo.order_status, DAY(uo.updated_date) as updated_day, MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year
            //                        from user_orders uo
            //                        join user_accounts ua on uo.user_id = ua.user_id
            //                        join agent_clients ac on ua.user_id = ac.user_id
            //                        where ac.agent_code = '" + user.AgentCode + "'";
            //        Response responserSR = GenericService.GetDataByQuery(querySR, 0, 0, false, null, false, null, true);
            //        if (responserSR.IsSuccess)
            //        {
            //            var srDyn = responserSR.Data;
            //            var responseSR = JsonConvert.SerializeObject(srDyn);
            //            List<dynamic> agentSR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSR);

            //            string[] transNos = agentSR.FirstOrDefault().trans_no.ToString().Split(',');
            //            for (int i = 0; i < transNos.Length; i++)
            //            {
            //                transNos[i] = "'" + transNos[i] + "'";
            //            }
            //            string transactionsNo = String.Join(",", transNos);

            //            //To get sales and redemption data from oracle database
            //            string queryGetPMaholderSR = @"select holder_no,trans_no, TRANSAMT, TRANSTYPE, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as TRANSMONTH, extract( year from trans_dt) as TRANSYEAR from uts.holder_ledger where trans_no in (" + transactionsNo + ")";
            //            Response responseGetPersonalSR = ServicesManager.GetDataByQuery(queryGetPMaholderSR);
            //            if (responseGetPersonalSR.IsSuccess)
            //            {
            //                var getPersonalSRDyn = responseGetPersonalSR.Data;
            //                var responseGetPersonalSRData = JsonConvert.SerializeObject(getPersonalSRDyn);
            //                List<dynamic> personalSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetPersonalSRData);

            //                //Continue here
            //                personalSRList.GroupBy(x => x.TRANSYEAR).ToList().ForEach(x =>
            //                {
            //                    if (DateTime.Now.Year - Convert.ToInt32(x.First().TRANSYEAR) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().TRANSYEAR) < 6)
            //                    {
            //                        if (x.First().TRANSTYPE == "SA")
            //                        {
            //                            getStatsByYear singleStatByYear = new getStatsByYear
            //                            {
            //                                Year = x.First().TRANSYEAR,
            //                                statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH)), commVal = (Convert.ToDouble(x.First().TRANSAMT)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
            //                            };

            //                            var yearIndex = statByYearSales.IndexOf(statByYearSales.Where(i => i.Year == Convert.ToString(x.First().TRANSYEAR)).First());
            //                            var monthIndex = statByYearSales[yearIndex].statByMonth.IndexOf(statByYearSales[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH))));
            //                            statByYearSales[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
            //                        }

            //                        if (x.First().TRANSTYPE == "RD")
            //                        {
            //                            getStatsByYear singleStatByYear = new getStatsByYear
            //                            {
            //                                Year = x.First().TRANSYEAR,
            //                                statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH)), commVal = (Convert.ToDouble(x.First().TRANSAMT)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
            //                            };

            //                            var yearIndex = statByYearRD.IndexOf(statByYearRD.Where(i => i.Year == Convert.ToString(x.First().TRANSYEAR)).First());
            //                            var monthIndex = statByYearRD[yearIndex].statByMonth.IndexOf(statByYearRD[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH))));
            //                            statByYearRD[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
            //                        }

            //                    }
            //                });
            //                if (statByYearSales.Any())
            //                {
            //                    statByYearSales = statByYearSales.OrderByDescending(x => x.Year).ToList();
            //                }

            //                if (statByYearRD.Any())
            //                {
            //                    statByYearRD = statByYearRD.OrderByDescending(x => x.Year).ToList();
            //                }
            //            }
            //        }

            //        if (statByYearCommission.Any())
            //        {
            //            getComparisonData singleData = new getComparisonData
            //            {
            //                transName = "Commission",
            //                getComparisonStatByYear = statByYearCommission
            //            };
            //            comparisonDataByYear.Add(singleData);
            //        }

            //        if (statByYearSales.Any())
            //        {
            //            getComparisonData singleData = new getComparisonData
            //            {
            //                transName = "Sales",
            //                getComparisonStatByYear = statByYearSales
            //            };
            //            comparisonDataByYear.Add(singleData);
            //        }

            //        if (statByYearRD.Any())
            //        {
            //            getComparisonData singleData = new getComparisonData
            //            {
            //                transName = "Redemption",
            //                getComparisonStatByYear = statByYearRD
            //            };
            //            comparisonDataByYear.Add(singleData);
            //        }
            //    }

            //    return comparisonDataByYear;
            //}
            //else {
            //    return null;
            //}
            string groupedTransactionTypes = "";
            bool isCommissionSelected = false;
            //split string data for sales,switch,redemption - commission
            for (int i = 0; i < selectedTransType.Length; i++)
            {

                if (selectedTransType[i] == "SA" || selectedTransType[i] == "RD" || selectedTransType[i] == "SW")
                {
                    if (!String.IsNullOrEmpty(groupedTransactionTypes))
                    {
                        groupedTransactionTypes += ",";
                    }
                    groupedTransactionTypes = "'" + selectedTransType[i] + "'";
                }

                if (selectedTransType[i] == "COM")
                {
                    isCommissionSelected = true;
                }
            }

            List<getStatsByYear> statByYearCommission = new List<getStatsByYear>();
            List<getStatsByYear> statByYearSales = new List<getStatsByYear>();
            List<getStatsByYear> statByYearRD = new List<getStatsByYear>();
            List<getComparisonData> comparisonDataByYear = new List<getComparisonData>();

            //if (selectedPeriod == 1)
            //{
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });
            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //}

            //if (selectedPeriod == 2)
            //{
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });
            //}
            //if (selectedPeriod == 3)
            //{
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //}
            //if (selectedPeriod == 4)
            //{
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });
            //}
            //if (selectedPeriod == 5)
            //{
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //    });
            //}

            //if (selectedPeriod == 6)
            //{
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //    });
            //    statByYearCommission.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 5),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //    });
            //    statByYearSales.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 5),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(DateTime.Now.Year),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
            //    });

            //    statByYearRD.Add(new getStatsByYear
            //    {
            //        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 5),
            //    });
            //}

            //To get commission Data 
            if (isCommissionSelected)
            {
                if (selectedPeriod == 1)
                {
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });
                }

                if (selectedPeriod == 2)
                {
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                    });
                }
                if (selectedPeriod == 3)
                {
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                    });
                }
                if (selectedPeriod == 4)
                {
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                    });
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                    });
                }
                if (selectedPeriod == 5)
                {
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                    });
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                    });
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
                    });
                }

                if (selectedPeriod == 6)
                {
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                    });

                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                    });
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                    });
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
                    });
                    statByYearCommission.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 5),
                    });
                }

                //To initialize 0 values for agent, who has no commission on a specific month.
                statByYearCommission.ForEach(y =>
                {
                    List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                    // take first 3 years assign months, year and commval 0;
                    for (int i = 0; i < 12; i++)
                    {

                        getStatsByMonth statMonth = new getStatsByMonth();
                        statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                        statMonth.commVal = "0";
                        statByMonths.Add(statMonth);

                    }
                    y.statByMonth = statByMonths;
                });

                string CommissionQueryString = @"select sum(comm_psc) as comm_psc, sum(comm_oc) as comm_oc, YEAR(updated_date) as updated_year, Month(updated_date)as updated_month from comm_ledger 
                                    where agent_code = '" + user.AgentCode + "' group by month(updated_date) order by month(updated_date) asc";

                Response responseAgentCommission = GenericService.GetDataByQuery(CommissionQueryString, 0, 0, false, null, false, null, true);
                if (responseAgentCommission.IsSuccess)
                {
                    var agentCommissionDyn = responseAgentCommission.Data;
                    var responseAgentCommissionJSON = JsonConvert.SerializeObject(agentCommissionDyn);
                    List<dynamic> agentCommissionList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentCommissionJSON);

                    agentCommissionList.GroupBy(x => x.updated_year).ToList().ForEach(x =>
                    {
                        if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                        {
                            getStatsByYear singleStatByYear = new getStatsByYear
                            {
                                Year = x.First().updated_year,
                                statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = (Convert.ToDouble(x.First().comm_psc) + Convert.ToDouble(x.First().comm_oc)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
                            };

                            var yearIndex = statByYearCommission.IndexOf(statByYearCommission.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
                            var monthIndex = statByYearCommission[yearIndex].statByMonth.IndexOf(statByYearCommission[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
                            statByYearCommission[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                        }
                    });
                    statByYearCommission = statByYearCommission.OrderByDescending(x => x.Year).ToList();
                }
               
            }
            else {
                //To initialize 0 values for agent, who has no commission on a specific month.
                if (selectedTransType.Contains("SA")) {

                    if (selectedPeriod == 1)
                    {
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });
                    }

                    if (selectedPeriod == 2)
                    {
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });
                    }
                    if (selectedPeriod == 3)
                    {
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });
                    }
                    if (selectedPeriod == 4)
                    {
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                        });
                    }
                    if (selectedPeriod == 5)
                    {
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                        });
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
                        });
                    }

                    if (selectedPeriod == 6)
                    {
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                        });
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
                        });
                        statByYearSales.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 5),
                        });
                    }
                    statByYearSales.ForEach(y =>
                    {
                        List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                        // take first 3 years assign months, year and commval 0;
                        for (int i = 0; i < 12; i++)
                        {

                            getStatsByMonth statMonth = new getStatsByMonth();
                            statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                            statMonth.commVal = "0";
                            statByMonths.Add(statMonth);

                        }
                        y.statByMonth = statByMonths;
                    });
                }

                if (selectedTransType.Contains("RD")) {
                    if (selectedPeriod == 1)
                    {
                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                    }

                    if (selectedPeriod == 2)
                    {
                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });
                    }
                    if (selectedPeriod == 3)
                    {
                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });
                    }
                    if (selectedPeriod == 4)
                    {
                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                        });
                    }
                    if (selectedPeriod == 5)
                    {
                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
                        });
                    }

                    if (selectedPeriod == 6)
                    {
                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(DateTime.Now.Year),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 3),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 4),
                        });

                        statByYearRD.Add(new getStatsByYear
                        {
                            Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 5),
                        });
                    }

                    //To initialize 0 values for agent, who has no commission on a specific month.
                    statByYearRD.ForEach(y =>
                    {
                        List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                        // take first 3 years assign months, year and commval 0;
                        for (int i = 0; i < 12; i++)
                        {

                            getStatsByMonth statMonth = new getStatsByMonth();
                            statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                            statMonth.commVal = "0";
                            statByMonths.Add(statMonth);

                        }
                        y.statByMonth = statByMonths;
                    });
                }

                //To get Buy, Redemption, Switch Data of Agent Clients.
                //string querySR = @"select GROUP_CONCAT(uo.reject_reason) as trans_no, uo.order_type, uo.consultantId, uo.order_status, DAY(uo.updated_date) as updated_day, MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year
                //                    from user_orders uo
                //                    join user_accounts ua on uo.user_id = ua.user_id
                //                    join agent_clients ac on ua.user_id = ac.user_id
                //                    where ac.agent_code = '" + user.AgentCode + "'";

                string querySR = @"select GROUP_CONCAT(uo.reject_reason) as trans_no, uo.order_type, uo.consultantId, uo.order_status, DAY(uo.updated_date) as updated_day, MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year
                                    from user_orders uo
                                    join user_accounts ua on uo.user_id = ua.user_id
                                    join agent_clients ac on ua.user_id = ac.user_id
                                    where ac.agent_code = 'AG0012'";
                Response responserSR = GenericService.GetDataByQuery(querySR, 0, 0, false, null, false, null, true);
                if (responserSR.IsSuccess)
                {
                    var srDyn = responserSR.Data;
                    var responseSR = JsonConvert.SerializeObject(srDyn);
                    List<dynamic> agentSR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSR);

                    string[] transNos = agentSR.FirstOrDefault().trans_no.ToString().Split(',');
                    for (int i = 0; i < transNos.Length; i++)
                    {
                        transNos[i] = "'" + transNos[i] + "'";
                    }
                    string transactionsNo = String.Join(",", transNos);

                    //To get sales and redemption data from oracle database
                    //string queryGetPMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + transactionsNo + ")";
                    string queryGetPMaholderSR = @"select sum(trans_amt) as trans_amt, trans_type, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year 
                                                    from uts.holder_ledger where trans_no in (" + transactionsNo + ") group by trans_type, extract(month from trans_dt), extract( year from trans_dt)";
                    Response responseGetPersonalSR = ServicesManager.GetDataByQuery(queryGetPMaholderSR);
                    if (responseGetPersonalSR.IsSuccess)
                    {
                        var getPersonalSRDyn = responseGetPersonalSR.Data;
                        var responseGetPersonalSRData = JsonConvert.SerializeObject(getPersonalSRDyn);
                        List<dynamic> personalSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetPersonalSRData);

                        //Continue here
                        personalSRList.GroupBy(x =>  x.TRANSYEAR).ToList().ForEach(x =>
                        {
                            if (DateTime.Now.Year - Convert.ToInt32(x.First().TRANSYEAR) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().TRANSYEAR) < 6)
                            {
                                if (x.First().TRANSTYPE == "SA")
                                {
                                    getStatsByYear singleStatByYear = new getStatsByYear
                                    {
                                        Year = x.First().TRANSYEAR,
                                        statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH)), commVal = (Convert.ToDouble(x.First().TRANSAMT)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
                                    };

                                    var yearIndex = statByYearSales.IndexOf(statByYearSales.Where(i => i.Year == Convert.ToString(x.First().TRANSYEAR)).First());
                                    var monthIndex = statByYearSales[yearIndex].statByMonth.IndexOf(statByYearSales[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH))));
                                    statByYearSales[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                                }

                                if (x.First().TRANSTYPE == "RD")
                                {
                                    getStatsByYear singleStatByYear = new getStatsByYear
                                    {
                                        Year = x.First().TRANSYEAR,
                                        statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH)), commVal = (Convert.ToDouble(x.First().TRANSAMT)).ToString("N", CultureInfo.CurrentCulture) }).ToList()
                                    };

                                    var yearIndex = statByYearRD.IndexOf(statByYearRD.Where(i => i.Year == Convert.ToString(x.First().TRANSYEAR)).First());
                                    var monthIndex = statByYearRD[yearIndex].statByMonth.IndexOf(statByYearRD[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().TRANSMONTH))));
                                    statByYearRD[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                                }

                            }
                        });
                        if (statByYearSales.Any())
                        {
                            statByYearSales = statByYearSales.OrderByDescending(x => x.Year).ToList();
                        }

                        if (statByYearRD.Any())
                        {
                            statByYearRD = statByYearRD.OrderByDescending(x => x.Year).ToList();
                        }
                    }
                }
            }

            if (statByYearCommission.Any())
            {
                getComparisonData singleData = new getComparisonData
                {
                    transName = "Commission",
                    getComparisonStatByYear = statByYearCommission
                };
                comparisonDataByYear.Add(singleData);
            }

            if (statByYearSales.Any())
            {
                getComparisonData singleData = new getComparisonData
                {
                    transName = "Sales",
                    getComparisonStatByYear = statByYearSales
                };
                comparisonDataByYear.Add(singleData);
            }

            if (statByYearRD.Any())
            {
                getComparisonData singleData = new getComparisonData
                {
                    transName = "Redemption",
                    getComparisonStatByYear = statByYearRD
                };
                comparisonDataByYear.Add(singleData);
            }

            return comparisonDataByYear;
        }

        public class getStatsByMonth
        {
            public string Month { get; set; }
            public string commVal { get; set; }
        }

        public class getStatsByYear
        {

            public string Year { get; set; }
            public List<getStatsByMonth> statByMonth { get; set; }
        }

        public class getComparisonData {
            public string transName { get; set; }
            public List<getStatsByYear> getComparisonStatByYear { get; set; }
        }
    }
}