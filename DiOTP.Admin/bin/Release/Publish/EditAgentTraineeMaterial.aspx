﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditAgentTraineeMaterial.aspx.cs" Inherits="Admin.EditAgentTraineeMaterial" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="editAgentTraineeMaterial" runat="server">
        <div class="row" id="FormId" data-value="editAgentTraineeMaterial">
            <div class="col-md-12">
                <div class="row mb-15">
                    <div class="col-md-4">
                        <label>Material Name:</label>
                    </div>
                    <div class="col-md-8">
                        <asp:HiddenField ID="Id" Value="0" runat="server" ClientIDMode="Static" />
                        <asp:TextBox ID="materialName" runat="server" ReadOnly="true" CssClass="document-title form-control" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Material Description:</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="materialDescription" TextMode="multiline" Columns="50" Rows="5" runat="server" CssClass="document-title form-control" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Material File:</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="Url" runat="server" CssClass="document-title form-control" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                        <div class="fileupload fileupload-new" data-provides="fileupload" id="statusOption" runat="server" clientidmode="static">
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> <span id="btnFileText">Select file</span></span> <i class="fa fa-spinner fa-spin hide"></i>
                                        <asp:FileUpload ID="editBtn" runat="server" ClientIDMode="Static" CssClass="default filePicker" />
                                    </span>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $ui(document).ready(function () {

                function sendFile(file) {
                    var formData = new FormData();
                    formData.append('file', file);
                    $ui.ajax({
                        type: 'post',
                        url: 'fileUploadMaterial.ashx',
                        data: formData,
                        processData: false,
                        contentType: false,
                        //xhr: function () {  // Custom XMLHttpRequest
                        //    var myXhr = $ui.ajaxSettings.xhr();
                        //    if (myXhr.upload) { // Check if upload property exists
                        //        //update progressbar percent complete
                        //        //$ui('#uploadStatus').html('0%');
                        //        // For handling the progress of the upload
                        //        myXhr.upload.addEventListener('progress', progressHandlingFunction, false);

                        //    }
                        //    return myXhr;
                        //},
                        success: function (status) {
                            console.log(status);
                            $('#btnFileText').text("Select file");
                            $('#editBtn').removeAttr('disabled');
                            $('.fileupload .fa.fa-spinner').addClass('hide');
                            $('#Url').val(status);
                        },
                        error: function () {
                            $('.fileupload .fa.fa-spinner').addClass('hide');
                            $('#btnFileText').text("Select file");
                            $('#editBtn').removeAttr('disabled');
                            alert("Whoops something went wrong!");
                        }
                    });
                }
                //function sendFile(file) {
                //    var formData = new FormData();
                //    formData.append('file', file);
                //    $ui.ajax({
                //        type: 'post',
                //        url: 'fileUploaderDownloads.ashx',
                //        data: formData,
                //        success: function (status) {
                //            console.log(status);
                //            $('#btnFileText').text("Select file");
                //            $('#editBtn').removeAttr('disabled');
                //            $('.fileupload .fa.fa-spinner').addClass('hide');
                //            $('#Url').val(status);
                //        },
                //        processData: false,
                //        contentType: false,
                //        error: function () {
                //            $('.fileupload .fa.fa-spinner').addClass('hide');
                //            $('#btnFileText').text("Select file");
                //            $('#fuEditDocument').removeAttr('disabled');
                //            alert("Whoops something went wrong!");
                //        }
                //    });
                //}

                var _URL = window.URL || window.webkitURL;
                $ui('.filePicker').unbind('change');
                $ui('.filePicker').change(function () {
                    var file;
                    file = this.files[0];
                    console.log(file);
                    
                    var extensions = ["ppt","pptx","pdf"];
                    var fileLink = document.getElementById('editBtn').value;
                    var fileExtensionType = fileLink.split(".").pop();
                    console.log(fileLink);
                    console.log(fileExtensionType.value);
                    //Check if file extension is included in the list.
                    if (extensions.some(fileExt => fileExtensionType.includes(fileExt))) {
                        var fullFilePath = fileLink;
                        var lastDot = fullFilePath.lastIndexOf('\\')+1;
                        var fileName = fullFilePath.substring(lastDot, fullFilePath.length);
                        
                        console.log("This is a valid file type!");
                        $('#btnFileText').text("Loading...");
                        $('#editBtn').attr('disabled', 'disabled');
                        $('.fileupload .fa.fa-spinner').removeClass('hide');
                        sendFile(file);              
                    }
                    else {
                        document.getElementById('URL').value = "";
                        alert("Only Powerpoint or PDF files are allowed!");
                    }
                });

                $ui('.default-color-picker').colorpicker();

            });
        </script>
    </form>
</body>
</html>
