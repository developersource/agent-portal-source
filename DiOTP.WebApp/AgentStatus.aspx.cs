﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
            {
                if (Session["isVerified"].ToString() == "1")
                {
                    User user = (User)(Session["user"]);
                    string query = (@" select * from agent_regs where user_id='" + user.Id + "' and status=1 ");
                    Response responseList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                    if (responseList.IsSuccess)
                    {
                        var UsersDyn = responseList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                        List<AgentReg> aoRequestList = JsonConvert.DeserializeObject<List<AgentReg>>(responseJSON);
                        if (aoRequestList.Count == 1)
                        {
                            StringBuilder stringBuilder = new StringBuilder();
                            int idx = 1;
                            aoRequestList.ForEach(x =>
                            {
                                stringBuilder.Append(@"<tr>
                                        <td>" + idx + @"</td>
                                        <td>" + x.introducer_code + @"</td>
                                        <td>" + (x.process_status == 0 ? "Incomplete" : (x.process_status == 1 ? "Request Submitted" : (x.process_status == 22 ? "Processing by Agent" : (x.process_status == 2 ? "Recommended By Agent" : (x.process_status == 32 ? "Processing By Admin" : (x.process_status == 3 ? "Approved" : (x.process_status == 19 || x.process_status == 29 || x.process_status == 39 ? "Rejected" : ""))))))) + @"</td>
                                    </tr>");
                                idx++;
                            });
                            tbodySignups.InnerHtml = stringBuilder.ToString();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseList.Message + "\", '');", true);
                    }
                }
                else if (Session["isVerified"].ToString() != "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Not verified.\", '/Portfolio.aspx');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Session expired.\", '/Index.aspx');", true);
            }
        }
    }
}