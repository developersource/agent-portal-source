﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
namespace Admin
{
    public partial class AddCommissionSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] != null)
            {
                User sessionUser = (User)Session["admin"];
                string idString = Request.QueryString["Id"];

                if (idString != "0" && !String.IsNullOrEmpty(idString)){
                    string queryCSettingsReg = (@" SELECT * FROM commission_settings where id ='" + idString + "'");

                    Response responseCSettingsList = GenericService.GetDataByQuery(queryCSettingsReg, 0, 0, false, null, false, null, false);
                    if (responseCSettingsList.IsSuccess)
                    {
                        var csDyn = responseCSettingsList.Data;
                        var responseCSJSON = JsonConvert.SerializeObject(csDyn);
                        List<DiOTP.Utility.CommissionSettings> cds = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionSettings>>(responseCSJSON);

                        commissionSetting.Value = cds.FirstOrDefault().code.ToString();
                        Id.Value = cds.FirstOrDefault().id.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseCSettingsList.Message + "\");", true);
                    }
                }
            }
        }
    }
}