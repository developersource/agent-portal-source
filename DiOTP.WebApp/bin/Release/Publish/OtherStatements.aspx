﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="OtherStatements.aspx.cs" Inherits="DiOTP.WebApp.OtherStatements" %>
<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link href="/Content/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/fixedHeader.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/js/bootstrap-datepicker.css" rel="stylesheet" />
    <style>
        #accountSelectedDropdown {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active" id="statementTypeBC" runat="server">Other Statement</li>
                </ol>
            </div>
            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head mob-color-font" id="statementTypeH" runat="server">Other Statement</h3>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="statement">
                        <h5 class="hidden-lg hidden-md">Generate below for selected account:</h5>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-3 mb-8">
                                        <label>Account Number:</label>
                                        <asp:DropDownList ID="ddlFundAccount" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 mb-8 hide">
                                        <label>Statement Type:</label>
                                        <asp:DropDownList ID="ddlStatementType" runat="server" CssClass="form-control" ClientIDMode="Static">
                                            <asp:ListItem Value="all" Text="All"></asp:ListItem>
                                            <asp:ListItem Value="ID" Text="Income Distribution"></asp:ListItem>
                                            <asp:ListItem Value="US" Text="Unit Split"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 mb-8">
                                        <label>Fund:</label>
                                        <asp:DropDownList ID="ddlFundList" runat="server" CssClass="form-control" ClientIDMode="Static">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 mb-8">
                                        <label>From</label>
                                        <asp:DropDownList ID="ddlFromMonth" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                                        <%--<asp:TextBox ID="txtDateStart" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>--%>
                                        <%--<asp:DropDownList ID="ddlFromYear" runat="server" CssClass="form-control" ClientIDMode="Static" Style="display: none;"></asp:DropDownList>--%>
                                    </div>
                                    <div class="col-md-3 mb-8">
                                        <label>To</label>
                                        <asp:DropDownList ID="ddlToMonth" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                                        <%--<asp:TextBox ID="txtDateEnd" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>--%>
                                        <%--<asp:DropDownList ID="ddlToYear" runat="server" CssClass="form-control" ClientIDMode="Static" Style="display: none;"></asp:DropDownList>--%>
                                    </div>
                                    <div class="col-md-3 mb-8 hide">
                                        <label>From</label>
                                        <asp:TextBox ID="txtDateStart" runat="server" TextMode="Month" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                        <%--<asp:DropDownList ID="ddlFromYear" runat="server" CssClass="form-control" ClientIDMode="Static" Style="display: none;"></asp:DropDownList>--%>
                                    </div>
                                    <div class="col-md-3 mb-8 hide">
                                        <label>To</label>
                                        <asp:TextBox ID="txtDateEnd" runat="server" TextMode="Month" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                        <%--<asp:DropDownList ID="ddlToYear" runat="server" CssClass="form-control" ClientIDMode="Static" Style="display: none;"></asp:DropDownList>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-8">
                                <asp:Button ID="btnGenerate" runat="server" Text="Submit" CssClass="btn btn-block btn-infos1 mt-30" OnClientClick="return clientStatementValidation()" OnClick="btnGenerate_Click" />
                            </div>
                        </div>
                    </div>

                    <h5>Statement : <span id="spanUsername" runat="server"></span></h5>
                    <table class="table table-font-size-13 table-cell-pad-5">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Account</th>
                                <th>Fund</th>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyStatementDownload" runat="server">
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="/Content/js/jquery.dataTables.min.js"></script>
    <script src="/Content/js/dataTables.bootstrap.min.js"></script>
    <script src="/Content/js/dataTables.responsive.min.js"></script>
    <script src="/Content/js/responsive.bootstrap.min.js"></script>
    <script src="Content/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(document).ready(function () {

            $("#txtDateStart").datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                startView: 2
            }).on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#txtDateEnd').datepicker('setStartDate', minDate);
            });

            $("#txtDateEnd").datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                startView: 2
            })
                .on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $('#startdate').datepicker('setEndDate', minDate);
                });

            //$("#ddlStatementType").change(function () {
            //    if ($('#ddlStatementType').val() == "0") {
            //        $('#txtDateStart').hide();
            //        $('#txtDateEnd').hide();
            //        $('#ddlFromYear').hide();
            //        $('#ddlToYear').hide();
            //    }
            //    else if ($('#ddlStatementType').val() == "1") {
            //        $('#txtDateStart').show();
            //        $('#txtDateEnd').show();
            //        $('#ddlFromYear').hide();
            //        $('#ddlToYear').hide();
            //    }
            //    else if ($('#ddlStatementType').val() == "3") {
            //        $('#txtDateStart').hide();
            //        $('#txtDateEnd').hide();
            //        $('#ddlFromYear').show();
            //        $('#ddlToYear').show();
            //    }
            //});

            //if ($('#ddlStatementType').val() == "0") {
            //    $('#txtDateStart').hide();
            //    $('#txtDateEnd').hide();
            //    $('#ddlFromYear').hide();
            //    $('#ddlToYear').hide();
            //}
            //else if ($('#ddlStatementType').val() == "1") {
            //    $('#txtDateStart').show();
            //    $('#txtDateEnd').show();
            //    $('#ddlFromYear').hide();
            //    $('#ddlToYear').hide();
            //}
            //else if ($('#ddlStatementType').val() == "3") {
            //    $('#txtDateStart').hide();
            //    $('#txtDateEnd').hide();
            //    $('#ddlFromYear').show();
            //    $('#ddlToYear').show();
            //}
        });

        function clientStatementValidation() {
            var isValid = false;
            if ($('#ddlStatementType').val() == "0") {
                ShowCustomMessage('Alert', 'Please select Statement Type', '');
                return false;
            }
            else if ($('#ddlStatementType').val() == "1") {
                if ($('#txtDateStart').val() == "") {
                    ShowCustomMessage('Alert', 'Please select a Month(From)', '');
                    return false;
                }
                else if ($('#txtDateEnd').val() == "") {
                    ShowCustomMessage('Alert', 'Please select a Month(To)', '');
                    return false;
                }
                else if ($('#txtDateStart').val() > $('#txtDateEnd').val()) {
                    ShowCustomMessage('Alert', 'Month(From) cannot be greater than Month(To)', '');
                    return false;
                }
                else {
                    isValid = true;
                }
            }
            else if ($('#ddlStatementType').val() == "3") {
                if ($('#ddlFromYear').val() == "") {
                    ShowCustomMessage('Alert', 'Please select a Year(From)', '');
                    return false;
                }
                else if ($('#ddlToYear').val() == "") {
                    ShowCustomMessage('Alert', 'Please select a Year(To)', '');
                    return false;
                }
                else if ($('#ddlFromYear').val() > $('#ddlToYear').val()) {
                    ShowCustomMessage('Alert', 'Year(From) cannot be greater than Year(To)', '');
                    return false;
                }
                else {
                    isValid = true;
                }
            }
            if (isValid == true) {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }
    </script>
</asp:Content>
