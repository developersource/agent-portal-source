﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ViewClient : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        public static List<UtmcFundInformation> UTMCFundInformations = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                }
            }
            if (Session["isVerified"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
            }
            if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
            {
                if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "1")
                {
                    User user = (User)(Session["user"]);
                    string id = Request.QueryString["id"];
                    if (!String.IsNullOrEmpty(id))
                    {
                        if (UTMCFundInformations == null)
                        {
                            Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" 1=1 ", 0, 0, false);
                            if (responseUFIList.IsSuccess)
                            {
                                UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                            }
                        }
                        string query = (@" select u.* from agent_clients ac 
                                join users u on u.id = ac.user_id 
                                where u.id='" + id + @"' and ac.agent_user_id = '" + user.Id + "' and ac.is_active=1 and ac.status=1 ");

                        Response responseList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);
                        if (responseList.IsSuccess)
                        {
                            var UsersDyn = responseList.Data;
                            var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                            List<User> clients = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(responseJSON);
                            if (clients.Count == 1)
                            {
                                User x = clients.First();
                                StringBuilder stringBuilder = new StringBuilder();
                                Response responseUA = IUserAccountService.GetDataByFilter(" user_id='" + x.Id + "' ", 0, 0, false);
                                if (responseUA.IsSuccess)
                                {
                                    Decimal consolidatedTotalInvestmentRM = 0;
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUA.Data;
                                    List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestmentsConsolidated = new List<UtmcDetailedMemberInvestment>();

                                    //
                                    List<HolderLedger> consolidatedLedgersAll = new List<HolderLedger>();
                                    List<UtmcMemberInvestment> consolidatedInvestments = new List<UtmcMemberInvestment>();
                                    List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();

                                    //

                                    userAccounts.ForEach(uA =>
                                    {
                                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                                        MaHolderReg maHolderReg = new MaHolderReg();
                                        if (responseMHR.IsSuccess)
                                        {
                                            maHolderReg = (MaHolderReg)responseMHR.Data;
                                            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = new List<UtmcDetailedMemberInvestment>();
                                            utmcDetailedMemberInvestments = ServicesManager.GetMAAccountSummary(uA.AccountNo, UTMCFundInformations);

                                            //-----------------------------------------------------------------------
                                            Decimal UnrealisedGain = 0;
                                            Decimal UnrealisedGainPer = 0;
                                            if (utmcDetailedMemberInvestments.Count > 0)
                                            {

                                                decimal InvestUnitsIP = 0;

                                                List<HolderLedger> consolidatedLedgers = new List<HolderLedger>();
                                                utmcDetailedMemberInvestments.ForEach(udi =>
                                                {
                                                    consolidatedLedgers.AddRange(udi.holderLedgers);
                                                    consolidatedLedgersAll.AddRange(udi.holderLedgers);
                                                });
                                                //NEW
                                                List<HolderLedger> holderLedgersFiltered = consolidatedLedgers;
                                                consolidatedLedgers.ForEach(checkX =>
                                                {
                                                    if (checkX.TransNO.Contains('X'))
                                                    {
                                                        string transNoX = checkX.TransNO;
                                                        string transNo = checkX.TransNO.TrimEnd('X');
                                                        holderLedgersFiltered = holderLedgersFiltered.Where(hlf => hlf.TransNO != transNo && hlf.TransNO != transNoX).ToList();
                                                    }
                                                });
                                                holderLedgersFiltered = holderLedgersFiltered.OrderBy(hlf => hlf.TransDt).ThenBy(hlf => Convert.ToInt32(hlf.TransNO)).ToList();

                                                holderLedgersFiltered.ForEach(calcLedger =>
                                                {
                                                    decimal Actual_Cost = 0;
                                                    List<HolderLedger> holderLedgerIn = holderLedgersFiltered.Where(hlf => hlf.FundID == calcLedger.FundID && hlf.HolderNo == calcLedger.HolderNo && hlf.TransDt <= calcLedger.TransDt).OrderBy(hlf => hlf.TransDt).ThenBy(hlf => hlf.SortSEQ).ThenByDescending(hlf => hlf.TransType).ToList();
                                                    holderLedgerIn.ForEach(hl =>
                                                    {
                                                        string Trans_Type = hl.TransType;
                                                        if (Trans_Type == "SA")
                                                        {
                                                            if (!(hl.AccType == "SW"))
                                                            {
                                                                Actual_Cost += hl.TransAmt != null ? hl.TransAmt.Value : 0;
                                                                Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                            }
                                                        }
                                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                                        {
                                                            if (hl.CurUnitHLDG > 0)
                                                            {
                                                                if (!(hl.AccType == "SW"))
                                                                {
                                                                    Actual_Cost += hl.TransAmt != null ? hl.TransAmt.Value : 0;
                                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (!(hl.AccType == "SW"))
                                                                {
                                                                    Actual_Cost -= Convert.ToDecimal(hl.TransAmt);
                                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                                }
                                                            }
                                                        }
                                                    });
                                                });
                                                var groupLedgerByYear = holderLedgersFiltered
                                                                                    .GroupBy(u => u.TransDt.Value.Year)
                                                                                   .Select(grp => grp.ToList())
                                                                                   .ToList();
                                                decimal TotalUnitsByCurMarketValue = 0;
                                                decimal TotalUnitsByYearMarketValue = 0;
                                                List<UtmcCompositionalTransaction> objUtmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
                                                decimal InvestmentMarketValue = 0;
                                                groupLedgerByYear.ForEach(ledger =>
                                                {
                                                    decimal NetInvestmentCost = 0;
                                                    decimal GainLoss = 0;
                                                    var groupByFundList = ledger.GroupBy(l => l.FundID).Select(grp => grp.OrderBy(y => y.TransDt).ToList()).ToList();
                                                    groupByFundList.ForEach(ledgerIn =>
                                                    {
                                                        UtmcCompositionalTransaction utmcCompositionalTransaction = new UtmcCompositionalTransaction();
                                                        ledgerIn.ForEach(calcLedger =>
                                                        {

                                                            //COST_________________________________________________________________
                                                            decimal Cost_RM = 0.00M;
                                                            string IPD_Fund_Code = calcLedger.FundID;
                                                            string EPF_No = "";//rdr[1].ToString();
                                                            string IPD_Member_Acc_No = calcLedger.HolderNo.ToString();
                                                            DateTime dt = calcLedger.TransDt.Value;

                                                            var startDate = new DateTime(dt.Year, dt.Month, 1);
                                                            var endDate = startDate.AddMonths(1).AddDays(-1);
                                                            string LastDay = endDate.ToString("yyyy-MM-dd");

                                                            string Effective_Date = dt.ToString("yyyy-MM-dd");
                                                            string Settlementdate = calcLedger.TransDt.Value.ToString("yyyy-MM-dd");
                                                            DateTime Settlementdatetime = new DateTime();
                                                            if (Settlementdate == "")
                                                            {
                                                                Settlementdate = Effective_Date;
                                                                Settlementdatetime = dt;
                                                            }
                                                            else
                                                            {
                                                                DateTime dt1 = calcLedger.TransDt.Value;
                                                                Settlementdate = dt1.ToString("yyyy-MM-dd");
                                                                Settlementdatetime = dt1;
                                                            }
                                                            string IPD_Unique_Transaction_ID = calcLedger.TransNO;
                                                            string Transaction_Code = calcLedger.TransType;
                                                            string AccountType = calcLedger.AccType;
                                                            string Reversed_Transaction_ID = "";
                                                            decimal AverageCost = calcLedger.HLDRAveCost != null ? calcLedger.HLDRAveCost.Value : 0;

                                                            string Sort_SEQ = calcLedger.SortSEQ.ToString();

                                                            decimal Units = Math.Abs(calcLedger.TransUnits != null ? calcLedger.TransUnits.Value : 0);
                                                            decimal Gross_Amount_RM = calcLedger.TransAmt != null ? calcLedger.TransAmt.Value : 0;
                                                            decimal Net_Amount_RM = calcLedger.UnitValue != null ? calcLedger.UnitValue.Value : 0;
                                                            decimal Fees_RM = calcLedger.FeeAMT != null ? calcLedger.FeeAMT.Value : 0;
                                                            decimal GST_RM = calcLedger.GstAMT.Value;
                                                            decimal Proceeds_RM = 0.00M;
                                                            decimal Realised_Gain_Loss = 0.00M;
                                                            AverageCost = decimal.Round(AverageCost, 4);
                                                            char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];

                                                            if (Check_reversed == 'X')
                                                            {
                                                                if (Transaction_Code == "SA")
                                                                {

                                                                    if (AccountType == "SW")
                                                                    {
                                                                        Transaction_Code = "XI";
                                                                        Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                                    }
                                                                    else
                                                                    {
                                                                        Transaction_Code = "XS";
                                                                        Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                                    }



                                                                }
                                                                else if (Transaction_Code == "RD")
                                                                {

                                                                    if (AccountType == "SW")
                                                                    {
                                                                        Transaction_Code = "XO";
                                                                        Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                                    }
                                                                    else
                                                                    {
                                                                        Transaction_Code = "XR";
                                                                        Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                                    }
                                                                }

                                                                else if (Transaction_Code == "DD")
                                                                {

                                                                    if (Units == 0)
                                                                    {
                                                                        Transaction_Code = "XD";
                                                                        Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                                    }
                                                                    else
                                                                    {
                                                                        Transaction_Code = "XV";
                                                                        Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                                    }

                                                                }
                                                            }

                                                            if ((Transaction_Code == "SA") || (Transaction_Code == "DD"))
                                                            {

                                                            }
                                                            if ((Transaction_Code == "SA") && (AccountType == "SW"))
                                                            {
                                                                Transaction_Code = "SI";

                                                            }
                                                            if ((Transaction_Code == "RD") && (AccountType == "SW"))
                                                            {
                                                                Transaction_Code = "SO";
                                                                //Gross_Amount_RM = 0.00M;                  // as per request SO
                                                                Net_Amount_RM = 0.00M;

                                                            }

                                                            if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO") || (Transaction_Code == "XR") || (Transaction_Code == "XD") || (Transaction_Code == "XO") || (Transaction_Code == "XV"))
                                                            {
                                                                Proceeds_RM = Gross_Amount_RM;

                                                                Realised_Gain_Loss = Proceeds_RM - Cost_RM;

                                                                Proceeds_RM = decimal.Round(Proceeds_RM, 2);
                                                                Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);


                                                            }

                                                            //filter by sign requirement 

                                                            if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO")
                                                                            || (Transaction_Code == "XS") || (Transaction_Code == "XC") || (Transaction_Code == "XD")
                                                                            || (Transaction_Code == "XV") || (Transaction_Code == "UX") || (Transaction_Code == "XI")
                                                                            || (Transaction_Code == "RO") || (Transaction_Code == "RI") || (Transaction_Code == "HO") || (Transaction_Code == "XO"))
                                                            {

                                                                if (!(Net_Amount_RM == 0))
                                                                {
                                                                    Net_Amount_RM = -Net_Amount_RM;

                                                                }
                                                                if (!(Fees_RM == 0))
                                                                {
                                                                    Fees_RM = -Fees_RM;

                                                                }
                                                                if (!(GST_RM == 0))
                                                                {
                                                                    GST_RM = -GST_RM;

                                                                }
                                                                Gross_Amount_RM = -Gross_Amount_RM;

                                                                if (!(Transaction_Code == "XO"))
                                                                {
                                                                    Units = -Units;
                                                                }
                                                                if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO"))
                                                                {
                                                                    Proceeds_RM = -Proceeds_RM;
                                                                }


                                                            }
                                                            ////////// hard code  adjustment Start

                                                            if (Transaction_Code == "SA")
                                                            {
                                                                Transaction_Code = "NS";
                                                                // final  aaa ---
                                                                Cost_RM = Gross_Amount_RM;
                                                                // final  aaa ---

                                                            }
                                                            if (Transaction_Code == "SI")
                                                            {
                                                                // final  aaa ---
                                                                Cost_RM = Gross_Amount_RM;
                                                                // final  aaa ---


                                                            }
                                                            if (Transaction_Code == "DD")
                                                            {
                                                                Transaction_Code = "DV";
                                                                Realised_Gain_Loss = 0.00M;
                                                            }




                                                            if (Transaction_Code == "XD")
                                                            {
                                                                Realised_Gain_Loss = 0.00M;
                                                                Proceeds_RM = 0.00M;

                                                            }

                                                            if (Transaction_Code == "XV")
                                                            {
                                                                Realised_Gain_Loss = 0.00M;
                                                                Proceeds_RM = 0.00M;
                                                            }




                                                            if (Transaction_Code == "RD")
                                                            {
                                                                Transaction_Code = "RE";
                                                                Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);


                                                                Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);

                                                                // final  aaa ---
                                                                Gross_Amount_RM = Proceeds_RM;
                                                                Net_Amount_RM = Proceeds_RM;
                                                                // final  aaa ---

                                                                Cost_RM = -Cost_RM;
                                                            }
                                                            if (Transaction_Code == "TR")
                                                            {
                                                                Transaction_Code = "TO";
                                                                Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);
                                                                Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                                                // final  aaa ---
                                                                Gross_Amount_RM = Proceeds_RM;
                                                                Net_Amount_RM = Proceeds_RM;
                                                                // final  aaa ---
                                                            }


                                                            if (Transaction_Code == "SO" || Transaction_Code == "XO")
                                                            {
                                                                Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);
                                                                Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                                                // final  aaa ---
                                                                Gross_Amount_RM = Proceeds_RM;
                                                                Net_Amount_RM = Proceeds_RM;
                                                                // final  aaa ---
                                                                Cost_RM = -Cost_RM;
                                                            }

                                                            if (Transaction_Code == "XO")
                                                            {
                                                                Cost_RM = -Cost_RM;
                                                                Realised_Gain_Loss = -Realised_Gain_Loss;

                                                            }

                                                            // FILTERED 0 UNIT DIVIDEND 
                                                            Boolean bolFILTER = true;
                                                            if (Transaction_Code == "DV")
                                                            {
                                                                if (Units == 0)
                                                                {
                                                                    bolFILTER = false;
                                                                }

                                                            }

                                                            if (Transaction_Code == "XS")

                                                            {
                                                                Cost_RM = Gross_Amount_RM;

                                                            }


                                                            if (Transaction_Code == "XI" || Transaction_Code == "XR")
                                                            {
                                                                Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);

                                                                if (Transaction_Code == "XI")
                                                                {
                                                                    Cost_RM = Gross_Amount_RM;
                                                                }

                                                                if (Transaction_Code == "XR")
                                                                {
                                                                    Realised_Gain_Loss = -(Proceeds_RM - Cost_RM);
                                                                    Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);

                                                                }

                                                            }

                                                            if (Transaction_Code == "DV" || Transaction_Code == "XV")
                                                            {
                                                                Net_Amount_RM = Gross_Amount_RM;
                                                            }
                                                            if (bolFILTER)
                                                            {
                                                                utmcCompositionalTransaction = new UtmcCompositionalTransaction()
                                                                {
                                                                    IpdFundCode = IPD_Fund_Code,
                                                                    MemberEpfNo = EPF_No,
                                                                    IpdMemberAccNo = IPD_Member_Acc_No,
                                                                    EffectiveDate = endDate,
                                                                    DateOfTransaction = dt,
                                                                    DateOfSettlement = Settlementdatetime,
                                                                    IpdUniqueTransactionId = IPD_Unique_Transaction_ID,
                                                                    TransactionCode = Transaction_Code,
                                                                    ReversedTransactionId = Reversed_Transaction_ID,
                                                                    Units = Units,
                                                                    ActualUnits = objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault().ActualUnits + Units : Units,
                                                                    GrossAmountRm = Gross_Amount_RM,
                                                                    NetAmountRm = Net_Amount_RM,
                                                                    FeesRm = Fees_RM,
                                                                    GstRm = GST_RM,
                                                                    CostRm = Cost_RM,
                                                                    ActualCost = objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault().ActualCost + Cost_RM : Cost_RM,
                                                                    ProceedsRm = objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault().ProceedsRm.Value + Proceeds_RM : Proceeds_RM,
                                                                    RealisedGainLoss = objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(uct => uct.IpdFundCode == IPD_Fund_Code).LastOrDefault().RealisedGainLoss.Value + Realised_Gain_Loss : Realised_Gain_Loss,
                                                                    ReportDate = endDate,
                                                                    TransPR = calcLedger.TransPr != null ? calcLedger.TransPr.Value : 0,
                                                                    AverageCost = calcLedger.HLDRAveCost != null ? calcLedger.HLDRAveCost.Value : 0
                                                                };
                                                                objUtmcCompositionalTransactions.Add(utmcCompositionalTransaction);
                                                            }

                                                            //COST_________________________________________________________________
                                                        });


                                                        UtmcFundInformation uFI = UTMCFundInformations.Where(uf => uf.IpdFundCode == ledgerIn.FirstOrDefault().FundID).FirstOrDefault();
                                                        InvestUnitsIP += ledgerIn.Sum(l => (l.TransUnits != null ? l.TransUnits.Value : 0));
                                                        NetInvestmentCost += utmcCompositionalTransaction.CostRm.Value;
                                                        GainLoss += utmcCompositionalTransaction.RealisedGainLoss.Value;
                                                        TotalUnitsByCurMarketValue += (ledgerIn.Sum(l => ((l.TransUnits != null ? l.TransUnits.Value : 0) * uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)));
                                                        if (ledgerIn.FirstOrDefault().TransDt.Value.Year == DateTime.Now.Year)
                                                        {
                                                            TotalUnitsByYearMarketValue = TotalUnitsByCurMarketValue;
                                                        }
                                                        else
                                                        {
                                                            TotalUnitsByYearMarketValue += (ledgerIn.Sum(l => ((l.TransUnits != null ? l.TransUnits.Value : 0) * (l.TransPr != null ? l.TransPr.Value : 0))));
                                                        }
                                                    });

                                                    decimal NetInvestmentCostAllFunds = 0;
                                                    decimal GainLossAllFunds = 0;
                                                    objUtmcCompositionalTransactions.GroupBy(uct => uct.IpdFundCode).Select(grp => grp.ToList()).ToList().ForEach(uct =>
                                                    {
                                                        NetInvestmentCostAllFunds += uct.LastOrDefault().ActualCost;
                                                        GainLossAllFunds += uct.LastOrDefault().RealisedGainLoss.Value;
                                                    });
                                                });
                                                //NEW


                                                utmcDetailedMemberInvestments.ForEach(udmi =>
                                                {
                                                    udmi.utmcCompositionalTransactions = objUtmcCompositionalTransactions.Where(y => y.IpdFundCode == udmi.utmcFundInformation.IpdFundCode).ToList().OrderByDescending(y => y.DateOfTransaction.Value).ThenByDescending(y => y.IpdUniqueTransactionId).ToList();
                                                });

                                                Decimal actualCost = utmcDetailedMemberInvestments.Sum(udmi => udmi.utmcCompositionalTransactions.FirstOrDefault().ActualCost);

                                                Decimal marketValue = utmcDetailedMemberInvestments.Sum(udmi => (udmi.holderInv.CurrUnitHldg * udmi.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice));

                                                UnrealisedGain = marketValue - actualCost;
                                                UnrealisedGainPer = (actualCost == 0 ? 0.00M : ((marketValue - actualCost) / actualCost) * 100);
                                                utmcCompositionalTransactions.AddRange(objUtmcCompositionalTransactions);

                                                //-----------------------------------------------------------------------
                                            }

                                            //

                                            if (utmcDetailedMemberInvestments.Count > 0)
                                            {
                                                consolidatedTotalInvestmentRM += utmcDetailedMemberInvestments.Where(udmi => udmi.holderInv.CurrUnitHldg > 0).Sum(udmi => (udmi.holderInv.CurrUnitHldg * udmi.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice));
                                            }
                                            utmcDetailedMemberInvestmentsConsolidated.AddRange(utmcDetailedMemberInvestments);
                                        }
                                    });
                                    //String MANos = String.Join(", ", userAccounts.Select(uA => uA.AccountNo).ToArray());
                                    stringBuilder.Append(@"<div class='card hovercard'>
                                                            <div class='cardheader'>
                                                            </div>
                                                            <div class='avatar'>
                                                                <img alt='' src='/Content/MyImage/9.png'>
                                                            </div>
                                                            <div class='info'>
                                                                <div class='title'>
                                                                    <a target='_blank' href='javascript:;'>" + x.Username + @"</a>
                                                                </div>
                                                                <div class='desc'>" + x.IdNo + @"</div>
                                                                <div class='desc'>" + x.EmailId + @"</div>
                                                                <div class='desc'>" + x.MobileNumber + @"</div>
                                                                <div class='desc'>" + (x.Status == 1 ? "<span class='text text-success'>Active</span>" : "<span class='text text-danger'>Inactive</span>") + @"</div>
                                                            </div>
                                                            <div class='bottom'>
                                                                <h5>Total Current Investment Value</h5>
                                                                <div><strong><span class='currencyFormat'>" + consolidatedTotalInvestmentRM + @"</span></strong></div>
                                                            </div>
                                                        </div>");
                                    divViewClientHTML.InnerHtml = stringBuilder.ToString();
                                    StringBuilder stringBuilderSummary = new StringBuilder();
                                    var groupByHolderNoList = utmcDetailedMemberInvestmentsConsolidated.OrderBy(f => f.holderInv.FundId).GroupBy(f => f.holderInv.HolderNo).Select(grp => grp.OrderBy(y => y.holderInv.FundId).ToList()).ToList();
                                    //var groupByFundInvest3List = utmcDetailedMemberInvestmentsConsolidated.OrderBy(f => f.holderInv.FundId).GroupBy(f => f.holderInv.FundId).Select(grp => grp.OrderBy(y => y.holderInv.FundId).ToList()).ToList();
                                    int idx = 1;
                                    userAccounts.ForEach(uA =>
                                    {
                                        var buyNewUrl = "/BuyFunds.aspx?ClientId=" + x.Id + "&AccountNo=" + uA.AccountNo;
                                        var recExists = groupByHolderNoList.FirstOrDefault(g => g.First().holderInv.HolderNo.ToString() == uA.AccountNo);
                                        if (recExists == null)
                                        {
                                            stringBuilderSummary.Append(@"<tr class='fs-12 details'>
                                                                        <td class='fs-12'>" + idx + @"</td>
                                                                        <td class='fs-12'>
                                                                            " + uA.AccountNo + " (" + CustomValues.GetAccounPlan(uA.HolderClass) + @")" + @"
                                                                            <a style='width:70px;' class='btn btn-sm btn-primary' href='" + buyNewUrl + @"'>Invest New</a>
                                                                        </td>
                                                                        <td colspan='9'>
                                                                            -
                                                                        </td>
                                                                    ");
                                        }
                                    });
                                    groupByHolderNoList.ForEach(g =>
                                    {
                                        var buyNewUrl = "/BuyFunds.aspx?ClientId=" + x.Id + "&AccountNo=" + g.First().holderInv.HolderNo;
                                        string holderClass = userAccounts.FirstOrDefault(uA => uA.AccountNo == g.First().holderInv.HolderNo.ToString()).HolderClass;
                                        stringBuilderSummary.Append(@"<tr class='fs-12 details'>
                                                                        <td rowspan='" + g.Count + @"' class='fs-12'>" + idx + @"</td>
                                                                        <td rowspan='" + g.Count + @"' class='fs-12'>
                                                                            " + g.First().holderInv.HolderNo + " (" + CustomValues.GetAccounPlan(holderClass) + @")" + @"
                                                                            <a style='width:70px;' class='btn btn-sm btn-primary' href='" + buyNewUrl + @"'>Invest New</a>
                                                                        </td>
                                                                    ");
                                        var totalUnits = Math.Round(g.Sum(gu => gu.holderInv.CurrUnitHldg), 4);
                                        var totalAc = g.Sum(gu => gu.utmcCompositionalTransactions[0].ActualCost);
                                        var totalAvgCost = g.Sum(gu => gu.utmcCompositionalTransactions[0].ActualCost) / g.Sum(gu => gu.holderInv.CurrUnitHldg);
                                        var totalMv = (g.Sum(gu => gu.holderInv.CurrUnitHldg) * g.First().utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice);
                                        var totalRgl = (totalMv - totalAc);
                                        var totalRglPer = ((totalMv - totalAc) / totalAc) * 100;
                                        g.ForEach(u =>
                                        {
                                            var distributionInstruction = u.holderInv.DistributionInsString;
                                            var units = Math.Round(u.holderInv.CurrUnitHldg, 4);
                                            var ac = u.utmcCompositionalTransactions[0].ActualCost;
                                            var avgCost = u.utmcCompositionalTransactions[0].ActualCost / u.holderInv.CurrUnitHldg;
                                            var mv = (u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice);
                                            var rgl = (mv - ac);
                                            var rglPer = (ac != 0 ? ((mv - ac) / ac) * 100 : 0);
                                            var buyUrl = "/BuyFunds.aspx?ClientId=" + x.Id + "&fundCode=" + u.utmcFundInformation.FundCode + "&AccountNo=" + u.holderInv.HolderNo;
                                            var sellUrl = "/SellFunds.aspx?ClientId=" + x.Id + "&fundCode=" + u.utmcFundInformation.FundCode + "&AccountNo=" + u.holderInv.HolderNo;
                                            var switchUrl = "/SwitchFunds.aspx?ClientId=" + x.Id + "&fundCode=" + u.utmcFundInformation.FundCode + "&AccountNo=" + u.holderInv.HolderNo;
                                            stringBuilderSummary.Append(@"
                                                                        <td class='fs-12' data-class='fund-color-" + u.utmcFundInformation.FundCode + @"' data-value='" + u.utmcFundInformation.IpdFundCode + @"'><strong>" + u.utmcFundInformation.FundName + @"</strong></td>
                                                                        <td class='fs-12 text-right'>" + units + @"</td>
                                                                        <td class='fs-12 text-right currencyFormatNoSymbol'>" + ac + @"</td>
                                                                        "+//<td class='fs-12 text-right unitFormat'>" + Math.Round(avgCost, 4) + @"</td>
                                                                        "<td class='fs-12 text-right currencyFormatNoSymbol'>" + (g.Sum(gu => gu.holderInv.CurrUnitHldg) * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice) + @"</td>
                                                                        <td class='fs-12 text-right'><span data-toggle='tooltip' title='" + (u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavDate.Value.ToString("dd/MM/yyyy")) + @"' class='unitFormat'>" + u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice + @"</span></td>
                                                                        <td class='fs-12 text-right " + (rgl >= 0 ? "text-success" : "text-danger") + @"'><span class='currencyFormatNoSymbol'>" + rgl + @"</span></td>
                                                                        <td class='fs-12 text-right " + (rgl >= 0 ? "text-success" : "text-danger") + @"'><span class='currencyFormatNoSymbol'>" + rglPer + @"</span></td>
                                                                        <td class='fs-12 text-right'>
                                                                            <div class='mb-5'>
                                                                                <a style='width:70px;' class='btn btn-sm btn-primary' href='" + buyUrl + @"'>Invest</a>
                                                                            </div>
                                                                            <div class='mb-5'>
                                                                                <a style='width:70px;' class='btn btn-sm btn-primary' href='" + sellUrl + @"'>Redeem</a>
                                                                            </div>
                                                                            <div class='mb-5'>
                                                                                <a style='width:70px;' class='btn btn-sm btn-primary' href='" + switchUrl + @"'>Switch</a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>");
                                        });
                                        idx++;
                                    });
                                    //groupByFundInvestList.ForEach(g =>
                                    //{
                                    //    stringBuilderSummary.Append(@"<tr class='fs-12 details' data-class='fund-color-" + g.First().utmcFundInformation.FundCode + @"' data-value='" + g.First().utmcFundInformation.IpdFundCode + @"'>
                                    //                                    <td rowspan='" + g.Count + @"' class='fs-12'>" + idx + @"</td>
                                    //                                    <td rowspan='" + g.Count + @"' class='fs-12'>" + g.First().utmcFundInformation.FundName + @"</td>
                                    //                                ");
                                    //    var totalUnits = Math.Round(g.Sum(gu => gu.holderInv.CurrUnitHldg), 4);
                                    //    var totalAc = g.Sum(gu => gu.utmcCompositionalTransactions[0].ActualCost);
                                    //    var totalAvgCost = g.Sum(gu => gu.utmcCompositionalTransactions[0].ActualCost) / g.Sum(gu => gu.holderInv.CurrUnitHldg);
                                    //    var totalMv = (g.Sum(gu => gu.holderInv.CurrUnitHldg) * g.First().utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice);
                                    //    var totalRgl = (totalMv - totalAc);
                                    //    var totalRglPer = ((totalMv - totalAc) / totalAc) * 100;
                                    //    g.ForEach(u =>
                                    //    {
                                    //        var distributionInstruction = u.holderInv.DistributionInsString;
                                    //        var units = Math.Round(u.holderInv.CurrUnitHldg, 4);
                                    //        var ac = u.utmcCompositionalTransactions[0].ActualCost;
                                    //        var avgCost = u.utmcCompositionalTransactions[0].ActualCost / u.holderInv.CurrUnitHldg;
                                    //        var mv = (u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice);
                                    //        var rgl = (mv - ac);
                                    //        var rglPer = ((mv - ac) / ac) * 100;
                                    //        var buyUrl = "/BuyFunds.aspx?ClientId=" + x.Id + "&fundCode=" + u.utmcFundInformation.FundCode;//+ "&AccountNo=" + HolderNo
                                    //        var sellUrl = "/SellFunds.aspx?ClientId=" + x.Id + "&fundCode=" + u.utmcFundInformation.FundCode;//+ "&AccountNo=" + HolderNo
                                    //        var switchUrl = "/SwitchFunds.aspx?ClientId=" + x.Id + "&fundCode=" + u.utmcFundInformation.FundCode;//+ "&AccountNo=" + HolderNo
                                    //        string holderClass = userAccounts.FirstOrDefault(uA => uA.AccountNo == u.holderInv.HolderNo.ToString()).HolderClass;
                                    //        stringBuilderSummary.Append(@"
                                    //                                    <td class='fs-12'><strong>" + u.holderInv.HolderNo + " (" + CustomValues.GetAccounPlan(holderClass) + @")" + @"</strong></td>
                                    //                                    <td class='fs-12 text-right'>" + units + @"</td>
                                    //                                    <td class='fs-12 text-right currencyFormatNoSymbol'>" + ac + @"</td>
                                    //                                    <td class='fs-12 text-right unitFormat'>" + Math.Round(avgCost, 4) + @"</td>
                                    //                                    <td class='fs-12 text-right currencyFormatNoSymbol'>" + (g.Sum(gu => gu.holderInv.CurrUnitHldg) * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice) + @"</td>
                                    //                                    <td class='fs-12 text-right'><span data-toggle='tooltip' title='" + (u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavDate.Value.ToString("dd/MM/yyyy")) + @"' class='unitFormat'>" + u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice + @"</span></td>
                                    //                                    <td class='fs-12 text-right " + (rgl >= 0 ? "text-success" : "text-danger") + @"'><span class='currencyFormatNoSymbol'>" + rgl + @"</span></td>
                                    //                                    <td class='fs-12 text-right " + (rgl >= 0 ? "text-success" : "text-danger") + @"'><span class='currencyFormatNoSymbol'>" + rglPer + @"</span></td>
                                    //                                    <td class='fs-12 text-right'>
                                    //                                        <div class='mb-5'>
                                    //                                            <a style='width:70px;' class='btn btn-sm btn-primary' href='" + buyUrl + @"'>Invest</a>
                                    //                                        </div>
                                    //                                        <div class='mb-5'>
                                    //                                            <a style='width:70px;' class='btn btn-sm btn-primary' href='" + sellUrl + @"'>Redeem</a>
                                    //                                        </div>
                                    //                                        <div class='mb-5'>
                                    //                                            <a style='width:70px;' class='btn btn-sm btn-primary' href='" + switchUrl + @"'>Switch</a>
                                    //                                        </div>
                                    //                                    </td>
                                    //                                </tr>");
                                    //    });
                                    //    idx++;
                                    //});
                                    investmentPortfolioTbody.InnerHtml = stringBuilderSummary.ToString();
                                }

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Invalid Client id\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Please select client\", 'AgentClients.aspx');", true);
                    }
                }


            }
        }


        public static decimal Get_Redumption_Cost_UTMC10(string Holder_No, string Fund_ID, string Sort_Seq, DateTime End_date, List<HolderLedger> holderLedgersFiltered)
        {
            try
            {
                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                {
                    {
                        int Count = 0; string[] numb;
                        {
                            List<HolderLedger> holderLedgerIn = holderLedgersFiltered.Where(x => x.FundID == Fund_ID && x.HolderNo.ToString() == Holder_No && x.TransDt <= End_date).OrderBy(x => x.TransDt).ThenBy(x => x.SortSEQ).ThenByDescending(x => x.TransType).ToList();

                            numb = new string[holderLedgerIn.Count];
                            holderLedgerIn.ForEach(hl =>
                            {
                                numb[Count++] = hl.TransNO;
                            });
                            Array.Resize(ref numb, Count);
                            Count = 0;
                            string Trans_String;
                            foreach (var hl in holderLedgerIn)
                            {
                                try
                                {
                                    Trans_String = hl.TransNO;
                                    bool TTT = false;
                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            TTT = true;
                                            break;
                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = hl.TransUnits != null ? hl.TransUnits.Value : 0;
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = hl.TransType.Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = hl.TransNO;
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= (hl.TransAmt != null ? hl.TransAmt.Value : 0);
                                                Book_value -= ((hl.TransUnits != null ? hl.TransUnits.Value : 0) * (hl.TransPr != null ? hl.TransPr.Value : 0));
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }
                                            else
                                            {
                                                Actual_transfer_value += (hl.TransAmt != null ? hl.TransAmt.Value : 0);
                                                Book_value += ((hl.TransUnits != null ? hl.TransUnits.Value : 0) * (hl.TransPr != null ? hl.TransPr.Value : 0));
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {



                                            Total_Redemption_cost = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = hl.TransUnits != null ? hl.TransUnits.Value : 0;
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            UnitCostRDfinal = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost
                                                                * UnitCostRD);


                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 4);


                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {



                                                Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                           * Total_Redemption_cost);
                                                Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                          * Total_Redemption_cost);



                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                Unit_Holding_value = 0.0000M;
                                            }

                                            Unit_Holding_value = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if (Sort_Seq == hl.SortSEQ.ToString())
                                            {
                                                break;
                                            }

                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            return UnitCostRDfinal;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                return 0.00M;

            }
        }

    }
}