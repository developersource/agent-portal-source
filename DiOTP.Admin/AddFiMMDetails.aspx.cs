﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddFiMMDetails : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["admin"] != null)
                {
                    User sessionUser = (User)Session["admin"];
                    Response response = IUserService.GetSingle(sessionUser.Id);
                    if (response.IsSuccess)
                    {
                        User user = (User)response.Data;
                        string idString = Request.QueryString["id"];
                        if (idString != null && idString != "")
                        {
                            int id = Convert.ToInt32(idString);
                            Id.Value = id.ToString();

                            bool isExamEnrolled = false;
                            AgentReg agentReg = new AgentReg();
                            string queryARegAutofill = (@" SELECT * FROM agent_regs where id=" + id + " ");
                            Response responseARegAutofillList = GenericService.GetDataByQuery(queryARegAutofill, 0, 0, false, null, false, null, true);
                            if (responseARegAutofillList.IsSuccess)
                            {
                                var agentsDyn = responseARegAutofillList.Data;
                                var responseARegJSON = JsonConvert.SerializeObject(agentsDyn);
                                List<AgentReg> aRegList = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseARegJSON);
                                if (aRegList.Count == 1)
                                {
                                    agentReg = aRegList.FirstOrDefault();
                                    process_status.Value = agentReg.process_status.ToString();
                                    if (agentReg.is_new_agent == 1)
                                    {
                                        //Enroll Exam here

                                        string mainQE = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status,exam_datetime FROM agent_exam_schedules where exam_datetime > '" + DateTime.Now.ToString("yyyy-MM-dd") + "' and is_active=1 and status=1 ");
                                        Response responseEList = GenericService.GetDataByQuery(mainQE, 0, 30, false, null, false, null, true);
                                        if (responseEList.IsSuccess)
                                        {
                                            var examsDyn = responseEList.Data;
                                            var responseexamsDynJSON = JsonConvert.SerializeObject(examsDyn);
                                            List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseexamsDynJSON);

                                            ddlLocation.DataSource = agentExamSchedules.GroupBy(y => y.location).Select(g => g.First()).ToList();
                                            ddlLocation.DataTextField = "location";
                                            ddlLocation.DataValueField = "location";
                                            ddlLocation.DataBind();

                                            ddlLocation.Items.Insert(0, new ListItem { Text = "Select", Value = "" });

                                            examEnrollDiv.Visible = true;
                                            fimmDiv.Visible = false;
                                            AgentRegExam agentRegExamExisting = new AgentRegExam();

                                            string queryAgentRegExam = (@" select * from agent_reg_exams where agent_reg_id=" + agentReg.id + " and is_enroll=1 and status=1 ");
                                            Response responseAgentRegExamList = GenericService.GetDataByQuery(queryAgentRegExam, 0, 0, false, null, false, null, true);
                                            response = responseAgentRegExamList;
                                            if (responseAgentRegExamList.IsSuccess)
                                            {
                                                var agentregExamDyn = responseAgentRegExamList.Data;
                                                var responseAgentRegExamsJSON = JsonConvert.SerializeObject(agentregExamDyn);
                                                List<AgentRegExam> agentRegExams = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegExam>>(responseAgentRegExamsJSON);
                                                if (agentRegExams.Count == 1)
                                                {
                                                    enrollStatus.InnerHtml = "Enrolled";
                                                    isExamEnrolled = true;
                                                    agentRegExamExisting = agentRegExams.FirstOrDefault();
                                                    ddlLocation.Items.Insert(0, agentRegExamExisting.location);
                                                    ddlLocation.Disabled = true;
                                                    ddlLanguage.Items.Insert(0, agentRegExamExisting.language);
                                                    ddlLanguage.Disabled = true;
                                                    ddlExamDate.Items.Insert(0, agentRegExamExisting.exam_date.HasValue ? agentRegExamExisting.exam_date.Value.ToString("dd/MM/yyyy") : "");
                                                    ddlExamDate.Disabled = true;
                                                    ddlTime.Items.Insert(0, agentRegExamExisting.exam_date.HasValue ? agentRegExamExisting.exam_date.Value.ToString("HH:mm tt") : "");
                                                    ddlTime.Disabled = true;
                                                    ddlResult.Value = agentRegExamExisting.result;
                                                    resultDiv.Visible = true;
                                                    if(agentRegExamExisting.result == "Pass")
                                                    {
                                                        ddlResult.Disabled = true;
                                                        agentReg.is_new_agent = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    enrollStatus.InnerHtml = "Pending";
                                                    enrollStatusDiv.Visible = false;
                                                    resultDiv.Visible = false;
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegExamList.Message + "\", '');", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseEList.Message + "\", '');", true);
                                        }

                                    }
                                    if (agentReg.is_new_agent == 0)
                                    {
                                        if (!isExamEnrolled)
                                        {
                                            examEnrollDiv.Visible = false;
                                        }
                                        fimmDiv.Visible = true;

                                        AgentRegFimm agentRegFimmExisting = new AgentRegFimm();

                                        string queryAgentRegFiMM = (@" select * from agent_reg_fimms where agent_reg_id=" + agentReg.id + " and status=1 ");
                                        Response responseAgentRegFiMMList = GenericService.GetDataByQuery(queryAgentRegFiMM, 0, 0, false, null, false, null, true);
                                        if (responseAgentRegFiMMList.IsSuccess)
                                        {
                                            var agentregFiMMDyn = responseAgentRegFiMMList.Data;
                                            var responseAgentRegFiMMsJSON = JsonConvert.SerializeObject(agentregFiMMDyn);
                                            List<AgentRegFimm> agentRegFiMMs = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegFimm>>(responseAgentRegFiMMsJSON);
                                            if (agentRegFiMMs.Count == 1)
                                            {
                                                agentRegFimmExisting = agentRegFiMMs.FirstOrDefault();
                                                //txtNewCardRecievedOn.Value = agentRegFimmExisting.new_card_received_on.HasValue ? agentRegFimmExisting.new_card_received_on.Value.ToString("dd/MM/yyyy") : "";
                                                txtFIMMNo.Value = agentRegFimmExisting.FIMM_no;
                                                txtApprovalDate.Value = agentRegFimmExisting.approval_date.HasValue ? agentRegFimmExisting.approval_date.Value.ToString("dd/MM/yyyy") : "";
                                                txtExpiryDate.Value = agentRegFimmExisting.expiry_date.HasValue ? agentRegFimmExisting.expiry_date.Value.ToString("dd/MM/yyyy") : "";
                                                txtRemarks.Value = agentRegFimmExisting.remarks;
                                                //rdnRenewalApplicaionYes.Checked = agentRegFimmExisting.is_renewal.HasValue ? agentRegFimmExisting.is_renewal.Value == 1 ? true : false : false;
                                                //rdnRenewalApplicaionNo.Checked = agentRegFimmExisting.is_renewal.HasValue ? agentRegFimmExisting.is_renewal.Value == 0 ? true : false : false;
                                                //txtRenewalCardRecievedOn.Value = agentRegFimmExisting.renewal_card_received_on.HasValue ? agentRegFimmExisting.renewal_card_received_on.Value.ToString("dd/MM/yyyy") : "";
                                                txtFIMMMICRCode.Value = agentRegFimmExisting.from_agent_MICR_code;
                                                txtFIMMBankName.Value = agentRegFimmExisting.from_agent_bank_name;
                                                txtFIMMChequeno.Value = agentRegFimmExisting.from_agent_cheque_no;
                                                txtFIMMTransRefno.Value = agentRegFimmExisting.from_agent_trans_ref_no;
                                                txtFIMMAmount.Value = agentRegFimmExisting.from_agent_amount.HasValue ? agentRegFimmExisting.from_agent_amount.Value.ToString() : "";
                                                txtFIMMDateReceived.Value = agentRegFimmExisting.from_agent_date_received.HasValue ? agentRegFimmExisting.from_agent_date_received.Value.ToString("dd/MM/yyyy") : "";
                                                txtFIMMPaymentRefno.Value = agentRegFimmExisting.from_agent_payment_ref_no;
                                                txtFIMMDepositAccountNo.Value = agentRegFimmExisting.from_agent_deposit_acc_no;
                                                txtToFiMMWithAccNo.Value = agentRegFimmExisting.to_FiMM_with_acc_no;
                                                txtToFiMMMICRCode.Value = agentRegFimmExisting.to_FiMM_MICR_code;
                                                txtToFiMMBankName.Value = agentRegFimmExisting.to_FiMM_bank_name;
                                                txtToFiMMChequeno.Value = agentRegFimmExisting.to_FiMM_cheque_no;
                                                txtToFiMMDateSent.Value = agentRegFimmExisting.to_FiMM_date_sent.HasValue ? agentRegFimmExisting.to_FiMM_date_sent.Value.ToString("dd/MM/yyyy") : "";
                                                txtFiMMRenewalAmount.Value = agentRegFimmExisting.to_FiMM_FIMM_renewal_amount.HasValue ? agentRegFimmExisting.to_FiMM_FIMM_renewal_amount.Value.ToString() : "";
                                                txtUTMCRenewalAmount.Value = agentRegFimmExisting.to_FiMM_UTMC_renewal_amount.HasValue ? agentRegFimmExisting.to_FiMM_UTMC_renewal_amount.Value.ToString() : "";
                                            }
                                        }
                                        else
                                        {
                                            response = responseAgentRegFiMMList;
                                        }


                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Something went wrong";
                                }
                            }
                            else
                            {
                                response = responseARegAutofillList;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Something went wrong\");", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + response.Message + "\");", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Session expired\");", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + ex.Message + "\");", true);
            }
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetSessionDetails(Int32 SessionId)
        {
            Response response = new Response() { IsSuccess = true };
            try
            {
                string mainQ = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status,group_concat(exam_datetime) as exam_datetimes FROM agent_exam_schedules where exam_datetime > '" + DateTime.Now.ToString("yyyy-MM-dd") + "' and is_active=1 and status=1 and id=" + SessionId + " ");
                Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 30, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var examsDyn = responseUList.Data;
                    var responseexamsDynJSON = JsonConvert.SerializeObject(examsDyn);
                    List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseexamsDynJSON);
                    CustomAgentExamSchedule customAgentExamSchedule = agentExamSchedules.FirstOrDefault();
                    response.Data = customAgentExamSchedule;
                }
                else
                {
                    response = responseUList;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetDetailsByLocation(String Location)
        {
            Response response = new Response() { IsSuccess = true };
            try
            {
                //,group_concat(DATE_FORMAT(exam_datetime, '%H:%i')) as exam_datetimes
                string mainQ = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status FROM agent_exam_schedules where exam_datetime > '" + DateTime.Now.ToString("yyyy-MM-dd") + "' and is_active=1 and status=1 and location='" + Location + "' ");
                Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 30, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var examsDyn = responseUList.Data;
                    var responseexamsDynJSON = JsonConvert.SerializeObject(examsDyn);
                    List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseexamsDynJSON);
                    //CustomAgentExamSchedule customAgentExamSchedule = agentExamSchedules.FirstOrDefault();
                    response.Data = agentExamSchedules;
                }
                else
                {
                    response = responseUList;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}