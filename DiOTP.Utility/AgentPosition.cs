﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_positions")]
    public class AgentPosition
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("agent_pos"), Display("AgentPos")]
        public String AgentPos { get; set; }
        [Database("agent_group"), Display("AgentGroup")]
        public String AgentGroup { get; set; }
        [Database("upline1"), Display("Upline1")]
        public String Upline1 { get; set; }
        [Database("upline2"), Display("Upline2")]
        public String Upline2 { get; set; }
        [Database("upline3"), Display("Upline3")]
        public String Upline3 { get; set; }
        [Database("prev_upline1"), Display("PrevUpline1")]
        public String PrevUpline1 { get; set; }
        [Database("prev_upline2"), Display("PrevUpline2")]
        public String PrevUpline2 { get; set; }
        [Database("prev_upline3"), Display("PrevUpline3")]
        public String PrevUpline3 { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
