﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_banks")]
    public class AgentRegBank
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_reg_payment_id"), Display("agent_reg_payment_id")]
        public Int32 agent_reg_payment_id { get; set; }
        [Database("bank_type"), Display("bank_type")]
        public Int32 bank_type { get; set; }
        [Database("currency"), Display("currency")]
        public String currency { get; set; }
        [Database("id_no"), Display("id_no")]
        public String id_no { get; set; }
        [Database("bank_name"), Display("bank_name")]
        public String bank_name { get; set; }
        [Database("bank_account_type"), Display("bank_account_type")]
        public String bank_account_type { get; set; }
        [Database("account_no"), Display("account_no")]
        public String account_no { get; set; }
        [Database("payment_mode"), Display("payment_mode")]
        public String payment_mode { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32? created_by { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
