﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    public class DailyNAVDTO
    {
        public string FUND_ID { get; set; }
        public DateTime TRANS_DT { get; set; }
        public Decimal UP_SELL { get; set; }
        public Decimal UNIT_IN_ISSUE { get; set; }
        public Decimal UNIT_IN_ISSUE_PRICE { get; set; }

    }
    
}
