﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAgentTraineeMaterial.aspx.cs" Inherits="Admin.AddAgentTraineeMaterial" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addAgentTraineeMaterialForm" runat="server">
        <div id="FormId" data-value="addBannerForm">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Material Name:</label>
                        </div>  
                        <div class="col-md-8">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="hidden" name="MaterialURL" id="MaterialURL" runat="server" clientidmode="static" />
                            <div>
                                <input type="text" name="MaterialName" id="MaterialNameTxt" runat="server" clientidmode="static" class="form-control" placeholder="Enter Material Name"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Material Description:</label>
                        </div>
                        <div class="col-md-8">
                            <textarea placeholder="Enter Description" rows="5" class="form-control" name="MaterialDescription" id="MaterialDescriptionTxtArea" runat="server"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Material Upload:</label>
                        </div>
                        <div class="col-md-8">
                            <asp:FileUpload ID="MaterialUploadBtn" runat="server" ClientIDMode="Static" CssClass="default filePicker" text="Upload Material"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $ui(document).ready(function () {

                function sendFile(file) {
                    var formData = new FormData();
                    formData.append('file', file);
                    $ui.ajax({
                        type: 'post',
                        url: 'fileUploadMaterial.ashx',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (status) {
                            console.log(status);
                        },
                        error: function () {
                            alert("Whoops something went wrong!");
                        }
                    });
                }

                var _URL = window.URL || window.webkitURL;
                $ui('.filePicker').unbind('change');
                $ui('.filePicker').change(function () {
                    var file;
                    file = this.files[0];
                    console.log(file);
                    
                    var extensions = ["ppt","pptx","pdf"];
                    var fileLink = document.getElementById('MaterialUploadBtn').value;
                    var fileExtensionType = fileLink.split(".").pop();
                    console.log(fileLink);
                    console.log(fileExtensionType.value);
                    //Check if file extension is included in the list.
                    if (extensions.some(fileExt => fileExtensionType.includes(fileExt))) {
                        var fullFilePath = fileLink;
                        var lastDot = fullFilePath.lastIndexOf('\\')+1;
                        var fileName = fullFilePath.substring(lastDot, fullFilePath.length);
                        
                        console.log("This is a valid file type!");
                        sendFile(file);
                        $('#MaterialURL').val(fileName);                       
                    }
                    else {
                        document.getElementById('URL').value = "";
                        alert("Only Powerpoint or PDF files are allowed!");
                    }
                });

                $ui('.default-color-picker').colorpicker();

            });
        </script>
    </form>
</body>
</html>
