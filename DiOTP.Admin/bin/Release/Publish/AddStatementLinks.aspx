﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddStatementLinks.aspx.cs" Inherits="Admin.AddStatementLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addStatementLinks" runat="server">
        <div id="FormId" data-value="addStatementLinks">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                    <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                    <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                    <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                    <input type="hidden" name="UpdatedBy" value="0" id="UpdatedBy" runat="server" clientidmode="static" />
                    <input type="hidden" name="UpdatedDate" value="0" id="UpdatedDate" runat="server" clientidmode="static" />

                    <input type="hidden" name="RefNo" value="0" id="RefNo" runat="server" clientidmode="static" />
                    <input type="hidden" name="UserOrderStatementTypeId" value="0" id="UserOrderStatementTypeId" runat="server" clientidmode="static" />
                    <input type="hidden" name="UserId" value="0" id="UserId" runat="server" clientidmode="static" />
                    <input type="hidden" name="UserAccountId" value="0" id="UserAccountId" runat="server" clientidmode="static" />

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Name:</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="Name" id="Name" runat="server" clientidmode="static" class="form-control" placeholder="Enter name" />
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Url:</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="Url" id="Url" runat="server" clientidmode="static" class="form-control" placeholder="Enter Short Display Content" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
