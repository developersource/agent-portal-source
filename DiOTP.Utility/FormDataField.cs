using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("form_data_fields")]
    public class FormDataField
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("form_type_id"), Display("FormTypeId")]
        public Int32 FormTypeId { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("field_id"), Display("FieldId")]
        public String FieldId { get; set; }
        [Database("field_set"), Display("FieldSet")]
        public Int32 FieldSet { get; set; }
        [Database("field_type"), Display("FieldType")]
        public String FieldType { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public FormType FormTypeIdFormType { get; set; }
        public List<FormDataFieldValue> FormDataFieldIdFormDataFieldValues { get; set; }
        public List<FormDataFieldOption> FormDataFieldIdFormDataFieldOptions { get; set; }
    }
}
