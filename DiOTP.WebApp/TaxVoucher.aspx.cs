﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class TaxVoucher : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserOrderStatementService> lazyUserStatementServiceObj = new Lazy<IUserOrderStatementService>(() => new UserOrderStatementService());
        public static IUserOrderStatementService IUserOrderStatementService { get { return lazyUserStatementServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Login.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Index.aspx');", true);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        String isVerified = "0";
                        if (Session["isVerified"] == null)
                        {
                            if (Request.Browser.IsMobileDevice)
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                        }
                        else
                        {
                            isVerified = (Session["isVerified"].ToString());
                            if (isVerified == "0")
                            {
                                if (Request.Browser.IsMobileDevice)
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Login.aspx');", true);
                                else
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Index.aspx');", true);
                            }
                            else if (isVerified == "1")
                            {
                                User user = (User)Session["user"];
                                Response response = IUserService.GetSingle(user.Id);
                                if (response.IsSuccess)
                                {
                                    user = (User)response.Data;
                                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                                    if (responseUAList.IsSuccess)
                                    {
                                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                        userAccounts = userAccounts.Where(x => x.IsVerified == 1 && x.Status == 1).ToList();

                                        string all = String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToArray());
                                        ddlFundAccount.Items.Add(new ListItem("All", all));
                                        foreach (UserAccount userAccount in userAccounts)
                                        {
                                            ddlFundAccount.Items.Add(new ListItem(userAccount.AccountNo, userAccount.Id.ToString()));
                                        }

                                        Response responseUOSList = IUserOrderStatementService.GetDataByFilter("status=1 and user_order_statement_type_id=4", 0, 0, false);
                                        if (responseUOSList.IsSuccess)
                                        {
                                            List<UserOrderStatement> userorderstatements = ((List<UserOrderStatement>)responseUOSList.Data).Where(x => x.UserId == user.Id).ToList();
                                            StringBuilder sb = new StringBuilder();
                                            Int32 idx = 1;
                                            foreach (UserOrderStatement userorderstatement in userorderstatements)
                                            {
                                                Response responseUAList1 = IUserAccountService.GetDataByFilter("ID = " + userorderstatement.UserAccountId + " and status=1 ", 0, 0, false);
                                                if (responseUAList1.IsSuccess)
                                                {
                                                    UserAccount ua = ((List<UserAccount>)responseUAList1.Data).FirstOrDefault();
                                                    sb.Append(@"<tr>
                                                                <td>" + idx + @"</td>
                                                                <td>" + ua.AccountNo + @"</td>
                                                                <td>" + DateTime.ParseExact(userorderstatement.RefNo, "yyyyMMdd", null).ToString("dd/MM/yyyy") + @"</td>
                                                                <td>" + "<a href='" + userorderstatement.Url + @"' target='_blank'><i class='fa fa-search' style='margin-left:20px'></i></a>" + @"</td>
                                                            </tr>");
                                                    idx++;
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUAList1.Message + "\", '');", true);
                                                }
                                            }
                                            taxVoucherTbody.InnerHtml = sb.ToString();
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUOSList.Message + "\", '');", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            GenerateTaxVoucher();
        }

        public void GenerateTaxVoucher()
        {
            try
            {
                if (!(txtDateStart.Text.Equals("") || txtDateEnd.Text.Equals("")))
                {
                    string selectedAccount = ddlFundAccount.SelectedItem.Text;
                    if (selectedAccount == "All")
                    {
                        selectedAccount = ddlFundAccount.SelectedValue;
                    }
                    string filter = " account_no in (" + selectedAccount + ") and is_verified=1 and status=1 ";
                    DateTime startDate = DateTime.ParseExact(txtDateStart.Text, "yyyy-MM-dd", null);
                    DateTime endDate = DateTime.ParseExact(txtDateEnd.Text, "yyyy-MM-dd", null);
                    if (endDate > startDate)
                    {
                        Response responseUAList = IUserAccountService.GetDataByFilter(filter, 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> uas = ((List<UserAccount>)responseUAList.Data).ToList();
                            //StringBuilder sb = new StringBuilder();
                            //Int32 idx = 1;
                            //foreach (UserAccount ua in uas)
                            //{
                            //    Response responseUOSList = IUserOrderStatementService.GetDataByFilter("user_account_id = '" + ua.Id + "' and ref_no between '" + startDateString + "' and '" + endDateString + "' ", 0, 0, false);
                            //    if (responseUOSList.IsSuccess)
                            //    {
                            //        List<UserOrderStatement> userorderstatements = ((List<UserOrderStatement>)responseUOSList.Data).Where(x => x.Status == 1 && x.UserOrderStatementTypeId == 4).ToList();
                            //        if (userorderstatements.Count > 0)
                            //        {
                            //            foreach (UserOrderStatement userorderstatement in userorderstatements)
                            //            {
                            //                sb.Append(@"<tr>
                            //                                <td>" + idx + @"</td>
                            //                                <td>" + ua.AccountNo + @"</td>
                            //                                <td>" + userorderstatement.CreatedDate + @"</td>
                            //                                <td>" + "<a href='" + userorderstatement.Url + @"' target='_blank'><i class='fa fa-search'></i></a>" + @"</td>
                            //                            </tr>");
                            //                idx++;
                            //            }
                            //        }
                            //    }
                            //    else
                            //    {
                            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUOSList.Message + "\", '');", true);
                            //    }
                            //}
                            //if (idx == 1)
                            //{
                            //    sb.Append(@"<tr>
                            //                    <td colspan='9' align='center'>No transaction record found.</td>
                            //                </tr>");
                            //}
                            //taxVoucherTbody.InnerHtml = sb.ToString();
                            List<UserOrderStatement> userorderstatements = new List<UserOrderStatement>();
                            Response responseUOSList = IUserOrderStatementService.GetDataByFilter("user_account_id in (" + String.Join(",", uas.Select(x => x.Id).ToArray()) + ") and STR_TO_DATE(ref_no, '%Y%m%d') between '" + startDate.ToString("yyyyMMdd") + "' and '" + endDate.ToString("yyyyMMdd") + "' and status=1 and user_order_statement_type_id=4 ", 0, 0, false);
                            if (responseUOSList.IsSuccess)
                            {
                                userorderstatements = (List<UserOrderStatement>)responseUOSList.Data;



                                if (userorderstatements.Count > 0)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    Int32 idx = 1;
                                    userorderstatements.ForEach(x =>
                                    {

                                        sb.Append(@"<tr>
                                                    <td>" + idx + @"</td>
                                                    <td>" + uas.FirstOrDefault(y => y.Id == x.UserAccountId).AccountNo + @"</td>
                                                            <td>" + DateTime.ParseExact(x.RefNo, "yyyyMMdd", null).ToString("dd/MM/yyyy") + @"</td>
                                                           <td>" + "<a href='" + x.Url + @"' target='_blank'><i class='fa fa-search'></i></a>" + @"</td>
                                                       </tr>");
                                        idx++;

                                    });
                                    taxVoucherTbody.InnerHtml = sb.ToString();
                                }
                                else
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.Append(@"<tr>
                                                <td colspan='9' align='center'>No record found.</td>
                                            </tr>");
                                    taxVoucherTbody.InnerHtml = sb.ToString();
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUOSList.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Date(From) cannot be greater than Date(To)', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select date range', '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("FundReport Page GenerateFundReport: " + ex.Message);
            }
        }
    }
}