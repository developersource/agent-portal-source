using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class NotificationCategoriesDefService : INotificationCategoriesDefService
    {
        private static readonly Lazy<INotificationCategoriesDefRepo> lazy = new Lazy<INotificationCategoriesDefRepo>(() => new NotificationCategoriesDefRepo());
        public static INotificationCategoriesDefRepo INotificationCategoriesDefRepo { get { return lazy.Value; } }
        public Response PostData(NotificationCategoriesDef obj)
        {
            Response response = new Response();
            try
            {
                response = INotificationCategoriesDefRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<NotificationCategoriesDef> objs)
        {
            Int32 result = 0;
            try
            {
                result = INotificationCategoriesDefRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(NotificationCategoriesDef obj)
        {
            Response response = new Response();
            try
            {
                response = INotificationCategoriesDefRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<NotificationCategoriesDef> objs)
        {
            Int32 result = 0;
            try
            {
                result = INotificationCategoriesDefRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public NotificationCategoriesDef DeleteData(Int32 Id)
        {
            NotificationCategoriesDef obj = new NotificationCategoriesDef();
            try
            {
                obj = INotificationCategoriesDefRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = INotificationCategoriesDefRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            NotificationCategoriesDef obj = new NotificationCategoriesDef();
            try
            {
                response = INotificationCategoriesDefRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<NotificationCategoriesDef> objs = new List<NotificationCategoriesDef>();
            try
            {
                response = INotificationCategoriesDefRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = INotificationCategoriesDefRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<NotificationCategoriesDef> objs = new List<NotificationCategoriesDef>();
            try
            {
                response = INotificationCategoriesDefRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = INotificationCategoriesDefRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<NotificationCategoriesDef> objs = new List<NotificationCategoriesDef>();
            try
            {
                response = INotificationCategoriesDefRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = INotificationCategoriesDefRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
