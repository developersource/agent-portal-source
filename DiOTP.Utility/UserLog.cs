﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    //[Database("user_file_types")]
    //public class UserFileType
    //{
    //    public List<UserFile> UserFileTypeIdUserFiles { get; set; }
    //    [Database("id"), Display("Id")]
    //    public Int32 Id { get; set; }
    //    [Database("name"), Display("Name")]
    //    public String Name { get; set; }
    //    [Database("status"), Display("Status")]
    //    public Int32 Status { get; set; }
    //}
    [Database("user_log")]
    public class UserLog
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("description"), Display("Description")]
        public string Description { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
