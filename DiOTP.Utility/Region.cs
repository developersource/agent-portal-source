﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class Region
    {
        public Int32 id { get; set; }
        public String code { get; set; }
        public String name { get; set; }
        public Int32 is_active { get; set; }
        public DateTime created_date { get; set; }
        public Int32 status { get; set; }
    }
}
