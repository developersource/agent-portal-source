﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddStaff : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypesDefService> lazyUserTypesDefServiceObj = new Lazy<IUserTypesDefService>(() => new UserTypesDefService());
        public static IUserTypesDefService IUserTypesDefService { get { return lazyUserTypesDefServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["admin"] != null)
            //{
            //    User sessionUser = (User)Session["admin"];
            //    Response response = IUserService.GetSingle(sessionUser.Id);
            //    if (response.IsSuccess)
            //    {
            //        User user = (User)response.Data;
            //        if (user != null)
            //        {
            //            if (user.UserRoleId == 1)
            //            {
            //                Response responseUTDList = IUserTypesDefService.GetDataByFilter(" name like '%Admin%' and status='1'", 0, 0, false);
            //                if (responseUTDList.IsSuccess)
            //                {
            //                    List<UserTypesDef> userTypesDefs = (List<UserTypesDef>)responseUTDList.Data;

            //                    //LoginAttempts is UserTypeId here
            //                    LoginAttempts.DataSource = userTypesDefs;
            //                    LoginAttempts.DataTextField = "name";
            //                    LoginAttempts.DataValueField = "id";
            //                    LoginAttempts.DataBind();

            //                    LoginAttempts.Items.Insert(0, new ListItem { Text = "Select", Value = "0" });
            //                }
            //            }
            //            else if (user.UserRoleId == 2)
            //            {
            //                Response responseUTDList = IUserTypesDefService.GetDataByFilter(" name like '%Admin' and name !='Main Admin' and status='1'", 0, 0, false);
            //                if (responseUTDList.IsSuccess)
            //                {
            //                    List<UserTypesDef> userTypesDefs = (List<UserTypesDef>)responseUTDList.Data;

            //                    //LoginAttempts is UserTypeId here
            //                    LoginAttempts.DataSource = userTypesDefs;
            //                    LoginAttempts.DataTextField = "name";
            //                    LoginAttempts.DataValueField = "id";
            //                    LoginAttempts.DataBind();

            //                    LoginAttempts.Items.Insert(0, new ListItem { Text = "Select", Value = "0" });
            //                }
            //            }
            //        }
            //    }
            //}


            string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
            CreatedBy.Value = "0";
            ModifiedBy.Value = "0";
            CreatedDate.Value = currentDateString;
            ModifiedDate.Value = currentDateString;



            string idString = Request.QueryString["id"];
            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);

                Response response = IUserService.GetSingle(id);
                if (response.IsSuccess)
                {
                    User obj = (User)response.Data;
                    Response responseUTList = IUserTypeService.GetDataByFilter(" user_id='" + obj.Id + "' and status='1'", 0, 0, false);
                    if (responseUTList.IsSuccess && responseUTList.Data != null)
                    {
                        UserType userType = ((List<UserType>)responseUTList.Data).FirstOrDefault();
                        if (userType != null)
                            LoginAttempts.Value = userType.UserTypeId.ToString();
                    }
                    UserRoleId.Value = obj.UserRoleId.ToString();
                    Username.Value = obj.Username;
                    EmailId.Value = obj.EmailId;
                    MobileNumber.Value = obj.MobileNumber;
                    Password.Value = obj.Password;
                    TransPwd.Value = obj.TransPwd;
                    UniqueKey.Value = obj.UniqueKey;
                    IsOnline.Value = obj.IsOnline.ToString();
                    IsActive.Value = obj.IsActive.ToString();
                    IsPrimary.Value = obj.IsPrimary.ToString();
                    RegisterIp.Value = obj.RegisterIp.ToString();
                    LastLoginOn.Value = obj.LastLoginDate == null ? DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") : obj.LastLoginDate.Value.ToString("MM/dd/yyyy HH:mm:ss");
                    LastLoginIp.Value = obj.LastLoginIp.ToString();

                    Id.Value = obj.Id.ToString();
                    Status.Value = obj.Status.ToString();
                    CreatedBy.Value = obj.CreatedBy.ToString();
                    CreatedDate.Value = obj.CreatedDate.ToString("MM/dd/yyyy HH:mm:ss");
                    ModifiedBy.Value = obj.ModifiedBy.ToString();
                    ModifiedDate.Value = obj.ModifiedDate == null ? DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") : obj.ModifiedDate.Value.ToString("MM/dd/yyyy HH:mm:ss");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}