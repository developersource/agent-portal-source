using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_compositional_transactions")]
    public class UtmcCompositionalTransaction
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("member_epf_no"), Display("MemberEpfNo")]
        public String MemberEpfNo { get; set; }
        [Database("ipd_member_acc_no"), Display("IpdMemberAccNo")]
        public String IpdMemberAccNo { get; set; }
        [Database("effective_date"), Display("EffectiveDate")]
        public DateTime? EffectiveDate { get; set; }
        [Database("date_of_transaction"), Display("DateOfTransaction")]
        public DateTime? DateOfTransaction { get; set; }
        [Database("date_of_settlement"), Display("DateOfSettlement")]
        public DateTime? DateOfSettlement { get; set; }
        [Database("ipd_unique_transaction_id"), Display("IpdUniqueTransactionId")]
        public String IpdUniqueTransactionId { get; set; }
        [Database("transaction_code"), Display("TransactionCode")]
        public String TransactionCode { get; set; }
        [Database("reversed_transaction_id"), Display("ReversedTransactionId")]
        public String ReversedTransactionId { get; set; }
        [Database("units"), Display("Units")]
        public Decimal? Units { get; set; }
        [Database("gross_amount_rm"), Display("GrossAmountRm")]
        public Decimal? GrossAmountRm { get; set; }
        [Database("net_amount_rm"), Display("NetAmountRm")]
        public Decimal? NetAmountRm { get; set; }
        [Database("fees_rm"), Display("FeesRm")]
        public Decimal? FeesRm { get; set; }
        [Database("gst_rm"), Display("GstRm")]
        public Decimal? GstRm { get; set; }
        [Database("cost_rm"), Display("CostRm")]
        public Decimal? CostRm { get; set; }
        [Database("proceeds_rm"), Display("ProceedsRm")]
        public Decimal? ProceedsRm { get; set; }
        [Database("realised_gain_loss"), Display("RealisedGainLoss")]
        public Decimal? RealisedGainLoss { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime? ReportDate { get; set; }
        [Database("isactive"), Display("Isactive")]
        public String Isactive { get; set; }

        public Decimal ActualUnits { get; set; }
        public Decimal ActualCost { get; set; }
        public Decimal TransPR { get; set; }
        public Decimal AverageCost { get; set; }
        public Decimal CurUnitHldg { get; set; }
        public Decimal SalesChargePer { get; set; }
        public Decimal? SalesChargeAmt { get; set; }
        public String TransType { get; set; }
    }
}
