﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using DiOTP.Service.IService;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class GenerateStatements : System.Web.UI.Page
    {

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else
            {
                User loginUser = (User)HttpContext.Current.Session["admin"];
                Response response = ServicesManager.isProcessedMonthlyCheck(101);
                if (response.IsSuccess)
                {
                    List<WS_Process> processList = (List<WS_Process>)response.Data;
                    if (processList.Count > 0)
                    {
                        if (processList.FirstOrDefault().status == 0)
                        {
                            generateStatus.InnerHtml = "<i class='fa fa-refresh'></i> Processing. Please check the status after sometime.";
                        }
                        else if (processList.FirstOrDefault().status == 1)
                        {
                            generateStatus.InnerHtml = "<i class='fa fa-file-o'></i> Generated.";
                            
                        }
                    }
                    else
                    {
                        Response responseGenerate = ServicesManager.GenerateStatements(loginUser);
                        if (responseGenerate.IsSuccess)
                        {
                            generateStatus.InnerHtml = "<i class='fa fa-refresh'></i> Processing. Please check the status after sometime.";
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Generate Monthly Statements / Check Status clicked",
                                TableName = "user_statements",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            //Audit Log Ends here
                        }
                    }
                }
            }
        }

        


    }
}