﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Admin.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .highcharts-color-green, .highcharts-color-green [class*=highcharts-color-], .highcharts-color-green .highcharts-point {
            fill: rgb(54, 179, 126);
        }
        .highcharts-color-blue, .highcharts-color-blue [class*=highcharts-color-], .highcharts-color-blue .highcharts-point {
            fill: rgb(0, 82, 204);
        }
        .link-container a {
            color: rgb(0, 82, 204);
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="dashboardForm" runat="server">
        <div class="container-fluid">
            <strong class="pull-right text-danger" id="AsOfDate1" runat="server"></strong>
            <div class="row">
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>MA REGISTRATIONS MILESTONE</strong></h4>
                        <figure class="highcharts-figure">
                            <div id="ma-count-chart"></div>
                        </figure>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>AUM MILESTONE</strong></h4>
                        <figure class="highcharts-figure">
                            <div id="ma-asset-chart"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>MA REGISTRATIONS CHART</strong></h4>
                        <figure class="highcharts-figure">
                            <select name="MaChartPlotType" id="MaChartPlotType" runat="server" clientidmode="static" style="                            position: absolute;
                                    margin-top: -30px;
                                    z-index: 9;
                                    right: 25px;
                            ">
                                <option value="MTD">MTD</option>
                                <option value="YTD">YTD</option>
                            </select>
                            <div id="ma-chart"></div>
                            <%--<p class="highcharts-description">
                            A basic column chart compares rainfall values between four cities.
        Tokyo has the overall highest amount of rainfall, followed by New York.
        The chart is making use of the axis crosshair feature, to highlight
        months as they are hovered over.
                        </p>--%>
                        </figure>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>MA INVESTMENTS CHART</strong></h4>
                        <figure class="highcharts-figure">
                            <select name="MaAssetPlotType" id="MaAssetPlotType" runat="server" clientidmode="static" style="position: absolute; margin-top: -30px; z-index: 9; right: 25px;">
                                <option value="MTD">MTD</option>
                                <option value="YTD">YTD</option>
                            </select>
                            <div id="aum-chart"></div>
                            <%--<p class="highcharts-description">
                            A basic column chart compares rainfall values between four cities.
        Tokyo has the overall highest amount of rainfall, followed by New York.
        The chart is making use of the axis crosshair feature, to highlight
        months as they are hovered over.
                        </p>--%>
                        </figure>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row link-container">
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>TO-DO LIST</strong></h4>
                        <table class="table table-bordered">
                            <tr>
                                <th>Address verification pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AddressChanging.aspx' data-title='MA Address Verification'>
                                        <span id="addrVerCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Bank verification pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BankDetailsVerification.aspx' data-title='Bank Details Verification'>
                                        <span id="bankVerCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Payment pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Payments.aspx' data-title='Payments'>
                                        <span id="paymentPendingCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Order pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Transactions.aspx' data-title='Transactions'>
                                        <span id="orderPendingCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>RSP enrollment pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='RegularSavingPlans.aspx' data-title='Regular Saving Enrollment'>
                                        <span id="rspPendingCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Account Opening</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AccountOpeningRequest.aspx' data-title='AccountOpeningRequest'>
                                        <span id="AccountOpeningCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center">
                            <strong>QUICK VIEW</strong>
                        </h4>
                        <table class="table table-bordered">
                            <tr>
                                <th>Hardcopy vs Softcopy requests</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='HardcopyRequest.aspx' data-title='Hardcopy Request'>
                                        <span id="newHardcopyRequestsCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>MA Bank bindings count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='MABanks.aspx' data-title='MA Bank Binding List'>
                                        <span id="newMABankBindingList" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="MaCountDiOTP" runat="server" clientidmode="static" />
        <input type="hidden" id="MaCountOracle" runat="server" clientidmode="static" />
        <input type="hidden" id="MaAssetDiOTP" runat="server" clientidmode="static" />
        <input type="hidden" id="MaAssetOracle" runat="server" clientidmode="static" />

    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <script src="/Content/js/highcharts.js"></script>
    <script>
        function numberWithCommas(number) {
            var parts = number.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        //var MaCountDiOTP = parseInt($('#MaCountDiOTP').val());
        //var MaCountOracle = parseInt($('#MaCountOracle').val());
        //var MaAssetDiOTP = parseFloat(parseFloat($('#MaAssetDiOTP').val()).toFixed(2));
        //var MaAssetOracle = parseFloat(parseFloat($('#MaAssetOracle').val()).toFixed(2));
        

        var machart = Highcharts.chart('ma-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
                //text: 'Monthly MA Accounts'
            },
            //subtitle: {
            //    text: 'Source: '
            //},
            credits: {
                href: 'javascript:;',
                text: '',
                position: {
                    align: 'right',
                    verticalAlign: 'bottom',
                }
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'MA Count'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                formatter: function () {
                    var s = [];
                    s.push('<span style="font-size:10px">' + this.points[0].key + '</span><table>');
                    $.each(this.points, function (i, point) {
                        var seriesName = point.series.name;
                        var y = point.y;
                        var x = point.x;

                        var str = y.toString().split('.');
                        if (str[0].length >= 4) {
                            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                        }
                        //s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%d/%m/%Y', new Date(x)) + '</span><br/><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
                        s.push('<tr><td style="color:' + point.color + ';padding:0">' + seriesName + ': </td>' +
                            '<td style="padding:0"><b>' + (str.join('.')) + '</b></td></tr>');
                    });
                    s.push('</table>');

                    return s.join('');
                },
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    //grouping: false,
                    shadow: false,
                    //pointPadding: 0.2,
                    borderWidth: 0,
                    showInLegend: true,
                    dataLabels: {
                        enabled: true,
                        //formatter: function () {
                        //    var y = this.y;
                        //    var max = this.series.yAxis.max,
                        //        color = this.y / max < 0.05 ? 'black' : 'white'; // 5% width
                        //    return '<span style="color: ' + color + '">' + y + ' </span>';
                        //},
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '10px',
                            fontFamily: 'helvetica, arial, sans-serif',
                            textShadow: false,
                            fontWeight: 'bold',
                            textOutline: false
                        }
                    }
                },
                point: {
                    events: {
                        legendItemClick: function () {
                            this.slice(null);
                            return false;
                        }
                    }
                }
            },
            series: []
        });
        var aumchart = Highcharts.chart('aum-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
                //text: 'Monthly Investment'
            },
            //subtitle: {
            //    text: 'Source: '
            //},
            credits: {
                href: 'javascript:;',
                text: '',
                position: {
                    align: 'right',
                    verticalAlign: 'bottom',
                }
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Investment (MYR)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                formatter: function () {
                    var s = [];
                    s.push('<span style="font-size:10px">' + this.points[0].key + '</span><table>');
                    $.each(this.points, function (i, point) {
                        var seriesName = point.series.name;
                        var y = point.y;
                        var x = point.x;

                        var str = y.toString().split('.');
                        if (str[0].length >= 4) {
                            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                        }
                        //s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%d/%m/%Y', new Date(x)) + '</span><br/><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
                        s.push('<tr><td style="color:' + point.color + ';padding:0">' + seriesName + ': </td>' +
                            '<td style="padding:0"><b>MYR ' + (str.join('.')) + '</b></td></tr>');
                    });
                    s.push('</table>');

                    return s.join('');
                },
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    //grouping: false,
                    shadow: false,
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                point: {
                    events: {
                        legendItemClick: function () {
                            this.slice(null);
                            return false;
                        }
                    }
                }
            },
        });
        $(document).ready(function () {

            $.ajax({
                url: "Dashboard.aspx/GetMACountStats",
                async: false,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var json = data.d;
                    var MaCountDiOTP = parseInt(json.MaCountDiOTP);
                    var MaCountOracle = parseInt(json.MaCountOracle);

                    var donutChartMACount = Highcharts.chart('ma-count-chart', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                        },
                        title: {
                            text: numberWithCommas(MaCountDiOTP) + ' <small>vs</small> ' + numberWithCommas(MaCountOracle),
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 80,
                            useHTML: true,
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                            followPointer: true,
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        credits: {
                            href: 'javascript:;',
                            text: '',
                            position: {
                                align: 'right',
                                verticalAlign: 'bottom',
                            }
                        },
                        plotOptions: {
                            pie: {
                                shadow: false,
                                showInLegend: true,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.percentage:.2f} %',
                                    style: {
                                        fontWeight: 'normal'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '100%',
                                point: {
                                    events: {
                                        legendItemClick: function () {
                                            this.slice(null);
                                            return false;
                                        }
                                    }
                                }
                            },
                        },
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal'
                                    },
                                    yAxis: {
                                        labels: {
                                            align: 'left',
                                            x: 0,
                                            y: -5
                                        },
                                        title: {
                                            text: null
                                        }
                                    },
                                    subtitle: {
                                        text: null
                                    },
                                    credits: {
                                        enabled: false
                                    }
                                }
                            }],
                        }
                    });

                    var donutChartMACountSeriesData = [];
                    donutChartMACountSeriesData.push({ className: "highcharts-color-green", name: "Registered", y: MaCountDiOTP });
                    donutChartMACountSeriesData.push({ className: "highcharts-color-blue", name: "Unregistered", y: MaCountOracle - MaCountDiOTP });
                    //donutChartMACountSeriesData.push(["Unregistered", MaCountOracle - MaCountDiOTP]);
                    donutChartMACount.addSeries({
                        type: 'pie',
                        name: 'Percent',
                        innerSize: '50%',
                        data: donutChartMACountSeriesData,
                        type: 'pie',
                        useHTML: true
                    });

                }
            });

            $.ajax({
                url: "Dashboard.aspx/GetMAAssetStats",
                async: false,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var json = data.d;
                    var MaAssetDiOTP = parseInt(json.MaAssetDiOTP);
                    var MaAssetOracle = parseInt(json.MaAssetOracle);

                    var donutChartMAAsset = Highcharts.chart('ma-asset-chart', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                        },
                        title: {
                            text: numberWithCommas(MaAssetDiOTP) + ' <small>vs</small> ' + numberWithCommas(MaAssetOracle),
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 80,
                            useHTML: true,
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                            followPointer: true,
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        credits: {
                            href: 'javascript:;',
                            text: '',
                            position: {
                                align: 'right',
                                verticalAlign: 'bottom',
                            }
                        },
                        plotOptions: {
                            pie: {
                                shadow: false,
                                showInLegend: true,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.percentage:.2f} %',
                                    style: {
                                        fontWeight: 'normal'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '100%',
                                point: {
                                    events: {
                                        legendItemClick: function () {
                                            this.slice(null);
                                            return false;
                                        }
                                    }
                                }
                            }
                        },
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal'
                                    },
                                    yAxis: {
                                        labels: {
                                            align: 'left',
                                            x: 0,
                                            y: -5
                                        },
                                        title: {
                                            text: null
                                        }
                                    },
                                    subtitle: {
                                        text: null
                                    },
                                    credits: {
                                        enabled: false
                                    }
                                }
                            }],
                        }
                    });
                    var donutChartMAAssetSeriesData = [];

                    donutChartMAAssetSeriesData.push({ className: "highcharts-color-green", name: "eApexIs Asset", y: MaAssetDiOTP });
                    donutChartMAAssetSeriesData.push({ className: "highcharts-color-blue", name: "Oracle Asset", y: MaAssetOracle - MaAssetDiOTP });
                    donutChartMAAsset.addSeries({
                        type: 'pie',
                        name: 'Percent',
                        innerSize: '50%',
                        data: donutChartMAAssetSeriesData,
                        type: 'pie',
                        useHTML: true
                    });

                }
            });


            var colorStrings = ['blue', 'green'];
            var theMonths = ["Jan", "Feb", "Mar", "Apr", "May",
                "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var YTDCDATA;
            $('#MaChartPlotType').change(function () {
                if ($(this).val() == 'MTD') {
                    $.ajax({
                        url: "Dashboard.aspx/GetMACountSeries",
                        async: false,
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { 'PlotType': JSON.stringify($(this).val()) },
                        success: function (data) {
                            var json = data.d;
                            YTDCDATA = json;
                            while (machart.series.length > 0) machart.series[0].remove(true);
                            machart.xAxis[0].visible = true;
                            machart.xAxis[0].setCategories(theMonths);
                            var html = "";
                            $.each(json, function (idx, y) {
                                var series = [];
                                for (i = 0; i < 12; i++) {
                                    var rec = $.grep(y.mACountByMonths, function (obj, index) {
                                        return theMonths[i] == obj.Month;
                                    });
                                    if (rec.length == 1)
                                        series.push(parseInt(rec[0].MACOUNT));
                                    else
                                        series.push(0);
                                }
                                machart.addSeries({
                                    className: "highcharts-color-" + colorStrings[idx],
                                    data: series,
                                    name: y.YEAR,
                                    useHTML: true
                                });
                            });
                        }
                    });
                }
                else if ($(this).val() == 'YTD') {
                    var theYears = $.map(YTDCDATA, function (obj, idx) {
                        return obj.YEAR;
                    });
                    while (machart.series.length > 0) machart.series[0].remove(true);
                    machart.xAxis[0].visible = false;
                    var json = YTDCDATA;
                    var html = "";
                    console.log(json);
                    $.each(json, function (idx, y) {
                        var series = [];
                        var yearCount = y.mACountByMonths.map(function (obj) { return parseInt(obj.MACOUNT); }).reduce(function (a, b) {
                            return a + b;
                        });
                        console.log(yearCount);
                        series.push(parseInt(yearCount));
                        machart.addSeries({
                            className: "highcharts-color-" + colorStrings[idx],
                            data: series,
                            name: y.YEAR,
                            useHTML: true
                        });
                    });

                }
            });
            $('#MaChartPlotType').change();

            var YTDADATA;
            $('#MaAssetPlotType').change(function () {
                if ($(this).val() == 'MTD') {
                    $.ajax({
                        url: "Dashboard.aspx/GetMAAssetSeries",
                        async: false,
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { 'PlotType': JSON.stringify($(this).val()) },
                        success: function (data) {
                            var json = data.d;
                            YTDADATA = json;
                            while (aumchart.series.length > 0) aumchart.series[0].remove(true);
                            aumchart.xAxis[0].visible = true;
                            aumchart.xAxis[0].setCategories(theMonths);
                            var html = "";
                            $.each(json, function (idx, y) {
                                var series = [];
                                for (i = 0; i < 12; i++) {
                                    var rec = $.grep(y.mAAssetByMonths, function (obj, index) {
                                        return theMonths[i] == obj.Month;
                                    });
                                    if (rec.length == 1)
                                        series.push(parseInt(rec[0].TRANSAMT));
                                    else
                                        series.push(0);
                                }
                                aumchart.addSeries({
                                    className: "highcharts-color-" + colorStrings[idx],
                                    data: series,
                                    name: y.YEAR,
                                    useHTML: true
                                });
                            });
                        }
                    });
                }
                else if ($(this).val() == 'YTD') {
                    var theYears = $.map(YTDADATA, function (obj, idx) {
                        return obj.YEAR;
                    });
                    while (aumchart.series.length > 0) aumchart.series[0].remove(true);
                    aumchart.xAxis[0].visible = false;
                    var json = YTDADATA;
                    var html = "";
                    $.each(json, function (idx, y) {
                        var series = [];
                        var yearCount = y.mAAssetByMonths.map(function (obj) { return parseInt(obj.TRANSAMT); }).reduce(function (a, b) {
                            return a + b;
                        });
                        series.push(parseInt(yearCount));
                        aumchart.addSeries({
                            className: "highcharts-color-" + colorStrings[idx],
                            data: series,
                            name: y.YEAR,
                            useHTML: true
                        });
                    });

                }
            });
            $('#MaAssetPlotType').change();

        });

    </script>
</body>
</html>
