﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;

namespace DiOTP.WebApp
{
    public partial class CorporateAccounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string dirPath = HttpContext.Current.Server.MapPath("/Downloads/Corporate Documents/");

            var dirInfo = new DirectoryInfo(dirPath);

            List<FileInfo> fileInfosList = dirInfo.GetFiles("*")
                            //.OrderByDescending(f => DateTime.ParseExact(f.Name.Replace(".PDF", "").Replace(".pdf", "").Split('_')[3], "yyyyMMdd", CultureInfo.InvariantCulture))
                            
                            .ToList();
            StringBuilder asb = new StringBuilder();
            int index = 1;
            fileInfosList.ForEach(x =>
                        {
                            string fileName = x.Name.Replace(".PDF", "").Replace(".pdf", "");
                        //string[] keys = fileName.Split('_');

                        //string accNo = keys[0];
                        //string fundId = keys[1].ToUpper();
                        //string statementType = keys[2].ToUpper();
                        //string statementDate = keys[3].ToUpper();
                        //string statementTag = "";
                        //if (keys.Length == 5)
                        //    statementTag = keys[4].ToUpper();

                        //Response responseUFI = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code = '" + fundId + "' ", 0, 0, false);
                        UtmcFundInformation utmcFundInformation = new UtmcFundInformation();
                        //if (responseUFI.IsSuccess)
                        //{
                        //    utmcFundInformation = ((List<UtmcFundInformation>)responseUFI.Data).FirstOrDefault();
                        //}




                            asb.Append(@"
                                        <div class='col-md-6'>
                                            <a target='_blank' href='/Downloads/Corporate Documents/" + fileName + @".pdf' class='row'>
                                                <i class='fa fa-file-text-o col-md-3' style='font-size: 50px;'></i>
                                                <div class='col-md-6 text-left' style='max-height: 49px;'>
                                                    <p>" + fileName + @"</p>
                                                </div>
                                            </a>
                                        </div>


                                    ");
                            index++;
                        });

            filesBody.InnerHtml = asb.ToString();
            //private static readonly Lazy<ISiteContentService> lazySiteContentObj = new Lazy<ISiteContentService>(() => new SiteContentService());

            //public static ISiteContentService ISiteContentService { get { return lazySiteContentObj.Value; } }

            //protected void Page_Load(object sender, EventArgs e)
            //{
            //    try
            //    {
            //        if (Request.QueryString["Id"] != null)
            //        {
            //            Int32 Id = Convert.ToInt32(Request.QueryString["Id"]);
            //            Response response = ISiteContentService.GetSingle(Id);
            //            if (response.IsSuccess)
            //            {
            //                SiteContent sc = (SiteContent)response.Data;
            //                contentTitle.InnerHtml = sc.Title;
            //                contentShortDisplayContent.InnerHtml = sc.ShortDisplayContent;
            //                contentDate.InnerHtml = sc.CreatedDate.ToString("dd-MM-yyyy");
            //                contentDate1.InnerHtml = sc.CreatedDate.ToString("dd-MM-yyyy");
            //                contentContent.InnerHtml = sc.Content;
            //            }
            //            else
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            //    }
            //}
        }
    }
}