﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System.Configuration;

namespace DiOTP.Service
{
    public class PageEvent : PdfPageEventHelper
    {
        private static readonly Lazy<IMaHolderRegService> lazyObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyObj.Value; } }

        public void AddLogo(PdfWriter writer, Document document)
        {
            string imageUrl = ConfigurationManager.AppSettings["LogoPath"].ToString();
            Image jpg = Image.GetInstance(imageUrl);

            //Resize image depend upon your need
            jpg.ScaleToFit(140f, 120f);
            //Give space before image
            jpg.SpacingBefore = 10f;
            //Give some space after the image
            jpg.SpacingAfter = 1f;
            jpg.Alignment = Element.ALIGN_RIGHT;
            document.Add(jpg);
        }

        public void AddHeader(PdfWriter writer, Document document, Response responseMHR, DateTime lastDate)
        {
            Font fontText = FontFactory.GetFont("ARIAL", 9);
            Font fontTextBold = FontFactory.GetFont("ARIAL", 9, Font.BOLD);

            //MasterAccount user = MasterAccountData.GetUserDetail(accountNo);
            //Response responseMHR = IMaHolderRegService.GetDataByPropertyName(nameof(MaHolderReg.HolderNo), accountNo, true, 0, 0, false);
            if (responseMHR.IsSuccess)
            {
                MaHolderReg user = (MaHolderReg)responseMHR.Data;
                Logger.WriteLog("maHG: " + user.HolderNo);
                //if(users.Count > 0)
                {
                    //sMaHolderReg user = users.FirstOrDefault();
                    Paragraph address3 = new Paragraph();
                Paragraph address4 = new Paragraph();

                //MasterAccount masterAccount = MasterAccountData.GetMasterAccount(accountNo);
                Paragraph paragraph = new Paragraph(DateTime.Now.ToString("dd MMMM yyyy") + "\n\n", fontText);
                Paragraph paragraph2 = new Paragraph(user.Name1, fontText);
                Paragraph address1 = new Paragraph(user.Addr1, fontText);
                Paragraph address2 = new Paragraph(user.Addr2, fontText);
                if (user.Addr3 != "")
                {
                    address3 = new Paragraph(user.Addr3, fontText);
                }
                if (user.Addr4 != "")
                {
                    address4 = new Paragraph(user.Addr4, fontText);
                }
                Paragraph paragraph4 = new Paragraph("\nStatement Date        :      " + lastDate.ToString("dd/MM/yyy") + "\n" +
                                                     "Master Account        :      " + user.HolderCls + "" + user.HolderNo.Value.ToString().PadLeft(6, '0') + "\n", fontText);
                Paragraph paragraph5 = new Paragraph("Summary of portfolio holdings as at " + lastDate.ToString("dd MMMM, yyyy") + ":", fontTextBold);

                document.Add(paragraph);
                document.Add(paragraph2);
                document.Add(address1);
                document.Add(address2);
                if (user.Addr3 != "")
                {
                    document.Add(address3);
                }
                if (user.Addr4 != "")
                {
                    document.Add(address4);
                }
                document.Add(paragraph4);
                document.Add(paragraph5);
            }
            }
            else
            {
                Logger.WriteLog("Get Ma Holder Reg: " + responseMHR.Message);

            }
        }

        public void AddHeader2(PdfWriter writer, Document document, DateTime startDate, DateTime endDate)
        {
            Font fontTextBold = FontFactory.GetFont("ARIAL", 9, Font.BOLD);

            Paragraph paragraph = new Paragraph("Transactions completed during the period from " + startDate.ToString("dd MMMM, yyyy") + " to " + endDate.ToString("dd MMMM, yyyy") + ":", fontTextBold);
            document.Add(paragraph);
        }

        public void AddInformation(Document document)
        {
            Font fontText = FontFactory.GetFont("ARIAL", 8);
            Font fontBold = FontFactory.GetFont("ARIAL", 8, Font.BOLD);
            Paragraph paragraph = new Paragraph("Please review this statement. If you notice there is any discrepancy, please notify us immediately.\n\n", fontText);
            Paragraph paragraph2 = new Paragraph("Contact Information\n" + "Who should I contact for further information or to lodge a complaint?\n" +
                                                    "1. For internal dispute resolution, you may contact our Customer Service personnel at 03-2095 9999.\n" +
                                                    "2. If you are dissatisfied with the outcome of the internal dispute resolution process, please refer you dispute to the Securities Industries" +
                                                    "Dispute Resolution Corporation (SIDREC) at 03-2282 2280.\n" +
                                                    "3. You can also direct your complaint to the Securities Commision(SC) even if you have initiated a dispute resolution process with " +
                                                    "SIDREC. To make a complaint, please contact the SC's Investor Affair & Complaints Department at 03-6204 8999\n\n", fontText);
            Paragraph paragraph3 = new Paragraph("Legend", fontBold);
            Paragraph paragraph4 = new Paragraph("Trans-Transaction; BI-Unit Split; SA-Purchase; BU-Bonus Unit; RD-Redemption; " +
                                                 "SWI-Switching In; TR-Transfer; SWO-Switching Out; DD-Dividend\n", fontText);
            PdfPTable legendTable = new PdfPTable(1);

            PdfPCell cell = new PdfPCell(paragraph4);
            PdfPCell cell2 = new PdfPCell(paragraph3);
            legendTable.WidthPercentage = 100;
            cell2.BorderWidthBottom = 0;
            cell.BorderWidthTop = 0;
            cell.PaddingBottom = 8f;
            document.Add(paragraph);
            document.Add(paragraph2);
            legendTable.AddCell(cell2);
            legendTable.AddCell(cell);
            document.Add(legendTable);

        }

        public void OnEndPage(PdfWriter writer, Document doc)
        {
            Font fontTextBold = FontFactory.GetFont("ARIAL", 8, Font.BOLD);
            Font fontAdress = FontFactory.GetFont("ARIAL", 7);

            Paragraph paragraph = new Paragraph("APEX INVESTMENT SERVICES BERHAD (420390-M)", fontTextBold);
            paragraph.Alignment = Element.ALIGN_CENTER;
            Paragraph paragraph2 = new Paragraph("3RD FLOOR MENARA MBSB, 46 JALAN DUNGUN, DAMANSARA HEIGHTS, 50490 W.P KUALA LUMPUR TEL:03-2095 999 FAX:03-2095 0693", fontAdress);
            Paragraph paragraph3 = new Paragraph();
            PdfPTable footerTbl = new PdfPTable(1);
            footerTbl.TotalWidth = 510;
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell = new PdfPCell(paragraph);
            PdfPCell cell2 = new PdfPCell(paragraph2);
            cell.BorderWidthTop = 0;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 1;
            cell.PaddingBottom = 3f;
            cell2.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            footerTbl.AddCell(cell);
            footerTbl.AddCell(cell2);
            footerTbl.WriteSelectedRows(0, -1, 45, 65, writer.DirectContent);
        }

        public void AddFooter(string pdfFilePath, Document document)
        {
            byte[] bytes = File.ReadAllBytes(pdfFilePath);
            Font blackFont = FontFactory.GetFont("Arial", 7, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {

                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase("Page " + i.ToString() + " of " + pages.ToString(), blackFont), document.PageSize.Width - document.LeftMargin, 7f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(pdfFilePath, bytes);
        }
    }
}
