﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AgentMaster.master" CodeBehind="AgentDashboard.aspx.cs" Inherits="DiOTP.WebApp.AgentDashboard1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Dashboard</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Dashboard</h3>
            </div>
            <div id="containerPage">
                <style>
                    .cpd-points-count {
                        height: 100px;
                        border: 4px solid #e4b418;
                        text-align: center;
                    }

                        .cpd-points-count .head {
                            text-align: center;
                            background: #e4b418;
                            height: 25px;
                            line-height: 22px;
                            font-weight: 900;
                            color: #fff;
                            letter-spacing: 1.5px;
                        }

                        .cpd-points-count .cpd-year {
                            font-weight: 500;
                            letter-spacing: 1px;
                        }

                        .cpd-points-count .cpd-points-value {
                            line-height: 70px;
                            font-size: 40px;
                            font-weight: 600;
                        }

                    .trTextFormat {
                        text-align: center;
                    }

                    .agentDashboardHr {
                        margin-top: 20px;
                        margin-bottom: 20px;
                    }

                    .agentDashboardProfDetails {
                        margin-bottom: 5px;
                        border-bottom: 1px solid #808080;
                    }

                    .agentCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: ghostwhite;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                    }

                    .agentCommissionCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: lightyellow;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .agentTransactionCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: aliceblue;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .noticeCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: lightgoldenrodyellow;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .segmentCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: floralwhite;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .whiteCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: white;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                        
                    }

                    .agentDetailTable, tbody.agentDetailTable > tr {
                        border: 2px solid grey;
                    }

                    /*Start of announcement CSS*/
                    .announcement-img {
                        width: 100%;
                        height: 350px;
                    }

                    .mySlides {
                        display: none;
                    }

                    .slideshow-container {
                        max-width: 1000px;
                        position: relative;
                        margin: auto;
                    }

                    .fade {
                        -webkit-animation-name: fade;
                        -webkit-animation-duration: 5s;
                        animation-name: fade;
                        animation-duration: 5s;
                    }

                    @-webkit-keyframes fade {
                        from {
                            opacity: .4
                        }

                        to {
                            opacity: 1
                        }
                    }

                    @keyframes fade {
                        from {
                            opacity: .4
                        }

                        to {
                            opacity: 1
                        }
                    }

                    /* Next & previous buttons */
                    .prev, .next {
                        cursor: pointer;
                        position: absolute;
                        top: 50%;
                        width: auto;
                        padding: 16px;
                        margin-top: -22px;
                        color: white;
                        font-weight: bold;
                        font-size: 18px;
                        transition: 0.6s ease;
                        border-radius: 0 3px 3px 0;
                        user-select: none;
                    }

                    /* Position the "next button" to the right */
                    .next {
                        right: 0;
                        border-radius: 3px 0 0 3px;
                    }

                        /* On hover, add a black background color with a little bit see-through */
                        .prev:hover, .next:hover {
                            background-color: rgba(0,0,0,0.8);
                        }

                    .caption-text {
                        color: #f2f2f2;
                        font-size: 15px;
                        padding: 8px 12px;
                        position: absolute;
                        bottom: 8px;
                        width: 100%;
                        text-align: center;
                    }

                    /*End of announcement CSS*/
                </style>
                <%--Segment 1--%>
                <div class="container segmentCard" style="padding: 25px 25px 25px 25px">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <%--<li class="nav-item active">
                                    <a class="nav-link active" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="home" aria-selected="true">News</a>
                                </li>--%>
                                <li class="nav-item active">
                                    <a class="nav-link" id="upcoming-tab" data-toggle="tab" href="#upcoming" role="tab" aria-controls="profile" aria-selected="true">Upcoming Events</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <%--<div class="tab-pane fade in active" id="news" role="tabpanel" aria-labelledby="news-tab">
                                    <%--Announcement Slide Show Content Here
                                    <div class="row">
                                        <div class="col-md-12" style="padding-top:10px;">
                                            <div class="slideshow-container">
                                                <div class="mySlides fade">
                                                    <img class="announcement-img" src="https://media.slidesgo.com/storage/2184775/conversions/0-event-announcement-newsletter-thumb.jpg" />
                                                    <div class="caption-text"></div>
                                                </div>

                                                <div class="mySlides fade">
                                                    <img class="announcement-img" src="https://i.pinimg.com/originals/52/95/b8/5295b82f2b680be1b83390f586beda95.jpg" />
                                                    <div class="caption-text"></div>
                                                </div>

                                                <div class="mySlides fade">
                                                    <img class="announcement-img" src="https://i.pinimg.com/originals/37/7f/61/377f6102282835892d3831e1830e93ee.jpg" />
                                                    <div class="caption-text"></div>
                                                </div>

                                                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="tab-pane fade in active" id="upcoming" role="tabpanel" aria-labelledby="upcoming-tab">
                                    <div class="row">
                                        <div class="col-md-12" style="padding-top:10px;">
                                            <div class="table-responsive" style="border: none!important;">
                                                <table class="table table-font-size-13 table-bordered " id="upcomingTable" style="width: 95% !important">
                                                    <thead id="upcomingthead" runat="server">
                                                        <tr>
                                                            <th class="text-center">Index</th>
                                                            <th class="text-center">Upcoming Event Description</th>
                                                            <th class="text-center">Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-center" id="upcomingtbody" runat="server" clientidmode="Static">
                                                        <tr>
                                                            <td>1
                                                            </td>
                                                            <td>
                                                                <a href="/UpcomingDetails.aspx" data-target="#AgentEventModal" data-toggle="modal">C.U.T.E Training</a>
                                                            </td>
                                                            <td>30/09/2021
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2
                                                            </td>
                                                            <td>
                                                                <a href="#">Agent Commission Update Notice</a>
                                                            </td>
                                                            <td>31/09/2021
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3
                                                            </td>
                                                            <td>
                                                                <a href="#">Agent Policy Update Notice</a>
                                                            </td>
                                                            <td>03/10/2021
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4
                                                            </td>
                                                            <td>
                                                                <%--<a href="javascript:;" data-id="1" data-original-title='View Upcoming Event Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='UpcomingDetails.aspx' data-title='View Upcoming Event Details' data-action='View'> FIMM Expiry Notice</a>--%>
                                                                <a href="javascript:;" data-id="01" data-original-title="View User Details" data-trigger="hover" data-placement="bottom" class="popovers action-button" data-url="UpcomingDetails.aspx" data-title="View details" data-action="View">FIMM Expiry Notice&nbsp;<i class='fa fa-eye'></i></a>
                                                                <%--<a href="UpcomingDetails.aspx" class="openModal">FIMM Expiry Notice</a>--%>
                                                            </td>
                                                            <td>08/10/2021
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5
                                                            </td>
                                                            <td>
                                                                <a href="#">Excel and Elevate Training</a>
                                                            </td>
                                                            <td>15/10/2021
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <%--<div class="container agentCard">
                    <div class="row">
                        <div class="col-md-3">
                            &nbsp
                        </div>
                        <div class="col-md-6">
                            <div class="row"><h4>My Personal Details:</h4>
                            
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-3">
                            <div id="agentImage" runat="server">

                            </div>
                            <%--Agent Image

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                
                                <table class="table table-bordered table-condensed agentDetailTable table-responsive">
                                    <tbody class="agentDetailTable">
                                        <tr>
                                            <td>Agent Name:</td>
                                            <td>
                                                <b><asp:Label ID="lblAgentName" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Agent Code:</td>
                                            <td>
                                                <b><asp:Label ID="lblAgentCode" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rank:</td>
                                            <td>
                                                <b><asp:Label ID="lblRank" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Join Date:</td>
                                            <td>
                                                <b><asp:Label ID="lblJoinDate" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Agency Group:</td>
                                            <td>
                                                <b><asp:Label ID="lblAgencyGroup" Text="Galaxy Agency Group" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Region/Branch:</td>
                                            <td>
                                                <b><asp:Label ID="lblRegion" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Business Development Manager:</td>
                                            <td>
                                                <b><asp:Label ID="lblBDM" Text="Ms. Clarkson, 012-8752613" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Direct Downline:</td>
                                            <td>
                                                <b><asp:Label ID="lblDirectDownline" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Indirect Downline:</td>
                                            <td>
                                                <b><asp:Label ID="lblIndirectDownline" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        
                    </div>

                    <div class="row" style="padding-bottom: 10px;">                       
                        <div class="col-md-6 col-md-offset-3">
                            <div class="row">
                                <h4>My Upline Agent Details</h4>
                                <table class="table table-bordered table-condensed table-responsive agentDetailTable">
                                    <tbody class="agentDetailTable">
                                        <tr>
                                            <td>Upline Name:</td>
                                            <td>
                                                <b><asp:Label ID="lblUplineName" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rank:</td>
                                            <td>
                                                <b><asp:Label ID="lblUplineRank" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No:</td>
                                            <td>
                                                <b><asp:Label ID="lblUplineMobileNo" Text="" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
                </div>--%>
                <hr class="agentDashboardHr" />

                <%--Segment 2--%>
                <div class="container segmentCard">
                    <div class="row">
                        <div class="col" style="padding-left: 20px">
                            <h4>My Performance:</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9" style="padding-bottom: 10px;">
                            <span class="fa fa-info-circle noticeCard" style="font-size: 14px; padding: 10px 10px 10px 10px; height:100px;">Notice:<br /><br />
                                1. Every Agent's total sales should be at least <b>RM30,000</b> per annum as per agency policy.<br /><br />
                                2. Every Agent must accumulate at least <b>16</b> CPD Points per annum as per FIMM policy.
                            </span>
                        </div>
                        <%--CPD POINTS--%>
                        <div class="col-sm-3">
                            <div class="row" style="padding-right:10px;">
                                <div class="cpd-points-count">
                                    <div class="head">CPD POINTS <span class="cpd-year" id="CPDYear" runat="server"></span></div>
                                    <span class="cpd-points-value" id="lblCPDPoints" runat="server"></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row" style="padding-bottom:10px;">
                        <div class="col-sm-6">
                            <div class="whiteCard">
                                <figure class="highcharts-figure">
                                    <div id="salesChart"></div>
                                </figure>
                            </div>                           
                        </div>
                        <div class="col-sm-6">
                            <div class="whiteCard">
                                <figure class="highcharts-figure">
                                    <div id="donutSalesChart"></div>
                                </figure>
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">AUM</th>
                                    </tr>
                                </thead>
                                <tbody class="table-st">
                                    <tr>
                                        <td>Personal</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblPersonalVal" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Personal Group</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblPersonalGroupVal" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total Group</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblTotalGroupVal" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-4">
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">Sales</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MTD PS</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblSalesMTDPS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MTD PGS</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblSalesMTDPGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MTD TGS</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblSalesMTDTGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD PS</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblSalesYTDPS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD PGS</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblSalesYTDPGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD TGS</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblSalesYTDTGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-4">
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">Redemption</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MTD PD</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblRdMPD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MTD PGD</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblRdMPGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MTD TGD</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblRdMTGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD PD</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblRdYPD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD PGD</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblRdYPGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD TGD</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblRdYTGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <%--CPD POINTS--%>
                        <%--<div class="col-sm-3">
                            <div class="row" style="padding-right: 10px;">
                                <div class="cpd-points-count">
                                    <div class="head">CPD POINTS <span class="cpd-year" id="CPDYear" runat="server"></span></div>
                                    <span class="cpd-points-value" id="lblCPDPoints" runat="server"></span>
                                </div>
                            </div>

                        </div>--%>
                    </div>


                </div>
                <hr class="agentDashboardHr" />

                <%-- Segment 3--%>
                <div class="container segmentCard">
                    <div class="row" style="padding-left: 20px;">
                        <div class="col">
                            <h4>My Commission:</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">Commission</th>
                                    </tr>
                                </thead>
                                <tbody class="table-st">
                                    <tr>
                                        <td>MTD PSC</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblCommissionMPSC" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>YTD PSC</td>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblCommissionYPSC" CssClass="trTextFormat" Text="0" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="padding-bottom:10px;">
                            <div class="back-board clearfix whiteCard">
                                <figure class="highcharts-figure">
                                    <select name="commChartPlotType" id="commChartPlotType" runat="server" clientidmode="static" style="position: absolute; margin-top: 10px; z-index: 9; right: 25px;">
                                        <option value="PC">Personal Commission</option>
                                        <option value="OC">Override Commission</option>
                                        <option value="ALL">All</option>
                                    </select>
                                    <input type="hidden" id="agentCode" name="agentCode" value="" clientidmode="static" runat="server" />
                                    <div id="commChart"></div>
                                </figure>
                            </div>
                        </div>

                    </div>
                </div>
                <hr class="agentDashboardHr" />

                <%--Segment 4--%>
                <div class="container segmentCard">
                    <div class="row">
                        <div class="col" style="padding-left: 20px;">
                            <h4>My Direct Downline:</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="border: none!important;">
                                <table class="table table-font-size-13 table-bordered " id="directDownlineTable" style="width: 95% !important">
                                    <thead id="theadDirectList" runat="server">
                                        <tr>
                                            <th>Index</th>
                                            <th>Consultant Name</th>
                                            <th>Consultant Code</th>
                                            <th>Status</th>
                                            <th>Rank</th>
                                            <th>Personal AUM (MYR)</th>
                                            <th>Incentive Trip Personal Points</th>
                                            <th>Accumulated CPD Points</th>
                                            <th>FIMM Expiry Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyDirectDownline" runat="server" clientidmode="Static">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="agentDashboardHr" />
                <%--Segment 5--%>
                <div class="container segmentCard">
                    <div class="row">
                        <div class="col" style="padding-left: 20px;">
                            <h4>My Indirect Downline:</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="border: none!important;">
                                <table class="table table-font-size-13 table-bordered " id="indirectDownlineTable" style="width: 95% !important">
                                    <thead id="theadIndirectList" runat="server">
                                        <tr>
                                            <th>Index</th>
                                            <th>Consultant Name</th>
                                            <th>Consultant Code</th>
                                            <th>Status</th>
                                            <th>Rank</th>
                                            <th>Personal AUM (MYR)</th>
                                            <th>Incentive Trip Personal Points</th>
                                            <th>Accumulated CPD Points</th>
                                            <th>FIMM Expiry Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyIndirectDownline" runat="server" clientidmode="Static">
                                        <%--<tr class="trTextFormat">
                                                <td>1</td>
                                                <td>Alex Wong</td>
                                                <td>CC0014</td>
                                                <td>Active</td>
                                                <td>WM</td>
                                                <td>10,000.00</td>
                                                <td>10</td>
                                                <td>25</td>
                                                <td>Active</td>
                                            </tr>--%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%--<script src="Content/mycj/common-scripts.js?v=1.0"></script>--%>
    <script src="/Content/js/highcharts.js"></script>
    <script src="/Content/assets/js/jquery.min.js"></script>
    <script src="/Content/assets/bootstrap/js/bootstrap.min.js"></script>

    <%--Script For MODAL POPUP--%>
    <script>
        $('.action-button').click(function () {
            //$('body').attr('data-action-active', 'View');
            var title = $(this).attr('data-title');
            var url = $(this).attr('data-url');
            var Id = $(this).attr('data-id');
            //var allDataAttributes = $(this).data();
            //console.log(allDataAttributes);
            //var cache = [];
            //var jsonString = JSON.stringify(allDataAttributes, function (key, value) {
            //    if (typeof value === 'object' && value !== null) {
            //        if (cache.indexOf(value) !== -1) {
            //             Duplicate reference found
            //            try {
            //                 If this value does not reference a parent it can be deduped
            //                return JSON.parse(JSON.stringify(value));
            //            } catch (error) {
            //                 discard key if value cannot be deduped
            //                return;
            //            }
            //        }
            //         Store value in our collection
            //        cache.push(value);
            //    }
            //    return value;
            //});
            //cache = null; // Enable garbage collection
            //var jsonString = JSON.stringify(allDataAttributes);
            //var jsonAttr = JSON.parse(jsonString);
            //Object.keys(jsonAttr).forEach(function (key) {
            //    if (key.indexOf('.') > -1)
            //        delete jsonAttr[key];
            //});
            //var param = $.param(jsonAttr);
            $('#commonModal').find('.modal-dialog').removeClass('sm');
            $('#commonModal').find('.modal-dialog').removeClass('md');
            $('#commonModal').find('.modal-dialog').removeClass('lg');
            $('#commonModal').modal('toggle');
            $('#commonModal').find('.modal-dialog').addClass('lg');
            $('#commonModalTitle').html(title);
            $('#commonModalContent').load(url);
            $('#commonModal').find('.modal-footer button').addClass('hide');

        });

    </script>

    <%--Announcement Slideshow Script--%>
    <script>

        //var slideIndex = 1;
        //var t;
        //showSlides(slideIndex);

        //function plusSlides(n) {
        //    slideIndex = slideIndex + n;
        //    clearTimeout(t);
        //    showSlides(slideIndex);
        //    console.log(slideIndex);
        //}

        //function currentSlide(n) {
        //    showSlides(slideIndex = n);
        //}

        //function showSlides(n) {
        //    var i;
        //    var slides = document.getElementsByClassName("mySlides");
        //    if (n == undefined) {
        //        n = ++slideIndex;
        //    }
        //    if (n > slides.length) {
        //        slideIndex = 1
        //    }
        //    if (n < 1) {
        //        slideIndex = slides.length
        //    }
        //    for (i = 0; i < slides.length; i++) {
        //        slides[i].style.display = "none";
        //    }
        //    slides[slideIndex - 1].style.display = "block";
        //    t = setTimeout(showSlides, 5000);  //change every 5 seconds
        //}
    </script>
    <script>
        var chart;
        chart = Highcharts.chart('commChart', {
            chart: {
                type: 'area'
            },
            title: {
                text: 'Sales Commission'
            },
            credits: {
                href: 'javascript:;',
                text: '',
                position: {
                    align: 'right',
                    verticalAlign: 'bottom',
                }
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },

            yAxis: {
                min: 0,
                title: {
                    text: 'Commission (MYR)'
                }
            },
            tooltip: {
                pointFormat: '{series.name} has commission value of <b>{point.y:,.0f}</b><br /> in { point.x }'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
        });

        $(document).ready(function () {
            var YTDCDATA;
            var colorStrings = ['blue', 'green'];
            var theMonths = ["Jan", "Feb", "Mar", "Apr", "May",
                "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            $('#commChartPlotType').change(function () {
                $.ajax({
                    url: "AgentDashboard.aspx/GetCommissionStats",
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: {
                        'commCode': JSON.stringify($(this).val())
                    },
                    success: function (data) {
                        var json = data.d;
                        YTDCDATA = json;
                        while (chart.series.length > 0) chart.series[0].remove(true);
                        chart.xAxis[0].visible = true;
                        chart.xAxis[0].setCategories(theMonths);
                        var html = "";
                        $.each(json, function (idx, y) {
                            var series = [];
                            for (i = 0; i < 12; i++) {
                                var rec = $.grep(y.statByMonth, function (obj, index) {
                                    return theMonths[i] == obj.Month;
                                });
                                if (rec.length == 1)
                                    series.push(parseInt(rec[0].commVal));
                                else
                                    series.push(0);
                            }
                            chart.addSeries({
                                className: "highcharts-color-" + colorStrings[idx],
                                data: series,
                                name: y.Year,
                                useHTML: true
                            });
                        });
                    }
                });

            });

            $('#commChartPlotType').change();

        });

    </script>

    <script>
        var currDate = new Date();
        var currYear = currDate.getFullYear();
        var barChart;

        barChart = Highcharts.chart('salesChart', {
            chart: {
                type: 'column',
            },
            title: {
                text: 'Monthly Sales Campaign'
            },
            credits: {
                href: 'javascript:;',
                text: '',
                position: {
                    align: 'right',
                    verticalAlign: 'bottom',
                }
            },
            xAxis: {
                //categories: [
                //    'Accumulated Sales for the year ' + currYear,
                //],
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Value (MYR)'
                },
                plotLines: [{
                    value: 30000,
                    color: 'green',
                    dashStyle: 'solid',
                    width: 2,
                    label: {
                        text: 'Annual Sales Target'
                    }
                }]

            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>RM{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
        });

        $(document).ready(function () {
            var theMonths = ["Jan", "Feb", "Mar", "Apr", "May",
                "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            
            //$.ajax({
            //    url: "AgentDashboard.aspx/GetSalesStats",
            //    async: false,
            //    contentType: 'application/json; charset=utf-8',
            //    type: "GET",
            //    dataType: "JSON",
            //    data: {

            //    },
            //    success: function (data) {
            //        var json = data.d;
            //        YTDCDATA = json;
            //        while (barChart.series.length > 0) barChart.series[0].remove(true);
            //        barChart.xAxis[0].visible = true;
            //        barChart.xAxis[0].setCategories(theMonths);
            //        var html = "";
            //        $.each(json, function (idx, y) {
            //            var series = [];
            //            for (i = 0; i < 12; i++) {
            //                var rec = $.grep(y.statByMonth, function (obj, index) {
            //                    return theMonths[i] == obj.Month;
            //                });
            //                if (rec.length == 1)
            //                    series.push(parseInt(rec[0].commVal));
            //                else
            //                    series.push(0);
            //            }
            //            barChart.addSeries({
            //                className: "highcharts-color-blue",
            //                data: series,
            //                name: "Personal Sales (" + y.Year + ")",
            //                useHTML: true
            //            });
            //        });
            //    }
            //});
            var donutChart;
            $.ajax({
                url: "AgentDashboard.aspx/GetAllSalesData",
                async: false,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: {

                },
                success: function (data) {
                    var json = data.d;

                    YTDCDATA = json;
                    while (barChart.series.length > 0) barChart.series[0].remove(true);
                    barChart.xAxis[0].visible = true;
                    barChart.xAxis[0].setCategories(theMonths);
                    var html = "";
                    $.each(json.getMonthlySalesData, function (idx, y) {
                        var series = [];
                        console.log(y);
                        for (i = 0; i < 12; i++) {
                            var rec = $.grep(y.statByMonth, function (obj, index) {
                                return theMonths[i] == obj.Month;
                            });
                            if (rec.length == 1)
                                series.push(parseInt(rec[0].commVal));
                            else
                                series.push(0);
                        }
                        barChart.addSeries({
                            className: "highcharts-color-blue",
                            data: series,
                            name: "Personal Sales (" + y.Year + ")",
                            useHTML: true
                        });
                    });

                    donutChart = Highcharts.chart('donutSalesChart', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: 'Annual <br/>Sales Target <br/>(2021)',
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 15
                        },
                        credits: {
                            enabled: false
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: false,
                                    distance: -50,
                                    style: {
                                        fontWeight: 'bold',
                                        color: 'white'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '110%'
                            }
                        },
                        series: [{
                            name: 'Sales Target Achieved',
                            innerSize: '50%',
                            showInLegend: true,
                            data: [{
                                name: json.getAnnualSalesData[0].Year,
                                y: parseFloat(json.getAnnualSalesData[0].SalesValue),
                                color: '#98FB98'
                            },
                            {
                                name: json.getAnnualSalesData[1].Year,
                                y: parseFloat(json.getAnnualSalesData[1].SalesValue),
                                color: '#DC143C'
                            }]
                        }]
                    });
                }
            });
        });

    </script>

    <%--Donut Chart Sales--%>
    <script>
    //var donutChart;
    //    $(document).ready(function () {
    //        var colorStrings = ['green', 'red'];
    //        var index = 0;
    //        $.ajax({
    //            url: "AgentDashboard.aspx/GetAnnualSalesTargetStat",
    //            async: false,
    //            contentType: 'application/json; charset=utf-8',
    //            type: "GET",
    //            dataType: "JSON",
    //            data: {

    //            },
    //            success: function (data) {
    //                var json = data.d;
    //                console.log(data.d);
    //                donutChart = Highcharts.chart('donutSalesChart', {
    //                    chart: {
    //                        plotBackgroundColor: null,
    //                        plotBorderWidth: 0,
    //                        plotShadow: false,
    //                        type: 'pie'
    //                    },
    //                    title: {
    //                        text: 'Annual <br/>Sales Target <br/>(2021)',
    //                        align: 'center',
    //                        verticalAlign: 'middle',
    //                        y: 15
    //                    },
    //                    credits:{
    //	                enabled:false
    //                    },
    //                    tooltip: {
    //                        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
    //                    },
    //                    accessibility: {
    //                        point: {
    //                            valueSuffix: '%'
    //                        }
    //                    },
    //                    plotOptions: {
    //                        pie: {
    //                            dataLabels: {
    //                                enabled: false,
    //                                distance: -50,
    //                                style: {
    //                                    fontWeight: 'bold',
    //                                    color: 'white'
    //                                }
    //                            },
    //                            startAngle: -90,
    //                            endAngle: 90,
    //                            center: ['50%', '75%'],
    //                            size: '110%'
    //                        }
    //                    },
    //                    series: [{
    //                        name: 'Sales Target Achieved',
    //                        innerSize: '50%',
    //                        showInLegend: true,
    //                        data: [{
    //                                name: json[0].Year,
    //                                y: parseFloat(json[0].SalesValue),
    //                                color: '#98FB98'
    //                                },
    //                                {
    //                                name: json[1].Year,
    //                                y: parseFloat(json[1].SalesValue),
    //                                color: '#DC143C'
    //                                }]
    //                            }]
    //                });

    //            }
    //        });
    //    });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>

