using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("users")]
    public class User
    {
        [Database("id"), Display("Id"), Required]
        public Int32 Id { get; set; }
        [Database("username"), Display("Username"), Required, LengthOrValue(3, 18)]
        public String Username { get; set; }
        [Database("email_id"), Display("EmailId"), Required, Email]
        public String EmailId { get; set; }
        [Database("mobile_number"), Display("MobileNumber")]
        public String MobileNumber { get; set; }
        [Database("id_no"), Display("IdNo")]
        public String IdNo { get; set; }
        [Database("password"), Display("Password")]
        public String Password { get; set; }
        [Database("trans_pwd"), Display("TransPwd")]
        public String TransPwd { get; set; }
        [Database("unique_key"), Display("UniqueKey")]
        public String UniqueKey { get; set; }
        [Database("user_role_id"), Display("UserRoleId")]
        public Int32 UserRoleId { get; set; }
        [Database("last_login_date"), Display("LastLoginDate")]
        public DateTime? LastLoginDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("modified_by"), Display("ModifiedBy")]
        public Int32? ModifiedBy { get; set; }
        [Database("modified_date"), Display("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [Database("is_online"), Display("IsOnline")]
        public Int32 IsOnline { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("is_primary"), Display("IsPrimary")]
        public Int32 IsPrimary { get; set; }
        [Database("is_security_checked"), Display("IsSecurityChecked")]
        public Int32 IsSecurityChecked { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("last_login_ip"), Display("LastLoginIp")]
        public String LastLoginIp { get; set; }
        [Database("register_ip"), Display("RegisterIp")]
        public String RegisterIp { get; set; }
        [Database("sat_updated_date"), Display("SatUpdatedDate")]
        public DateTime? SatUpdatedDate { get; set; }
        [Database("is_sat_checked"), Display("IsSatChecked")]
        public Int32? IsSatChecked { get; set; }
        [Database("password_reset_code"), Display("PasswordResetCode")]
        public String PasswordResetCode { get; set; }
        [Database("reset_expired"), Display("ResetExpired")]
        public Int32 ResetExpired { get; set; }
        [Database("verification_code"), Display("VerificationCode")]
        public Int32? VerificationCode { get; set; }
        [Database("verify_expired"), Display("VerifyExpired")]
        public Int32 VerifyExpired { get; set; }
        [Database("login_attempts"), Display("LoginAttempts")]
        public Int32 LoginAttempts { get; set; }
        [Database("sms_attempts"), Display("SmsAttempts")]
        public Int32 SmsAttempts { get; set; }
        [Database("google_auth_attempts"), Display("GoogleAuthAttempts")]
        public Int32 GoogleAuthAttempts { get; set; }
        [Database("is_login_locked"), Display("IsLoginLocked")]
        public Int32 IsLoginLocked { get; set; }
        [Database("is_sms_locked"), Display("IsSmsLocked")]
        public Int32 IsSmsLocked { get; set; }
        [Database("is_hard_copy"), Display("IsHardCopy")]
        public Int32 IsHardCopy { get; set; }
        [Database("hardcopy_updated_date"), Display("HardcopyUpdatedDate")]
        public DateTime? HardcopyUpdatedDate { get; set; }

        [Database("activation_date"), Display("ActivationDate")]
        public DateTime? ActivationDate { get; set; }

        [Database("email_code"), Display("EmailCode")]
        public String EmailCode { get; set; }
        [Database("is_agent"), Display("IsAgent")]
        public Int32 IsAgent { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("agent_status"), Display("AgentStatus")]
        public Int32 AgentStatus { get; set; }
        [Join("id")]
        public User ModifiedByUser { get; set; }
        [Join("id")]
        public User CreatedByUser { get; set; }
        [Join("id")]
        public UserRole UserRoleIdUserRole { get; set; }
        [Join("user_id")]
        public List<UserType> UserIdUserTypes { get; set; }
        [Join("user_id")]
        public List<UserStatement> UserIdUserStatements { get; set; }
        [Join("user_id")]
        public List<UserSecurity> UserIdUserSecurities { get; set; }
        [Join("user_id")]
        public List<UserNotificationSetting> UserIdUserNotificationSettings { get; set; }
        [Join("user_id")]
        public List<UserFile> UserIdUserFiles { get; set; }
        [Join("modified_by")]
        public List<UserDetail> ModifiedByUserDetails { get; set; }
        [Join("created_by")]
        public List<UserDetail> CreatedByUserDetails { get; set; }
        [Join("user_id")]
        public List<UserDetail> UserIdUserDetails { get; set; }
        [Join("modified_by")]
        public List<UserAccount> ModifiedByUserAccounts { get; set; }
        [Join("created_by")]
        public List<UserAccount> CreatedByUserAccounts { get; set; }
        [Join("user_id")]
        public List<UserAccount> UserIdUserAccounts { get; set; }
        [Join("user_id")]
        public List<UserCorpAccount> UserIdUserCorpAccounts { get; set; }
        [Join("user_id")]
        public List<DvDistributionIntruction> UserIdDvDistributionIntructions { get; set; }
    }

    [Database("users")]
    public class customAgentUser {
        [Database("id"), Display("Id"), Required]
        public Int32 Id { get; set; }
        [Database("username"), Display("Username"), Required, LengthOrValue(3, 18)]
        public String Username { get; set; }
        [Database("email_id"), Display("EmailId"), Required, Email]
        public String EmailId { get; set; }
        [Database("mobile_number"), Display("MobileNumber")]
        public String MobileNumber { get; set; }
        [Database("id_no"), Display("IdNo")]
        public String IdNo { get; set; }
        [Database("password"), Display("Password")]
        public String Password { get; set; }
        [Database("trans_pwd"), Display("TransPwd")]
        public String TransPwd { get; set; }
        [Database("unique_key"), Display("UniqueKey")]
        public String UniqueKey { get; set; }
        [Database("user_role_id"), Display("UserRoleId")]
        public Int32 UserRoleId { get; set; }
        [Database("last_login_date"), Display("LastLoginDate")]
        public DateTime? LastLoginDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("modified_by"), Display("ModifiedBy")]
        public Int32? ModifiedBy { get; set; }
        [Database("modified_date"), Display("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [Database("is_online"), Display("IsOnline")]
        public Int32 IsOnline { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("is_primary"), Display("IsPrimary")]
        public Int32 IsPrimary { get; set; }
        [Database("is_security_checked"), Display("IsSecurityChecked")]
        public Int32 IsSecurityChecked { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("last_login_ip"), Display("LastLoginIp")]
        public String LastLoginIp { get; set; }
        [Database("register_ip"), Display("RegisterIp")]
        public String RegisterIp { get; set; }
        [Database("sat_updated_date"), Display("SatUpdatedDate")]
        public DateTime? SatUpdatedDate { get; set; }
        [Database("is_sat_checked"), Display("IsSatChecked")]
        public Int32? IsSatChecked { get; set; }
        [Database("password_reset_code"), Display("PasswordResetCode")]
        public String PasswordResetCode { get; set; }
        [Database("reset_expired"), Display("ResetExpired")]
        public Int32 ResetExpired { get; set; }
        [Database("verification_code"), Display("VerificationCode")]
        public Int32? VerificationCode { get; set; }
        [Database("verify_expired"), Display("VerifyExpired")]
        public Int32 VerifyExpired { get; set; }
        [Database("login_attempts"), Display("LoginAttempts")]
        public Int32 LoginAttempts { get; set; }
        [Database("sms_attempts"), Display("SmsAttempts")]
        public Int32 SmsAttempts { get; set; }
        [Database("google_auth_attempts"), Display("GoogleAuthAttempts")]
        public Int32 GoogleAuthAttempts { get; set; }
        [Database("is_login_locked"), Display("IsLoginLocked")]
        public Int32 IsLoginLocked { get; set; }
        [Database("is_sms_locked"), Display("IsSmsLocked")]
        public Int32 IsSmsLocked { get; set; }
        [Database("is_hard_copy"), Display("IsHardCopy")]
        public Int32 IsHardCopy { get; set; }
        [Database("hardcopy_updated_date"), Display("HardcopyUpdatedDate")]
        public DateTime? HardcopyUpdatedDate { get; set; }

        [Database("activation_date"), Display("ActivationDate")]
        public DateTime? ActivationDate { get; set; }

        [Database("email_code"), Display("EmailCode")]
        public String EmailCode { get; set; }
        [Database("is_agent"), Display("IsAgent")]
        public Int32 IsAgent { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("agent_status"), Display("AgentStatus")]
        public Int32 AgentStatus { get; set; }
        [Join("id")]
        public User ModifiedByUser { get; set; }
        [Join("id")]
        public User CreatedByUser { get; set; }
        [Join("id")]
        public UserRole UserRoleIdUserRole { get; set; }
        [Join("user_id")]
        public List<UserType> UserIdUserTypes { get; set; }
        [Join("user_id")]
        public List<UserStatement> UserIdUserStatements { get; set; }
        [Join("user_id")]
        public List<UserSecurity> UserIdUserSecurities { get; set; }
        [Join("user_id")]
        public List<UserNotificationSetting> UserIdUserNotificationSettings { get; set; }
        [Join("user_id")]
        public List<UserFile> UserIdUserFiles { get; set; }
        [Join("modified_by")]
        public List<UserDetail> ModifiedByUserDetails { get; set; }
        [Join("created_by")]
        public List<UserDetail> CreatedByUserDetails { get; set; }
        [Join("user_id")]
        public List<UserDetail> UserIdUserDetails { get; set; }
        [Join("modified_by")]
        public List<UserAccount> ModifiedByUserAccounts { get; set; }
        [Join("created_by")]
        public List<UserAccount> CreatedByUserAccounts { get; set; }
        [Join("user_id")]
        public List<UserAccount> UserIdUserAccounts { get; set; }
        [Join("user_id")]
        public List<UserCorpAccount> UserIdUserCorpAccounts { get; set; }
        [Join("user_id")]
        public List<DvDistributionIntruction> UserIdDvDistributionIntructions { get; set; }

        public string AgentRank { get; set; }

    }

    [Database("users")]
    public class CustomStaffUser
    {
        [Database("id"), Display("Id"), Required]
        public Int32 Id { get; set; }
        [Database("username"), Display("Username"), Required, LengthOrValue(3, 18)]
        public String Username { get; set; }
        [Database("email_id"), Display("EmailId"), Required, Email]
        public String EmailId { get; set; }
        [Database("mobile_number"), Display("MobileNumber")]
        public String MobileNumber { get; set; }
        [Database("id_no"), Display("IdNo")]
        public String IdNo { get; set; }
        [Database("password"), Display("Password")]
        public String Password { get; set; }
        [Database("trans_pwd"), Display("TransPwd")]
        public String TransPwd { get; set; }
        [Database("unique_key"), Display("UniqueKey")]
        public String UniqueKey { get; set; }
        [Database("user_role_id"), Display("UserRoleId")]
        public Int32 UserRoleId { get; set; }
        [Database("last_login_date"), Display("LastLoginDate")]
        public DateTime? LastLoginDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("modified_by"), Display("ModifiedBy")]
        public Int32? ModifiedBy { get; set; }
        [Database("modified_date"), Display("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [Database("is_online"), Display("IsOnline")]
        public Int32 IsOnline { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("is_primary"), Display("IsPrimary")]
        public Int32 IsPrimary { get; set; }
        [Database("is_security_checked"), Display("IsSecurityChecked")]
        public Int32 IsSecurityChecked { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("last_login_ip"), Display("LastLoginIp")]
        public String LastLoginIp { get; set; }
        [Database("register_ip"), Display("RegisterIp")]
        public String RegisterIp { get; set; }
        [Database("sat_updated_date"), Display("SatUpdatedDate")]
        public DateTime? SatUpdatedDate { get; set; }
        [Database("is_sat_checked"), Display("IsSatChecked")]
        public Int32? IsSatChecked { get; set; }
        [Database("password_reset_code"), Display("PasswordResetCode")]
        public String PasswordResetCode { get; set; }
        [Database("reset_expired"), Display("ResetExpired")]
        public Int32 ResetExpired { get; set; }
        [Database("verification_code"), Display("VerificationCode")]
        public Int32? VerificationCode { get; set; }
        [Database("verify_expired"), Display("VerifyExpired")]
        public Int32 VerifyExpired { get; set; }
        [Database("login_attempts"), Display("LoginAttempts")]
        public Int32 LoginAttempts { get; set; }
        [Database("sms_attempts"), Display("SmsAttempts")]
        public Int32 SmsAttempts { get; set; }
        [Database("google_auth_attempts"), Display("GoogleAuthAttempts")]
        public Int32 GoogleAuthAttempts { get; set; }
        [Database("is_login_locked"), Display("IsLoginLocked")]
        public Int32 IsLoginLocked { get; set; }
        [Database("is_sms_locked"), Display("IsSmsLocked")]
        public Int32 IsSmsLocked { get; set; }
        [Database("is_hard_copy"), Display("IsHardCopy")]
        public Int32 IsHardCopy { get; set; }
        [Database("hardcopy_updated_date"), Display("HardcopyUpdatedDate")]
        public DateTime? HardcopyUpdatedDate { get; set; }

        [Database("activation_date"), Display("ActivationDate")]
        public DateTime? ActivationDate { get; set; }

        [Database("email_code"), Display("EmailCode")]
        public String EmailCode { get; set; }
        [Database("is_agent"), Display("IsAgent")]
        public Int32 IsAgent { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("agent_status"), Display("AgentStatus")]
        public Int32 AgentStatus { get; set; }
        [Join("id")]
        public User ModifiedByUser { get; set; }
        [Join("id")]
        public User CreatedByUser { get; set; }
        [Join("id")]
        public UserRole UserRoleIdUserRole { get; set; }
        [Join("user_id")]
        public List<UserType> UserIdUserTypes { get; set; }
        [Join("user_id")]
        public List<UserStatement> UserIdUserStatements { get; set; }
        [Join("user_id")]
        public List<UserSecurity> UserIdUserSecurities { get; set; }
        [Join("user_id")]
        public List<UserNotificationSetting> UserIdUserNotificationSettings { get; set; }
        [Join("user_id")]
        public List<UserFile> UserIdUserFiles { get; set; }
        [Join("modified_by")]
        public List<UserDetail> ModifiedByUserDetails { get; set; }
        [Join("created_by")]
        public List<UserDetail> CreatedByUserDetails { get; set; }
        [Join("user_id")]
        public List<UserDetail> UserIdUserDetails { get; set; }
        [Join("modified_by")]
        public List<UserAccount> ModifiedByUserAccounts { get; set; }
        [Join("created_by")]
        public List<UserAccount> CreatedByUserAccounts { get; set; }
        [Join("user_id")]
        public List<UserAccount> UserIdUserAccounts { get; set; }
        [Join("user_id")]
        public List<UserCorpAccount> UserIdUserCorpAccounts { get; set; }
        [Join("user_id")]
        public List<DvDistributionIntruction> UserIdDvDistributionIntructions { get; set; }
        public Int32 DepartmentType { get; set; }
        public Int32 Maker { get; set; }
        public Int32 Checker { get; set; }
        public Int32 ViewReport { get; set; }
        public Int32 GenerateReport { get; set; }

    }
}
