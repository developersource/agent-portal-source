﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddStatementLinks : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderStatementService> lazyUserStatementServiceObj = new Lazy<IUserOrderStatementService>(() => new UserOrderStatementService());

        public static IUserOrderStatementService IUserOrderStatementService { get { return lazyUserStatementServiceObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
                CreatedBy.Value = "0";
                UpdatedBy.Value = "0";
                CreatedDate.Value = currentDateString;
                UpdatedDate.Value = currentDateString;


                string idString = Request.QueryString["id"];
                string orderIdString = Request.QueryString["orderId"];
                string userOrderStatementTypeId = Request.QueryString["UserOrderStatementTypeId"];
                if (idString != null && idString != "")
                {
                    int id = Convert.ToInt32(idString);
                    if (id == 0)
                    {
                        int orderId = Convert.ToInt32(orderIdString);
                        Response response = IUserOrderService.GetSingle(orderId);
                        if (response.IsSuccess)
                        {
                            UserOrder obj = (UserOrder)response.Data;
                            Name.Value = userOrderStatementTypeId == "1" ? "Tax invoice" : (userOrderStatementTypeId == "2" ? "Confirmation advice slip" : (userOrderStatementTypeId == "3" ? "Credit note" : ""));
                            RefNo.Value = obj.RefNo;
                            UserOrderStatementTypeId.Value = userOrderStatementTypeId;
                            UserId.Value = obj.UserId.ToString();
                            UserAccountId.Value = obj.UserAccountId.ToString();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + response.Message + "');", true);
                        }
                    }
                    else
                    {
                        Response response = IUserOrderStatementService.GetSingle(id);
                        if (response.IsSuccess)
                        {
                            UserOrderStatement obj = (UserOrderStatement)response.Data;
                            Id.Value = obj.Id.ToString();

                            Name.Value = obj.Name;
                            Url.Value = obj.Url;
                            RefNo.Value = obj.RefNo;
                            UserOrderStatementTypeId.Value = obj.UserOrderStatementTypeId.ToString();
                            UserId.Value = obj.UserId.ToString();
                            UserAccountId.Value = obj.UserAccountId.ToString();

                            Status.Value = obj.Status.ToString();
                            CreatedBy.Value = obj.CreatedBy.ToString();
                            CreatedDate.Value = obj.CreatedDate.ToString("MM/dd/yyyy");
                            UpdatedBy.Value = obj.UpdatedBy.ToString();
                            UpdatedDate.Value = obj.UpdatedDate == null ? DateTime.Now.ToString("MM/dd/yyyy") : obj.UpdatedDate.Value.ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + response.Message + "');", true);
                        }
                    }
                }
            }
        }
    }
}