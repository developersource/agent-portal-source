﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCommStructureMapping.aspx.cs" Inherits="Admin.AddCommStructureMapping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addCommStructureMapForm" runat="server">
        <div class="row" id="FormId" data-value="addCommStructureMapForm">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Fund:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                        <asp:DropDownList  ID="ddlFundList" runat="server" clientIdMode="Static" cssClass="form-control">
                            <asp:ListItem Value="">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Apply To Structure:</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList  ID="ddlCommSettings" runat="server" clientIdMode="Static" cssClass="form-control">
                            <asp:ListItem Value="">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
