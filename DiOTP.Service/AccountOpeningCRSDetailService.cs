﻿using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class AccountOpeningCRSDetailService : IAccountOpeningCRSDetailService
    {
        private static readonly Lazy<IAccountOpeningCRSDetailRepo> lazy = new Lazy<IAccountOpeningCRSDetailRepo>(() => new AccountOpeningCRSDetailRepo());
        public static IAccountOpeningCRSDetailRepo IAccountOpeningCRSDetailRepo { get { return lazy.Value; } }
        public Response PostData(AccountOpeningCRSDetail obj)
        {
            Response response = new Response();
            try
            {
                response = IAccountOpeningCRSDetailRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<AccountOpeningCRSDetail> objs)
        {
            Int32 result = 0;
            try
            {
                result = IAccountOpeningCRSDetailRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(AccountOpeningCRSDetail obj)
        {
            Response response = new Response();
            try
            {
                response = IAccountOpeningCRSDetailRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<AccountOpeningCRSDetail> objs)
        {
            Int32 result = 0;
            try
            {
                result = IAccountOpeningCRSDetailRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public AccountOpeningCRSDetail DeleteData(Int32 Id)
        {
            AccountOpeningCRSDetail obj = new AccountOpeningCRSDetail();
            try
            {
                obj = IAccountOpeningCRSDetailRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IAccountOpeningCRSDetailRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            AccountOpening obj = new AccountOpening();
            try
            {
                response = IAccountOpeningCRSDetailRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningCRSDetail> objs = new List<AccountOpeningCRSDetail>();
            try
            {
                response = IAccountOpeningCRSDetailRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IAccountOpeningCRSDetailRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningCRSDetail> objs = new List<AccountOpeningCRSDetail>();
            try
            {
                response = IAccountOpeningCRSDetailRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IAccountOpeningCRSDetailRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningCRSDetail> objs = new List<AccountOpeningCRSDetail>();
            try
            {
                response = IAccountOpeningCRSDetailRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IAccountOpeningCRSDetailRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
