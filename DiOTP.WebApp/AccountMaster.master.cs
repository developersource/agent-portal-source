﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace DiOTP.WebApp
{
    public partial class AccountMaster : System.Web.UI.MasterPage
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null && Session["isVerified"] != null)
                {
                    if (Session["isVerified"].ToString() == "1")
                    {
                        User user = (User)(Session["user"]);

                        Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                        if (IsImpersonate)
                        {
                            buyLink.Attributes.Remove("href");
                            sellLink.Attributes.Remove("href");
                            switchLink.Attributes.Remove("href");
                            transferLink.Attributes.Remove("href");
                            coolingOffLink.Attributes.Remove("href");
                            rspLink.Attributes.Remove("href");

                            buyLink.Visible = false;
                            buyLink.Attributes.Add("class", "no-link");
                            buyLink.Attributes.Add("data-message", "Impersonated cannot do transactions");
                            sellLink.Visible = false;
                            sellLink.Attributes.Add("class", "no-link");
                            sellLink.Attributes.Add("data-message", "Impersonated cannot do transactions");
                            switchLink.Visible = false;
                            switchLink.Attributes.Add("class", "no-link");
                            switchLink.Attributes.Add("data-message", "Impersonated cannot do transactions");
                            transferLink.Visible = false;
                            transferLink.Attributes.Add("class", "no-link");
                            transferLink.Attributes.Add("data-message", "Impersonated cannot do transactions");
                            coolingOffLink.Visible = false;
                            coolingOffLink.Attributes.Add("class", "no-link");
                            coolingOffLink.Attributes.Add("data-message", "Impersonated cannot do transactions");
                            coolingOffLink.Attributes.Add("class", "no-link");
                            coolingOffLink.Attributes.Add("data-message", "Impersonated cannot do transactions");
                            rspLink.Visible = false;
                            rspLink.Attributes.Add("class", "no-link");
                            rspLink.Attributes.Add("data-message", "Impersonated cannot do transactions");

                            buyLink2.Visible = false;
                            sellLink2.Visible = false;
                            switchLink2.Visible = false;
                            transferLink2.Visible = false;
                            coolingOffLink2.Visible = false;
                            rspLink2.Visible = false;
                        }

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            UserAccount primaryAcc = new UserAccount();
                            
                            List<UserAccount> userAccountsSorted = new List<UserAccount>();

                            List<UserAccount> userAccountsCORP = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CORP").ToList();
                            if (userAccountsCORP.Count > 0)
                                userAccountsSorted.AddRange(userAccountsCORP);
                            List<UserAccount> userAccountsCASH = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
                            if (userAccountsCASH.Count > 0)
                                userAccountsSorted.AddRange(userAccountsCASH);
                            List<UserAccount> userAccountsJOINT = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                            if (userAccountsJOINT.Count > 0)
                                userAccountsSorted.AddRange(userAccountsJOINT);
                            List<UserAccount> userAccountsEPF = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                            if (userAccountsEPF.Count > 0)
                                userAccountsSorted.AddRange(userAccountsEPF);

                            userAccounts = userAccountsSorted;

                            primaryAcc = userAccounts.FirstOrDefault();

                            if (primaryAcc.IsPrinciple == 0 && !IsImpersonate)
                            {
                                buyLink.Attributes.Remove("href");
                                sellLink.Attributes.Remove("href");
                                switchLink.Attributes.Remove("href");
                                transferLink.Attributes.Remove("href");
                                coolingOffLink.Attributes.Remove("href");
                                rspLink.Attributes.Remove("href");


                                buyLink.Attributes.Add("class", "no-link");
                                buyLink.Attributes.Add("class", "hide");
                                buyLink.Visible = false;
                                buyLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                sellLink.Visible = false;
                                sellLink.Attributes.Add("class", "no-link");
                                sellLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                switchLink.Visible = false;
                                switchLink.Attributes.Add("class", "no-link");
                                switchLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                transferLink.Visible = false;
                                transferLink.Attributes.Add("class", "no-link");
                                transferLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                coolingOffLink.Visible = false;
                                coolingOffLink.Attributes.Add("class", "no-link");
                                coolingOffLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                coolingOffLink.Attributes.Add("class", "no-link");
                                coolingOffLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                rspLink.Visible = false;
                                rspLink.Attributes.Add("class", "no-link");
                                rspLink.Attributes.Add("data-message", "Secondary holder cannot do transactions");
                                orderListLink.Visible = false;

                                buyLink2.Visible = false;
                                sellLink2.Visible = false;
                                switchLink2.Visible = false;
                                transferLink2.Visible = false;
                                coolingOffLink2.Visible = false;
                                rspLink2.Visible = false;
                                orderListLink2.Visible = false;
                            }
                            else if (CustomValues.GetAccounPlan(primaryAcc.HolderClass) == "CORP")
                            {
                                buyLink.Attributes.Remove("href");
                                sellLink.Attributes.Remove("href");
                                switchLink.Attributes.Remove("href");
                                transferLink.Attributes.Remove("href");
                                coolingOffLink.Attributes.Remove("href");
                                rspLink.Attributes.Remove("href");

                                buyLink.Visible = false;
                                buyLink.Attributes.Add("class", "no-link");
                                buyLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                sellLink.Visible = false;
                                sellLink.Attributes.Add("class", "no-link");
                                sellLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                switchLink.Visible = false;
                                switchLink.Attributes.Add("class", "no-link");
                                switchLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                transferLink.Visible = false;
                                transferLink.Attributes.Add("class", "no-link");
                                transferLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                coolingOffLink.Visible = false;
                                coolingOffLink.Attributes.Add("class", "no-link");
                                coolingOffLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                coolingOffLink.Attributes.Add("class", "no-link");
                                coolingOffLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                rspLink.Visible = false;
                                rspLink.Attributes.Add("class", "no-link");
                                rspLink.Attributes.Add("data-message", "Corporate account cannot do transactions");
                                rsp.Visible = false;
                                orderListLink.Visible = false;

                                buyLink2.Visible = false;
                                sellLink2.Visible = false;
                                switchLink2.Visible = false;
                                transferLink2.Visible = false;
                                coolingOffLink2.Visible = false;
                                rspLink2.Visible = false;
                                orderListLink2.Visible = false;
                            }


                            if (primaryAcc != null)
                            {
                                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                                MaHolderReg maHolderReg = new MaHolderReg();
                                if (responseMHR.IsSuccess)
                                {
                                    maHolderReg = (MaHolderReg)responseMHR.Data;
                                    string name = maHolderReg.Name1;
                                    string accountPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                                    if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT")
                                        name = maHolderReg.Name1 + "<div class='aAccountName-joint'>(" + maHolderReg.Name2 + ")</div>";
                                    if (primaryAcc.IsPrinciple == 0)
                                        name = maHolderReg.Name2 + "<div class='aAccountName-joint'>(" + maHolderReg.Name1 + ")</div>";
                                    aAccountName.InnerHtml = name;
                                    smallAccountPlan.InnerHtml = accountPlan;
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid user account', '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }

                        agentApplicationStatus.Visible = false;
                        string query = (@" select * from agent_signups where process_status=1 and user_id='" + user.Id + "' and status=1 ");
                        Response responseList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);
                        if (responseList.IsSuccess)
                        {
                            var UsersDyn = responseList.Data;
                            var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                            List<AgentSignup> aoRequestList = JsonConvert.DeserializeObject<List<AgentSignup>>(responseJSON);
                            if (aoRequestList.Count == 1)
                            {
                                agentApplicationStatus.Visible = true;
                            }
                        }
                        //To get agent process status for allowing agent trainee to access agent portal for training materials.
                        bool isRecommended = false;
                        string queryCheckAgent = (@"Select * from agent_regs where user_id ='" +user.Id+"'");
                        Response responseCheckAgent = GenericService.GetDataByQuery(queryCheckAgent, 0, 0, false, null, false, null, true);
                        if (responseCheckAgent.IsSuccess) {
                            var checkAgentDyn = responseCheckAgent.Data;
                            var responseCheckAgentJSON = JsonConvert.SerializeObject(checkAgentDyn);
                            List<AgentReg> checkAgentList = JsonConvert.DeserializeObject<List<AgentReg>>(responseCheckAgentJSON);
                            AgentReg singleAgent = checkAgentList.FirstOrDefault();

                            //If process status = 2. Agent has been recommended
                            if (singleAgent != null) {
                                isRecommended = singleAgent.process_status == 2 ? true : false;
                            }
                            
                        }

                        if (user.IsAgent == 0 && isRecommended == true)
                        {
                            agentPortalLink.Visible = false;
                            agentTraineeLink.Visible = true;
                        }
                        else if(user.IsAgent == 1)
                        {                            
                            agentPortalLink.Visible = true;
                        }
                        else {
                            agentPortalLink.Visible = false;
                            agentTraineeLink.Visible = false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.WriteLog("AccountMaster Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void lnkLogout3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Logout.aspx", false);
        }


    }
}

