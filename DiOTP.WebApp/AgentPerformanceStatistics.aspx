﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AgentMaster.master" CodeBehind="AgentPerformanceStatistics.aspx.cs" Inherits="DiOTP.WebApp.AgentPerformanceStatistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Performance</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Performance Statistics</h3>
            </div>
            <div id="containerPage">
                <style>
                    .transType li {
                        display:inline-block;
                        padding:20px;
                        border: 1px solid black;
                        cursor:pointer;
                    }
                    .btn-chkbox {
                       opacity:0;
                    }

                    .contentHover:hover {
                        background-color:lightgray;
                    }

                    .salesTD {
                        width:250px;
                    }
                </style>

                <%--Segment 1--%>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="text-center"><i>*Note: This chart is for individual agent use, It does not reflect group and total group details.*</i></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-head">
                            
                            </div>
                        <div class="col-md-3" style="padding-top:100px;">
                            <table>
                                <tbody>
                                    <tr class="btn-border contentHover">
                                        <td class="salesTD text-center">
                                            <input type="checkbox" class="btn-chkbox" id="btnSales" value="SA" checked="checked"/>
                                            <label class="btn" for="btnSales">Sales</label><br/>
                                        </td>
                                    </tr>
                                    <tr class="btn-border contentHover">
                                        <td class="redemptionTD text-center">
                                            <input type="checkbox" class="btn-chkbox" id="btnRedemption" value="RD"/>
                                            <label class="btn" for="btnRedemption">Redemption</label><br/>
                                        </td>
                                    </tr>
<%--                                    <tr class="btn-border contentHover">
                                        <td class="switchTD text-center">
                                            <input type="checkbox" class="btn-chkbox" id="btnSwitch"  value="SW"/>
                                            <label class="btn" for="btnSwitch">Switch</label><br/>
                                        </td>
                                    </tr>--%>
                                    <tr class="btn-border contentHover">
                                        <td class="commissionTD text-center">
                                            <input type="checkbox" class="btn-chkbox" id="btnCommission"  value="COM"/>
                                            <label class="btn" for="btnCommission">Commission</label><br/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <ul class="nav nav-tabs chart-tabs d-ib">
                                        <!-- chart-tabs: Used in script -->
                                        <li class="active">
                                            <a  href="#" data-toggle="tab" data-value="1">Current Year</a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="tab"data-value="2">Last Year</a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="tab" data-value="3">Last 2 Years</a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="tab" data-value="4">Last 3 Years</a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="tab" data-value="5">Last 4 Years</a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="tab" data-value="6">Last 5 Years</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="loadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                                <%--<i class="fa fa-spinner fa-spin"></i>--%>
                                            </div>
                                    <%--Chart Generation--%>
                                    <figure class="highcharts-figure">
                                        <div id="chartContainer"></div>

                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>

        <%--<script src="Content/mycj/common-scripts.js?v=1.0"></script>--%>
        <script src="/Content/js/highcharts.js"></script>
        <script src="Content/assets/js/jquery.min.js"></script>

        <script>
            var chart;
            var selectedTransType = new Array();
            var isSalesCheckedVal;
            var isRedemptionCheckedVal;
            //var isSwitchCheckedVal;
            var isCommissionCheckedVal;
            var selectedPeriod;
            var isChartUpdated = 1;
            var transactionObject = new Array();

            $(document).ready(function () {
                $('.loadingChartDiv').fadeOut();
                //preload sales button data as default
                if ($('#btnSales').prop('checked') == true) {
                        $('.salesTD').css("background-color", "lightskyblue");
                        isSalesCheckedVal = $('#btnSales').val();
                        selectedTransType.push(isSalesCheckedVal);
                }
                //preload period to current year as default.
                $("ul li:first").addClass('active');

                 $('.chart-tabs a').click(function () {
                selectedPeriod = $(this).attr('data-value');
                isChartUpdated = 1;
            });

                function PlotChart() {
                    //load ajax data only if transaction type is selected and period is selected
                    if (selectedTransType.length != 0 && selectedPeriod != null) {
                        var YTDCDATA;
                        var colorStrings = ['blue', 'green','black'];

                        $.ajax({
                            url: "AgentPerformanceStatistics.aspx/GetComparisonSeries",
                            async: false,
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            data: {
                                'selectedTransType': JSON.stringify(selectedTransType), 'selectedPeriod': JSON.stringify(selectedPeriod)
                            },
                            success: function (data) {
                                var json = data.d;
                                if (isChartUpdated == 1) {
                                    while (chart.series.length > 0) chart.series[0].remove(true);
                                    transactionObject = new Array();
                                            isChartUpdated = 0;
                                            chart.xAxis[0].setCategories(json.Data[0].Labels);
                                        }
                                transactionObject.push(json.Data[0]);


                                //YTDCDATA = json;
                                //while (chart.series.length > 0) chart.series[0].remove(true);
                                //chart.xAxis[0].visible = true;
                                //chart.xAxis[0].setCategories(theMonths);
                                var html = "";
                                $.each(json, function (idx, y) {
                                    var series = [];
                                    for (i = 0; i < 12; i++) {
                                        var rec = $.grep(y.getComparisonStatByYear.statByMonth, function (obj, index) {
                                            return theMonths[i] == obj.Month;
                                        });
                                        if (rec.length == 1)
                                            series.push(parseInt(rec[0].commVal));
                                        else
                                            series.push(0);
                                    }
                                    chart.addSeries({
                                        className: "highcharts-color-" + colorStrings[idx],
                                        data: series,
                                        name: y.transName,
                                        useHTML: true
                                    });
                                });
                            }
                        });
                    }

                }

                $('input[type=checkbox]').click(function () {
                    //For indication of selected transaction types
                    if ($(this).prop('checked') == true) {
                        if ($(this).is('#btnSales')) {
                            $('.salesTD').css("background-color", "lightskyblue");
                            isSalesCheckedVal = $(this).val();
                            selectedTransType.push(isSalesCheckedVal);
                            $('#btnSales').attr('checked',true);
                        }
                        if ($(this).is('#btnRedemption')) {
                            $('.redemptionTD').css("background-color", "lightskyblue");
                            isRedemptionCheckedVal = $(this).val();
                            selectedTransType.push(isRedemptionCheckedVal);
                            $('#btnRedemption').attr('checked',true);
                        }
                        //if ($(this).is('#btnSwitch')) {
                        //    $('.switchTD').css("background-color", "lightskyblue");
                        //    isSwitchCheckedVal = $(this).val();
                        //    selectedTransType.push(isSwitchCheckedVal);
                        //}
                        if ($(this).is('#btnCommission')) {
                            $('.commissionTD').css("background-color", "lightskyblue");
                            isCommissionCheckedVal = $(this).val();
                            selectedTransType.push(isCommissionCheckedVal);
                            $('#btnCommission').attr('checked',true);
                        }
                    
                    }
                    else {
                        if ($(this).is('#btnSales')) {
                            $('.salesTD').css("background-color", "#FFFFFF");
                            updateArray(isSalesCheckedVal);
                            isSalesCheckedVal = '';
                            $('#btnSales').attr('checked',false);
                        }
                        if ($(this).is('#btnRedemption')) {
                            $('.redemptionTD').css("background-color", "#FFFFFF");
                            updateArray(isRedemptionCheckedVal);
                            isRedemptionCheckedVal = '';
                            $('#btnRedemption').attr('checked',false);
                        }
                        //if ($(this).is('#btnSwitch')) {
                        //    $('.switchTD').css("background-color", "#FFFFFF");
                        //    updateArray(isSwitchCheckedVal);
                        //    isSwitchCheckedVal = '';
                        //}
                        if ($(this).is('#btnCommission')) {
                            $('.commissionTD').css("background-color", "#FFFFFF");
                            updateArray(isCommissionCheckedVal);
                            isCommissionCheckedVal = '';
                             $('#btnCommission').attr('checked',false);
                        }
                    }
                });

                function updateArray(transType) {
                    if (selectedTransType.length > 0)
                    {
                        var index = selectedTransType.indexOf(transType);
                        if (index > -1) {
                          selectedTransType.splice(index, 1);
                        }
                    }
                }

                Highcharts.chart('chartContainer', {

                    title: {
                        text: 'Based On Individual Performance'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Value (MYR)'
                        }
                    },

                    xAxis: {
                        accessibility: {
                            rangeDescription: undefined
                        },
                        labels: {
                        rotation: -45,
                        step: 30,
                        showLastLabel: true,
                        endOnTick: true,
                        x: -10,
                        formatter: function () {
                            var x = this.value;
                            var transDetails = transactionObject[0];
                            //single object -> 

                            //to get years length and month length of object
                            var xLabelYears = transDetails.getComparisonStatByYear.length;
                            var xLabelMonths = transDetails.getComparisonStatByYear.statByMonth.length;

                            var theMonths = transDetails.getComparisonStatByYear.statByMonth[0].Month;
                            var theYear = transDetails.getComparisonStatByYear.Year;
                            var theDate = theYear + '-' + theMonths + '-' + '01';
                            //    $.unique($.map(transDetails.getComparisonStatByYear, function (obj) {
                            //    return (new Date(obj).getFullYear());
                            //}))

                            //1 = current year
                            if (xLabelYears == 1) {
                                if (xLabelMonths == 12) {
                                    console.log(xLabelMonths);
                                    chart.xAxis[0].options.labels.step = 30;
                                    return Highcharts.dateFormat('%b %Y', new Date(theDate));
                                }
                            }
                            //2 = previous year
                            else if (xLabelYears == 2) {

                                if (xLabelMonths.length == 12) {
                                    console.log(xLabelMonths.length);
                                    chart.xAxis[0].options.labels.step = 30;
                                    return Highcharts.dateFormat('%b %Y', new Date(theDate));
                                }
                            }
                            //3 = Past 2 years
                            else if (xLabelYears == 3) {
                                chart.xAxis[0].options.labels.step = 58;
                                return Highcharts.dateFormat('%b %Y', new Date(theDate));
                            }
                            //4 = Past 3 years
                            else if (xLabelYears == 4) {
                                chart.xAxis[0].options.labels.step = 1;
                                return Highcharts.dateFormat('%b %Y', new Date(theDate));
                            }
                            //5 = Past 4 years
                            else if (xLabelYears == 5) {
                                chart.xAxis[0].options.labels.step = 4;
                                return Highcharts.dateFormat('%b %Y', new Date(theDate));
                            }
                            //6 = Past 5 years
                            else if (xLabelYears == 6) {
                                chart.xAxis[0].options.labels.step = 4;
                                return Highcharts.dateFormat('%b %Y', new Date(theDate));
                            }
                            else {
                                chart.xAxis[0].options.labels.step = 6;
                                return Highcharts.dateFormat('%b %Y', new Date(theDate));
                            }


                            //var labelIndex = jQuery.inArray(($.grep(transDetails.Labels, function (obj) {
                            //    return obj === x;
                            //})[0]), transDetails.Labels);
                            //var navDate = transDetails.NavDates[labelIndex];
                            //var xLabelYears = $.unique($.map(transDetails.NavDates, function (obj) {
                            //    return (new Date(obj).getFullYear());
                            //}));
                            
                            //if (xLabelYears.length == 1) {
                            //    var xLabelMonths = $.unique($.map(transDetails.NavDates, function (obj) {
                            //        return (new Date(obj).getMonth() + 1);
                            //    }));
                            //    if (xLabelMonths.length == 2) {
                            //        chart.xAxis[0].options.labels.step = 3;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //    else if (xLabelMonths.length == 1) {
                            //        chart.xAxis[0].options.labels.step = 1;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //    else if (xLabelMonths.length == 4) {
                            //        chart.xAxis[0].options.labels.step = 10;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //    else if (xLabelMonths.length == 12) {
                            //        console.log(xLabelMonths.length);
                            //        chart.xAxis[0].options.labels.step = 30;
                            //        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //    }
                            //    else {
                            //        chart.xAxis[0].options.labels.step = 14;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //}
                            //else if (xLabelYears.length == 2) {
                            //    var xLabelMonths = $.unique($.map(transDetails.NavDates, function (obj) {
                            //        return (new Date(obj).getMonth() + 1);
                            //    }));
                            //    if (xLabelMonths.length == 1) {
                            //        chart.xAxis[0].options.labels.step = 1;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //    else if (xLabelMonths.length == 2) {
                            //        chart.xAxis[0].options.labels.step = 3;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //    else if (xLabelMonths.length == 4) {
                            //        chart.xAxis[0].options.labels.step = 10;
                            //        return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                            //    }
                            //    else if (xLabelMonths.length == 12) {
                            //        console.log(xLabelMonths.length);
                            //        chart.xAxis[0].options.labels.step = 30;
                            //        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //    }
                            //    else {
                            //        chart.xAxis[0].options.labels.step = 29;
                            //        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //    }
                            //}
                            //else if (xLabelYears.length == 3) {
                            //    chart.xAxis[0].options.labels.step = 58;
                            //    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //}
                            //else if (xLabelYears.length == 4) {
                            //    chart.xAxis[0].options.labels.step = 1;
                            //    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //}
                            //else if (xLabelYears.length == 6) {
                            //    chart.xAxis[0].options.labels.step = 4;
                            //    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //}
                            //else {
                            //    chart.xAxis[0].options.labels.step = 6;
                            //    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            //}
                            //Highcharts.dateFormat('%b %Y', new Date(this.value))
                        },
                        style: {
                            fontSize: '10px'
                        },
                        padding: 2,
                        distance: 5
                    },
                        categories: [],
                        crosshair: true
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },

                    tooltip: {
                        pointFormat: '{series.name} has value of <b>{point.y:,.0f}</b><br /> in { point.x }'
                    },

                    //series: [{
                    //    name: 'Installation',
                    //    data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                    //}//, {
                    ////    name: 'Manufacturing',
                    ////    data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                    ////}, {
                    ////    name: 'Sales & Distribution',
                    ////    data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                    ////}, {
                    ////    name: 'Project Development',
                    ////    data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                    ////}, {
                    ////    name: 'Other',
                    ////    data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
                    //    //}
                    //],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            });

           
             //$('.contentHover').mouseover(function () {
             //   $('.contentHover').css("background-color", "lightgray");
             //   console.log('Hovered');
             //});
        </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>