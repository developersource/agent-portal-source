﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="DiOTP.WebApp.PrivacyPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Privacy Policy</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Privacy Policy</h3>
                    </div>

                    <p class="text-justify">Apex Investment Services Berhad (AISB) (“the Company”, “our”, “us”, or “we”) is a holder of a Capital Markets Services Licence issued under the Capital Market and Services Act 2007 permitting the Company to carry out regulated activities of fund management and dealings in securities (restricted to unit trust).</p>
                    <h4 class="text-justify">THIS MEANS, MOST IMPORTANTLY, THAT WE DO NOT SELL CLIENT INFORMATION-WHETHER IT IS YOUR PERSONAL INFORMATION OR THE FACT THAT YOU ARE AN APEX INVESTMENT SERVICES BERHAD (AISB) CLIENT-TO ANYONE.</h4>
                    <p class="text-justify">In the course of the Company carrying out the aforesaid permitted activities, the Company may collect, record, hold, store or process your Personal Data (as defined in the Personal Data Protection Act 2010).  We have always (and will continue to do so) respected the privacy and confidentiality of all the personal information we have received and collected in the course of the provision of our services to and/or our dealings with you and taken all reasonable steps to ensure the proper safeguard of such information.</p>
                    <h4>YOUR CONSENT</h4>
                    <p>By proceeding to access and use this portal, you are deemed to have consented to the collection and use of your personal information including for the disclosure of such personal information to the relevant entities and/or regulatory bodies.</p>
                    <h4>SAFEGUARDING OF YOUR PERSONAL DATA</h4>
                    <p>Please take note that your Personal Data may be stored or processed to or in locations or systems in jurisdictions outside Malaysia (where necessary to facilitate the provision of our services and products to you) subject to those jurisdictions having similar data protection laws in place and/or our securing reciprocal confidentiality undertakings.</p>
                    <p>Please be assured that we will take all necessary practical steps including but not limited to incorporating reasonable security measures into any equipment in which your Personal Data is stored, to protect your Personal Data from any loss, misuse or unauthorised access or disclosure.</p>
                    <h4>DESCRIPTION OF THE PERSONAL DATA WE COLLECT AND PROCESS</h4>
                    <p>You provide personal information when you complete an AISB account application.  (If you enter information in an online application, we may store the information even if you don't complete or submit the application.).  You also required providing personal information when you request a transaction that involves AISB or one of the AISB affiliated companies.</p>
                    <p>In addition to personal information you provide to us, we may receive information about you that you have authorised third parties to provide to us.  We also may obtain personal information from third parties in order for us to verify your identity, prevent fraud, and to help us identify products and services that may benefit you.</p>
                    <p>Personal information collected from any source may include your:</p>
                    <ul>
                        <li>Name and address</li>
                        <li>Identification Card Number/Passport Number</li>
                        <li>Gender</li>
                        <li>Contact numbers</li>
                        <li>Email address</li>
                        <li>Employment</li>
                        <li>Financial Information</li>
                        <li>Information on your spouse or immediate relatives</li>
                        <li>Any other information that may be required by the regulatory such as the Securities Commission of Malaysia.</li>
                    </ul><br />
                    <p>We will not disclose any of your personal information except where required by regulatory bodies, regulation of legal process, judicial order or as required or permitted by law or for lawful purposes only.  Further, we may use your personal information within AISB, professional advisors, consultants and/or third party's performing services on our behalf and such confidential information will only be used by the said parties for the purpose for which it is provided whereby the said parties have undertaken to treat such information confidential.</p>
                    <h4>HOW DO WE COLLECT YOUR DATA?</h4>
                    <p>Your data are collected through any of the following methods:</p>
                    <ul>
                        <li>Personal data which are directly provided by you by way of application forms or agreements;</li>
                        <li>Personal data may be obtained by AISB from publicly available sources;</li>
                        <li>Personal data related to your transactions with us or our affiliates;</li>
                        <li>Information that you have provided to us on our websites;</li>
                        <li>From third parties such as credit reference agencies or from any other sources as AISB considers appropriate;</li>
                        <li>Any other sources which you have given your consent to disclose information relating to you and/or where not otherwise restricted.</li>
                    </ul><br />
                    <h4>PURPOSE OF COLLECTING PERSONAL DATA</h4>
                    <p>The personal data provided by you shall be used in the ordinary course of our business which may include the following purposes:</p>
                    <ul>
                        <li>The processing of your application for our facilities, products and services, including identification verification;</li>
                        <li>Assessing or verifying your credit worthiness (i.e.: ongoing credit worthiness) and/or facilitate your dealings in respect of credit facilities;</li>
                        <li>To communicate or respond to your enquiries and resolving any services issues or complaints;</li>
                        <li>To provide you with information on products and services offered by us and/or our business partners;</li>
                        <li>For purposes of cross selling, marketing and promotions with our strategic partners;</li>
                        <li>To comply with regulatory requirements and provide assistance to law enforcement agencies;</li>
                        <li>For enforcement of our rights and obligations;</li>
                        <li>Developing financial products and services;</li>
                        <li>To improve and develop our services and quality assurance;</li>
                        <li>Any other purposes as permitted by applicable law and for any other incidental and associated purposes relating to any of the above.</li>
                    </ul><br />
                    <p>AISB retains personal data for as long as necessary as permitted by applicable law for its legitimate business purposes or otherwise destroyed and/or deleted from our records and systems in accordance with our retention policy in the event such data is no longer required for the said purposes.</p>
                    <h4>DISCLOSURE OF YOUR PERSONAL DATA</h4>
                    <p>Your personal data will not be disclosed to any third parties unless it is within the ambit of permitted disclosures under the prevailing laws/guidelines and/or you have consented to such disclosure.</p>
                    <ul>
                        <li>AISB protects your personal data by ensuring we have sufficient security measures in place and shall ensure that your personal data is stored and handled in such a way as to prevent any unauthorised disclosure.</li>
                        <li>AISB shall take all reasonable action to prevent unauthorised use, access or disclosure of and to protect the confidentiality of your personal data in connection with the purpose for which the personal data, has been disclosed to, or has been collected by us.</li>
                        <li>In order to alert you to other financial products and services that AISB offers or sponsors, we may share your information within the AISB affiliated companies.  This would include, for example, sharing your information within AISB to make you aware of new AISB funds or other investment offerings and trust services offered through AISB’s trust companies and registered investment advisors.</li>
                        <li>In certain instances, we may contract with non-affiliated companies to perform services for us.  Where necessary, we will disclose information we have about you to these third parties.  In all such cases, we provide the third party with only the information necessary to carry out its assigned responsibilities and only for that purpose.  And, we require these third parties to treat your private information with the same high degree of confidentiality that we do.</li>
                        <li>We may also disclose your personal data to our consultants, representatives, services providers, vendors, professional advisers acting on our behalf or appointed by us to act on our behalf, our strategic partner outside Malaysia for the purposes of providing you services to trade in overseas securities if you choose so, our professional advisors including auditors, solicitors, accountants and/or other consultants in connection with your facilities, products and services.</li>
                        <li>Finally, we will release information about you if you direct us to do so, if we are compelled by law to do so, or if we are required or authorised by regulatory, government bodies or other authorities to do so to discharge any regulatory function under any law or in relation to any order or judgment of a court.</li>

                    </ul><br />
                    <h4>HOW WE PROTECT PRIVACY ONLINE</h4>
                    <p>Our concern for the privacy of our shareholders naturally extends to those who use our portal, <a href="www.eapexis.apexis.com.my"></a>www.eapexis.apexis.com.my.</p>
                    <ul>
                        <li>Our portal uses some of the most secure forms of online communication available, including data encryption, Secure Sockets Layer (SSL) protocol, and user names and passwords.  These technologies provide a high level of security and privacy when you access your account information, initiate online transactions, or send secure messages;</li>
                        <li><a href="www.eapexis.apexis.com.my">www.eapexis.apexis.com.my</a> offers customised features that require our use of "HTTP cookies" - tiny pieces of information that we ask your browser to store.  However, we make very limited use of these cookies.  We don't use them to pull data from your hard drive, to learn your e-mail address, or to view data in cookies created by other Sites. We won't share the information in our cookies or give others access to it-except to help us better serve your investment needs;</li>
                        <li>When you visit our portal, we may collect certain technical and navigational information, such as computer browser type, Internet protocol address, and pages visited and average time spent on our portal.  This information may be used, for example, to alert you to software compatibility issues or to resolve technical or service problems, or it may be analysed to improve our portal design and functionality and our ability to service you and your accounts.</li>
                    </ul><br />
                    <h4>WHAT YOU CAN DO</h4>
                    <p>For your protection, if you have an online account with us, we advise you NOT to reveal any information relating to your account to anyone.  You are responsible to maintain the confidentiality of your account information, user names, login ids, password, security questions and answers to access your account in this portal.  DO NOT reveal your account information, user names, login ids, password, security questions and answers to access your account in this portal, to anyone at any time under any circumstances. You acknowledge that you are fully responsible for your account and activities and should you become aware of any suspicious and unauthorised activities or any other breach of security, please act immediately and notify us as soon as possible. You should also aware that there shall be risk factors involved in sending confidential information over the Internet Sites, e-mails or other electronic means.</p>
                    <p>By using this portal, you consent and authorise for such transfer of information and you agree that we may contact you through any of the means stated herein.  You further consent and authorise that you will receive information from us through the electronic means from time to time and/or other means as determined by us and/or as required by the regulatory bodies and/or by the law.</p>
                    <p>Online safety tips:-</p>
                    <ul>
                        <li>You should keep your confidential information in a safe place;</li>
                        <li>Always protect your PINs and other passwords.  You should not reveal the PINs and passwords to anyone unless it's for a service or transaction you request;</li>
                        <li>Check your account regularly.  Contact us immediately should there any error or dispute a transaction in your account.</li>
                        <li>If you believe you are a victim of identity theft, take immediate action and keep records of your conversations and correspondence.</li>
                    </ul><br />
                    <p>Do contact us immediately.  File a report with your local police station and keep a copy of the police report for records.</p>
                    <h4>RIGHTS TO MODIFY</h4>
                    <p>We reserve the right to modify this policy at any time without prior notice to you.  Any such amendment shall be effective once the revised changes have been posted on the portal.  Since we may update the portal from time to time, you should constantly check the contents herein from time to time so that you are aware of any changes or amendments to the policy.</p>
                    <h4>YOU NEED TO UPDATE YOUR CURRENT INFORMATION</h4>
                    <p>You should always ensure that you provide us with your most updated and current personal particulars and information in order to ensure that your records with us are kept up to date, complete and accurate.  If any information supplied by you changes during the course of your account with us, you should notify us immediately to enable us to update your information.</p>
                    <h4>RIGHT OF ACCESS, OPT-OUT OF DISCLOSURE AND CORRECT PERSONAL DATA</h4>
                    <p>You have the right to request for access to your personal data and if you wish to opt-out of disclosure of your personal data or request for updating/correction of your personal data held by AISB, please submit your request in writing via post or email to: -</p>
                    <p>Mailing address:<br/>Apex Investment Services Berhad<br/>3rd Floor, Menara MBSB,<br/>46 Jalan Dungun, Damansara Heights,<br/>50490 Kuala Lumpur, Malaysia.<br /></p>
                    <p>Email address:<br/>	<a href="mailto:enquiry@apexis.com.my">enquiry@apexis.com.my</a></p>
                    <h4>ANY ENQUIRIES?</h4>
                    <p>If you have any enquiries or concerns on the Privacy Policy, please <a href="Contact.aspx">contact us</a> and we will respond to your enquiries as soon as possible.</p>
                    <br/>

                </div>
            </div>
        </div>
    </section>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
