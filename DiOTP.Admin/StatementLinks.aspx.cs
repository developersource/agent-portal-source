﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class StatementLinks : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderStatementService> lazyUserStatementServiceObj = new Lazy<IUserOrderStatementService>(() => new UserOrderStatementService());

        public static IUserOrderStatementService IUserOrderStatementService { get { return lazyUserStatementServiceObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        private static readonly Lazy<IUserNotificationSettingService> lazyUserNotificationSettingServiceObj = new Lazy<IUserNotificationSettingService>(() => new UserNotificationSettingService());

        public static IUserNotificationSettingService IUserNotificationSettingService { get { return lazyUserNotificationSettingServiceObj.Value; } }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            string dirPath = HttpContext.Current.Server.MapPath("/Content/Statements/Others/");

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (!Directory.Exists(dirPath))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('No directory');", true);
                }
                else
                {
                    var dirInfo = new DirectoryInfo(dirPath);

                    StringBuilder filter = new StringBuilder();

                    if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                    {
                        IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                    }
                    if (IsNewSearch.Value == "1")
                    {
                        hdnCurrentPageNo.Value = "";
                    }
                    if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                    {
                        Search.Value = Request.QueryString["Search"].ToString();
                        string matchingWords1 = "income distribution id";
                        string matchingWords2 = "unit split us";
                        if (matchingWords1.Contains(Search.Value.Trim().ToLower()))
                        {
                            filter.Append("*_ID_*");
                        }
                        else if (matchingWords2.Contains(Search.Value.Trim().ToLower()))
                        {
                            filter.Append("*_US_*");
                        }
                        else
                            filter.Append("*" + Search.Value.Trim() + "*");
                    }

                    int skip = 0, take = 20;
                    if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                        take = Convert.ToInt32(pageLength.Value);
                    if (hdnCurrentPageNo.Value == "")
                    {
                        skip = 0;
                        hdnNumberPerPage.Value = take.ToString();
                        hdnCurrentPageNo.Value = "1";
                        hdnTotalRecordsCount.Value = dirInfo.GetFiles("*", SearchOption.AllDirectories).Length.ToString();
                    }
                    else
                    {
                        skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                    }
                    List<FileInfo> fileInfosList = dirInfo.GetFiles(filter.Length == 0 ? "*" : filter.ToString(), SearchOption.AllDirectories)
                        .OrderByDescending(f => DateTime.ParseExact(f.Name.Replace(".PDF", "").Replace(".pdf", "").Split('_')[3], "yyyyMMdd", CultureInfo.InvariantCulture))
                        .Skip(skip).Take(take)
                        .ToList();
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    fileInfosList.ForEach(x =>
                    {
                        string fileName = x.Name.Replace(".PDF", "").Replace(".pdf", "");
                        string[] keys = fileName.Split('_');

                        string accNo = keys[0];
                        string fundId = keys[1].ToUpper();
                        string statementType = keys[2].ToUpper();
                        string statementDate = keys[3].ToUpper();
                        string statementTag = "";
                        if (keys.Length == 5)
                            statementTag = keys[4].ToUpper();

                        Response responseUFI = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code = '" + fundId + "' ", 0, 0, false);
                        UtmcFundInformation utmcFundInformation = new UtmcFundInformation();
                        if (responseUFI.IsSuccess)
                        {
                            utmcFundInformation = ((List<UtmcFundInformation>)responseUFI.Data).FirstOrDefault();
                        }

                        DateTime date;
                        bool isDate = DateTime.TryParseExact(statementDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

                        asb.Append(@"<tr>
                                            <td class='icheck'>
                                                <label>" + index + @"</label>
                                                <!--<div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRow' class='checkRow' value='" + x.FullName + @"' /> <label>" + index + @"</label><br/>
                                                    </div>
                                                </div>-->
                                            </td>
                                            <td><a target='_blank' href='/Content/Statements/Others/" + accNo + "/" + statementDate + "/" + x.Name + "'>" + x.Name + @"</a></td>
                                            <td>" + accNo + @"</td>
                                            <td>" + (utmcFundInformation == null ? fundId : utmcFundInformation.FundName.Capitalize()) + @"</td>
                                            <td>" + (statementType == "ID" ? "Income Distribution " + (statementTag == "C" ? "(Letter)" : (statementTag == "D" ? "(Statement)" : "")) + "" : ((statementType == "US" ? "Unit Split " + (statementTag == "C" ? "(Letter)" : (statementTag == "D" ? "(Statement)" : "")) + "" : "-"))) + @"</td>
                                            <td>" + (isDate ? date.ToString("dd/MM/yyyy") : "-") + @"</td>
                                        </tr>
                                    ");
                        index++;
                    });

                    statementLinksTbody.InnerHtml = asb.ToString();
                }
            }
        }

        protected void Page_Load1(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append(" 1=1 ");
                filter.Append(" and status=1 ");
                filter.Append(" and ((order_status=3) or (payment_method='3' and order_status=2))");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameOderNo = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.OrderNo));
                    filter.Append(" and " + columnNameOderNo + " like '%" + Search.Value.Trim() + "%'");
                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IUserOrderService.GetCountByColumnGroup(filter.ToString(), "order_no").ToString(); ;
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }
                filter.Append(" group by order_no ");
                Response responseUOList = IUserOrderService.GetDataByFilter(filter.ToString(), skip, take, true);
                if (responseUOList.IsSuccess)
                {
                    List<UserOrder> userOrder = (List<UserOrder>)responseUOList.Data;
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (UserOrder u in userOrder)
                    {
                        Response responseUOSList = IUserOrderStatementService.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), u.OrderNo, true, 0, 0, true);
                        if (responseUOSList.IsSuccess)
                        {
                            List<UserOrderStatement> userorderstatement = (List<UserOrderStatement>)responseUOSList.Data;

                            UserOrderStatement tax = userorderstatement.Where(x => x.UserOrderStatementTypeId == 1 && x.Status == 1).FirstOrDefault();
                            UserOrderStatement cas = userorderstatement.Where(x => x.UserOrderStatementTypeId == 2 && x.Status == 1).FirstOrDefault();
                            UserOrderStatement creditnote = userorderstatement.Where(x => x.UserOrderStatementTypeId == 3 && x.Status == 1).FirstOrDefault();

                            Response response = IUserService.GetSingle(u.UserId);
                            if (response.IsSuccess)
                            {
                                User user = (User)response.Data;

                                Response response2 = IUserAccountService.GetSingle(u.UserAccountId);
                                if (response2.IsSuccess)
                                {
                                    UserAccount userAccount = (UserAccount)response2.Data;

                                    asb.Append(@"<tr>
                                                    <td class='icheck'>
                                                        <div class='square single-row'>
                                                            <div class='checkbox'>
                                                                <input type='checkbox' name='checkRow' class='checkRow' value='" + u.Id + @"' /> <label>" + index + @"</label><br/>
                                                            </div>
                                                        </div>
                                                        <span class='row-status hide'>
                                                            " + (u.Status == 1 ?
                                                            "<span class='label label-success'>Approved</span>" :
                                                            u.Status == 0 ?
                                                            "<span class='label label-warning'>Pending</span>" :
                                                            "<span class='label label-inverse'>Rejected</span>")
                                                        + @"</span>
                                                    </td>
                                                    <td>" + u.OrderNo + @"</td>
                                                    <td>" + user.Username + @"</td>
                                                    <td>" + userAccount.AccountNo + @"</td>
                                                    <td>" + u.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                                    <td>" + (u.IsTaxInvoice == 1 && tax != null ? "<a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text - info' href='" + tax.Url + @"' target='_blank'><i class='fa fa-eye'></i></a>" : "Not available") + @"</td>
                                                    <td>" + (u.IsCas == 1 && cas != null ? "<a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text - info' href='" + cas.Url + @"' target='_blank'><i class='fa fa-eye'></i></a>" : "Not available") + @"</td>
                                                    <td>" + (u.IsCreditNote == 1 && creditnote != null ? "<a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text - info' href='" + creditnote.Url + @"' target='_blank'><i class='fa fa-eye'></i></a>" : "Not available") + @"</td>
                                                 </tr>
                                              ");
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                    "alert('" + response2.Message + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + response.Message + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert(\"" + responseUOSList.Message + "\");", true);
                        }
                        index++;

                    }
                    statementLinksTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseUOList.Message + "');", true);
                }
                
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(UserOrderStatement obj)
        {
            try
            {
                if (obj.Id == 0)
                {
                    obj.CreatedDate = DateTime.Now;
                    IUserOrderStatementService.PostData(obj);
                    Response responseUOList = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.RefNo), obj.RefNo, true, 0, 0, true);
                    if (responseUOList.IsSuccess)
                    {
                        List<UserOrder> uos = (List<UserOrder>)responseUOList.Data;
                        foreach (UserOrder u in uos)
                        {
                            if (obj.UserOrderStatementTypeId == 1)
                                u.IsTaxInvoice = 1;
                            if (obj.UserOrderStatementTypeId == 2)
                                u.IsCas = 1;
                            if (obj.UserOrderStatementTypeId == 3)
                                u.IsCreditNote = 1;
                        }
                        IUserOrderService.UpdateBulkData(uos);
                    }
                }
                else
                {
                    obj.UpdatedDate = DateTime.Now;
                    IUserOrderStatementService.UpdateData(obj);
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Sattement Links Add: " + ex.Message);
                return false;
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object Upload(Statement[] statements)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            List<string> responseMsgs = new List<string>();
            int idx = 1;
            try
            {
                foreach (Statement s in statements)
                {
                    string ex = Path.GetExtension(s.Name).ToUpper();
                    if (ex == ".PDF")
                    {
                        string fileName = s.Name.Replace(".PDF", "").Replace(".pdf", "");
                        string[] keys = fileName.Split('_');
                        int index = 5;
                        if (index == keys.Length)
                        {
                            // MANO_FUNDID_DOCTYPE_YYYYMMDD_TAG.pdf  DOCTYPE: ID - Income Distribution, US - Unit Split | TAG: C - COVER LETTER, D - DOCUMENT
                            // Eg. 368_01_ID_20191231.pdf
                            if (keys[0] != "" && keys[1] != "" && keys[2] != "" && keys[3] != "" && keys[4] != "")
                            {
                                string accNo = keys[0];
                                string fundId = keys[1].ToUpper();
                                string statementType = keys[2].ToUpper();
                                string statementDate = keys[3].ToUpper();
                                string statementTag = keys[4].ToUpper();
                                Response responseUA = IUserAccountService.GetDataByFilter(" account_no ='" + accNo + "' ", 0, 0, false);
                                if (responseUA.IsSuccess)
                                {
                                    UserAccount userAcc = ((List<UserAccount>)responseUA.Data).FirstOrDefault();
                                    if (userAcc != null)
                                    {
                                        Response responseUFI = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code = '" + fundId + "' ", 0, 0, false);
                                        if (responseUFI.IsSuccess)
                                        {
                                            UtmcFundInformation utmcFundInformation = ((List<UtmcFundInformation>)responseUFI.Data).FirstOrDefault();
                                            if (utmcFundInformation != null)
                                            {
                                                if (statementType == "ID" || statementType == "US")
                                                {
                                                    DateTime date;
                                                    bool isDate = DateTime.TryParseExact(statementDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                                                    if (isDate)
                                                    {
                                                        if (date < DateTime.Now)
                                                        {
                                                            if (statementTag == "C" || statementTag == "D")
                                                            {
                                                                string dirPath = HttpContext.Current.Server.MapPath("/Content/Statements/Others/");
                                                                string filename = Path.GetFileName(s.Name);
                                                                dirPath += accNo + "/" + statementDate + "/";
                                                                string filePath = dirPath + filename;
                                                                Directory.CreateDirectory(dirPath);
                                                                if (File.Exists(filePath))
                                                                    File.Delete(filePath);
                                                                byte[] result = Convert.FromBase64String(s.Base64String.Replace("data:application/pdf;base64,", ""));
                                                                File.WriteAllBytes(filePath, result);

                                                                //Insert for notification
                                                                UserNotificationSetting userNotificationSetting = new UserNotificationSetting
                                                                {
                                                                    UserId = userAcc.UserId,
                                                                    UserAccountId = userAcc.Id,
                                                                    NotificationTypeDefId = (statementType == "ID" ? 5 : (statementType == "US" ? 6 : 0)),
                                                                    Value = "/Content/Statements/Others/" + filename,
                                                                    CreatedBy = 0,
                                                                    CreatedDate = DateTime.Now,
                                                                    UpdatedBy = 0,
                                                                    UpdatedDate = DateTime.Now,
                                                                    Status = 1,
                                                                    FundCode = ""
                                                                };

                                                                Response responseInsert = IUserNotificationSettingService.PostData(userNotificationSetting);
                                                                if (!responseInsert.IsSuccess)
                                                                {
                                                                    Logger.WriteLog(responseInsert.Message + " - Exception");
                                                                }

                                                                responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-success'><i class='fa fa-check'></i></label></td><td><strong class='text-success'>" + s.Name + "</strong></td><td> <strong class='text-success'>Successfully uploaded! "+ (responseInsert.IsSuccess ? "" : responseInsert.Message + " - Ex") + "</strong></td></tr>");
                                                                //Audit Log starts here
                                                                AdminLogMain alm = new AdminLogMain()
                                                                {
                                                                    Description = s.Name + " succesfully uploaded",
                                                                    TableName = "user_order_statements",
                                                                    UpdatedDate = DateTime.Now,
                                                                    UserId = loginUser.Id,
                                                                };
                                                                Response responseLog = IAdminLogMainService.PostData(alm);
                                                                if (!responseLog.IsSuccess)
                                                                {
                                                                    //Audit log failed
                                                                }
                                                                else
                                                                {

                                                                }

                                                                //Audit Log Ends here
                                                                idx++;
                                                            }
                                                            else
                                                            {
                                                                response.IsSuccess = false;
                                                                responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> Tag is invalid!</td></tr>");
                                                                idx++;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            response.IsSuccess = false;
                                                            responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> Date is invalid!</td></tr>");
                                                            idx++;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        response.IsSuccess = false;
                                                        responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> Date format is invalid!</td></tr>");
                                                        idx++;
                                                    }
                                                }
                                                else
                                                {
                                                    response.IsSuccess = false;
                                                    responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> Statement Type does not exist!</td></tr>");
                                                    idx++;
                                                }
                                            }
                                            else
                                            {
                                                response.IsSuccess = false;
                                                responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> Fund ID does not exist!</td></tr>");
                                                idx++;
                                            }
                                        }
                                        else
                                        {
                                            response.IsSuccess = false;
                                            responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> " + responseUFI.Message + "</td></tr>");
                                            idx++;
                                        }
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> Account No does not exist!</td></tr>");
                                        idx++;
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> " + responseUA.Message + "</td></tr>");
                                    idx++;
                                }
                            }
                            else
                            {
                                response.IsSuccess = false;
                                responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> File name is invalid!</td></tr>");
                                idx++;
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> File name is invalid!</td></tr>");
                            idx++;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> File type is invalid!</td></tr>");
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                responseMsgs.Add("<tr><td colspan='4' class='text-center'><label class='label label-danger'><i class='fa fa-times'></i></label>" + ex.Message + "</td></tr>");
            }
            response.Message = String.Join("<br/>", responseMsgs.ToArray());
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object Upload1(Statement[] statements)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            List<string> responseMsgs = new List<string>();

            try
            {
                foreach (Statement s in statements)
                {
                    string ex = Path.GetExtension(s.Name).ToUpper();
                    if (ex == ".PDF")
                    {
                        string[] keys = s.Name.Split('_');
                        int index = 3;

                        if (index == keys.Length)
                        {
                            if (keys[0] != "" && keys[1] != "" && keys[2] != "")
                            {
                                string accNo = keys[0];
                                string orderNo = keys[1].ToUpper();
                                string fileType = keys[2].ToUpper();

                                Response responseUA = IUserAccountService.GetDataByFilter(" account_no ='" + accNo + "' ", 0, 0, false);
                                if (responseUA.IsSuccess)
                                {
                                    UserAccount userAcc = ((List<UserAccount>)responseUA.Data).FirstOrDefault();
                                    if (userAcc != null)
                                    {
                                        StringBuilder filter = new StringBuilder();
                                        filter.Append("user_account_id = " + userAcc.Id);
                                        filter.Append(" and order_no = '" + orderNo + "' ");
                                        filter.Append(" group by order_no");
                                        Response responseUOList = IUserOrderService.GetDataByFilter(filter.ToString(), 0, 0, true);
                                        if (responseUOList.IsSuccess)
                                        {
                                            UserOrder userOrder = ((List<UserOrder>)responseUOList.Data).FirstOrDefault();
                                            if (userOrder != null)
                                            {
                                                if (userOrder.OrderStatus == 2 || userOrder.OrderStatus == 3)
                                                {
                                                    Response responseUOSList = IUserOrderStatementService.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), userOrder.RefNo, true, 0, 0, true);
                                                    if (responseUOSList.IsSuccess)
                                                    {
                                                        List<UserOrderStatement> userorderstatement = (List<UserOrderStatement>)responseUOSList.Data;
                                                        Response response1 = IUserService.GetSingle(userOrder.UserId);
                                                        if (response1.IsSuccess)
                                                        {
                                                            User user = (User)response1.Data;
                                                            if (accNo == userAcc.AccountNo)
                                                            {
                                                                if (orderNo == userOrder.OrderNo)
                                                                {
                                                                    if (fileType.Equals("TI" + ex) || fileType.Equals("CAS" + ex) || fileType.Equals("CN" + ex))
                                                                    {
                                                                        UserOrderStatement userOrderStatement = userorderstatement.Where(x => x.Name == s.Name).FirstOrDefault();
                                                                        UserOrderStatement newUserOrderStatement = new UserOrderStatement();

                                                                        byte[] result = Convert.FromBase64String(s.Base64String.Replace("data:application/pdf;base64,", ""));
                                                                        File.WriteAllBytes(HttpContext.Current.Server.MapPath("/Content/Statements/") + Path.GetFileName(s.Name), result);

                                                                        Response responseUOList1 = IUserOrderService.GetDataByFilter(" order_no = '" + orderNo + "' group by id", 0, 0, false);
                                                                        if (responseUOList1.IsSuccess)
                                                                        {
                                                                            List<UserOrder> order = (List<UserOrder>)responseUOList1.Data;
                                                                            List<UserOrder> multipleOrders = new List<UserOrder>();
                                                                            order.ForEach(x =>
                                                                            {
                                                                                Response response2 = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                                                                                if (response2.IsSuccess)
                                                                                {
                                                                                    List<UserOrder> ol = (List<UserOrder>)response2.Data;
                                                                                    if (ol.Count > 0)
                                                                                    {
                                                                                        multipleOrders.AddRange(ol);
                                                                                    }
                                                                                }
                                                                            });

                                                                            if (fileType.Equals("TI" + ex))
                                                                            {
                                                                                newUserOrderStatement.UserOrderStatementTypeId = 1;
                                                                                multipleOrders.ForEach(x =>
                                                                                {
                                                                                    x.IsTaxInvoice = 1;
                                                                                });
                                                                                IUserOrderService.UpdateBulkData(multipleOrders);
                                                                            }
                                                                            else if (fileType.Equals("CAS" + ex))
                                                                            {
                                                                                newUserOrderStatement.UserOrderStatementTypeId = 2;
                                                                                multipleOrders.ForEach(x =>
                                                                                {
                                                                                    x.IsCas = 1;
                                                                                });
                                                                                IUserOrderService.UpdateBulkData(multipleOrders);
                                                                            }
                                                                            else if (fileType.Equals("CN" + ex))
                                                                            {
                                                                                newUserOrderStatement.UserOrderStatementTypeId = 3;
                                                                                multipleOrders.ForEach(x =>
                                                                                {
                                                                                    x.IsCreditNote = 1;
                                                                                });
                                                                                IUserOrderService.UpdateBulkData(multipleOrders);
                                                                            }
                                                                        }

                                                                        if (userOrderStatement != null)
                                                                        {
                                                                            //update existing record as old file.
                                                                            userOrderStatement.Status = 0;
                                                                            IUserOrderStatementService.UpdateData(userOrderStatement);

                                                                            newUserOrderStatement.RefNo = userOrder.OrderNo;
                                                                            newUserOrderStatement.Name = s.Name;
                                                                            newUserOrderStatement.Url = "/Content/Statements/" + Path.GetFileName(s.Name);
                                                                            newUserOrderStatement.CreatedBy = 0;
                                                                            newUserOrderStatement.CreatedDate = DateTime.Now;
                                                                            newUserOrderStatement.UpdatedBy = 0;
                                                                            newUserOrderStatement.Status = 1;
                                                                            newUserOrderStatement.UserId = userOrder.UserId;
                                                                            newUserOrderStatement.UserAccountId = userOrder.UserAccountId;
                                                                            IUserOrderStatementService.PostData(newUserOrderStatement);

                                                                            response.IsSuccess = true;
                                                                            responseMsgs.Add("[" + s.Name + "] updates successfully");

                                                                            Email email = new Email();
                                                                            email.user = user;
                                                                            EmailService.SendUpdateMail(email, "Statement Notification", "Your latest Fund Annual Report and Statement are ready for viewing <br/> Please log on to eApexIs at <a href='https://eapexis.apexis.com.my/' style='color:#7777ea'>eapexis.apexis.com.my</a> and select 'Statements' option, to view your report or statement", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                                                            //Audit Log starts here
                                                                            AdminLogMain alm = new AdminLogMain()
                                                                            {
                                                                                Description = s.Name + " succesfully updated",
                                                                                TableName = "user_order_statements",
                                                                                UpdatedDate = DateTime.Now,
                                                                                UserId = loginUser.Id,
                                                                            };
                                                                            Response responseLog = IAdminLogMainService.PostData(alm);
                                                                            if (!responseLog.IsSuccess)
                                                                            {
                                                                                //Audit log failed
                                                                            }
                                                                            else
                                                                            {

                                                                            }

                                                                            //Audit Log Ends here
                                                                        }
                                                                        else
                                                                        {
                                                                            newUserOrderStatement.RefNo = userOrder.OrderNo;
                                                                            newUserOrderStatement.Name = s.Name;
                                                                            newUserOrderStatement.Url = "/Content/Statements/" + Path.GetFileName(s.Name);
                                                                            newUserOrderStatement.CreatedBy = 0;
                                                                            newUserOrderStatement.CreatedDate = DateTime.Now;
                                                                            newUserOrderStatement.UpdatedBy = 0;
                                                                            newUserOrderStatement.Status = 1;
                                                                            newUserOrderStatement.UserId = userOrder.UserId;
                                                                            newUserOrderStatement.UserAccountId = userOrder.UserAccountId;
                                                                            IUserOrderStatementService.PostData(newUserOrderStatement);

                                                                            response.IsSuccess = true;
                                                                            responseMsgs.Add("[" + s.Name + "] uploads successfully");

                                                                            Email email = new Email();
                                                                            email.user = user;
                                                                            EmailService.SendUpdateMail(email, "Statement Notification", "Statement is uploaded", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                                                            //Audit Log starts here
                                                                            AdminLogMain alm = new AdminLogMain()
                                                                            {
                                                                                Description = s.Name + " succesfully uploaded",
                                                                                TableName = "user_order_statements",
                                                                                UpdatedDate = DateTime.Now,
                                                                                UserId = loginUser.Id,
                                                                            };
                                                                            Response responseLog = IAdminLogMainService.PostData(alm);
                                                                            if (!responseLog.IsSuccess)
                                                                            {
                                                                                //Audit log failed
                                                                            }
                                                                            else
                                                                            {

                                                                            }

                                                                            //Audit Log Ends here
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        response.IsSuccess = false;
                                                                        responseMsgs.Add("Statement Type of [" + s.Name + "] contains invalid name");
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    response.IsSuccess = false;
                                                                    responseMsgs.Add("Order No of [" + s.Name + "] is not exist");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.IsSuccess = false;
                                                                responseMsgs.Add("Account No of [" + s.Name + "] is not exist");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            response.IsSuccess = false;
                                                            responseMsgs.Add(response1.Message);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        response.IsSuccess = false;
                                                        responseMsgs.Add(responseUOSList.Message);
                                                    }
                                                }
                                                else
                                                {
                                                    response.IsSuccess = false;
                                                    responseMsgs.Add("Order No of [" + s.Name + "] is not approved");
                                                }
                                            }
                                            else
                                            {
                                                response.IsSuccess = false;
                                                responseMsgs.Add("Order No of [" + s.Name + "] may not exist / not belong to this MA");
                                            }
                                        }
                                        else
                                        {
                                            response.IsSuccess = false;
                                            responseMsgs.Add(responseUOList.Message);
                                        }
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        responseMsgs.Add("Account No of [" + s.Name + "] is not exist");
                                    }
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    responseMsgs.Add(responseUA.Message);
                                }
                            }
                            else
                            {
                                response.IsSuccess = false;
                                responseMsgs.Add("File name of [" + s.Name + "] is invalid");
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            responseMsgs.Add("File name of [" + s.Name + "] is invalid");
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        responseMsgs.Add("File type of [" + s.Name + "] is invalid");
                    }
                }

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                responseMsgs.Add(ex.Message);
            }
            response.Message = String.Join(", ", responseMsgs.ToArray());
            return response;
        }

        public class Statement
        {
            public string Base64String { get; set; }
            public string Name { get; set; }
        }
    }
}