﻿using DiOTP.OracleData;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DiOTP.WebApi.Controllers
{
    public class UtmcMemberInvestmentApiController : ApiController
    {
        // GET api/<controller>/5
        public List<OracleData.UtmcMemberInvestment> Get(string holderNo, string startDate, string endDate, string fundIds)
        {
            Console.WriteLine("API Holder: " + holderNo);
            return UtmcMemberInvestmentLiveData.GetData(holderNo, startDate, endDate, fundIds);
        }

    }
}
