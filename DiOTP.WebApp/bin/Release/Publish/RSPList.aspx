﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="RSPList.aspx.cs" Inherits="DiOTP.WebApp.RSPList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">RSP Enrollment Status</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">RSP Enrollment Status</h3>
            </div>

            <div class="row mb-10">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <asp:DropDownList CssClass="form-control" ID="ddlUserAccountId" runat="server" ClientIDMode="Static"></asp:DropDownList>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hide">
                    <asp:DropDownList CssClass="form-control" ID="ddlOrderType" runat="server" ClientIDMode="Static">
                        <asp:ListItem Value="" Text="All types"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Buy"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Sell"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Switch"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <asp:DropDownList CssClass="form-control" ID="ddlOrderStatus" runat="server" ClientIDMode="Static">
                        <asp:ListItem Value="" Text="All Status"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Pending"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Processing"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Completed"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Failed"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <asp:DropDownList ID="ddlTransactionPeriod" runat="server" CssClass="form-control">
                        <asp:ListItem Value="" Text="Transaction Period" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="-3" Text="Past 3 months"></asp:ListItem>
                        <asp:ListItem Value="-6" Text="Past 6 months"></asp:ListItem>
                        <asp:ListItem Value="-12" Text="Past 12 months"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btn-block" ClientIDMode="Static" Text="Submit" OnClick="btnSearch_Click" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h5>RSP status for Master Account: <span id="spanMA" runat="server"></span></h5>
                    <div class="table-responsive">
                        <table class="table table-font-size-13 table-bordered OrderListTable">
                            <thead id="theadOrderList" runat="server">
                                <tr>
                                    <th style="width: 50px"></th>
                                    <th style="width: 50px">S. No</th>
                                    <th>Account No</th>
                                    <th>Order No</th>
                                    <th>Order Date</th>
                                    <th>Bank Name</th>
                                    <th>FPX ID</th>
                                    <th>Order Status</th>
                                    <th class="text-right">RSP Amount(MYR)</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyUserOrders" runat="server" clientidmode="Static">
                                <%--<tr>
                                <td><a href="javascript:;" class="show-extended-row"><i class="fa fa-plus"></i></a>1</td>
                                <td>SA0000001</td>
                                <td>08/11/2019</td>
                                <td>MYR 500.00</td>
                            </tr>
                            <tr class="hide">
                                <td colspan="4">
                                    <div class="extended-row" style="display:none;">
                                        <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Fund Name</th>
                                                <th>Payment Method</th>
                                                <th>Transaction date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Apex</td>
                                                <td>FPX</td>
                                                <td>Date</td>
                                                <td>Pending</td>
                                                <td>Act</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                    
                                </td>
                            </tr>--%>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script>
        $(document).ready(function () {
            $('.show-extended-row').click(function () {
                if ($(this).find('i').hasClass('fa-plus')) {
                    $('.show-extended-row i.fa-minus').each(function (idx, obj) {
                        var ele = $(obj).parents('.show-extended-row');
                        var extendedTRAll = $(ele).parents('tr').next();
                        var extendedRowAll = $(ele).parents('tr').next().find('.extended-row');
                        $(extendedRowAll).slideUp(500, function () {
                            $(extendedTRAll).addClass('hide');
                        });
                        $(ele).find('i').addClass('fa-plus');
                        $(ele).find('i').removeClass('fa-minus');
                    });
                }
                var extendedTR = $(this).parents('tr').next();
                var extendedRow = $(this).parents('tr').next().find('.extended-row');
                if ($(this).find('i').hasClass('fa-plus')) {
                    $(extendedTR).removeClass('hide');
                    $(extendedRow).slideDown();
                    $(this).find('i').removeClass('fa-plus');
                    $(this).find('i').addClass('fa-minus');
                }
                else {
                    $(extendedRow).slideUp(500, function () {
                        $(extendedTR).addClass('hide');
                    });
                    $(this).find('i').removeClass('fa-minus');
                    $(this).find('i').addClass('fa-plus');
                }
            });
        });
    </script>
</asp:Content>
