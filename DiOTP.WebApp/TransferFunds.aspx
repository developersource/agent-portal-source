﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="TransferFunds.aspx.cs" Inherits="DiOTP.WebApp.TransferFunds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-10 col-md-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Transfer Funds</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Transfer Fund</h3>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h4 class="mb-4">Current Account Selected:</h4>
                        <h5>Account Name: <span id="accName" runat="server"></span></h5>
                        <h5>MA Number: <span id="maAccNumber" runat="server"></span></h5>
                        <h5>Account Plan: <span id="accType" runat="server"></span></h5>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center form-box">
                    <div class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-universal-access"></i></div>
                                <p>Account Overview</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                                <p>Fund Selection & Account</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-file-text-o"></i></div>
                                <p>Confirmation</p>
                            </div>
                        </div>
                        <small class="text-danger" id="errorMessage"></small>
                        <small class="text-success" id="successMessage"></small>
                        <fieldset class="fieldsetAccountFundsOverview">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Fund</th>
                                                <th>Units</th>
                                                <th>Market Value</th>
                                                <th>Current NAV</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyAccountFunds" runat="server">
                                        </tbody>
                                    </table>
                                    <div class="f1-buttons">
                                        <button type="button" class="btn btn-next">Next</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="fieldsetfundSelectionAndAmount">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlFundsList" runat="server" ClientIDMode="Static" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtUnits" runat="server" ClientIDMode="Static" placeholder="Enter units" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:HiddenField ID="hdnTransferToAccountId" runat="server" ClientIDMode="Static" />
                                            <asp:TextBox ID="txtTransferToAccountNo" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                                            <div class="form-group mb-10">
                                                <asp:CheckBox ID="chkAccountConfirm" ClientIDMode="Static" runat="server" CssClass="checkbox-inline" Text="I confirm the Account No to Transfer." />
                                                <a href="javascript:;" class="btn btn-sm btn-infos1 pull-right checkAccountNo mb-10">Check</a>
                                            </div>
                                            <div id="divAccountDetails" class="hide">
                                                <div class="loadingDiv">
                                                    <div class="typing_loader"></div>
                                                    <div class="text-center">Loading...</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Account Name</td>
                                                                    <td class="accountName">Account Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Holder Class</td>
                                                                    <td class="holderClass">0</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 mt-10 pull-right">
                                            <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block">Add</a>
                                            <%--<asp:Button ID="btnAddFund" runat="server" ClientIDMode="Static" CssClass="btn btn-infos1" Text="Add" />--%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Selected Fund</th>
                                                        <th>To Account</th>
                                                        <th>Transfer Units</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds" runat="server" clientidmode="static"></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormat tdTotalSwitchingUnits" id="tdTotalSwitchingUnits" runat="server" clientidmode="static"></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <button type="button" class="btn btn-next">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="fieldsetConfirmation">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Selected Fund</th>
                                                        <th>To Account</th>
                                                        <th>Transfer Units</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds1" runat="server" clientidmode="static"></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormat tdTotalSwitchingUnits" id="tdTotalSwitchingUnits1" runat="server" clientidmode="static"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-next btn-infos1" Text="Proceed" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <asp:HiddenField ID="hdnAccountPlan" runat="server" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div class="modal fade" id="commonTermsPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4>Privacy Policy</h4>
            </div>
            <div class="modal-body">
                <div style="text-align: left;">
                    <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Privacy Policy</h3>
                </div>

                <p class="text-justify">You may transfer all or some of your investments to another person by completing a transfer form(ops/marketing to prepare) and signed by both parties (transferor and transferee).
A fee of RM XX will be charged to the transferor for each account transferred. However, in the case of a deceased person, the transfer fee of RM XX will be waived/charged(Company to decide).
The transfer is subject to the Managers approval. <strong><u>Fee is non-refundable for any failed transfer.</u></strong></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-infos1" data-dismiss="modal">Accept</button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script type="text/javascript">
        function OpenCommonTermsPopup() {
            $('#commonTermsPopup').modal({
                keyboard: false,
                backdrop: "static",
            });
        }
        OpenCommonTermsPopup();
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        var isUniFormatNoSymbol;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: ' UNITS', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
        }
        FormatAllUnit();
        function scroll_to_class(element_class, removed_height) {
            var scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({ scrollTop: scroll_to }, 0);
            }
        }
        function bar_progress(progress_line_object, direction) {
            var number_of_steps = progress_line_object.data('number-of-steps');
            var now_value = progress_line_object.data('now-value');
            var new_value = 0;
            if (direction == 'right') {
                new_value = now_value + (100 / number_of_steps);
            }
            else if (direction == 'left') {
                new_value = now_value - (100 / number_of_steps);
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }
        /* Form */
        $('.f1 fieldset:first').fadeIn('slow');
        $('.f1 input[type="text"], .f1 input[type="password"], .f1 input[type="checkbox"], .f1 textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });
        // next step
        $('.f1 .btn-next').on('click', function () {

            var parent_fieldset = $(this).parents('fieldset');
            var next_step = true;
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            // fields validation
            if (parent_fieldset.hasClass('fieldsetfundSelectionAndAmount')) {
                if (CartItems == undefined) {
                    alert('Please add fund to proceed. Thank you.');
                    next_step = false;
                }
                else {
                    if (CartItems.length == 0) {
                        alert('Please add fund to proceed. Thank you.');
                        next_step = false;
                    }
                }
            }
            else
                parent_fieldset.find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                    if ($(this).val() == "") {
                        $(this).addClass('input-error');
                        next_step = false;
                    }
                    else {
                        if ($(this).is(':checkbox')) {
                            if (!$(this).attr('checked')) {
                                $(this).parents('.form-group').addClass('input-error');
                                next_step = false;
                            }
                            else {
                                $(this).parents('.form-group').removeClass('input-error');
                            }
                        }
                        else {
                            $(this).removeClass('input-error');
                        }
                    }
                });
            // fields validation
            if (next_step) {
                parent_fieldset.fadeOut(400, function () {
                    // change icons
                    current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                    // progress bar
                    bar_progress(progress_line, 'right');
                    // show next step
                    $(this).next().fadeIn();
                    // scroll window to beginning of the form
                    scroll_to_class($('.f1'), 20);
                });
            }
        });
        // previous step
        $('.f1 .btn-previous').on('click', function () {
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            $(this).parents('fieldset').fadeOut(400, function () {
                // change icons
                current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                // progress bar
                bar_progress(progress_line, 'left');
                // show previous step
                $(this).prev().fadeIn();
                // scroll window to beginning of the form
                scroll_to_class($('.f1'), 20);
            });
        });
        // submit
        $('.f1').on('submit', function (e) {
            // fields validation
            $(this).find('input[type="text"], input[type="password"], input[type="checkbox"], textarea').each(function () {
                if ($(this).val() == "") {
                    e.preventDefault();
                    $(this).addClass('input-error');
                }
                else {
                    $(this).removeClass('input-error');
                }
            });
            // fields validation
        });

        function BindCart() {
            $('.tdTotalSwitchingUnits').html(TotalUnits);
            $('#tbodySelectedFunds, #tbodySelectedFunds1').html('');
            var tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundCode + " - " + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td>" + obj.userAccount2.MaHolderRegIdMaHolderReg.Name1 + " - " + obj.userAccount2.AccountNo + "</td>"
                tbodySelectedFundsHtml += "<td class='unitFormat'>" + obj.Units + "</td>"
                tbodySelectedFundsHtml += "<td><a href='javascript:;' data-id=" + obj.Id +
                    " class='btn btn-sm btn-default removeFund' data-toggle='tooltip' title='Remove'><i class='fa fa-times'></i></a></td>";
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds').html(tbodySelectedFundsHtml);
            tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundCode + " - " + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td>" + obj.userAccount2.MaHolderRegIdMaHolderReg.Name1 + " - " + obj.userAccount2.AccountNo + "</td>"
                tbodySelectedFundsHtml += "<td class='unitFormat'>" + obj.Units + "</td>"
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds1').html(tbodySelectedFundsHtml);
            FormatAllCurrency();
            FormatAllUnit();
        }
        function FundDetails(Id) {
            $('.loadingDiv').removeClass('hide');
            $.ajax({
                url: "SellFunds.aspx/GetFundDetails",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { 'Id': Id },
                success: function (data) {
                    var json = data.d.UtmcFundInformation;
                    var latestNavPU = parseFloat(json.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice).toFixed(4);
                    //console.log(data.d.utmcDetailedMemberInvestmentsByFund[0].utmcMemberInvestments[0]);
                    var utmcDetailedMemberInvestmentsByFund = data.d.utmcDetailedMemberInvestmentsByFund;
                    $('#txtUnits').val(utmcDetailedMemberInvestmentsByFund[0].utmcMemberInvestments[0].ActualTransferredFromEpfRm.toFixed(2));
                    $('.loadingDiv').addClass('hide');
                }
            });
        }
        var CartItems;
        var TotalUnits;
        $(document).ready(function () {

            $('#ddlFundsList').change(function () {
                var Id = $(this).val();
                FundDetails(Id);
            });

            $.ajax({
                url: "TransferFunds.aspx/BindFunds",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                //data: { fundId: fundId, amount: amount },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalUnits = data.d.TotalUnits;
                    BindCart();
                }
            });

            $('.checkAccountNo').click(function () {
                var accounNo = $('#txtTransferToAccountNo').val();
                if (accounNo != '') {
                    $('#divAccountDetails .loadingDiv').removeClass('hide');
                    $('#divAccountDetails').removeClass('hide');
                    $.ajax({
                        url: "TransferFunds.aspx/GetAccountDetails",
                        async: true,
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { 'accNo': accounNo },
                        success: function (data) {
                            var isError = data.d.isError;
                            if (isError == 1) {
                                var ErrorMessage = data.d.ErrorMessage;
                                $('.accountNo').html('');
                                $('.accountName').html('<span class="text-danger">' + ErrorMessage + '</span>');
                                $('.holderClass').html('');
                                $('#chkAccountConfirm').removeAttr('checked');
                            }
                            else {
                                var userAccount = data.d.userAccount;
                                var holderClass = data.d.holderClass;
                                var maHolderReg = userAccount.MaHolderRegIdMaHolderReg;
                                $('#hdnTransferToAccountId').val(userAccount.Id);
                                $('.accountNo').html(userAccount.AccountNo);
                                $('.accountName').html(maHolderReg.Name1 + " " + maHolderReg.Name2 + " " + maHolderReg.Name3 + " " + maHolderReg.Name4);
                                $('.holderClass').html(holderClass.Code + "- " + holderClass.Name);
                            }
                            $('#divAccountDetails .loadingDiv').addClass('hide');
                        }
                    });
                }
                else {
                    $('#txtTransferToAccountNo').addClass('input-error');
                    $('#chkAccountConfirm').removeAttr('checked');
                }
            });

            $('#btnAddFund').click(function () {
                var fundId = $('#ddlFundsList').val();
                var toAccountId = $('#hdnTransferToAccountId').val();
                var units = $('#txtUnits').val();
                if (CartItems != undefined) {
                    var items = $.grep(CartItems, function (obj, idx) {
                        return obj.UtmcFundInformation.Id == parseInt(fundId);
                    });
                    var isModify = 0;
                    if (items.length > 0) {
                        if (confirm('Do you want to modify existing fund units?')) {
                            isModify = 1;
                        }
                        else {
                            $('#errorMessage').html('Operation Cancelled by you.');
                            return false;
                        }
                    }
                }
                if (units == "" && toAccountId == "") {
                    $('#errorMessage').html('Units & Account is required.');
                    return false;
                }
                else {
                    $('#errorMessage').html('');
                    $('#successMessage').html('');
                    var fundholdingUnits = $('#ddlFundsList option:selected').attr('data-holding-units');
                    var accountPlan = $('#hdnAccountPlan').val();
                    if (parseFloat(units) > parseFloat(fundholdingUnits)) {
                        $('#errorMessage').html('Cannot exceed current holding: ' + fundholdingUnits);
                        return false;
                    }
                    $('#errorMessage').html('');
                    $.ajax({
                        url: "TransferFunds.aspx/AddFund",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { fundId: fundId, ToAccountId: toAccountId, units: units },
                        success: function (data) {
                            CartItems = data.d.CartItems;
                            TotalUnits = data.d.TotalUnits;
                            BindCart();
                            if (isModify == 1) {
                                $('#successMessage').html('Successfully Modified with your confirmation.');
                            }
                            else {
                                $('#successMessage').html('Successfully Added.');
                            }
                        }
                    });
                }
            });
            $('#tbodySelectedFunds').on('click', '.removeFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var Id = $(this).attr('data-id');
                $.ajax({
                    url: "TransferFunds.aspx/RemoveFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: Id },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalUnits = data.d.TotalUnits;
                        BindCart();
                        $('#successMessage').html('Successfully removed.');
                    }
                });
            });
        });

    </script>
</asp:Content>
