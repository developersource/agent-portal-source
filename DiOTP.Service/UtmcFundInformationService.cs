using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UtmcFundInformationService : IUtmcFundInformationService
    {
        private static readonly Lazy<IUtmcFundInformationRepo> lazy = new Lazy<IUtmcFundInformationRepo>(() => new UtmcFundInformationRepo());
        public static IUtmcFundInformationRepo IUtmcFundInformationRepo { get { return lazy.Value; } }

        private static readonly Lazy<IUtmcDailyNavFundRepo> lazyIUtmcDailyNavFundRepo = new Lazy<IUtmcDailyNavFundRepo>(() => new UtmcDailyNavFundRepo());
        public static IUtmcDailyNavFundRepo IUtmcDailyNavFundRepo { get { return lazyIUtmcDailyNavFundRepo.Value; } }

        private static readonly Lazy<IFundReturnRepo> lazyIFundReturnRepo = new Lazy<IFundReturnRepo>(() => new FundReturnRepo());
        public static IFundReturnRepo IFundReturnRepo { get { return lazyIFundReturnRepo.Value; } }

        private static readonly Lazy<IUtmcFundCorporateActionRepo> lazyIUtmcFundCorporateActionRepo = new Lazy<IUtmcFundCorporateActionRepo>(() => new UtmcFundCorporateActionRepo());
        public static IUtmcFundCorporateActionRepo IUtmcFundCorporateActionRepo { get { return lazyIUtmcFundCorporateActionRepo.Value; } }

        public Response PostData(UtmcFundInformation obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcFundInformationRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundInformation> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcFundInformationRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundInformation obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcFundInformationRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundInformation> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcFundInformationRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundInformation DeleteData(Int32 Id)
        {
            UtmcFundInformation obj = new UtmcFundInformation();
            try
            {
                obj = IUtmcFundInformationRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcFundInformationRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundInformation obj = new UtmcFundInformation();
            try
            {
                response = IUtmcFundInformationRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundInformation> objs = new List<UtmcFundInformation>();
            try
            {
                response = IUtmcFundInformationRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUtmcFundInformationRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundInformation> objs = new List<UtmcFundInformation>();
            try
            {
                response = IUtmcFundInformationRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcFundInformationRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundInformation> objs = new List<UtmcFundInformation>();
            try
            {
                response = IUtmcFundInformationRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcFundInformationRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public List<FundDetailedInformation> GetFundDetailedInformation()
        {
            //NEW
            //string propertyNameCorporateActionDate = nameof(UtmcFundCorporateAction.CorporateActionDate);
            //NEW
            List<FundDetailedInformation> fundDetailedInformations = new List<FundDetailedInformation>();
            try
            {
                Response responseUFIList = GetDataByFilter(" 1=1 ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    List<UtmcFundInformation> UtmcFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                    string propertyNameIpdFundCode = nameof(UtmcDailyNavFund.IpdFundCode);
                    UtmcFundInformations.ForEach(utmcFundInformation =>
                    {
                        FundDetailedInformation FundDetailedInformation = new FundDetailedInformation();
                        FundDetailedInformation.UtmcFundInformation = utmcFundInformation;
                        Response responseUFCAList = IUtmcFundCorporateActionRepo.GetDataByPropertyName(propertyNameIpdFundCode, utmcFundInformation.IpdFundCode, true, 0, 0, false);
                        if (responseUFCAList.IsSuccess)
                        {
                            FundDetailedInformation.UtmcFundCorporateActions = (List<UtmcFundCorporateAction>)responseUFCAList.Data;
                        }
                        else
                        {
                            Logger.WriteLog("responseUFCAList: " + responseUFCAList.Message + " - Exception");
                        }
                        Response responseUDNFList = IUtmcDailyNavFundRepo.GetDataByPropertyName(propertyNameIpdFundCode, utmcFundInformation.IpdFundCode, true, 0, 0, false);
                        if (responseUDNFList.IsSuccess)
                        {
                            List<UtmcDailyNavFund> UtmcDailyNavFunds = (List<UtmcDailyNavFund>)responseUDNFList.Data;
                            FundDetailsAfterDividend fundDetailsAfterDividend = new FundDetailsAfterDividend();
                            List<UtmcDailyNavFundAfterDividend> utmcDailyNavFundAfterDividends = new List<UtmcDailyNavFundAfterDividend>();

                            if (UtmcDailyNavFunds.Count > 0)
                            {
                                // Changes Per Day
                                List<UtmcDailyNavFund> UtmcDailyNavFundsRev = UtmcDailyNavFunds.OrderByDescending(x => x.DailyNavDate).ToList();
                                List<UtmcDailyNavFundWithChange> UtmcDailyNavFundWithChangesRev = (from a in UtmcDailyNavFundsRev
                                                                                                   select new UtmcDailyNavFundWithChange
                                                                                                   {
                                                                                                       UtmcDailyNavFund = a
                                                                                                   }).ToList();


                                List<UtmcDailyNavFundWithChange> UtmcDailyNavFundsWithChanges = Enumerable
                                                                .Range(1, UtmcDailyNavFundWithChangesRev.Count - 1)
                                                                .Select(i =>
                                                                {
                                                                    UtmcDailyNavFundWithChangesRev[i - 1].Change = Math.Round(UtmcDailyNavFundWithChangesRev[i - 1].UtmcDailyNavFund.DailyUnitPrice - UtmcDailyNavFundWithChangesRev[i].UtmcDailyNavFund.DailyUnitPrice, 4);
                                                                    UtmcDailyNavFundWithChangesRev[i - 1].ChangePer = Math.Round(((UtmcDailyNavFundWithChangesRev[i - 1].UtmcDailyNavFund.DailyUnitPrice - UtmcDailyNavFundWithChangesRev[i].UtmcDailyNavFund.DailyUnitPrice) / UtmcDailyNavFundWithChangesRev[i - 1].UtmcDailyNavFund.DailyUnitPrice) * 100, 2);
                                                                    return UtmcDailyNavFundWithChangesRev[i - 1];
                                                                })
                                                                .ToList();
                                UtmcDailyNavFundsWithChanges.Add(UtmcDailyNavFundWithChangesRev.Last());
                                //FundDetailedInformation.UtmcDailyNavFunds = UtmcDailyNavFunds;
                                FundDetailedInformation.UtmcDailyNavFundsWithChanges = UtmcDailyNavFundsWithChanges;

                                //Changes Per Month
                                UtmcDailyNavFundWithChangesRev = UtmcDailyNavFundsWithChanges
                                            .GroupBy(x => new
                                            {
                                                x.UtmcDailyNavFund.DailyNavDate.Year,
                                                x.UtmcDailyNavFund.DailyNavDate.Month
                                            })
                                            .Select(g =>
                                            {
                                                DateTime maxDate = g.Max(x => x.UtmcDailyNavFund.DailyNavDate);
                                                return g.Where(x => x.UtmcDailyNavFund.DailyNavDate == maxDate).FirstOrDefault();
                                            }).OrderByDescending(x => x.UtmcDailyNavFund.DailyNavDate).ToList();

                                List<UtmcDailyNavFundWithChange> UtmcDailyNavFundsMonthEnds = Enumerable
                                                                .Range(1, UtmcDailyNavFundWithChangesRev.Count - 1)
                                                                .Select(i =>
                                                                {
                                                                    UtmcDailyNavFundWithChangesRev[i - 1].Change = Math.Round(UtmcDailyNavFundWithChangesRev[i - 1].UtmcDailyNavFund.DailyUnitPrice - UtmcDailyNavFundWithChangesRev[i].UtmcDailyNavFund.DailyUnitPrice, 4);
                                                                    UtmcDailyNavFundWithChangesRev[i - 1].ChangePer = Math.Round(((UtmcDailyNavFundWithChangesRev[i - 1].UtmcDailyNavFund.DailyUnitPrice - UtmcDailyNavFundWithChangesRev[i].UtmcDailyNavFund.DailyUnitPrice) / UtmcDailyNavFundWithChangesRev[i].UtmcDailyNavFund.DailyUnitPrice) * 100, 2);
                                                                    return UtmcDailyNavFundWithChangesRev[i - 1];
                                                                })
                                                                .ToList();
                                UtmcDailyNavFundsMonthEnds.Add(UtmcDailyNavFundWithChangesRev.Last());
                                //FundDetailedInformation.UtmcDailyNavFundsMonthEnd = UtmcDailyNavFundsMonthEnds;
                                FundDetailedInformation.UtmcDailyNavFundWithChangeMonthEnd = UtmcDailyNavFundsMonthEnds;
                                // Set Latest Updates
                                FundDetailedInformation.CurrentNavDate = UtmcDailyNavFundsMonthEnds.FirstOrDefault().UtmcDailyNavFund.DailyNavDate;
                                FundDetailedInformation.Change = UtmcDailyNavFundsMonthEnds.FirstOrDefault().Change;
                                FundDetailedInformation.ChangePercent = UtmcDailyNavFundsMonthEnds.FirstOrDefault().ChangePer;
                                FundDetailedInformation.CurrentUnitPrice = Math.Round(UtmcDailyNavFundsMonthEnds.FirstOrDefault().UtmcDailyNavFund.DailyUnitPrice, 4);
                                FundDetailedInformation.CurrentPrice = Math.Round(UtmcDailyNavFundsMonthEnds.FirstOrDefault().UtmcDailyNavFund.DailyNav, 4);


                                List<UtmcDailyNavFund> utmcDailyNavFunds = UtmcDailyNavFunds.OrderBy(x => x.DailyNavDate).ToList();

                                Decimal InitialUnitPrice = utmcDailyNavFunds.FirstOrDefault().DailyUnitPrice;
                                Decimal Distributions = 1;
                                utmcDailyNavFundAfterDividends.Add(new UtmcDailyNavFundAfterDividend
                                {
                                    ReportDate = utmcDailyNavFunds.FirstOrDefault().ReportDate,
                                    DailyNavDate = utmcDailyNavFunds.FirstOrDefault().DailyNavDate,
                                    DailyNavUnitPrice = utmcDailyNavFunds.FirstOrDefault().DailyUnitPrice,
                                    Distribution = 1,
                                    NavAfterDividend = utmcDailyNavFunds.FirstOrDefault().DailyUnitPrice,
                                    Changes = 0
                                });

                                Response responseUFCAList2 = IUtmcFundCorporateActionRepo.GetDataByPropertyName(propertyNameIpdFundCode, utmcFundInformation.IpdFundCode, true, 0, 0, false);
                                if (responseUFCAList2.IsSuccess)
                                {
                                    List<UtmcFundCorporateAction> UtmcFundCorporateActions = (List<UtmcFundCorporateAction>)responseUFCAList2.Data;
                                    Enumerable
                                    .Range(1, utmcDailyNavFunds.Count - 1)
                                    .Select(i =>
                                    {
                                        Decimal DailyUnitPrice = utmcDailyNavFunds[i].DailyUnitPrice;
                                        DateTime DailyNavDate = utmcDailyNavFunds[i].DailyNavDate;
                                        UtmcFundCorporateAction utmcFundCorporateAction = UtmcFundCorporateActions.Where(x => x.CorporateActionDate.ToString("yyyy-MM-dd") == utmcDailyNavFunds[i].DailyNavDate.ToString("yyyy-MM-dd")).FirstOrDefault();
                                        if (utmcFundCorporateAction != null)
                                            Distributions = Math.Round((utmcDailyNavFundAfterDividends[i - 1].Distribution * (1 + utmcFundCorporateAction.Distributions / DailyUnitPrice)), 14);
                                        Decimal NavAfterDividend = (DailyUnitPrice / utmcDailyNavFundAfterDividends[i - 1].DailyNavUnitPrice) * (Distributions / utmcDailyNavFundAfterDividends[i - 1].Distribution) * utmcDailyNavFundAfterDividends[i - 1].NavAfterDividend;
                                        Decimal Changes = ((NavAfterDividend / InitialUnitPrice) - 1) * 100;
                                        utmcDailyNavFundAfterDividends.Add(new UtmcDailyNavFundAfterDividend
                                        {
                                            ReportDate = utmcDailyNavFunds[i].ReportDate,
                                            DailyNavUnitPrice = DailyUnitPrice,
                                            DailyNavDate = DailyNavDate,
                                            Distribution = Distributions,
                                            NavAfterDividend = NavAfterDividend,
                                            Changes = Changes,
                                            Days = i
                                        });
                                        return utmcDailyNavFunds[i - 1];
                                    })
                                    .ToList();
                                    fundDetailsAfterDividend.utmcDailyNavFundAfterDividends = utmcDailyNavFundAfterDividends;
                                    fundDetailsAfterDividend.Return = utmcDailyNavFundAfterDividends.LastOrDefault().Changes;
                                    fundDetailsAfterDividend.Average = (fundDetailsAfterDividend.Return * 365) / utmcDailyNavFundAfterDividends.LastOrDefault().Days;
                                }
                                else
                                {
                                    Logger.WriteLog("responseUFCAList2: " + responseUFCAList2.Message + " - Exception");
                                }
                            }
                            FundDetailedInformation.FundDetailsAfterDividend = fundDetailsAfterDividend;
                            fundDetailedInformations.Add(FundDetailedInformation);
                        }
                        else
                        {
                            Logger.WriteLog("responseUDNFList: " + responseUDNFList.Message + " - Exception");
                        }

                    });
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(" UtmcFundInformationService GetFundDetailedInformation: " + ex.Message + " - Exception");
            }
            return fundDetailedInformations;
        }
        public List<FundDetailedInformation> GetDataTableDataFundDetail(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;
            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            List<FundDetailedInformation> FundDetailedInformationSearch = new List<FundDetailedInformation>();
            List<FundDetailedInformation> FundDetailedInformations = GetFundDetailedInformation();
            if (String.IsNullOrWhiteSpace(searchBy) == false)
            {
                var searchTerms = searchBy.Split(' ').ToList().ConvertAll(x => x.ToLower());
                //UtmcFundInformationSearch.AddRange(UtmcFundInformations.Where(s => searchTerms.Any(srch => s.Name1.ToLower().Contains(srch))));
            }
            if (FundDetailedInformationSearch.Count == 0)
                FundDetailedInformationSearch = FundDetailedInformations;
            FundDetailedInformationSearch = (sortBy.Contains(".")) ?
                sortDir ? FundDetailedInformationSearch.OrderBy(x => typeof(FundDetailedInformation).GetProperty(sortBy.Split('.')[0]).PropertyType.GetProperty(sortBy.Split('.')[1]).GetValue(x.UtmcFundInformation)).ToList() : FundDetailedInformationSearch.OrderByDescending(x => typeof(FundDetailedInformation).GetProperty(sortBy.Split('.')[0]).PropertyType.GetProperty(sortBy.Split('.')[1]).GetValue(x.UtmcFundInformation)).ToList()
            :
                sortDir ? FundDetailedInformationSearch.OrderBy(x => typeof(FundDetailedInformation).GetProperty(sortBy).GetValue(x)).ToList() : FundDetailedInformationSearch.OrderByDescending(x => typeof(FundDetailedInformation).GetProperty(sortBy).GetValue(x)).ToList();
            var result =
                FundDetailedInformationSearch
                    .Skip(skip)
                    .Take(take)
                    .ToList();
            //Calculate
            filteredResultsCount = FundDetailedInformationSearch.Count();
            totalResultsCount = FundDetailedInformations.Count();
            if (result == null)
            {
                return new List<FundDetailedInformation>();
            }
            return result;
        }
        public FundDetailsAfterDividend GetFundChartInfo(FundDetailedInformation fundDetailedInformation, DateTime startDate, DateTime endDate)
        {

            string propertyNameIpdFundCode = nameof(UtmcDailyNavFund.IpdFundCode);
            string propertyNameCorporateActionDate = nameof(UtmcFundCorporateAction.CorporateActionDate);

            FundDetailsAfterDividend fundDetailsAfterDividend = new FundDetailsAfterDividend();
            try
            {
                Response responseUDNFList = IUtmcDailyNavFundRepo.GetDataByPropertyName(propertyNameIpdFundCode, fundDetailedInformation.UtmcFundInformation.IpdFundCode, true, 0, 0, false);
                if (responseUDNFList.IsSuccess)
                {
                    List<UtmcDailyNavFund> UtmcDailyNavFunds = (List<UtmcDailyNavFund>)responseUDNFList.Data;

                    //NEW
                    List<UtmcDailyNavFundAfterDividend> utmcDailyNavFundAfterDividends = new List<UtmcDailyNavFundAfterDividend>();
                    DateTime dateStart = startDate;
                    DateTime dateLimit = endDate;
                    List<UtmcDailyNavFund> utmcDailyNavFunds =
                        UtmcDailyNavFunds
                        .OrderBy(x => x.DailyNavDate)
                        .Where(x =>
                            x.DailyNavDate >= dateStart &&
                            x.DailyNavDate <= dateLimit)
                        .ToList();
                    if (utmcDailyNavFunds.Count != 0)
                    {
                        Decimal InitialUnitPrice = utmcDailyNavFunds.FirstOrDefault().DailyUnitPrice;
                        Decimal Distributions = 1;
                        utmcDailyNavFundAfterDividends.Add(new UtmcDailyNavFundAfterDividend
                        {
                            ReportDate = utmcDailyNavFunds.FirstOrDefault().ReportDate,
                            DailyNavDate = utmcDailyNavFunds.FirstOrDefault().DailyNavDate,
                            DailyNavUnitPrice = utmcDailyNavFunds.FirstOrDefault().DailyUnitPrice,
                            Distribution = 1,
                            NavAfterDividend = utmcDailyNavFunds.FirstOrDefault().DailyUnitPrice,
                            Changes = 0
                        });
                        Logger.WriteLog(" UtmcFundInformationService GetFundChartInfo: " + fundDetailedInformation.UtmcFundInformation.IpdFundCode);
                        Enumerable
                        .Range(1, utmcDailyNavFunds.Count - 1)
                        .Select(i =>
                        {
                            Decimal DailyUnitPrice = utmcDailyNavFunds[i].DailyUnitPrice;
                            DateTime DailyNavDate = utmcDailyNavFunds[i].DailyNavDate;
                            Response responseUFCAList = IUtmcFundCorporateActionRepo.GetDataByPropertyName(propertyNameCorporateActionDate, utmcDailyNavFunds[i].DailyNavDate.ToString("yyyy-MM-dd"), true, 0, 0, false);
                            if (responseUFCAList.IsSuccess)
                            {
                                UtmcFundCorporateAction utmcFundCorporateAction = ((List<UtmcFundCorporateAction>)responseUFCAList.Data).Where(x => x.IpdFundCode == fundDetailedInformation.UtmcFundInformation.IpdFundCode).FirstOrDefault();
                                if (utmcFundCorporateAction != null)
                                    Distributions = Math.Round((utmcDailyNavFundAfterDividends[i - 1].Distribution * (1 + utmcFundCorporateAction.Distributions / DailyUnitPrice)), 14);
                                Decimal NavAfterDividend = (DailyUnitPrice / utmcDailyNavFundAfterDividends[i - 1].DailyNavUnitPrice) * (Distributions / utmcDailyNavFundAfterDividends[i - 1].Distribution) * utmcDailyNavFundAfterDividends[i - 1].NavAfterDividend;
                                Decimal Changes = ((NavAfterDividend / InitialUnitPrice) - 1) * 100;
                                utmcDailyNavFundAfterDividends.Add(new UtmcDailyNavFundAfterDividend
                                {
                                    ReportDate = utmcDailyNavFunds[i].ReportDate,
                                    DailyNavUnitPrice = DailyUnitPrice,
                                    DailyNavDate = DailyNavDate,
                                    Distribution = Distributions,
                                    NavAfterDividend = NavAfterDividend,
                                    Changes = Changes,
                                    Days = i
                                });
                            }
                            return utmcDailyNavFunds[i - 1];
                        })
                        .ToList();
                        Logger.WriteLog(" UtmcFundInformationService GetFundChartInfo utmcDailyNavFundAfterDividends.LastOrDefault().Days: " + utmcDailyNavFundAfterDividends.LastOrDefault().Days);
                        if (utmcDailyNavFundAfterDividends.LastOrDefault().Days != 0)
                        {
                            fundDetailsAfterDividend.utmcDailyNavFundAfterDividends = utmcDailyNavFundAfterDividends;
                            fundDetailsAfterDividend.Return = utmcDailyNavFundAfterDividends.LastOrDefault().Changes;
                            fundDetailsAfterDividend.Average = (fundDetailsAfterDividend.Return * 365) / utmcDailyNavFundAfterDividends.LastOrDefault().Days;
                        }
                        //NEW
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(" UtmcFundInformationService GetFundChartInfo: " + ex.Message + " - Exception");
            }
            return fundDetailsAfterDividend;
        }
        public List<FundChartInformation> GenerateFundChartInfo(Int32[] selectedFunds, Int32 dataValue)
        {
            List<FundChartInformation> FundChartInformations = new List<FundChartInformation>();
            try
            {
                DisplayChartBy dcb = (DisplayChartBy)dataValue == 0 ? DisplayChartBy.SixMonth : (DisplayChartBy)dataValue;
                DateTime currentDate = DateTime.Now;
                DateTime chartDateFrom = DateTime.Now;
                DateTime chartDateTo = DateTime.Now;

                List<FundDetailedInformation> FundDetailedInformations = GetFundDetailedInformation();
                foreach (Int32 fundId in selectedFunds)
                {
                    FundDetailedInformation FundDetailedInformation = FundDetailedInformations.Where(x => x.UtmcFundInformation.Id == fundId).FirstOrDefault();

                    currentDate = FundDetailedInformation.CurrentNavDate;
                    switch (dcb)
                    {
                        case DisplayChartBy.YTD:
                            chartDateFrom = new DateTime(currentDate.Year, 1, 1);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.LastYear:
                            chartDateFrom = new DateTime(currentDate.Year - 1, 1, 1);
                            chartDateTo = new DateTime(currentDate.Year - 1, 12, 31);
                            break;
                        case DisplayChartBy.OneWeek:
                            chartDateFrom = currentDate.AddDays(-7);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.OneMonth:
                            chartDateFrom = currentDate.AddMonths(-1);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.ThreeMonth:
                            chartDateFrom = currentDate.AddMonths(-3);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.SixMonth:
                            chartDateFrom = currentDate.AddMonths(-6);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.OneYear:
                            chartDateFrom = currentDate.AddYears(-1);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.TwoYear:
                            chartDateFrom = currentDate.AddYears(-2);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.ThreeYear:
                            chartDateFrom = currentDate.AddYears(-3);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.FiveYear:
                            chartDateFrom = currentDate.AddYears(-5);
                            chartDateTo = currentDate;
                            break;
                        case DisplayChartBy.TenYear:
                            chartDateFrom = currentDate.AddYears(-10);
                            chartDateTo = currentDate;
                            break;
                    }


                    FundDetailedInformation.FundDetailsAfterDividend = GetFundChartInfo(FundDetailedInformation, chartDateFrom, chartDateTo);

                    List<string> dataX = new List<string>();
                    List<string> navDates = new List<string>();
                    List<Decimal> Labels = new List<Decimal>();
                    List<Decimal> changePers = new List<Decimal>();
                    List<Decimal> navPrices = new List<Decimal>();
                    List<Decimal> Values = new List<Decimal>();
                    if (FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends != null)
                    {
                        if ((Int32)dcb >= (Int32)DisplayChartBy.ThreeYear)
                        {
                            FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends = FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.GroupBy(x => x.ReportDate).Select(grp => grp.ToList().OrderByDescending(y => y.DailyNavDate).FirstOrDefault()).ToList();
                        }
                        dataX.AddRange(
                            FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Select(x => x.Days.ToString()).ToList()
                        );
                        dataX
                            .ForEach(x =>
                            {
                                changePers
                                    .AddRange(
                                      FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends
                                      .Where(z => z.Days.ToString() == x)
                                      .Select(z => z.Changes)
                                      .ToList()
                                  );
                                navPrices
                                    .AddRange(
                                      FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends
                                      .Where(z => z.Days.ToString() == x)
                                      .Select(z => z.DailyNavUnitPrice)
                                      .ToList()
                                  );
                                navDates
                                    .AddRange(
                                      FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends
                                      .Where(z => z.Days.ToString() == x)
                                      .Select(z => z.DailyNavDate.ToString("yyyy-MM-dd"))
                                      .ToList()
                                  );
                                Values
                                    .AddRange(
                                      FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends
                                      .Where(z => z.Days.ToString() == x)
                                      .Select(z => z.Changes)
                                      .ToList()
                                  );
                            });
                    }
                    FundChartInformations.Add(new FundChartInformation
                    {
                        fundId = fundId,
                        fundName = FundDetailedInformation.UtmcFundInformation.FundName.Capitalize(),
                        currentNav = FundDetailedInformation.CurrentUnitPrice,
                        currentNavDate = FundDetailedInformation.CurrentNavDate.ToString("yyyy-MM-dd"),
                        minChange = Math.Round(FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Max(x => x.Changes)),
                        maxChange = Math.Round(FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Min(x => x.Changes)),
                        maxNav = FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Max(x => x.DailyNavUnitPrice),
                        minNav = FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Min(x => x.DailyNavUnitPrice),
                        Labels = dataX,
                        totalReturns = Math.Round(FundDetailedInformation.FundDetailsAfterDividend.Return, 4),
                        average = Math.Round((FundDetailedInformation.FundDetailsAfterDividend.Return / FundDetailedInformation.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Last().Days), 4),
                        Values = Values.Select(x => { return Math.Round(x, 4).ToString(); }).ToList(),
                        NavPrices = navPrices.Select(x => x.ToString()).ToList(),
                        NavDates = navDates
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(" UtmcFundInformationService GenerateFundChartInfo: " + ex.Message + " - Exception");

            }
            return FundChartInformations;
        }

    }
}
