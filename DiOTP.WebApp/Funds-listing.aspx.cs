﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.WebApp.ServiceCalls;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiOTP.PolicyAndRules;
using static DiOTP.PolicyAndRules.FundPolicy;

namespace DiOTP.WebApp
{
    public partial class Funds_listing : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyfdObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyfdObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if(Session["user"] != null)
                {
                    homeLink.HRef = "/Portfolio.aspx";
                }
                if (!IsPostBack)
                {
                    Response responseFunds = (Response)FundPolicy.Get(new FundPolicy.FundClass { Step = 1, httpContext = Context });

                    if (responseFunds.IsSuccess)
                    {
                        ResponseFundStep1 responseFundStep1 = (ResponseFundStep1)responseFunds.Data;
                        FundPolicyEnum fundPolicyEnum = (FundPolicyEnum)Enum.Parse(typeof(FundPolicyEnum), responseFundStep1.Code);
                        string message = fundPolicyEnum.ToDescriptionString();
                        if (message == "SUCCESS")
                        {
                            fundListTableBody.InnerHtml = responseFundStep1.utmcFundInformationsTbodyString;
                        }
                        else if (message == "EXCEPTION")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseFundStep1.Message + "','');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + message + "','');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseFunds.Message + "','');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnFundDetails_Click(object sender, EventArgs e)
        {
            Int32 FundID = Convert.ToInt32(hdnFundID.Value);
            Session["FundID"] = FundID;
            Response.Redirect("Fund-Information.aspx", false);
        }
        

        public class DataTables
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public object data { get; set; }
        }


    }
}
