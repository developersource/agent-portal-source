﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddInstructor.aspx.cs" Inherits="Admin.AddInstructor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="addInstructorForm" runat="server">
        <div class="row" id="FormId" data-value="addInstructorForm">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Username:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                        <input type="text" name="Name" id="Name" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                    </div>
                </div>
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Email:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="EmailId" id="EmailId" runat="server" clientidmode="static" class="form-control" placeholder="Enter Email" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Mobile number:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="MobileNumber" id="MobileNumber" runat="server" clientidmode="static" class="form-control" placeholder="Enter Mobile number" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>
