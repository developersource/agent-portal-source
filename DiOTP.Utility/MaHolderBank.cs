using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ma_holder_bank")]
    public class MaHolderBank
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("is_primary"), Display("IsPrimary")]
        public Int32 IsPrimary { get; set; }
        [Database("bank_def_id"), Display("BankDefId")]
        public Int32 BankDefId { get; set; }
        [Database("account_name"), Display("AccountName")]
        public String AccountName { get; set; }
        [Database("bank_account_no"), Display("BankAccountNo")]
        public String BankAccountNo { get; set; }
        [Database("remarks"), Display("Remarks")]
        public String Remarks { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("version"), Display("Version")]
        public Int32 Version { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Join("id")]
        public BanksDef BankDefIdBanksDef { get; set; }
        public User UserIdUser { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("image"), Display("Image")]
        public string Image { get; set; }
    }
}
