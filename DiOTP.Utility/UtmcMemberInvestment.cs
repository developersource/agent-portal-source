using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_member_investment")]
    public class UtmcMemberInvestment
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("member_epf_no"), Display("MemberEpfNo")]
        public String MemberEpfNo { get; set; }
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("ipd_member_acc_no"), Display("IpdMemberAccNo")]
        public String IpdMemberAccNo { get; set; }
        [Database("actual_transferred_from_epf_rm"), Display("ActualTransferredFromEpfRm")]
        public Decimal ActualTransferredFromEpfRm { get; set; }
        [Database("actual_cost"), Display("ActualCost")]
        public Decimal ActualCost { get; set; }
        [Database("units"), Display("Units")]
        public Decimal Units { get; set; }
        [Database("book_value"), Display("BookValue")]
        public Decimal BookValue { get; set; }
        [Database("market_value"), Display("MarketValue")]
        public Decimal MarketValue { get; set; }
        [Database("effective_date"), Display("EffectiveDate")]
        public DateTime EffectiveDate { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime ReportDate { get; set; }
        [Database("redemption_cost"), Display("RedemptionCost")]
        public Decimal? RedemptionCost { get; set; }
        [Database("isactive"), Display("Isactive")]
        public String Isactive { get; set; }
        [Database("Current_Unit_Holding_Value"), Display("CurrentUnitHoldingValue")]
        public Decimal? CurrentUnitHoldingValue { get; set; }

        public String TransString { get; set; }
        public String TransType { get; set; }
        public String AccType { get; set; }
        public String EntryDt { get; set; }
        public String TransDt { get; set; }
        public String TransUnits { get; set; }
        public String TransAmt { get; set; }
        public String TransPR { get; set; }
        public String TransMarketValue { get; set; }
        public String TransUnitsXTransPR { get; set; }
        public String CurUnitHldg { get; set; }
        public String CumValueHldg { get; set; }
    }
}
