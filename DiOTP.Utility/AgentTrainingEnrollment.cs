﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_training_enrollments")]
    public class AgentTrainingEnrollment
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("agent_training_topic_id"), Display("AgentTrainingTopicId")]
        public Int32 AgentTrainingTopicId { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("is_attended"), Display("IsAttended")]
        public Int32 IsAttended { get; set; }
        [Database("is_passed"), Display("IsPassed")]
        public Int32 IsPassed { get; set; }
        [Database("is_reported"), Display("IsReported")]
        public Int32 IsReported { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32 UpdatedBy { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public Int32 IsAssessment { get; set; }
    }

}
