using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_fund_details")]
    public class UtmcFundDetail
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("utmc_fund_information_id"), Display("UtmcFundInformationId")]
        public Int32 UtmcFundInformationId { get; set; }
        [Database("launch_date"), Display("LaunchDate")]
        public DateTime LaunchDate { get; set; }
        [Database("relaunch_date"), Display("RelaunchDate")]
        public DateTime? RelaunchDate { get; set; }
        [Database("launch_price"), Display("LaunchPrice")]
        public Decimal LaunchPrice { get; set; }
        [Database("pricing_basis"), Display("PricingBasis")]
        public String PricingBasis { get; set; }
        [Database("investment_objective"), Display("InvestmentObjective")]
        public String InvestmentObjective { get; set; }
        [Database("investment_strategy_and_policy"), Display("InvestmentStrategyAndPolicy")]
        public String InvestmentStrategyAndPolicy { get; set; }
        [Database("latest_nav_price"), Display("LatestNavPrice")]
        public Decimal LatestNavPrice { get; set; }
        [Database("latest_nav_date"), Display("LatestNavDate")]
        public DateTime? LatestNavDate { get; set; }
        [Database("historical_income_distribution"), Display("HistoricalIncomeDistribution")]
        public Int32 HistoricalIncomeDistribution { get; set; }
        [Database("is_epf_approved"), Display("IsEpfApproved")]
        public Int32 IsEpfApproved { get; set; }
        [Database("shariah_compliant"), Display("ShariahCompliant")]
        public Int32 ShariahCompliant { get; set; }
        [Database("risk_rating"), Display("RiskRating")]
        public String RiskRating { get; set; }
        [Database("fund_size_rm"), Display("FundSizeRm")]
        public String FundSizeRm { get; set; }
        [Database("min_initial_investment_cash"), Display("MinInitialInvestmentCash")]
        public Int32 MinInitialInvestmentCash { get; set; }
        [Database("min_initial_investment_epf"), Display("MinInitialInvestmentEpf")]
        public Int32 MinInitialInvestmentEpf { get; set; }
        [Database("min_subsequent_investment_cash"), Display("MinSubsequentInvestmentCash")]
        public Int32 MinSubsequentInvestmentCash { get; set; }
        [Database("min_subsequent_investment_epf"), Display("MinSubsequentInvestmentEpf")]
        public Int32 MinSubsequentInvestmentEpf { get; set; }
        [Database("min_rsp_investment_initial_rm"), Display("MinRspInvestmentInitialRm")]
        public Int32 MinRspInvestmentInitialRm { get; set; }
        [Database("min_rsp_investment_additional_rm"), Display("MinRspInvestmentAdditionalRm")]
        public Int32 MinRspInvestmentAdditionalRm { get; set; }
        [Database("min_red_amount_units"), Display("MinRedAmountUnits")]
        public Int32 MinRedAmountUnits { get; set; }
        [Database("min_holding_units"), Display("MinHoldingUnits")]
        public Int32 MinHoldingUnits { get; set; }
        [Database("colling_off_period"), Display("CollingOffPeriod")]
        public String CollingOffPeriod { get; set; }
        [Database("distribution_policy"), Display("DistributionPolicy")]
        public String DistributionPolicy { get; set; }
        public UtmcFundInformation UtmcFundInformationIdUtmcFundInformation { get; set; }
    }
}
