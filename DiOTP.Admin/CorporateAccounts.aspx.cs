﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using DiOTP.Utility.Helper;
using DiOTP.Utility.CustomClasses;
using System.Configuration;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text.RegularExpressions;
using Admin.ServiceCalls;
using System.IO;
using OfficeOpenXml;

namespace Admin
{
    public partial class CorporateAccounts : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append(" 1=1 ");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameUsername = Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.Username));
                    filter.Append(" and a." + columnNameUsername + " like '%" + Search.Value.Trim() + "%'");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and a.status in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(" and a.status in (0,1)");
                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    Response responseCount = GenericService.GetCountByQuery("select count(*) from (select count(a.id) from users a inner join user_accounts b on  b.user_id=a.id and b.holder_class in ('RC', 'BC', 'NC', 'FC')  where  1=1  and a.status in (0,1) group by a.id) a");
                    
                    hdnTotalRecordsCount.Value = responseCount.Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseCUList = GenericService.GetDataByJoin<User>(filter.ToString(), typeof(UserAccount), " b.user_id=a.id and b.holder_class in ('RC', 'BC', 'NC', 'FC') ", skip, take, false, null, true, null, true, true, nameof(DiOTP.Utility.User.Id));
                if (responseCUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseCUList.Data;

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (User u in users)
                    {
                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        Response responseUA = GenericService.GetDataByFilter<UserAccount>(" user_id='" + u.Id + "' ", 0, 0, false, null, true, null, false, false, null);
                        if (responseUA.IsSuccess)
                        {
                            UserAccount userAccount = ((List<UserAccount>)responseUA.Data).FirstOrDefault();

                            asb.Append(@"<tr>
                                <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + u.Id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (u.Status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                                </td>
                                <td>
                                    <table class='table'>
                                        <tbody>
                                            <tr>
                                                <td style='width:50%;'>Name</td>
                                                <td><strong><a href='javascript:;' data-id='" + u.Id + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='UserDetails.aspx' data-title='View details' data-action='View'>" + u.Username + @"&nbsp;<i class='fa fa-eye'></i></a></strong></td>
                                            </tr>
                                            <tr>
                                                <td>Mobile number</td>
                                                <td><strong>" + u.MobileNumber + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Registered Date</td>
                                                <td><strong>" + (u.CreatedDate == null ? "" : u.CreatedDate.ToString("dd/MM/yyyy")) + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>ID No</td>
                                                <td><strong>" + u.IdNo + @"</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table class='table'>
                                        <tbody>
                                            <tr>
                                                <td style='width:50%;'>MA Acc No</td>
                                                <td><strong>" + userAccount.AccountNo + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><strong>" + u.EmailId + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Last login</td>
                                                <td><strong>" + (u.LastLoginDate == null ? "" : u.LastLoginDate.Value.ToString("dd/MM/yyyy HH:mm:ss")) + @"</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <a href='" + siteURL + "ImpersonateUser.aspx?id=" + CustomEncryptorDecryptor.EncryptText(u.Id.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff")) + @"' target='_blank' class='btn btn-sm btn-primary'>Impersonate User</a>
                                </td>
                            </tr>");
                            index++;
                        }
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            try
            {
                string idString = String.Join(",", ids);
                Response responseUList = IUserService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUList.Data;
                    if (action == "Deactivate")
                    {
                        users.ForEach(x =>
                        {
                            x.Status = 0;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = (x.Username) + " " + "(" + x.IdNo + ") Corporate Account deactivation Successful",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                    if (action == "Activate")
                    {
                        users.ForEach(x =>
                        {
                            x.Status = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = (x.Username) + " Corporate Account activation Successful",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                    if (action == "ResendLink")
                    {
                        users.ForEach(x =>
                        {
                            var unique_code = CustomGenerator.GenerateSixDigitPin();
                            x.EmailCode = unique_code;
                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                            Email email = new Email
                            {
                                user = x,
                                link = siteURL + "ActivateAccount.aspx",
                            };
                            EmailService.SendVerificationLink(email, unique_code);
                        });
                        IUserService.UpdateBulkData(users);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Corporate User accounts action: " + ex.Message);
                return false;
            }
        }

        public static Response IsValidPhoneNumber(string phoneNumber)
        {
            Response response = new Response();
            try
            {
                //will match +61 or +66- or 0 or nothing followed by a nine digit number
                bool isValid = Regex.Match(phoneNumber,
                    @"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$").Success;
                //to vary this, replace 61 with an international code of your choice 
                //or remove [\+]?61[-]? if international code isn't needed
                //{8} is the number of digits in the actual phone number less one
                if (isValid)
                {
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid mobile number";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(User obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();

            if (obj.EmailId == "")
            {
                response.IsSuccess = false;
                response.Message = "Email cannot be empty";
                return response;
            }
            else
            {
                if (!CustomValidator.IsValidEmail(obj.EmailId))
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid email";
                    return response;
                }
            }
            if (obj.MobileNumber == "")
            {
                response.IsSuccess = false;
                response.Message = "Mobile number cannot be empty";
                return response;
            }
            else
            {
                response = IsValidPhoneNumber(obj.MobileNumber);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }


            Int32 LoginUserId = 0;
            if (HttpContext.Current.Session["admin"] != null)
            {
                User sessionUser = (User)HttpContext.Current.Session["admin"];
                LoginUserId = sessionUser.Id;
            }
            try
            {
                if (obj.Id == 0)
                {
                    StringBuilder filter = new StringBuilder();
                    filter.Append(" 1=1");
                    filter.Append(" and " + Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.EmailId)) + " = '" + obj.EmailId + "'");
                    Response responseUList = IUserService.GetDataByFilter(filter.ToString(), 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        List<User> userMatches = (List<User>)responseUList.Data;
                        if (userMatches.Count > 0)
                        {
                            response.IsSuccess = false;
                            response.Message = "Account with this email already exists.";
                            return response;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseUList.Message;
                        return response;
                    }

                    if (obj.LastLoginIp == "")
                    {
                        response.IsSuccess = false;
                        response.Message = "MA No. cannot be empty";
                        return response;
                    }
                    else
                    {
                        Regex regex = new Regex(@"^\d+$");
                        if (regex.IsMatch(obj.LastLoginIp.Trim()))
                        {
                            
                            {
                                
                                {
                                    Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(obj.LastLoginIp);
                                    if (responseMA.IsSuccess)
                                    {
                                        MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                        if (obj.RegisterIp == "")
                                        {
                                            response.IsSuccess = false;
                                            response.Message = "Company ID cannot be empty";
                                            return response;
                                        }
                                        else
                                        {
                                            if (maHolderReg == null)
                                            {
                                                response.IsSuccess = false;
                                                response.Message = "No MA records";
                                                return response;
                                            }
                                            obj.RegisterIp = obj.RegisterIp.Replace("-", "");
                                            obj.RegisterIp = obj.RegisterIp.Replace(" ", "");
                                            maHolderReg.IdNo = maHolderReg.IdNo.Replace("-", "");
                                            maHolderReg.IdNo = maHolderReg.IdNo.Replace(" ", "");
                                            if (maHolderReg.IdNo != obj.RegisterIp.Trim())
                                            {
                                                response.IsSuccess = false;
                                                response.Message = "Invalid MA Number or Company ID";
                                                return response;
                                            }
                                            else if (maHolderReg.HolderStatus == "I" || maHolderReg.HolderStatus == "S")
                                            {
                                                response.IsSuccess = false;
                                                response.Message = (maHolderReg.HolderStatus == "I" ? "MA Inactive." : (maHolderReg.HolderStatus == "S" ? "MA Suspended." : ""));
                                                return response;
                                            }
                                            else if(CustomValues.GetAccounPlan(maHolderReg.HolderCls) != "CORP")
                                            {
                                                response.IsSuccess = false;
                                                response.Message = "Not a Corporate account";
                                                return response;
                                            }
                                            else
                                            {
                                                Response responseCount = GenericService.GetCountByQuery("select count(*) from users where REPLACE(REPLACE(id_no, '-', ''), ' ', '') =  '" + maHolderReg.IdNo + "' ");
                                                if (responseCount.IsSuccess)
                                                {
                                                    int count = Convert.ToInt32(responseCount.Data);
                                                    if (count < 5)
                                                    {
                                                        User user = new User
                                                        {
                                                            Username = maHolderReg.Name1,
                                                            IdNo = maHolderReg.IdNo,
                                                            EmailId = obj.EmailId,
                                                            Password = "",
                                                            MobileNumber = obj.MobileNumber,
                                                            CreatedDate = DateTime.Now,
                                                            IsActive = 1,
                                                            IsSecurityChecked = 1, // IsCorp
                                                            IsSatChecked = 0,
                                                            Status = 1,
                                                            RegisterIp = TrackIPAddress.GetUserPublicIP(""),
                                                            ModifiedBy = 0,
                                                            VerificationCode = 0,
                                                            VerifyExpired = 1,
                                                            ResetExpired = 1
                                                        };
                                                        user.UserIdUserAccounts = new List<UserAccount>();
                                                        user.UserIdUserAccounts.Add(new UserAccount
                                                        {
                                                            UserId = user.Id,
                                                            IdNo = maHolderReg.IdNo,
                                                            AccountNo = maHolderReg.HolderNo.ToString(),
                                                            CreatedBy = 0,
                                                            CreatedDate = DateTime.Now,
                                                            Status = 1,
                                                            IsVerified = 1,
                                                            VerifyExpired = 1,
                                                            MaHolderRegId = maHolderReg.Id,
                                                            IsPrimary = 1,
                                                            ModifiedBy = 0,
                                                            HolderClass = maHolderReg.HolderCls,
                                                            IsPrinciple = 1,
                                                            MaHolderRegIdMaHolderReg = maHolderReg
                                                        });
                                                        maHolderReg.OtpActSt = "2";
                                                        maHolderReg.HandPhoneNo = obj.MobileNumber;
                                                        Response responseU = IUserService.PostData(user);
                                                        Response responseU1 = IMaHolderRegService.PostData(maHolderReg);
                                                        if (responseU.IsSuccess && responseU1.IsSuccess)
                                                        {
                                                            User returnUser = (User)responseU.Data;
                                                            response.IsSuccess = true;
                                                            response.Message = "Success. " + (count + 1) + " of 5 users has been registered with this company ID.";

                                                            //Audit Log starts here
                                                            AdminLogMain alm = new AdminLogMain()
                                                            {
                                                                Description = "Corporate Account Added " + (count + 1) + " of 5 users has been registered with this company ID (" + obj.RegisterIp + ")",
                                                                TableName = "users",
                                                                UpdatedDate = DateTime.Now,
                                                                UserId = loginUser.Id,
                                                            };
                                                            Response responseLog = IAdminLogMainService.PostData(alm);


                                                            //Audit Log ends here

                                                            return response;
                                                        }
                                                        else
                                                        {
                                                            response = responseU;
                                                            return response;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        response.IsSuccess = false;
                                                        response.Message = "Cannot register. 5 users has been registered with this company ID";
                                                        return response;
                                                    }
                                                }
                                                else
                                                {
                                                    response = responseCount;
                                                    return response;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        response = responseMA;
                                        return response;
                                    }
                                }
                                
                            }
                            
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "MA No. must be Numeric";
                            return response;
                        }
                    }
                }
                else
                {
                    Response responseGet = IUserService.GetSingle(obj.Id);
                    if (responseGet.IsSuccess)
                    {
                        User user = (User)responseGet.Data;
                        user.EmailId = obj.EmailId;
                        user.ModifiedBy = LoginUserId;
                        user.ModifiedDate = DateTime.Now;
                        user.MobileNumber = obj.MobileNumber;
                        IUserService.UpdateData(user);

                        response.IsSuccess = true;
                        response.Message = "Success";
                    }
                    else
                    {
                        return responseGet;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add corporate user action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "Corporate_Accounts_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Corporate Accounts");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No.", "Status", "Name", "Mobile No", "Created date", "ID No.", "MA Accounts", "Email", "Last login date" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Corporate Accounts"];

                string docDetails = "Corporate Accounts";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();

                    filterQuery.Append(" users.user_role_id='3'");
                    filterQuery.Append(" and user_accounts.holder_class in ('RC', 'BC', 'NC', 'FC')");
                    if (filter == "" || filter == null)
                    {
                        docDetails += " | Status: All (Active, Inactive)";
                        filterQuery.Append(" and users.status in (0, 1)");
                    }
                    else
                    {
                        docDetails += " | Status: " + (filter == "1" ? "Active" : (filter == "0" ? "Inactive" : "-")) + " ";
                        filterQuery.Append(" and users.status in (" + filter + ")");
                    }

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseUList = IUserService.GetDataByMA(filterQuery.ToString(), 0, 0, true);
                    if (responseUList.IsSuccess)
                    {
                        List<User> users = (List<User>)responseUList.Data;

                        StringBuilder asb = new StringBuilder();
                        int index = 1;
                        foreach (User u in users)
                        {
                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                            Response responseUA = IUserAccountService.GetDataByFilter(" user_id='" + u.Id + "' ", 0, 0, false);
                            if (responseUA.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUA.Data;

                                worksheet.Cells[row, 1].Value = index;
                                worksheet.Cells[row, 2].Value = (u.Status == 1 ? "Active" : "Inactive");
                                worksheet.Cells[row, 3].Value = u.Username;
                                worksheet.Cells[row, 4].Value = u.MobileNumber;
                                worksheet.Cells[row, 5].Value = (u.CreatedDate == null ? "" : u.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss"));
                                worksheet.Cells[row, 6].Value = u.IdNo;
                                worksheet.Cells[row, 7].Value = String.Join(", ", userAccounts.Select(x => x.AccountNo).ToArray());
                                worksheet.Cells[row, 8].Value = u.EmailId;
                                worksheet.Cells[row, 9].Value = (u.LastLoginDate == null ? "" : u.LastLoginDate.Value.ToString("dd/MM/yyyy HH:mm:ss"));
                                row++;
                                index++;
                            }
                        }
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Corporate Account List downloaded",
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Corporate_Accounts" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

    }
}