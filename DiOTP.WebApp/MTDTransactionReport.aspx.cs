﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentDashboardTemp : System.Web.UI.Page
    {
        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Reset filters to empty
            //ddlTransactionType.SelectedIndex = 0;
            //ddlPlanType.SelectedIndex = 0;
            //ddlProductType.SelectedIndex = 0;
            //ddlFundList.SelectedIndex = 0;

            User user = (User)(Session["user"]);
            StringBuilder filter = new StringBuilder();

            string mainQCount = (@"select count(*) as count from (");
            StringBuilder countFilter = new StringBuilder();
            
            //Check user Session
            if (user == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
            }
            else {


                StringBuilder TableBuilder = new StringBuilder();

                if (!IsPostBack) {
                    //Populate fund names for drop down list.
                    string queryFL = @"Select * from utmc_fund_information";
                    Response responseFundsList = GenericService.GetDataByQuery(queryFL, 0, 0, false, null, false, null, false);
                    if (responseFundsList.IsSuccess)
                    {
                        var fundsDyn = responseFundsList.Data;
                        var responseFundsJSON = JsonConvert.SerializeObject(fundsDyn);
                        List<DiOTP.Utility.UtmcFundInformation> fundList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.UtmcFundInformation>>(responseFundsJSON);

                        ddlFundList.DataSource = fundList;
                        ddlFundList.DataTextField = "FundName";
                        ddlFundList.DataValueField = "IpdFundCode";
                        ddlFundList.DataBind();
                        ddlFundList.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFundsList.Message + "\", '');", true);
                    }
                }
                
                //Check dropdownlist for values
                //TRANSACTION TYPE
                if (ddlTransactionType.SelectedIndex != 0 && ddlTransactionType.Items[ddlTransactionType.SelectedIndex].Value != "") {
                    ddlTransactionType.Value = ddlTransactionType.Items[ddlTransactionType.SelectedIndex].Value;
                    var selectedSetting = ddlTransactionType.Items[ddlTransactionType.SelectedIndex].Value;
                    filter.Append(" and hl.trans_type in ('" + selectedSetting + "')");
                }

                //PRODUCT TYPE
                if (ddlProductType.SelectedIndex != 0 && ddlProductType.Items[ddlProductType.SelectedIndex].Value != "")
                {
                    ddlProductType.Value = ddlProductType.Items[ddlProductType.SelectedIndex].Value;
                }

                //PLAN TYPE
                if (ddlPlanType.SelectedIndex != 0 && ddlPlanType.Items[ddlPlanType.SelectedIndex].Value != "")
                {
                    if (ddlProductType.Items[ddlTransactionType.SelectedIndex].Value != "" && ddlTransactionType.SelectedIndex != 0) {
                        ddlProductType.Disabled = false;
                    }

                    ddlPlanType.Value = ddlPlanType.Items[ddlPlanType.SelectedIndex].Value;
                    var selectedSetting = ddlPlanType.Items[ddlPlanType.SelectedIndex].Value;
                    if (selectedSetting == "CS") {
                        filter.Append(" and hr.holder_cls in ('RN','BI','NI','FI','B1','B2','B3','N1','N2','N3','H1','H2','SB','SF','SN','SS','MS')");
                    }
                    if (selectedSetting == "EPF") {
                        filter.Append(" and hr.holder_cls in ('EB','EN','ER','ES','EZ')");
                    }
                    
                }

                //FUND TYPE
                if (ddlFundList.SelectedIndex != 0 && ddlFundList.Items[ddlFundList.SelectedIndex].Value != "")
                {
                    ddlFundList.Value = ddlFundList.Items[ddlFundList.SelectedIndex].Value;
                    var selectedSetting = ddlFundList.Items[ddlFundList.SelectedIndex].Value;
                    filter.Append(" and hl.fund_id in ('" + selectedSetting + "')");
                }
                //reload js configs
                ClientScript.RegisterStartupScript(this.GetType(), "CallUpdate", "whenPostback()", true);

                //Query to get transactions that has been serviced by current agent, in current month (MTD).
                string queryTransByMonth = @"select 
                                            GROUP_CONCAT(uo.reject_reason) as trans_no, ar.agent_code, 
                                            DAY(uo.updated_date) as updated_day , MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year 
                                           from user_orders uo
											join agent_clients ac on uo.user_id = ac.user_id
											join agent_regs ar on ar.user_id = ac.agent_user_id
                                           where ar.user_id = '"+user.Id+"' and  MONTH(uo.updated_date) = MONTH(NOW()) group by ar.user_id  ";
                Response responseTransByMonth = GenericService.GetDataByQuery(queryTransByMonth, 0, 0, false, null, false, null, true);
                if (responseTransByMonth.IsSuccess)
                {
                    var transByMonthDyn = responseTransByMonth.Data;
                    var transByMonthJSON = JsonConvert.SerializeObject(transByMonthDyn);
                    List<dynamic> agentClientTrans = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(transByMonthJSON);

                    //To format transaction no for oracle query
                    if (agentClientTrans.Count != 0)
                    {
                        string[] transNos = agentClientTrans.FirstOrDefault().trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++)
                        {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",", transNos);

                        //Query to get transaction values and details from ORACLE
                        string queryGetMaholderSR = @"select hl.enter_dt as LEDGER_DATE, hl.trans_dt as BOOKING_DATE, hl.holder_no as MA_NUMBER, hl.trans_type as TRANSACTION_TYPE, hr.name_1 as NAME,
                                                    hr.holder_cls as PLAN_TYPE, f.l_name as FUND_NAME, hl.trans_amt as TRANSACTION_AMOUNT, hl.com_fee as SALES_CHARGE, hl.fee_amt as COMMISSION_AMOUNT
                                                    from UTS.HOLDER_LEDGER hl
                                                    left join UTS.HOLDER_REG hr on hl.holder_no = hr.holder_no
                                                    left join UTS.FUND f on hl.fund_id = f.fund_id
                                                    where hl.trans_no in (" + transactionsNo + ")";

                        Response responseGetlSR = ServicesManager.GetDataByQuery(queryGetMaholderSR + filter.ToString());
                        if (responseGetlSR.IsSuccess)
                        {
                            var getMAHSRDyn = responseGetlSR.Data;
                            var getMAHSRJSON = JsonConvert.SerializeObject(getMAHSRDyn);
                            List<dynamic> MaHolderSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(getMAHSRJSON);
                            int tableIndex = 1;

                            //if no data
                            if (MaHolderSRList.Count == 0)
                            {
                                tbodyTransactionList.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='12'>No data available for display.</td></tr>";
                            }
                            else
                            {
                                MaHolderSRList.ForEach(x =>
                                {
                                    TableBuilder.Append(
                                                        @"
                                                   <tr class='" + "trTextFormat" + @"'>
                                                   <td>" + tableIndex + @"</td>
                                                   <td>" + x.LEDGERDATE.ToString("dd/MM/yyyy") + @"</td>
                                                   <td>" + x.BOOKINGDATE.ToString("dd/MM/yyyy") + @"</td>
                                                   <td>" + x.MANUMBER + @"</td>
                                                   <td>" + x.NAME + @"</td>
                                                   <td>" + x.TRANSACTIONTYPE + @"</td>
                                                   <td>" + "UT" + @"</td>
                                                   <td>" + DiOTP.Utility.Helper.CustomValues.GetAccounPlan(x.PLANTYPE.ToString()) + @"</td>
                                                   <td>" + x.FUNDNAME + @"</td>
                                                   <td>" + x.TRANSACTIONAMOUNT.ToString("N", System.Globalization.CultureInfo.CurrentCulture) + @"</td>
                                                   <td>" + x.SALESCHARGE + @"</td>
                                                   <td>" + x.COMMISSIONAMOUNT.ToString("N", System.Globalization.CultureInfo.CurrentCulture) + @"</td>
                                               </tr>");
                                    tableIndex++;
                                });
                                tbodyTransactionList.InnerHtml = TableBuilder.ToString();
                            }

                        }
                        else
                        {
                            tbodyTransactionList.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='12'>No data available for display.</td></tr>";
                        }
                    }
                    else {
                        tbodyTransactionList.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td colspan='12'>No data available for display.</td></tr>";
                    }
                }
                else
                {

                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["user"];
            Response response = new Response();
            string message = string.Empty;
            try
            {
                string[] filters = filter.Split(',');
                string transType = filters[0];
                string prodType = filters[1];
                string planType = filters[2];
                string fundType = filters[3];

                string tempPath = Path.GetTempPath() + "MTD_Transaction_Report_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("MTD Transaction Report");

                var headerRow = new List<string[]>()
            {
                new string[] { "Index", "Ledger Date", "Booking Date", "MA Number", "Name", "Transaction Type", "Product Type", "Plan Type", "Fund Name", "Amount Invested/Redeemed", "Sales Charge %", "Commission Amount"}
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["MTD Transaction Report"];

                string docDetails = "MTD Transaction Report";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();

                    //Check for data filtering is empty or not
                    if (filter != "" && filter != null)
                    {
                        //filter details modify here
                        if (filter != "")
                        {
                            docDetails += " | Filters Applied: ";
                            

                            if (!String.IsNullOrEmpty(transType))
                            {
                                filterQuery.Append(" and hl.trans_type in ('" + transType + "')");
                                docDetails += "Transaction Type - " + transType;
                            }
                            if (!String.IsNullOrEmpty(prodType))
                            {
                                if (!String.IsNullOrEmpty(transType))
                                {
                                    docDetails += " ,";
                                }
                                //filterQuery.Append(" and hl.trans_type in ('" + transType + "')");
                                docDetails += "Product Type - " + prodType;
                            }
                            if (!String.IsNullOrEmpty(planType))
                            {
                                if (planType == "CS")
                                {
                                    filterQuery.Append(" and hr.holder_cls in ('RN','BI','NI','FI','B1','B2','B3','N1','N2','N3','H1','H2','SB','SF','SN','SS','MS')");
                                    if (!String.IsNullOrEmpty(prodType)) {
                                        docDetails += " ,";
                                    }
                                    docDetails += "Plan Type - " + planType;
                                }
                                if (planType == "EPF")
                                {
                                    filterQuery.Append(" and hr.holder_cls in ('EB','EN','ER','ES','EZ')");
                                    if (!String.IsNullOrEmpty(prodType))
                                    {
                                        docDetails += " ,";
                                    }
                                    docDetails += "Plan Type -" + DiOTP.Utility.Helper.CustomValues.GetAccounPlan(planType);
                                }
                            }
                            if (!String.IsNullOrEmpty(fundType))
                            {
                                filterQuery.Append(" and hl.fund_id in ('" + fundType + "')");
                                if (!String.IsNullOrEmpty(planType) || !String.IsNullOrEmpty(prodType))
                                {
                                    docDetails += " ,";
                                }
                                
                                docDetails += "Fund Type - " + fundType;
                            }
                            if (String.IsNullOrEmpty(fundType) && String.IsNullOrEmpty(planType) && String.IsNullOrEmpty(transType)) {
                                docDetails += "None";
                            }

                        }
                        else
                        {
                            docDetails += " | Filters Applied: None";
                        }
                    }

                    //To create first line in excel "Page + filtering conditions"
                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    //variables for data population
                    int index = 1;

                    //After creating header and filtering option. Populate data to be downloaded.
                    //Query to get transactions that has been serviced by current agent, in current month (MTD).
                    //string queryTransByMonth = @"select 
                    //                        GROUP_CONCAT(uo.reject_reason) as trans_no, uo.consultantId, 
                    //                        DAY(uo.updated_date) as updated_day, MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year 
                    //                       from user_orders uo
                    //                       join user_accounts ua on uo.user_id = ua.user_id
                    //                       join agent_regs ar on ar.agent_code = uo.consultantId
                    //                       where uo.consultantId ='" + loginUser.AgentCode + "' and  MONTH(uo.updated_date) = MONTH(NOW()) group by uo.consultantId  ";

                    string queryTransByMonth = @"select 
                                            GROUP_CONCAT(uo.reject_reason) as trans_no, ar.agent_code, 
                                            DAY(uo.updated_date) as updated_day , MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year 
                                           from user_orders uo
											join agent_clients ac on uo.user_id = ac.user_id
											join agent_regs ar on ar.user_id = ac.agent_user_id
                                           where ar.user_id = '" + loginUser.Id + "' and  MONTH(uo.updated_date) = MONTH(NOW()) group by ar.user_id";
                    Response responseTransByMonth = GenericService.GetDataByQuery(queryTransByMonth, 0, 0, false, null, false, null, true);
                    if (responseTransByMonth.IsSuccess)
                    {
                        var transByMonthDyn = responseTransByMonth.Data;
                        var transByMonthJSON = JsonConvert.SerializeObject(transByMonthDyn);
                        List<dynamic> agentClientTrans = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(transByMonthJSON);

                        //To format transaction no for oracle query
                        string[] transNos = agentClientTrans.FirstOrDefault().trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++)
                        {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",", transNos);

                        //Query to get transaction values and details from ORACLE
                        string queryGetMaholderSR = @"select hl.enter_dt as LEDGER_DATE, hl.trans_dt as BOOKING_DATE, hl.holder_no as MA_NUMBER, hl.trans_type as TRANSACTION_TYPE, hr.name_1 as NAME,
                                                    hr.holder_cls as PLAN_TYPE, f.l_name as FUND_NAME, hl.trans_amt as TRANSACTION_AMOUNT, hl.com_fee as SALES_CHARGE, hl.fee_amt as COMMISSION_AMOUNT
                                                    from UTS.HOLDER_LEDGER hl
                                                    left join UTS.HOLDER_REG hr on hl.holder_no = hr.holder_no
                                                    left join UTS.FUND f on hl.fund_id = f.fund_id
                                                    where hl.trans_no in (" + transactionsNo + ")";

                        Response responseGetlSR = ServicesManager.GetDataByQuery(queryGetMaholderSR + filterQuery.ToString());
                        if (responseGetlSR.IsSuccess)
                        {
                            var getMAHSRDyn = responseGetlSR.Data;
                            var getMAHSRJSON = JsonConvert.SerializeObject(getMAHSRDyn);
                            List<dynamic> MaHolderSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(getMAHSRJSON);
                            
                            //populate values into excel rows
                            MaHolderSRList.ForEach(x => {
                                worksheet.Cells[row, 1].Value = index;
                                worksheet.Cells[row, 2].Value = x.LEDGERDATE.ToString("dd/MM/yyyy");
                                worksheet.Cells[row, 3].Value = x.BOOKINGDATE.ToString("dd/MM/yyyy");
                                worksheet.Cells[row, 4].Value = String.IsNullOrEmpty(x.MANUMBER.ToString()) ? "-" : x.MANUMBER.ToString();
                                worksheet.Cells[row, 5].Value = String.IsNullOrEmpty(x.NAME.ToString()) ? "-" : x.NAME.ToString();
                                worksheet.Cells[row, 6].Value = String.IsNullOrEmpty(x.TRANSACTIONTYPE.ToString()) ? "-" : x.TRANSACTIONTYPE.ToString();
                                worksheet.Cells[row, 7].Value = !String.IsNullOrEmpty(x.PLANTYPE.ToString()) && (DiOTP.Utility.Helper.CustomValues.GetAccounPlan(x.PLANTYPE.ToString()) == "CASH" || DiOTP.Utility.Helper.CustomValues.GetAccounPlan(x.PLANTYPE.ToString()) == "EPF") ? "UT" : "PRS"; //String.IsNullOrEmpty(prodType.ToString()) ? "-" : prodType.ToString();
                                worksheet.Cells[row, 8].Value = String.IsNullOrEmpty(x.PLANTYPE.ToString()) ? "-" : DiOTP.Utility.Helper.CustomValues.GetAccounPlan(x.PLANTYPE.ToString());
                                worksheet.Cells[row, 9].Value = String.IsNullOrEmpty(x.FUNDNAME.ToString()) ? "-" : x.FUNDNAME.ToString();
                                worksheet.Cells[row, 10].Value = x.TRANSACTIONAMOUNT == 0 ? Convert.ToDouble("0.00") : Convert.ToDouble(x.TRANSACTIONAMOUNT.ToString("N", System.Globalization.CultureInfo.CurrentCulture));
                                worksheet.Cells[row, 11].Value = x.SALESCHARGE == 0 ? Convert.ToDouble("0.0000") : Convert.ToDouble(x.SALESCHARGE);
                                worksheet.Cells[row, 12].Value = x.COMMISSIONAMOUNT == 0 ? Convert.ToDouble("0.00"): Convert.ToDouble(x.COMMISSIONAMOUNT.ToString("N", System.Globalization.CultureInfo.CurrentCulture));

                                //update row and index value
                                row++;
                                index++;
                            });
                        }

                        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                        // Apply some predefined styles for data to look nicely :)
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        // Save this data as a file
                        excel.SaveAs(excelFile);
                        // Display SUCCESS message
                        response.IsSuccess = true;
                        response.Message = "Downloading...";
                        //Audit Log starts here
                        AdminLogMain alm = new AdminLogMain()
                        {
                            Description = "MTD Transaction Report downloaded",
                            TableName = "'Transaction_Reports'",
                            UpdatedDate = DateTime.Now,
                            UserId = loginUser.Id,
                        };
                        Response responseLog = IAdminLogMainService.PostData(alm);
                    }
                    else
                    {
                        response = responseTransByMonth;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "MTD_Transaction_Report " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

        
        public class reportDetails {
            DateTime Ledger_Date { get; set; }
            DateTime Booking_Date { get; set; }
            string MaNumber { get; set; }
            string Name { get; set; }
            string Transaction_Type { get; set; }
            string Product_Type { get; set; }
            string Plan_Type { get; set; }
            string Fund_Name { get; set; }
            double Amount { get; set; }
            double Sales_Charge { get; set; }
            double Commission_Amount { get; set; }

        }

    }
}