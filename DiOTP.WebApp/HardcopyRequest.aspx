﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="HardcopyRequest.aspx.cs" Inherits="DiOTP.WebApp.HardcopyRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li><a href="/Settings.aspx">Settings</a></li>
                            <li class="active">Hardcopy Request</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Hardcopy Request</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">

                    <div class="row">
                        <div class="col-md-5">
                            <p>Master Account Number : </p>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlUserAccountId" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="hardcopyActivation" id="hardcopyActivation" runat="server">
                        <h4 class="mb-10">Requested for Hardcopy Activation: </h4>
                        <p class="mb-8">Upon your request, statements/reports will be sent to your mailing address & softcopy for statements/reports will not be sent to you.</p>
                        <p class="mb-10">However you can choose to conserve trees and fuel by replacing your hardcopy statements and reports with an electronic version by de-activate hardcopy.</p>
                        <div class="row mb-20">
                            <div class="col-lg-3 col-md-5">
                                <asp:Button CssClass="btn btn-infos1 btn-block" ID="btnActivate" runat="server" ClientIDMode="Static" OnClick="btnActivate_Click" Text="Activate Hardcopy" Width="200px" />
                            </div>
                        </div>
                        <div>
                            <h5 class="mb-8">NOTE:</h5>
                            <p class="mb-8">Your request for hardcopy statements/reports will be made available in the next statement cycle.</p>
                            <asp:Button CssClass="btn btn-infos1" ID="btnbacnk2" runat="server" OnClick="btnback_Click" Text="Back" />
                        </div>
                    </div>

                    <div class="hardcopyActivation" id="hardcopyDeactivation" runat="server">
                        <h4 class="mb-10">Requested for Hardcopy De-Activation:</h4>
                        <p class="mb-8">
                            Upon your request, Statements/Reports will be sent to your email address and hardcopy Statements/Reports WILL NOT be sent to you.  
                        </p>
                        <div class="row mb-20">
                            <div class="col-lg-3 col-md-5">
                                <asp:Button CssClass="btn btn-infos1 btn-block" ID="btnDeactivate" runat="server" ClientIDMode="Static" OnClick="btnDeactivate_Click" Text="De-Activate Hardcopy" Width="200px" />
                            </div>
                        </div>
                        <div>
                            <h5 class="mb-8">NOTE:</h5>
                            <p class="mb-8">Your request to de-activate hardcopy statements/reports will take place in next statement cycle.</p>
                            <asp:Button CssClass="btn btn-infos1" ID="btnback" runat="server" OnClick="btnback_Click" Text="Back" />
                        </div>
                    </div>

                    <div id="divMessage" runat="server" class="text-success"></div>

                </div>
            </div>


        </section>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script>
        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });
    </script>
</asp:Content>
