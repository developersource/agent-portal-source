﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class OccupationView
    {
        public int id { get; set; }
        public String OCC_CODE { get; set; }
        public String ACCOUNT_TYPE { get; set; }
        public String nob { get; set; }
        public String OTP_VERSION { get; set; }
        public String holder_no { get; set; }
        public String name_of_employer { get; set; }
        public String addr_1 { get; set; }
        public String addr_2 { get; set; }
        public String addr_3 { get; set; }
        public String postcode { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String country { get; set; }
        public String office_tel_no_2 { get; set; }
        public String Isemployed { get; set; }
        public String Status { get; set; }
        public String StatusInfo { get; set; }


    }
}
