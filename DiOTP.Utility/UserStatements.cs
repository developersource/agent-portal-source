﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class UserStatements
    {
        public Int32 id { get; set; }
        public UserAccount UserA { get; set; }
        public Int32 user_id { get; set; }
        public Int32 user_account_id { get; set; }
        public Int32 user_statment_category_id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public DateTime createdate { get; set; }
        public DateTime statementdate { get; set; }
        public Int32 status { get; set; }
    }
}
