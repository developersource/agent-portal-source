﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DistributionInstruction.aspx.cs" Inherits="Admin.DistributionInstruction" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="distribtionInstructionForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button id="download" data-message="Success" data-status="Active" data-original-title="Download as excel" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Download" data-isconfirm="0"><i class="fa fa-download"></i></button>
                                                <span class="common-divider pull-right"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can search here with User Id</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can Download on Instructions</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="filter-section mt-10">
                                            <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                            <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="3">All</button>
                                            <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="1">Reinvest</button>
                                            <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="2">Payout</button>
                                            <br />
                                            <small>
                                                <strong>Note:</strong> 
                                                <span>Instructions Filtering</span>
                                            </small>
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>User Id (Username)</th>
                                            <th>MA No</th>
                                            <th>Fund Name</th>
                                            <th>Instruction</th>
                                            <th>Updated Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="TbodyDistributionInstruction" runat="server">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
