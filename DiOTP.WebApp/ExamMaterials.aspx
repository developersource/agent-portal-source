﻿<%@ Page Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="ExamMaterials.aspx.cs" Inherits="DiOTP.WebApp.ExamMaterials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>

            .trTextFormat {
                        text-align: center;
            }

            .theadFormat tr th {
                text-align:center;
            }

        .theadFormat tr td {
            text-align:center;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/AgentDashboard.aspx">Dashboard</a></li>
                    <li class="active">Exam Preparation Materials</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Exam Preparation Materials</h3>
            </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="border: none!important;">
                                <table class="table table-font-size-13 table-bordered " id="AgentTraineeMaterialTable" style="width: 95% !important">
                                    <thead id="theadAgentTraineeMaterialList" class="theadFormat" runat="server">
                                        <tr>
                                            <th>Index</th>
                                            <th>Material Name</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyAgentTraineeMaterialList" class="theadFormat" runat="server" clientidmode="Static">
                                        <tr>
                                            <td>1</td>
                                            <td><a href="#">Exam Preparation Module 1 <i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                            <td>To help agents understand the fundamentals of Unit Trust.</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="#">Exam Preparation Module 2 <i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                            <td>To provide guidance on how to adapt into the market for newcomvers.</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="#">Exam Preparation Module 3 <i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                            <td>To prepare agents for CUTE Exam questions.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            <%--<div class="row hide">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="card hovercard">

                        <div class="cardheader">
                        </div>
                        <div class="avatar">
                            <img alt="" src="/Content/MyImage/9.png">
                        </div>
                        <div class="info">
                            <div class="title">
                                <a target="_blank" href="https://scripteden.com/">Script Eden</a>
                            </div>
                            <div class="desc">Passionate designer</div>
                            <div class="desc">Curious developer</div>
                            <div class="desc">Tech geek</div>
                        </div>
                        <div class="bottom">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="divClientsHTML" runat="server">
            </div>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>