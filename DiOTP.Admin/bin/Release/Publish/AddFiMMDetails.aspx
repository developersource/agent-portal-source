﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFiMMDetails.aspx.cs" Inherits="Admin.AddFiMMDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="FormId" data-value="addFiMMForm">
        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
        <div class="row" id="examEnrollDiv" runat="server" clientidmode="static">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-12">
                        <h5>Enroll for Exam</h5>
                    </div>
                    <div class="col-md-4">
                        <label>Location</label>
                        <div>
                            <select id="ddlLocation" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Language</label>
                        <div>
                            <select id="ddlLanguage" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Exam Date</label>
                        <div>
                            <select id="ddlExamDate" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Time</label>
                        <div>
                            <select id="ddlTime" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4" id="enrollStatusDiv" runat="server">
                        <label>Status</label>
                        <div>
                            <div id="enrollStatus" runat="server" clientidmode="static" class="form-control" readonly="true">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="resultDiv" runat="server">
                        <label>Result</label>
                        <div>
                            <select id="ddlResult" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="Pass">Pass</option>
                                <option value="Fail">Fail</option>
                                <option value="Absent">Absent</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="fimmDiv" runat="server" clientidmode="static">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-12">
                        <h5>FIMM Details</h5>
                    </div>
                    <div class="col-md-4">
                        <label>FIMM License No *</label>
                        <div>
                            <input id="txtFIMMNo" runat="server" class="form-control" placeholder="Enter FIMM number" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Approval Date *</label>
                        <div>
                            <input id="txtApprovalDate" runat="server" class="form-control" placeholder="Select Approval Date" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Expiry Date *</label>
                        <div>
                            <input id="txtExpiryDate" runat="server" class="form-control" placeholder="Select Expiry Date" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Remarks</label>
                        <div>
                            <textarea id="txtRemarks" runat="server" class="form-control" placeholder="Remarks"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row mb-6 hide">
                    <div class="col-md-12">
                        <h5>Payment Details - From Agent</h5>
                    </div>
                    <div class="col-md-4">
                        <label>MICR Code *</label>
                        <div>
                            <input id="txtFIMMMICRCode" runat="server" class="form-control" placeholder="Enter MICR Code" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Bank Name *</label>
                        <div>
                            <input id="txtFIMMBankName" runat="server" class="form-control" placeholder="Enter Bank Name" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Cheque No *</label>
                        <div>
                            <input id="txtFIMMChequeno" runat="server" class="form-control" placeholder="Enter Cheque No" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Transaction Ref No *</label>
                        <div>
                            <input id="txtFIMMTransRefno" runat="server" class="form-control" placeholder="Enter Transaction Ref No" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Amount *</label>
                        <div>
                            <input id="txtFIMMAmount" runat="server" class="form-control" placeholder="Enter Amount" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Date Received *</label>
                        <div>
                            <input id="txtFIMMDateReceived" runat="server" class="form-control" placeholder="Select Date Received" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Payment Ref No *</label>
                        <div>
                            <input id="txtFIMMPaymentRefno" runat="server" class="form-control" placeholder="Enter Payment Ref No" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Deposit Account No *</label>
                        <div>
                            <input id="txtFIMMDepositAccountNo" runat="server" class="form-control" placeholder="Enter Deposit Account No" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5>Payment Details - To FiMM</h5>
                    </div>
                    <div class="col-md-4">
                        <label>Withdrawal A/C No *</label>
                        <div>
                            <input id="txtToFiMMWithAccNo" runat="server" class="form-control" placeholder="Enter Withdrawal Account No" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>MICR Code *</label>
                        <div>
                            <input id="txtToFiMMMICRCode" runat="server" class="form-control" placeholder="Enter MICR Code" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Bank Name *</label>
                        <div>
                            <input id="txtToFiMMBankName" runat="server" class="form-control" placeholder="Enter Bank Name" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Cheque No *</label>
                        <div>
                            <input id="txtToFiMMChequeno" runat="server" class="form-control" placeholder="Enter Cheque No" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Date Sent *</label>
                        <div>
                            <input id="txtToFiMMDateSent" runat="server" class="form-control" placeholder="Select Date Sent" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>FiMM Renewal Amount *</label>
                        <div>
                            <input id="txtFiMMRenewalAmount" runat="server" class="form-control" placeholder="Enter FiMM Renewal Amount" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>UTMC Renewal Amount *</label>
                        <div>
                            <input id="txtUTMCRenewalAmount" runat="server" class="form-control" placeholder="Enter UTMC Renewal Amount" />
                        </div>
                    </div>
                </div>
                <div>
                    <strong class="pull-right">Note: On updating FiMM details, application will be pushed to Admin Manager to Approve.</strong>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="process_status" runat="server" value="0" clientidmode="static" />
    <script>
        $('#btnCommonModalSubmit').text('Update');
        //var process_status = $('#process_status').val();
        //if (process_status == "3") {
        //    $('#btnCommonModalSubmit').text('Request to Approve');
        //}
        //else if (process_status == "4") {
        //    $('#btnCommonModalSubmit').text('Update');
        //    //$('#btnCommonModalSubmit').hide();
        //}
        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
        function parseJsonDate(jsonDateString) {
            var formattedDate = new Date(parseInt(jsonDateString.replace('/Date(', '')));
            var hours = formattedDate.getHours();
            var minutes = formattedDate.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = pad(hours, 2) + ':' + pad(minutes, 2) + ' ' + ampm;
            var newDate = pad(formattedDate.getDate(), 2) + "/" + pad(formattedDate.getMonth() + 1, 2) + "/" + formattedDate.getFullYear() + " " + strTime;
            return newDate;
        }
        var AgentExamSchedules;
        $ui(document).ready(function () {

            $ui('#ddlLocation').change(function () {
                $ui.ajax({
                    url: "AddFiMMDetails.aspx/GetDetailsByLocation",
                    contentType: 'application/json; charset=utf-8',
                    type: "POST",
                    dataType: "JSON",
                    data: JSON.stringify({ Location: $ui('#ddlLocation').val() }),
                    success: function (data) {
                        var json = data.d;
                        if (!json.IsSuccess) {
                            ShowCustomMessage('Alert', json.Message, '');
                        }
                        else if (json.IsSuccess) {
                            AgentExamSchedules = json.Data;
                            console.log(AgentExamSchedules);

                            var languages = $.map(AgentExamSchedules, function (obj, idx) {
                                return obj.language;
                            }).filter((value, index, self) => self.indexOf(value) === index);
                            console.log(languages);
                            $('#ddlLanguage option:not(:first)').remove();
                            $.each(languages, function (idx, obj) {
                                $('#ddlLanguage').append('<option value="' + obj + '">' + obj + '</option>');
                            });

                            //var dates = $.map(AgentExamSchedules, function (obj, idx) {
                            //    var time = parseJsonDate(obj.exam_datetime);
                            //    return time.split(' ')[0];
                            //}).filter((value, index, self) => self.indexOf(value) === index);
                            //console.log(dates);
                            //$.each(dates, function (idx, obj) {
                            //    $('#ddlExamDate').append('<option value="' + obj + '">' + obj + '</option>');
                            //});

                            //var times = $.map(AgentExamSchedules, function (obj, idx) {
                            //    var time = parseJsonDate(obj.exam_datetime);
                            //    return time.split(' ')[1] + ' ' + time.split(' ')[2];
                            //});
                            //console.log(times);
                            //$.each(times, function (idx, obj) {
                            //    $('#ddlTime').append('<option value="' + obj + '">' + obj + '</option>');
                            //});
                        }
                    }
                });
            });

            $ui('#ddlLanguage').change(function () {
                console.log(AgentExamSchedules);
                var languageSchedules = $.grep(AgentExamSchedules, function (obj, idx) {
                    return obj.language == $ui('#ddlLanguage').val();
                });
                console.log(languageSchedules);

                var dates = $.map(languageSchedules, function (obj, idx) {
                    var time = parseJsonDate(obj.exam_datetime);
                    return time.split(' ')[0];
                }).filter((value, index, self) => self.indexOf(value) === index);
                console.log(dates);
                $('#ddlExamDate option:not(:first)').remove();
                $.each(dates, function (idx, obj) {
                    $('#ddlExamDate').append('<option value="' + obj + '">' + obj + '</option>');
                });
            });

            $ui('#ddlExamDate').change(function () {
                var dateSchedules = $.grep(AgentExamSchedules, function (obj, idx) {
                    var time = parseJsonDate(obj.exam_datetime);
                    return time.split(' ')[0] == $ui('#ddlExamDate').val();
                });
                console.log(dateSchedules);

                //var times = $.map(dateSchedules, function (obj, idx) {
                //    var time = parseJsonDate(obj.exam_datetime);
                //    return time.split(' ')[1] + ' ' + time.split(' ')[2];
                //});
                //console.log(times);
                $('#ddlTime option:not(:first)').remove();
                $.each(dateSchedules, function (idx, obj) {
                    var time = parseJsonDate(obj.exam_datetime);
                    $('#ddlTime').append('<option value="' + obj.id + '">' + time.split(' ')[1] + ' ' + time.split(' ')[2] + '</option>');
                });
            });

            $ui('#txtNewCardRecievedOn, #txtApprovalDate, #txtRenewalCardRecievedOn, #txtFIMMDateReceived, #txtToFiMMDateSent').datepicker({
                format: 'dd/mm/yyyy',
                pickTime: false,
                todayBtn: true,
                todayHighlight: true,
                autoclose: true,
            }).on('changeDate', function (ev) {
                console.log(ev.date);
                var year = ev.date.getFullYear();
                var month = ev.date.getMonth();
                var day = ev.date.getDate();
                var newDate = new Date(year + 1, month, day);
                $ui('#txtExpiryDate').val(pad(day, 2) + "/" + pad((month + 1), 2) + "/" + (year + 1));
            });

            //$ui('#ddlSessionId').change(function () {
            //    var SessionId = $ui(this).val();
            //    if (SessionId == "") {
            //        $ui('#txtLanguage').val('');
            //        $ui('#txtLocation').val('');
            //        $ui('#txtExamDate').val('');
            //    }
            //    else {
            //        $ui.ajax({
            //            url: "AddFiMMDetails.aspx/GetSessionDetails",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "POST",
            //            dataType: "JSON",
            //            data: JSON.stringify({ SessionId: SessionId }),
            //            success: function (data) {
            //                var json = data.d;
            //                if (!json.IsSuccess) {
            //                    ShowCustomMessage('Alert', json.Message, '');
            //                }
            //                else if (json.IsSuccess) {
            //                    var CustomAgentExamSchedule = json.Data;
            //                    var dat = parseJsonDate(CustomAgentExamSchedule.exam_datetime);
            //                    $ui('#txtLanguage').val(CustomAgentExamSchedule.language);
            //                    $ui('#txtLocation').val(CustomAgentExamSchedule.location);
            //                    $ui('#txtExamDate').val(dat);
            //                }
            //            }
            //        });
            //    }
            //});
        });
    </script>
</body>
</html>
