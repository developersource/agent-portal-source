﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.AgentApp
{
    public partial class AgentSignups : System.Web.UI.Page
    {
        private static readonly Lazy<IAgentSignupService> lazyIAgentSignupServiceObj = new Lazy<IAgentSignupService>(() => new AgentSignupService());
        public static IAgentSignupService IAgentSignupService { get { return lazyIAgentSignupServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
            {
                if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "1")
                {
                    string[] approvalTypes = new string[] { "GM", "SGM" };
                    User user = (User)(Session["user"]);
                    string query = (@" select a.*, u.username, u.email_id, u.mobile_number, u.id_no 
                                from agent_regs a 
                                join users u on u.id = a.user_id 
                                where a.process_status in (1, 2, 22, 19, 29, 3, 4) and a.introducer_code = '" + user.AgentCode + "' and a.status=1 ");

                    Response responseList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                    if (responseList.IsSuccess)
                    {
                        var UsersDyn = responseList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                        List<DiOTP.Utility.AgentRegInfo> aoRequestList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegInfo>>(responseJSON);
                        StringBuilder stringBuilder = new StringBuilder();
                        int idx = 1;
                        aoRequestList.ForEach(x =>
                        {
                            string actionsHTML = "";
                            if (x.process_status == 1)
                            {
                                actionsHTML += "<a href='CompleteSignup.aspx?status=1&agentId=" + x.id + @"&userId=" + x.user_id + @"' class='btn btn-infos1 btn-sm mb-5'>Register</a>";
                                actionsHTML += "<a href='CompleteSignup.aspx?status=19&agentId=" + x.id + @"&userId=" + x.user_id + @"' class='btn btn-infos1 btn-danger btn-sm mb-5'>Reject</a>";
                            }
                            else if (x.process_status == 2 || x.process_status == 3 || x.process_status == 4)
                            {
                                actionsHTML += "<a href='CompleteSignup.aspx?status=1&agentId=" + x.id + @"&userId=" + x.user_id + @"&viewonly=1' class='btn btn-sm mb-5'>View</a>";
                                if (user.UserIdUserTypes.Where(t => approvalTypes.Contains(t.UserTypeIdUserTypesDef.Name)).Count() > 0)
                                {
                                    //actionsHTML += "<a href='CompleteSignup.aspx?status=4&agentId=" + x.Id + @"' class='btn btn-infos1 btn-sm mb-5'>Approve</a>";
                                    //actionsHTML += "<a href='CompleteSignup.aspx?status=39&agentId=" + x.Id + @"' class='btn btn-infos1 btn-sm mb-5'>Reject</a>";
                                }
                            }
                            else if (x.process_status == 19 || x.process_status == 29 || x.process_status == 39)
                            {
                                actionsHTML += "-";
                            }
                            stringBuilder.Append(@"<tr>
                                            <td>" + idx + @"</td>
                                            <td>" + x.username + @"</td>
                                            <td>" + x.email_id + @"</td>
                                            <td>" + x.mobile_number + @"</td>
                                            <td>" + x.id_no + @"</td>
                                            <td>" + x.introducer_code + @"</td>
                                            <td>" + (x.process_status == 1 ? "New" : (x.process_status == 2 ? "Recommended" : (x.process_status == 19 || x.process_status == 29 || x.process_status == 39 ? "Rejected" : (x.process_status == 3 ? "Completed" : (x.process_status == 4 ? "Approved" : ""))))) + @"</td>
                                            <td>" + actionsHTML + @"</td>
                                        </tr>");
                            idx++;
                        });
                        tbodySignups.InnerHtml = stringBuilder.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseList.Message + "\", '');", true);
                    }
                }
                else if (Session["isVerified"].ToString() != "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Not verified.\", '/Portfolio.aspx');", true);
                }
                else if (Session["isAgent"].ToString() != "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Not agent.\", '/Portfolio.aspx');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Session expired.\", '/Index.aspx');", true);
            }
        }
    }
}