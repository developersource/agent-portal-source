﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class RequestHistory : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null && Session["isVerified"] != null)
                {
                    User user = (User)Session["user"];

                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount primaryAcc = new UserAccount();
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (Session["SelectedAccountHolderNo"] != null)
                        {
                            primaryAcc = userAccounts.Where(x => x.AccountNo == Session["SelectedAccountHolderNo"].ToString()).FirstOrDefault();

                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;
                            }
                        }
                        spanUsername.InnerHtml = primaryAcc.AccountNo + " - " + maHolderReg.Name1;

                        StringBuilder filter = new StringBuilder();
                        filter.Append(" user_id=" + user.Id + " ");
                        int skip = 0, take = 10;
                        if (hdnCurrentPageNo.Value == "")
                        {
                            skip = 0;
                            take = 10;
                            hdnNumberPerPage.Value = "10";
                            hdnCurrentPageNo.Value = "1";
                            
                            int rspCount = IUserOrderService.GetCountByFilter(filter.ToString());
                            hdnTotalRecordsCount.Value = rspCount.ToString();

                        }
                        else
                        {
                            skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * 10;
                            take = 10;
                        }
                        Response responseRSPList = IUserOrderService.GetDataByFilter(filter.ToString(), skip, take, false);
                        if (responseRSPList.IsSuccess)
                        {
                            Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ",0, 0, false);
                            if (responseUFIList.IsSuccess)
                            {
                                List<UtmcFundInformation> UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                                List<UserOrder> userRspescs = (List<UserOrder>)responseRSPList.Data;
                                if (userRspescs.Count > 0)
                                {
                                    int i = 1;
                                    StringBuilder sb = new StringBuilder();
                                    foreach (UserOrder ur in userRspescs)
                                    {
                                        string fundName = string.Empty;
                                        if (UTMCFundInformations.Count(a => a.Id == 2) != 0)
                                        {
                                            fundName = UTMCFundInformations.FirstOrDefault(a => a.Id == ur.FundId).FundName.Capitalize();
                                        }
                                        sb.Append(@"<tr>
                                                    <td>" + i + @"</td>
                                                    <td>" + fundName + @"</td>
                                                    <td><span class='currencyFormatNoSymbol' style='padding-left:50px;'>" + ur.Amount + @"</span></td>
                                                    <td>" + ur.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                                    <td>" + ur.Status + @"</td>
                                                </tr>");
                                        i++;
                                    }
                                    tbodyUserOrders.InnerHtml = sb.ToString();
                                }
                                else
                                {

                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRSPList.Message + "\", '');", true);
                        }
                        
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }

        }
    }
}