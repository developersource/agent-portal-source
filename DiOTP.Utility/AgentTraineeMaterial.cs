﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_trainee_materials")]
    public class AgentTraineeMaterial
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("material_name"), Display("material_name")]
        public String material_name { get; set; }
        [Database("material_description"), Display("material_description")]
        public String material_description { get; set; }
        [Database("material_url_path"), Display("material_url_path")]
        public String material_url_path { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("updated_date")]
        public DateTime updated_date { get; set; }
        [Database("updated_by"), Display("updated_by")]
        public Int32 updated_by { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
