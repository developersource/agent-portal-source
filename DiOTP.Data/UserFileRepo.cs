using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UserFileRepo : IUserFileRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UserFile obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserFile>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserFile> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserFile>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserFile obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserFile>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserFile> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserFile>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserFile DeleteData(Int32 Id)
        {
            Response responseUF = new Response();
            if (responseUF.IsSuccess)
            {
                UserFile obj = (UserFile)responseUF.Data;
                try
                {
                    string query = obj.ObjectToQuery<UserFile>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUF = new Response();
                    if (responseUF.IsSuccess)
                    {
                        UserFile obj = (UserFile)responseUF.Data;
                        query += obj.ObjectToQuery<UserFile>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserFile obj = new UserFile();
            try
            {
                string query = "select * from user_files where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UserFile
                       {
                           Id = x.Field<Int32>("id"),
                           UserId = x.Field<Int32>("user_id"),
                           UserFileTypeId = x.Field<Int32>("user_file_type_id"),
                           Title = x.Field<String>("title"),
                           Url = x.Field<String>("url"),
                           ThumbnailUrl = x.Field<String>("thumbnail_url"),
                           Status = x.Field<Int32>("status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserFile> objs = new List<UserFile>();
            try
            {
                string query = "select * from user_files";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserFile
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserFileTypeId = x.Field<Int32>("user_file_type_id"),
                            Title = x.Field<String>("title"),
                            Url = x.Field<String>("url"),
                            ThumbnailUrl = x.Field<String>("thumbnail_url"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_files";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UserFile>(propertyName);
            List<UserFile> objs = new List<UserFile>();
            try
            {
                string query = "select * from user_files where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from user_files where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserFile
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserFileTypeId = x.Field<Int32>("user_file_type_id"),
                            Title = x.Field<String>("title"),
                            Url = x.Field<String>("url"),
                            ThumbnailUrl = x.Field<String>("thumbnail_url"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UserFile>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_files where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from user_files where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserFile> objs = new List<UserFile>();
            try
            {
                string query = "select * from user_files where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserFile
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserFileTypeId = x.Field<Int32>("user_file_type_id"),
                            Title = x.Field<String>("title"),
                            Url = x.Field<String>("url"),
                            ThumbnailUrl = x.Field<String>("thumbnail_url"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(distinct user_id) from user_files where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
       
    }
}
