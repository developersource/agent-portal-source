﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class AccountOpeningRequest
    {
        public Int32 Id { get; set; }
        public Int32 UsCitizen { get; set; }
        public String IdNo { get; set; }
        public String PrincipleId { get; set; }
        public Int32 PrincipleUserId { get; set; }
        public String Username { get; set; }
        public String JointRelationship { get; set; }
        public String MobileNo { get; set; }
        public Int32 IsMobileVerified { get; set; }
        public Int32 IsOtpRequested { get; set; }
        public Int32 OtpAttempts { get; set; }
        public Int32 OtpCode { get; set; }
        public Int32 IsOtpExpired { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public Int32 EmailVerificationCode { get; set; }
        public Int32 IsEmailRequested { get; set; }
        public Int32 IsEmailVerified { get; set; }
        public Int32 ResidentOfMalaysia { get; set; }
        public String Nationality { get; set; }
        public String Name { get; set; }
        public String Dob { get; set; }
        public String Race { get; set; }
        public String BumiputraStatus { get; set; }
        public String MaritalStatus { get; set; }
        public String Gender { get; set; }
        public String Salutation { get; set; }
        public Int32 AreYouPep { get; set; }
        public String PepStatus { get; set; }
        public String FPepStatus { get; set; }
        public Int32 TaxResidentOutsideMalaysia { get; set; }
        public String TaxResidencyStatus { get; set; }
        public String Tin { get; set; }
        public Int32 Declaration { get; set; }
        public Int32 Status { get; set; }
        public Int32 AddressType { get; set; }
        public String Addr1 { get; set; }
        public String Addr2 { get; set; }
        public String Addr3 { get; set; }
        public String City { get; set; }
        public Int32 PostCode { get; set; }
        public String State { get; set; }
        public String Country { get; set; }
        public String TelNo { get; set; }
        public Int32? Agreement { get; set; }
        public Int32? AccountType { get; set; }
        public String Currency { get; set; }
        public Int32? BankId { get; set; }
        public String BankName { get; set; }
        public String AccountName { get; set; }
        public String AccountNo { get; set; }
        public String BankAccountType { get; set; }
        public String Purpose { get; set; }
        public String PurposeDescription { get; set; }
        public String Source { get; set; }
        public String EmployedByFundCompany { get; set; }
        public String Relationship { get; set; }
        public String NameOfFunder { get; set; }
        public String FundersIndustory { get; set; }
        public Int32 IsFunderMoneyChanger { get; set; }
        public Int32 IsFunderBeneficialOwner { get; set; }
        public String FundOwnerName { get; set; }
        public String EstimatedNetWorth { get; set; }
        public Int32 Employed { get; set; }
        public String Occupation { get; set; }
        public String EmployerName { get; set; }
        public String NatureOfBusiness { get; set; }
        public String MonthlyIncome { get; set; }
        public String OccupationOthers { get; set; }
        public Int32 ProcessStatus { get; set; }
        public DateTime SubmittedDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public String Remarks { get; set; }
        public Int32 FileType { get; set; }
        public String FileName { get; set; }
        public String Url { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Int32 IsIdentityVerified { get; set; }
        public String IdentityStatus { get; set; }
        public Int32 IsDocumentsVerified { get; set; }
        public String DocumentsStatus { get; set; }
        public Int32 IsAdditionalAdmin { get; set; }
        public String AdditionalDesc { get; set; }
        public String RegisterIPAddress { get; set; }
        public String RaceDescription { get; set; }
        public String AgentCode { get; set; }
    }
}
