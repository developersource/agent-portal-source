﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentProfile : System.Web.UI.Page
    {
        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //btnAction.Text = "Testing";
            }
            //txtAddressLine1.Text = "7 Jalan SS21/28";
            //txtAddressLine2.Text = "Damansara Utama";
            //txtAddressLine3.Text = "47400 Petaling Jaya";
            //txtAddressLine4.Text = "SELANGOR";
            //txtPostCode.Text = "47400";

            User user = (User)(Session["user"]);
            if (user != null)
            {
                StringBuilder asb = new StringBuilder();
                string agentPos = "";
                string agentGroup = "";
                string uplineCode = "";

                //Pre-populate state and country code drop down lists.
                Response responseCDList = ICountriesDefService.GetData(0, 0, false);
                if (responseCDList.IsSuccess)
                {
                    List<CountriesDef> countries = (List<CountriesDef>)responseCDList.Data;
                    ddlCountry.DataTextField = "Name";
                    ddlCountry.DataValueField = "Code";
                    ddlCountry.DataSource = countries;
                    ddlCountry.DataBind();

                    ddlCountry.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCDList.Message + "\", '');", true);
                }

                Response responseStates = IStatesDefService.GetData(0, 0, false);
                if (responseStates.IsSuccess)
                {
                    List<StatesDef> stateList = (List<StatesDef>)responseStates.Data;

                    ddlState.DataTextField = "Name";
                    ddlState.DataValueField = "Code";
                    ddlState.DataSource = stateList;
                    ddlState.DataBind();

                    ddlState.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseStates.Message + "\", '');", true);
                }


                //To populate agent details +agent upline detail
                string queryAgentDetail = @"select arp.name as agent_name, arp.mail_addr1, arp.mail_addr2, arp.mail_addr3,arp.mail_addr4,arp.post_code, arp.state, arp.country, ar.agent_code, ar.updated_date as join_date, ar.introducer_code as upline_code, ao.name as office_name, arf.url as selfie_url, ap.agent_pos as agent_position, ap.agent_group from agent_reg_personal arp
                                            join agent_regs ar on arp.agent_reg_id = ar.id
                                            join agent_offices ao on ar.office_id =  ao.id
                                            join agent_reg_files arf on arp.agent_reg_id = arf.agent_reg_id
                                            join agent_positions ap on ar.agent_code = ap.agent_code
                                            where ar.user_id = '" + user.Id + "' and arf.file_type = '3'";

                Response responseAgentDetailList = GenericService.GetDataByQuery(queryAgentDetail, 0, 0, false, null, false, null, true);
                if (responseAgentDetailList.IsSuccess)
                {
                    var agentDetailDyn = responseAgentDetailList.Data;
                    var responseAgentJSON = JsonConvert.SerializeObject(agentDetailDyn);
                    List<dynamic> agentDetailsObjList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentJSON);
                    dynamic agentObj = agentDetailsObjList.FirstOrDefault();

                    lblAgentName.Text = agentObj.agent_name.ToString();
                    lblAgentCode.Text = agentObj.agent_code.ToString();
                    lblRank.Text = user.UserIdUserTypes.Where(a => a.UserTypeIdUserTypesDef.Id == 6 || a.UserTypeIdUserTypesDef.Id == 7 || a.UserTypeIdUserTypesDef.Id == 8 || a.UserTypeIdUserTypesDef.Id == 9).FirstOrDefault().UserTypeIdUserTypesDef.Name;
                    lblJoinDate.Text = agentObj.join_date.ToString("dd/MM/yyyy");
                    lblRegion.Text = agentObj.office_name.ToString();
                    agentImage.ImageUrl = agentObj.selfie_url.ToString();

                    //Address
                    txtAddressLine1.Text = agentObj.mail_addr1.ToString();
                    txtAddressLine2.Text = agentObj.mail_addr2.ToString();
                    txtAddressLine3.Text = agentObj.mail_addr3.ToString();
                    txtAddressLine4.Text = agentObj.mail_addr4.ToString();
                    txtPostCode.Text = agentObj.post_code.ToString();
                    ddlState.SelectedValue = agentObj.state.ToString();
                    ddlCountry.SelectedValue = agentObj.country.ToString();
                    agentPos = agentObj.agent_position.ToString();
                    agentGroup = agentObj.agent_group.ToString();
                    uplineCode = agentObj.upline_code.ToString();
                }

                int agentPosLength = agentPos.Length + 3;
                //To populate direct + indirect Downline Count
                string queryCountUDLine = @"select count(*) as direct_downline, (select count(*) from agent_positions where agent_group = '" + agentGroup + "' and char_length(agent_pos) >" + agentPosLength + @") as indirect_downline 
                                            from agent_positions 
                                            where agent_group = '" + agentGroup + "' and CHAR_LENGTH(agent_pos) ='" + agentPosLength + "'";
                Response responseCountUDList = GenericService.GetDataByQuery(queryCountUDLine, 0, 0, false, null, false, null, true);
                if (responseCountUDList.IsSuccess)
                {
                    var countUDDyn = responseCountUDList.Data;
                    var responseCountUD = JsonConvert.SerializeObject(countUDDyn);
                    List<dynamic> countUDList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseCountUD);

                    lblDirectDownline.Text = countUDList.FirstOrDefault().direct_downline.ToString();
                    lblIndirectDownline.Text = countUDList.FirstOrDefault().indirect_downline.ToString();
                }

                //To populate agent upline details
                string queryAgentUpline = @"select utd.name as agent_rank, arp.* from agent_reg_personal arp 
                                            join agent_regs ar on arp.agent_reg_id = ar.id
                                            join user_types ut on ut.user_id = ar.user_id
                                            join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')
                                            where ar.agent_code = '" + uplineCode + "'";
                Response responseAgentUpline = GenericService.GetDataByQuery(queryAgentUpline, 0, 0, false, null, false, null, true);
                if (responseAgentUpline.IsSuccess)
                {
                    var agentUplineDyn = responseAgentUpline.Data;
                    var responseAgent = JsonConvert.SerializeObject(agentUplineDyn);
                    List<dynamic> agentUplineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgent);
                    if (agentUplineList.Count > 0)
                    {
                        lblUplineName.Text = agentUplineList.FirstOrDefault().name.ToString();
                        lblUplineRank.Text = agentUplineList.FirstOrDefault().agent_rank.ToString();
                        lblUplineMobileNo.Text = agentUplineList.FirstOrDefault().hand_phone.ToString();
                    }
                    else
                    {
                        lblUplineName.Text = "Not Available";
                        lblUplineRank.Text = "Not Available";
                        lblUplineMobileNo.Text = "Not Available";
                    }
                    lblUplineName.Text = String.IsNullOrEmpty(agentUplineList.FirstOrDefault().name.ToString()) ? "-" : agentUplineList.FirstOrDefault().name.ToString();
                    lblUplineRank.Text = String.IsNullOrEmpty(agentUplineList.FirstOrDefault().agent_rank.ToString()) ? "-" : agentUplineList.FirstOrDefault().agent_rank.ToString();
                    lblUplineMobileNo.Text = String.IsNullOrEmpty(agentUplineList.FirstOrDefault().hand_phone.ToString()) ? "-" : agentUplineList.FirstOrDefault().hand_phone.ToString();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object UpdateAddress(string addressline1, string addressline2, string addressline3, string addressline4, string postCode, string stateCode, string countryCode)
        {

            User loginUser = (User)HttpContext.Current.Session["user"];
            Response response = new Response();
            //Check user Session
            if (loginUser == null)
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
            }
            else {
                string agentCode = loginUser.AgentCode;

                //Validation for null or empty fields when updating the agent account address.
                if (String.IsNullOrEmpty(addressline1))
                {
                    response.Message = "Address Line 1 Cannot be empty!";
                    response.IsSuccess = false;
                    return response;
                }
                if (String.IsNullOrEmpty(addressline2))
                {
                    response.Message = "Address Line 2 Cannot be empty!";
                    response.IsSuccess = false;
                    return response;
                }
                if (String.IsNullOrEmpty(postCode))
                {
                    response.Message = "Post Code Cannot be empty!";
                    response.IsSuccess = false;
                    return response;
                }
                if (String.IsNullOrEmpty(stateCode))
                {
                    response.Message = "State Code Cannot be empty!";
                    response.IsSuccess = false;
                    return response;
                }
                if (String.IsNullOrEmpty(countryCode))
                {
                    response.Message = "Country Code Cannot be empty!";
                    response.IsSuccess = false;
                    return response;
                }

                try
                {
                    //check agent code
                    //if input is not empty or null => get agent name.
                    if (!String.IsNullOrEmpty(agentCode))
                    {
                        string queryAgentCode = @"SELECT arp.* FROM agent_regs ar
                                            left join agent_reg_personal arp on arp.agent_reg_id = ar.id
                                            where ar.agent_code = '" + agentCode + "'";
                        Response responseAgentCode = GenericService.GetDataByQuery(queryAgentCode, 0, 0, false, null, false, null, true);
                        if (responseAgentCode.IsSuccess)
                        {
                            var agentCodeDyn = responseAgentCode.Data;
                            var responseJSON = JsonConvert.SerializeObject(agentCodeDyn);
                            List<AgentRegPersonal> arpList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentRegPersonal>>(responseJSON);

                            //Assign new object
                            AgentRegPersonal singleARP = arpList.FirstOrDefault();

                            //Update the new object address only
                            singleARP.mail_addr1 = !String.IsNullOrEmpty(addressline1) ? addressline1 : "";
                            singleARP.mail_addr2 = !String.IsNullOrEmpty(addressline2) ? addressline2 : "";
                            singleARP.mail_addr3 = !String.IsNullOrEmpty(addressline3) ? addressline3 : "";
                            singleARP.mail_addr4 = !String.IsNullOrEmpty(addressline4) ? addressline4 : "";
                            singleARP.post_code = postCode;
                            singleARP.state = stateCode;
                            singleARP.country = countryCode;

                            //query to update.
                            Response responseUpdateARP = GenericService.UpdateData<AgentRegPersonal>(singleARP);
                            if (responseUpdateARP.IsSuccess)
                            {
                                response.Message = "Successfully updated the Address!";
                                response.IsSuccess = true;
                            }
                            else
                            {
                                response.Message = "Address update is unsuccessful.";
                                response.IsSuccess = false;
                            }

                        }
                        else
                        {
                            response.Message = "Invalid Agent Code";
                            response.IsSuccess = false;
                            return response;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog("Agent Profile Page Update Address: " + ex.Message);
                }

                return response;
            }
            return response;
        }
    }
}