using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UserRepo : IUserRepo
    {
        IUserSecurityRepo IUserSecurityRepo = new UserSecurityRepo();
        IUserAccountRepo IUserAccountRepo = new UserAccountRepo();
        IUserDetailRepo IUserDetailRepo = new UserDetailRepo();
        IUserRoleRepo IUserRoleRepo = new UserRoleRepo();
        IUserTypeRepo IUserTypeRepo = new UserTypeRepo();
        IUserTypesDefRepo IUserTypesDefRepo = new UserTypesDefRepo();
        IMaHolderRegRepo IMaHolderRegRepo = new MaHolderRegRepo();

        IUserFileRepo IUserFileRepo = new UserFileRepo();

        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(User obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<User>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                //NEW
                //obj = GetDataByPropertyName(nameof(User.EmailId), obj.EmailId, true, 0, 0, false).FirstOrDefault();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<User> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<User>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(User obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<User>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<User> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<User>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public User DeleteData(Int32 Id)
        {
            Response responseU = GetSingle(Id);
            if (responseU.IsSuccess)
            {
                User obj = (User)responseU.Data;
                try
                {
                    string query = obj.ObjectToQuery<User>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseU = GetSingle(Id);
                    if (responseU.IsSuccess)
                    {
                        User obj = (User)responseU.Data;
                        query += obj.ObjectToQuery<User>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            User obj = new User();
            try
            {
                string query = "select * from users where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new User
                       {
                           Id = x.Field<Int32>("id"),
                           Username = x.Field<String>("username"),
                           EmailId = x.Field<String>("email_id"),
                           MobileNumber = x.Field<String>("mobile_number"),
                           IdNo = x.Field<String>("id_no"),
                           Password = x.Field<String>("password"),
                           TransPwd = x.Field<String>("trans_pwd"),
                           UniqueKey = x.Field<String>("unique_key"),
                           UserRoleId = x.Field<Int32>("user_role_id"),
                           LastLoginDate = x.Field<DateTime?>("last_login_date"),
                           CreatedBy = x.Field<Int32>("created_by"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                           ModifiedDate = x.Field<DateTime?>("modified_date"),
                           IsOnline = (x.Field<Int32>("is_online")),
                           IsActive = (x.Field<Int32>("is_active")),
                           IsPrimary = (x.Field<Int32>("is_primary")),
                           IsSecurityChecked = Convert.ToInt32(x.Field<Int32>("is_security_checked")),
                           Status = x.Field<Int32>("status"),
                           LastLoginIp = x.Field<String>("last_login_ip"),
                           RegisterIp = x.Field<String>("register_ip"),
                           SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                           IsSatChecked = x.Field<Int32?>("is_sat_checked") == null ? 0 : x.Field<Int32>("is_sat_checked"),
                           PasswordResetCode = x.Field<String>("password_reset_code"),
                           ResetExpired = (x.Field<Int32>("reset_expired")),
                           VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                           VerifyExpired = Convert.ToInt32(x.Field<Int32>("verify_expired")),
                           LoginAttempts = x.Field<Int32>("login_attempts"),
                           SmsAttempts = x.Field<Int32>("sms_attempts"),
                           GoogleAuthAttempts = x.Field<Int32>("google_auth_attempts"),
                           IsLoginLocked = x.Field<Int32>("is_login_locked"),
                           IsSmsLocked = x.Field<Int32>("is_sms_locked"),
                           IsHardCopy = x.Field<Int32>("is_hard_copy"),
                           HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                           EmailCode = x.Field<String>("email_code"),
                           ActivationDate = x.Field<DateTime?>("activation_date"),
                           IsAgent = x.Field<Int32>("is_agent"),
                           AgentCode = x.Field<String>("agent_code"),
                           AgentStatus = x.Field<Int32>("agent_status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                String UserId = nameof(UserSecurity.UserId);
                Response responseUSList = IUserSecurityRepo.GetDataByPropertyName(UserId, obj.Id.ToString(), true, 0, 0, false);
                if (responseUSList.IsSuccess)
                {
                    obj.UserIdUserSecurities = (List<UserSecurity>)responseUSList.Data;
                }
                Response responseUAList = IUserAccountRepo.GetDataByPropertyName(UserId, obj.Id.ToString(), true, 0, 0, false);
                //userAccounts.ForEach(userAccount =>
                //{
                //    userAccount.MaHolderRegIdMaHolderReg = IMaHolderRegRepo.GetSingle(userAccount.MaHolderRegId);
                //});
                if (responseUAList.IsSuccess)
                {
                    obj.UserIdUserAccounts = (List<UserAccount>)responseUAList.Data;
                }
                Response responseUDList = IUserDetailRepo.GetDataByPropertyName(UserId, obj.Id.ToString(), true, 0, 0, false);
                if (responseUDList.IsSuccess)
                {
                    obj.UserIdUserDetails = (List<UserDetail>)responseUDList.Data;
                }
                Response responseUR = IUserRoleRepo.GetSingle(obj.UserRoleId);
                if (responseUR.IsSuccess)
                {
                    obj.UserRoleIdUserRole = (UserRole)responseUR.Data;
                }
                Response responseUTList = IUserTypeRepo.GetDataByPropertyName(UserId, obj.Id.ToString(), true, 0, 0, false);
                if (responseUTList.IsSuccess)
                {
                    ((List<UserType>)responseUTList.Data).ForEach(usertype =>
                    {
                        Response responseUTD = IUserTypesDefRepo.GetSingle(usertype.UserTypeId);
                        if (responseUTD.IsSuccess)
                        {
                            usertype.UserTypeIdUserTypesDef = (UserTypesDef)responseUTD.Data;
                        }
                    });
                    obj.UserIdUserTypes = (List<UserType>)responseUTList.Data;
                }

                Response responseUFList = IUserFileRepo.GetDataByPropertyName(nameof(UserFile.UserId), obj.Id.ToString(), true, 0, 0, false);
                if (responseUFList.IsSuccess)
                {
                    obj.UserIdUserFiles = (List<UserFile>)responseUFList.Data;
                }
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                string query = "select * from users";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new User
                        {
                            Id = x.Field<Int32>("id"),
                            Username = x.Field<String>("username"),
                            EmailId = x.Field<String>("email_id"),
                            MobileNumber = x.Field<String>("mobile_number"),
                            IdNo = x.Field<String>("id_no"),
                            Password = x.Field<String>("password"),
                            TransPwd = x.Field<String>("trans_pwd"),
                            UniqueKey = x.Field<String>("unique_key"),
                            UserRoleId = x.Field<Int32>("user_role_id"),
                            LastLoginDate = x.Field<DateTime?>("last_login_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            IsOnline = (x.Field<Int32>("is_online")),
                            IsActive = (x.Field<Int32>("is_active")),
                            IsPrimary = (x.Field<Int32>("is_primary")),
                            IsSecurityChecked = Convert.ToInt32(x.Field<Int32>("is_security_checked")),
                            Status = x.Field<Int32>("status"),
                            LastLoginIp = x.Field<String>("last_login_ip"),
                            RegisterIp = x.Field<String>("register_ip"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                            IsSatChecked = x.Field<Int32?>("is_sat_checked") == null ? 0 : x.Field<Int32>("is_sat_checked"),
                            PasswordResetCode = x.Field<String>("password_reset_code"),
                            ResetExpired = (x.Field<Int32>("reset_expired")),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                            VerifyExpired = Convert.ToInt32(x.Field<Int32>("verify_expired")),
                            LoginAttempts = x.Field<Int32>("login_attempts"),
                            SmsAttempts = x.Field<Int32>("sms_attempts"),
                            GoogleAuthAttempts = x.Field<Int32>("google_auth_attempts"),
                            IsLoginLocked = x.Field<Int32>("is_login_locked"),
                            IsSmsLocked = x.Field<Int32>("is_sms_locked"),
                            IsHardCopy = x.Field<Int32>("is_hard_copy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            EmailCode = x.Field<String>("email_code"),
                            ActivationDate = x.Field<DateTime?>("activation_date"),
                            IsAgent = x.Field<Int32>("is_agent"),
                            AgentCode = x.Field<String>("agent_code"),
                            AgentStatus = x.Field<Int32>("agent_status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                String UserId = nameof(UserSecurity.UserId);
                objs.ForEach(user =>
                {
                    Response responseUSList = IUserSecurityRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUSList.IsSuccess)
                    {
                        user.UserIdUserSecurities = (List<UserSecurity>)responseUSList.Data;
                    }
                    Response responseUAList = IUserAccountRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        user.UserIdUserAccounts = (List<UserAccount>)responseUAList.Data;
                    }
                    Response responseUDList = IUserDetailRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUDList.IsSuccess)
                    {
                        user.UserIdUserDetails = (List<UserDetail>)responseUDList.Data;
                    }
                    Response responseUR = IUserRoleRepo.GetSingle(user.UserRoleId);
                    if (responseUR.IsSuccess)
                    {
                        user.UserRoleIdUserRole = (UserRole)responseUR.Data;
                    }
                    Response responseUTList = IUserTypeRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUTList.IsSuccess)
                    {
                        ((List<UserType>)responseUTList.Data).ForEach(usertype =>
                        {
                            Response responseUTD = IUserTypesDefRepo.GetSingle(usertype.UserTypeId);
                            if (responseUTD.IsSuccess)
                            {
                                usertype.UserTypeIdUserTypesDef = (UserTypesDef)responseUTD.Data;
                            }
                        });
                        user.UserIdUserTypes = (List<UserType>)responseUTList.Data;
                    }

                    Response responseUFList = IUserFileRepo.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUFList.IsSuccess)
                    {
                        user.UserIdUserFiles = (List<UserFile>)responseUFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from users";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<User>(propertyName);
            List<User> objs = new List<User>();
            try
            {
                string query = "select * from users where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from users where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new User
                        {
                            Id = x.Field<Int32>("id"),
                            Username = x.Field<String>("username"),
                            EmailId = x.Field<String>("email_id"),
                            MobileNumber = x.Field<String>("mobile_number"),
                            IdNo = x.Field<String>("id_no"),
                            Password = x.Field<String>("password"),
                            TransPwd = x.Field<String>("trans_pwd"),
                            UniqueKey = x.Field<String>("unique_key"),
                            UserRoleId = x.Field<Int32>("user_role_id"),
                            LastLoginDate = x.Field<DateTime?>("last_login_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            IsOnline = (x.Field<Int32>("is_online")),
                            IsActive = (x.Field<Int32>("is_active")),
                            IsPrimary = (x.Field<Int32>("is_primary")),
                            IsSecurityChecked = Convert.ToInt32(x.Field<Int32>("is_security_checked")),
                            Status = x.Field<Int32>("status"),
                            LastLoginIp = x.Field<String>("last_login_ip"),
                            RegisterIp = x.Field<String>("register_ip"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                            IsSatChecked = x.Field<Int32?>("is_sat_checked") == null ? 0 : x.Field<Int32>("is_sat_checked"),
                            PasswordResetCode = x.Field<String>("password_reset_code"),
                            ResetExpired = (x.Field<Int32>("reset_expired")),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                            VerifyExpired = Convert.ToInt32(x.Field<Int32>("verify_expired")),
                            LoginAttempts = x.Field<Int32>("login_attempts"),
                            SmsAttempts = x.Field<Int32>("sms_attempts"),
                            GoogleAuthAttempts = x.Field<Int32>("google_auth_attempts"),
                            IsLoginLocked = x.Field<Int32>("is_login_locked"),
                            IsSmsLocked = x.Field<Int32>("is_sms_locked"),
                            IsHardCopy = x.Field<Int32>("is_hard_copy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            EmailCode = x.Field<String>("email_code"),
                            ActivationDate = x.Field<DateTime?>("activation_date"),
                            IsAgent = x.Field<Int32>("is_agent"),
                            AgentCode = x.Field<String>("agent_code"),
                            AgentStatus = x.Field<Int32>("agent_status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                String UserId = nameof(UserSecurity.UserId);
                objs.ForEach(user =>
                {
                    Response responseUSList = IUserSecurityRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUSList.IsSuccess)
                    {
                        user.UserIdUserSecurities = (List<UserSecurity>)responseUSList.Data;
                    }
                    Response responseUAList = IUserAccountRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        user.UserIdUserAccounts = (List<UserAccount>)responseUAList.Data;
                    }
                    Response responseUDList = IUserDetailRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUDList.IsSuccess)
                    {
                        user.UserIdUserDetails = (List<UserDetail>)responseUDList.Data;
                    }
                    Response responseUR = IUserRoleRepo.GetSingle(user.UserRoleId);
                    if (responseUR.IsSuccess)
                    {
                        user.UserRoleIdUserRole = (UserRole)responseUR.Data;
                    }
                    Response responseUTList = IUserTypeRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUTList.IsSuccess)
                    {
                        ((List<UserType>)responseUTList.Data).ForEach(usertype =>
                        {
                            Response responseUTD = IUserTypesDefRepo.GetSingle(usertype.UserTypeId);
                            if (responseUTD.IsSuccess)
                            {
                                usertype.UserTypeIdUserTypesDef = (UserTypesDef)responseUTD.Data;
                            }
                        });
                        user.UserIdUserTypes = (List<UserType>)responseUTList.Data;
                    }

                    Response responseUFList = IUserFileRepo.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUFList.IsSuccess)
                    {
                        user.UserIdUserFiles = (List<UserFile>)responseUFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<User>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from users where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from users where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                string query = "select * from users where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new User
                        {
                            Id = x.Field<Int32>("id"),
                            Username = x.Field<String>("username"),
                            EmailId = x.Field<String>("email_id"),
                            MobileNumber = x.Field<String>("mobile_number"),
                            IdNo = x.Field<String>("id_no"),
                            Password = x.Field<String>("password"),
                            TransPwd = x.Field<String>("trans_pwd"),
                            UniqueKey = x.Field<String>("unique_key"),
                            UserRoleId = x.Field<Int32>("user_role_id"),
                            LastLoginDate = x.Field<DateTime?>("last_login_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            IsOnline = (x.Field<Int32>("is_online")),
                            IsActive = (x.Field<Int32>("is_active")),
                            IsPrimary = (x.Field<Int32>("is_primary")),
                            IsSecurityChecked = Convert.ToInt32(x.Field<Int32>("is_security_checked")),
                            Status = x.Field<Int32>("status"),
                            LastLoginIp = x.Field<String>("last_login_ip"),
                            RegisterIp = x.Field<String>("register_ip"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                            IsSatChecked = x.Field<Int32?>("is_sat_checked") == null ? 0 : x.Field<Int32>("is_sat_checked"),
                            PasswordResetCode = x.Field<String>("password_reset_code"),
                            ResetExpired = (x.Field<Int32>("reset_expired")),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                            VerifyExpired = Convert.ToInt32(x.Field<Int32>("verify_expired")),
                            LoginAttempts = x.Field<Int32>("login_attempts"),
                            SmsAttempts = x.Field<Int32>("sms_attempts"),
                            GoogleAuthAttempts = x.Field<Int32>("google_auth_attempts"),
                            IsLoginLocked = x.Field<Int32>("is_login_locked"),
                            IsSmsLocked = x.Field<Int32>("is_sms_locked"),
                            IsHardCopy = x.Field<Int32>("is_hard_copy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            EmailCode = x.Field<String>("email_code"),
                            ActivationDate = x.Field<DateTime?>("activation_date"),
                            IsAgent = x.Field<Int32>("is_agent"),
                            AgentCode = x.Field<String>("agent_code"),
                            AgentStatus = x.Field<Int32>("agent_status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                String UserId = nameof(UserSecurity.UserId);
                objs.ForEach(user =>
                {
                    Response responseUSList = IUserSecurityRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUSList.IsSuccess)
                    {
                        user.UserIdUserSecurities = (List<UserSecurity>)responseUSList.Data;
                    }
                    Response responseUAList = IUserAccountRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        user.UserIdUserAccounts = (List<UserAccount>)responseUAList.Data;
                    }
                    Response responseUDList = IUserDetailRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUDList.IsSuccess)
                    {
                        user.UserIdUserDetails = (List<UserDetail>)responseUDList.Data;
                    }
                    Response responseUR = IUserRoleRepo.GetSingle(user.UserRoleId);
                    if (responseUR.IsSuccess)
                    {
                        user.UserRoleIdUserRole = (UserRole)responseUR.Data;
                    }
                    Response responseUTList = IUserTypeRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUTList.IsSuccess)
                    {
                        ((List<UserType>)responseUTList.Data).ForEach(usertype =>
                        {
                            Response responseUTD = IUserTypesDefRepo.GetSingle(usertype.UserTypeId);
                            if (responseUTD.IsSuccess)
                            {
                                usertype.UserTypeIdUserTypesDef = (UserTypesDef)responseUTD.Data;
                            }
                        });
                        user.UserIdUserTypes = (List<UserType>)responseUTList.Data;
                    }

                    Response responseUFList = IUserFileRepo.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUFList.IsSuccess)
                    {
                        user.UserIdUserFiles = (List<UserFile>)responseUFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from users where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }

        public Response GetDataByMA(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                string query = "select * from users INNER JOIN user_accounts ON user_accounts.user_id=users.ID where " + filter + " group by users.id ";
                if (isOrderByDesc)
                    query += " order by users.ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new User
                        {
                            Id = x.Field<Int32>("id"),
                            Username = x.Field<String>("username"),
                            EmailId = x.Field<String>("email_id"),
                            MobileNumber = x.Field<String>("mobile_number"),
                            IdNo = x.Field<String>("id_no"),
                            Password = x.Field<String>("password"),
                            TransPwd = x.Field<String>("trans_pwd"),
                            UniqueKey = x.Field<String>("unique_key"),
                            UserRoleId = x.Field<Int32>("user_role_id"),
                            LastLoginDate = x.Field<DateTime?>("last_login_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            IsOnline = (x.Field<Int32>("is_online")),
                            IsActive = (x.Field<Int32>("is_active")),
                            IsPrimary = (x.Field<Int32>("is_primary")),
                            IsSecurityChecked = Convert.ToInt32(x.Field<Int32>("is_security_checked")),
                            Status = x.Field<Int32>("status"),
                            LastLoginIp = x.Field<String>("last_login_ip"),
                            RegisterIp = x.Field<String>("register_ip"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                            IsSatChecked = x.Field<Int32?>("is_sat_checked") == null ? 0 : x.Field<Int32>("is_sat_checked"),
                            PasswordResetCode = x.Field<String>("password_reset_code"),
                            ResetExpired = (x.Field<Int32>("reset_expired")),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                            VerifyExpired = Convert.ToInt32(x.Field<Int32>("verify_expired")),
                            LoginAttempts = x.Field<Int32>("login_attempts"),
                            SmsAttempts = x.Field<Int32>("sms_attempts"),
                            GoogleAuthAttempts = x.Field<Int32>("google_auth_attempts"),
                            IsLoginLocked = x.Field<Int32>("is_login_locked"),
                            IsSmsLocked = x.Field<Int32>("is_sms_locked"),
                            IsHardCopy = x.Field<Int32>("is_hard_copy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            EmailCode = x.Field<String>("email_code"),
                            ActivationDate = x.Field<DateTime?>("activation_date"),
                            IsAgent = x.Field<Int32>("is_agent"),
                            AgentCode = x.Field<String>("agent_code"),
                            AgentStatus = x.Field<Int32>("agent_status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                String UserId = nameof(UserSecurity.UserId);
                objs.ForEach(user =>
                {
                    Response responseUSList = IUserSecurityRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUSList.IsSuccess)
                    {
                        user.UserIdUserSecurities = (List<UserSecurity>)responseUSList.Data;
                    }
                    Response responseUAList = IUserAccountRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        user.UserIdUserAccounts = (List<UserAccount>)responseUAList.Data;
                    }
                    Response responseUDList = IUserDetailRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUDList.IsSuccess)
                    {
                        user.UserIdUserDetails = (List<UserDetail>)responseUDList.Data;
                    }
                    Response responseUR = IUserRoleRepo.GetSingle(user.UserRoleId);
                    if (responseUR.IsSuccess)
                    {
                        user.UserRoleIdUserRole = (UserRole)responseUR.Data;
                    }
                    Response responseUTList = IUserTypeRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUTList.IsSuccess)
                    {
                        ((List<UserType>)responseUTList.Data).ForEach(usertype =>
                        {
                            Response responseUTD = IUserTypesDefRepo.GetSingle(usertype.UserTypeId);
                            if (responseUTD.IsSuccess)
                            {
                                usertype.UserTypeIdUserTypesDef = (UserTypesDef)responseUTD.Data;
                            }
                        });
                        user.UserIdUserTypes = (List<UserType>)responseUTList.Data;
                    }

                    Response responseUFList = IUserFileRepo.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUFList.IsSuccess)
                    {
                        user.UserIdUserFiles = (List<UserFile>)responseUFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public Int32 GetCountByMA(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from (select count(*) from users INNER JOIN user_accounts ON user_accounts.user_id=users.ID where " + filter + " group by users.id) a ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }

        public Response GetDataByUT(string filter, int skip, int take, bool isOrderByDesc, string jfilter, string jtable)
        {
            Response response = new Response();
            List<User> objs = new List<User>();
            try
            {
                string query = "select * from users INNER JOIN " + jtable + " ON " + jfilter + " where " + filter + " ";
                if (isOrderByDesc)
                    query += " order by users.ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new User
                        {
                            Id = x.Field<Int32>("id"),
                            Username = x.Field<String>("username"),
                            EmailId = x.Field<String>("email_id"),
                            MobileNumber = x.Field<String>("mobile_number"),
                            IdNo = x.Field<String>("id_no"),
                            Password = x.Field<String>("password"),
                            TransPwd = x.Field<String>("trans_pwd"),
                            UniqueKey = x.Field<String>("unique_key"),
                            UserRoleId = x.Field<Int32>("user_role_id"),
                            LastLoginDate = x.Field<DateTime?>("last_login_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            IsOnline = (x.Field<Int32>("is_online")),
                            IsActive = (x.Field<Int32>("is_active")),
                            IsPrimary = (x.Field<Int32>("is_primary")),
                            IsSecurityChecked = Convert.ToInt32(x.Field<Int32>("is_security_checked")),
                            Status = x.Field<Int32>("status"),
                            LastLoginIp = x.Field<String>("last_login_ip"),
                            RegisterIp = x.Field<String>("register_ip"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                            IsSatChecked = x.Field<Int32?>("is_sat_checked") == null ? 0 : x.Field<Int32>("is_sat_checked"),
                            PasswordResetCode = x.Field<String>("password_reset_code"),
                            ResetExpired = (x.Field<Int32>("reset_expired")),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                            VerifyExpired = Convert.ToInt32(x.Field<Int32>("verify_expired")),
                            LoginAttempts = x.Field<Int32>("login_attempts"),
                            SmsAttempts = x.Field<Int32>("sms_attempts"),
                            GoogleAuthAttempts = x.Field<Int32>("google_auth_attempts"),
                            IsLoginLocked = x.Field<Int32>("is_login_locked"),
                            IsSmsLocked = x.Field<Int32>("is_sms_locked"),
                            IsHardCopy = x.Field<Int32>("is_hard_copy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            EmailCode = x.Field<String>("email_code"),
                            ActivationDate = x.Field<DateTime?>("activation_date"),
                            IsAgent = x.Field<Int32>("is_agent"),
                            AgentCode = x.Field<String>("agent_code"),
                            AgentStatus = x.Field<Int32>("agent_status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                String UserId = nameof(UserSecurity.UserId);
                objs.ForEach(user =>
                {
                    Response responseUSList = IUserSecurityRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUSList.IsSuccess)
                    {
                        user.UserIdUserSecurities = (List<UserSecurity>)responseUSList.Data;
                    }
                    Response responseUAList = IUserAccountRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        user.UserIdUserAccounts = (List<UserAccount>)responseUAList.Data;
                    }
                    Response responseUDList = IUserDetailRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUDList.IsSuccess)
                    {
                        user.UserIdUserDetails = (List<UserDetail>)responseUDList.Data;
                    }
                    Response responseUR = IUserRoleRepo.GetSingle(user.UserRoleId);
                    if (responseUR.IsSuccess)
                    {
                        user.UserRoleIdUserRole = (UserRole)responseUR.Data;
                    }
                    Response responseUTList = IUserTypeRepo.GetDataByPropertyName(UserId, user.Id.ToString(), true, 0, 0, false);
                    if (responseUTList.IsSuccess)
                    {
                        ((List<UserType>)responseUTList.Data).ForEach(usertype =>
                        {
                            Response responseUTD = IUserTypesDefRepo.GetSingle(usertype.UserTypeId);
                            if (responseUTD.IsSuccess)
                            {
                                usertype.UserTypeIdUserTypesDef = (UserTypesDef)responseUTD.Data;
                            }
                        });
                        user.UserIdUserTypes = (List<UserType>)responseUTList.Data;
                    }

                    Response responseUFList = IUserFileRepo.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUFList.IsSuccess)
                    {
                        user.UserIdUserFiles = (List<UserFile>)responseUFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public Int32 GetCountByUT(string filter, string jfilter, string jtable)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from (select count(*) from users INNER JOIN " + jtable + " ON " + jfilter + " where " + filter + ") a ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
