﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class SAT_Form : System.Web.UI.Page
    {

        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyIUserAccountService = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyIUserAccountService.Value; } }

        private static readonly Lazy<IFormTypeService> lazyFormTypeServiceObj = new Lazy<IFormTypeService>(() => new FormTypeService());

        public static IFormTypeService IFormTypeService { get { return lazyFormTypeServiceObj.Value; } }

        private static readonly Lazy<IFormDataFieldValueService> lazyFormDataFieldValueServiceObj = new Lazy<IFormDataFieldValueService>(() => new FormDataFieldValueService());

        public static IFormDataFieldValueService IFormDataFieldValueService { get { return lazyFormDataFieldValueServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserLogSubService> lazyUserLogSubServiceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());
        public static IUserLogSubService IUserLogSubService { get { return lazyUserLogSubServiceObj.Value; } }

        public static string satRedirectUrl = "";

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (Session["user"] != null && Session["isVerified"] != null && Session["isAgent"] != null)
            {
                if (Session["isVerified"].ToString() == "1" && Session["isAgent"].ToString() == "1")
                {
                    Session["isAgentTrans"] = "1";
                    if (Session["isAgentTrans"] != null && Session["isAgentTrans"].ToString() == "1")
                    {
                        this.MasterPageFile = "~/AgentMaster.master";
                    }
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }

                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }

                String redirectUrl = Request.QueryString["redirectUrl"];
                if (!string.IsNullOrEmpty(redirectUrl))
                {
                    satRedirectUrl = redirectUrl;
                }
                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    Response response = IUserService.GetSingle(user.Id);
                    if (response.IsSuccess)
                    {
                        user = (User)response.Data;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }
                    Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                    if (IsImpersonate)
                    {
                        btnSubmit.Visible = false;
                        btnEdit.Visible = false;
                    }
                    String AccountNo = Request.QueryString["AccountNo"];
                    if (AccountNo != null)
                    {
                        if (Session["isAgentTrans"] != null && Session["AgentClientId"] != null && Session["isAgentTrans"].ToString() == "1")
                        {
                            String ClientId = HttpContext.Current.Session["AgentClientId"].ToString();
                            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + ClientId + "' and status='1' ", 0, 0, false);
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == AccountNo).FirstOrDefault();

                                //To check if the agent has alreayd submitted the SAT-FORM or not. If yes, its pending approval which is denoted by code 12
                                if (primaryAcc.IsSatChecked == 12)
                                {

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "ShowCustomMessage('Alert', \"" + "SAT is Pending Approval from Investor" + "\", ''); window.location='AgentClients.aspx'", true);
                                }
                                //Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                                //MaHolderReg maHolderReg = new MaHolderReg();
                                //if (responseMHR.IsSuccess)
                                //else
                                {
                                    //maHolderReg = (MaHolderReg)responseMHR.Data;

                                    string HolderClass = primaryAcc.HolderClass;
                                    string SATFormName = "";
                                    if (HolderClass == "BC" || HolderClass == "FC" || HolderClass == "NC" || HolderClass == "RC")
                                        SATFormName = "SAT (NON-INDIVIDUAL)";
                                    else
                                        SATFormName = "SAT (INDIVIDUAL)";
                                    string propName = nameof(FormType.Code);
                                    Response responseFTList = IFormTypeService.GetDataByPropertyName(propName, SATFormName, true, 0, 0, false);
                                    if (responseFTList.IsSuccess)
                                    {
                                        FormType SATForm = ((List<FormType>)responseFTList.Data).FirstOrDefault();
                                        if (!IsPostBack)
                                        {
                                            formNameSpan.InnerHtml = SATForm.Name;
                                        }
                                        BindForm(SATForm, primaryAcc);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFTList.Message + "\", '');", true);
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == AccountNo).FirstOrDefault();

                                //Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                                //MaHolderReg maHolderReg = new MaHolderReg();
                                //if (responseMHR.IsSuccess)
                                {
                                    //maHolderReg = (MaHolderReg)responseMHR.Data;
                                    if (primaryAcc.IsSatChecked == 12)
                                    { //If in pending status
                                        btnSubmit.Text = "Approve";
                                    }
                                    else {
                                        btnSubmit.Text = "Submit";
                                    }

                                    string HolderClass = primaryAcc.HolderClass;
                                    string SATFormName = "";
                                    if (HolderClass == "BC" || HolderClass == "FC" || HolderClass == "NC" || HolderClass == "RC")
                                        SATFormName = "SAT (NON-INDIVIDUAL)";
                                    else
                                        SATFormName = "SAT (INDIVIDUAL)";
                                    string propName = nameof(FormType.Code);
                                    Response responseFTList = IFormTypeService.GetDataByPropertyName(propName, SATFormName, true, 0, 0, false);
                                    if (responseFTList.IsSuccess)
                                    {
                                        FormType SATForm = ((List<FormType>)responseFTList.Data).FirstOrDefault();
                                        if (!IsPostBack)
                                        {
                                            formNameSpan.InnerHtml = SATForm.Name;
                                        }
                                        BindForm(SATForm, primaryAcc);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFTList.Message + "\", '');", true);
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void BindForm(FormType SATForm, UserAccount primaryAcc)
        {
            try
            {

                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                MaHolderReg maHolderReg = new MaHolderReg();
                if (responseMHR.IsSuccess)
                {
                    maHolderReg = (MaHolderReg)responseMHR.Data;
                }
                List<FormDataFieldValue> formDataFieldValues = new List<FormDataFieldValue>();
                if (primaryAcc.SatUpdatedDate != null)
                {
                    Response responseFDFVList = IFormDataFieldValueService.GetDataByPropertyName(nameof(FormDataFieldValue.CreatedDate), primaryAcc.SatUpdatedDate.Value.ToString("yyyy-MM-dd HH:mm:ss"), true, 0, 0, false);
                    if (responseFDFVList.IsSuccess)
                    {
                        formDataFieldValues = (List<FormDataFieldValue>)responseFDFVList.Data;
                        if (formDataFieldValues.Count == 0)
                        {
                            Response responseFDFVList1 = IFormDataFieldValueService.GetDataByPropertyName(nameof(FormDataFieldValue.UserAccountId), primaryAcc.Id.ToString(), true, 0, 0, true);
                            if (responseFDFVList1.IsSuccess)
                            {
                                formDataFieldValues = (List<FormDataFieldValue>)responseFDFVList1.Data;
                                formDataFieldValues = formDataFieldValues.GroupBy(x => x.FormDataFieldId).Select(grp => grp.FirstOrDefault()).ToList();
                            }
                        }
                        formDataFieldValues = formDataFieldValues.Where(x => x.UserAccountId == primaryAcc.Id).ToList();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFDFVList.Message + "\", '');", true);
                    }
                }

                List<FormDataField> fieldSet1 = SATForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 1).ToList();

                TextBox txtBox = new TextBox();

                txtBox.ID = fieldSet1[0].FieldId;
                txtBox.CssClass = "bottomborder";
                txtBox.ClientIDMode = ClientIDMode.Static;
                txtBox.Text = primaryAcc.AccountNo;
                txtBox.ReadOnly = true;

                if (formDataFieldValues.Count != 0)
                {
                    FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fieldSet1[0].Id).FirstOrDefault();
                    if (fdfv != null)
                    {
                        txtBox.Text = fdfv.Value;
                        txtBox.ReadOnly = true;
                    }
                }

                fieldSet1Div.Controls.Add(txtBox);

                int i = 0;
                List<FormDataField> fieldSet4 = SATForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 4).ToList();
                foreach (FormDataField fdf in fieldSet4)
                {
                    if (fdf.FieldType == "checkbox")
                    {
                        var grpFieldSetOptions = fdf.FormDataFieldIdFormDataFieldOptions.GroupBy(x => x.Name.Split('-')[0]).Select(grp => grp.ToList()).ToList();
                        foreach (var fdfos in grpFieldSetOptions)
                        {
                            if (fdf.FormDataFieldIdFormDataFieldOptions.Count > 0)
                            {
                                i = 0;
                                HtmlTableCell cellLabelCategory = new HtmlTableCell();
                                cellLabelCategory.InnerText = fdfos.FirstOrDefault().Name.Split('-')[0];
                                cellLabelCategory.RowSpan = fdfos.Count;
                                foreach (var fdfo in fdfos)
                                {
                                    HtmlTableRow row = new HtmlTableRow();
                                    if (i == 0)
                                    {
                                        row.Controls.Add(cellLabelCategory);
                                        i++;
                                    }
                                    HtmlTableCell cellLabel = new HtmlTableCell();
                                    HtmlTableCell cell = new HtmlTableCell();
                                    Label label = new Label();
                                    label.AssociatedControlID = fdf.FieldId + fdfo.Score;
                                    label.Text = fdfo.Name.Split('-')[1];
                                    cellLabel.Controls.Add(label);
                                    row.Controls.Add(cellLabel);

                                    CheckBoxList cb = new CheckBoxList();
                                    //CheckBox cb = new CheckBox();
                                    cb.ID = fdf.FieldId + fdfo.Score;
                                    cb.ClientIDMode = ClientIDMode.Static;
                                    ListItem listItem = new ListItem { Text = "", Value = fdfo.Score.ToString() };
                                    if (formDataFieldValues.Count != 0)
                                    {
                                        FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fdf.Id).FirstOrDefault();
                                        if (fdfv != null && listItem.Value == fdfv.Value)
                                        {
                                            listItem.Selected = true;
                                        }
                                    }
                                    cb.Items.Add(listItem);

                                    cell.Controls.Add(cb);
                                    row.Controls.Add(cell);
                                    fieldSet4Tbody.Controls.Add(row);
                                }
                            }
                        }
                    }
                }
                i = 0;
                List<FormDataField> fieldSet2 = SATForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 2).ToList();
                foreach (FormDataField fdf in fieldSet2)
                {
                    HtmlTableRow row = new HtmlTableRow();
                    if (i == 0)
                    {
                        HtmlTableCell cell = new HtmlTableCell();

                        cell.InnerHtml = fieldSet2[0].Name;
                        row.Controls.Add(cell);

                        cell = new HtmlTableCell();

                        txtBox = new TextBox();
                        txtBox.ID = fieldSet2[0].FieldId;
                        txtBox.CssClass = "noborder";
                        txtBox.ClientIDMode = ClientIDMode.Static;

                        if (fieldSet2[0].FieldId.ToLower().Contains("date"))
                        {
                            txtBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
                            txtBox.ReadOnly = true;
                        }

                        if (formDataFieldValues.Count != 0)
                        {
                            FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fieldSet2[0].Id).FirstOrDefault();
                            if (fdfv != null)
                            {
                                txtBox.Text = fdfv.Value;
                                txtBox.ReadOnly = true;
                            }
                        }

                        cell.Controls.Add(txtBox);
                        row.Controls.Add(cell);


                        cell = new HtmlTableCell();
                        cell.InnerHtml = fieldSet2[1].Name;
                        row.Controls.Add(cell);

                        txtBox = new TextBox();
                        txtBox.ID = fieldSet2[1].FieldId;
                        txtBox.CssClass = "noborder";
                        txtBox.ClientIDMode = ClientIDMode.Static;

                        if (fieldSet2[1].FieldId.ToLower().Contains("idno"))
                        {
                            txtBox.Text = primaryAcc.IdNo;
                            txtBox.ReadOnly = true;
                        }

                        if (formDataFieldValues.Count != 0)
                        {
                            FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fieldSet2[1].Id).FirstOrDefault();
                            if (fdfv != null)
                            {
                                txtBox.Text = fdfv.Value;
                                txtBox.ReadOnly = true;
                            }
                        }

                        cell = new HtmlTableCell();
                        cell.Controls.Add(txtBox);
                        row.Controls.Add(cell);
                    }
                    if (i > 1)
                    {
                        HtmlTableCell cell = new HtmlTableCell();

                        cell.InnerHtml = fieldSet2[i].Name;
                        row.Controls.Add(cell);

                        cell = new HtmlTableCell();
                        cell.ColSpan = 3;
                        if (fdf.FieldType == "text" || fdf.FieldType == "date")
                        {
                            txtBox = new TextBox();
                            txtBox.ID = fieldSet2[i].FieldId;
                            txtBox.CssClass = "noborder";
                            txtBox.ClientIDMode = ClientIDMode.Static;
                            if (fieldSet2[i].FieldId.ToLower().Contains("name"))
                            {
                                txtBox.Text = maHolderReg.Name1 == null ? "Test" : maHolderReg.Name1;
                                txtBox.ReadOnly = true;
                            }

                            if (formDataFieldValues.Count != 0)
                            {
                                FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fieldSet2[i].Id).FirstOrDefault();
                                if (fdfv != null)
                                {
                                    txtBox.Text = fdfv.Value;
                                    txtBox.ReadOnly = true;
                                }
                            }

                            cell.Controls.Add(txtBox);
                            row.Controls.Add(cell);
                        }

                        if (fdf.FieldType == "checkbox")
                        {
                            CheckBoxList cb = new CheckBoxList();

                            if (fdf.FormDataFieldIdFormDataFieldOptions.Where(x => x.Name.Length > 60).Count() > 0)
                                cb.RepeatDirection = RepeatDirection.Vertical;
                            else
                            {
                                if (fdf.FormDataFieldIdFormDataFieldOptions.Count > 5)
                                {
                                    cb.RepeatColumns = 4;
                                    cb.RepeatDirection = RepeatDirection.Vertical;
                                }
                                else
                                    cb.RepeatDirection = RepeatDirection.Horizontal;
                            }



                            var FormDataFieldIdFormDataFieldOptions = (from fdfo in fdf.FormDataFieldIdFormDataFieldOptions
                                                                       select new
                                                                       {
                                                                           Name = fdfo.Name,
                                                                           Score = fdfo.Score
                                                                       }).ToList();
                            cb.ID = fdf.FieldId;
                            cb.ClientIDMode = ClientIDMode.Static;
                            cb.CssClass = "checkboxlist";
                            cb.DataSource = FormDataFieldIdFormDataFieldOptions;
                            cb.DataTextField = nameof(FormDataFieldOption.Name);
                            cb.DataValueField = nameof(FormDataFieldOption.Score);
                            cb.DataBind();



                            if (formDataFieldValues.Count != 0)
                            {
                                FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fdf.Id).FirstOrDefault();
                                if (fdfv != null)
                                {
                                    cb.SelectedValue = fdfv.Value;
                                }
                            }

                            cell.Controls.Add(cb);

                            if (FormDataFieldIdFormDataFieldOptions.Where(x => x.Name == "Others, Please specify").ToList().Count > 0)
                            {
                                txtBox = new TextBox();
                                txtBox.ID = fieldSet2[i].FieldId + "_Others";
                                txtBox.CssClass = "noborder";
                                txtBox.ClientIDMode = ClientIDMode.Static;

                                if (formDataFieldValues.Count != 0)
                                {
                                    FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fdf.Id).FirstOrDefault();
                                    if (fdfv != null && (cb.SelectedValue == null || cb.SelectedValue == ""))
                                    {
                                        cb.SelectedValue = cb.Items[cb.Items.Count - 1].Value;
                                        txtBox.Text = fdfv.Value;
                                    }
                                }

                                cell.Controls.Add(txtBox);

                            }


                            row.Controls.Add(cell);
                        }


                    }
                    fieldSet2Tbody.Controls.Add(row);
                    i++;

                }


                List<FormDataField> fieldSet3 = SATForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 3).ToList();
                i = 1;
                foreach (FormDataField fdf in fieldSet3)
                {
                    HtmlTableRow row = new HtmlTableRow();

                    HtmlTableCell cell = new HtmlTableCell();
                    cell.InnerHtml = i + ".";
                    row.Controls.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerHtml = fdf.Name + "<br />";
                    if (fdf.FieldType == "checkbox")
                    {
                        CheckBoxList cb = new CheckBoxList();

                        if (fdf.FormDataFieldIdFormDataFieldOptions.Where(x => x.Name.Length > 60).Count() > 0)
                            cb.RepeatDirection = RepeatDirection.Vertical;
                        else
                        {
                            if (fdf.FormDataFieldIdFormDataFieldOptions.Count > 5)
                            {
                                cb.RepeatColumns = 4;
                                cb.RepeatDirection = RepeatDirection.Vertical;
                            }
                            else
                                cb.RepeatDirection = RepeatDirection.Horizontal;
                        }

                        string bornyear = maHolderReg.BirthDt == null ? "09/05/1990" : maHolderReg.BirthDt;
                        string bornmonth = bornyear.Substring(0, 2);
                        int month = Convert.ToInt32(new string(bornmonth.Where(Char.IsDigit).ToArray()));

                        int Byear = Convert.ToInt32(bornyear.Substring(bornyear.Length - 4));
                        int Nyear = Convert.ToInt32(DateTime.Now.Year);

                        int Age = Nyear - Byear;

                        var FormDataFieldIdFormDataFieldOptions = (from fdfo in fdf.FormDataFieldIdFormDataFieldOptions
                                                                   select new
                                                                   {
                                                                       Name = fdfo.Name + " (" + fdfo.Score + ")",
                                                                       Score = fdfo.Score
                                                                   }).ToList();
                        cb.ID = fdf.FieldId;
                        cb.ClientIDMode = ClientIDMode.Static;
                        cb.CssClass = "checkboxlist";
                        cb.DataSource = FormDataFieldIdFormDataFieldOptions;
                        cb.DataTextField = nameof(FormDataFieldOption.Name);
                        cb.DataValueField = nameof(FormDataFieldOption.Score);
                        cb.DataBind();


                        if (fdf.FieldId == "CurrentAge")
                        {
                            if (Age > 60 && month >= 7)
                                cb.SelectedValue = "1";
                            else if (Age > 50 && Age <= 60 || (Age == 50 && month >= 7))
                                cb.SelectedValue = "2";
                            else if (Age > 40 && Age <= 50 || (Age == 40 && month >= 7))
                                cb.SelectedValue = "3";
                            else if (Age > 30 && Age <= 40 || (Age == 30 && month >= 7))
                                cb.SelectedValue = "4";
                            else
                                cb.SelectedValue = "5";

                            cb.Enabled = false;
                        }
                        else if (formDataFieldValues.Count != 0)
                        {
                            FormDataFieldValue fdfv = formDataFieldValues.Where(x => x.FormDataFieldId == fdf.Id).FirstOrDefault();
                            if (fdfv != null)
                            {
                                //Check for 4 values split
                                cb.SelectedValue = fdfv.Value;
                                if (cb.RepeatColumns == 4)
                                {
                                    string[] fdfvSplits = fdfv.Value.Split(',');
                                    int idx = 0;
                                    int repeat = 0;
                                    foreach (ListItem li in cb.Items)
                                    {
                                        if (repeat < 4)
                                        {
                                            if (cb.Items[idx].Value == fdfvSplits[repeat])
                                            {
                                                cb.Items[idx].Selected = true;
                                            }
                                            if (cb.Items[idx + 1].Value == fdfvSplits[repeat])
                                            {
                                                cb.Items[idx + 1].Selected = true;
                                            }
                                            if (cb.Items[idx + 2].Value == fdfvSplits[repeat])
                                            {
                                                cb.Items[idx + 2].Selected = true;
                                            }
                                        }
                                        idx = idx + 3;
                                        repeat++;
                                    }
                                }
                            }
                        }

                        cell.Controls.Add(cb);

                    }
                    row.Controls.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerHtml = "";
                    row.Controls.Add(cell);

                    fieldSet3Tbody.Controls.Add(row);
                    i++;
                }
                if (formDataFieldValues.Count != 0)
                {
                    hdnIsEdit.Value = "1";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("SAT-Form Page BindForm: " + ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            String AccountNo = Request.QueryString["AccountNo"];
            if (AccountNo != null)
            {
                try
                {
                    User user = (User)Session["user"];
                    if (Session["isAgentTrans"] != null && Session["AgentClientId"] != null && Session["isAgentTrans"].ToString() == "1")
                    {
                        String ClientId = HttpContext.Current.Session["AgentClientId"].ToString();
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + ClientId + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == AccountNo).FirstOrDefault();

                            List<FormDataFieldValue> formDataFieldValues = new List<FormDataFieldValue>();
                            if (!string.IsNullOrEmpty(hdnScoreGrandTotal.Value) && hdnScoreGrandTotal.Value != "0")
                            {
                                Int32 grandTotal = Convert.ToInt32(hdnScoreGrandTotal.Value);

                                string propName = nameof(FormType.Code);
                                Response responseFTList = IFormTypeService.GetDataByPropertyName(propName, "SAT (INDIVIDUAL)", true, 0, 0, false);
                                if (responseFTList.IsSuccess)
                                {
                                    FormType SATNonIndividualForm = ((List<FormType>)responseFTList.Data).FirstOrDefault();
                                    List<FormDataField> fieldSet1 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 1).ToList();
                                    List<FormDataField> fieldSet2 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 2).ToList();
                                    List<FormDataField> fieldSet4 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 4).ToList();
                                    List<FormDataField> fieldSet3 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 3).ToList();
                                    foreach (FormDataField fdf in fieldSet1)
                                    {
                                        TextBox txtInstance = (TextBox)fieldSet1Div.FindControl(fdf.FieldId);
                                        string value = txtInstance.Text;

                                        FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                        formDataFieldValue.FormDataFieldId = fdf.Id;
                                        formDataFieldValue.Value = value;
                                        formDataFieldValue.UserAccountId = primaryAcc.Id;
                                        formDataFieldValues.Add(formDataFieldValue);
                                    }

                                    foreach (FormDataField fdf in fieldSet2)
                                    {
                                        if (fdf.FieldType == "text" || fdf.FieldType == "date")
                                        {
                                            TextBox txtInstance = (TextBox)fieldSet2Tbody.FindControl(fdf.FieldId);
                                            string value = txtInstance.Text;

                                            FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                            formDataFieldValue.FormDataFieldId = fdf.Id;
                                            formDataFieldValue.Value = value;
                                            formDataFieldValue.UserAccountId = primaryAcc.Id;
                                            formDataFieldValues.Add(formDataFieldValue);
                                        }
                                        else if (fdf.FieldType == "checkbox")
                                        {
                                            CheckBoxList chkInstance = (CheckBoxList)fieldSet3Tbody.FindControl(fdf.FieldId);
                                            string value = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList());

                                            string text = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Text).ToList());
                                            if (text.Contains("Other"))
                                            {
                                                TextBox txtInstance = (TextBox)fieldSet2Tbody.FindControl(fdf.FieldId + "_Others");
                                                value = txtInstance.Text;
                                            }
                                            FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                            formDataFieldValue.FormDataFieldId = fdf.Id;
                                            formDataFieldValue.Value = value;
                                            formDataFieldValue.UserAccountId = primaryAcc.Id;
                                            formDataFieldValues.Add(formDataFieldValue);
                                        }

                                    }

                                    foreach (FormDataField fdf in fieldSet4)
                                    {
                                        if (fdf.FieldType == "checkbox")
                                        {
                                            foreach (FormDataFieldOption fdfo in fdf.FormDataFieldIdFormDataFieldOptions)
                                            {
                                                CheckBoxList chkInstance = (CheckBoxList)fieldSet4Tbody.FindControl(fdf.FieldId + fdfo.Score);
                                                string value = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList());
                                                if (value != "")
                                                {
                                                    FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                                    formDataFieldValue.FormDataFieldId = fdf.Id;
                                                    formDataFieldValue.Value = value;
                                                    formDataFieldValue.UserAccountId = primaryAcc.Id;
                                                    formDataFieldValues.Add(formDataFieldValue);
                                                }
                                            }
                                        }

                                    }

                                    foreach (FormDataField fdf in fieldSet3)
                                    {
                                        CheckBoxList chkInstance = (CheckBoxList)fieldSet3Tbody.FindControl(fdf.FieldId);
                                        string value = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList());

                                        FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                        formDataFieldValue.FormDataFieldId = fdf.Id;
                                        formDataFieldValue.Value = value;
                                        formDataFieldValue.UserAccountId = primaryAcc.Id;
                                        formDataFieldValues.Add(formDataFieldValue);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFTList.Message + "\", '');", true);
                                }
                            }
                            DateTime currentDate = DateTime.Now;
                            formDataFieldValues.Select(x =>
                            {
                                x.CreatedDate = currentDate;
                                return x;
                            }).ToList();
                            IFormDataFieldValueService.PostBulkData(formDataFieldValues);

                            string oldDate = (primaryAcc.SatUpdatedDate.HasValue ? primaryAcc.SatUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "");
                            string oldScore = primaryAcc.SatScore.ToString();

                            primaryAcc.IsSatChecked = 12;
                            //primaryAcc.SatUpdatedDate = currentDate;
                            primaryAcc.SatScore = Convert.ToInt32(hdnScoreGrandTotal.Value);
                            IUserAccountService.UpdateData(primaryAcc);

                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Update SAT successful",
                                TableName = "user_accounts",
                                UpdatedDate = DateTime.Now,
                                UserId = primaryAcc.UserId,
                                UserAccountId = primaryAcc.Id,
                                RefId = primaryAcc.Id,
                                RefValue = primaryAcc.SatScore.ToString(),
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (responseLog.IsSuccess)
                            {
                                ulm = (UserLogMain)responseLog.Data;
                                UserLogSub uls1 = new UserLogSub
                                {
                                    UserLogMainId = ulm.Id,
                                    ColumnName = "sat_score",
                                    ValueOld = oldScore,
                                    ValueNew = primaryAcc.SatScore.ToString()
                                };
                                Response responseLogSub1 = IUserLogSubService.PostData(uls1);
                                UserLogSub uls2 = new UserLogSub
                                {
                                    UserLogMainId = ulm.Id,
                                    ColumnName = "sat_updated_date",
                                    ValueOld = oldDate,
                                    ValueNew = primaryAcc.SatUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss")
                                };
                                Response responseLogSub2 = IUserLogSubService.PostData(uls2);
                            }
                            else
                            {
                                //Audit log failed
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "RedirectToUrl('/" + satRedirectUrl + "?AccountNo=" + primaryAcc.AccountNo + "');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == AccountNo).FirstOrDefault();

                            List<FormDataFieldValue> formDataFieldValues = new List<FormDataFieldValue>();
                            if (!string.IsNullOrEmpty(hdnScoreGrandTotal.Value) && hdnScoreGrandTotal.Value != "0")
                            {
                                Int32 grandTotal = Convert.ToInt32(hdnScoreGrandTotal.Value);

                                string propName = nameof(FormType.Code);
                                Response responseFTList = IFormTypeService.GetDataByPropertyName(propName, "SAT (INDIVIDUAL)", true, 0, 0, false);
                                if (responseFTList.IsSuccess)
                                {
                                    FormType SATNonIndividualForm = ((List<FormType>)responseFTList.Data).FirstOrDefault();
                                    List<FormDataField> fieldSet1 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 1).ToList();
                                    List<FormDataField> fieldSet2 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 2).ToList();
                                    List<FormDataField> fieldSet4 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 4).ToList();
                                    List<FormDataField> fieldSet3 = SATNonIndividualForm.FormTypeIdFormDataFields.Where(x => x.FieldSet == 3).ToList();
                                    foreach (FormDataField fdf in fieldSet1)
                                    {
                                        TextBox txtInstance = (TextBox)fieldSet1Div.FindControl(fdf.FieldId);
                                        string value = txtInstance.Text;

                                        FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                        formDataFieldValue.FormDataFieldId = fdf.Id;
                                        formDataFieldValue.Value = value;
                                        formDataFieldValue.UserAccountId = primaryAcc.Id;
                                        formDataFieldValues.Add(formDataFieldValue);
                                    }

                                    foreach (FormDataField fdf in fieldSet2)
                                    {
                                        if (fdf.FieldType == "text" || fdf.FieldType == "date")
                                        {
                                            TextBox txtInstance = (TextBox)fieldSet2Tbody.FindControl(fdf.FieldId);
                                            string value = txtInstance.Text;

                                            FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                            formDataFieldValue.FormDataFieldId = fdf.Id;
                                            formDataFieldValue.Value = value;
                                            formDataFieldValue.UserAccountId = primaryAcc.Id;
                                            formDataFieldValues.Add(formDataFieldValue);
                                        }
                                        else if (fdf.FieldType == "checkbox")
                                        {
                                            CheckBoxList chkInstance = (CheckBoxList)fieldSet3Tbody.FindControl(fdf.FieldId);
                                            string value = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList());

                                            string text = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Text).ToList());
                                            if (text.Contains("Other"))
                                            {
                                                TextBox txtInstance = (TextBox)fieldSet2Tbody.FindControl(fdf.FieldId + "_Others");
                                                value = txtInstance.Text;
                                            }
                                            FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                            formDataFieldValue.FormDataFieldId = fdf.Id;
                                            formDataFieldValue.Value = value;
                                            formDataFieldValue.UserAccountId = primaryAcc.Id;
                                            formDataFieldValues.Add(formDataFieldValue);
                                        }

                                    }

                                    foreach (FormDataField fdf in fieldSet4)
                                    {
                                        if (fdf.FieldType == "checkbox")
                                        {
                                            foreach (FormDataFieldOption fdfo in fdf.FormDataFieldIdFormDataFieldOptions)
                                            {
                                                CheckBoxList chkInstance = (CheckBoxList)fieldSet4Tbody.FindControl(fdf.FieldId + fdfo.Score);
                                                string value = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList());
                                                if (value != "")
                                                {
                                                    FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                                    formDataFieldValue.FormDataFieldId = fdf.Id;
                                                    formDataFieldValue.Value = value;
                                                    formDataFieldValue.UserAccountId = primaryAcc.Id;
                                                    formDataFieldValues.Add(formDataFieldValue);
                                                }
                                            }
                                        }

                                    }

                                    foreach (FormDataField fdf in fieldSet3)
                                    {
                                        CheckBoxList chkInstance = (CheckBoxList)fieldSet3Tbody.FindControl(fdf.FieldId);
                                        string value = String.Join(",", chkInstance.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList());

                                        FormDataFieldValue formDataFieldValue = new FormDataFieldValue();
                                        formDataFieldValue.FormDataFieldId = fdf.Id;
                                        formDataFieldValue.Value = value;
                                        formDataFieldValue.UserAccountId = primaryAcc.Id;
                                        formDataFieldValues.Add(formDataFieldValue);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFTList.Message + "\", '');", true);
                                }
                            }
                            DateTime currentDate = DateTime.Now;
                            formDataFieldValues.Select(x => { x.CreatedDate = currentDate; return x; }).ToList();
                            IFormDataFieldValueService.PostBulkData(formDataFieldValues);

                            string oldDate = (primaryAcc.SatUpdatedDate.HasValue ? primaryAcc.SatUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "");
                            string oldScore = primaryAcc.SatScore.ToString();

                            primaryAcc.IsSatChecked = 1;
                            primaryAcc.SatUpdatedDate = currentDate;
                            primaryAcc.SatScore = Convert.ToInt32(hdnScoreGrandTotal.Value);
                            IUserAccountService.UpdateData(primaryAcc);

                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Update SAT successful",
                                TableName = "user_accounts",
                                UpdatedDate = DateTime.Now,
                                UserId = primaryAcc.UserId,
                                UserAccountId = primaryAcc.Id,
                                RefId = primaryAcc.Id,
                                RefValue = primaryAcc.SatScore.ToString(),
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (responseLog.IsSuccess)
                            {
                                ulm = (UserLogMain)responseLog.Data;
                                UserLogSub uls1 = new UserLogSub
                                {
                                    UserLogMainId = ulm.Id,
                                    ColumnName = "sat_score",
                                    ValueOld = oldScore,
                                    ValueNew = primaryAcc.SatScore.ToString()
                                };
                                Response responseLogSub1 = IUserLogSubService.PostData(uls1);
                                UserLogSub uls2 = new UserLogSub
                                {
                                    UserLogMainId = ulm.Id,
                                    ColumnName = "sat_updated_date",
                                    ValueOld = oldDate,
                                    ValueNew = primaryAcc.SatUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss")
                                };
                                Response responseLogSub2 = IUserLogSubService.PostData(uls2);
                            }
                            else
                            {
                                //Audit log failed
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "RedirectToUrl('/" + satRedirectUrl + "?AccountNo=" + primaryAcc.AccountNo + "');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog("SAT-Form Page btnSubmit_Click: " + ex.Message);
                }
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static string getAge()
        {
            User user = (User)HttpContext.Current.Session["user"];
            string age = "";
            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
            }
            return age;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            String AccountNo = Request.QueryString["AccountNo"];
            if (AccountNo != null)
                Response.Redirect("Settings.aspx?AccountNo=" + AccountNo, false);
            else
                Response.Redirect("Settings.aspx", false);
        }
    }
}