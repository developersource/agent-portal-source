﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourseAttendanceNoAssessment.aspx.cs" Inherits="DiOTP.WebApp.CourseAttendanceNoAssessment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/Content/assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/Content/assets/css/style.css" />
    <style>

        .attendance-card {
            display: block;
            position: relative;
            padding: 5px 20px 20px 20px;
            box-shadow: 0px 0px 10px #d6d6d6;
            width: 100%;
            margin: 0 auto;
            background-color:honeydew;
        }

        .courseDetailCard{
            background-color:aliceblue;
            padding: 5px 20px 20px 20px;
            box-shadow: 0px 0px 10px #d6d6d6;
            width: 100%;
            margin: 0 auto;
        }

        .detailsCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        padding-top:10px;
                        word-wrap: break-word;
                        background-color: aliceblue;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
        }

        .detailsTable td , .detailsTable th, .detailsTable table, .detailsTable tr{
            border:none !important;
        }

        .detailsTable th {
            background-color:darkcyan !important;
        }
    </style>
</head>
<body>
    <form id="courseAttendanceNoAssessmentForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="attendance-card" style="width:75%;margin-top:50px;position:fixed;left:13%;">
                        <h3>Training Attendance</h3>
                        <%--<span>08.10.2021</span>--%>
                        <hr />

                        <div class="row">
                            <div class="col-md-5 detailsCard" style="margin-left:20px;padding-bottom:25px;">
                                <table class="table table-condensed detailsTable table-responsive">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Training Course Details</th>
                                        </tr>
                                    </thead>
                                    <tbody class="agentDetailTable text-center">
                                        <tr>
                                            <td>Training Course Title:</td>
                                            <td>
                                                <b><asp:Label ID="lblTrainingCourseTitle" Text="Agent Rapport Training" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Training Course Date:</td>
                                            <td>
                                                <b><asp:Label ID="lblTrainingCourseDate" Text="21/10/2021" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CPD Points Allocated:</td>
                                            <td>
                                                <b><asp:Label ID="lblCpdPoints" Text="6 CPD POINTS" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-6 detailsCard pull-right" style="margin-right:20px;">
                                <table class="table table-condensed detailsTable table-responsive">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Attendee Details</th>
                                        </tr>
                                    </thead>
                                    <tbody class="agentDetailTable text-center">
                                        <tr>
                                            <td>Attendee NRIC:</td>
                                            <td>
                                                <%--<asp:TextBox ID="txtAttendeeNRIC" placeholder="Example: 881015158432" runat="server"></asp:TextBox>--%>
                                                 <input type="text" id="txtAttendeeNRIC" placeholder="Example: 881015158432" runat="server" style="width: -webkit-fill-available;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Attendee Agent Code:</td>
                                            <td>
                                                <%--<asp:TextBox ID="txtAttendeeAgentCode" placeholder="Enter Agent Code" runat="server"></asp:TextBox>--%>
                                                <input type="text" id="txtAttendeeAgentCode" placeholder="Example: AG1234" runat="server" style="width: -webkit-fill-available;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input type="submit" name="submit" value="Submit" id="submitBtn" runat="server" class="btn btn-primary pull-right" style="width: 20%;border-radius:5px;"/>
                                               <%--<asp:Button class="btn btn-primary pull-right" name="submit" ID="submit" Text="Submit" runat="server"  ClientIDMode="Static"/>--%>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr />
                        <h5>Petraware Training Agency Team</h5>
                        <span id="currentDate"></span>
                    </div>                    
                </div>               
            </div>
        </div>
    </form>

    <script>
        var today = new Date();
        var currDate = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
        document.getElementById("currentDate").innerText = currDate;
    </script>
</body>
</html>
