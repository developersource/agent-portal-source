﻿<%-- <%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="AgentDashboard.aspx.cs" Inherits="DiOTP.WebApp.AgentDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Dashboard</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Dashboard</h3>
            </div>
            <div id="containerPage">
                <style>
                    .cpd-points-count {
                        height: 150px;
                        border: 5px solid #e4b418;
                        text-align: center;
                    }
                    .cpd-points-count .head {
                        text-align: center;
                        background: #e4b418;
                        height: 25px;
                        line-height: 22px;
                        font-weight: 900;
                        color: #fff;
                        letter-spacing: 1.5px;
                    }
                    .cpd-points-count .cpd-year {
                        font-weight: 500;
                        letter-spacing: 1px;
                    }
                    .cpd-points-count .cpd-points-value {
                        line-height: 110px;
                        font-size: 50px;
                        font-weight: 900;
                    }
                </style>
                
                <div class="row">
                    <div class="col-md-10">
                    </div>
                    <div class="col-md-2">
                        <div class="cpd-points-count">
                            <div class="head">CPD POINTS <span class="cpd-year" id="CPDYear" runat="server"></span></div>
                            <span class="cpd-points-value" id="lblCPDPoints" runat="server"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content> --%>

<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="AgentDashboards.aspx.cs" Inherits="DiOTP.WebApp.AgentDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Dashboard</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Dashboard</h3>
            </div>
            <div id="containerPage">
                <style>
                    .cpd-points-count {
                        height: 150px;
                        border: 4px solid #e4b418;
                        text-align: center;
                    }

                        .cpd-points-count .head {
                            text-align: center;
                            background: #e4b418;
                            height: 25px;
                            line-height: 22px;
                            font-weight: 900;
                            color: #fff;
                            letter-spacing: 1.5px;
                        }

                        .cpd-points-count .cpd-year {
                            font-weight: 500;
                            letter-spacing: 1px;
                        }

                        .cpd-points-count .cpd-points-value {
                            line-height: 110px;
                            font-size: 50px;
                            font-weight: 900;
                        }

                    .trTextFormat {
                        text-align: center;
                    }

                    .agentDashboardHr {
                        margin-top: 20px;
                        margin-bottom: 20px;
                    }

                    .agentDashboardProfDetails {
                        margin-bottom: 5px;
                        border-bottom: 1px solid #808080;
                    }

                    .agentCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: ghostwhite;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                    }

                    .agentCommissionCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: lightyellow;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                    }

                    .agentTransactionCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: aliceblue;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                    }

                    .agentDetailTable, tbody.agentDetailTable > tr{
                    
                        border:2px solid grey;
                        
                    }
                </style>

                <%--Segment 1--%>
                <div class="container agentCard">
                    <div class="row">
                        <div class="col-md-3">
                            &nbsp
                        </div>
                        <div class="col-md-6">
                            <div class="row"><h4>My Personal Details:</h4>
                            
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-3">
                            <div id="agentImage" runat="server">

                            </div>
                            <%--Agent Image--%>

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                
                                <table class="table table-bordered table-condensed agentDetailTable table-responsive">
                                    <tbody class="agentDetailTable">
                                        <tr>
                                            <td>Agent Name:</td>
                                            <td>
                                                <asp:Label ID="lblAgentName" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Agent Code:</td>
                                            <td>
                                                <asp:Label ID="lblAgentCode" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rank:</td>
                                            <td>
                                                <asp:Label ID="lblRank" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Join Date:</td>
                                            <td>
                                                <asp:Label ID="lblJoinDate" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Agency Group:</td>
                                            <td>
                                                <asp:Label ID="lblAgencyGroup" Text="Galaxy Agency Group" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Region/Branch:</td>
                                            <td>
                                                <asp:Label ID="lblRegion" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Business Development Manager:</td>
                                            <td>
                                                <asp:Label ID="lblBDM" Text="Ms. Clarkson, 012-8752613" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Direct Downline:</td>
                                            <td>
                                                <asp:Label ID="lblDirectDownline" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Indirect Downline:</td>
                                            <td>
                                                <asp:Label ID="lblIndirectDownline" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div class="col-md-3">
                            <div class="cpd-points-count">
                            <div class="head">CPD POINTS <span class="cpd-year" id="CPDYear" runat="server"></span></div>
                            <span class="cpd-points-value" id="lblCPDPoints" runat="server"></span>
                        </div>
                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 10px;">                       
                        <div class="col-md-6 col-md-offset-3">
                            <div class="row">
                                <h4>My Upline Agent Details</h4>
                                <table class="table table-bordered table-condensed table-responsive agentDetailTable">
                                    <tbody class="agentDetailTable">
                                        <tr>
                                            <td>Upline Name:</td>
                                            <td>
                                                <asp:Label ID="lblUplineName" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rank:</td>
                                            <td>
                                                <asp:Label ID="lblUplineRank" Text="WM" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No:</td>
                                            <td>
                                                <asp:Label ID="lblUplineMobileNo" Text="" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
                </div>
                <hr class="agentDashboardHr" />

                <%--Segment 2--%>
                <div class="container agentTransactionCard">
                    <div class="row" style="margin-top:20px;">
                        <div class="col-sm-4">
                        <table class="table table-bordered table-condensed table-responsive">
                            <thead>
                                <tr>
                                    <th colspan="2" class="text-center">AUM</th>
                                </tr>
                            </thead>
                            <tbody class="table-st">
                                <tr>
                                    <td>Personal</td>
                                    <td>
                                        <asp:Label ID="lblPersonalVal" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Personal Group</td>
                                    <td>
                                        <asp:Label ID="lblPersonalGroupVal" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total Group</td>
                                    <td>
                                        <asp:Label ID="lblTotalGroupVal" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        </div>   
                        
                        <div class="col-sm-4">
                        <table class="table table-bordered table-condensed table-responsive">
                            <thead>
                                <tr>
                                    <th colspan="2" class="text-center">Sales</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>MTD PS</td>
                                    <td>
                                        <asp:Label ID="lblSalesMTDPS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>MTD PGS</td>
                                    <td>
                                        <asp:Label ID="lblSalesMTDPGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>MTD TGS</td>
                                    <td>
                                        <asp:Label ID="lblSalesMTDTGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                  <tr>
                                    <td>YTD PS</td>
                                    <td>
                                        <asp:Label ID="lblSalesYTDPS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>YTD PGS</td>
                                    <td>
                                        <asp:Label ID="lblSalesYTDPGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>YTD TGS</td>
                                    <td>
                                         <asp:Label ID="lblSalesYTDTGS" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div> 

                        <div class="col-sm-4">
                        <table class="table table-bordered table-condensed table-responsive">
                            <thead>
                                <tr>
                                    <th colspan="2" class="text-center">Redemption</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>MTD PD</td>
                                    <td>
                                        <asp:Label ID="lblRdMPD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>MTD PGD</td>
                                    <td>
                                        <asp:Label ID="lblRdMPGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>MTD TGD</td>
                                    <td>
                                         <asp:Label ID="lblRdMTGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>YTD PD</td>
                                    <td>
                                        <asp:Label ID="lblRdYPD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>YTD PGD</td>
                                    <td>
                                        <asp:Label ID="lblRdYPGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>YTD TGD</td>
                                    <td>
                                         <asp:Label ID="lblRdYTGD" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>     

                    </div>


                </div>

                <hr class="agentDashboardHr" />

                <%-- Segment 3--%>
                <div class="container agentCommissionCard">
                    <div class="row" style="margin-top:20px;">
                        <div class="col-sm-4">
                        <table class="table table-bordered table-condensed table-responsive">
                            <thead>
                                <tr>
                                    <th colspan="2" class="text-center">Commission</th>
                                </tr>
                            </thead>
                            <tbody class="table-st">
                                <tr>
                                    <td>MTD PSC</td>
                                    <td>
                                        <asp:Label ID="lblCommissionMPSC" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>YTD PSC</td>
                                    <td>
                                        <asp:Label ID="lblCommissionYPSC" CssClass="trTextFormat" Text="0" runat="server"></asp:Label>
                                    </td>
                               </tr>
                            </tbody>
                        </table>
                        </div>                       
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="back-board clearfix">
                                <figure class="highcharts-figure">
                                    <select name="commChartPlotType" id="commChartPlotType" runat="server" clientidmode="static" style="position: absolute; margin-top: -30px; z-index: 9; right: 25px;">
                                        <option value="PC">Personal Commission</option>
                                        <option value="OC">Override Commission</option>
                                        <option value="ALL">All</option>
                                    </select>
                                    <input type="hidden" id="agentCode" name="agentCode" value="" clientidmode="static" runat="server"/>
                                    <div id="commChart"></div>
                                </figure>
                            </div>
                        </div>

                    </div>
                    </div>
                    <hr class="agentDashboardHr" />

                    <%--Segment 4--%>
                    <div class="container">
                        <div class="row" >
                            <div class="col">
                                <h5>My Direct Downline:</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive" style="border: none!important;">
                                <table class="table table-font-size-13 table-bordered " id="directDownlineTable" style="width: 95% !important">
                                    <thead id="theadDirectList" runat="server">
                                        <tr>
                                            <th>Index</th>
                                            <th>Consultant Name</th>
                                            <th>Consultant Code</th>
                                            <th>Status</th>
                                            <th>Rank</th>
                                            <th>Personal AUM (MYR)</th>
                                            <th>Incentive Trip Personal Points</th>
                                            <th>Accumulated CPD Points</th>
                                            <th>FIMM Renewal Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyDirectDownline" runat="server" clientidmode="Static">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <hr class="agentDashboardHr" />
                    <%--Segment 5--%>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <h5>My Indirect Downline:</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive" style="border: none!important;">
                                <table class="table table-font-size-13 table-bordered " id="indirectDownlineTable" style="width: 95% !important">
                                    <thead id="theadIndirectList" runat="server">
                                        <tr>
                                            <th>Index</th>
                                            <th>Consultant Name</th>
                                            <th>Consultant Code</th>
                                            <th>Status</th>
                                            <th>Rank</th>
                                            <th>Personal AUM (MYR)</th>
                                            <th>Incentive Trip Personal Points</th>
                                            <th>Accumulated CPD Points</th>
                                            <th>FIMM Renewal Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyIndirectDownline" runat="server" clientidmode="Static">
                                        <%--<tr class="trTextFormat">
                                            <td>1</td>
                                            <td>Alex Wong</td>
                                            <td>CC0014</td>
                                            <td>Active</td>
                                            <td>WM</td>
                                            <td>10,000.00</td>
                                            <td>10</td>
                                            <td>25</td>
                                            <td>Active</td>
                                        </tr>--%>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <%--<script src="Content/mycj/common-scripts.js?v=1.0"></script>--%>
        <script src="/Content/js/highcharts.js"></script>
        <script src="Content/assets/js/jquery.min.js"></script>
        <script>
            var chart;
            chart = Highcharts.chart('commChart', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: 'Commission chart'
                },
                credits: {
                    href: 'javascript:;',
                    text: '',
                    position: {
                        align: 'right',
                        verticalAlign: 'bottom',
                    }
                },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },

                yAxis: {
                    min: 0,
                    title: {
                        text: 'Commission (MYR)'
                    }
                },
            //    tooltip: {
            //    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            //    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            //        '<td style="padding:0"><b>{point.y}</b></td></tr>',
            //    formatter: function () {
            //        var s = [];
            //        s.push('<span style="font-size:10px">' + this.points[0].key + '</span><table>');
            //        $.each(this.points, function (i, point) {
            //            var seriesName = point.series.name;
            //            var y = point.y;
            //            var x = point.x;

            //            var str = y.toString().split('.');
            //            if (str[0].length >= 4) {
            //                str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
            //            }
            //            //s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%d/%m/%Y', new Date(x)) + '</span><br/><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
            //            s.push('<tr><td style="color:' + point.color + ';padding:0">' + seriesName + ': </td>' +
            //                '<td style="padding:0"><b>' + (str.join('.')) + '</b></td></tr>');
            //        });
            //        s.push('</table>');

            //        return s.join('');
            //    },
            //    footerFormat: '</table>',
            //    shared: true,
            //    useHTML: true
            //},
                tooltip: {
                    pointFormat: '{series.name} has commission value of <b>{point.y:,.0f}</b><br /> in { point.x }'
                },
                plotOptions: {
                    area: {
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    }
                },
                //series: [{
                //    name: 'USA',
                //    data: [
                //        null, null, null, null, null, 6, 11, 32, 110, 235,
                //        369, 640, 1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468,
                //        20434, 24126, 27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342,
                //        26662, 26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
                //        24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586, 22380,
                //        21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950, 10871, 10824,
                //        10577, 10527, 10475, 10421, 10358, 10295, 10104, 9914, 9620, 9326,
                //        5113, 5113, 4954, 4804, 4761, 4717, 4368, 4018
                //    ]
                //}, {
                //    name: 'USSR/Russia',
                //    data: [null, null, null, null, null, null, null, null, null, null,
                //        5, 25, 50, 120, 150, 200, 426, 660, 869, 1060,
                //        1605, 2471, 3322, 4238, 5221, 6129, 7089, 8339, 9399, 10538,
                //        11643, 13092, 14478, 15915, 17385, 19055, 21205, 23044, 25393, 27935,
                //        30062, 32049, 33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000,
                //        37000, 35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                //        21000, 20000, 19000, 18000, 18000, 17000, 16000, 15537, 14162, 12787,
                //        12600, 11400, 5500, 4512, 4502, 4502, 4500, 4500
                //    ]
                //}]
            });

            $(document).ready(function(){
             
            var colorStrings = ['blue', 'green'];
            var theMonths = ["Jan", "Feb", "Mar", "Apr", "May",
                "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var YTDCDATA;
                $('#commChartPlotType').change(function(){
                    $.ajax({
                        url: "AgentDashboard.aspx/GetCommissionStats",
                        async: false,
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: {
                            'commCode': JSON.stringify($(this).val())
                        },
                        success: function (data) {
                            var json = data.d;
                            YTDCDATA = json;
                            while (chart.series.length > 0) chart.series[0].remove(true);
                            chart.xAxis[0].visible = true;
                            chart.xAxis[0].setCategories(theMonths);
                            var html = "";
                            $.each(json, function (idx, y) {
                                var series = [];
                                for (i = 0; i < 12; i++) {
                                    var rec = $.grep(y.statByMonth, function (obj, index) {
                                        return theMonths[i] == obj.Month;
                                    });
                                    if (rec.length == 1)
                                        series.push(parseInt(rec[0].commVal));
                                    else
                                        series.push(0);
                                }
                                chart.addSeries({
                                    className: "highcharts-color-" + colorStrings[idx],
                                    data: series,
                                    name: y.Year,
                                    useHTML: true
                                });
                            });
                        }
                    });

                });
            
                $('#commChartPlotType').change();

            });           
        </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
