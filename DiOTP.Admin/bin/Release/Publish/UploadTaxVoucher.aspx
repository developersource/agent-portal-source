﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadTaxVoucher.aspx.cs" Inherits="Admin.UploadTaxVoucher" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="uploadTaxVoucherForm" runat="server">
        <div class="row" id="FormId" data-value="uploadTaxVoucherForm">
            <div class="col-md-12">
                <input type="file" name="statements" id="statements" class="form-control" multiple="multiple" />
            </div>
        </div>
    </form>
</body>
</html>
