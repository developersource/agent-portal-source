﻿using AutoMapper;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomAttributes;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    /// <summary>
    /// Data layer to extract data from database
    /// </summary>
    public static class GenericRepoClass
    {

        static MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();

        static Response response = new Response();

        public static Response PostData<T>(T obj)
        {
            try
            {
                string query = obj.ObjectToQuery<T>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response PostBulkData<T>(List<T> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<T>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response UpdateData<T>(T obj)
        {
            try
            {
                string query = obj.ObjectToQuery<T>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response UpdateBulkData<T>(List<T> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<T>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Data = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response DeleteData<T>(Int32 Id)
        {
            Response responseObj = GetSingle<T>(Id, true, null, false);
            if (responseObj.IsSuccess)
            {
                T obj = (T)responseObj.Data;
                try
                {
                    string query = obj.ObjectToQuery<T>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                    response.IsSuccess = true;
                    response.Data = obj;
                }
                catch (Exception ex)
                {
                    response.IsSuccess = false;
                    response.Data = ex.Message;
                    Console.Write(ex.ToString());
                }
                return response;
            }
            else
            {
                return responseObj;
            }
        }

        public static Response GetSingle<T>(Int32 Id, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            T obj = (T)Activator.CreateInstance(typeof(T));
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + " where ID=" + Id;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = dt.ToDynamicObject<T>().FirstOrDefault();

                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    otherProperties.ForEach(op =>
                        {
                            if (op.PropertyType.IsGenericType)
                            {
                                if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                    string propName = className + "Id";
                                    Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, responseList.Data);
                                    }
                                }
                            }
                            else if (op.PropertyType.IsClass)
                            {
                                var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                string propName = "Id";
                                string propValue = propPrimary.GetValue(obj).ToString();
                                if (op.PropertyType.Name == className)
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                else
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                var propertyName = op.Name;
                                //if (op.Name.Contains(op.PropertyType.Name))
                                //{
                                //    if (op.Name.EndsWith(op.PropertyType.Name))
                                //    {

                                //    }
                                //}
                                Response responseList = GenericRepoChild.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                if (responseList.IsSuccess)
                                {
                                    prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                }
                            }
                        });
                }
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetData<T>(int skip, int take, bool isOrderByDesc, string orderByProp, bool isAllProperties, List<string> properties, bool isGetChildren, bool isGroupBy, string groupByProp)
        {
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            List<T> objs = new List<T>();
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + "";
                if (isGroupBy)
                {
                    string groupByColumnName = Converter.GetColumnNameByPropertyName<T>(groupByProp);
                    query += " group by " + groupByColumnName;
                }
                if (isOrderByDesc)
                {
                    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                    query += " order by " + orderByColumnName + " desc";
                }
                else
                {
                    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                    query += " order by " + orderByColumnName;
                }
                //if (orderByProp != null)
                //{
                //    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                //    query += " order by " + orderByColumnName + " desc";
                //}
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject<T>();
                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    objs.ForEach(obj =>
                    {

                        otherProperties.ForEach(op =>
                        {
                            if (op.PropertyType.IsGenericType)
                            {
                                if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                    string propName = className + "Id";
                                    Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, responseList.Data);
                                    }
                                }
                            }
                            else if (op.PropertyType.IsClass)
                            {
                                var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                string propName = "Id";
                                string propValue = propPrimary.GetValue(obj).ToString();
                                if (op.PropertyType.Name == className)
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                else
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                var propertyName = op.Name;
                                //if (op.Name.Contains(op.PropertyType.Name))
                                //{
                                //    if (op.Name.EndsWith(op.PropertyType.Name))
                                //    {

                                //    }
                                //}
                                Response responseList = GenericRepoChild.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                if (responseList.IsSuccess)
                                {
                                    prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                }
                            }
                        });
                    });
                }
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetCount<T>(bool isGroupBy, string groupByProp)
        {
            var firstProperty = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace == "System").FirstOrDefault();
            Int32 count = 0;
            try
            {
                string databaseTableName = Converter.GetDatabaseName<T>();
                string firstColumnName = Converter.GetColumnNameByPropertyName<T>(firstProperty.Name);
                string query = "select count(" + firstColumnName + ") from " + databaseTableName;
                if (isGroupBy)
                {
                    string groupByColumnName = Converter.GetColumnNameByPropertyName<T>(groupByProp);
                    query += " group by " + groupByColumnName;
                }
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = count;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetDataByFilter<T>(string filter, int skip, int take, bool isOrderByDesc, string orderByProp, bool isAllProperties, List<string> properties, bool isGetChildren, bool isGroupBy, string groupByProp)
        {
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            List<T> objs = new List<T>();
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + " where " + filter;
                if (isGroupBy)
                {
                    string groupByColumnName = Converter.GetColumnNameByPropertyName<T>(groupByProp);
                    query += " group by " + groupByColumnName;
                }
                if (isOrderByDesc)
                {
                    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                    query += " order by " + orderByColumnName + " desc";
                }
                else if (orderByProp != null)
                {
                    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                    query += " order by " + orderByColumnName + " desc";
                }
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject<T>();
                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    objs.ForEach(obj =>
                        {

                            otherProperties.ForEach(op =>
                            {
                                if (op.PropertyType.IsGenericType)
                                {
                                    if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                    {
                                        var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                        var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                        string propName = className + "Id";
                                        Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                        var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                        if (responseList.IsSuccess)
                                        {
                                            prop.SetValue(obj, responseList.Data);
                                        }
                                    }
                                }
                                else if (op.PropertyType.IsClass)
                                {
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                    string propName = "Id";
                                    string propValue = propPrimary.GetValue(obj).ToString();
                                    if (op.PropertyType.Name == className)
                                    {
                                        propValue = propPrimary.GetValue(obj).ToString();
                                    }
                                    else
                                    {
                                        propValue = propPrimary.GetValue(obj).ToString();
                                    }
                                    var propertyName = op.Name;
                                    //if (op.Name.Contains(op.PropertyType.Name))
                                    //{
                                    //    if (op.Name.EndsWith(op.PropertyType.Name))
                                    //    {

                                    //    }
                                    //}
                                    Response responseList = GenericRepoChild.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                    }
                                }
                            });
                        });
                }
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetCountByFilter<T>(string filter, int skip, int take, bool isGroupBy, string groupByProp)
        {
            var firstProperty = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace == "System").FirstOrDefault();
            Int32 count = 0;
            try
            {
                string databaseTableName = Converter.GetDatabaseName<T>();
                string firstColumnName = Converter.GetColumnNameByPropertyName<T>(firstProperty.Name);
                string query = "select count(" + firstColumnName + ") from " + databaseTableName + " where " + filter;
                if (isGroupBy)
                {
                    string groupByColumnName = Converter.GetColumnNameByPropertyName<T>(groupByProp);
                    query += " group by " + groupByColumnName;
                }
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = count;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetDataByJoin<T>(string filter, Type typeb, string typebFilter, int skip, int take, bool isOrderByDesc, string orderByProp, bool isAllProperties, List<string> properties, bool isGetChildren, bool isGroupBy, string groupByProp)
        {
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            List<T> objs = new List<T>();
            string databaseTableName = Converter.GetDatabaseName<T>();
            string databaseJoinTableName = Converter.GetDatabaseName(typeb);
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + " a inner join " + databaseJoinTableName + " b on " + typebFilter + " where " + filter;
                if (isGroupBy)
                {
                    string groupByColumnName = Converter.GetColumnNameByPropertyName<T>(groupByProp);
                    query += " group by a." + groupByColumnName;
                }
                if (isOrderByDesc)
                {
                    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                    query += " order by a." + orderByColumnName + " desc";
                }
                else if (orderByProp != null)
                {
                    string orderByColumnName = Converter.GetColumnNameByPropertyName<T>(orderByProp);
                    query += " order by a." + orderByColumnName + " desc";
                }
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject<T>();
                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    objs.ForEach(obj =>
                    {

                        otherProperties.ForEach(op =>
                        {
                            if (op.PropertyType.IsGenericType)
                            {
                                if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                    string propName = className + "Id";
                                    Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, responseList.Data);
                                    }
                                }
                            }
                            else if (op.PropertyType.IsClass)
                            {
                                var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                string propName = "Id";
                                string propValue = propPrimary.GetValue(obj).ToString();
                                if (op.PropertyType.Name == className)
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                else
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                var propertyName = op.Name;
                                //if (op.Name.Contains(op.PropertyType.Name))
                                //{
                                //    if (op.Name.EndsWith(op.PropertyType.Name))
                                //    {

                                //    }
                                //}
                                Response responseList = GenericRepoChild.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                if (responseList.IsSuccess)
                                {
                                    prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                }
                            }
                        });
                    });
                }
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetCountByJoin<T>(string filter, Type typeb, string typebFilter, int skip, int take, bool isGroupBy, string groupByProp)
        {
            var firstProperty = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace == "System").FirstOrDefault();
            Int32 count = 0;
            try
            {
                string databaseTableName = Converter.GetDatabaseName<T>();
                string databaseJoinTableName = Converter.GetDatabaseName(typeb);
                string firstColumnName = "a." + Converter.GetColumnNameByPropertyName<T>(firstProperty.Name);
                string query = "select count(" + firstColumnName + ") from " + databaseTableName + " a inner join " + databaseJoinTableName + " b on " + typebFilter + " where " + filter;
                if (isGroupBy)
                {
                    string groupByColumnName = Converter.GetColumnNameByPropertyName<T>(groupByProp);
                    query += " group by a." + groupByColumnName;
                }
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = count;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetDataByQuery(string query, int skip, int take, bool isOrderByDesc, string orderByColumn, bool isGroupBy, string groupByColumn)
        {
            try
            {
                if (isGroupBy)
                {
                    query += " group by " + groupByColumn;
                }
                if (isOrderByDesc)
                {
                    query += " order by " + orderByColumn + " desc";
                }
                else
                {
                    query += " order by " + orderByColumn;
                }
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = dt;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int place = Source.LastIndexOf(Find);

            if (place == -1)
                return Source;

            string result = Source.Remove(place, Find.Length).Insert(place, Replace);
            return result;
        }

        public static Response GetSingleNew<T>(this T obj, Int32 Id, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            //T obj = (T)Activator.CreateInstance(typeof(T));
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + " where ID=" + Id;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = dt.ToDynamicObject<T>().FirstOrDefault();

                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    otherProperties.ForEach(op =>
                    {
                        if (op.PropertyType.IsGenericType)
                        {
                            if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                string propName = className + "Id";
                                Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                if (responseList.IsSuccess)
                                {
                                    prop.SetValue(obj, responseList.Data);
                                }
                            }
                        }
                        else if (op.PropertyType.IsClass)
                        {
                            var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                            string propName = "Id";
                            string propValue = propPrimary.GetValue(obj).ToString();
                            if (op.PropertyType.Name == className)
                            {
                                propValue = propPrimary.GetValue(obj).ToString();
                            }
                            else
                            {
                                propValue = propPrimary.GetValue(obj).ToString();
                            }
                            var propertyName = op.Name;
                            //if (op.Name.Contains(op.PropertyType.Name))
                            //{
                            //    if (op.Name.EndsWith(op.PropertyType.Name))
                            //    {

                            //    }
                            //}
                            Response responseList = GenericRepoChild.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                            var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                            if (responseList.IsSuccess)
                            {
                                prop.SetValue(obj, ((IList)responseList.Data)[0]);
                            }
                        }
                    });
                }
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }


        public static Response GetSingleEx<T>(Int32 Id)
        {
            response = GenericRepoClass.GetSingle<T>(Id, true, null, false);
            return response;
        }

        /// <summary>
        /// Pulls all data if take = 0.
        /// </summary>
        public static Response PullData<T>(int skip, int take)
        {
            response = GenericRepoClass.GetData<T>(skip, take, false, null, true, null, false, false, null);
            return response;
        }
        /// <summary>
        /// Pulls all data if take = 0.
        /// isOrderByDesc = true, false.
        /// OrderByPropName = property name
        /// </summary>
        public static Response PullData<T>(int skip, int take, bool isOrderByDesc, string OrderByPropName)
        {
            response = GenericRepoClass.GetData<T>(skip, take, isOrderByDesc, OrderByPropName, true, null, false, false, null);
            return response;
        }
        /// <summary>
        /// Add filter like 'column_name = value'.
        /// Pulls all data if take = 0.
        /// isOrderByDesc = true, false.
        /// OrderByPropName = property name
        /// </summary>
        public static Response PullData<T>(string filter, int skip, int take, bool isOrderByDesc, string OrderByPropName)
        {
            if (filter == null || filter == "")
                response = GenericRepoClass.GetData<T>(skip, take, isOrderByDesc, OrderByPropName, true, null, false, false, null);
            else
                response = GenericRepoClass.GetDataByFilter<T>(filter, skip, take, isOrderByDesc, OrderByPropName, true, null, false, false, null);
            return response;
        }

        /// <summary>
        /// Add filter like 'column_name = value'.
        /// Pulls all data if take = 0.
        /// isOrderByDesc = true, false.
        /// OrderByPropName = property name
        /// </summary>
        public static Response GetGroupByData<T>(int Skip, int Take, bool IsOrderByDesc, string OrderByPropName, string GroupByPropName)
        {
            response = GenericRepoClass.GetData<T>(Skip, Take, IsOrderByDesc, OrderByPropName, true, null, false, true, GroupByPropName);
            return response;
        }

        public static Response LeftJoin<T>(this T obj, Type listType)
        {
            if (response.IsSuccess)
            {
                Response res = (Response)(object)obj;
                if (response.Data.GetType().IsGenericType)
                {
                    if (response.Data.GetType().GetGenericTypeDefinition() == typeof(List<>))
                    {
                        var listOPClass = response.Data.GetType().GenericTypeArguments.FirstOrDefault();
                        var className = listOPClass.Name;
                        var otherProperties = listOPClass.GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                        ((IEnumerable)response.Data).Cast<object>().ToList().ForEach(z =>
                        {
                            otherProperties.ForEach(op =>
                            {
                                if (op.PropertyType.IsGenericType)
                                {
                                    if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                    {
                                        var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                        if (listType == null)
                                        {
                                            var propPrimary = z.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                            string propName = className + "Id";
                                            string columnName = Converter.GetJoinNameByPropertyName(z.GetType(), op.Name);
                                            propName = Converter.GetPropertyNameByColumnName(op.PropertyType.GenericTypeArguments.FirstOrDefault(), columnName);
                                            Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(z).ToString(), 0, 0, false);
                                            var prop = z.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                            if (responseList.IsSuccess)
                                            {
                                                prop.SetValue(z, responseList.Data);
                                            }
                                        }
                                        else if (listObjClass == listType)
                                        {
                                            var propPrimary = z.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                            string propName = className + "Id";
                                            string columnName = Converter.GetJoinNameByPropertyName(z.GetType(), op.Name);
                                            propName = Converter.GetPropertyNameByColumnName(op.PropertyType.GenericTypeArguments.FirstOrDefault(), columnName);
                                            Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(z).ToString(), 0, 0, false);
                                            var prop = z.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                            if (responseList.IsSuccess)
                                            {
                                                prop.SetValue(z, responseList.Data);
                                            }
                                        }
                                    }
                                }
                            });
                        });
                    }
                }
                else
                {
                    var className = response.Data.GetType().Name;
                    var otherProperties = response.Data.GetType().GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    otherProperties.ForEach(op =>
                    {
                        if (op.PropertyType.IsGenericType)
                        {
                            if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                            {
                                var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                if (listObjClass == listType)
                                {
                                    var propPrimary = response.Data.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                    string propName = className + "Id";
                                    string columnName = Converter.GetJoinNameByPropertyName(response.Data.GetType(), op.Name);
                                    propName = Converter.GetPropertyNameByColumnName(op.PropertyType.GenericTypeArguments.FirstOrDefault(), columnName);
                                    Response responseList = GenericRepoChild.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(response.Data).ToString(), 0, 0, false);
                                    var prop = response.Data.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(response.Data, responseList.Data);
                                    }
                                }
                            }
                        }
                    });
                }
            }
            return response;
        }

    }
}
