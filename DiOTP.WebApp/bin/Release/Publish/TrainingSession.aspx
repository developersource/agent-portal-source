﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="TrainingSession.aspx.cs" Inherits="DiOTP.WebApp.TrainingSession" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" type="text/css" />
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" />  --%>
    <script src="https://code.jquery.com/jquery.min.js"></script>

    <meta charset="utf-8" />
    <style>
        /* Flat iconbox
-------------------------------------------------------------- */
        .flat-iconbox {
            margin: 0 -15px;
        }

            .flat-iconbox .item-three-column {
                float: left;
                padding: 0 15px;
                margin-bottom: 30px;
            }

        .iconbox {
            background: #eff7f8;
            padding: 40px 40px 30px 40px;
            position: relative;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

            .iconbox:hover {
                -webkit-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
                -moz-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
                -ms-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
                -o-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
                box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
                -webkit-transform: translateY(-10px);
                -moz-transform: translateY(-10px);
                -ms-transform: translateY(-10px);
                -o-transform: translateY(-10px);
                transform: translateY(-10px);
            }

            .iconbox .box-header {
                margin-bottom: 20px;
            }

                .iconbox .box-header .box-icon i {
                    font-size: 28px;
                    position: absolute;
                    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
                    color: #ffffff;
                    width: 60px;
                    height: 60px;
                    background-color: #059cce;
                    text-align: center;
                    line-height: 60px;
                    top: 0;
                    left: 40px;
                    -webkit-transition: all 0.3s ease-in-out;
                    -moz-transition: all 0.3s ease-in-out;
                    -ms-transition: all 0.3s ease-in-out;
                    -o-transition: all 0.3s ease-in-out;
                    transition: all 0.3s ease-in-out;
                }

            .iconbox:hover .box-header .box-icon i {
                background-color: #18ba60;
            }
            .iconbox .box-header .box-icon i.fa-check-square-o {
                background-color: #18ba60;
            }

            .iconbox .box-header .box-icon i.icons {
                font-size: 32px;
            }

            .iconbox .box-header .box-icon img {
                margin-bottom: -15px;
            }

            .iconbox .box-header .box-title {
                margin: 45px 0 0 0;
                text-transform: uppercase;
                font-weight: 700;
                line-height: 1.1;
            }

            .iconbox .box-readmore {
                margin-top: 20px;
            }

                .iconbox .box-readmore a {
                    color: #18ba60;
                    display: inline-block;
                    font-weight: bold;
                    position: relative;
                    text-transform: uppercase;
                }

                    .iconbox .box-readmore a:after {
                        content: "\f054";
                        font-family: "fontAwesome";
                        font-size: 14px;
                        font-weight: normal;
                        margin-left: 10px;
                        display: inline-block;
                    }

                    .iconbox .box-readmore a:before {
                        content: "";
                        position: absolute;
                        height: 5px;
                        left: 0;
                        bottom: -30px;
                        width: 0;
                        -webkit-transition: all 0.3s ease-in-out;
                        -moz-transition: all 0.3s ease-in-out;
                        -ms-transition: all 0.3s ease-in-out;
                        -o-transition: all 0.3s ease-in-out;
                        transition: all 0.3s ease-in-out;
                    }

                    .iconbox .box-readmore a:hover:before {
                        width: 100%;
                        background-color: #18ba60;
                    }

            .iconbox:hover .box-header .box-icon i:after {
                -webkit-transform: rotate(-45deg);
                -ms-transform: rotate(-45deg);
                transform: rotate(-45deg);
            }

            .iconbox .box-content .synopsis {
                min-height: 110px;
                overflow-y: scroll;
            }
            .iconbox .box-content .synopsis::-webkit-scrollbar {
                width: 5px;
            }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            font-weight: bolder;
            color: white !important;
            background-color: #059cce !important;
        }

        #sidebar a, #sidebar a:focus, #sidebar a:hover {
            color: black;
            background-color: white;
        }

        .nav-tabs > li > a {
            padding: 10px 20px 10px 20px;
        }
        /*Intro Section*/
        .intro-section {
            position: relative;
            /*Video Column Style*/
            /*Intro Tabs Style*/
        }

            .intro-section .video-column {
                position: relative;
                z-index: 1;
            }

                .intro-section .video-column .intro-video {
                    position: relative;
                    text-align: center;
                    width: 100%;
                    display: block;
                }

                    .intro-section .video-column .intro-video h4 {
                        position: absolute;
                        width: 100%;
                        text-align: center;
                        color: #ffffff;
                        font-weight: 500;
                        font-size: 16px;
                        text-transform: capitalize;
                        bottom: 0;
                        left: 50%;
                        transform: translate(-50%, -50%);
                    }

                    .intro-section .video-column .intro-video .intro-video-box {
                        position: relative;
                        width: 60px;
                        height: 60px;
                        z-index: 99;
                        color: #00aa15;
                        font-weight: 400;
                        font-size: 24px;
                        text-align: center;
                        border-radius: 50%;
                        padding-left: 7px;
                        line-height: 60px;
                        display: inline-block;
                        background-color: #ffffff;
                        transition: all 900ms ease;
                        -moz-transition: all 900ms ease;
                        -webkit-transition: all 900ms ease;
                        -ms-transition: all 900ms ease;
                        -o-transition: all 900ms ease;
                        box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
                    }

                    .intro-section .video-column .intro-video:before {
                        position: absolute;
                        content: '';
                        left: 0px;
                        top: 0px;
                        right: 0px;
                        bottom: 0px;
                        background-color: rgba(0, 0, 0, 0.1);
                    }

                .intro-section .video-column .course-features-info {
                    border-radius: 3px;
                    padding: 20px;
                    background: #ffffff;
                    box-shadow: 0 0 30px #eee;
                }

                    .intro-section .video-column .course-features-info ul li {
                        display: block;
                        padding: 10px 0;
                    }

                        .intro-section .video-column .course-features-info ul li i {
                            color: #ff5421;
                        }

                        .intro-section .video-column .course-features-info ul li .label {
                            padding-left: 10px;
                            color: #000;
                        }

                        .intro-section .video-column .course-features-info ul li .value {
                            float: right;
                            padding-right: 5px;
                        }

                .intro-section .video-column .btn-part {
                    background: #ffffff;
                    box-shadow: 0 0 35px #eee;
                    padding: 50px 50px 35px;
                    border-radius: 5px;
                }

                    .intro-section .video-column .btn-part .btn {
                        margin: 0 0 15px;
                        width: 100%;
                    }

            .intro-section .intro-tabs {
                position: relative;
                border: none;
            }

                .intro-section .intro-tabs .tab-btns {
                    position: relative;
                    text-align: center;
                    width: 25%;
                }

                    .intro-section .intro-tabs .tab-btns .tab-btn {
                        position: relative;
                        display: block;
                        width: 100%;
                        font-size: 14px;
                        background: none;
                        background: #ffffff;
                        color: #626262;
                        border: 1px solid #eee;
                        font-weight: 500;
                        line-height: 24px;
                        cursor: pointer;
                        float: left;
                        padding: 12px 40px 10px;
                        text-transform: capitalize;
                        transition: all 500ms ease;
                    }

                /*.intro-section .intro-tabs .tab-btns .tab-btn:hover,
                        .intro-section .intro-tabs .tab-btns .tab-btn.active {
                            color: #ffffff;
                            background: #ff5421;
                            border-color: #ff5421;
                        }*/

                .intro-section .intro-tabs .tabs-content {
                    position: relative;
                    overflow: hidden;
                    background-color: #ffffff;
                }

                    .intro-section .intro-tabs .tabs-content .minutes {
                        position: relative;
                        color: #21a7d0;
                        font-size: 14px;
                        font-weight: 500;
                        margin-top: 12px;
                    }
        /* Course Overview */
        .course-overview {
            position: relative;
        }

            .course-overview .instructor-title {
                position: relative;
                color: #111111;
                font-size: 20px;
                font-weight: 600;
                line-height: 1.3em;
                margin-bottom: 18px;
            }

            .course-overview .inner-box {
                position: relative;
                padding: 0px 40px 40px;
            }

                .course-overview .inner-box h4 {
                    position: relative;
                    color: #111111;
                    font-weight: 600;
                    line-height: 1.3em;
                    margin-bottom: 15px;
                }

                .course-overview .inner-box h3 {
                    position: relative;
                    color: #111111;
                    font-size: 20px;
                    font-weight: 600;
                    line-height: 1.3em;
                    margin-bottom: 18px;
                }

                .course-overview .inner-box p {
                    position: relative;
                    color: #626262;
                    font-size: 16px;
                    line-height: 1.8em;
                    margin-bottom: 15px;
                }

                .course-overview .inner-box .student-list {
                    position: relative;
                    margin-top: 25px;
                    margin-bottom: 30px;
                }

                    .course-overview .inner-box .student-list li {
                        position: relative;
                        color: #393939;
                        font-size: 16px;
                        font-weight: 500;
                        line-height: 1.3em;
                        margin-right: 80px;
                        display: inline-block;
                        list-style: disc;
                        margin-bottom: 10px;
                    }

                        .course-overview .inner-box .student-list li .fa {
                            color: #ff5421;
                            margin: 0px 2px;
                        }

                        .course-overview .inner-box .student-list li:last-child {
                            margin-right: 0px;
                        }

                .course-overview .inner-box .review-list {
                    position: relative;
                    margin-bottom: 20px;
                }

                    .course-overview .inner-box .review-list li {
                        position: relative;
                        color: #626262;
                        font-size: 16px;
                        margin-bottom: 14px;
                        padding-left: 30px;
                    }

                        .course-overview .inner-box .review-list li:before {
                            position: absolute;
                            content: "\f101";
                            left: 0px;
                            top: 5px;
                            color: #ff5421;
                            font-size: 14px;
                            line-height: 1em;
                            font-family: "FontAwesome";
                        }

                        .course-overview .inner-box .review-list li:last-child {
                            margin-bottom: 0px;
                        }
        /*Course Review*/
        .cource-review-box {
            position: relative;
            padding: 0px 40px;
        }

            .cource-review-box h4 {
                position: relative;
                color: #111111;
                font-weight: 700;
                line-height: 1.3em;
                margin-bottom: 15px;
            }

            .cource-review-box .rating {
                position: relative;
                color: #626262;
                font-size: 16px;
            }

                .cource-review-box .rating .fa {
                    position: relative;
                    color: #ff5421;
                    margin: 0px 2px;
                    font-size: 16px;
                }

            .cource-review-box .total-rating {
                position: relative;
                color: #ff5421;
                font-size: 16px;
                font-weight: 600;
                margin-right: 5px;
            }

            .cource-review-box .text {
                position: relative;
                color: #626262;
                font-size: 16px;
                line-height: 1.8em;
                margin-top: 15px;
            }

            .cource-review-box .helpful {
                position: relative;
                color: #393939;
                font-size: 16px;
                line-height: 1.8em;
                margin-top: 12px;
                font-weight: 500;
                margin-bottom: 15px;
            }

            .cource-review-box .like-option {
                position: relative;
            }

                .cource-review-box .like-option li {
                    position: relative;
                    margin-right: 10px;
                    display: inline-block;
                }

                    .cource-review-box .like-option li i {
                        position: relative;
                        width: 36px;
                        height: 36px;
                        color: #626262;
                        text-align: center;
                        line-height: 36px;
                        border-radius: 50px;
                        display: inline-block;
                        background-color: #eaeff5;
                    }

                        .cource-review-box .like-option li i:hover {
                            color: #21a7d0;
                        }

                .cource-review-box .like-option .report {
                    position: relative;
                    color: #333;
                    font-size: 14px;
                    font-weight: 500;
                }

                    .cource-review-box .like-option .report:hover {
                        color: #21a7d0;
                    }

            .cource-review-box .more {
                position: relative;
                color: #ff5421;
                font-size: 14px;
                font-weight: 500;
                margin-top: 30px;
                display: inline-block;
            }

            .ddlStyles 
            {
                    
                width:75%;
            }

            .transFilter , tbody.filterTable > tr > td 
            {
                border: 0px solid transparent;
            }

            #filterBtn, #filterCSS {
                        width:100%;
                        background-color: DodgerBlue;
                        border: none;
                        color: white;
                        cursor: pointer;
                        font-size: 16px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/AgentDashboard.aspx">Dashboard</a></li>
                    <li class="active">e-Learning</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">e-Learning</h3>
            </div>

            <%--Data Filtering--%>
            <div class="row">
                <div class="col-md-3" style="padding-top:12px;">
                    Date:
                    <select class="ddlStyles" name="ddlDate" id="ddlDate" clientidmode="static" runat="server">
                        <option value="">Select</option>
                        <option value=" 01">Januray</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>  
                </div>
                <div class="col-md-3" style="padding-top:12px;">
                    Location:
                    <select class="ddlStyles" name="ddlLocation" id="ddlLocation" clientidmode="static" runat="server">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="col-md-3" style="padding-top:10px;">
                    <button type="submit" id="filterBtn"><i class="fa fa-filter">Apply Filters</i></button>
                </div>
               <%-- <div class="col-md-4">
                     <table class="table table-borderless table-condensed transFilter table-responsive">
                                    <tbody class="filterTable">
                                        <tr>
                                            <td>Date:</td>
                                            <td>
                                                <select class="ddlStyles" name="ddlDate" id="ddlDate" clientidmode="static" runat="server">
                                                    <option value="">Select</option>
                                                    <option value=" 01">Januray</option>
                                                    <option value="02">February</option>
                                                    <option value="03">March</option>
                                                    <option value="04">April</option>
                                                    <option value="05">May</option>
                                                    <option value="06">June</option>
                                                    <option value="07">July</option>
                                                    <option value="08">August</option>
                                                    <option value="09">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Location:</td>
                                            <td>
                                                <select class="ddlStyles" name="ddlLocation" id="ddlLocation" clientidmode="static" runat="server">
                                                    <option value="">Select</option>
                                                    <option value="Petaling Jaya">Petaling Jaya</option>
                                                    <option value="Cheras">Cheras</option>
                                                    <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                    <option value="Puchong">Puchong</option>
                                                    <option value="Zoom">Zoom</option>
                                                </select>                                                

                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td colspan="2">
                                                <%--<span id="filterCss"><i class="fa fa-filter"></i><asp:Button runat="server" id="filterBtn" CssClass="fa fa-filter" Text="Apply Report Filter" OnClick="filterBtn_Click"/></span>--%>
                                                <%--<button type="submit" id="filterBtn"><i class="fa fa-filter">Apply Filters</i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                </div>--%>
                <div class="col-md-3">
                    <a class="text-primary pull-right" href="/ELearning.aspx"><i class="fa fa-arrow-circle-left mt-20 mr-10"></i><span>Back</span></a>
                </div> 
            </div>

            <div id="selectionPage">
                <div class="row">
                    <div class="col-md-12">                       
                        <div class="training">
                            <h4><asp:Label ID="learningTitle" runat="server"></asp:Label></h4>
                            <div class="row">
                                <%--<div class="col-md-3">
                                        <div class="iconbox">
                                            <div class="box-header">
                                                <div class="box-icon">
                                                    <i class="fa fa-pie-chart"></i>
                                                </div>
                                                <h4 class="box-title">Consultative Selling Training</h4>
                                            </div>
                                            <div class="box-content">
                                                Consultative Selling Skills provides a powerful roadmap for a successful need-based dialogue.
                                                <p class="box-readmore">
                                                    <a href="#" data-toggle="modal" data-target="#detailsPanel">Learn more</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>--%>
                                <div id="selection" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div id="tabContent" runat="server">
        <div class="modal fade" id="detailsPanel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog sett two">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h4 id="topic">Test1</h4>
                    </div>
                    <div class="modal-body intro-section">

                        <div class="row clearfix">
                            <!-- Content Column -->
                            <div class="col-lg-8 md-mb-50">
                                <!-- Intro Info Tabs-->
                                <div class="intro-info-tabs">
                                    <ul class="nav nav-tabs intro-tabs tabs-box" id="myTab" role="tablist">
                                        <li class="nav-item tab-btns active">
                                            <a class="nav-link tab-btn" id="prod-overview-tab" data-toggle="tab" href="#prod-overview" role="tab" aria-controls="prod-overview" aria-selected="true">Details</a>
                                        </li>
                                        <li class="nav-item tab-btns">
                                            <a class="nav-link tab-btn" id="prod-instructor-tab" data-toggle="tab" href="#prod-instructor" role="tab" aria-controls="prod-instructor" aria-selected="false">Instructor</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content tabs-content" id="myTabContent">
                                        <div class="tab-pane tab fade active in" id="prod-overview" role="tabpanel" aria-labelledby="prod-overview-tab">
                                            <div class="content white-bg pt-30">
                                                <!-- Cource Overview -->
                                                <div class="course-overview">
                                                    <div class="inner-box">
                                                        <h4>Educavo Course Details</h4>
                                                        <p>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus.</p>
                                                        <p>Eleifend euismod pellentesque vel Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus.</p>
                                                        <ul class="student-list">
                                                            <li>23,564 Total Students</li>
                                                            <li><span class="theme_color">4.5</span> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>(1254 Rating)</li>
                                                            <li>256 Reviews</li>
                                                        </ul>
                                                        <h3>What you’ll learn?</h3>
                                                        <ul class="review-list">
                                                            <li>Phasellus enim magna, varius et commodo ut.</li>
                                                            <li>Sed consequat justo non mauris pretium at tempor justo.</li>
                                                            <li>Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                            <li>Phasellus enim magna, varius et commodo ut.</li>
                                                            <li>Phasellus enim magna, varius et commodo ut.</li>
                                                            <li>Sed consequat justo non mauris pretium at tempor justo.</li>
                                                            <li>Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                            <li>Phasellus enim magna, varius et commodo ut.</li>
                                                        </ul>
                                                        <h3>Requirements</h3>
                                                        <ul class="review-list">
                                                            <li>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                            <li>Ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel.</li>
                                                            <li>Phasellus enim magna, varius et commodo ut.</li>
                                                            <li>Varius et commodo ut, ultricies vitae velit. Ut nulla tellus.</li>
                                                            <li>Phasellus enim magna, varius et commodo ut.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="prod-instructor" role="tabpanel" aria-labelledby="prod-instructor-tab">
                                            <div class="content pt-30 pb-30 pl-30 pr-30 white-bg">
                                                <h3 class="instructor-title">Instructors</h3>
                                                <div class="row rs-team style1 orange-color transparent-bg clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 sm-mb-30">
                                                        <div class="team-item">
                                                            <%--<img src="assets/images/team/1.jpg" alt="">--%>
                                                            <div class="content-part">
                                                                <h4 class="name"><a href="#">Jhon Pedrocas</a></h4>
                                                                <span class="designation">Professor</span>
                                                                <ul class="social-links">
                                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="team-item">
                                                            <%--<img src="assets/images/team/2.jpg" alt="">--%>
                                                            <div class="content-part">
                                                                <h4 class="name"><a href="#">Jhon Pedrocas</a></h4>
                                                                <span class="designation">Professor</span>
                                                                <ul class="social-links">
                                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Video Column -->
                            <div class="video-column col-lg-4">
                                <div class="inner-column">
                                    <!-- Video Box -->
                                    <div class="intro-video media-icon orange-color2">
                                        <img class="video-img" src="assets/images/about/about-video-bg2.png" alt="Video Image">
                                        <a class="popup-videos" href="https://www.youtube.com/watch?v=atMUy_bPoQI">
                                            <i class="fa fa-play"></i>
                                        </a>
                                        <h4>Preview this course</h4>
                                    </div>
                                    <!-- End Video Box -->
                                    <div class="course-features-info">
                                        <ul>
                                            <li class="lectures-feature">
                                                <i class="fa fa-files-o"></i>
                                                <span class="label">Lectures</span>
                                                <span class="value">3</span>
                                            </li>

                                            <li class="quizzes-feature">
                                                <i class="fa fa-puzzle-piece"></i>
                                                <span class="label">Quizzes</span>
                                                <span class="value">0</span>
                                            </li>

                                            <li class="duration-feature">
                                                <i class="fa fa-clock-o"></i>
                                                <span class="label">Duration</span>
                                                <span class="value">10 week </span>
                                            </li>

                                            <li class="students-feature">
                                                <i class="fa fa-users"></i>
                                                <span class="label">Students</span>
                                                <span class="value">21</span>
                                            </li>

                                            <li class="assessments-feature">
                                                <i class="fa fa-check-square-o"></i>
                                                <span class="label">Assessments</span>
                                                <span class="value">Yes</span>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="btn-part">
                                        <a href="#" class="btn readon2 orange">$35</a>
                                        <a href="#" class="btn readon2 orange-transparent">Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-infos1" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnCourseId" runat="server" ClientIDMode="Static" />
    <asp:Button CssClass="hide" ID="btnEnroll" runat="server" ClientIDMode="Static" OnClick="btnEnroll_Click" />
    <asp:Button CssClass="hide" ID="btnWithdraw" runat="server" ClientIDMode="Static" OnClick="btnWithdraw_Click" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/moment@2.24.0/min/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.js"></script>
    <script>
        $(document).ready(function () {
            // DEPENDENCY: https://github.com/flatlogic/bootstrap-tabcollapse

            $('.btnEnrollNow').click(function () {
                var id = $(this).data('id');
                $('#hdnCourseId').val(id);
                $('#btnEnroll').click();
            });

             $('.btnWithdrawNow').click(function () {
                var id = $(this).data('id');
                $('#hdnCourseId').val(id);
                $('#btnWithdraw').click();
            });
            

            // if the tabs are in a narrow column in a larger viewport
            //$('.sidebar-tabs').tabCollapse({
            //    tabsClass: 'visible-tabs',
            //    accordionClass: 'hidden-tabs'
            //});

            // if the tabs are in wide columns on larger viewports
            //$('.content-tabs').tabCollapse();

            // initialize tab function
            //$('.nav-tabs a').click(function (e) {
            //    e.preventDefault();
            //    $(this).tab('show');
            //});

            // slide to top of panel-group accordion
            //$('.panel-group').on('shown.bs.collapse', function () {
            //    var panel = $(this).find('.in');
            //    $('html, body').animate({
            //        scrollTop: panel.offset().top + (-60)
            //    }, 500);
            //});



        });
    </script>
</asp:Content>
