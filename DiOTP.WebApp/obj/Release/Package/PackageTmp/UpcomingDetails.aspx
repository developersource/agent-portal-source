﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpcomingDetails.aspx.cs" Inherits="DiOTP.WebApp.UpcomingDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>

        .upcoming-content {
            display: block;
            position: relative;
            padding: 5px 20px 20px 20px;
            box-shadow: 0px 0px 10px #d6d6d6;
            width: 100%;
            margin: 0 auto;
        }

    </style>
</head>
<body>
        <div class="modal-body">
                        <div class="upcoming-content">
                            <h3>FIMM Expiry Notice</h3>
                            <span>08.10.2021</span>
                            <hr />

                            <p>
                                Dear Agent, 
                               <br/>
                               <br/>
                                Your Federation of Investment Managers Malaysia (FIMM) Certification is expiring on <b>15.10.2021</b>. Please renew your FIMM Certification status with FIMM and contact our staff with a digital copy of the license to update your FIMM Expiry date. Thank You.
                                <br/>
                            </p>
                            <hr />
                            <h5>Company Management Team.</h5>
                            <span>08.10.2021</span>
                        </div>                    
            <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    </div>
        </div>
</body>
</html>
