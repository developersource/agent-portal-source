﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class EditFundDownloads : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> lazyUtmcFundFileServiceObj = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IUtmcFundFileService { get { return lazyUtmcFundFileServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileTypesDefService> lazyUtmcFundFileTypesDefServiceObj = new Lazy<IUtmcFundFileTypesDefService>(() => new UtmcFundFileTypesDefService());
        public static IUtmcFundFileTypesDefService IUtmcFundFileTypesDefService { get { return lazyUtmcFundFileTypesDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                string title = Request.QueryString["titleIn"];
                string idString = Request.QueryString["id"];
                string innerIdString = Request.QueryString["innerId"];
                if (!string.IsNullOrEmpty(idString) && !string.IsNullOrEmpty(innerIdString))
                {
                    int id = Convert.ToInt32(idString);
                    int innerId = Convert.ToInt32(innerIdString);
                    string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
                    CreatedDate.Value = currentDateString;
                    CreatedBy.Value = "0";
                    UpdatedDate.Value = currentDateString;
                    UpdatedBy.Value = "0";
                    Status.Value = "1";
                    UtmcFundInformationId.Value = idString;
                    if (innerId != 0)
                    {
                        Response response = IUtmcFundInformationService.GetSingle(id);
                        if (response.IsSuccess)
                        {
                            UtmcFundInformation utmcFundInformation = (UtmcFundInformation)response.Data;
                            Response response2 = IUtmcFundFileService.GetSingle(innerId);
                            if (response2.IsSuccess)
                            {
                                UtmcFundFile uff = (UtmcFundFile)response2.Data;
                                UtmcFundInformationId.Value = uff.UtmcFundInformationId.ToString();
                                UtmcFundFileTypesDefId.Value = uff.UtmcFundFileTypesDefId.ToString();
                                CreatedDate.Value = uff.CreatedDate.ToString("MM/dd/yyyy");
                                CreatedBy.Value = uff.CreatedBy.ToString();
                                UpdatedDate.Value = (uff.UpdatedDate == null ? default(DateTime).ToString("MM/dd/yyyy") : uff.UpdatedDate.Value.ToString("MM/dd/yyyy"));
                                UpdatedBy.Value = uff.UpdatedBy.ToString();

                                Id.Value = uff.Id.ToString();
                                Name.Text = uff.Name;
                                Name.ReadOnly = (uff.UtmcFundFileTypesDefId != 5);
                                Url.Text = uff.Url;

                                if (title == "Delete")
                                {
                                    statusOption.InnerHtml = "Are you sure you want to delete?";
                                    Name.ReadOnly = true;
                                    Url.ReadOnly = true;
                                    uff.Status = 0;
                                    Status.Value = "0";
                                    //IUtmcFundFileService.UpdateData(uff);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                    "alert('" + response2.Message + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + response.Message + "');", true);
                        }
                    }
                    else
                    {
                        string fileTypeIdString = Request.QueryString["fileTypeId"];
                        if (!string.IsNullOrEmpty(fileTypeIdString))
                        {
                            Int32 fileTypeId = Convert.ToInt32(fileTypeIdString);
                            if (fileTypeIdString != "5")
                            {
                                Response responseFFT = IUtmcFundFileTypesDefService.GetDataByFilter(" status='1'", 0, 0, false);
                                if (responseFFT.IsSuccess)
                                {
                                    List<UtmcFundFileTypesDef> utmcFundFileTypesDefs = (List<UtmcFundFileTypesDef>)responseFFT.Data;
                                    UtmcFundFileTypesDef utmcFundFileTypesDef = utmcFundFileTypesDefs.FirstOrDefault(x => x.Id == fileTypeId);
                                    Name.Text = utmcFundFileTypesDef.Name;
                                    Name.ReadOnly = true;
                                }
                            }
                            UtmcFundFileTypesDefId.Value = fileTypeIdString;
                        }
                    }
                }
            }
        }
    }
}