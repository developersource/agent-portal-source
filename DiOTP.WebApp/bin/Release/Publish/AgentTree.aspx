﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgentTree.aspx.cs" MasterPageFile="~/AgentMaster.master" Inherits="DiOTP.WebApp.AgentTree" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Agent Tree Hierarchy</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Agent Tree Hierarchy</h3>
            </div>
            <div id="containerPage">
                <style>
                    .agentCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: ghostwhite;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                    }

                    .agentCommissionCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: lightyellow;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .agentTransactionCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: aliceblue;
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .noticeCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: lightgoldenrodyellow;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .segmentCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: floralwhite;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                    }

                    .whiteCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        background-color: white;
                        background-clip: border-box;
                        border: 0px solid lightgrey;
                        border-radius: .25rem;
                        box-shadow: 0px 0px 10px #d6d6d6;
                        
                    }

                </style>

                <div class="row agentTransactionCard">
                    <div class="col-md-12">
                        <figure class="highcharts-figure">
                            <div id="AgentTree"></div>
                            <p class="highcharts-description">
                            </p>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<script src="Content/mycj/common-scripts.js?v=1.0"></script>--%>
    <%--<script src="/Content/js/highcharts.js"></script>--%>
    <script src="/Content/assets/js/jquery.min.js"></script>
    <script src="/Content/assets/bootstrap/js/bootstrap.min.js"></script>

    <%--org scripts--%>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/sankey.js"></script>
    <script src="https://code.highcharts.com/modules/organization.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script>
        //Organisational Chart function
        var agentChart;

        $(document).ready(function () {
            $.ajax({
                url: "AgentTree.aspx/GetAgentHierarchy",
                async: false,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: {

                },
                success: function (data) {
                    var json = data.d;
                    console.log(data.d);
                    console.log(json.CurrentName);
                    var dataArray = [];
                    var nodeArray = []; //id"agentposition"-rank-name-image
                    nodeArray.push(["Agency", "", "", "",""]);

                    if (json.IndirectUplineAgent[1].CurrentPosition.length > 0 && json.IndirectUplineAgent[0].CurrentPosition.length == 0) {
                        //Agency - Indirect 1 (highest upline)
                        dataArray.push(["Agency", json.IndirectUplineAgent[1].CurrentPosition]);
                        nodeArray.push([json.IndirectUplineAgent[1].CurrentPosition, json.IndirectUplineAgent[1].Rank, json.IndirectUplineAgent[1].Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg' , ""]);

                        //Indirect 1 - Direct Upline
                        if (json.DirectUplineAgent.CurrentPosition.length > 0) {
                            dataArray.push([json.IndirectUplineAgent[1].CurrentPosition, json.DirectUplineAgent.CurrentPosition]);
                            nodeArray.push([json.DirectUplineAgent.CurrentPosition, json.DirectUplineAgent.Rank, json.DirectUplineAgent.Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                            //Direct Upline - Current Agent
                            dataArray.push([json.DirectUplineAgent.CurrentPosition, json.CurrentPosition]);
                            nodeArray.push([json.CurrentPosition, json.CurrentRank, json.CurrentName, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',"hasColor"]);

                        }
                    }

                    if (json.IndirectUplineAgent[1].CurrentPosition.length == 0 && json.IndirectUplineAgent[0].CurrentPosition.length > 0) {
                        //Agency - Indirect 0
                        dataArray.push(["Agency", json.IndirectUplineAgent[0].CurrentPosition]);
                        nodeArray.push([json.IndirectUplineAgent[0].CurrentPosition, json.IndirectUplineAgent[0].Rank, json.IndirectUplineAgent[0].Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                        //Indirect 0 - Direct Upline
                        if (json.DirectUplineAgent.CurrentPosition.length > 0) {
                            dataArray.push([json.IndirectUplineAgent[0].CurrentPosition, json.DirectUplineAgent.CurrentPosition]);
                            nodeArray.push([json.DirectUplineAgent.CurrentPosition, json.DirectUplineAgent.Rank, json.DirectUplineAgent.Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                            //Direct Upline - Current Agent
                            dataArray.push([json.DirectUplineAgent.CurrentPosition, json.CurrentPosition]);
                            nodeArray.push([json.CurrentPosition, json.CurrentRank, json.CurrentName, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',"hasColor"]);

                        }

                    }

                    //If have 2 Indirect Uplines
                    if (json.IndirectUplineAgent[0].CurrentPosition.length > 0 && json.IndirectUplineAgent[1].CurrentPosition.length > 0) {
                        //Agency - Indirect 1
                        dataArray.push(["Agency", json.IndirectUplineAgent[1].CurrentPosition]);
                        nodeArray.push([json.IndirectUplineAgent[1].CurrentPosition, json.IndirectUplineAgent[1].Rank, json.IndirectUplineAgent[1].Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                        //Indirect 1 - Indirect 0
                        dataArray.push([json.IndirectUplineAgent[1].CurrentPosition, json.IndirectUplineAgent[0].CurrentPosition]);
                        nodeArray.push([json.IndirectUplineAgent[0].CurrentPosition, json.IndirectUplineAgent[0].Rank, json.IndirectUplineAgent[0].Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                        //Indirect 0 - Direct Upline
                        if (json.DirectUplineAgent.CurrentPosition.length > 0) {
                            dataArray.push([json.IndirectUplineAgent[0].CurrentPosition, json.DirectUplineAgent.CurrentPosition]);
                            nodeArray.push([json.DirectUplineAgent.CurrentPosition, json.DirectUplineAgent.Rank, json.DirectUplineAgent.Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                            //Direct Upline - Current Agent
                            dataArray.push([json.DirectUplineAgent.CurrentPosition, json.CurrentPosition]);
                            nodeArray.push([json.CurrentPosition, json.CurrentRank, json.CurrentName, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',"hasColor"]);

                        }

                    }

                    //If no Indirect Upline and have 1 Direct Upline
                    if (json.IndirectUplineAgent[0].CurrentPosition.length == 0 && json.IndirectUplineAgent[1].CurrentPosition.length == 0 && json.DirectUplineAgent.CurrentPosition.length > 0) {
                        //Agency - direct upline (if no indirect upline exist)
                        dataArray.push(["Agency", json.DirectUplineAgent.CurrentPosition]);
                        nodeArray.push([json.DirectUplineAgent.CurrentPosition, json.DirectUplineAgent.Rank, json.DirectUplineAgent.Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);
                        //direct upline - current agent
                        dataArray.push([json.DirectUplineAgent.CurrentPosition, json.CurrentPosition]);
                        nodeArray.push([json.CurrentPosition, json.CurrentRank, json.CurrentName, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',"hasColor"]);

                    }

                    //If no Indirect upline and no direct Upline
                    if (json.IndirectUplineAgent[0].CurrentPosition.length == 0 && json.IndirectUplineAgent[1].CurrentPosition.length == 0 && json.DirectUplineAgent.CurrentPosition.length == 0) {
                        dataArray.push(["Agency", json.CurrentPosition]);
                        nodeArray.push([json.CurrentPosition, json.CurrentRank, json.CurrentName, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',"hasColor"]);
                    }

                    console.log("This is data array");
                    console.log(dataArray);

                    //DirectDownlineAgents
                    for (i = 0; i < json.DirectDownlineAgent.length; i++) {
                        if (json.DirectDownlineAgent[i].CurrentPosition.length > 0) {
                            //current agent - direct downline
                            dataArray.push([json.CurrentPosition, json.DirectDownlineAgent[i].CurrentPosition]);
                            nodeArray.push([json.DirectDownlineAgent[i].CurrentPosition, json.DirectDownlineAgent[i].Rank, json.DirectDownlineAgent[i].Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                            for (y = 0; y < json.DirectDownlineAgent[i].IndirectDownlineAgent.length; y++) {
                                if (json.DirectDownlineAgent[i].IndirectDownlineAgent[y].CurrentPosition.length > 0) {
                                    //direct downline - indirect downline                              
                                    dataArray.push([json.DirectDownlineAgent[i].CurrentPosition, json.DirectDownlineAgent[i].IndirectDownlineAgent[y].CurrentPosition]);
                                    nodeArray.push([json.DirectDownlineAgent[i].IndirectDownlineAgent[y].CurrentPosition, json.DirectDownlineAgent[i].IndirectDownlineAgent[y].Rank, json.DirectDownlineAgent[i].IndirectDownlineAgent[y].Name, 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg',""]);

                                }
                            }

                        }
                    }
                    console.log("This is node array");
                    console.log(nodeArray);
                    
                    console.log(nodeArray[0][0]);
                    var nodeItems = [];
                    for (i = 0; i < nodeArray.length; i++){
                        let colors = (nodeArray[i][4] === undefined || nodeArray[i][4] === '') ? '#007ad0' : '#ffff00';
                        console.log('Colors here');
                        console.log(colors);
                        item = {
                            id: nodeArray[i][0],
                            name: nodeArray[i][2],
                            title: nodeArray[i][1],
                            image: nodeArray[i][3],
                            color: colors,

                        };
                        nodeItems.push(item);
                    }
                    console.log(nodeItems);

                    agentCHart = Highcharts.chart('AgentTree', {
                        chart: {
                            height: 500,
                            inverted: true,
                            style: {
                                fontSize: '16px'
                            }
                            
                        },
                        credits: {
                            text: 'Petraware Technologies',
                        },
                        title: {
                            text: '<b>My Agent Tree</b>'
                        },
                        plotOptions: {
                            series: {
                                nodeWidth: '22%',
                                nodePadding: 5,
                            }
                        },

                        accessibility: {
                            point: {
                                descriptionFormatter: function (point) {
                                    var nodeName = point.toNode.name,
                                        nodeId = point.toNode.id,
                                        nodeDesc = nodeName === nodeId ? nodeName : nodeName + ', ' + nodeId,
                                        parentDesc = point.fromNode.id;
                                    return point.index + '. ' + nodeDesc + ', reports to ' + parentDesc + '.';
                                }
                            }
                        },

                        series: [{
                            type: 'organization',
                            name: 'Petraware Technologies',
                            keys: ['from', 'to'],
                            linkLineWidth: 3,
                            linkColor: '#ccc',                           
                            data: dataArray,
                            //levels: [{
                            //    level: 0,
                            //    color: '#4AAAA9',
                            //    dataLabels: {
                            //        color: 'black'
                                    
                            //    },
                            //    height: 25
                            //}, {
                            //    level: 1,
                            //    color: '#74CFCE',
                            //    dataLabels: {
                            //        color: 'black'
                            //    }
                                
                            //}, {
                            //    level: 2,
                            //        color: '#60E05B',
                            //    dataLabels: {
                            //        color: 'black',
                            //        fontSize: '20px'
                            //    }
                            //}, {
                            //    level: 3,
                            //        color: '#B0FFFE',
                            //    dataLabels: {
                            //        color: 'black'
                            //    }
                            //}],
                            nodes: nodeItems,//[{
                            //    id: 'Agency'
                            //}, {
                            //    id: 'AAE',
                            //    title: 'GM',
                            //    name: 'Lee Kian Hing',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg'
                            //}, {
                            //    id: 'AAEAAA',
                            //    title: 'WM',
                            //    name: 'Siti Fatimah Binti Ramli',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131210/Highsoft_04045_.jpg'
                            //}, {
                            //    id: 'AAEAAAAAA',
                            //    title: 'WA',
                            //    name: 'Jamal Bin Ludin',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131120/Highsoft_04074_.jpg'
                            //}, {
                            //    id: 'AAEAAAAAB',
                            //    title: 'WA',
                            //    name: 'Brenda Amanda',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131213/Highsoft_03998_.jpg'
                            //}, {
                            //    id: 'AAEAAAAAC',
                            //    title: 'WA',
                            //    name: 'Muthu A/L Suba',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131156/Highsoft_03834_.jpg'
                            //}, {
                            //    id: 'AAEAAAAAD',
                            //    title: 'WA',
                            //    name: 'Wrights',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131156/Highsoft_03834_.jpg'
                            //},{
                            //    id: 'AAEAAAAADAAA',
                            //    title: 'WA',
                            //    name: 'Marven Lim',
                            //    image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131156/Highsoft_03834_.jpg'
                            //}],
                            colorByPoint: false,
                            color: '#007ad0',
                            dataLabels: {
                                color: 'black'
                            },
                            borderColor: 'white',
                            nodeWidth: 65,
                            nodePadding: 2
                        }],
                        tooltip: {
                            outside: true
                        },
                        exporting: {
                            allowHTML: true,
                            sourceWidth: 800,
                            sourceHeight: 600
                        }

                    })
                }
            });
         });

        //Highcharts.chart('AgentTree', {
        //    chart: {
        //        height: 600,
        //        inverted: true
        //    },

        //    title: {
        //        text: 'Agent Tree Hierarchy'
        //    },

        //    accessibility: {
        //        point: {
        //            descriptionFormatter: function (point) {
        //                var nodeName = point.toNode.name,
        //                    nodeId = point.toNode.id,
        //                    nodeDesc = nodeName === nodeId ? nodeName : nodeName + ', ' + nodeId,
        //                    parentDesc = point.fromNode.id;
        //                return point.index + '. ' + nodeDesc + ', reports to ' + parentDesc + '.';
        //            }
        //        }
        //    },

        //    series: [{
        //        type: 'organization',
        //        name: 'Highsoft',
        //        keys: ['from', 'to'],
        //        data: [
        //            ['Shareholders', 'Board'],
        //            ['Board', 'CEO'],
        //            ['CEO', 'CTO'],
        //            ['CEO', 'CPO'],
        //            ['CEO', 'CSO'],
        //            ['CEO', 'HR'],
        //            ['CTO', 'Product'],
        //            ['CTO', 'Web'],
        //            ['CSO', 'Sales'],
        //            ['HR', 'Market'],
        //            ['CSO', 'Market'],
        //            ['HR', 'Market'],
        //            ['CTO', 'Market']
        //        ],
        //        levels: [{
        //            level: 0,
        //            color: 'silver',
        //            dataLabels: {
        //                color: 'black'
        //            },
        //            height: 25
        //        }, {
        //            level: 1,
        //            color: 'silver',
        //            dataLabels: {
        //                color: 'black'
        //            },
        //            height: 25
        //        }, {
        //            level: 2,
        //            color: '#980104'
        //        }, {
        //            level: 4,
        //            color: '#359154'
        //        }],
        //        nodes: [{
        //            id: 'Shareholders'
        //        }, {
        //            id: 'Board'
        //        }, {
        //            id: 'CEO',
        //            title: 'CEO',
        //            name: 'Grethe Hjetland',
        //            image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131126/Highsoft_03862_.jpg'
        //        }, {
        //            id: 'HR',
        //            title: 'HR/CFO',
        //            name: 'Anne Jorunn Fjærestad',
        //            color: '#007ad0',
        //            image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131210/Highsoft_04045_.jpg'
        //        }, {
        //            id: 'CTO',
        //            title: 'CTO',
        //            name: 'Christer Vasseng',
        //            image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131120/Highsoft_04074_.jpg'
        //        }, {
        //            id: 'CPO',
        //            title: 'CPO',
        //            name: 'Torstein Hønsi',
        //            image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131213/Highsoft_03998_.jpg'
        //        }, {
        //            id: 'CSO',
        //            title: 'CSO',
        //            name: 'Anita Nesse',
        //            image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2020/03/17131156/Highsoft_03834_.jpg'
        //        }, {
        //            id: 'Product',
        //            name: 'Product developers'
        //        }, {
        //            id: 'Web',
        //            name: 'Web devs, sys admin'
        //        }, {
        //            id: 'Sales',
        //            name: 'Sales team'
        //        }, {
        //            id: 'Market',
        //            name: 'Marketing team',
        //            column: 5
        //        }],
        //        colorByPoint: false,
        //        color: '#007ad0',
        //        dataLabels: {
        //            color: 'white'
        //        },
        //        borderColor: 'white',
        //        nodeWidth: 65
        //    }],
        //    tooltip: {
        //        outside: true
        //    },
        //    exporting: {
        //        allowHTML: true,
        //        sourceWidth: 800,
        //        sourceHeight: 600
        //    }

        //});
    </script>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>