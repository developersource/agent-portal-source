﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class AgentSignupInfo
    {
        public Int32 Id { get; set; }
        public Int32 UserId { get; set; }
        public Int32 RegistrationType { get; set; }
        public String IntroducerCode { get; set; }
        public String UplineCode { get; set; }
        public Int32 ProcessStatus { get; set; }
        public String Remarks { get; set; }
        public String AgentCode { get; set; }
        public Int32 IsActive { get; set; }
        public Int32 IsNewAgent { get; set; }
        public String QualificationUrl { get; set; }
        public String IcUrl { get; set; }
        public String SelfieUrl { get; set; }
        public String PopUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 Status { get; set; }
        public String Username { get; set; }
        public String EmailId { get; set; }
        public String MobileNumber { get; set; }
        public String IdNo { get; set; }
        public String AgentRegId { get; set; }
    }
}
