﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AnnouncementListing : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazyUserServiceObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                string siteContentTypeId = Converter.GetColumnNameByPropertyName<SiteContent>(nameof(SiteContent.SiteContentTypeId));
                string siteContentStatus = Converter.GetColumnNameByPropertyName<SiteContent>(nameof(SiteContent.Status));
                filter.Append(" " + siteContentTypeId + "='1'");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameTitle = Converter.GetColumnNameByPropertyName<SiteContent>(nameof(SiteContent.Title));
                    filter.Append(" and " + columnNameTitle + " like '%" + Search.Value.Trim() + "%'");
                }

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = ISiteContentService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }
                filter.Append(" order by created_date desc ");
                Response responseSCList = ISiteContentService.GetDataByFilter(filter.ToString(), skip, take, false);
                if (responseSCList.IsSuccess)
                {
                    List<SiteContent> sitecontent = (List<SiteContent>)responseSCList.Data;

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (SiteContent a in sitecontent)
                    {
                        asb.Append(@"
             <tr
                        data-Title='" + a.Title + @"' 
                        data-Content='" + a.Content + @"' 
                        data-PublishDate='" + a.PublishDate + @"'
                        data-Status='" + a.Status + @"' 
             >
                 <td class='icheck'>
                     <div class='square single-row'>
                        <div class='checkbox'>
                             <input type='checkbox' name='checkRow' class='checkRow' value='" + a.Id + @"' /> <label>" + index + @"</label><br/>
                         </div>
                    </div>
                     <span class='row-status'>" + (a.Status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                 </td>
                <td>" + a.Title + @"</td>
                <td>" + a.PublishDate + @"</td>
                <td class='td-limit'>" + a.Content + @"</td>
            </tr>
                   
                     ");
                        index++;
                    }
                    announcementsTbody.InnerHtml = asb.ToString();
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            try
            {
                string idString = String.Join(",", ids);
                Response responseSCList = ISiteContentService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseSCList.IsSuccess)
                {
                    List<SiteContent> siteContent = (List<SiteContent>)responseSCList.Data;
                    if (action == "Deactivate")
                    {
                        siteContent.ForEach(x =>
                        {
                            x.Status = 0;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Announcement content with " + x.Id + " has been deactivated",
                                TableName = "site_content",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        ISiteContentService.UpdateBulkData(siteContent);
                        
                    }
                    if (action == "Activate")
                    {
                        siteContent.ForEach(x =>
                        {
                            x.Status = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Announcement content with " + x.Id + " has been activated",
                                TableName = "site_content",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        ISiteContentService.UpdateBulkData(siteContent);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Site contents action: " + ex.Message);
                return false;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(SiteContent obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            if (obj.Title == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Title";
                return response;
            }
            if (obj.ShortDisplayContent == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Short Display Content";
                return response;
            }
            if (obj.Content == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Content";
                return response;
            }
            if (obj.PublishDate.ToString() == "1/1/0001 12:00:00 AM")
            {
                response.IsSuccess = false;
                response.Message = "Choose the Publish Date";
                return response;
            }

            try
            {
                if (obj.Id == 0)
                {
                    ISiteContentService.PostData(obj);
                    response.IsSuccess = true;
                    response.Message = "Success";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Announcement content has been successfully added",
                        TableName = "site_content",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here
                }
                else
                {
                    ISiteContentService.UpdateData(obj);
                    response.IsSuccess = true;
                    response.Message = "Success";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Announcement content with " + obj.Id + " has been updated",
                        TableName = "site_content",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here

                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add language dir action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }

}