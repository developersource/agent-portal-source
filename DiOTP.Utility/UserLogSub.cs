﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_log_sub")]
    public class UserLogSub
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_log_main_id"), Display("UserLogMainId")]
        public Int32 UserLogMainId { get; set; }
        [Database("column_name"), Display("ColumnName")]
        public string ColumnName { get; set; }
        [Database("value_new"), Display("ValueNew")]
        public string ValueNew { get; set; }
        [Database("value_old"), Display("ValueOld")]
        public string ValueOld { get; set; }
    }
}
