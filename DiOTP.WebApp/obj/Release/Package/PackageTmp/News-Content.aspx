﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="News-Content.aspx.cs" Inherits="DiOTP.WebApp.News_Content" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <%--<li><a href="/News.aspx">Announcements</a></li>--%>
                            <li class="active">Content</li>
                        </ol>
                    </div>
                    <h2 class="news-head" id="contentTitle" runat="server">Content</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="announcement-content">
                                <h3 id="contentShortDisplayContent" runat="server">Launch eApexIs, Online Trading Platform. </h3>
                                <span id="contentDate" runat="server">01.01.2018</span>

                                <hr />
                                <p id="contentContent" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu nisl nec leo pulvinar molestie quis vitae sapien. Donec tempor finibus est, eu gravida nunc euismod vitae. Suspendisse potenti. Ut non nulla euismod, suscipit mauris in, aliquam nunc. Sed id diam sit amet felis iaculis efficitur. Suspendisse vehicula, ex et ultricies mattis, leo eros maximus quam, nec placerat arcu massa ultricies nunc. Curabitur ut nunc venenatis sem ullamcorper pretium. Donec non molestie nunc. Nam eget dignissim justo. Nam sed nulla massa. Quisque nulla sapien, faucibus ut dapibus sed, ultrices non leo. Nunc vestibulum consectetur purus at finibus. Pellentesque eget condimentum lacus, quis vehicula dolor.</p>
                                <h5>Apex Management Team.</h5>
                                <span id="contentDate1" runat="server">01.01.2018</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
