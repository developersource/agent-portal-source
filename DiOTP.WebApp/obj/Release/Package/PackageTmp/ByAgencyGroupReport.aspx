﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="ByAgencyGroupReport.aspx.cs" Inherits="DiOTP.WebApp.ByAgencyGroupReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Reports</li>
                </ol>
            </div>

                      <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Agency Report</h3>
            </div>
            <div id="containerPage">
                <style>

                    .trTextFormat {
                        text-align: center;
                    }


                    .agentDetailTable, tbody.agentDetailTable > tr{
                    
                        border:2px solid grey;
                        
                    }

                    .customBtn {
                      background-color: DodgerBlue;
                      border: none;
                      color: white;
                      padding: 11px 25px;
                      cursor: pointer;
                      font-size: 16px;
                    }

                    .ddlStyles {
                    
                        width:100%;
                    }
                    .row{
                        border:1px solid black;
                    }

                </style>

                <%--Segment 1--%>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <h4><u>Agency Tree Report : By Agency</u></h4>
                        </div>        
                    </div>

                    <div class="row ">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-condensed agentDetailTable table-responsive">
                                    <tbody class="agentDetailTable">
                                       
                                        
                                        <tr>
                                            <td></td>
                                            <td>Fund House</td>
                                            <td>XYZ AM Bhd</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td>Total Agency Group:</td>
                                            <td>Defaulted to respective agency group</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td>Agent Rank:</td>
                                            <td>Dropdown Selection ***</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 
                                         
                                        
                                        <tr>
                                            <td></td>
                                            <td>Agent Name:</td>
                                            <td>Dropdown Selection ***</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td>Total No of Agent:</td>
                                            <td>* Count Number</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr>
                                        
                                        <tr>
                                            </tr>

                                         <tr>
                                            </tr>
                                         <tr class="row"><td></td> </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Islamic</td>
                                            <td>Conventional</td>
                                            <td></td>                                           
                                        </tr> 
                                         
                                           
                                        <tr>
                                            <td></td>
                                            <td><b>Total AUM:</b></td>
                                            <td>By Plan Type:</td>
                                            <td>Cash</td>
                                            <td>: Money Market Fund</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: None Core Funds</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>PRS</td>
                                            <td>: Core Funds</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: None Core Funds</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>EPF</td>
                                            <td>: All EPF Funds</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                             </tr> 

                                        <tr>
                                             </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>By Holder Category:</td>
                                            <td>Individual</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Foreign</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Join</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td>*Both holders are Bumi</td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td>* Either one is Non Bumi</td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Foreign</td>
                                            <td></td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Corporate</td>
                                            <td>: Private Limited (Sdn Bhd)</td>
                                            <td>: Bumi</td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Non Bumi</td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Private (Bhd)</td>
                                            <td>: Bumi</td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 


                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Non Bumi</td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Others</td>
                                            <td>: Bumi</td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 


                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Non Bumi</td>
                                            <td>Value</td>
                                            <td>Value</td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                             </tr> 

                                        <tr>
                                             </tr> 

                                       
                                        <tr>
                                             </tr> 

                                        <tr>
                                             </tr> 
                                        <tr class="row"><td></td> </tr>
                                        <tr>
                                            <td></td>
                                            <td><b>Total No of Account:</b></td>
                                            <td>By Plan Type:</td>
                                            <td>Cash</td>
                                            <td>: Money Market Fund</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: None Money Market Fund</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>PRS</td>
                                            <td>: Core Funds</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: None Core Funds</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>EPF</td>
                                            <td>: All EPF Funds</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                             </tr> 

                                        <tr>
                                             </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>By Holder Category:</td>
                                            <td>Individual</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Foreign</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Join</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>*Both holders are Bumi</td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>* Either one is Non Bumi</td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Foreign</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Corporate</td>
                                            <td>: Private Limited (Sdn Bhd)</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Private (Bhd)</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>: Others</td>
                                            <td>: Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>:Non Bumi</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                        </tr> 

                                        <tr>
                                          </tr>
                                        
                                        <tr>
                                          </tr>
                                        
                                        <tr>
                                          </tr>
                                        <tr class="row"><td></td> </tr>
                                        <tr style="border-width: thick;  border-style: dashed;">
                                            <td></td>
                                            <td>Agent Name</td>
                                            <td>Agent Code</td>
                                            <td>Rank</td>
                                            <td>Personal AUM</td>
                                            <td>Incentive Trip Personal Points</td>
                                            <td>Accumulated CPD Points</td>
                                            <td>FiMM Renewal</td>
                                            <td>Agent Status</td>                                           
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td>Mr ABC</td>
                                            <td>AG0011</td>
                                            <td>WA</td>
                                            <td>100,000.00</td>
                                            <td>10</td>
                                            <td>20</td>
                                            <td>Yes</td>
                                            <td>Active</td>                                           
                                        </tr> 


                                        </tbody>
                                        </table>
                            </div>
                        </div>

                </div>
            </div>
            </div>
        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
