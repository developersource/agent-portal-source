using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_notification_settings")]
    public class UserNotificationSetting
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("notification_type_def_id"), Display("NotificationTypeDefId")]
        public Int32 NotificationTypeDefId { get; set; }
        [Database("fund_id"), Display("FundId")]
        public Int32 FundId { get; set; }

        [Database("fund_code"), Display("FundCode")]
        public String FundCode { get; set; }
        [Database("value"), Display("Value")]
        public String Value { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("actual_price"), Display("ActualPrice")]
        public Decimal ActualPrice { get; set; }
        [Database("expected_price"), Display("ExpectedPrice")]
        public Decimal ExpectedPrice { get; set; }
        public NotificationTypesDef NotificationTypeDefIdNotificationTypesDef { get; set; }
        [Join("Id")]
        public User UserIdUser { get; set; }
        [Join("Id")]
        public UserAccount UserAccountIdUserAccount { get; set; }
    }
}
