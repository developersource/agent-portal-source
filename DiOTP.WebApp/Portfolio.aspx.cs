﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.PolicyAndRules.UserPolicy;
using DiOTP.PolicyAndRules;

namespace DiOTP.WebApp
{
    public partial class Portfolio : System.Web.UI.Page
    {

        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcMemberInvestmentService> lazyUtmcMemberInvestmentObj = new Lazy<IUtmcMemberInvestmentService>(() => new UtmcMemberInvestmentService());

        public static IUtmcMemberInvestmentService IUtmcMemberInvestmentService { get { return lazyUtmcMemberInvestmentObj.Value; } }

        private static readonly Lazy<IFormTypeService> lazyFormTypeServiceObj = new Lazy<IFormTypeService>(() => new FormTypeService());

        public static IFormTypeService IFormTypeService { get { return lazyFormTypeServiceObj.Value; } }

        private static readonly Lazy<IFormDataFieldValueService> lazyFormDataFieldValueServiceObj = new Lazy<IFormDataFieldValueService>(() => new FormDataFieldValueService());

        public static IFormDataFieldValueService IFormDataFieldValueService { get { return lazyFormDataFieldValueServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        public static Int32 SMSExpirationTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeInSeconds"]);

        public static Int32 SMSLockTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSLockTimeInSeconds"]);


        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        public static string loginRedirectUrl = "";




        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Boolean enableTwoFactorAuthen = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["twoFactorAuthentication"]));
                if (!enableTwoFactorAuthen) Session["isVerified"] = 1;

                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    Response response = IUserService.GetSingle(user.Id);
                    if (response.IsSuccess)
                    {
                        user = (User)response.Data;
                        DateTime modifiedDate = user.ModifiedDate.Value;
                        if (modifiedDate.ToString("yyyy") != "0001")
                        {
                            TimeSpan passwordDays = DateTime.Now.Subtract(modifiedDate);
                            int day = Convert.ToInt32(passwordDays.Days);
                            hdnPasswordExpiredDate.Value = day.ToString();
                        }
                        else
                        {
                            hdnPasswordExpiredDate.Value = "0";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }
                }

                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                else
                    RefreshData();
                if (!IsPostBack)
                {
                    string q = Request.QueryString["q"];
                    if (q == "SetPassword")
                    {
                        serviceMessage.InnerHtml = "Password updated.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void RefreshData()
        {
            try
            {
                Boolean IsImpersonated = false;
                if (Session["isVerified"] != null)
                {
                    hdnIsVerified.Value = (Session["isVerified"].ToString());
                }
                hdnSMSExpirationTimeInSeconds.Value = SMSExpirationTimeInSeconds.ToString();
                hdnSMSLockTimeInSeconds.Value = SMSLockTimeInSeconds.ToString();

                if (Session["user"] != null)
                {
                    IsImpersonated = ServicesManager.IsImpersonated(Context);
                    if (IsImpersonated)
                    {
                        consolidatedSummaryNoData.Visible = false;
                        btnExit.Visible = true;
                        hdnIsVerified.Value = "1";
                        Session["isVerified"] = "1";
                    }
                }
                User user = (User)Session["user"];
                User sessionUser = user;
                Response response = IUserService.GetSingle(user.Id);
                if (response.IsSuccess)
                {
                    user = (User)response.Data;
                    hdnIsSMSLocked.Value = user.IsSmsLocked.ToString();
                    hdnSMSAttempts.Value = user.SmsAttempts.ToString();
                    hdnGoogleAuthAttempts.Value = user.GoogleAuthAttempts.ToString();
                    username.InnerHtml = user.Username;
                    email.InnerHtml = user.EmailId;
                    mobile.InnerHtml = user.MobileNumber;
                    hdnMobileNumber.Value = user.MobileNumber;
                    if (Session["LastLoginTime"] != null)
                    {
                        lastLoginTime.InnerHtml = Convert.ToDateTime(Session["LastLoginTime"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");
                        lastLoginIP.InnerHtml = Session["LastLoginIP"].ToString();
                        if (Convert.ToDateTime(Session["LastLoginTime"].ToString()).ToString("dd/MM/yyyy") == "01/01/0001")
                        {
                            ploginDateTime.Visible = false;
                        }
                        else
                            loginDateTime.InnerHtml = Convert.ToDateTime(Session["LastLoginTime"].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");
                    }
                    else
                    {
                        ploginDateTime.Visible = false;
                    }
                    userStatus.Attributes.Remove("class");
                    Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUSList.IsSuccess)
                    {
                        List<UserSecurity> userSecurities = (List<UserSecurity>)responseUSList.Data;
                        UserSecurity EmailSecurity = userSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                        UserSecurity MobileSecurity = userSecurities.Where(x => x.UserSecurityTypeId == 2).FirstOrDefault();
                        UserSecurity GoogleSecurity = userSecurities.Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                        if (user.IsActive == 1)
                        {
                            userStatus.Attributes.Add("class", "text-success ml-4 position-absolute");
                            userStatus.InnerHtml = "<i class='fa fa-circle fs-11' data-toggle='tooltip' title='Active'></i>";
                        }
                        else
                        {
                            userStatus.Attributes.Add("class", "text-danger ml-4 position-absolute");
                            userStatus.InnerHtml = "<i class='fa fa-circle fs-11' data-toggle='tooltip' title='Inactive'></i>";
                        }

                        if (MobileSecurity == null)
                        {
                            mobileStatus.Attributes.Add("class", "text-danger ml-4 position-absolute");
                            mobileStatus.InnerHtml = "<i class='fa fa-warning fs-11' data-toggle='tooltip' title='Not Verified'></i>";
                        }
                        else if (MobileSecurity.IsVerified == 0)
                        {
                            mobileStatus.Attributes.Add("class", "text-warning ml-4 position-absolute");
                            mobileStatus.InnerHtml = "<i class='fa fa-warning fs-11' data-toggle='tooltip' title='Not Verified'></i>";
                        }
                        else
                        {
                            mobileStatus.Attributes.Add("class", "text-success ml-4 position-absolute");
                            mobileStatus.InnerHtml = "<i class='fa fa-check fs-11' data-toggle='tooltip' title='Verified'></i>";
                        }

                        if (GoogleSecurity == null)
                        {
                            txtPin.Attributes.Remove("placeholder");
                            txtPin.Attributes.Add("placeholder", "Not binded");
                            txtPin.Attributes.Add("disabled", "disabled");

                            btnCode.Attributes.Add("disabled", "disabled");

                            btnCode.CssClass = "btn btn-block";

                            ShowGoogleAuthLostLink.Visible = false;
                            ShowGoogleAuthBindMessage.Visible = true;
                        }
                        else
                        {
                            if (GoogleSecurity.IsVerified == 0)
                            {
                                txtPin.Attributes.Remove("placeholder");
                                txtPin.Attributes.Add("placeholder", "Not binded");
                                txtPin.Attributes.Add("disabled", "disabled");

                                btnCode.Attributes.Add("disabled", "disabled");

                                btnCode.CssClass = "btn btn-block";

                                ShowGoogleAuthLostLink.Visible = false;
                                ShowGoogleAuthBindMessage.Visible = true;
                            }
                            else
                            {

                                btnCode.Attributes.Remove("class");
                                btnCode.CssClass = "btn btn-block btn-info";

                                ShowGoogleAuthLostLink.Visible = true;
                                ShowGoogleAuthBindMessage.Visible = false;
                            }
                        }
                    }
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                        if(userAccounts.Count == 1 && userAccounts.FirstOrDefault().IsPrinciple == 0)
                        {
                            consolidatedSummaryNoData.Visible = false;
                        }

                        StringBuilder sb = new StringBuilder();
                        StringBuilder sbNew = new StringBuilder();
                        if (userAccounts.ToList().Count == 0)
                        {
                            hdnIsMAActive.Value = "0";
                            sb.Append("<tr><td colspan='3' class='text-center'>No MA Account</td></tr>");
                        }
                        else
                        {
                            if (userAccounts.Where(x => x.IsVerified == 1).Count() == 0)
                                hdnIsMAActive.Value = "0";
                            else
                                hdnIsMAActive.Value = "1";
                        }

                        if (CustomValues.GetAccounPlan(userAccounts.First(x => x.IsPrimary == 1).HolderClass) == "CORP" || IsImpersonated)
                            addAccountFunction.Visible = false;
                        else
                            addAccountFunction.Visible = true;

                        hdnSelectedAccountNo.Value = userAccounts.First(x => x.IsPrimary == 1).AccountNo;
                        ddlPorfolioAccountsMenuMobile.Items.Clear();
                        ddlPorfolioAccountsMenuMobile.Items.Add(new ListItem { Text = "Select Account", Value = "" });

                        List<UserAccount> userAccountsSorted = new List<UserAccount>();

                        List<UserAccount> userAccountsCORP = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CORP").ToList();
                        if (userAccountsCORP.Count > 0)
                            userAccountsSorted.AddRange(userAccountsCORP);
                        List<UserAccount> userAccountsCASH = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
                        if (userAccountsCASH.Count > 0)
                            userAccountsSorted.AddRange(userAccountsCASH);
                        List<UserAccount> userAccountsJOINT = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                        if (userAccountsJOINT.Count > 0)
                            userAccountsSorted.AddRange(userAccountsJOINT);
                        List<UserAccount> userAccountsEPF = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                        if (userAccountsEPF.Count > 0)
                            userAccountsSorted.AddRange(userAccountsEPF);


                        foreach (UserAccount ua in userAccountsSorted)
                        {

                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(ua.AccountNo);
                            MaHolderReg maHolderReg = new MaHolderReg();
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;


                                if (ua.IsPrinciple == 1)
                                {
                                    sbNew.Append(@"<span class='mr-2'><a href='javascript:;' class='maHolderRegNo btn btn-infos1-default " + (ua.IsVerified == 0 ? "text-danger" : "") + @"' data-accNo='" + maHolderReg.HolderNo + "' data-id='" + maHolderReg.Id + "' data-toggle='tooltip' title='" + (ua.IsVerified == 0 ? "Click to Activate" : "Click to See Details") + @"'><i class='fa fa-caret-down'></i>" + maHolderReg.HolderNo + " - <small class='fs-10'>" + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)) + (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT" ? " (" + maHolderReg.Name2 + ")" : "") + " </small></a></span>");
                                    ddlPorfolioAccountsMenuMobile.Items.Add(new ListItem { Text = maHolderReg.HolderNo + " - " + maHolderReg.Name1 + " - " + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)), Value = maHolderReg.HolderNo.Value.ToString() });
                                    sb.Append(@"<tr>
                                    <td>" + (userAccountsSorted.IndexOf(ua) + 1) + @"</td>
                                    <td>" + maHolderReg.HolderNo + @"</td>
                                    <td>" + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)) + @"</td>
                                </tr>");
                                }
                                else
                                {
                                    sbNew.Append(@"<span class='mr-2'><a href='javascript:;' style='background-color:#d8d8d8;border-color:#d8d8d8;' class='maHolderRegNo btn btn-infos1-default " + (ua.IsVerified == 0 ? "text-danger" : "text-black") + @"' data-accNo='" + maHolderReg.HolderNo + "' data-id='" + maHolderReg.Id + "' data-toggle='tooltip' title='" + (ua.IsVerified == 0 ? "Click to Activate" : "Click to See Details") + @"'><i class='fa fa-caret-down'></i>" + maHolderReg.HolderNo + " - <small class='fs-10'>" + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)) + (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT" ? " (" + maHolderReg.Name2 + ")" : "") + " </small></a></span>");
                                    ddlPorfolioAccountsMenuMobile.Items.Add(new ListItem { Text = maHolderReg.HolderNo + " - " + maHolderReg.Name1 + " - " + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)), Value = maHolderReg.HolderNo.Value.ToString() });
                                    sb.Append(@"<tr>
                                    <td>" + (userAccountsSorted.IndexOf(ua) + 1) + @"</td>
                                    <td>" + maHolderReg.HolderNo + @"</td>
                                    <td>" + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)) + @"</td>
                                </tr>");
                                }
                            }
                        }
                        if (hdnIsVerified.Value == "1")
                        {
                            maAccounts.InnerHtml = sb.ToString();

                            portfolioAccountsMenu.InnerHtml = sbNew.ToString();
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page load: " + ex.Message);
            }
        }

        private static readonly Lazy<ITransactionCodeService> lazyTransactionCodeServiceObj = new Lazy<ITransactionCodeService>(() => new TransactionCodeService());
        public static ITransactionCodeService ITransactionCodeService { get { return lazyTransactionCodeServiceObj.Value; } }

        public static List<UtmcFundInformation> UTMCFundInformations = null;

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMAAccountUtmcMemberInvestments(String HolderNo)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (user != null)
            {
                List<UtmcFundInformation> UTMCFundInformations = null;
                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                }
                if (UTMCFundInformations != null)
                {
                    //HttpContext.Current.Session["SelectedAccountHolderNo"] = HolderNo;
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount uA = userAccounts.Where(x => x.AccountNo == HolderNo).FirstOrDefault();
                        Decimal totalInvestmentUNITS = 0;
                        Decimal totalInvestmentRM = 0;
                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;
                            maHolderReg.FieldDesc1 = CustomValues.GetGroupByScore(uA.SatScore);
                            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = new List<UtmcDetailedMemberInvestment>();
                            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestmentsForChart = new List<UtmcDetailedMemberInvestment>();
                            if (uA.IsVerified == 0)
                            {
                                return new
                                {
                                    utmcDetailedMemberInvestments = utmcDetailedMemberInvestments,
                                    utmcDetailedMemberInvestmentsForChart = utmcDetailedMemberInvestmentsForChart,
                                    userAccount = uA,
                                    accounPlan = CustomValues.GetAccounPlan(uA.HolderClass)
                                };
                            }
                            else
                            {
                                UserInfo userInfo = UserInfos.FirstOrDefault(x => x.holderNo == uA.AccountNo);
                                utmcDetailedMemberInvestments = userInfo.utmcDetailedMemberInvestments;
                                //utmcDetailedMemberInvestments = ServicesManager.GetMAAccountSummary(uA.AccountNo, fundDetailedInformations);
                                Decimal UnrealisedGain = 0;
                                Decimal UnrealisedGainPer = 0;
                                if (utmcDetailedMemberInvestments.Count > 0)
                                {
                                    //utmcDetailedMemberInvestments = IUtmcMemberInvestmentService.GetMAAccountSummary(propertyName, maHolderReg.HolderNo.ToString()).ToList();
                                    //>0
                                    //utmcDetailedMemberInvestments = utmcDetailedMemberInvestments.Where(x => x.utmcMemberInvestments[0].Units > 0).ToList();

                                    decimal InvestUnitsIP = 0;

                                    List<HolderLedger> consolidatedLedgers = new List<HolderLedger>();
                                    utmcDetailedMemberInvestments.ForEach(x =>
                                    {
                                        //consolidatedInvestments.AddRange(x.utmcMemberInvestments);
                                        //utmcCompositionalTransactions.AddRange(x.utmcCompositionalTransactions);
                                        consolidatedLedgers.AddRange(x.holderLedgers);
                                    });
                                    //NEW
                                    List<HolderLedger> holderLedgersFiltered = consolidatedLedgers;
                                    consolidatedLedgers.ForEach(checkX =>
                                    {
                                        if (checkX.TransNO.Contains('X'))
                                        {
                                            string transNoX = checkX.TransNO;
                                            string transNo = checkX.TransNO.TrimEnd('X');
                                            holderLedgersFiltered = holderLedgersFiltered.Where(x => x.TransNO != transNo && x.TransNO != transNoX).ToList();
                                        }
                                    });
                                    holderLedgersFiltered = holderLedgersFiltered.OrderBy(x => x.TransDt).ThenBy(x => Convert.ToInt32(x.TransNO)).ToList();

                                    holderLedgersFiltered.ForEach(calcLedger =>
                                    {
                                        decimal Actual_Cost = 0;
                                        List<HolderLedger> holderLedgerIn = holderLedgersFiltered.Where(x => x.FundID == calcLedger.FundID && x.HolderNo == calcLedger.HolderNo && x.TransDt <= calcLedger.TransDt).OrderBy(x => x.TransDt).ThenBy(x => x.SortSEQ).ThenByDescending(x => x.TransType).ToList();
                                        holderLedgerIn.ForEach(hl =>
                                        {
                                            string Trans_Type = hl.TransType;
                                            if (Trans_Type == "SA")
                                            {
                                                if (!(hl.AccType == "SW"))
                                                {
                                                    Actual_Cost += hl.TransAmt != null ? hl.TransAmt.Value : 0;
                                                    Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                }
                                            }
                                            if (Trans_Type == "RD" || Trans_Type == "TR")
                                            {
                                                if (hl.CurUnitHLDG > 0)
                                                {
                                                    if (!(hl.AccType == "SW"))
                                                    {
                                                        Actual_Cost += hl.TransAmt != null ? hl.TransAmt.Value : 0;
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }
                                                }
                                                else
                                                {
                                                    if (!(hl.AccType == "SW"))
                                                    {
                                                        Actual_Cost -= Convert.ToDecimal(hl.TransAmt);
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }
                                                }
                                            }
                                        });
                                        //calcLedger.HLDRAveCost = utmcCompositionalTransaction.CostRm.Value;
                                    });
                                    var groupLedgerByYear = holderLedgersFiltered
                                                                        .GroupBy(u => u.TransDt.Value.Year)
                                                                       .Select(grp => grp.ToList())
                                                                       .ToList();
                                    decimal TotalUnitsByCurMarketValue = 0;
                                    decimal TotalUnitsByYearMarketValue = 0;
                                    List<UtmcCompositionalTransaction> objUtmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
                                    decimal InvestmentMarketValue = 0;
                                    groupLedgerByYear.ForEach(ledger =>
                                    {
                                        decimal NetInvestmentCost = 0;
                                        decimal GainLoss = 0;
                                        var groupByFundList = ledger.GroupBy(x => x.FundID).Select(grp => grp.OrderBy(y => y.TransDt).ToList()).ToList();
                                        groupByFundList.ForEach(ledgerIn =>
                                        {
                                            UtmcCompositionalTransaction utmcCompositionalTransaction = new UtmcCompositionalTransaction();
                                            ledgerIn.ForEach(calcLedger =>
                                            {

                                                //COST_________________________________________________________________
                                                decimal Cost_RM = 0.0000M;
                                                string IPD_Fund_Code = calcLedger.FundID;
                                                string EPF_No = "";//rdr[1].ToString();
                                                string IPD_Member_Acc_No = calcLedger.HolderNo.ToString();
                                                DateTime dt = calcLedger.TransDt.Value;

                                                var startDate = new DateTime(dt.Year, dt.Month, 1);
                                                var endDate = startDate.AddMonths(1).AddDays(-1);
                                                string LastDay = endDate.ToString("yyyy-MM-dd");

                                                string Effective_Date = dt.ToString("yyyy-MM-dd");
                                                //string Date_Of_Transaction = rdr[7].ToString();                                 
                                                string Settlementdate = calcLedger.TransDt.Value.ToString("yyyy-MM-dd");
                                                DateTime Settlementdatetime = new DateTime();
                                                if (Settlementdate == "")
                                                {
                                                    Settlementdate = Effective_Date;
                                                    Settlementdatetime = dt;
                                                }
                                                else
                                                {
                                                    DateTime dt1 = calcLedger.TransDt.Value;
                                                    Settlementdate = dt1.ToString("yyyy-MM-dd");
                                                    Settlementdatetime = dt1;
                                                }
                                                // DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                                                // string Settlementdate = dt1.ToString("yyyy-MM-dd");

                                                string IPD_Unique_Transaction_ID = calcLedger.TransNO;
                                                string Transaction_Code = calcLedger.TransType;
                                                string AccountType = calcLedger.AccType;
                                                string Reversed_Transaction_ID = "";
                                                decimal AverageCost = calcLedger.HLDRAveCost != null ? calcLedger.HLDRAveCost.Value : 0;

                                                string Sort_SEQ = calcLedger.SortSEQ.ToString();

                                                decimal Units = Math.Abs(calcLedger.TransUnits != null ? calcLedger.TransUnits.Value : 0);
                                                decimal Gross_Amount_RM = calcLedger.TransAmt != null ? calcLedger.TransAmt.Value : 0;
                                                decimal Net_Amount_RM = calcLedger.UnitValue != null ? calcLedger.UnitValue.Value : 0;
                                                decimal Fees_RM = calcLedger.FeeAMT != null ? calcLedger.FeeAMT.Value : 0;
                                                decimal GST_RM = calcLedger.GstAMT.Value;
                                                decimal Proceeds_RM = 0.00M;
                                                decimal Realised_Gain_Loss = 0.00M;
                                                AverageCost = decimal.Round(AverageCost, 4);
                                                char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];

                                                if (Check_reversed == 'X')
                                                {
                                                    if (Transaction_Code == "SA")
                                                    {

                                                        if (AccountType == "SW")
                                                        {
                                                            Transaction_Code = "XI";
                                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                        }
                                                        else
                                                        {
                                                            Transaction_Code = "XS";
                                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                        }



                                                    }
                                                    else if (Transaction_Code == "RD")
                                                    {

                                                        if (AccountType == "SW")
                                                        {
                                                            Transaction_Code = "XO";
                                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                        }
                                                        else
                                                        {
                                                            Transaction_Code = "XR";
                                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                        }
                                                    }

                                                    else if (Transaction_Code == "DD")
                                                    {

                                                        if (Units == 0)
                                                        {
                                                            Transaction_Code = "XD";
                                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                        }
                                                        else
                                                        {
                                                            Transaction_Code = "XV";
                                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                        }

                                                    }
                                                }

                                                if ((Transaction_Code == "SA") || (Transaction_Code == "DD"))
                                                {

                                                }
                                                if ((Transaction_Code == "SA") && (AccountType == "SW"))
                                                {
                                                    Transaction_Code = "SI";

                                                }
                                                if ((Transaction_Code == "RD") && (AccountType == "SW"))
                                                {
                                                    Transaction_Code = "SO";
                                                    //Gross_Amount_RM = 0.00M;                  // as per request SO
                                                    Net_Amount_RM = 0.00M;

                                                }

                                                if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO") || (Transaction_Code == "XR") || (Transaction_Code == "XD") || (Transaction_Code == "XO") || (Transaction_Code == "XV"))
                                                {
                                                    Proceeds_RM = Gross_Amount_RM;
                                                    // Cost_RM = Units * AverageCost;
                                                    Realised_Gain_Loss = Proceeds_RM - Cost_RM;
                                                    // Cost_RM = decimal.Round(Cost_RM, 2);
                                                    Proceeds_RM = decimal.Round(Proceeds_RM, 2);
                                                    Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);


                                                }

                                                //filter by sign requirement 

                                                if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO")
                                                            || (Transaction_Code == "XS") || (Transaction_Code == "XC") || (Transaction_Code == "XD")
                                                            || (Transaction_Code == "XV") || (Transaction_Code == "UX") || (Transaction_Code == "XI")
                                                            || (Transaction_Code == "RO") || (Transaction_Code == "RI") || (Transaction_Code == "HO") || (Transaction_Code == "XO"))
                                                {

                                                    if (!(Net_Amount_RM == 0))
                                                    {
                                                        Net_Amount_RM = -Net_Amount_RM;

                                                    }
                                                    if (!(Fees_RM == 0))
                                                    {
                                                        Fees_RM = -Fees_RM;

                                                    }
                                                    if (!(GST_RM == 0))
                                                    {
                                                        GST_RM = -GST_RM;

                                                    }
                                                    Gross_Amount_RM = -Gross_Amount_RM;

                                                    if (!(Transaction_Code == "XO"))
                                                    {
                                                        Units = -Units;
                                                    }
                                                    if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO"))
                                                    {
                                                        Proceeds_RM = -Proceeds_RM;
                                                    }


                                                }
                                                ////////// hard code  adjustment Start

                                                if (Transaction_Code == "SA")
                                                {
                                                    Transaction_Code = "NS";
                                                    // final  aaa ---
                                                    Cost_RM = Gross_Amount_RM;
                                                    // final  aaa ---

                                                }
                                                if (Transaction_Code == "SI")
                                                {
                                                    // final  aaa ---
                                                    Cost_RM = Gross_Amount_RM;
                                                    // final  aaa ---


                                                }
                                                if (Transaction_Code == "DD")
                                                {
                                                    Transaction_Code = "DV";
                                                    Realised_Gain_Loss = 0.00M;
                                                }




                                                if (Transaction_Code == "XD")
                                                {
                                                    Realised_Gain_Loss = 0.00M;
                                                    // Net_Amount_RM = Gross_Amount_RM;
                                                    Proceeds_RM = 0.00M;

                                                }

                                                if (Transaction_Code == "XV")
                                                {
                                                    Realised_Gain_Loss = 0.00M;

                                                    // Net_Amount_RM = Gross_Amount_RM;
                                                    Proceeds_RM = 0.00M;
                                                }




                                                if (Transaction_Code == "RD")
                                                {
                                                    Transaction_Code = "RE";
                                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);


                                                    Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);

                                                    // final  aaa ---
                                                    Gross_Amount_RM = Proceeds_RM;
                                                    Net_Amount_RM = Proceeds_RM;
                                                    // final  aaa ---

                                                    Cost_RM = -Cost_RM;
                                                }
                                                if (Transaction_Code == "TR")
                                                {
                                                    Transaction_Code = "TO";
                                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);
                                                    Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                                    // final  aaa ---
                                                    Gross_Amount_RM = Proceeds_RM;
                                                    Net_Amount_RM = Proceeds_RM;
                                                    // final  aaa ---
                                                }


                                                if (Transaction_Code == "SO" || Transaction_Code == "XO")
                                                {
                                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);
                                                    Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                                    // final  aaa ---
                                                    Gross_Amount_RM = Proceeds_RM;
                                                    Net_Amount_RM = Proceeds_RM;
                                                    // final  aaa ---
                                                    Cost_RM = -Cost_RM;
                                                }

                                                if (Transaction_Code == "XO")
                                                {
                                                    Cost_RM = -Cost_RM;
                                                    Realised_Gain_Loss = -Realised_Gain_Loss;
                                                    //Units = Units;

                                                }

                                                // FILTERED 0 UNIT DIVIDEND 
                                                Boolean bolFILTER = true;
                                                if (Transaction_Code == "DV")
                                                {
                                                    if (Units == 0)
                                                    {
                                                        bolFILTER = false;
                                                    }

                                                }

                                                if (Transaction_Code == "XS")

                                                {
                                                    Cost_RM = Gross_Amount_RM;

                                                }


                                                if (Transaction_Code == "XI" || Transaction_Code == "XR")
                                                {
                                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);

                                                    if (Transaction_Code == "XI")
                                                    {
                                                        Cost_RM = Gross_Amount_RM;
                                                    }

                                                    if (Transaction_Code == "XR")
                                                    {
                                                        Realised_Gain_Loss = -(Proceeds_RM - Cost_RM);
                                                        Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);

                                                    }

                                                }

                                                if (Transaction_Code == "DV" || Transaction_Code == "XV")
                                                {
                                                    Net_Amount_RM = Gross_Amount_RM;
                                                }
                                                if (bolFILTER)
                                                {
                                                    utmcCompositionalTransaction = new UtmcCompositionalTransaction()
                                                    {
                                                        //EpfIpdCode = ConfigurationManager.AppSettings["IPD_CODE"].ToString(),
                                                        IpdFundCode = IPD_Fund_Code,
                                                        MemberEpfNo = EPF_No,
                                                        IpdMemberAccNo = IPD_Member_Acc_No,
                                                        EffectiveDate = endDate,
                                                        DateOfTransaction = dt,
                                                        DateOfSettlement = Settlementdatetime,
                                                        IpdUniqueTransactionId = IPD_Unique_Transaction_ID,
                                                        TransactionCode = Transaction_Code,
                                                        ReversedTransactionId = Reversed_Transaction_ID,
                                                        Units = Units,
                                                        ActualUnits = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().ActualUnits + Units : Units,
                                                        GrossAmountRm = Gross_Amount_RM,
                                                        NetAmountRm = Net_Amount_RM,
                                                        FeesRm = Fees_RM,
                                                        GstRm = GST_RM,
                                                        CostRm = Cost_RM,
                                                        ActualCost = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().ActualCost + Cost_RM : Cost_RM,
                                                        ProceedsRm = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().ProceedsRm.Value + Proceeds_RM : Proceeds_RM,
                                                        RealisedGainLoss = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().RealisedGainLoss.Value + Realised_Gain_Loss : Realised_Gain_Loss,
                                                        ReportDate = endDate,
                                                        TransPR = calcLedger.TransPr != null ? calcLedger.TransPr.Value : 0,
                                                        AverageCost = calcLedger.HLDRAveCost != null ? calcLedger.HLDRAveCost.Value : 0,
                                                        CurUnitHldg = calcLedger.CurUnitHLDG != null ? calcLedger.CurUnitHLDG.Value : 0,
                                                        SalesChargePer = calcLedger.AppFee != null ? calcLedger.AppFee.Value : 0,
                                                        SalesChargeAmt = calcLedger.FeeAMT,
                                                        TransType = calcLedger.TransType
                                                    };
                                                    objUtmcCompositionalTransactions.Add(utmcCompositionalTransaction);
                                                }

                                                //COST_________________________________________________________________
                                            });


                                            UtmcFundInformation uFI = UTMCFundInformations.Where(x => x.IpdFundCode == ledgerIn.FirstOrDefault().FundID).FirstOrDefault();
                                            InvestUnitsIP += ledgerIn.Sum(x => (x.TransUnits != null ? x.TransUnits.Value : 0));
                                            NetInvestmentCost += utmcCompositionalTransaction.CostRm.Value;
                                            GainLoss += utmcCompositionalTransaction.RealisedGainLoss.Value;
                                            TotalUnitsByCurMarketValue += (ledgerIn.Sum(x => ((x.TransUnits != null ? x.TransUnits.Value : 0) * (uFI != null ? uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice : 0))));
                                            if (ledgerIn.FirstOrDefault().TransDt.Value.Year == DateTime.Now.Year)
                                            {
                                                TotalUnitsByYearMarketValue = TotalUnitsByCurMarketValue;
                                            }
                                            else
                                            {
                                                TotalUnitsByYearMarketValue += (ledgerIn.Sum(x => ((x.TransUnits != null ? x.TransUnits.Value : 0) * (x.TransPr != null ? x.TransPr.Value : 0))));
                                            }
                                        });

                                        decimal NetInvestmentCostAllFunds = 0;
                                        decimal GainLossAllFunds = 0;
                                        objUtmcCompositionalTransactions.GroupBy(x => x.IpdFundCode).Select(grp => grp.ToList()).ToList().ForEach(x =>
                                        {
                                            NetInvestmentCostAllFunds += x.LastOrDefault().ActualCost;
                                            GainLossAllFunds += x.LastOrDefault().RealisedGainLoss.Value;
                                        });
                                    });

                                    holderLedgersFiltered.ForEach(ledger =>
                                    {

                                    });

                                    //NEW

                                    Response responseTC = ITransactionCodeService.GetData(0, 0, false);
                                    List<TransactionCode> transactionCodes = new List<TransactionCode>();
                                    if (responseTC.IsSuccess)
                                    {
                                        transactionCodes = (List<TransactionCode>)responseTC.Data;
                                    }
                                    //objUtmcCompositionalTransactions.ForEach(x =>
                                    //{
                                    //    TransactionCode transactionCode = transactionCodes.FirstOrDefault(y => y.Code == x.TransType);
                                    //    x.TransactionCode = transactionCode != null ? transactionCode.Name : x.TransType;
                                    //});
                                    holderLedgersFiltered.ForEach(x =>
                                    {
                                        TransactionCode transactionCode = transactionCodes.FirstOrDefault(y => y.Code == x.TransType);
                                        if (x.AccType == "SW")
                                        {
                                            x.TransType = (x.TransType == "SA" ? "Switch In" : (x.TransType == "RD" ? "Switch Out" : (transactionCode != null ? transactionCode.Name : x.TransType)));
                                        }
                                        else
                                        {
                                            x.TransType = transactionCode != null ? transactionCode.Name : x.TransType;
                                        }
                                        try
                                        {
                                            x.CostRM = (objUtmcCompositionalTransactions.FirstOrDefault(o => o.IpdUniqueTransactionId == x.TransNO) != null ? objUtmcCompositionalTransactions.FirstOrDefault(o => o.IpdUniqueTransactionId == x.TransNO).CostRm.HasValue ? objUtmcCompositionalTransactions.FirstOrDefault(o => o.IpdUniqueTransactionId == x.TransNO).CostRm.Value : 0 : 0);
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.WriteLog("GetMAAccountUtmcMemberInvestments holderLedgersFiltered.ForEach(x => - " + ex.Message + " - Exception");
                                        }
                                    });

                                    utmcDetailedMemberInvestments.ForEach(x =>
                                    {
                                        //x.utmcCompositionalTransactions = objUtmcCompositionalTransactions.Where(y => y.IpdFundCode == x.fundDetailedInformation.UtmcFundInformation.IpdFundCode).ToList().OrderByDescending(y => y.DateOfTransaction.Value).ThenByDescending(y => y.IpdUniqueTransactionId).ToList();
                                        x.holderLedgers = holderLedgersFiltered.Where(y => y.FundID == x.holderInv.FundId).ToList().OrderByDescending(y => y.TransDt).ThenByDescending(y => Convert.ToInt32(y.TransNO)).ToList();
                                        //?????????????????????????????????????????????????????????????????
                                        //x.utmcCompositionalTransactions = objUtmcCompositionalTransactions;
                                        utmcDetailedMemberInvestmentsForChart.Add(new UtmcDetailedMemberInvestment
                                        {
                                            //fundDetailedInformation = x.fundDetailedInformation,
                                            //utmcCompositionalTransactions = x.utmcCompositionalTransactions,
                                            //utmcMemberInvestments = x.utmcMemberInvestments.OrderBy(y => y.EffectiveDate).GroupBy(y => new { y.EffectiveDate.Year, y.EffectiveDate.Month }).Select(grp => grp.LastOrDefault()).ToList(),
                                            //holderLedgers = holderLedgersFiltered.Where(y => y.FundID == x.holderInv.FundId).ToList().OrderByDescending(y => y.TransDt).ThenByDescending(y => Convert.ToInt32(y.TransNO)).ToList()
                                        });
                                    });

                                    Decimal actualCost = utmcDetailedMemberInvestments.Sum(x => x.utmcCompositionalTransactions.FirstOrDefault().ActualCost);

                                    Decimal marketValue = utmcDetailedMemberInvestments.Sum(x => (x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice));

                                    UnrealisedGain = marketValue - actualCost;
                                    UnrealisedGainPer = ((marketValue - actualCost) / actualCost) * 100;

                                    totalInvestmentUNITS = utmcDetailedMemberInvestments.Sum(x => x.holderInv.CurrUnitHldg);
                                    totalInvestmentRM = marketValue;
                                }
                                //// User account bank details
                                //StringBuilder stringBuilderBank = new StringBuilder();
                                //List<BanksDef> bankdefs = new List<BanksDef>();
                                //Response responseBDList = IBanksDefService.GetData(0, 0, true);
                                //if (responseBDList.IsSuccess)
                                //{
                                //    bankdefs = (List<BanksDef>)responseBDList.Data;
                                //    Response responseMHBList = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserAccountId), uA.Id.ToString(), true, 0, 0, true);
                                //    if (responseMHBList.IsSuccess)
                                //    {
                                //        List<MaHolderBank> hbs = (List<MaHolderBank>)responseMHBList.Data;
                                //        if (hbs.Count > 0)
                                //        {
                                //            MaHolderBank hb = hbs.FirstOrDefault(x => x.Status == 1);
                                //            if (hbs.FirstOrDefault().Status != 1)
                                //            {
                                //                hb = hbs.FirstOrDefault(x => x.Status == 0 || x.Status == 9);
                                //                hb.BankDefIdBanksDef = bankdefs.FirstOrDefault(x => x.Id == hb.BankDefId);
                                //                if (hb != null)
                                //                {
                                //                    stringBuilderBank.Append(@"<tr>
                                //                                    <td> Bank </td>
                                //                                    <td> " + hb.BankDefIdBanksDef.Name + @" </ td>
                                //                                </tr>
                                //                                <tr>
                                //                                    <td> Account Name </td>
                                //                                    <td> " + hb.AccountName + @" </td>
                                //                                </tr>
                                //                                <tr>
                                //                                    <td> Account Number </td>
                                //                                    <td> " + hb.BankAccountNo + @" </td>
                                //                                </tr>");
                                //                }
                                //                else
                                //                    stringBuilderBank.Append("Bank details not found.");
                                //            }
                                //        }
                                //        else
                                //            stringBuilderBank.Append("Bank details not found.");
                                //    }
                                //}
                                //// End
                                //// MA Address
                                //string address = "";
                                //MaHolderReg maHolder = new MaHolderReg();
                                //if (responseMHR.IsSuccess)
                                //{
                                //    maHolder = (MaHolderReg)responseMHR.Data;
                                //    if (maHolder != null)
                                //    {
                                //        address += maHolder.Addr1 + "<br />";
                                //        address += maHolder.Addr2 + "<br />";
                                //        address += maHolder.Addr3 + "<br />";
                                //        address += "Post code: " + maHolder.Postcode.ToString();
                                //    }
                                //}
                                //else
                                //    address = "Address not found";
                                ////check user hardcopy
                                //string hardCopyStatus = "";
                                //Response responseUList = IUserService.GetSingle(user.Id);
                                //if (responseUList.IsSuccess)
                                //{
                                //    User userHard = (User)responseUList.Data;
                                //    if (userHard.IsHardCopy == 1)
                                //    {
                                //        hardCopyStatus = "HardCopy Activated";
                                //    }
                                //    else
                                //    {
                                //        hardCopyStatus = "HardCopy Deactivated";
                                //    }
                                //}
                                ////End

                                string bankDetails = BindBankApproval(uA.UserId, uA);

                                return new
                                {
                                    totalInvestmentUNITS = totalInvestmentUNITS,
                                    totalInvestmentRM = totalInvestmentRM,
                                    utmcDetailedMemberInvestments = utmcDetailedMemberInvestments,
                                    //utmcDetailedMemberInvestmentsForChart = utmcDetailedMemberInvestmentsForChart,
                                    userAccount = uA,
                                    maHolderReg = maHolderReg,
                                    accounPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls),
                                    UnrealisedGain = UnrealisedGain,
                                    UnrealisedGainPer = UnrealisedGainPer,
                                    bankDetails = bankDetails
                                    //BankDetails = stringBuilderBank.ToString(),
                                    //MAAddress = address,
                                    //HardCopyStatus = hardCopyStatus
                                };
                            }
                        }
                        else
                        {
                            Logger.WriteLog("GetMAAccountUtmcMemberInvestments responseMHR - " + responseMHR.Message + " - Exception");
                        }
                    }
                    else
                    {
                        Logger.WriteLog("GetMAAccountUtmcMemberInvestments responseUAList - " + responseUAList.Message + " - Exception");
                    }
                }
            }
            return null;
        }

        public static string BindBankApproval(int UserId, UserAccount uA)
        {
            string result = string.Empty;
            StringBuilder filterQ = new StringBuilder();

            string mainQ = (@"select ua.account_no, bd.name, mhb.account_name, mhb.bank_account_no, mhb.image, uab.* from ");
            string mainQCount = (@"select count(*) from ");
            filterQ.Append(@" user_account_banks uab
join user_accounts ua on ua.id = uab.user_account_id
join ma_holder_bank mhb on mhb.id = uab.ma_holder_bank_id
join banks_def bd on bd.id = mhb.bank_def_id where uab.status = 1 and ua.account_no = '" + uA.AccountNo + "' ");

            Response reponseBankBinded = GenericService.GetDataByQuery(mainQ + filterQ.ToString(), 0, 20, false, null, false, null, true);
            if (reponseBankBinded.IsSuccess)
            {
                var uoDyn = reponseBankBinded.Data;
                var responseJSON = JsonConvert.SerializeObject(uoDyn);
                List<PortfolioBank> hbs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PortfolioBank>>(responseJSON);


                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + reponseBankBinded.Message + "\", '');", true);
                //}


                //Response responseBDList = IBanksDefService.GetData(0, 0, true);
                //List<BanksDef> bankdefs = new List<BanksDef>();
                //if (responseBDList.IsSuccess)
                //{
                //    bankdefs = (List<BanksDef>)responseBDList.Data;
                //}
                //string filter = " user_id = '" + UserId + "' and status=1 ";

                //Response responseuserMHBList = IMaHolderBankService.GetDataByFilter(filter, 0, 0, true);
                //if (responseuserMHBList.IsSuccess)
                //{
                //List<MaHolderBank> hbs = (List<MaHolderBank>)responseuserMHBList.Data;
                if (hbs.Count == 0)
                {
                    return "Not Binded";
                }
                else if (hbs.Count > 0)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    int idx = 1;
                    foreach (var hb in hbs)
                    {
                        string image = !string.IsNullOrEmpty(hb.image) ? "<i class='fa fa-eye'></i>" : "";
                        string imgURL = "\"" + hb.image + "\"";
                        PortfolioBank userAccountBanks = hbs.Where(x => x.ma_holder_bank_id == hb.id).FirstOrDefault();
                        stringBuilder.Append(@"<tr>
                                        <td>" + idx + @"</td>
                                        <td>" + hb.name + @"</td>
                                        <td>" + hb.account_name + @"</td>
                                        <td>" + hb.bank_account_no + @"</td>
                                        <td><a target='_blank' href=" + imgURL + ">" + image + @"</a></td>
                                        <td>" + hb.created_date + @"</td>
                                    </tr>");
                        idx++;
                    }
                    result = stringBuilder.ToString();
                }
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMAAccountUtmcMemberInvestmentsTrans(UserAccount primaryAcc, MaHolderReg holderReg) // Used for Transactions
        {
            UserAccount uA = primaryAcc;

            holderReg.FieldDesc1 = CustomValues.GetGroupByScore(uA.SatScore);
            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = new List<UtmcDetailedMemberInvestment>();
            if (uA.IsVerified == 0)
            {
                return utmcDetailedMemberInvestments;
            }
            else
            {
                MaHolderReg maHolderReg = holderReg;
                String propertyName = nameof(UtmcMemberInvestment.IpdMemberAccNo);
                dynamic accSummary = GetMAAccountUtmcMemberInvestments(uA.AccountNo);
                utmcDetailedMemberInvestments = (List<UtmcDetailedMemberInvestment>)accSummary.utmcDetailedMemberInvestments;

                return utmcDetailedMemberInvestments;
            }
        }

        public static List<UserInfo> UserInfos = new List<UserInfo>();

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetConsolidatedAccountSummary()
        {
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                if (user != null)
                {
                    Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" 1=1 ", 0, 0, false);
                    if (responseUFIList.IsSuccess)
                    {
                        UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                    }

                    if (UTMCFundInformations != null)
                    {
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            userAccounts = userAccounts.Where(x => x.IsVerified == 1 && x.Status == 1).ToList();

                            List<UserAccount> userAccountsSorted = new List<UserAccount>();

                            List<UserAccount> userAccountsCORP = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CORP").ToList();
                            if (userAccountsCORP.Count > 0)
                                userAccountsSorted.AddRange(userAccountsCORP);
                            List<UserAccount> userAccountsCASH = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
                            if (userAccountsCASH.Count > 0)
                                userAccountsSorted.AddRange(userAccountsCASH);
                            List<UserAccount> userAccountsJOINT = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                            if (userAccountsJOINT.Count > 0)
                                userAccountsSorted.AddRange(userAccountsJOINT);
                            List<UserAccount> userAccountsEPF = userAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                            if (userAccountsEPF.Count > 0)
                                userAccountsSorted.AddRange(userAccountsEPF);

                            userAccounts = userAccountsSorted;

                            List<PortfolioAllocation> PortfolioAllocations = new List<PortfolioAllocation>();
                            List<FundAllocation> FundAllocations = new List<FundAllocation>();
                            List<AccountAllocation> AccountAllocations = new List<AccountAllocation>();
                            List<FCAllocation> FCAllocations = new List<FCAllocation>();
                            Decimal consolidatedTotalActualCost = 0;
                            Decimal consolidatedTotalInvestmentRM = 0;
                            Decimal consolidatedTotalInvestmentUNITS = 0;
                            Decimal consolidatedAccountUnrealisedGainRM = 0;
                            List<HolderLedger> consolidatedLedgersAll = new List<HolderLedger>();
                            List<UtmcMemberInvestment> consolidatedInvestments = new List<UtmcMemberInvestment>();
                            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
                            DateTime asAtDate = DateTime.Now;
                            decimal InvestUnits = 0;
                            decimal InvestCost = 0;
                            decimal InvestMarketValue = 0;
                            decimal RealizedProfit = 0;
                            decimal RealizedProfitPer = 0;
                            decimal TotalUnitsByYearMarketValueAll = 0;

                            UserInfos.Clear();

                            userAccounts.ForEach(uA =>
                            {
                                Decimal totalInvestmentUNITS = 0;
                                Decimal totalInvestmentRM = 0;
                                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                                MaHolderReg maHolderReg = new MaHolderReg();
                                if (responseMHR.IsSuccess)
                                {
                                    maHolderReg = (MaHolderReg)responseMHR.Data;
                                    List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments = new List<UtmcDetailedMemberInvestment>();

                                    //API
                                    utmcDetailedMemberInvestments = ServicesManager.GetMAAccountSummary(uA.AccountNo, UTMCFundInformations);
                                    UserInfo userInfo = new UserInfo();
                                    userInfo.holderNo = uA.AccountNo;
                                    userInfo.utmcDetailedMemberInvestments = utmcDetailedMemberInvestments;

                                    UserInfos.Add(userInfo);
                                    //-----------------------------------------------------------------------
                                    Decimal UnrealisedGain = 0;
                                    Decimal UnrealisedGainPer = 0;
                                    if (utmcDetailedMemberInvestments.Count > 0)
                                    {

                                        decimal InvestUnitsIP = 0;

                                        List<HolderLedger> consolidatedLedgers = new List<HolderLedger>();
                                        utmcDetailedMemberInvestments.ForEach(x =>
                                        {
                                            consolidatedLedgers.AddRange(x.holderLedgers);
                                            consolidatedLedgersAll.AddRange(x.holderLedgers);
                                        });
                                        //NEW
                                        List<HolderLedger> holderLedgersFiltered = consolidatedLedgers;
                                        consolidatedLedgers.ForEach(checkX =>
                                        {
                                            if (checkX.TransNO.Contains('X'))
                                            {
                                                string transNoX = checkX.TransNO;
                                                string transNo = checkX.TransNO.TrimEnd('X');
                                                holderLedgersFiltered = holderLedgersFiltered.Where(x => x.TransNO != transNo && x.TransNO != transNoX).ToList();
                                            }
                                        });
                                        holderLedgersFiltered = holderLedgersFiltered.OrderBy(x => x.TransDt).ThenBy(x => Convert.ToInt32(x.TransNO)).ToList();

                                        holderLedgersFiltered.ForEach(calcLedger =>
                                        {
                                            decimal Actual_Cost = 0;
                                            List<HolderLedger> holderLedgerIn = holderLedgersFiltered.Where(x => x.FundID == calcLedger.FundID && x.HolderNo == calcLedger.HolderNo && x.TransDt <= calcLedger.TransDt).OrderBy(x => x.TransDt).ThenBy(x => x.SortSEQ).ThenByDescending(x => x.TransType).ToList();
                                            holderLedgerIn.ForEach(hl =>
                                            {
                                                string Trans_Type = hl.TransType;
                                                if (Trans_Type == "SA")
                                                {
                                                    if (!(hl.AccType == "SW"))
                                                    {
                                                        Actual_Cost += hl.TransAmt != null ? hl.TransAmt.Value : 0;
                                                        Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                    }
                                                }
                                                if (Trans_Type == "RD" || Trans_Type == "TR")
                                                {
                                                    if (hl.CurUnitHLDG > 0)
                                                    {
                                                        if (!(hl.AccType == "SW"))
                                                        {
                                                            Actual_Cost += hl.TransAmt != null ? hl.TransAmt.Value : 0;
                                                            Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!(hl.AccType == "SW"))
                                                        {
                                                            Actual_Cost -= Convert.ToDecimal(hl.TransAmt);
                                                            Actual_Cost = decimal.Round(Actual_Cost, 2);
                                                        }
                                                    }
                                                }
                                            });
                                        });
                                        var groupLedgerByYear = holderLedgersFiltered
                                                                            .GroupBy(u => u.TransDt.Value.Year)
                                                                           .Select(grp => grp.ToList())
                                                                           .ToList();
                                        decimal TotalUnitsByCurMarketValue = 0;
                                        decimal TotalUnitsByYearMarketValue = 0;
                                        List<UtmcCompositionalTransaction> objUtmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
                                        decimal InvestmentMarketValue = 0;
                                        groupLedgerByYear.ForEach(ledger =>
                                        {
                                            decimal NetInvestmentCost = 0;
                                            decimal GainLoss = 0;
                                            var groupByFundList = ledger.GroupBy(x => x.FundID).Select(grp => grp.OrderBy(y => y.TransDt).ToList()).ToList();
                                            groupByFundList.ForEach(ledgerIn =>
                                            {
                                                UtmcCompositionalTransaction utmcCompositionalTransaction = new UtmcCompositionalTransaction();
                                                ledgerIn.ForEach(calcLedger =>
                                                {

                                                    //COST_________________________________________________________________
                                                    decimal Cost_RM = 0.00M;
                                                    string IPD_Fund_Code = calcLedger.FundID;
                                                    string EPF_No = "";//rdr[1].ToString();
                                                    string IPD_Member_Acc_No = calcLedger.HolderNo.ToString();
                                                    DateTime dt = calcLedger.TransDt.Value;

                                                    var startDate = new DateTime(dt.Year, dt.Month, 1);
                                                    var endDate = startDate.AddMonths(1).AddDays(-1);
                                                    string LastDay = endDate.ToString("yyyy-MM-dd");

                                                    string Effective_Date = dt.ToString("yyyy-MM-dd");
                                                    string Settlementdate = calcLedger.TransDt.Value.ToString("yyyy-MM-dd");
                                                    DateTime Settlementdatetime = new DateTime();
                                                    if (Settlementdate == "")
                                                    {
                                                        Settlementdate = Effective_Date;
                                                        Settlementdatetime = dt;
                                                    }
                                                    else
                                                    {
                                                        DateTime dt1 = calcLedger.TransDt.Value;
                                                        Settlementdate = dt1.ToString("yyyy-MM-dd");
                                                        Settlementdatetime = dt1;
                                                    }
                                                    string IPD_Unique_Transaction_ID = calcLedger.TransNO;
                                                    string Transaction_Code = calcLedger.TransType;
                                                    string AccountType = calcLedger.AccType;
                                                    string Reversed_Transaction_ID = "";
                                                    decimal AverageCost = calcLedger.HLDRAveCost != null ? calcLedger.HLDRAveCost.Value : 0;

                                                    string Sort_SEQ = calcLedger.SortSEQ.ToString();

                                                    decimal Units = Math.Abs(calcLedger.TransUnits != null ? calcLedger.TransUnits.Value : 0);
                                                    decimal Gross_Amount_RM = calcLedger.TransAmt != null ? calcLedger.TransAmt.Value : 0;
                                                    decimal Net_Amount_RM = calcLedger.UnitValue != null ? calcLedger.UnitValue.Value : 0;
                                                    decimal Fees_RM = calcLedger.FeeAMT != null ? calcLedger.FeeAMT.Value : 0;
                                                    decimal GST_RM = calcLedger.GstAMT.Value;
                                                    decimal Proceeds_RM = 0.00M;
                                                    decimal Realised_Gain_Loss = 0.00M;
                                                    AverageCost = decimal.Round(AverageCost, 4);
                                                    char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];

                                                    if (Check_reversed == 'X')
                                                    {
                                                        if (Transaction_Code == "SA")
                                                        {

                                                            if (AccountType == "SW")
                                                            {
                                                                Transaction_Code = "XI";
                                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                            }
                                                            else
                                                            {
                                                                Transaction_Code = "XS";
                                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                            }



                                                        }
                                                        else if (Transaction_Code == "RD")
                                                        {

                                                            if (AccountType == "SW")
                                                            {
                                                                Transaction_Code = "XO";
                                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                            }
                                                            else
                                                            {
                                                                Transaction_Code = "XR";
                                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                            }
                                                        }

                                                        else if (Transaction_Code == "DD")
                                                        {

                                                            if (Units == 0)
                                                            {
                                                                Transaction_Code = "XD";
                                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                            }
                                                            else
                                                            {
                                                                Transaction_Code = "XV";
                                                                Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                                            }

                                                        }
                                                    }

                                                    if ((Transaction_Code == "SA") || (Transaction_Code == "DD"))
                                                    {

                                                    }
                                                    if ((Transaction_Code == "SA") && (AccountType == "SW"))
                                                    {
                                                        Transaction_Code = "SI";

                                                    }
                                                    if ((Transaction_Code == "RD") && (AccountType == "SW"))
                                                    {
                                                        Transaction_Code = "SO";
                                                        //Gross_Amount_RM = 0.00M;                  // as per request SO
                                                        Net_Amount_RM = 0.00M;

                                                    }

                                                    if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO") || (Transaction_Code == "XR") || (Transaction_Code == "XD") || (Transaction_Code == "XO") || (Transaction_Code == "XV"))
                                                    {
                                                        Proceeds_RM = Gross_Amount_RM;

                                                        Realised_Gain_Loss = Proceeds_RM - Cost_RM;

                                                        Proceeds_RM = decimal.Round(Proceeds_RM, 2);
                                                        Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);


                                                    }

                                                    //filter by sign requirement 

                                                    if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO")
                                                                    || (Transaction_Code == "XS") || (Transaction_Code == "XC") || (Transaction_Code == "XD")
                                                                    || (Transaction_Code == "XV") || (Transaction_Code == "UX") || (Transaction_Code == "XI")
                                                                    || (Transaction_Code == "RO") || (Transaction_Code == "RI") || (Transaction_Code == "HO") || (Transaction_Code == "XO"))
                                                    {

                                                        if (!(Net_Amount_RM == 0))
                                                        {
                                                            Net_Amount_RM = -Net_Amount_RM;

                                                        }
                                                        if (!(Fees_RM == 0))
                                                        {
                                                            Fees_RM = -Fees_RM;

                                                        }
                                                        if (!(GST_RM == 0))
                                                        {
                                                            GST_RM = -GST_RM;

                                                        }
                                                        Gross_Amount_RM = -Gross_Amount_RM;

                                                        if (!(Transaction_Code == "XO"))
                                                        {
                                                            Units = -Units;
                                                        }
                                                        if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO"))
                                                        {
                                                            Proceeds_RM = -Proceeds_RM;
                                                        }


                                                    }
                                                    ////////// hard code  adjustment Start

                                                    if (Transaction_Code == "SA")
                                                    {
                                                        Transaction_Code = "NS";
                                                        // final  aaa ---
                                                        Cost_RM = Gross_Amount_RM;
                                                        // final  aaa ---

                                                    }
                                                    if (Transaction_Code == "SI")
                                                    {
                                                        // final  aaa ---
                                                        Cost_RM = Gross_Amount_RM;
                                                        // final  aaa ---


                                                    }
                                                    if (Transaction_Code == "DD")
                                                    {
                                                        Transaction_Code = "DV";
                                                        Realised_Gain_Loss = 0.00M;
                                                    }




                                                    if (Transaction_Code == "XD")
                                                    {
                                                        Realised_Gain_Loss = 0.00M;
                                                        Proceeds_RM = 0.00M;

                                                    }

                                                    if (Transaction_Code == "XV")
                                                    {
                                                        Realised_Gain_Loss = 0.00M;
                                                        Proceeds_RM = 0.00M;
                                                    }




                                                    if (Transaction_Code == "RD")
                                                    {
                                                        Transaction_Code = "RE";
                                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);


                                                        Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);

                                                        // final  aaa ---
                                                        Gross_Amount_RM = Proceeds_RM;
                                                        Net_Amount_RM = Proceeds_RM;
                                                        // final  aaa ---

                                                        Cost_RM = -Cost_RM;
                                                    }
                                                    if (Transaction_Code == "TR")
                                                    {
                                                        Transaction_Code = "TO";
                                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);
                                                        Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                                        // final  aaa ---
                                                        Gross_Amount_RM = Proceeds_RM;
                                                        Net_Amount_RM = Proceeds_RM;
                                                        // final  aaa ---
                                                    }


                                                    if (Transaction_Code == "SO" || Transaction_Code == "XO")
                                                    {
                                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);
                                                        Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                                        // final  aaa ---
                                                        Gross_Amount_RM = Proceeds_RM;
                                                        Net_Amount_RM = Proceeds_RM;
                                                        // final  aaa ---
                                                        Cost_RM = -Cost_RM;
                                                    }

                                                    if (Transaction_Code == "XO")
                                                    {
                                                        Cost_RM = -Cost_RM;
                                                        Realised_Gain_Loss = -Realised_Gain_Loss;

                                                    }

                                                    // FILTERED 0 UNIT DIVIDEND 
                                                    Boolean bolFILTER = true;
                                                    if (Transaction_Code == "DV")
                                                    {
                                                        if (Units == 0)
                                                        {
                                                            bolFILTER = false;
                                                        }

                                                    }

                                                    if (Transaction_Code == "XS")

                                                    {
                                                        Cost_RM = Gross_Amount_RM;

                                                    }


                                                    if (Transaction_Code == "XI" || Transaction_Code == "XR")
                                                    {
                                                        Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate, holderLedgersFiltered);

                                                        if (Transaction_Code == "XI")
                                                        {
                                                            Cost_RM = Gross_Amount_RM;
                                                        }

                                                        if (Transaction_Code == "XR")
                                                        {
                                                            Realised_Gain_Loss = -(Proceeds_RM - Cost_RM);
                                                            Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);

                                                        }

                                                    }

                                                    if (Transaction_Code == "DV" || Transaction_Code == "XV")
                                                    {
                                                        Net_Amount_RM = Gross_Amount_RM;
                                                    }
                                                    if (bolFILTER)
                                                    {
                                                        utmcCompositionalTransaction = new UtmcCompositionalTransaction()
                                                        {
                                                            IpdFundCode = IPD_Fund_Code,
                                                            MemberEpfNo = EPF_No,
                                                            IpdMemberAccNo = IPD_Member_Acc_No,
                                                            EffectiveDate = endDate,
                                                            DateOfTransaction = dt,
                                                            DateOfSettlement = Settlementdatetime,
                                                            IpdUniqueTransactionId = IPD_Unique_Transaction_ID,
                                                            TransactionCode = Transaction_Code,
                                                            ReversedTransactionId = Reversed_Transaction_ID,
                                                            Units = Units,
                                                            ActualUnits = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().ActualUnits + Units : Units,
                                                            GrossAmountRm = Gross_Amount_RM,
                                                            NetAmountRm = Net_Amount_RM,
                                                            FeesRm = Fees_RM,
                                                            GstRm = GST_RM,
                                                            CostRm = Cost_RM,
                                                            ActualCost = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().ActualCost + Cost_RM : Cost_RM,
                                                            ProceedsRm = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().ProceedsRm.Value + Proceeds_RM : Proceeds_RM,
                                                            RealisedGainLoss = objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault() != null ? objUtmcCompositionalTransactions.Where(x => x.IpdFundCode == IPD_Fund_Code).LastOrDefault().RealisedGainLoss.Value + Realised_Gain_Loss : Realised_Gain_Loss,
                                                            ReportDate = endDate,
                                                            TransPR = calcLedger.TransPr != null ? calcLedger.TransPr.Value : 0,
                                                            AverageCost = calcLedger.HLDRAveCost != null ? calcLedger.HLDRAveCost.Value : 0
                                                        };
                                                        objUtmcCompositionalTransactions.Add(utmcCompositionalTransaction);
                                                    }

                                                    //COST_________________________________________________________________
                                                });


                                                UtmcFundInformation uFI = UTMCFundInformations.Where(x => x.IpdFundCode == ledgerIn.FirstOrDefault().FundID).FirstOrDefault();
                                                InvestUnitsIP += ledgerIn.Sum(x => (x.TransUnits != null ? x.TransUnits.Value : 0));
                                                NetInvestmentCost += utmcCompositionalTransaction.CostRm.Value;
                                                GainLoss += utmcCompositionalTransaction.RealisedGainLoss.Value;
                                                TotalUnitsByCurMarketValue += (ledgerIn.Sum(x => ((x.TransUnits != null ? x.TransUnits.Value : 0) * uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)));
                                                if (ledgerIn.FirstOrDefault().TransDt.Value.Year == DateTime.Now.Year)
                                                {
                                                    TotalUnitsByYearMarketValue = TotalUnitsByCurMarketValue;
                                                }
                                                else
                                                {
                                                    TotalUnitsByYearMarketValue += (ledgerIn.Sum(x => ((x.TransUnits != null ? x.TransUnits.Value : 0) * (x.TransPr != null ? x.TransPr.Value : 0))));
                                                }
                                            });

                                            decimal NetInvestmentCostAllFunds = 0;
                                            decimal GainLossAllFunds = 0;
                                            objUtmcCompositionalTransactions.GroupBy(x => x.IpdFundCode).Select(grp => grp.ToList()).ToList().ForEach(x =>
                                            {
                                                NetInvestmentCostAllFunds += x.LastOrDefault().ActualCost;
                                                GainLossAllFunds += x.LastOrDefault().RealisedGainLoss.Value;
                                            });
                                        });
                                        TotalUnitsByYearMarketValueAll += TotalUnitsByYearMarketValue;
                                        holderLedgersFiltered.ForEach(ledger =>
                                        {

                                        });

                                        //NEW


                                        utmcDetailedMemberInvestments.ForEach(x =>
                                                {
                                                    x.utmcCompositionalTransactions = objUtmcCompositionalTransactions.Where(y => y.IpdFundCode == x.utmcFundInformation.IpdFundCode).ToList().OrderByDescending(y => y.DateOfTransaction.Value).ThenByDescending(y => y.IpdUniqueTransactionId).ToList();
                                                });

                                        Decimal actualCost = utmcDetailedMemberInvestments.Sum(x => x.utmcCompositionalTransactions.FirstOrDefault().ActualCost);

                                        Decimal marketValue = utmcDetailedMemberInvestments.Sum(x => (x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice));

                                        UnrealisedGain = marketValue - actualCost;
                                        UnrealisedGainPer = (actualCost == 0 ? 0.00M : ((marketValue - actualCost) / actualCost) * 100);



                                        InvestUnits += utmcDetailedMemberInvestments.Sum(x => x.holderInv.CurrUnitHldg);
                                        InvestMarketValue += marketValue;
                                        InvestCost += actualCost;

                                        RealizedProfit += UnrealisedGain;
                                        RealizedProfitPer += UnrealisedGainPer;

                                        Logger.WriteLog("GetConsolidatedAccountSummary marketValue: " + marketValue);
                                        Logger.WriteLog("GetConsolidatedAccountSummary actualCost: " + actualCost);

                                        Logger.WriteLog("GetConsolidatedAccountSummary UnrealisedGain: " + UnrealisedGain);
                                        Logger.WriteLog("GetConsolidatedAccountSummary UnrealisedGainPer: " + UnrealisedGainPer);

                                        Logger.WriteLog("GetConsolidatedAccountSummary RealizedProfit: " + RealizedProfit);
                                        Logger.WriteLog("GetConsolidatedAccountSummary RealizedProfitPer: " + RealizedProfitPer);

                                        utmcCompositionalTransactions.AddRange(objUtmcCompositionalTransactions);

                                        //-----------------------------------------------------------------------




                                        //Local

                                        string holderClass = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                                        consolidatedTotalActualCost += utmcDetailedMemberInvestments.Where(x => x.holderInv.CurrUnitHldg > 0).Sum(x => (x.holderInv.CumValueHldg));

                                        consolidatedTotalInvestmentRM += utmcDetailedMemberInvestments.Where(x => x.holderInv.CurrUnitHldg > 0).Sum(x => (x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice));
                                        consolidatedTotalInvestmentUNITS += utmcDetailedMemberInvestments.Sum(x => x.holderInv.CurrUnitHldg);

                                        List<UtmcFundInformation> ExistingUTMCFundInformations = UTMCFundInformations.Where(x => utmcDetailedMemberInvestments.Select(y => y.utmcFundInformation.Id).Contains(x.Id)).ToList();
                                        if (utmcDetailedMemberInvestments.Count > 0)
                                        {
                                            asAtDate = utmcDetailedMemberInvestments.FirstOrDefault().utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavDate.Value;
                                        }
                                        foreach (UtmcFundInformation fund in ExistingUTMCFundInformations)
                                        {
                                            FundAllocations.Add(new FundAllocation
                                            {
                                                FundCode = fund.FundCode,
                                                FundName = fund.FundName.Capitalize(),
                                                Investment = utmcDetailedMemberInvestments.Where(x => x.utmcFundInformation.Id == fund.Id).Sum(x => x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)
                                            });
                                        }
                                        PortfolioAllocations.Add(new PortfolioAllocation
                                        {
                                            AccountPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls),
                                            Investment = utmcDetailedMemberInvestments.Sum(x => x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)
                                        });

                                        FCAllocations.Add(new FCAllocation
                                        {
                                            FundCategoryName = "Equity",
                                            Investment = utmcDetailedMemberInvestments.Where(x => x.utmcFundInformation.FundCls == "EE").Sum(x => x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)
                                        });

                                        FCAllocations.Add(new FCAllocation
                                        {
                                            FundCategoryName = "Mixed Asset",
                                            Investment = utmcDetailedMemberInvestments.Where(x => x.utmcFundInformation.FundCls == "MA").Sum(x => x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)
                                        });

                                        FCAllocations.Add(new FCAllocation
                                        {
                                            FundCategoryName = "Money Market",
                                            Investment = utmcDetailedMemberInvestments.Where(x => x.utmcFundInformation.FundCls == "MM").Sum(x => x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)
                                        });

                                        AccountAllocations.Add(new AccountAllocation
                                        {
                                            AccountNo = uA.AccountNo,
                                            AccountPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls),
                                            Investment = utmcDetailedMemberInvestments.Sum(x => x.holderInv.CurrUnitHldg * x.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice)
                                        });
                                    }
                                }
                            });

                            Decimal cug = (InvestMarketValue - InvestCost);
                            Decimal cugPer = InvestCost == 0 ? 0 : ((InvestMarketValue - InvestCost) / InvestCost) * 100;

                            List<PortfolioMarketValueMovement> PortfolioMarketValueMovements = new List<PortfolioMarketValueMovement>();
                            decimal TotalUnitsByCurMarketValueNew = 0;
                            decimal TotalUnitsByYearMarketValueNew = 0;



                            PortfolioAllocations.ForEach(x =>
                            {
                                x.Percent = ((x.Investment) / (PortfolioAllocations.Sum(y => y.Investment))) * 100;
                            });
                            AccountAllocations.ForEach(x =>
                            {
                                x.Percent = ((x.Investment) / (AccountAllocations.Sum(y => y.Investment))) * 100;
                            });
                            FCAllocations.ForEach(x =>
                            {
                                x.Percent = x.Investment != 0 ? ((x.Investment) / (FCAllocations.Sum(y => y.Investment))) * 100 : 0;
                            });

                            PortfolioAllocations = PortfolioAllocations
                                .GroupBy(grp => grp.AccountPlan).Where(g => g.Sum(x => x.Percent) > 0).ToList()
                                .Select(g => new PortfolioAllocation
                                {
                                    AccountPlan = g.FirstOrDefault().AccountPlan,
                                    Investment = g.Sum(x => x.Investment),
                                    Percent = g.Sum(x => x.Percent)
                                }).ToList();
                            AccountAllocations = AccountAllocations
                                .GroupBy(grp => grp.AccountNo).Where(g => g.Sum(x => x.Percent) > 0).ToList()
                                .Select(g => new AccountAllocation
                                {
                                    AccountNo = g.FirstOrDefault().AccountNo,
                                    AccountPlan = g.FirstOrDefault().AccountPlan,
                                    Investment = g.Sum(x => x.Investment),
                                    Percent = g.Sum(x => x.Percent)
                                }).ToList();

                            FCAllocations = FCAllocations
                                .GroupBy(grp => grp.FundCategoryName).Where(g => g.Sum(x => x.Percent) > 0).ToList()
                                .Select(g => new FCAllocation
                                {
                                    FundCategoryName = g.FirstOrDefault().FundCategoryName,
                                    Investment = g.Sum(x => x.Investment),
                                    Percent = g.Sum(x => x.Percent)
                                }).ToList();
                            var groupedFundAllocations = FundAllocations.GroupBy(x => x.FundName.Capitalize()).Select(grp => grp.ToList()).ToList();
                            List<FundAllocation> FundAllocationsNew = new List<FundAllocation>();
                            foreach (List<FundAllocation> fs in groupedFundAllocations)
                            {
                                if (fs.Sum(x => x.Investment) != 0)
                                {
                                    FundAllocationsNew.Add(new FundAllocation
                                    {
                                        FundCode = fs.FirstOrDefault().FundCode,
                                        FundName = fs.FirstOrDefault().FundName.Capitalize(),
                                        Investment = fs.Sum(x => x.Investment),
                                    });
                                }
                            }
                            FundAllocationsNew.ForEach(x =>
                            {
                                x.Percent = (x.Investment / (FundAllocationsNew.Sum(y => y.Investment))) * 100;
                            });
                            consolidatedAccountUnrealisedGainRM = (((consolidatedTotalInvestmentRM - consolidatedTotalActualCost) != 0 ? (consolidatedTotalInvestmentRM - consolidatedTotalActualCost) : 1) / (consolidatedTotalActualCost != 0 ? consolidatedTotalActualCost : 1)) * 100;
                            PortfolioMarketValueMovements = PortfolioMarketValueMovements.OrderBy(x => x.ReportDate).ToList();
                            return new
                            {
                                asAtDate = asAtDate,
                                consolidatedTotalInvestmentRM = consolidatedTotalInvestmentRM,
                                consolidatedTotalInvestmentUNITS = consolidatedTotalInvestmentUNITS,
                                NoOfAccounts = userAccounts.Count,
                                consolidatedAccountUnrealisedGainRM = cug,
                                consolidatedAccountUnrealisedGainRMPer = cugPer,
                                PortfolioMarketValueMovements = PortfolioMarketValueMovements,
                                PortfolioAllocations = PortfolioAllocations,
                                AccountAllocations = AccountAllocations,
                                FundAllocations = FundAllocationsNew,
                                FCAllocations = FCAllocations,
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("GetConsolidatedAccountSummary: " + ex.Message + " - Exception");
                Logger.WriteLog("GetConsolidatedAccountSummary StackTrace: " + ex.StackTrace);
            }
            return null;
        }

        //Google
        protected void btnCode_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            lblInfo.InnerHtml = "";
            lblGoogleAuthenticationInfo.InnerHtml = "";
            Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { Type = 1, authPin = txtPin.Text, Step = 3, Condition1 = true, httpContext = HttpContext.Current });
            try
            {
                if (responseUser.IsSuccess)
                {
                    responseUserStep3 = (UserLoginStep)responseUser.Data;
                    UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep3.Code);
                    string message = userPolicyEnum3.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;
                        RefreshData();
                        hdnIsVerified.Value = "1";
                        Response.Redirect("Portfolio.aspx", false);
                    }
                    else if (message == "EXCEPTION")
                    {

                        responseUser.IsSuccess = false;
                        lblGoogleAuthenticationInfo.InnerHtml = responseUser.Message;
                        hdnIsVerified.Value = "0";
                        hdnOption.Value = "G";
                    }
                    else
                    {
                        if (userPolicyEnum3 == UserPolicyEnum3.UA6)
                        {
                            hdnOption.Value = "G";
                            lblGoogleAuthenticationInfo.InnerHtml = message;

                        }
                        else if (userPolicyEnum3 == UserPolicyEnum3.UA7)
                        {
                            lblGoogleAuthenticationInfo.InnerHtml = message;
                        }
                    }
                }
                else
                {
                    responseUser.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                responseUser.IsSuccess = false;
                responseUser.Message = ex.Message;
                Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PhoneVerify()
        {
            Response responseUser = new Response();
            try
            {
                responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { Step = 2, Condition1 = true, title = "LOGIN", httpContext = HttpContext.Current });
                if (responseUser.IsSuccess)
                {
                    responseUserStep2 = (UserLoginStep)responseUser.Data;
                    UserPolicyEnum2 userPolicyEnum2 = (UserPolicyEnum2)Enum.Parse(typeof(UserPolicyEnum2), responseUserStep2.Code);
                    string message = userPolicyEnum2.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        return responseUser;
                    }
                    else if (message == "EXCEPTION")
                    {
                        responseUser.Message = message;
                        responseUser.IsSuccess = false;
                        return responseUser;
                    }
                    else
                    {
                        responseUser.Message = message;
                        return responseUser;
                    }
                }
                else
                {
                    return responseUser;
                }
            }
            catch (Exception ex)
            {
                responseUser.IsSuccess = false;
                responseUser.Message = ex.Message;
                Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);
                return responseUser;
            }
        }

        private static UserLoginStep responseUserStep3 { get; set; }

        //Mobile
        protected void btnVerifyByMobile_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            hdOtpStatus.Value = "0";
            lblGoogleAuthenticationInfo.InnerHtml = "";
            Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { User = user, Type = 0, authPin = txtMobilePin.Text, Condition1 = true, Step = 3, httpContext = HttpContext.Current });
            try
            {
                if (responseUser.IsSuccess)
                {
                    responseUserStep3 = (UserLoginStep)responseUser.Data;
                    UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep3.Code);
                    string message = userPolicyEnum3.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;
                        hdnIsVerified.Value = "1";
                        Response.Redirect("Portfolio.aspx", false);
                    }
                    else if (message == "EXCEPTION")
                    {
                        responseUser.Message = message;
                        responseUser.IsSuccess = false;
                    }
                    else
                    {
                        responseUser.Message = message;
                        responseUser.IsSuccess = false;
                        hdnIsVerified.Value = "0";
                        hdnOption.Value = "M";
                        lblInfo.InnerHtml = responseUser.Message;
                        if (userPolicyEnum3 == UserPolicyEnum3.UA1)
                        {
                            hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSec).ToString();

                        }
                        else if (userPolicyEnum3 == UserPolicyEnum3.UA2)
                        {
                            hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSec).ToString();
                            btnVerifyByMobile.Enabled = true;
                        }
                        else if (userPolicyEnum3 == UserPolicyEnum3.UA3)
                        {

                        }
                        else if (userPolicyEnum3 == UserPolicyEnum3.UA4)
                        {

                            hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSec).ToString();
                            hdOtpStatus.Value = "1";
                        }
                        else if (userPolicyEnum3 == UserPolicyEnum3.UA5)
                        {
                            requestNewPinLinkDiv.Attributes.Remove("class");
                            requestNewPinLinkDiv.Attributes.Add("class", "mt-10 text-center");
                            hdnMobilePinRequested.Value = "0";
                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    responseUser.IsSuccess = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseUser.Message + "','');", true);
                }


            }
            catch (Exception ex)
            {
                responseUser.IsSuccess = false;
                responseUser.Message = ex.Message;
                Logger.WriteLog("index Page VerifyLogin " + ex.Message);
            }
        }

        private static UserLoginStep responseUserStep2 { get; set; }


        protected void btnMAActivation_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            if (hdnIsMAActive.Value == "0") //Means MA inactive (Single MA)
            {
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> uAs = (List<UserAccount>)responseUAList.Data;
                    Int32 VerificationCode = uAs.FirstOrDefault().VerificationCode.Value;
                    Int32 VerificationExpiry = uAs.FirstOrDefault().VerifyExpired;
                    Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { User = user, authPin = txtMAActivationPin.Text, Step = 3, Condition1 = false, additionalAccNo = hdnIsMAActive.Value, httpContext = HttpContext.Current });
                    try
                    {
                        if (responseUser.IsSuccess)
                        {
                            responseUserStep3 = (UserLoginStep)responseUser.Data;
                            UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep3.Code);
                            string message = userPolicyEnum3.ToDescriptionString();
                            if (message == "SUCCESS")
                            {
                                hdnMobilePinRequested.Value = "0";
                                hdnIsMAActive.Value = "1";
                                Session["isVerified"] = "1";
                                string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                Email email = new Email
                                {
                                    user = user,
                                    link = siteURL + "Index.aspx"
                                };
                                EmailService.SendActivationMail(email);
                                Response.Redirect("Portfolio.aspx", false);
                            }
                            else if (message == "EXCEPTION")
                            {
                                responseUser.Message = message;
                                responseUser.IsSuccess = false;
                            }
                            else
                            {
                                lblInfoMAActivation.InnerHtml = responseUser.Message;
                                hdnIsMAActive.Value = "0";

                                if (userPolicyEnum3 == UserPolicyEnum3.UA1)
                                {
                                    hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSecMA).ToString();
                                    lblInfoMAActivation.InnerHtml = message;
                                    txtMAActivationPin.ReadOnly = false;
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA2)
                                {
                                    hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSecMA).ToString();
                                    btnRequestMAActivation.Disabled = true;
                                    hdnIsMAActive.Value = "0";
                                    lblInfoMAActivation.InnerHtml = message;
                                    btnRequestMAActivation.Disabled = true;
                                    //Used to invalid pin
                                    hdnMobilePinRequested.Value = "2";
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA3)
                                {
                                    btnRequestMAActivation.Disabled = false;
                                    lblInfoMAActivation.InnerHtml = message;
                                    hdnIsMAActive.Value = "0";
                                    hdnMobilePinRequested.Value = "0";
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA4)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSecMA).ToString();
                                    btnRequestMAActivation.Disabled = true;

                                    //Used to invalid pin
                                    hdnMobilePinRequested.Value = "2";


                                    hdnIsMAActive.Value = "0";

                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA5)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    btnRequestMAActivation.Disabled = false;
                                    hdnIsMAActive.Value = "0";
                                    hdnMobilePinRequested.Value = "0";
                                }

                                responseUser.Message = message;
                                responseUser.IsSuccess = false;
                            }
                        }
                        else
                        {
                            responseUser.IsSuccess = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        responseUser.IsSuccess = false;
                        responseUser.Message = ex.Message;
                        Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);
                    }
                    if (txtMAActivationPin.Text == "" || txtMAActivationPin.Text == null)
                    {
                        lblInfoMAActivation.InnerHtml = "Please Enter OTP.";
                        hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSecMA).ToString();
                        txtMAActivationPin.ReadOnly = false;
                    }
                    else
                    {
                        if (VerificationCode != 0)
                        {
                            if (VerificationExpiry == 0)
                            {
                                if (Int32.TryParse(txtMAActivationPin.Text.Trim(), out int result))
                                {
                                    Int32 verPin = Convert.ToInt32(txtMAActivationPin.Text.TrimStart(new Char[] { '0' }) == "" ? "0" : txtMAActivationPin.Text.TrimStart(new Char[] { '0' }));
                                    if (verPin == VerificationCode)
                                    {

                                        uAs.ForEach(uA =>
                                        {
                                            uA.IsVerified = 1;
                                            uA.VerificationCode = 0;
                                            uA.VerifyExpired = 1;
                                            IUserAccountService.UpdateData(uA);

                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                Description = "MA (" + uA.AccountNo + ") Activation successful - Mobile.",
                                                TableName = "user_accounts",
                                                UpdatedDate = DateTime.Now,
                                                UserId = user.Id,
                                                UserAccountId = uA.Id,
                                                RefId = uA.Id,
                                                RefValue = uA.AccountNo,
                                                StatusType = 1
                                            };
                                            Response responseLog = IUserLogMainService.PostData(ulm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                            }



                                        });

                                        hdnMobilePinRequested.Value = "0";
                                        hdnIsMAActive.Value = "1";
                                        UserSecurity MobileSecurity = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 2).FirstOrDefault();
                                        MobileSecurity.IsVerified = 1;
                                        MobileSecurity.VerifiedDate = DateTime.Now;
                                        MobileSecurity.VerificationPin = VerificationCode;
                                        IUserSecurityService.UpdateData(MobileSecurity);

                                        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                        if (responseUSList.IsSuccess)
                                        {
                                            List<UserSecurity> uSs = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId != 2).ToList();
                                            uSs.Add(MobileSecurity);
                                            user.UserIdUserSecurities = uSs;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                                        }
                                        Session["isVerified"] = "1";
                                        Response responseU = IUserService.GetSingle(user.Id);
                                        if (responseU.IsSuccess)
                                        {
                                            User u = (User)responseU.Data;
                                            u.HardcopyUpdatedDate = DateTime.Now;
                                            u.ActivationDate = DateTime.Now;
                                            IUserService.UpdateData(u);
                                        }

                                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                        Email email = new Email
                                        {
                                            user = user,
                                            link = siteURL + "Index.aspx"
                                        };
                                        EmailService.SendActivationMail(email);
                                        Response.Redirect("Portfolio.aspx", false);
                                    }
                                    else
                                    {
                                        UserLogMain ulm = new UserLogMain()
                                        {
                                            Description = "MA Activation failed - Mobile.",
                                            TableName = "users",
                                            UpdatedDate = DateTime.Now,
                                            UserId = user.Id,
                                            UserAccountId = 0,
                                            RefId = uAs.FirstOrDefault(x => x.IsVerified == 0).Id,
                                            RefValue = uAs.FirstOrDefault(x => x.IsVerified == 0).AccountNo,
                                            StatusType = 1
                                        };
                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                        if (!responseLog.IsSuccess)
                                        {
                                            //Audit log failed
                                        }
                                        lblInfoMAActivation.InnerHtml = "Wrong OTP. Please Enter Again.";
                                        hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSecMA).ToString();
                                        btnRequestMAActivation.Disabled = true;
                                        hdnIsMAActive.Value = "0";
                                        //Used to invalid pin
                                        hdnMobilePinRequested.Value = "2";
                                    }
                                }
                                else
                                {
                                    lblInfoMAActivation.InnerHtml = "OTP must be Numeric.";
                                    hdnSMSExpirationTimeInSeconds.Value = (SMSExpirationTimeInSeconds - currentSecMA).ToString();
                                    btnRequestMAActivation.Disabled = true;
                                    hdnIsMAActive.Value = "0";
                                    //Used to invalid pin
                                    hdnMobilePinRequested.Value = "2";
                                }
                            }
                            else
                            {
                                lblInfoMAActivation.InnerHtml = "OTP Expired. Please Request Again.";
                                hdnIsMAActive.Value = "0";
                                hdnMobilePinRequested.Value = "0";
                            }
                        }
                        else
                        {
                            lblInfoMAActivation.InnerHtml = "Please Request OTP.";
                            hdnIsMAActive.Value = "0";
                            hdnMobilePinRequested.Value = "0";
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                }
            }
            else // Additional MA Activation
            {
                String additionalAccNo = hdnMAToBeActivated.Value;
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                    UserAccount uA = userAccounts.Where(x => x.AccountNo == additionalAccNo).FirstOrDefault();
                    Int32 VerificationExpiry = uA.VerifyExpired;
                    Int32 VerificationCode = uA.VerificationCode.Value;
                    Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { authPin = txtMAActivationPin.Text, Step = 3, Condition1 = false, additionalAccNo = hdnMAToBeActivated.Value, httpContext = HttpContext.Current });
                    try
                    {
                        if (responseUser.IsSuccess)
                        {
                            responseUserStep3 = (UserLoginStep)responseUser.Data;
                            UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep3.Code);
                            string message = userPolicyEnum3.ToDescriptionString();
                            if (message == "SUCCESS")
                            {
                                hdnMobilePinRequested.Value = "0";
                                hdnIsMAActive.Value = "1";
                                Session["isVerified"] = "1";
                                string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                Email email = new Email
                                {
                                    user = user,
                                    link = siteURL + "Index.aspx"
                                };
                                EmailService.SendActivationMail(email);
                                Response.Redirect("Portfolio.aspx", false);
                            }
                            else if (message == "EXCEPTION")
                            {
                                responseUser.Message = message;
                                responseUser.IsSuccess = false;

                            }
                            else
                            {
                                lblInfoMAActivation.InnerHtml = responseUser.Message;
                                hdnIsMAActive.Value = "0";

                                if (userPolicyEnum3 == UserPolicyEnum3.UA1)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    txtMAActivationPin.ReadOnly = false;
                                    //Used to invalid pin
                                    hdnMobilePinRequested.Value = "2";
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA2)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    btnRequestMAActivation.Disabled = true;
                                    hdnIsMAActive.Value = "0";
                                    //Used to invalid pin
                                    hdnMobilePinRequested.Value = "2";
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA3)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    btnRequestMAActivation.Disabled = false;
                                    hdnIsMAActive.Value = "0";
                                    hdnMobilePinRequested.Value = "0";
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA4)
                                {
                                    btnRequestMAActivation.Disabled = true;
                                    hdnIsMAActive.Value = "0";
                                    hdnMobilePinRequested.Value = "2";
                                    lblInfoMAActivation.InnerHtml = message;

                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA5)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    hdnIsMAActive.Value = "0";
                                    btnRequestMAActivation.Disabled = false;
                                    hdnMobilePinRequested.Value = "0";
                                }
                                else if (userPolicyEnum3 == UserPolicyEnum3.UA8)
                                {
                                    lblInfoMAActivation.InnerHtml = message;
                                    btnRequestMAActivation.Disabled = false;
                                    hdnMobilePinRequested.Value = "0";
                                }

                                responseUser.Message = message;
                                responseUser.IsSuccess = false;

                            }
                        }
                        else
                        {
                            responseUser.IsSuccess = false;

                        }
                    }
                    catch (Exception ex)
                    {
                        responseUser.IsSuccess = false;
                        responseUser.Message = ex.Message;
                        Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);

                    }
                    if (txtMAActivationPin.Text == "" || txtMAActivationPin.Text == null)
                    {
                        lblInfoMAActivation.InnerHtml = "Please Enter OTP.";
                        txtMAActivationPin.ReadOnly = false;
                    }
                    else
                    {
                        if (VerificationCode != 0)
                        {
                            if (VerificationExpiry == 0)
                            {
                                if (Int32.TryParse(txtMAActivationPin.Text.Trim(), out int result))
                                {
                                    if (uA.IsVerified == 0)
                                    {
                                        Int32 verPin = Convert.ToInt32(txtMAActivationPin.Text.TrimStart(new Char[] { '0' }) == "" ? "0" : txtMAActivationPin.Text.TrimStart(new Char[] { '0' }));
                                        if (VerificationCode == verPin)
                                        {
                                            uA.IsVerified = 1;
                                            uA.VerificationCode = 0;
                                            IUserAccountService.UpdateData(uA);
                                            List<UserAccount> uAs = userAccounts.Where(x => x.AccountNo != additionalAccNo).ToList();
                                            uAs.Add(uA);
                                            user.UserIdUserAccounts = uAs;

                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                Description = "Additional MA (" + uA.AccountNo + ") Activation successful - Mobile.",
                                                TableName = "user_accounts",
                                                UpdatedDate = DateTime.Now,
                                                UserId = user.Id,
                                                UserAccountId = uA.Id,
                                                RefId = uA.Id,
                                                RefValue = uA.AccountNo,
                                                StatusType = 1
                                            };
                                            Response responseLog = IUserLogMainService.PostData(ulm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                            }

                                            Session["user"] = user;
                                            RefreshData();
                                        }
                                        else
                                        {
                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                Description = "Additional MA (" + uA.AccountNo + ") Activation failed - Mobile.",
                                                TableName = "user_accounts",
                                                UpdatedDate = DateTime.Now,
                                                UserId = user.Id,
                                                UserAccountId = uA.Id,
                                                RefId = uA.Id,
                                                RefValue = uA.AccountNo,
                                                StatusType = 0
                                            };
                                            Response responseLog = IUserLogMainService.PostData(ulm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                            }

                                            lblInfoMAActivation.InnerHtml = "Wrong OTP. Please Enter Again.";
                                            btnRequestMAActivation.Disabled = true;
                                            hdnIsMAActive.Value = "0";
                                            hdnMobilePinRequested.Value = "2";
                                        }
                                    }
                                    else
                                    {
                                        lblInfoMAActivation.InnerHtml = "MasterAccount is verified.";
                                        btnRequestMAActivation.Disabled = true;
                                        hdnIsMAActive.Value = "0";
                                    }
                                }
                                else
                                {
                                    lblInfoMAActivation.InnerHtml = "OTP must be Numeric.";
                                    btnRequestMAActivation.Disabled = true;
                                    hdnIsMAActive.Value = "0";
                                    hdnMobilePinRequested.Value = "2";
                                }
                            }
                            else
                            {
                                lblInfoMAActivation.InnerHtml = "OTP Expired. Please Request Again.";
                                btnRequestMAActivation.Disabled = false;
                                hdnIsMAActive.Value = "0";
                                hdnMobilePinRequested.Value = "0";
                            }
                        }
                        else
                        {
                            lblInfoMAActivation.InnerHtml = "Please Request OTP.";
                            btnRequestMAActivation.Disabled = false;
                            hdnIsMAActive.Value = "0";
                            hdnMobilePinRequested.Value = "0";
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                }
            }

        }

        public static string CheckAndRequestPin(String mobileNo)
        {
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    user = (User)responseUList.Data;
                    if (user != null)
                    {
                        if (user.IsSmsLocked == 1)
                        {
                            Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                            PinTimerThread.Start();
                            returnString = "sms locked";
                        }
                        else if (user.VerifyExpired == 1)
                        {
                            if (user.SmsAttempts >= 3)
                            {
                                user.IsSmsLocked = 1;
                                user.VerifyExpired = 1;
                                Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                                PinTimerThread.Start();
                                returnString = "sms locked";
                            }
                            else
                            {
                                //String toMobileNumber = user.MobileNumber;
                                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                String result = "";
                                String title = "LOGIN";
                                result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                Int32 length = mobileNo.Length;
                                String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);
                                user.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                user.VerifyExpired = 0;
                                user.SmsAttempts = user.SmsAttempts + 1;

                                Thread PinTimerThread = new Thread(() => PinTimer(user));
                                PinTimerThread.Start();
                                returnString = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                            }
                            IUserService.UpdateData(user);
                            return returnString;
                        }
                        else
                        {
                            Thread PinTimerThread = new Thread(() => PinTimer(user));
                            PinTimerThread.Start();
                            returnString = "already sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                        }
                    }
                    else
                        returnString = "no account";
                }
                //lblInfo.InnerHtml = "No Account Registered with this Mobile Number.";
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnFuAddressUpload_Click: " + ex.Message);
            }
            return returnString;
        }

        public static Int32 currentSec = 0;
        public static void PinTimer(User user)
        {
            try
            {

                while (currentSec <= SMSExpirationTimeInSeconds)
                {
                    if (currentSec == SMSExpirationTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.VerifyExpired = 1;
                        IUserService.UpdateData(user);
                        currentSec = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSec++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page PinTimer: " + ex.Message);
            }
        }

        public static Int32 currentSecLock = 0;
        public static void PinTimerForSMSLock(User user)
        {
            try
            {
                //User user = (User)HttpContext.Current.Session["user"];
                //User user = sessionUser;

                while (currentSecLock <= SMSLockTimeInSeconds)
                {
                    if (currentSecLock == SMSLockTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.SmsAttempts = 0;
                        user.IsSmsLocked = 0;
                        IUserService.UpdateData(user);
                        currentSecLock = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecLock++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page PinTimerForSMSLock: " + ex.Message);
            }
        }

        protected void btnSubmitChangePw_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdbChangePw.Checked)
                {
                    var rule1 = txtEOPNewPw.Text.Any(char.IsLetter);
                    var rule2 = txtEOPNewPw.Text.Any(char.IsNumber);
                    int rule3;
                    if (txtEOPNewPw.Text.Length >= 8 && txtEOPNewPw.Text.Length <= 16)
                        rule3 = 1;
                    else
                        rule3 = 0;

                    User user = (User)Session["user"];

                    if (txtEOPExisting1.Text != "" && txtEOPNewPw.Text != "" && txtEOPCfNewPw.Text != "")
                    {
                        string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(txtEOPExisting1.Text);
                        if (user.Password == encryptedPassword)
                        {
                            if (txtEOPNewPw.Text == txtEOPCfNewPw.Text)
                            {
                                if (rule1 == true && rule2 == true && rule3 == 1)
                                {
                                    string encryptedNewPassword = CustomEncryptorDecryptor.EncryptPassword(txtEOPNewPw.Text);
                                    Response response = IUserService.GetSingle(user.Id);
                                    if (response.IsSuccess)
                                    {
                                        User newUser = (User)response.Data;
                                        newUser.Password = encryptedNewPassword;
                                        newUser.ModifiedDate = DateTime.Now;
                                        Response response2 = IUserService.UpdateData(newUser);
                                        newUser = (User)response2.Data;
                                        Session["user"] = newUser;

                                        UserLogMain ulm = new UserLogMain()
                                        {
                                            Description = "Change Password successful",
                                            TableName = "users",
                                            UpdatedDate = DateTime.Now,
                                            UserId = user.Id,
                                            UserAccountId = 0,
                                            RefId = user.Id,
                                            RefValue = user.Username,
                                            StatusType = 1
                                        };
                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                        if (!responseLog.IsSuccess)
                                        {
                                            //Audit log failed
                                        }

                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('Successfully updated Password.');", true);
                                        Response.Redirect(Page.Request.Url.ToString(), false);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                    }
                                }
                                else
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Password format invalid.', '');", true);
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Password doesn't match.', '');", true);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Wrong password.', '');", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Please enter required fields.', '');", true);

                }
                else if (rdbKeepPw.Checked == true)
                {
                    User user = (User)Session["user"];
                    if (txtEOPExisting2.Text != "" && txtEOPCfExisting.Text != "")
                    {
                        string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(txtEOPExisting2.Text);
                        if (user.Password == encryptedPassword)
                        {
                            if (txtEOPExisting2.Text == txtEOPCfExisting.Text)
                            {
                                //string encryptedNewPassword = CustomEncryptorDecryptor.EncryptPassword(txtEOPExisting2.Text);
                                Response response = IUserService.GetSingle(user.Id);
                                if (response.IsSuccess)
                                {
                                    User newUser = (User)response.Data;
                                    //newUser.Password = encryptedNewPassword;
                                    newUser.ModifiedDate = DateTime.Now;
                                    Response response2 = IUserService.UpdateData(newUser);
                                    newUser = (User)response2.Data;
                                    Session["user"] = newUser;

                                    UserLogMain ulm = new UserLogMain()
                                    {
                                        Description = "Maintaining Password successful",
                                        TableName = "users",
                                        UpdatedDate = DateTime.Now,
                                        UserId = user.Id,
                                        UserAccountId = 0,
                                        RefId = user.Id,
                                        RefValue = user.Username,
                                        StatusType = 1
                                    };
                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('Successfully maintaining Password.');", true);
                                    Response.Redirect(Page.Request.Url.ToString(), false);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                }
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('modalExpiryOfPassword');ShowCustomMessage('Alert', 'Password doesn't match.', '');", true);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('modalExpiryOfPassword');ShowCustomMessage('Alert', 'Wrong password.', '');", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('modalExpiryOfPassword');ShowCustomMessage('Alert', 'Please enter required fields.', '');", true);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page btnSubmit_Click: " + ex.Message);
            }

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Index.aspx", false);
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            User user = (User)HttpContext.Current.Session["user"];
            Boolean IsImpersonated = false;
            if (HttpContext.Current.Session["user"] != null)
            {
                IsImpersonated = ServicesManager.IsImpersonated(Context);
            }
            if (IsImpersonated)
            {
                Session.Clear();
                Response.Write("<script>window.close();</script>");
            }
            else
            {
                user = (User)Session["user"];
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> uAs = (List<UserAccount>)responseUAList.Data;
                    UserAccount uA = ((List<UserAccount>)responseUAList.Data).Where(x => x.IsPrimary == 1).FirstOrDefault();
                    if (uA.IsVerified == 0)
                    {
                        foreach (UserAccount ua in uAs)
                        {
                            ua.VerificationCode = 0;
                            ua.VerifyExpired = 1;
                        }
                        IUserAccountService.UpdateBulkData(uAs);
                    }
                }
                Session.Clear();
                Session.Abandon();
                Response.Redirect("Index.aspx", false);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PhoneVerifyMAActivation()
        {
            User user = (User)HttpContext.Current.Session["user"];
            Response responseUAList = IUserAccountService.GetDataByFilter("user_id = '" + user.Id + "' and is_primary = 1 and status=1 ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                UserAccount ua = ((List<UserAccount>)responseUAList.Data).ToList().FirstOrDefault();
                Response responseMAHolderRegList = ServicesManager.GetMaHolderRegByAccountNo(ua.AccountNo);
                if (responseMAHolderRegList.IsSuccess)
                {
                    MaHolderReg maHolderReg = (MaHolderReg)responseMAHolderRegList.Data;
                    string toMobileNumber = maHolderReg.HandPhoneNo;
                    if (ua.IsPrinciple == 0)
                        toMobileNumber = maHolderReg.JointTelNo;
                    return CheckMAActivation(toMobileNumber);
                }
                else
                {
                    return responseMAHolderRegList.Message;
                }
            }
            else
            {
                return responseUAList.Message;
            }
        }

        public static string CheckMAActivation(String mobileNo)
        {
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    user = (User)responseUList.Data;
                    if (user != null)
                    {
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        {
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> uas = (List<UserAccount>)responseUAList.Data;
                                UserAccount ua1 = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();

                                if (ua1.VerifyExpired == 1)
                                {
                                    //String toMobileNumber = user.MobileNumber;
                                    String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                    String result = "";
                                    String title = "MA ACTIVATION";
                                    result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                    Int32 length = mobileNo.Length;
                                    String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);

                                    foreach (UserAccount ua in uas)
                                    {
                                        ua.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                        ua.VerifyExpired = 0;

                                        IUserAccountService.UpdateData(ua);
                                    }

                                    Thread PinTimerThread = new Thread(() => PinTimerMA(user));
                                    PinTimerThread.Start();
                                    returnString = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);

                                    IUserService.UpdateData(user);

                                    UserLogMain ulm = new UserLogMain()
                                    {
                                        Description = "Request OTP for MA Activation successful",
                                        TableName = "users",
                                        UpdatedDate = DateTime.Now,
                                        UserId = user.Id,
                                        UserAccountId = 0,
                                        RefId = user.Id,
                                        RefValue = user.Username,
                                        StatusType = 1
                                    };
                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }


                                    return returnString;
                                }
                                else
                                {
                                    Thread PinTimerThread = new Thread(() => PinTimerMA(user));
                                    PinTimerThread.Start();

                                    returnString = "already sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                                }
                            }
                        }
                    }
                    else
                        returnString = "no account";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnFuAddressUpload_Click: " + ex.Message);
            }
            return returnString;
        }

        public static Int32 currentSecMA = 0;
        public static void PinTimerMA(User user)
        {
            try
            {
                //User user = sessionUser;

                while (currentSecMA <= SMSExpirationTimeInSeconds)
                {
                    if (currentSecMA == SMSExpirationTimeInSeconds)
                    {
                        Response responseUserAccsList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        List<UserAccount> userAccs = (List<UserAccount>)responseUserAccsList.Data;
                        foreach (UserAccount ua in userAccs)
                        {

                            ua.VerifyExpired = 1;
                            IUserAccountService.UpdateData(ua);
                        }
                        currentSecMA = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecMA++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page PinTimer: " + ex.Message);
            }
        }

        public static decimal Get_Redumption_Cost_UTMC10(string Holder_No, string Fund_ID, string Sort_Seq, DateTime End_date, List<HolderLedger> holderLedgersFiltered)
        {
            try
            {
                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                {
                    {
                        int Count = 0; string[] numb;
                        {
                            List<HolderLedger> holderLedgerIn = holderLedgersFiltered.Where(x => x.FundID == Fund_ID && x.HolderNo.ToString() == Holder_No && x.TransDt <= End_date).OrderBy(x => x.TransDt).ThenBy(x => x.SortSEQ).ThenByDescending(x => x.TransType).ToList();

                            numb = new string[holderLedgerIn.Count];
                            holderLedgerIn.ForEach(hl =>
                            {
                                numb[Count++] = hl.TransNO;
                            });
                            Array.Resize(ref numb, Count);
                            Count = 0;
                            string Trans_String;
                            foreach (var hl in holderLedgerIn)
                            {
                                try
                                {
                                    Trans_String = hl.TransNO;
                                    bool TTT = false;
                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            TTT = true;
                                            break;
                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = hl.TransUnits != null ? hl.TransUnits.Value : 0;
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = hl.TransType.Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = hl.TransNO;
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= (hl.TransAmt != null ? hl.TransAmt.Value : 0);
                                                Book_value -= ((hl.TransUnits != null ? hl.TransUnits.Value : 0) * (hl.TransPr != null ? hl.TransPr.Value : 0));
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }
                                            else
                                            {
                                                Actual_transfer_value += (hl.TransAmt != null ? hl.TransAmt.Value : 0);
                                                Book_value += ((hl.TransUnits != null ? hl.TransUnits.Value : 0) * (hl.TransPr != null ? hl.TransPr.Value : 0));
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {



                                            Total_Redemption_cost = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = hl.TransUnits != null ? hl.TransUnits.Value : 0;
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            UnitCostRDfinal = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost
                                                                * UnitCostRD);


                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 4);


                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {



                                                Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                           * Total_Redemption_cost);
                                                Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                          * Total_Redemption_cost);



                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                Unit_Holding_value = 0.0000M;
                                            }

                                            Unit_Holding_value = hl.CurUnitHLDG != null ? hl.CurUnitHLDG.Value : 0;
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if (Sort_Seq == hl.SortSEQ.ToString())
                                            {
                                                break;
                                            }

                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            return UnitCostRDfinal;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                return 0.00M;

            }
        }

    }
}