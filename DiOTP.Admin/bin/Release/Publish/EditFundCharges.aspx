﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditFundCharges.aspx.cs" Inherits="Admin.EditFundCharges" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label>Discounted Initial Sales Charge</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundDiscountedInitialSalesCharge" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Annual Management Charge</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundAnnualManagementCharge" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Trustee Fee</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundTrusteeFee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Switching Fee</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundSwitchingFee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label>Redemption Fee</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundRedemptionFee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Transfer Fee</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundTransferFee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Other Significant Fees</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundOtherSignificantFee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>GST Fee</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="txtFundGSTFee" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
