﻿using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.OracleDTOs;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiOTP.WebApp
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    homeLink.HRef = "/Portfolio.aspx";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            
            String email = txtEmail.Text;

            String name = txtName.Text;
            bool isSent = EmailService.SendContactUs(email, name);
            if (!isSent)
            {
                Logger.WriteLog("ContactUs Email user notify failed: " + email + " - Error");

            }
            String message = txtDescription.Text;
            isSent = EmailService.SendEnquiry(email, name, message);
            if (!isSent)
            {
                Logger.WriteLog("ContactUs Email user notify failed: " + email + " - Error");

            }
        }
    }
}