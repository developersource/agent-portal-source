﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Google.Authenticator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiOTP.PolicyAndRules;
using static DiOTP.PolicyAndRules.UserPolicy;

namespace DiOTP.WebApp
{
    public partial class UserSettings : System.Web.UI.Page
    {
        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());

        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());

        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegServiceObj.Value; } }

        private static readonly Lazy<IMaHolderClassService> lazyMaHolderClassServiceObj = new Lazy<IMaHolderClassService>(() => new MaHolderClassService());

        public static IMaHolderClassService IMaHolderClassService { get { return lazyMaHolderClassServiceObj.Value; } }

        private static readonly Lazy<INotificationCategoriesDefService> lazyNotificationCategoriesDefServiceObj = new Lazy<INotificationCategoriesDefService>(() => new NotificationCategoriesDefService());

        public static INotificationCategoriesDefService INotificationCategoriesDefService { get { return lazyNotificationCategoriesDefServiceObj.Value; } }

        private static readonly Lazy<INotificationTypesDefService> lazyNotificationTypesDefServiceObj = new Lazy<INotificationTypesDefService>(() => new NotificationTypesDefService());

        public static INotificationTypesDefService INotificationTypesDefService { get { return lazyNotificationTypesDefServiceObj.Value; } }

        private static readonly Lazy<IUserNotificationSettingService> lazyUserNotificationSettingServiceObj = new Lazy<IUserNotificationSettingService>(() => new UserNotificationSettingService());

        public static IUserNotificationSettingService IUserNotificationSettingService { get { return lazyUserNotificationSettingServiceObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        public static IOccupationCodesDefService IOccupationCodesDefService { get { return lazyOccupationCodesDefServiceObj.Value; } }

        private static readonly Lazy<IRaceDefService> lazyIRaceDefServiceObj = new Lazy<IRaceDefService>(() => new RaceDefService());

        public static IRaceDefService IRaceDefService { get { return lazyIRaceDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());

        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        private static readonly Lazy<IUserLogService> lazyIUserLogServiceObj = new Lazy<IUserLogService>(() => new UserLogService());

        public static IUserLogService IUserLogService { get { return lazyIUserLogServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());

        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserLogSubService> lazyIUserLogSuberviceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());

        public static IUserLogSubService IUserLogSubService { get { return lazyIUserLogSuberviceObj.Value; } }

        private static readonly Lazy<IUserFileService> lazyIUserFileServiceObj = new Lazy<IUserFileService>(() => new UserFileService());

        public static IUserFileService IUserFileService { get { return lazyIUserFileServiceObj.Value; } }

        private static readonly Lazy<IUserLoginHistoryService> lazyUserLoginHistoryServiceObj = new Lazy<IUserLoginHistoryService>(() => new UserLoginHistoryService());

        public static IUserLoginHistoryService IUserLoginHistoryService { get { return lazyUserLoginHistoryServiceObj.Value; } }

        private static readonly Lazy<IUserDetailService> lazyUserDetailServiceObj = new Lazy<IUserDetailService>(() => new UserDetailService());

        public static IUserDetailService IUserDetailService { get { return lazyUserDetailServiceObj.Value; } }


        public static Int32 SMSExpirationTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeInSeconds"]);

        public static Int32 SMSLockTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSLockTimeInSeconds"]);

        public static Response responseMA = new Response();

        public static UserAccount userAccount = new UserAccount();

        public static List<UserAccount> userAccounts = new List<UserAccount>();
        public static List<BanksDef> bankdefs = new List<BanksDef>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null && Session["isVerified"] != null)
                {
                    MaHolderReg maHolderReg = new MaHolderReg();
                    User sessionUser = (User)Session["user"];

                    if (sessionUser.UserIdUserAccounts.Count == 1 && sessionUser.UserIdUserAccounts.FirstOrDefault().IsPrinciple == 0)
                    {
                        transactionPasswordDiv.Visible = false;
                        hardcopyDiv.Visible = false;
                        bankDiv.Visible = false;
                    }

                    if (!IsPostBack)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["isPopup"]))
                        {
                            if (Request.QueryString["isPopup"].ToString() == "1")
                            {
                                if (!string.IsNullOrEmpty(Request.QueryString["Popup"]))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup(" + Request.QueryString["Popup"].ToString() + ");", true);
                                }
                            }
                        }

                        responseMA = ServicesManager.GetMaHolderRegByAccountNo(sessionUser.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
                        if (responseMA.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMA.Data;
                            BindSettings(maHolderReg);
                            bindBankDetails(sessionUser, maHolderReg);
                        }
                    }
                    hdnSMSExpirationTimeInSeconds.Value = SMSExpirationTimeInSeconds.ToString();
                    hdnSMSLockTimeInSeconds.Value = SMSLockTimeInSeconds.ToString();
                    if (sessionUser.UserIdUserAccounts.Count == 1 && sessionUser.UserIdUserAccounts.FirstOrDefault(x => x.HolderClass == "CORP") != null)
                    {
                        bankDiv.Visible = false;
                    }
                    else if (sessionUser.UserIdUserAccounts.Count == 1 && sessionUser.UserIdUserAccounts.FirstOrDefault(x => x.HolderClass == "EPF") != null)
                    {
                        bankDiv.Visible = false;
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }

        }

        public void BindSettings(MaHolderReg maHolderReg)
        {
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                Response response = IUserService.GetSingle(user.Id);
                if (response.IsSuccess)
                {
                    user = (User)response.Data;
                }
                if (user.IsHardCopy == 0)
                {
                    hardcopyActivation.Visible = true;
                    hardcopyDeactivation.Visible = false;
                }
                else
                {
                    hardcopyActivation.Visible = false;
                    hardcopyDeactivation.Visible = true;
                }

                profileUsername.InnerText = user.Username;
                profileEmailId.InnerText = user.EmailId;
                profileContactNumber.InnerText = user.MobileNumber;

                Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                if (responseUSList.IsSuccess)
                {
                    List<UserSecurity> userSecurities = (List<UserSecurity>)responseUSList.Data;
                    UserSecurity emailSec = userSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                    if (emailSec.IsVerified == 1)
                    {
                        String emailId = user.EmailId;
                        emailBindStatus.InnerHtml = "Binded";
                        emailBindStatus.Attributes.Add("class", "text-success");
                        int totalLength = emailId.Split('@')[0].Length;
                        int displayLength = Convert.ToInt32(totalLength * 0.4);
                        emailIdBindedHidden.InnerHtml = emailId.Split('@')[0].Substring(0, displayLength) + "*********" + "@" + emailId.Split('@')[1];
                        emailIdBinded.InnerHtml = user.EmailId;
                        emailIdBindedDate.InnerHtml = emailSec.CreatedDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        emailBindStatus.InnerHtml = "Not Binded";
                        emailBindStatus.Attributes.Add("class", "text-danger");
                        emailIdBindedHidden.InnerHtml = "";
                        emailIdBinded.InnerHtml = "";
                        emailIdBindedDate.InnerHtml = "";
                    }

                    UserSecurity mobileSec = userSecurities.Where(x => x.UserSecurityTypeId == 2).FirstOrDefault();
                    if (mobileSec.IsVerified == 1)
                    {
                        mobileNoBindStatus.InnerHtml = "Binded";
                        mobileNoBindStatus.Attributes.Add("class", "text-success");
                        mobileNoBindedHidden.InnerHtml = "*******" + user.MobileNumber.Substring(user.MobileNumber.Length - 3);
                        mobileNoBinded.InnerHtml = user.MobileNumber;
                        mobileNoBindedDate.InnerHtml = mobileSec.CreatedDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        mobileNoBindStatus.InnerHtml = "Not Binded";
                        mobileNoBindStatus.Attributes.Add("class", "text-danger");
                        mobileNoBindedHidden.InnerHtml = "";
                        mobileNoBinded.InnerHtml = "";
                        mobileNoBindedDate.InnerHtml = "";
                    }

                    UserSecurity googleSec = userSecurities.Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                    if (googleSec != null)
                    {
                        if (googleSec.IsVerified == 1)
                        {
                            googleAuthBindStatus.InnerHtml = "Binded";
                            googleAuthBindStatus.Attributes.Add("class", "text-success");
                            googleAuthBindedDate.InnerHtml = googleSec.VerifiedDate.Value.ToString("dd/MM/yyyy");
                            googleAuthRebindButton.Visible = true;
                            googleAuthBindCode.Visible = false;
                        }
                        else
                        {
                            googleAuthBindStatus.InnerHtml = "Not Binded";
                            googleAuthBindStatus.Attributes.Add("class", "text-danger");
                            googleAuthBindedDate.InnerHtml = " - <span class='text-danger'>Not Binded</span>";
                            imgQRCode.ImageUrl = googleSec.Url;
                            lblManualEntryKey.Text = googleSec.Value;
                            googleAuthRebindButton.Visible = false;
                            googleAuthBindCode.Visible = true;
                        }
                    }
                    else
                    {
                        TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                        tfa.DefaultClockDriftTolerance = TimeSpan.FromMinutes(2);
                        var setupInfo = tfa.GenerateSetupCode("eApexIs", user.EmailId, user.UniqueKey, 150, 150);
                        UserSecurity userSecurity = new UserSecurity
                        {
                            UserId = user.Id,
                            UserSecurityTypeId = 3,
                            Url = setupInfo.QrCodeSetupImageUrl,
                            Value = setupInfo.ManualEntryKey,
                            IsVerified = 0,
                            Status = 1,
                            VerificationPin = 0,
                            CreatedDate = DateTime.Now
                        };
                        Response response2 = IUserSecurityService.PostData(userSecurity);
                        userSecurity = (UserSecurity)response2.Data;
                        divBindGoogleAuthenticator.Visible = true;

                        imgQRCode.ImageUrl = setupInfo.QrCodeSetupImageUrl;
                        lblManualEntryKey.Text = setupInfo.ManualEntryKey;
                        userSecurities.Add(userSecurity);
                        user.UserIdUserSecurities = userSecurities;

                        googleAuthBindStatus.InnerHtml = "Not Binded";
                        googleAuthBindStatus.Attributes.Add("class", "text-danger");
                        googleAuthBindedDate.InnerHtml = " - <span class='text-danger'>Not Binded</span>";
                        googleAuthRebindButton.Visible = false;
                        googleAuthBindCode.Visible = true;
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                }



                Response responseULHList = IUserLoginHistoryService.GetDataByPropertyName(nameof(UserLoginHistory.UserId), user.Id.ToString(), true, 0, 10, true);
                if (responseULHList.IsSuccess)
                {
                    List<UserLoginHistory> userLoginHistorys = (List<UserLoginHistory>)responseULHList.Data;

                    if (userLoginHistorys.Count > 0)
                    {
                        int i = 1;
                        StringBuilder sb = new StringBuilder();
                        foreach (UserLoginHistory uLH in userLoginHistorys)
                        {
                            string date = uLH.LoginDate.ToString("dd/MM/yyyy");
                            string time = uLH.LoginDate.ToString("hh:mm tt");
                            sb.Append(@"<tr>
                            <td>" + i + @"</td>
                            <td>" + date + @"</td>
                            <td>" + time + @"</td>
                            <td>" + (uLH.LoginIp.ToString().Substring(uLH.LoginIp.Length - 2) == ",0" ? uLH.LoginIp.ToString().Substring(0, uLH.LoginIp.Length - 2) : uLH.LoginIp) + @"</td>
                        </tr>");
                            i++;
                        }
                        tbodyUserLoginHistory.InnerHtml = sb.ToString();
                    }
                }


                //check user hardcopy
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    User userHard = (User)responseUList.Data;
                    if (userHard.IsHardCopy == 1)
                    {
                        hardCopyStatus.InnerHtml = "Activated";
                        hardCopyStatus.Attributes.Add("class", "text-success");
                    }
                    else
                    {
                        hardCopyStatus.InnerHtml = "Deactivated";
                        hardCopyStatus.Attributes.Add("class", "text-danger");
                    }
                }

                //Bank Details
                Response responseBDList = IBanksDefService.GetDataByFilter(" status = 1 ", 0, 0, true);
                List<BanksDef> bankdefs = new List<BanksDef>();

                if (!IsPostBack)
                {
                    if (responseBDList.IsSuccess)
                    {
                        bankdefs = (List<BanksDef>)responseBDList.Data;
                        bankdefs = bankdefs.OrderBy(x => x.Name).ToList();
                        foreach (BanksDef x in bankdefs)
                        {
                            ListItem listItem = new ListItem
                            {
                                Text = x.Name,
                                Value = x.Id.ToString()
                            };
                            //listItem.Attributes.Add("accountnumlength", x.NoFormat);
                            ddlBank.Items.Add(listItem);
                        }

                        Response responseMHBList = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserId), user.Id.ToString(), true, 0, 0, true);
                        if (responseMHBList.IsSuccess)
                        {
                            List<MaHolderBank> hbs = (List<MaHolderBank>)responseMHBList.Data;
                            if (hbs.Count > 0)
                            {
                                MaHolderBank hb = hbs.FirstOrDefault(x => x.Status == 1);
                                if (hb != null)
                                {
                                    hb.BankDefIdBanksDef = bankdefs.FirstOrDefault(x => x.Id == hb.BankDefId);
                                    hdnBankNoFormat.Value = (hb.BankDefIdBanksDef != null ? hb.BankDefIdBanksDef.NoFormat : "");
                                    if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT")
                                    {

                                    }
                                    else
                                    {
                                        ddlAccountname.Items.Add(new ListItem(maHolderReg.Name1, maHolderReg.Name1));
                                    }

                                }
                                if (hbs.FirstOrDefault().Status != 1)
                                {
                                    hb = hbs.FirstOrDefault(x => x.Status == 0 || x.Status == 9);
                                    if (hb != null)
                                    {
                                        if (hb.Status == 0 || hb.Status == 9)
                                        {
                                            hb.BankDefIdBanksDef = bankdefs.Where(x => x.Id == hb.BankDefId).LastOrDefault();
                                            if (hb.BankDefIdBanksDef != null)
                                            {
                                                string htmlPending = "<tr><th>Bank Name</th><td>" + (hb.BankDefIdBanksDef != null ? hb.BankDefIdBanksDef.Name : "Inactive bank") + "</td></tr>";
                                                htmlPending += "<tr><th>Account Name</th><td>" + hb.AccountName + "</td></tr>";
                                                htmlPending += "<tr><th>Account Number</th><td>" + hb.BankAccountNo + "</td></tr>";
                                                bankdetail.InnerHtml = htmlPending;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                hdnBankNoFormat.Value = (bankdefs.FirstOrDefault() != null ? bankdefs.FirstOrDefault().NoFormat : "");
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseMHBList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBDList.Message + "\", '');", true);
                    }


                }
                Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                if (IsImpersonate)
                {
                    googleAuthBindCode.Visible = false;
                    txtOldPassword.Enabled = false;
                    txtNewPassword.Enabled = false;
                    txtConfirmNewPassword.Enabled = false;
                    txtPacNo.Enabled = false;
                    btnPAC.Visible = false;
                    btnChangePassword.Enabled = false;
                    txtPassword.Enabled = false;
                    txtNewTransactionPassword.Enabled = false;
                    txtConfirmTransactionPassword.Enabled = false;
                    txtPacNoTrans.Enabled = false;
                    btnPACTrans.Visible = false;
                    btnDeactivate.Enabled = false;
                    ddlBank.Enabled = false;
                    txtAccountNumber.Enabled = false;
                    ddlAccountname.Enabled = false;
                    FileUpload1.Enabled = false;
                    txtPacNoBank.Enabled = false;
                    btnPACBank.Visible = false;
                    btnBankDetailsSubmit.Enabled = false;
                    googleAuthRebindButton.Visible = false;
                    AddBank.Visible = false;
                    hdnImpersonate.Value = "1";
                }
                else
                {
                    hdnImpersonate.Value = "0";

                }

            }
        }

        public void bindBankDetails(User user, MaHolderReg maHolderReg)
        {
            //Bank Details
            ddlBank.Items.Clear();
            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            userAccounts = (List<UserAccount>)responseUAList.Data;

            UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == maHolderReg.HolderNo.ToString()).FirstOrDefault();
            Response responseBDList = IBanksDefService.GetDataByFilter(" status=1 ", 0, 0, true);
            if (responseBDList.IsSuccess)
            {
                List<BanksDef> bankdefs2 = (List<BanksDef>)responseBDList.Data;
                ListItem selectItem = new ListItem
                {
                    Text = "Select Bank",
                    Value = "0"
                };
                ddlBank.Items.Add(selectItem);
                foreach (BanksDef x in bankdefs2.OrderBy(a => a.Name).ToList())
                {
                    ListItem listItem = new ListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    };
                    //listItem.Attributes.Add("accountnumlength", x.NoFormat);
                    ddlBank.Items.Add(listItem);
                }

                ddlAccountname.Items.Clear();
                if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT")
                {
                    ListItem listItem = new ListItem();
                    if (primaryAcc.IsPrinciple == 1)
                    {
                        listItem.Text = maHolderReg.Name1;
                        listItem.Value = maHolderReg.Name1;
                    }
                    else
                    {
                        listItem.Text = maHolderReg.Name2;
                        listItem.Value = maHolderReg.Name2;

                    }

                    ddlAccountname.Items.Add(listItem);

                }
                else
                {
                    ddlAccountname.Items.Add(new ListItem(maHolderReg.Name1, maHolderReg.Name1));
                }
                bankdetail.InnerHtml = "";
                bankdenied.Visible = false;
                BindBankApproval(bankdefs2);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBDList.Message + "\", '');", true);
            }
        }

        public void BindBankApproval(List<BanksDef> bankdefs2)
        {
            Boolean IsImpersonate = false;
            banknotverified.Visible = false;
            bankverified.Visible = false;
            bankdenied.Visible = false;
            if (Session["user"] != null)
            {
                IsImpersonate = ServicesManager.IsImpersonated(Context);
            }
            User user = (User)Session["user"];
            if (user != null)
            {
                Response responseuserMHBList = IMaHolderBankService.GetDataByFilter(" user_id='" + user.Id + "' and status not in (8,9) ", 0, 0, true);
                if (responseuserMHBList.IsSuccess)
                {
                    List<MaHolderBank> hbs = (List<MaHolderBank>)responseuserMHBList.Data;
                    if (hbs.Count > 0)
                    {
                        bankDetailsBindStatus.InnerHtml = "Binded (" + hbs.Count + ")";
                        bankDetailsBindStatus.Attributes.Add("class", "text-success");
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var hb in hbs)
                        {

                            string bankStatus = string.Empty;
                            string image = !string.IsNullOrEmpty(hb.Image) ? "Image <i class='fa fa-eye'></i>" : "";
                            switch (hb.Status)
                            {
                                case (int)CustomStatus.UserBank_Status.Pending:
                                    bankStatus = CustomStatus.UserBank_Status.Pending.ToString();
                                    break;
                                case (int)CustomStatus.UserBank_Status.Approved:
                                    bankStatus = CustomStatus.UserBank_Status.Approved.ToString();
                                    break;
                                case (int)CustomStatus.UserBank_Status.OldApproved:
                                    bankStatus = CustomStatus.UserBank_Status.OldApproved.ToString();
                                    break;
                                case (int)CustomStatus.UserBank_Status.Rejected:
                                    bankStatus = CustomStatus.UserBank_Status.Rejected.ToString();
                                    break;
                            }

                            string bankName = string.Empty;
                            Response responseBD = IBanksDefService.GetSingle(hb.BankDefId);
                            if (responseBD.IsSuccess)
                            {
                                BanksDef banksDef = (BanksDef)responseBD.Data;
                                bankName = banksDef.Name;
                                bankStatus = bankStatus != "Rejected" ? bankStatus + (banksDef.Status == 0 ? " (Inactive)" : "") : bankStatus;
                            }
                            if (bankStatus != "Rejected")
                            {
                                string accountfilter = " ma_holder_bank_id=" + hb.Id + " and status=1 ";
                                var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                                List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;

                                stringBuilder.Append(@"<tr>
                                        <td>" + bankName + @"</td>
                                        <td>" + hb.BankAccountNo + @"</td>
                                        <td>" + (listBanks.Count == 0 ? "-" : String.Join(", ", user.UserIdUserAccounts.Where(x => listBanks.Select(y => y.UserAccountId).Contains(x.Id)).Select(x => x.AccountNo).ToArray())) + @"</td>
                                        <td><a target='_blank' href='" + hb.Image + @"'>" + image + @"</a></td>
                                        <td>" + hb.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                        <td>" + bankStatus + @"</td>
                                        <td>" + (IsImpersonate ? "" : "<a href='javascript:;' class='btn btn-sm deleteBank' data-id='" + hb.Id + @"'><i class='fa fa-trash'></i></a>") + @" </td>
                                    </tr>");
                            }

                        }
                        tbodyUserBankAccounts.InnerHtml = stringBuilder.ToString();
                        if (hbs.Count == 5)
                        {
                            divAddBank.Visible = false;
                            AddBank.Visible = false;
                        }
                    }
                    else
                    {
                        bankDetailsBindStatus.InnerHtml = "Not binded";
                        bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseuserMHBList.Message + "\", '');", true);
                }
            }
            else
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                }
            }
        }

        protected void btnBankDetailsSubmit_Click(object sender, EventArgs e)
        {
            Boolean IsImpersonate = false;
            int checkAuthenticationNumber = 0;
            int authenticatorType = 0;
            try
            {
                if (rdnGoogleBank.Checked == true)
                    authenticatorType = 1;
                else if (rdnMobileBank.Checked == true)
                    authenticatorType = 0;

                Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { Type = authenticatorType, authPin = txtPacNoBank.Text, Step = 3, Condition1 = true, httpContext = HttpContext.Current });
                try
                {
                    if (responseUser.IsSuccess)
                    {
                        responseUserStep1 = (UserLoginStep)responseUser.Data;
                        UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
                        string message = userPolicyEnum3.ToDescriptionString();
                        if (message == "SUCCESS")
                        {
                            Session["isVerified"] = 1;
                            checkAuthenticationNumber = 1;
                        }
                        else if (message == "EXCEPTION")
                        {

                            responseUser.IsSuccess = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');addBank();", true);
                            //lblGoogleAuthenticationInfo.InnerHtml = responseUser.Message;

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                        }
                    }
                    else
                    {
                        responseUser.IsSuccess = false;
                    }
                }
                catch (Exception ex)
                {
                    responseUser.IsSuccess = false;
                    responseUser.Message = ex.Message;
                    Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);

                }
                string subdir = Server.MapPath("/UserKyc/");
                if (checkAuthenticationNumber == 1)
                {
                    User user = (User)Session["user"];

                    Response responseUser2 = (Response)UserPolicy.AddBank(new UserPolicy.UserClass { Type = authenticatorType, authPin = txtPin.Text, Step = 1, bankID = ddlBank.SelectedValue, bankAccountNum = txtAccountNumber.Text, User = user, FileUpload1 = FileUpload1, path = subdir, AccountName = ddlAccountname.SelectedValue, httpContext = HttpContext.Current });
                    try
                    {
                        if (responseUser2.IsSuccess)
                        {
                            responseUserCenterStep2 = (UserCenterStep)responseUser2.Data;
                            UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserCenterStep2.Code);
                            string message = userPolicyEnum3.ToDescriptionString();
                            if (message == "SUCCESS")
                            {
                                Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(y => y.IsPrimary == 1).FirstOrDefault().AccountNo);
                                if (responseMA.IsSuccess)
                                {
                                    MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                    // alert message
                                    //bindBankDetails(user, maHolderReg);

                                    Response responseBindBankDetails = (Response)UserPolicy.bindBankDetails(user, maHolderReg);
                                    if (responseBindBankDetails.IsSuccess)
                                    {
                                        try
                                        {
                                            responseUserCenterStep2 = (UserCenterStep)responseBindBankDetails.Data;


                                            ListItem selectItem = new ListItem
                                            {
                                                Text = "Select Bank",
                                                Value = "0"
                                            };
                                            ddlBank.Items.Add(selectItem);

                                            foreach (BanksDef x in responseUserCenterStep2.BanksDefs.OrderBy(a => a.Name).ToList())
                                            {
                                                ListItem listItem = new ListItem
                                                {
                                                    Text = x.Name,
                                                    Value = x.Id.ToString()
                                                };
                                                //listItem.Attributes.Add("accountnumlength", x.NoFormat);
                                                ddlBank.Items.Add(listItem);
                                            }
                                            ddlAccountname.Items.Clear();
                                            bankdetail.InnerHtml = "";
                                            bankdenied.Visible = false;

                                        }
                                        catch (Exception ex)
                                        {
                                            responseBindBankDetails.IsSuccess = false;
                                            responseBindBankDetails.Message = ex.Message;
                                            Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);

                                        }
                                        banknotverified.Visible = false;
                                        bankverified.Visible = false;
                                        bankdenied.Visible = false;
                                        if (Session["user"] != null)
                                        {
                                            IsImpersonate = ServicesManager.IsImpersonated(Context);
                                        }

                                        User tempUser = (User)Session["user"];
                                        Response responseBindBankApproval = (Response)UserPolicy.BindBankApproval(new UserPolicy.UserClass { Type = authenticatorType, authPin = txtPin.Text, Step = 1, Condition1 = true, User = user, bankdefs = responseUserCenterStep2.BanksDefs, httpContext = HttpContext.Current });
                                        if (responseBindBankApproval.IsSuccess)
                                        {
                                            responseUserCenterStep3 = (UserCenterStep)responseBindBankApproval.Data;
                                            userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserCenterStep3.Code);
                                            message = userPolicyEnum3.ToDescriptionString();
                                            if (message != "EXCEPTION")
                                            {
                                                if (responseUserCenterStep3.maHolderBanks.Count > 0)
                                                {
                                                    StringBuilder stringBuilder = new StringBuilder();
                                                    bankDetailsBindStatus.InnerHtml = "Binded (" + responseUserCenterStep3.maHolderBanks.Count + ")";
                                                    bankDetailsBindStatus.Attributes.Add("class", "text-success");
                                                    int i = 0;
                                                    foreach (var hb in responseUserCenterStep3.maHolderBanks)
                                                    {
                                                        string image = !string.IsNullOrEmpty(hb.Image) ? "Image <i class='fa fa-eye'></i>" : "";
                                                        if (responseUserCenterStep3.BankStatus[i] != "Rejected")
                                                        {
                                                            stringBuilder.Append(@"<tr>

                                                            <td>" + responseUserCenterStep3.BankName + @"</td>

                                                            <td>" + hb.BankAccountNo + @"</td>

                                                            <td>" + (responseUserCenterStep3.userAccountBanks.Count == 0 ? "-" : String.Join(", ", user.UserIdUserAccounts.Where(x => responseUserCenterStep3.userAccountBanks.Select(y => y.UserAccountId).Contains(x.Id)).Select(x => x.AccountNo).ToArray())) + @"</td>

                                                            <td><a target='_blank' href='" + hb.Image + @"'>" + image + @"</a></td>
                                                            <td>" + hb.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                                            <td>" + (bankdefs.FirstOrDefault(a => a.Id == hb.BankDefId).Status == 0 ? "Inactive bank" : responseUserCenterStep3.BankStatus[i]) + @"</td>
                                                            <td>" + (IsImpersonate == true ? "" : "<a href='javascript:;' class='btn btn-sm deleteBank' data-id='" + hb.Id + @"'><i class='fa fa-trash'></i></a>") + @" </td>
                                                        </tr>");
                                                        }
                                                        i++;
                                                    }
                                                    tbodyUserBankAccounts.InnerHtml = stringBuilder.ToString();
                                                    if (responseUserCenterStep3.maHolderBanks.Count == 5)
                                                    {
                                                        divAddBank.Visible = false;
                                                        AddBank.Visible = false;
                                                    }
                                                }
                                                else
                                                {
                                                    bankDetailsBindStatus.InnerHtml = "Not binded";
                                                    bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                                                }
                                            }
                                            else
                                            {
                                                responseUser2.IsSuccess = false;
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseUser2.Message + "\", '');addBank();", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBankApproval.Message + "\", '');addBank();", true);
                                        }

                                        //BindBankApproval(bankdefs);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBankDetails.Message + "\", '');addBank();", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseMA.Message + "\", '');addBank();", true);
                                }

                                ddlBank.Items.Clear();

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Successfully sent request for adding bank details. You will receive a email once Apex admin has approved/rejected your request.', '/UserSettings.aspx?isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                            }
                            else if (message == "EXCEPTION")
                            {

                                responseUser2.IsSuccess = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseUser2.Message + "\", '');addBank();", true);
                                //lblGoogleAuthenticationInfo.InnerHtml = responseUser.Message;

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                            }
                        }
                        else
                        {
                            responseUser2.IsSuccess = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        responseUser2.IsSuccess = false;
                        responseUser2.Message = ex.Message;
                        Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);

                    }
                }

                txtPacNoBank.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setting Page btnBankDetailsSubmit_Click " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }
        }

        private static UserLoginStep responseUserStep1 { get; set; }
        private static UserCenterStep responseUserCenterStep1 { get; set; }
        private static UserCenterStep responseUserCenterStep2 { get; set; }
        private static UserCenterStep responseUserCenterStep3 { get; set; }
        private static UserLoginStep responseUserStep3 { get; set; }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                var hasUpperChar = txtNewPassword.Text.Any(char.IsUpper);
                var hasLowerChar = txtNewPassword.Text.Any(char.IsLower);
                var rule2 = txtNewPassword.Text.Any(char.IsNumber);
                int rule3;
                string message = "";
                bool isValid = true;
                string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(txtOldPassword.Text);
                int checkAuthenticationNumber = 0;
                User user = (User)Session["user"];

                if (txtNewPassword.Text.Length >= 8 && txtNewPassword.Text.Length <= 16)
                    rule3 = 1;
                else
                    rule3 = 0;

                if (txtOldPassword.Text == "" && txtNewPassword.Text == "" && txtConfirmNewPassword.Text == "")
                {
                    message += "Please enter required fields.<br/>";
                    isValid = false;
                }
                if (user.Password != encryptedPassword)
                {
                    message += "Wrong password.<br/>";
                    isValid = false;
                }
                if (txtNewPassword.Text != txtConfirmNewPassword.Text)
                {
                    message += "New Password and Confirm New Password must be the same.<br/>";
                    isValid = false;
                }
                if (rule2 == false)
                {
                    message += "New Password must contain at least one number.<br/>";
                    isValid = false;
                }
                if (rule3 == 0)
                {
                    message += "New Password length must between 8 to 16 characters.<br/>";
                    isValid = false;
                }
                if (hasUpperChar == false)
                {
                    message += "New Password must contain at least one uppercase alphabet letter.<br/>";
                    isValid = false;
                }
                if (hasLowerChar == false)
                {
                    message += "New Password must contain at least one lowercase alphabet letter.<br/>";
                    isValid = false;
                }
                if (CustomEncryptorDecryptor.EncryptPassword(txtNewPassword.Text) == user.Password)
                {
                    message += "This password has been used before, please choose another password in order to proceed.<br/>";
                    isValid = false;
                }

                if (isValid == true)
                {
                    //google authenticator check
                    if (rdnGoogle.Checked == true)
                    {
                        LoginPasswordGA.Visible = true;
                        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                        if (responseUSList.IsSuccess)
                        {
                            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                            if (googleAuthUserSecurity.IsVerified != 0)
                            {
                                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                if (Int32.TryParse(txtPacNo.Text.Trim(), out int result))
                                {
                                    bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, txtPacNo.Text);
                                    if (isCorrectPIN)
                                    {
                                        checkAuthenticationNumber = 1;
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Please enter correct authentication code.', '');", true);
                                        checkAuthenticationNumber = 0;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Authentication code must be Numeric.', '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Please bind Google Authenticator.', '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('loginPassword');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                        }
                    }
                    //mobile pin check
                    else if (rdnMobile.Checked == true)
                    {
                        lblGoogleAuthenticationInfo.InnerHtml = "";
                        Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { User = user, Type = 0, authPin = txtPacNo.Text, Condition1 = true, Step = 3, httpContext = HttpContext.Current });
                        try
                        {
                            if (responseUser.IsSuccess)
                            {
                                responseUserStep3 = (UserLoginStep)responseUser.Data;
                                UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep3.Code);
                                string message3 = userPolicyEnum3.ToDescriptionString();
                                if (message3 == "SUCCESS")
                                {
                                    Session["isVerified"] = 1;
                                    hdnMobilePinRequested.Value = "1";
                                    checkAuthenticationNumber = 1;
                                }
                                else if (message3 == "EXCEPTION")
                                {
                                    responseUser.Message = message3;
                                    responseUser.IsSuccess = false;
                                    checkAuthenticationNumber = 0;

                                }
                                else
                                {
                                    responseUser.Message = message3;
                                    responseUser.IsSuccess = false;
                                    hdnMobilePinRequested.Value = "0";
                                    lblInfo.InnerHtml = responseUser.Message;
                                    if (userPolicyEnum3 == UserPolicyEnum3.UA3)
                                    {
                                        hdnMobilePinRequested.Value = "0";
                                    }
                                    else if (userPolicyEnum3 == UserPolicyEnum3.UA4)
                                    {
                                        checkAuthenticationNumber = 0;
                                    }
                                    else if (userPolicyEnum3 == UserPolicyEnum3.UA5)
                                    {
                                        hdnMobilePinRequested.Value = "0";
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            {
                                responseUser.IsSuccess = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseUser.Message + "','');", true);

                            }


                        }
                        catch (Exception ex)
                        {
                            responseUser.IsSuccess = false;
                            responseUser.Message = ex.Message;
                            Logger.WriteLog("index Page VerifyLogin " + ex.Message);
                        }

                    }
                    if (checkAuthenticationNumber == 1)
                    {
                        string encryptedNewPassword = CustomEncryptorDecryptor.EncryptPassword(txtNewPassword.Text);
                        Response response = IUserService.GetSingle(user.Id);
                        if (response.IsSuccess)
                        {
                            User newUser = (User)response.Data;
                            if (newUser.Password == newUser.TransPwd)
                            {
                                newUser.TransPwd = encryptedNewPassword;
                            }
                            newUser.Password = encryptedNewPassword;
                            newUser.SmsAttempts = 0;
                            newUser.VerifyExpired = 1;
                            newUser.VerificationCode = 0;
                            Response response2 = IUserService.UpdateData(newUser);
                            newUser = (User)response2.Data;
                            Session["user"] = newUser;

                            UserLogMain ulm = new UserLogMain
                            {
                                TableName = "users",
                                Description = "Change password successful",
                                UserId = user.Id,
                                UserAccountId = 0,
                                UpdatedDate = DateTime.Now,
                                RefId = user.Id,
                                RefValue = user.Username,
                                StatusType = 1
                            };

                            Response response1 = IUserLogMainService.PostData(ulm);
                            ulm = (UserLogMain)response1.Data;
                            SendEmail("User Information Update", "Login Password has been changed on ");
                            hdnMobilePinRequested.Value = "0";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Successfully updated Password.', '');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('loginPassword');ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', 'Please enter correct authentication code.', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('loginPassword');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
                }

                txtPacNo.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnChangePassword_Click: " + ex.Message);
            }
        }

        protected void btnAddressSubmit_Click(object sender, EventArgs e)
        {

        }

        protected void btnTransactionPassword_Click(object sender, EventArgs e)
        {
            try
            {
                var hasUpperChar = txtNewTransactionPassword.Text.Any(char.IsUpper);
                var hasLowerChar = txtNewTransactionPassword.Text.Any(char.IsLower);
                var rule2 = txtNewTransactionPassword.Text.Any(char.IsNumber);
                int rule3;
                int checkAuthenticationNumber = 0;
                User user = (User)Session["user"];
                string message = "";
                bool isValid = true;
                string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(txtPassword.Text);

                if (txtNewTransactionPassword.Text.Length >= 8 && txtNewTransactionPassword.Text.Length <= 16)
                    rule3 = 1;
                else
                    rule3 = 0;

                if (txtNewTransactionPassword.Text == "" && txtConfirmTransactionPassword.Text == "" && txtPassword.Text == "")
                {
                    message += "Please enter required fields.<br/>";
                    isValid = false;
                }
                if (user.Password != encryptedPassword)
                {
                    message += "Wrong password.<br/>";
                    isValid = false;
                }
                if (txtNewTransactionPassword.Text != txtConfirmTransactionPassword.Text)
                {
                    message += "New Transaction Password and Confirm Transaction Password must be same.<br/>";
                    isValid = false;
                }
                if (rule2 == false)
                {
                    message += "New Transaction Password must contain at least one number.<br/>";
                    isValid = false;
                }
                if (rule3 == 0)
                {
                    message += "New Transaction Password length must between 8 to 16 characters.<br/>";
                    isValid = false;
                }
                if (hasUpperChar == false)
                {
                    message += "New Transaction Password must contain at least one uppercase alphabet letter.<br/>";
                    isValid = false;
                }
                if (hasLowerChar == false)
                {
                    message += "New Transaction Password must contain at least one lowercase alphabet letter.<br/>";
                    isValid = false;
                }
                if (CustomEncryptorDecryptor.EncryptPassword(txtNewTransactionPassword.Text) == user.TransPwd)
                {
                    message += "This transaction password has been used before, please choose another password in order to proceed.<br/>";
                    isValid = false;
                }

                if (isValid == true)
                {

                    if (rdnGoogleTrans.Checked == true)
                    {
                        TransactionPasswordGA.Visible = true;
                        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                        if (responseUSList.IsSuccess)
                        {
                            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                            if (googleAuthUserSecurity.IsVerified != 0)
                            {
                                if (Int32.TryParse(txtPacNoTrans.Text.Trim(), out int result))
                                {
                                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                    bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, txtPacNoTrans.Text);
                                    if (isCorrectPIN)
                                    {
                                        checkAuthenticationNumber = 1;
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Please enter correct authentication code.', '');", true);
                                        checkAuthenticationNumber = 0;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Authentication code must be Numeric.', '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Please bind Google Authenticator.', '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('transPassword');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                        }
                    }
                    //mobile pin check
                    else if (rdnMobileTrans.Checked == true)
                    {
                        TransactionPasswordGA.Visible = false;
                        Response responseUser = IUserService.GetSingle(user.Id);
                        if (responseUser.IsSuccess)
                        {
                            user = (User)responseUser.Data;
                            if (!string.IsNullOrEmpty(txtPacNoTrans.Text))
                            {
                                if (user.VerificationCode != 0)
                                {
                                    if (user.VerifyExpired == 0)
                                    {
                                        if (Int32.TryParse(txtPacNoTrans.Text.Trim(), out int result))
                                        {
                                            if (user.VerificationCode == Convert.ToInt32(txtPacNoTrans.Text.ToString()))
                                            {
                                                checkAuthenticationNumber = 1;
                                                hdnMobilePinRequestedTrans.Value = "0";
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Wrong OTP. Please Enter Again.', '');", true);
                                                checkAuthenticationNumber = 0;
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'OTP must be Numeric.', '');", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'OTP Expired. Please Request Again.', '');", true);
                                        hdnMobilePinRequestedTrans.Value = "0";
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Please Request OTP.', '');", true);
                                    hdnMobilePinRequestedTrans.Value = "0";
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Please Enter OTP.', '');", true);
                            }
                        }
                    }
                    if (checkAuthenticationNumber == 1)
                    {
                        string encryptedNewPassword = CustomEncryptorDecryptor.EncryptPassword(txtNewTransactionPassword.Text);
                        Response response = IUserService.GetSingle(user.Id);
                        if (response.IsSuccess)
                        {
                            User newUser = (User)response.Data;
                            newUser.TransPwd = encryptedNewPassword;

                            newUser.SmsAttempts = 0;
                            newUser.VerifyExpired = 1;
                            newUser.VerificationCode = 0;
                            Response response2 = IUserService.UpdateData(newUser);
                            newUser = (User)response2.Data;
                            Session["user"] = newUser;

                            UserLogMain ulm = new UserLogMain
                            {
                                TableName = "users",
                                Description = "Change transaction password successful",
                                UserId = user.Id,
                                UserAccountId = 0,
                                UpdatedDate = DateTime.Now,
                                RefId = user.Id,
                                RefValue = user.Username,
                                StatusType = 1
                            };

                            Response response1 = IUserLogMainService.PostData(ulm);
                            ulm = (UserLogMain)response1.Data;
                            SendEmail("User Information Update", "Transaction Password has been changed on ");

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Transaction Password successfully updated.', '');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('transPassword');ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', 'Please enter correct authentication code.', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('transPassword');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
                }
                txtPacNoTrans.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnTransactionPassword_Click: " + ex.Message);
            }
        }

        private static UserLoginStep responseUserStep2 { get; set; }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PhoneVerify(String title, String password)
        {

            User user = (User)HttpContext.Current.Session["user"];
            if (user == null)
            {
                return "se";
            }


            Response responseUser = new Response();
            try
            {
                responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { title = title, password = password, Step = 2, Condition1 = true, httpContext = HttpContext.Current });
                if (responseUser.IsSuccess)
                {
                    responseUserStep2 = (UserLoginStep)responseUser.Data;
                    UserPolicyEnum2 userPolicyEnum2 = (UserPolicyEnum2)Enum.Parse(typeof(UserPolicyEnum2), responseUserStep2.Code);
                    string message = userPolicyEnum2.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        return responseUser.Message;
                    }
                    else if (message == "EXCEPTION")
                    {
                        responseUser.Message = message;
                        responseUser.IsSuccess = false;
                        return responseUser.Message;
                    }
                    else
                    {
                        return responseUser.Message;
                    }
                }
                else
                {
                    return responseUser.Message;
                }
            }
            catch (Exception ex)
            {
                responseUser.IsSuccess = false;
                responseUser.Message = ex.Message;
                Logger.WriteLog("Portfolio Page VerifyLogin " + ex.Message);
                return responseUser.Message;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static string OtpVerify(string otp)
        {
            User user = (User)HttpContext.Current.Session["user"];
            Response responseUser = IUserService.GetSingle(user.Id);
            if (responseUser.IsSuccess)
            {
                user = (User)responseUser.Data;
                if (!string.IsNullOrEmpty(otp))
                {
                    if (user.VerificationCode != 0)
                    {
                        if (user.VerifyExpired == 0)
                        {
                            if (Int32.TryParse(otp, out int result))
                            {
                                if (user.VerificationCode == Convert.ToInt32(otp))
                                {
                                    return "success";
                                }
                                else
                                {
                                    return "Wrong OTP. Please Enter Again.";
                                }
                            }
                            else
                            {
                                return "OTP must be Numeric.";
                            }
                        }
                        else
                        {
                            return "OTP Expired. Please Request Again.";
                        }
                    }
                    else
                    {
                        return "Please Request OTP.";
                    }
                }
                else
                {
                    return "Please Enter OTP.";
                }
            }
            return "Please Enter OTP.";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object BankPhoneVerify()
        {
            User user = (User)HttpContext.Current.Session["user"];
            Response responseUAList = IUserAccountService.GetDataByFilter("user_id = '" + user.Id + "' and is_primary = 1 and status=1 ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                UserAccount ua = ((List<UserAccount>)responseUAList.Data).ToList().FirstOrDefault();
                Response responseMAHolderRegList = ServicesManager.GetMaHolderRegByAccountNo(ua.AccountNo);
                if (responseMAHolderRegList.IsSuccess)
                {

                    MaHolderReg maHolderReg = (MaHolderReg)responseMAHolderRegList.Data;
                    string toMobileNumber = maHolderReg.HandPhoneNo;
                    if (ua.IsPrinciple == 0)
                        toMobileNumber = maHolderReg.JointTelNo;
                    return BankCheckAndRequestPin(toMobileNumber);
                }
                else
                {
                    return responseMAHolderRegList.Message;
                }
            }
            else
            {
                return responseUAList.Message;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetBankAccNoLength(Int32 bankId)
        {
            Response response = new Response();
            try
            {
                if (bankId != 0)
                {
                    Response responseBD = IBanksDefService.GetSingle(bankId);
                    if (responseBD.IsSuccess)
                    {
                        BanksDef banksDef = (BanksDef)responseBD.Data;
                        if (banksDef != null)
                        {
                            string accNoLength = banksDef.NoFormat;
                            response.IsSuccess = true;
                            response.Data = accNoLength;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Selected bank is Inactive";
                        }
                    }
                    else
                    {
                        return responseBD;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Int32 currentSecLock = 0;


        public static void PinTimerForSMSLock(User user)
        {
            try
            {
                while (currentSecLock <= SMSLockTimeInSeconds)
                {
                    if (currentSecLock == SMSLockTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.SmsAttempts = 0;
                        user.IsSmsLocked = 0;
                        IUserService.UpdateData(user);
                        currentSecLock = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecLock++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page PinTimerForSMSLock: " + ex.Message);
            }
        }

        public static Int32 currentSec = 0;
        public static void PinTimer(User user)
        {
            try
            {
                while (currentSec <= SMSExpirationTimeInSeconds)
                {
                    if (currentSec == SMSExpirationTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.VerifyExpired = 1;
                        IUserService.UpdateData(user);
                        currentSec = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSec++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page PinTimer: " + ex.Message);
            }
        }

        public static string CheckAndRequestPin(String mobileNo, String title)
        {
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    user = (User)responseUList.Data;
                    if (user != null)
                    {
                        if (user.IsSmsLocked == 1)
                        {
                            Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                            PinTimerThread.Start();
                            returnString = "sms locked";
                        }
                        else if (user.VerifyExpired == 1)
                        {
                            if (user.SmsAttempts >= 3)
                            {
                                user.IsSmsLocked = 1;
                                user.VerifyExpired = 1;
                                Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                                PinTimerThread.Start();
                                returnString = "sms locked";
                            }
                            else
                            {
                                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                String result = "";

                                result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                Int32 length = mobileNo.Length;
                                String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);
                                user.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                user.VerifyExpired = 0;
                                user.SmsAttempts = user.SmsAttempts + 1;

                                Thread PinTimerThread = new Thread(() => PinTimer(user));
                                PinTimerThread.Start();
                                returnString = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                            }
                            IUserService.UpdateData(user);
                            return returnString;
                        }
                        else
                        {
                            Thread PinTimerThread = new Thread(() => PinTimer(user));
                            PinTimerThread.Start();
                            returnString = "already sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                        }
                    }
                    else
                        returnString = "no account";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page CheckAndRequestPin: " + ex.Message);
            }
            return returnString;
        }

        public static string BankCheckAndRequestPin(String mobileNo)
        {
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    user = (User)responseUList.Data;
                    if (user != null)
                    {
                        if (user.IsSmsLocked == 1)
                        {
                            Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                            PinTimerThread.Start();
                            returnString = "sms locked";
                        }
                        else if (user.VerifyExpired == 1)
                        {
                            if (user.SmsAttempts >= 3)
                            {
                                user.IsSmsLocked = 1;
                                user.VerifyExpired = 1;
                                Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                                PinTimerThread.Start();
                                returnString = "sms locked";
                            }
                            else
                            {
                                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                String result = "";
                                String title = "-";
                                result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                Int32 length = mobileNo.Length;
                                String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);
                                user.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                user.VerifyExpired = 0;
                                user.SmsAttempts = user.SmsAttempts + 1;

                                Thread PinTimerThread = new Thread(() => PinTimer(user));
                                PinTimerThread.Start();
                                returnString = "sent";
                            }
                            IUserService.UpdateData(user);
                            return returnString;
                        }
                        else
                        {
                            Thread PinTimerThread = new Thread(() => PinTimer(user));
                            PinTimerThread.Start();
                            returnString = "already sent";
                        }
                    }
                    else
                        returnString = "no account";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page CheckAndRequestPin: " + ex.Message);
            }
            return returnString;
        }

        protected void btnFuAddressUpload_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];

                Response responseUFList = IUserFileService.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                if (responseUFList.IsSuccess)
                {
                    List<UserFile> userfiles = (List<UserFile>)responseUFList.Data;
                    UserFile addressprove = userfiles.Where(x => x.UserFileTypeId == 2 && x.Status != 88).FirstOrDefault();
                    UserFile newaddressprove = new UserFile();

                    string Date = DateTime.Now.ToString("ddMMyyyy-mmsshh");
                    string G = Guid.NewGuid().ToString();

                    if (fuAddressupload.HasFile)
                    {
                        if (Path.GetExtension(fuAddressupload.FileName).ToLower() == ".jpg" ||
                            Path.GetExtension(fuAddressupload.FileName).ToLower() == ".jpeg" ||
                            Path.GetExtension(fuAddressupload.FileName).ToLower() == ".png" ||
                            Path.GetExtension(fuAddressupload.FileName).ToLower() == ".pdf")
                        {
                            string subdir = Server.MapPath("/UserAddressProof/");
                            string dbpath = "/UserAddressProof/";

                            if (!Directory.Exists(subdir))
                                Directory.CreateDirectory(subdir);

                            string filename = "ID" + user.Id + "_" + Date + (Path.GetFileName(fuAddressupload.FileName));
                            subdir = subdir + filename;
                            dbpath = dbpath + filename;

                            fuAddressupload.SaveAs(subdir);

                            if (addressprove != null)
                            {
                                //update existing record as old file.
                                addressprove.Status = 88;
                                Response response = IUserFileService.UpdateData(addressprove);
                                addressprove = (UserFile)response.Data;

                                //insert new record
                                newaddressprove.UserId = user.Id;
                                newaddressprove.UserFileTypeId = 2;
                                newaddressprove.Title = user.Id + "_Address Proof";
                                newaddressprove.Url = dbpath;
                                newaddressprove.ThumbnailUrl = "-";
                                newaddressprove.Status = 0;

                                Response response1 = IUserFileService.PostData(newaddressprove);
                                newaddressprove = (UserFile)response1.Data;
                                SendEmail("Address Proof status notification", "New Address Proof uploaded successfully");

                                UserLogMain log = new UserLogMain()
                                {
                                    TableName = "user_files",
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    Description = "User update address proof",
                                    UpdatedDate = DateTime.Now,
                                    RefId = 0,
                                    RefValue = "",
                                    StatusType = 1
                                };

                                Response response2 = IUserLogMainService.PostData(log);
                                log = (UserLogMain)response2.Data;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('addressupload');ShowCustomMessage('Alert', 'Successfully uploaded New Address Proof.', 'UserSettings.aspx');", true);
                            }
                            else
                            {
                                //insert new record
                                newaddressprove.UserId = user.Id;
                                newaddressprove.UserFileTypeId = 2;
                                newaddressprove.Title = user.Id + "_Address Proof";
                                newaddressprove.Url = dbpath;
                                newaddressprove.ThumbnailUrl = "-";
                                newaddressprove.Status = 0;

                                Response response = IUserFileService.PostData(newaddressprove);
                                newaddressprove = (UserFile)response.Data;
                                SendEmail("Address Proof status notification", "Address Proof uploaded successfully");

                                UserLogMain log = new UserLogMain()
                                {
                                    TableName = "user_files",
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    Description = "User create address proof",
                                    UpdatedDate = DateTime.Now,
                                    RefId = 0,
                                    RefValue = "",
                                    StatusType = 1
                                };

                                Response response1 = IUserLogMainService.PostData(log);
                                log = (UserLogMain)response1.Data;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('addressupload');ShowCustomMessage('Alert', 'Successfully uploaded Address Proof.', 'UserSettings.aspx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('addressupload');ShowCustomMessage('Alert', 'Invalid file format.', '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please choose file', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUFList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnFuAddressUpload_Click: " + ex.Message);
            }
        }

        protected void btnFuKYC_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                Response responseUFList = IUserFileService.GetDataByPropertyName(nameof(UserFile.UserId), user.Id.ToString(), true, 0, 0, false);
                if (responseUFList.IsSuccess)
                {
                    List<UserFile> userfiles = (List<UserFile>)responseUFList.Data;
                    UserFile kyc = userfiles.Where(x => x.UserFileTypeId == 1 && x.Status != 88).FirstOrDefault();
                    UserFile newkyc = new UserFile();

                    string Date = DateTime.Now.ToString("ddMMyyyy-mmsshh");
                    string G = Guid.NewGuid().ToString();

                    if (fuKYC.HasFile)
                    {
                        if (Path.GetExtension(fuKYC.FileName).ToLower() == ".jpg" ||
                            Path.GetExtension(fuKYC.FileName).ToLower() == ".jpeg" ||
                            Path.GetExtension(fuKYC.FileName).ToLower() == ".png" ||
                            Path.GetExtension(fuKYC.FileName).ToLower() == ".pdf")
                        {
                            string subdir = Server.MapPath("/UserKyc/");
                            string dbpath = "/UserKyc/";

                            if (!Directory.Exists(subdir))
                                Directory.CreateDirectory(subdir);

                            string filename = "ID" + user.Id + "_" + Date + (Path.GetFileName(fuKYC.FileName));
                            subdir = subdir + filename;
                            dbpath = dbpath + filename;

                            fuKYC.SaveAs(subdir);

                            if (kyc != null)
                            {
                                //update existing record as old file.
                                kyc.Status = 88;
                                Response response = IUserFileService.UpdateData(kyc);
                                kyc = (UserFile)response.Data;

                                //insert new record
                                newkyc.UserId = user.Id;
                                newkyc.UserFileTypeId = 1;
                                newkyc.Title = user.Id + "_KYC";
                                newkyc.Url = dbpath;
                                newkyc.ThumbnailUrl = "-";
                                newkyc.Status = 0;

                                Response response1 = IUserFileService.PostData(newkyc);
                                newkyc = (UserFile)response1.Data;
                                SendEmail("KYC status notification", "New KYC uploaded successfully");

                                UserLogMain log = new UserLogMain()
                                {
                                    TableName = "user_files",
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    Description = "User update KYC ",
                                    UpdatedDate = DateTime.Now,
                                    RefId = 0,
                                    RefValue = "",
                                    StatusType = 1
                                };

                                Response response2 = IUserLogMainService.PostData(log);
                                log = (UserLogMain)response2.Data;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('KYC');ShowCustomMessage('Alert', 'Successfully uploaded New KYC.', 'UserSettings.aspx');", true);
                            }
                            else
                            {
                                //insert new record
                                newkyc.UserId = user.Id;
                                newkyc.UserFileTypeId = 1;
                                newkyc.Title = user.Id + "_KYC";
                                newkyc.Url = dbpath;
                                newkyc.ThumbnailUrl = "-";
                                newkyc.Status = 0;

                                Response response = IUserFileService.PostData(newkyc);
                                newkyc = (UserFile)response.Data;
                                SendEmail("KYC status notification", "KYC uploaded successfully");

                                UserLogMain log = new UserLogMain()
                                {
                                    TableName = "user_files",
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    Description = "User create KYC",
                                    UpdatedDate = DateTime.Now,
                                    RefId = 0,
                                    RefValue = "",
                                    StatusType = 1
                                };
                                Response response1 = IUserLogMainService.PostData(log);
                                log = (UserLogMain)response1.Data;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('KYC');ShowCustomMessage('Alert', 'Successfully uploaded New KYC.', 'UserSettings.aspx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('KYC');ShowCustomMessage('Alert', 'Invalid file format.', '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please choose file', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUFList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnFuKYC_Click: " + ex.Message);
            }
        }

        protected void btnSubmitProfile_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                Response responseUDList = IUserDetailService.GetDataByPropertyName(nameof(UserDetail.UserId), user.Id.ToString(), true, 0, 0, true);
                if (responseUDList.IsSuccess)
                {
                    UserDetail ud = ((List<UserDetail>)responseUDList.Data).FirstOrDefault();
                    string dt = DateTime.Now.ToString("ddMMyyyy-mmsshh");

                    if (imageUpload.HasFile)
                    {
                        string subdir = Server.MapPath("/UserProfile/");
                        string dbpath = "/UserProfile/";

                        if (!Directory.Exists(subdir))
                            Directory.CreateDirectory(subdir);

                        string filename = "ID" + user.Id + "_" + dt + (Path.GetFileName(imageUpload.FileName));
                        subdir = subdir + filename;
                        dbpath = dbpath + filename;

                        imageUpload.SaveAs(subdir);

                        if (ud == null)
                        {
                            UserDetail x = new UserDetail
                            {
                                UserId = user.Id,
                                FirstName = "",
                                LastName = "",
                                Description = "",
                                DateOfBirth = null,
                                CreatedBy = user.Id,
                                ImageUrl = dbpath,
                                CreatedDate = DateTime.Now,
                                ModifiedBy = user.Id,
                                ModifiedDate = DateTime.Now,
                                Status = 1,
                            };

                            Response response = IUserDetailService.PostData(x);
                            x = (UserDetail)response.Data;
                            SendEmail("Profile Picture Uploaded", "Your Profile Picture has been uploaded successfully");
                        }
                        else
                        {
                            ud.ImageUrl = dbpath;
                            ud.ModifiedBy = user.Id;
                            ud.ModifiedDate = DateTime.Now;

                            Response response = IUserDetailService.UpdateData(ud);
                            ud = (UserDetail)response.Data;
                            SendEmail("Profile Picture uploaded", "Your New Profile Picture has been uploaded successfully");
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUDList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnSubmitProfile_Click: " + ex.Message);
            }
        }

        public void SendEmail(string title, string content)
        {
            User sessionUser = (User)HttpContext.Current.Session["user"];

            try
            {

                Email email = new Email();
                email.user = sessionUser;
                EmailService.SendUpdateMail(email, title, content, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page SendEmail: " + ex.Message);
            }
        }

        protected void btnActivate_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                user.IsHardCopy = 1;
                user.HardcopyUpdatedDate = DateTime.Now;
                Response response = IUserService.UpdateData(user);
                user = (User)response.Data;

                UserLogMain x = new UserLogMain()
                {
                    Description = "Activate hardcopy successful",
                    TableName = "users",
                    UpdatedDate = DateTime.Now,
                    UserId = user.Id,
                    UserAccountId = 0,
                    RefId = user.Id,
                    RefValue = user.Username,
                    StatusType = 1
                };

                Response response1 = IUserLogMainService.PostData(x);
                x = (UserLogMain)response1.Data;

                UserLogSub z = new UserLogSub()
                {
                    UserLogMainId = x.Id,
                    ColumnName = "is_hard_copy",
                    ValueOld = "Inactive",
                    ValueNew = "Active",
                };

                Response response2 = IUserLogSubService.PostData(z);
                z = (UserLogSub)response2.Data;
                SendEmail("User Information Update", "Your request for a Softcopy Statement/Report has been enabled successfully");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "ShowCustomMessage('Alert', 'HardCopy activated.', 'UserSettings.aspx');", true);
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnActivate_Click: " + ex.Message);
            }
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                user.IsHardCopy = 0;
                user.HardcopyUpdatedDate = DateTime.Now;
                Response response = IUserService.UpdateData(user);
                user = (User)response.Data;

                UserLogMain x = new UserLogMain()
                {
                    Description = "Deactivate hardcopy successful",
                    TableName = "users",
                    UpdatedDate = DateTime.Now,
                    UserId = user.Id,
                    UserAccountId = 0,
                    RefId = user.Id,
                    RefValue = user.Username,
                    StatusType = 1
                };

                Response response1 = IUserLogMainService.PostData(x);
                x = (UserLogMain)response1.Data;

                UserLogSub z = new UserLogSub()
                {
                    UserLogMainId = x.Id,
                    ColumnName = "is_hard_copy",
                    ValueOld = "Active",
                    ValueNew = "InActive",
                };

                Response response2 = IUserLogSubService.PostData(z);
                z = (UserLogSub)response2.Data;
                SendEmail("User Information Update", "Your request for a Hardcopy Statement/Report has been enabled successfully");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "ShowCustomMessage('Alert', 'HardCopy deactivated.', 'UserSettings.aspx');", true);
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnDeactivate_Click: " + ex.Message);
            }
        }

        protected void btnCode_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                Response responseUser = IUserService.GetSingle(user.Id);
                if (responseUser.IsSuccess)
                {
                    user = (User)responseUser.Data;
                    if (string.IsNullOrEmpty(txtPin.Text) || string.IsNullOrEmpty(txtOTP.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Please enter Code and OTP.', '');", true);
                    }
                    else
                    {
                        if (user.VerificationCode != 0)
                        {
                            if (user.VerifyExpired == 0)
                            {
                                if (Int32.TryParse(txtOTP.Text.Trim(), out int result))
                                {

                                    {
                                        hdnMobilePinRequestedGoogle.Value = "0";
                                        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                        if (responseUSList.IsSuccess)
                                        {
                                            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                                            if (user.VerificationCode == Convert.ToInt32(txtOTP.Text.ToString()))
                                            {
                                                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                                bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, txtPin.Text);
                                                if (isCorrectPIN)
                                                {
                                                    Session["isVerified"] = 1;
                                                    if (googleAuthUserSecurity.IsVerified == 0)
                                                    {
                                                        googleAuthUserSecurity.VerificationPin = Convert.ToInt32(txtPin.Text);
                                                        googleAuthUserSecurity.IsVerified = 1;
                                                        googleAuthUserSecurity.VerifiedDate = DateTime.Now;
                                                        IUserSecurityService.UpdateData(googleAuthUserSecurity);
                                                        Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
                                                        if (responseMA.IsSuccess)
                                                        {
                                                            MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                                            BindSettings(maHolderReg);
                                                        }
                                                        googleAuthBindedDate.InnerHtml = googleAuthUserSecurity.VerifiedDate.Value.ToString("dd/MM/yyyy");

                                                        SendEmail("User Information Update", "Google Authenticator has been binded on ");
                                                        txtPin.Text = "";
                                                        txtOTP.Text = "";

                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "Bind Google Authenticator successful",
                                                            TableName = "user_securities",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = 0,
                                                            RefId = googleAuthUserSecurity.Id,
                                                            RefValue = googleAuthUserSecurity.Value,
                                                            StatusType = 1
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                        }

                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication binded successfully', '');", true);
                                                    }
                                                    else
                                                    {
                                                        googleAuthUserSecurity.VerificationPin = Convert.ToInt32(txtPin.Text);
                                                        googleAuthUserSecurity.VerifiedDate = DateTime.Now;
                                                        IUserSecurityService.UpdateData(googleAuthUserSecurity);

                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "Bind Google Authenticator successful",
                                                            TableName = "user_securities",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = 0,
                                                            RefId = googleAuthUserSecurity.Id,
                                                            RefValue = googleAuthUserSecurity.Value,
                                                            StatusType = 1
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                        }

                                                        SendEmail("User Information Update", "Google Authenticator has been binded on ");
                                                        txtPin.Text = "";
                                                        txtOTP.Text = "";
                                                        googleAuthBindedDate.InnerHtml = googleAuthUserSecurity.VerifiedDate.Value.ToString("dd/MM/yyyy");
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication binded successfully', '');", true);
                                                    }
                                                }
                                                else
                                                {
                                                    UserLogMain ulm = new UserLogMain()
                                                    {
                                                        Description = "Bind Google Authenticator failed",
                                                        TableName = "user_securities",
                                                        UpdatedDate = DateTime.Now,
                                                        UserId = user.Id,
                                                        UserAccountId = 0,
                                                        RefId = googleAuthUserSecurity.Id,
                                                        RefValue = googleAuthUserSecurity.Value,
                                                        StatusType = 1
                                                    };
                                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                                    if (!responseLog.IsSuccess)
                                                    {
                                                        //Audit log failed
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                    }
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Verification failed.', '');", true);
                                                }
                                            }
                                            else
                                            {

                                                UserLogMain ulm = new UserLogMain()
                                                {
                                                    Description = "Bind Google Authenticator failed",
                                                    TableName = "user_securities",
                                                    UpdatedDate = DateTime.Now,
                                                    UserId = user.Id,
                                                    UserAccountId = 0,
                                                    RefId = googleAuthUserSecurity.Id,
                                                    RefValue = googleAuthUserSecurity.Value,
                                                    StatusType = 1
                                                };
                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                if (!responseLog.IsSuccess)
                                                {
                                                    //Audit log failed
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                }
                                                googleAuthUserSecurity.IsVerified = 0;
                                                IUserSecurityService.UpdateData(googleAuthUserSecurity);
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Wrong OTP. Google authentication unbinded. Try again.', '');", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                                        }
                                    }

                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'OTP must be Numeric.', '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'OTP Expired. Please Request Again.', '');", true);
                                hdnMobilePinRequestedGoogle.Value = "0";
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Please Request OTP.', '');", true);
                            hdnMobilePinRequestedGoogle.Value = "0";
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }
            //User user = (User)Session["user"];
            //int verificationFlag = 0;
            //try
            //{
            //    Response responseOTP = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { Type = 0, authPin = txtOTP.Text, Step = 3, Condition1 = true, httpContext = HttpContext.Current });

            //    if (responseOTP.IsSuccess)
            //    {
            //        responseUserStep1 = (UserLoginStep)responseOTP.Data;
            //        UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
            //        string message = userPolicyEnum3.ToDescriptionString();
            //        if (message == "SUCCESS")
            //        {
            //            Session["isVerified"] = 1;



            //            Response responseGoogle = (Response)UserPolicy.GoogleBinding(new UserPolicy.UserClass { Type = 0, User = user, authPin = txtPin.Text, Step = 2, httpContext = HttpContext.Current });
            //            if (responseGoogle.IsSuccess)
            //            {
            //                responseUserStep1 = (UserLoginStep)responseGoogle.Data;
            //                userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
            //                message = userPolicyEnum3.ToDescriptionString();
            //                if (message == "SUCCESS")
            //                {
            //                    Session["isVerified"] = 1;
            //                    verificationFlag = 1;
            //                }
            //                else if (message == "EXCEPTION")
            //                {

            //                    responseGoogle.IsSuccess = false;
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseGoogle.Message + "\", '');", true);
            //                    //lblGoogleAuthenticationInfo.InnerHtml = responseUser.Message;

            //                }
            //                else
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
            //                }
            //            }
            //            else
            //            {
            //                responseGoogle.IsSuccess = false;
            //            }

            //        }
            //        else if (message == "EXCEPTION")
            //        {

            //            responseOTP.IsSuccess = false;
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseOTP.Message + "\", '');", true);
            //            //lblGoogleAuthenticationInfo.InnerHtml = responseUser.Message;

            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseOTP.Message + "\", '');", true);
            //    }


            //    if (verificationFlag == 1)
            //    {
            //        Response responseUserCenter = (Response)UserPolicy.GoogleBinding(new UserPolicy.UserClass { User = user, authPin = txtOTP.Text, authPin2 = txtPin.Text, Step = 2, Type = 1, httpContext = HttpContext.Current });

            //        if (responseUserCenter.IsSuccess)
            //        {
            //            responseUserCenterStep2 = (UserCenterStep)responseUserCenter.Data;
            //            if (responseUserCenterStep2.Flag1 == true)
            //            {
            //                hdnMobilePinRequestedGoogle.Value = "0";
            //            }
            //            UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
            //            string message = userPolicyEnum3.ToDescriptionString();
            //            if (message == "SUCCESS")
            //            {
            //                Session["isVerified"] = 1;
            //                //googleFlag = 1;
            //                BindSettings(responseUserCenterStep2.MaHolderReg);
            //                SendEmail("User Information Update", "Google Authenticator has been binded on ");
            //                txtPin.Text = "";
            //                txtOTP.Text = "";
            //                googleAuthBindedDate.InnerHtml = responseUserCenterStep2.UserSecurity.VerifiedDate.Value.ToString("dd/MM/yyyy");
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication binded successfully', '');", true);
            //            }
            //            else if (message == "EXCEPTION")
            //            {
            //                responseUserCenter.IsSuccess = false;
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUserCenter.Message + "\", '');", true);
            //                //lblGoogleAuthenticationInfo.InnerHtml = responseUser.Message;
            //            }
            //            else
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
            //            }
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUserCenter.Message + "\", '');", true);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            //}
        }

        protected void btnRebindGoogleAuth_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];

            Response responseGoogleAuthenticator = (Response)UserPolicy.GoogleBinding(new UserPolicy.UserClass { Step = 1, User = user });
            try
            {
                if (responseGoogleAuthenticator.IsSuccess)
                {
                    responseUserCenterStep1 = (UserCenterStep)responseGoogleAuthenticator.Data;
                    UserPolicyEnum4 userPolicyEnum4 = (UserPolicyEnum4)Enum.Parse(typeof(UserPolicyEnum4), responseUserCenterStep1.Code);
                    string message = userPolicyEnum4.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        imgQRCode.ImageUrl = responseUserCenterStep1.Url;
                        lblManualEntryKey.Text = responseUserCenterStep1.ManualEntryKey;
                        googleAuthRebindButton.Visible = responseUserCenterStep1.Flag1;
                        googleAuthBindCode.Visible = responseUserCenterStep1.Flag2;
                        Session["isVerified"] = 1;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('googleAuth');", true);
                    }
                    else if (message == "EXCEPTION")
                    {
                        responseGoogleAuthenticator.IsSuccess = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseGoogleAuthenticator.Message + "\", '');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
                    }
                }
                else
                {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseGoogleAuthenticator.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                responseGoogleAuthenticator.IsSuccess = false;
                responseGoogleAuthenticator.Message = ex.Message;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }
            //try
            //{
            //    User user = (User)Session["user"];
            //    Response responseUser = IUserService.GetSingle(user.Id);
            //    if (responseUser.IsSuccess)
            //    {
            //        user = (User)responseUser.Data;

            //        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
            //        if (responseUSList.IsSuccess)
            //        {
            //            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();

            //            imgQRCode.ImageUrl = googleAuthUserSecurity.Url;
            //            lblManualEntryKey.Text = googleAuthUserSecurity.Value;

            //            googleAuthRebindButton.Visible = false;
            //            googleAuthBindCode.Visible = true;

            //            UserLogMain ulm = new UserLogMain()
            //            {
            //                Description = "Requested rebind for Google Authenticator",
            //                TableName = "user_securities",
            //                UpdatedDate = DateTime.Now,
            //                UserId = user.Id,
            //                UserAccountId = 0,
            //            };
            //            Response responseLog = IUserLogMainService.PostData(ulm);
            //            if (!responseLog.IsSuccess)
            //            {
            //                //Audit log failed
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //            }


            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('googleAuth');", true);
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            //}
        }

        protected void btnUnbindGoogleAuth_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];

            Response responseGoogleAuthenticator = (Response)UserPolicy.GoogleBinding(new UserPolicy.UserClass { Step = 1, Condition1 = false, User = user });
            try
            {
                if (responseGoogleAuthenticator.IsSuccess)
                {
                    responseUserCenterStep1 = (UserCenterStep)responseGoogleAuthenticator.Data;
                    UserPolicyEnum4 userPolicyEnum4 = (UserPolicyEnum4)Enum.Parse(typeof(UserPolicyEnum4), responseUserCenterStep1.Code);
                    string message = userPolicyEnum4.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        imgQRCode.ImageUrl = responseUserCenterStep1.Url;
                        lblManualEntryKey.Text = responseUserCenterStep1.ManualEntryKey;
                        googleAuthRebindButton.Visible = responseUserCenterStep1.Flag1;
                        googleAuthBindCode.Visible = responseUserCenterStep1.Flag2;
                        Session["isVerified"] = 1;
                        UserSecurity googleAuthUserSecurity = responseUserCenterStep1.UserSecurity;
                        googleAuthUserSecurity.IsVerified = 0;
                        googleAuthUserSecurity.VerificationPin = 0;
                        TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                        tfa.DefaultClockDriftTolerance = TimeSpan.FromMinutes(2);
                        user.UniqueKey = CustomGenerator.GenerateGoogleAuthenticationUniqueKeyForEachUser(user.Username);
                        Response rU = IUserService.UpdateData(user);
                        if (rU.IsSuccess)
                        {
                            var setupInfo = tfa.GenerateSetupCode("eApexIs", user.EmailId, user.UniqueKey, 150, 150);
                            googleAuthUserSecurity.Url = setupInfo.QrCodeSetupImageUrl;
                            googleAuthUserSecurity.Value = setupInfo.ManualEntryKey;

                            Response response2 = IUserSecurityService.UpdateData(googleAuthUserSecurity);
                            googleAuthUserSecurity = (UserSecurity)response2.Data;

                            imgQRCode.ImageUrl = googleAuthUserSecurity.Url;
                            lblManualEntryKey.Text = googleAuthUserSecurity.Value;
                            Response responseUpdate = IUserSecurityService.UpdateData(googleAuthUserSecurity);
                            if (responseUpdate.IsSuccess)
                            {
                                Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
                                if (responseMA.IsSuccess)
                                {
                                    MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                    BindSettings(maHolderReg);
                                    UserLogMain ulm = new UserLogMain()
                                    {
                                        Description = "Unbind Google Authenticator successful",
                                        TableName = "user_securities",
                                        UpdatedDate = DateTime.Now,
                                        UserId = user.Id,
                                        UserAccountId = 0,
                                    };
                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                    }
                                    SendEmail("User Information Update", "Google Authenticator has been unbinded on ");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication succesfully unbinded','');openPopup('googleAuth');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseMA.Message + "\", '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUpdate.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + rU.Message + "\", '');", true);
                        }
                    }
                    else if (message == "EXCEPTION")
                    {

                        responseGoogleAuthenticator.IsSuccess = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseGoogleAuthenticator.Message + "\", '');", true);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + message + "\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseGoogleAuthenticator.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                responseGoogleAuthenticator.IsSuccess = false;
                responseGoogleAuthenticator.Message = ex.Message;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }

            //try
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Do you want to proceed?', '');", true);
            //    User user = (User)Session["user"];
            //    Response responseUser = IUserService.GetSingle(user.Id);
            //    if (responseUser.IsSuccess)
            //    {
            //        user = (User)responseUser.Data;

            //        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
            //        if (responseUSList.IsSuccess)
            //        {
            //            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
            //            googleAuthUserSecurity.IsVerified = 0;
            //            googleAuthUserSecurity.VerificationPin = 0;
            //            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            //            tfa.DefaultClockDriftTolerance = TimeSpan.FromMinutes(2);
            //            user.UniqueKey = CustomGenerator.GenerateGoogleAuthenticationUniqueKeyForEachUser(user.Username);
            //            Response rU = IUserService.UpdateData(user);
            //            if (rU.IsSuccess)
            //            {
            //                var setupInfo = tfa.GenerateSetupCode("eApexIs", user.EmailId, user.UniqueKey, 150, 150);
            //                googleAuthUserSecurity.Url = setupInfo.QrCodeSetupImageUrl;
            //                googleAuthUserSecurity.Value = setupInfo.ManualEntryKey;

            //                Response response2 = IUserSecurityService.UpdateData(googleAuthUserSecurity);
            //                googleAuthUserSecurity = (UserSecurity)response2.Data;

            //                imgQRCode.ImageUrl = googleAuthUserSecurity.Url;
            //                lblManualEntryKey.Text = googleAuthUserSecurity.Value;

            //                Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
            //                if (responseMA.IsSuccess)
            //                {
            //                    MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
            //                    BindSettings(maHolderReg);
            //                }

            //                UserLogMain ulm = new UserLogMain()
            //                {
            //                    Description = "Unbind Google Authenticator successful",
            //                    TableName = "user_securities",
            //                    UpdatedDate = DateTime.Now,
            //                    UserId = user.Id,
            //                    UserAccountId = 0,
            //                };
            //                Response responseLog = IUserLogMainService.PostData(ulm);
            //                if (!responseLog.IsSuccess)
            //                {
            //                    //Audit log failed
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //                }

            //                SendEmail("User Information Update", "Google Authenticator has been unbinded on ");
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication succesfully unbinded','UserSettings.aspx');", true);
            //            }
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            //}
        }

        protected void btnDeleteBank_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];

            Response responseUser = (Response)UserPolicy.DeleteBank(new UserPolicy.UserClass { bankID = hdnBankId.Value, User = user });
            try
            {
                if (responseUser.IsSuccess)
                {
                    responseUserCenterStep1 = (UserCenterStep)responseUser.Data;
                    UserPolicyEnum4 userPolicyEnum4 = (UserPolicyEnum4)Enum.Parse(typeof(UserPolicyEnum4), responseUserCenterStep1.Code);
                    string message = userPolicyEnum4.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Successfully deleted.', '/UserSettings.aspx?isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                    }
                    else if (message == "EXCEPTION")
                    {

                        responseUser.IsSuccess = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');addBank();", true);


                    }
                    else
                    {
                        if (userPolicyEnum4 == UserPolicyEnum4.UA4)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomConfirmationMessage('Confirmation', 'Please unbind the bank before delete.', 'Settings.aspx?AccountNo=" + responseUserCenterStep1.UserAccount.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')&redirectBack=UserSettings.aspx');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                        }

                    }
                }
                else
                {
                    responseUser.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                responseUser.IsSuccess = false;
                responseUser.Message = ex.Message;
                Logger.WriteLog("User Settings Deelete Bank " + ex.Message);

            }

            //string BankId = hdnBankId.Value;
            //if (!string.IsNullOrEmpty(BankId))
            //{
            //    User user = (User)Session["user"];
            //    Response responseHB = IMaHolderBankService.GetSingle(Convert.ToInt32(BankId));
            //    if (responseHB.IsSuccess)
            //    {
            //        MaHolderBank hb = (MaHolderBank)responseHB.Data;
            //        //Check if bank binded to MA.
            //        string accountfilter = " ma_holder_bank_id=" + hb.Id + " and status=1 ";
            //        var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
            //        List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
            //        if (listBanks.Count == 0)
            //        {
            //            //Update bank status.
            //            hb.Status = 8;
            //            hb.UpdatedBy = user.Id;
            //            hb.UpdatedDate = DateTime.Now;
            //            IMaHolderBankService.UpdateData(hb);
            //            UserLogMain ulm = new UserLogMain()
            //            {
            //                Description = "User has deleted Bank details",
            //                TableName = "ma_holder_bank",
            //                UpdatedDate = DateTime.Now,
            //                UserId = user.Id,
            //            };
            //            Response responseLog = IUserLogMainService.PostData(ulm);
            //            if (responseLog.IsSuccess)
            //            {
            //                ulm = (UserLogMain)responseLog.Data;
            //            }
            //            else
            //            {
            //                //Audit log failed
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //            }
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Successfully deleted.', '/UserSettings.aspx?isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
            //        }
            //        else
            //        {
            //            UserAccount uA = user.UserIdUserAccounts.FirstOrDefault(x => x.Id == listBanks.FirstOrDefault().UserAccountId);
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomConfirmationMessage('Confirmation', 'Please unbind the bank before delete.', 'Settings.aspx?AccountNo=" + uA.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')&redirectBack=UserSettings.aspx');", true);
            //        }
            //    }
            //    else
            //    {

            //    }
            //}
        }
    }
}