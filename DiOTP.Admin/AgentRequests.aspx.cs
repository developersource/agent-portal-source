﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AgentRequests : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            LoginRole loginRole = ServicesManager.GetRole(Context);

            StringBuilder filter = new StringBuilder();
            string mainQ = (@"select a.*, u.username, u.email_id, u.mobile_number, u.id_no
                                from ");
            string mainQCount = (@"SELECT count(*) from ");
            filter.Append(@" agent_regs a 
                                join users u on u.id = a.user_id 
                                where a.status=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];

                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.email_id like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.mobile_number like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]) && Request.QueryString["FilterValue"].Trim() != "0")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and a.process_status in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(@" and a.process_status in (2, 29, 3, 39, 4, 49, 5, 59) or (a.process_status = 1 and a.introducer_code = '') ");
                }

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by a.created_date desc");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseList = GenericService.GetDataByQuery(mainQ + filter.ToString(), 0, 0, false, null, false, null, true);
                if (responseList.IsSuccess)
                {
                    var UsersDyn = responseList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.AgentRegInfo> aoRequestList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegInfo>>(responseJSON);
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    aoRequestList.ForEach(x =>
                    {
                        string checkBox = string.Empty;
                        if ((loginRole.isAdminOfficer && x.process_status == 4) || x.process_status == 29 || x.process_status == 39 || x.process_status == 49 || x.process_status == 59)
                        {
                            checkBox = @"<label>" + index + @"</label><br/>";
                        }
                        else if ((loginRole.isAdminOfficer && x.process_status == 1 && x.introducer_code == "") || (loginRole.isAdminOfficer && x.process_status == 2) || (loginRole.isAdminOfficer && x.process_status == 3) || (loginRole.isAdminManager && x.process_status == 4))
                        {
                            checkBox = @"<div class='square single-row'>
                                            <div class='checkbox'>
                                                <input type='checkbox' name='checkRow' class='checkRow' value='" + x.id + @"'" + @" /> <label>" + index + @"</label><br/>
                                            </div>
                                        </div>";
                        }
                        else
                        {
                            checkBox = @"<label>" + index + @"</label><br/>";
                        }
                        asb.Append(@"<tr>
                                    <td class='icheck'>
                                        " + checkBox + @"
                                        <span class='row-status'>" + (x.process_status == 1 ? "<span class='label label-warning'>Recommend</span>" : (x.process_status == 2 ? "<span class='label label-warning'>Pending</span>" : (x.process_status == 3 ? "<span class='label label-warning'>CUTE Passed</span>" : (x.process_status == 49 ? "<span class='label label-danger'>Reject Requested</span>" : (x.process_status == 4 ? "<span class='label label-success'>Approve Requested</span>" : (x.process_status == 59 ? "<span class='label label-danger'>Rejected</span>" : (x.process_status == 5 ? "<span class='label label-success'>Approved</span>" : "-"))))))) + @"</span>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Username</td>
                                                    <td><strong><a href='javascript:;' data-id='" + x.id + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='AgentRequestsDetails.aspx' data-title='View details' data-action='View'>   " + x.username + @"&nbsp;<i class='fa fa-eye'></i></a></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Id No</td>
                                                    <td><strong>" + x.id_no + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><strong>" + x.email_id + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Mobile number</td>
                                                    <td><strong>" + x.mobile_number + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table class='table'>
                                            <tbody>
                                                <tr>
                                                    <td>Introducer</td>
                                                    <td><strong>" + (x.process_status == 1 ? "Management" : x.introducer_code) + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Updated datetime</td>
                                                    <td><strong>" + (x.created_date != null && x.created_date.ToString("yyyy") != "0001" ? x.created_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Remarks</td>
                                                    <td><strong>" + (x.remarks) + @"</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>");
                        index++;
                    });
                    usersTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                               "alert(\"" + responseList.Message + "\");", true);
                }
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                User loginUser = (User)HttpContext.Current.Session["admin"];
                if (loginUser == null)
                {
                    response1.IsSuccess = false;
                    response1.Message = "Session Expired!";
                }
                else
                {
                    LoginRole loginRole = ServicesManager.GetRole(HttpContext.Current);
                    if (!loginRole.isAdminManager && action == "Approve")
                    {
                        response1.IsSuccess = false;
                        response1.Message = "Admin Manager only can Approve!";
                    }
                    else
                    {
                        string idString = String.Join(",", ids);
                        string queryAReg = (@" SELECT * FROM agent_regs where id in (" + idString + ") ");
                        Response responseARegList = GenericService.GetDataByQuery(queryAReg, 0, 0, false, null, false, null, true);
                        if (responseARegList.IsSuccess)
                        {
                            var agentRegsDyn = responseARegList.Data;
                            var responseJSON = JsonConvert.SerializeObject(agentRegsDyn);
                            List<DiOTP.Utility.AgentReg> agentRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseJSON);



                            if (action == "Approve")
                            {
                                //GENERATE AGENT CODE
                                string agentCode = "";

                                string queryGetCode = @"select agent_code from users where agent_code !='' order by agent_code desc limit 1";
                                Response responseACList = GenericService.GetDataByQuery(queryGetCode, 0, 0, false, null, false, null, false);
                                if (responseACList.IsSuccess)
                                {
                                    var agentCodeDyns = responseACList.Data;
                                    var responseJSONAC = JsonConvert.SerializeObject(agentCodeDyns);
                                    List<dynamic> agentCodeDynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSONAC);
                                    dynamic agentCodeDyn = agentCodeDynamic.First();
                                    string lastAgentCode = agentCodeDyn.AgentCode;

                                    agentCode = "AG" + (Convert.ToInt32(lastAgentCode.Substring(2)) + 1).ToString().PadLeft(4, '0');
                                }
                                rejectReason = agentCode;

                                string queryAExists = (@" SELECT * FROM users where agent_code in ('" + rejectReason + "') ");
                                Response responseAExistsList = GenericService.GetDataByQuery(queryAExists, 0, 0, false, null, false, null, false);
                                if (responseAExistsList.IsSuccess)
                                {
                                    var agentsDyn = responseAExistsList.Data;
                                    var responseAJSON = JsonConvert.SerializeObject(agentsDyn);
                                    List<DiOTP.Utility.User> agents = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.User>>(responseAJSON);
                                    if (agents.Count > 0)
                                    {
                                        response1.IsSuccess = false;
                                        response1.Message = "Agent Code already used.";
                                        responseMsgs.Add(response1.Message);
                                    }
                                    else
                                    {
                                        agentRegs.ForEach(x =>
                                        {
                                            if (x.process_status == 4)
                                            {
                                                x.process_status = 5;
                                                x.agent_code = rejectReason;
                                                x.remarks = rejectReason;

                                                //agentReg.agent_code = rejectReason;
                                                Response resUpdate = GenericService.UpdateData<AgentReg>(x);
                                                if (resUpdate.IsSuccess)
                                                {
                                                    AgentPosition agentPosition = new AgentPosition();
                                                    agentPosition.Upline1 = x.introducer_code == "" ? "MG" : x.introducer_code;
                                                    agentPosition.AgentCode = x.agent_code;
                                                    agentPosition.IsActive = 1;
                                                    agentPosition.Status = 1;
                                                    agentPosition.CreatedDate = DateTime.Now;

                                                    string queryUpline = @"select * from agent_positions where agent_code in ('" + (x.introducer_code == "" ? "MG" : x.introducer_code) + "', '" + rejectReason + "') ";
                                                    Response responseUplineList = GenericService.GetDataByQuery(queryUpline, 0, 0, false, null, false, null, false);
                                                    if (responseUplineList.IsSuccess)
                                                    {
                                                        var agentUplineDyn = responseUplineList.Data;
                                                        var responseUplineJSON = JsonConvert.SerializeObject(agentUplineDyn);
                                                        List<AgentPosition> agentUplines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseUplineJSON);                                                        
                                                        if (agentUplines.Where(a => a.AgentCode == (x.introducer_code == "" ? "MG" : x.introducer_code)).Count() == 1)
                                                        {
                                                            agentPosition.Upline2 = agentUplines.First(a => a.AgentCode == (x.introducer_code == "" ? "MG" : x.introducer_code)).Upline1;
                                                            agentPosition.Upline3 = agentUplines.First(a => a.AgentCode == (x.introducer_code == "" ? "MG" : x.introducer_code)).Upline2;
                                                        }
                                                        if (agentUplines.Where(a => a.AgentCode == x.agent_code).Count() == 1)
                                                        {
                                                            agentPosition.Id = agentUplines.First(a => a.AgentCode == x.agent_code).Id;
                                                        }
                                                        Response resUpdateAgentPos = GenericService.UpdateData<AgentReg>(x);

                                                        //checking if agent has recommendation or not
                                                        string queryAgentChild;
                                                        if (String.IsNullOrEmpty(x.introducer_code))
                                                        {
                                                            //If no agent recommendation, default AAA position
                                                            queryAgentChild = "SELECT * FROM di_otp_ag.agent_positions where  agent_pos NOT like '____%' order by agent_pos desc limit 1";
                                                        }
                                                        else {
                                                            queryAgentChild = "SELECT * FROM di_otp_ag.agent_positions where agent_pos != '" + agentUplines.Where(a => a.AgentCode == x.introducer_code).FirstOrDefault().AgentPos + "' and agent_pos like '" + agentUplines.Where(a => a.AgentCode == x.introducer_code).FirstOrDefault().AgentPos + "%' and length(agent_pos) ='"+ (Convert.ToInt32(agentUplines.Where(a => a.AgentCode == x.introducer_code).FirstOrDefault().AgentPos.Length)+ 3) + "' order by agent_pos desc limit 1";
                                                        }
                                                        //Generate Agent Position
                                                        Response responseAgentChild = GenericService.GetDataByQuery(queryAgentChild, 0, 0, false, null, false, null, false);
                                                        if (responseAgentChild.IsSuccess) {
                                                            var agentChildDyn = responseAgentChild.Data;
                                                            var responseAgentChildJSON = JsonConvert.SerializeObject(agentChildDyn);
                                                            List<AgentPosition> agentChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentChildJSON);

                                                            string newAgentPosition = GenerateAgentPos(agentChild.Count == 0 ? agentUplines.Where(a => a.AgentCode == x.introducer_code).FirstOrDefault().AgentPos : agentChild.FirstOrDefault().AgentPos, agentChild.Count);
                                                            agentPosition.AgentPos = newAgentPosition;

                                                        }
                                                        if (agentPosition.Id == 0)
                                                        {
                                                            resUpdateAgentPos = GenericService.PostData<AgentPosition>(agentPosition);
                                                        }
                                                        else
                                                        {
                                                            resUpdateAgentPos = GenericService.UpdateData<AgentPosition>(agentPosition);
                                                        }
                                                        if (resUpdateAgentPos.IsSuccess)
                                                        {
                                                            Response responsePrimary = IUserService.GetSingle(x.user_id);
                                                            if (responsePrimary.IsSuccess)
                                                            {
                                                                User user = (User)responsePrimary.Data;
                                                                user.IsAgent = 1;
                                                                user.AgentCode = rejectReason;
                                                                user.AgentStatus = 1;
                                                                Response resUserUpdate = IUserService.UpdateData(user);
                                                                if (resUserUpdate.IsSuccess)
                                                                {
                                                                    UserType userType = new UserType
                                                                    {
                                                                        UserId = user.Id,
                                                                        UserTypeId = 6,
                                                                        IsVerified = 1,
                                                                        Status = 1
                                                                    };
                                                                    Response resUserTypePost = IUserTypeService.PostData(userType);
                                                                    if (resUserTypePost.IsSuccess)
                                                                    {
                                                                        response1.IsSuccess = true;
                                                                        responseMsgs.Add(user.Username + " Agent application approved.");
                                                                    }
                                                                    else
                                                                    {
                                                                        response1.IsSuccess = false;
                                                                        responseMsgs.Add(user.Username + ": " + resUserTypePost.Message);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    response1.IsSuccess = false;
                                                                    responseMsgs.Add(user.Username + ": " + resUserUpdate.Message);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response1.IsSuccess = false;
                                                                responseMsgs.Add(responsePrimary.Message);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            response1.IsSuccess = false;
                                                            responseMsgs.Add(resUpdateAgentPos.Message);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        response1.IsSuccess = false;
                                                        responseMsgs.Add(responseUplineList.Message);
                                                    }
                                                }
                                                else
                                                {
                                                    response1.IsSuccess = false;
                                                    responseMsgs.Add(resUpdate.Message);
                                                }
                                            }
                                            else
                                            {
                                                response1.IsSuccess = false;
                                                responseMsgs.Add("Cannot approve");
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    response1.IsSuccess = false;
                                    responseMsgs.Add(responseAExistsList.Message);
                                }
                            }
                            if (action == "Recommend")
                            {
                                agentRegs.ForEach(x =>
                                {
                                    if (x.process_status == 1)
                                    {
                                        x.process_status = 2;
                                        x.recommendation = rejectReason;
                                        x.recommendation_date = DateTime.Now;
                                        x.updated_by = loginUser.Id;
                                        Response resUpdate = GenericService.UpdateData<AgentReg>(x);
                                        if (resUpdate.IsSuccess)
                                        {
                                            Response responsePrimary = IUserService.GetSingle(x.user_id);
                                            if (responsePrimary.IsSuccess)
                                            {
                                                User user = (User)responsePrimary.Data;
                                                response1.IsSuccess = true;
                                                responseMsgs.Add(user.Username + " Agent application recommended.");
                                            }
                                            else
                                            {
                                                response1.IsSuccess = false;
                                                responseMsgs.Add(responsePrimary.Message);
                                            }
                                        }
                                        else
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add(resUpdate.Message);
                                        }
                                    }
                                    else
                                    {
                                        response1.IsSuccess = false;
                                        responseMsgs.Add("Cannot approve");
                                    }
                                });
                            }
                            if (action == "ResendLink")
                            {
                                agentRegs.ForEach(x =>
                                {
                                    if (x.process_status == 3)
                                    {
                                        x.process_status = 22;
                                        x.remarks = rejectReason;

                                    }
                                    Response resUpdate = GenericService.UpdateData<AgentReg>(x);
                                    if (resUpdate.IsSuccess)
                                    {

                                    }
                                    else
                                    {
                                        response1.IsSuccess = false;
                                        responseMsgs.Add(resUpdate.Message);
                                    }
                                });
                            }
                            if (action == "Reject")
                            {
                                if (!loginRole.isAdminManager)
                                {
                                    agentRegs.ForEach(x =>
                                    {
                                        if (x.process_status == 1 || x.process_status == 2 || x.process_status == 3)
                                        {
                                            x.process_status = 49;
                                            x.remarks = rejectReason;

                                            //agentReg.agent_code = rejectReason;
                                            Response resUpdate = GenericService.UpdateData<AgentReg>(x);
                                            if (resUpdate.IsSuccess)
                                            {
                                                responseMsgs.Add(x.id + " Agent application reject requested.");
                                            }
                                            else
                                            {
                                                response1.IsSuccess = false;
                                                responseMsgs.Add(resUpdate.Message);
                                            }
                                        }
                                        else if (x.process_status == 4)
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("Agent application has already requested for approval.");
                                        }
                                        else if (x.process_status == 5)
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("Agent application already approved.");
                                        }
                                        else if (x.process_status == 49)
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("Agent application has already requested for rejection.");
                                        }
                                        else if (x.process_status == 59)
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("Agent application already rejected.");
                                        }
                                        else
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("Cannot reject.");
                                        }
                                    });
                                }
                                else
                                {
                                    agentRegs.ForEach(x =>
                                    {
                                        if (x.process_status == 49)
                                        {
                                            x.process_status = 5;
                                            x.remarks = rejectReason;

                                            //agentReg.agent_code = rejectReason;
                                            Response resUpdate = GenericService.UpdateData<AgentReg>(x);
                                            if (resUpdate.IsSuccess)
                                            {
                                                responseMsgs.Add(x.id + " Agent application rejected.");
                                            }
                                            else
                                            {
                                                response1.IsSuccess = false;
                                                responseMsgs.Add(resUpdate.Message);
                                            }
                                        }
                                        else
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("Please request reject request first");
                                        }
                                    });
                                }
                            }
                        }
                        else
                        {
                            response1.IsSuccess = false;
                            responseMsgs.Add(responseARegList.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add("Action: " + ex.Message + " - Exception");
                Console.WriteLine("Account Opening action: " + ex.Message);
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(AgentFIMMDetails obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            try
            {
                User user = new User();
                User userDB = new User();
                if (obj.Id != 0)
                {
                    AgentReg agentReg = new AgentReg();
                    AgentRegExam agentRegExamExisting = new AgentRegExam();
                    AgentRegFimm agentRegFimmExisting = new AgentRegFimm();
                    bool isExamPassed = false;
                    string queryARegAutofill = (@" SELECT * FROM agent_regs where id=" + obj.Id + " ");
                    Response responseARegAutofillList = GenericService.GetDataByQuery(queryARegAutofill, 0, 0, false, null, false, null, true);
                    if (responseARegAutofillList.IsSuccess)
                    {
                        var agentsDyn = responseARegAutofillList.Data;
                        var responseARegJSON = JsonConvert.SerializeObject(agentsDyn);
                        List<AgentReg> aRegList = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentReg>>(responseARegJSON);
                        if (aRegList.Count == 1)
                        {
                            agentReg = aRegList.FirstOrDefault();
                            if (agentReg.is_new_agent == 1)
                            {
                                string queryAgentRegExam = (@" select * from agent_reg_exams where agent_reg_id=" + agentReg.id + " and is_enroll=1 and status=1 ");
                                Response responseAgentRegExamList = GenericService.GetDataByQuery(queryAgentRegExam, 0, 0, false, null, false, null, true);
                                response = responseAgentRegExamList;
                                if (responseAgentRegExamList.IsSuccess)
                                {
                                    var agentregExamDyn = responseAgentRegExamList.Data;
                                    var responseAgentRegExamsJSON = JsonConvert.SerializeObject(agentregExamDyn);
                                    List<AgentRegExam> agentRegExams = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegExam>>(responseAgentRegExamsJSON);
                                    StringBuilder stringBuilder = new StringBuilder();
                                    if (agentRegExams.Count == 1)
                                    {
                                        agentRegExamExisting = agentRegExams.FirstOrDefault();
                                        isExamPassed = (agentRegExamExisting.result == "Pass");
                                    }
                                }
                            }
                            if (agentReg.is_new_agent == 0 || isExamPassed)
                            {
                                string queryAgentRegFiMM = (@" select * from agent_reg_fimms where agent_reg_id=" + agentReg.id + " and status=1 ");
                                Response responseAgentRegFiMMList = GenericService.GetDataByQuery(queryAgentRegFiMM, 0, 0, false, null, false, null, true);
                                response = responseAgentRegFiMMList;
                                if (responseAgentRegFiMMList.IsSuccess)
                                {
                                    var agentregFiMMDyn = responseAgentRegFiMMList.Data;
                                    var responseAgentRegFiMMsJSON = JsonConvert.SerializeObject(agentregFiMMDyn);
                                    List<AgentRegFimm> agentRegFiMMs = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegFimm>>(responseAgentRegFiMMsJSON);
                                    if (agentRegFiMMs.Count == 1)
                                    {
                                        agentRegFimmExisting = agentRegFiMMs.FirstOrDefault();
                                    }
                                    response = responseAgentRegFiMMList;
                                }
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Something went wrong";
                        }
                    }
                    else
                    {
                        response = responseARegAutofillList;
                    }
                    if (response.IsSuccess)
                    {
                        Response responsePost = new Response();
                        if (agentReg.is_new_agent == 0 || isExamPassed)
                        {
                            if (//obj.txtNewCardRecievedOn == "" || 
                                obj.txtFIMMNo == "" || obj.txtApprovalDate == "" || obj.txtExpiryDate == "" || obj.txtRemarks == ""
                                //|| obj.rdnRenewalApplicaion == "" || obj.txtRenewalCardRecievedOn == "" || obj.txtFIMMMICRCode == "" || obj.txtFIMMBankName == ""
                                //|| obj.txtFIMMChequeno == "" || obj.txtFIMMTransRefno == "" || obj.txtFIMMAmount == "" || obj.txtFIMMDateReceived == ""
                                //|| obj.txtFIMMPaymentRefno == "" || obj.txtFIMMDepositAccountNo == "" || obj.txtToFiMMWithAccNo == "" || obj.txtToFiMMMICRCode == ""
                                //|| obj.txtToFiMMBankName == "" || obj.txtToFiMMChequeno == "" || obj.txtToFiMMDateSent == "" || obj.txtFiMMRenewalAmount == ""
                                //|| obj.txtUTMCRenewalAmount == ""
                                )
                            {
                                response.IsSuccess = false;
                                response.Message = "Please fill in the details";
                                return response;
                            }
                            AgentRegFimm agentRegFimm = new AgentRegFimm
                            {
                                id = agentRegFimmExisting.id,
                                agent_reg_id = agentReg.id,
                                //new_card_received_on = DateTime.ParseExact(obj.txtNewCardRecievedOn, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                                FIMM_no = obj.txtFIMMNo,
                                approval_date = DateTime.ParseExact(obj.txtApprovalDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                                expiry_date = DateTime.ParseExact(obj.txtExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                                renewal_card_received_on = default(DateTime),
                                from_agent_date_received = default(DateTime),
                                to_FiMM_date_sent = default(DateTime),
                                is_renewal = 0,
                                from_agent_amount = 0,
                                to_FiMM_FIMM_renewal_amount = 0,
                                to_FiMM_UTMC_renewal_amount = 0,
                                remarks = obj.txtRemarks,
                                //is_renewal = Convert.ToInt32(obj.rdnRenewalApplicaion),
                                //renewal_card_received_on = DateTime.ParseExact(obj.txtRenewalCardRecievedOn, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                                //from_agent_MICR_code = obj.txtFIMMMICRCode,
                                //from_agent_bank_name = obj.txtFIMMBankName,
                                //from_agent_cheque_no = obj.txtFIMMChequeno,
                                //from_agent_trans_ref_no = obj.txtFIMMTransRefno,
                                //from_agent_amount = Convert.ToDecimal(obj.txtFIMMAmount),
                                //from_agent_date_received = DateTime.ParseExact(obj.txtFIMMDateReceived, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                                //from_agent_payment_ref_no = obj.txtFIMMPaymentRefno,
                                //from_agent_deposit_acc_no = obj.txtFIMMDepositAccountNo,
                                //to_FiMM_with_acc_no = obj.txtToFiMMWithAccNo,
                                //to_FiMM_MICR_code = obj.txtToFiMMMICRCode,
                                //to_FiMM_bank_name = obj.txtToFiMMBankName,
                                //to_FiMM_cheque_no = obj.txtToFiMMChequeno,
                                //to_FiMM_date_sent = DateTime.ParseExact(obj.txtToFiMMDateSent, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                                //to_FiMM_FIMM_renewal_amount = Convert.ToDecimal(obj.txtFiMMRenewalAmount),
                                //to_FiMM_UTMC_renewal_amount = Convert.ToDecimal(obj.txtUTMCRenewalAmount),
                                is_active = 1,
                                created_by = loginUser.Id,
                                created_date = DateTime.Now,
                                status = 1
                            };
                            if (agentRegFimm.id == 0)
                                responsePost = GenericService.PostData<AgentRegFimm>(agentRegFimm);
                            else
                                responsePost = GenericService.UpdateData<AgentRegFimm>(agentRegFimm);
                            if (responsePost.IsSuccess)
                            {
                                agentReg.process_status = 4;
                                responsePost = GenericService.UpdateData<AgentReg>(agentReg);
                                response.Message = "Updated Status Successfully";
                            }
                        }
                        else
                        {
                            string mainQ = (@"SELECT id,session_id,language,location,exam_datetime,is_active,created_date,created_by,updated_date,updated_by,status,group_concat(exam_datetime) as exam_datetimes FROM agent_exam_schedules where is_active=1 and status=1 and id=" + (obj.ddlTime != null ? obj.ddlTime : agentRegExamExisting.id.ToString()) + " ");
                            Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 30, false, null, false, null, true);
                            if (responseUList.IsSuccess)
                            {
                                var examsDyn = responseUList.Data;
                                var responseexamsDynJSON = JsonConvert.SerializeObject(examsDyn);
                                List<DiOTP.Utility.CustomAgentExamSchedule> agentExamSchedules = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CustomAgentExamSchedule>>(responseexamsDynJSON);
                                CustomAgentExamSchedule customAgentExamSchedule = agentExamSchedules.FirstOrDefault();
                                AgentRegExam agentRegExam = new AgentRegExam
                                {
                                    id = agentRegExamExisting.id,
                                    is_enroll = 1,
                                    is_prefer = agentRegExamExisting.is_prefer,
                                    //session_id = obj.ddlSessionId.ToString(),
                                    agent_reg_id = agentReg.id,
                                    language = customAgentExamSchedule.language,
                                    location = customAgentExamSchedule.location,
                                    exam_date = customAgentExamSchedule.exam_datetime,
                                    is_first_time = 1,
                                    resit_date = default(DateTime),
                                    result = obj.ddlResult,
                                    is_active = 1,
                                    created_by = user.Id,
                                    created_date = DateTime.Now,
                                    status = 1
                                };
                                if (agentRegExam.id == 0)
                                    responsePost = GenericService.PostData<AgentRegExam>(agentRegExam);
                                else
                                    responsePost = GenericService.UpdateData<AgentRegExam>(agentRegExam);
                                if (responsePost.IsSuccess)
                                {
                                    if (obj.ddlResult == "Pass")
                                        agentReg.process_status = 3;
                                    responsePost = GenericService.UpdateData<AgentReg>(agentReg);
                                }
                            }
                            else
                                response = responseUList;
                            if (responsePost.IsSuccess)
                            {
                                response.IsSuccess = true;
                                response.Message = "Updated Successfully";
                            }
                            else
                                response = responsePost;
                        }
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid record id";
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add admin action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        public static string GenerateAgentPos(string currPos, int childCount)
        {
            string genPos = "";
            char[] posIndex = currPos.ToCharArray();

            //If parent has child, take the last child
            if (childCount == 1)
            {
                if (posIndex[posIndex.Length - 1] != 'Z')
                {
                    posIndex[posIndex.Length - 1]++;
                    string generated = new string(posIndex);
                    genPos = generated;
                }
                else
                {
                    if (posIndex[posIndex.Length - 2] != 'Z')
                    {
                        posIndex[posIndex.Length - 2]++;
                        string generated = new string(posIndex);
                        genPos = generated;
                    }
                    else
                    {
                        if (posIndex[posIndex.Length - 3] != 'Z')
                        {
                            posIndex[posIndex.Length - 3]++;
                            string generated = new string(posIndex);
                            genPos = generated;
                        }
                        else
                        {
                            Console.WriteLine("Maximum agent Position achieved");
                        }
                    }
                }
            }
            else
            {
                genPos = currPos + "AAA";
            }

            return genPos;
        }

    }


    public class AgentFIMMDetails
    {
        public Int32 Id { get; set; }
        public string ddlLocation { get; set; }
        public string ddlLanguage { get; set; }
        public string ddlExamDate { get; set; }
        public string ddlTime { get; set; }
        public string ddlResult { get; set; }
        //public Int32 ddlSessionId { get; set; }
        //public string txtNewCardRecievedOn { get; set; }
        public string txtFIMMNo { get; set; }
        public string txtApprovalDate { get; set; }
        public string txtExpiryDate { get; set; }
        public string txtRemarks { get; set; }
        //public string rdnRenewalApplicaion { get; set; }
        //public string txtRenewalCardRecievedOn { get; set; }
        //public string txtFIMMMICRCode { get; set; }
        //public string txtFIMMBankName { get; set; }
        //public string txtFIMMChequeno { get; set; }
        //public string txtFIMMTransRefno { get; set; }
        //public string txtFIMMAmount { get; set; }
        //public string txtFIMMDateReceived { get; set; }
        //public string txtFIMMPaymentRefno { get; set; }
        //public string txtFIMMDepositAccountNo { get; set; }
        //public string txtToFiMMWithAccNo { get; set; }
        //public string txtToFiMMMICRCode { get; set; }
        //public string txtToFiMMBankName { get; set; }
        //public string txtToFiMMChequeno { get; set; }
        //public string txtToFiMMDateSent { get; set; }
        //public string txtFiMMRenewalAmount { get; set; }
        //public string txtUTMCRenewalAmount { get; set; }
    }
}