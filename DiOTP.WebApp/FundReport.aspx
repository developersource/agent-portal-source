﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="FundReport.aspx.cs" Inherits="DiOTP.WebApp.FundReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        #accountSelectedDropdown {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Fund Report</li>
                        </ol>
                    </div>

                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Fund Report</h3>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="statement hide">
                                <h5 class="hidden-lg hidden-md">Generate below for selected account:</h5>
                                <div class="row">
                                    <div class="col-md-3 mb-8">
                                        <label>Account Number:</label>
                                        <asp:DropDownList ID="ddlFundAccount" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-2 mb-8">
                                        <asp:Button ID="btnGenerate" runat="server" Text="Submit" CssClass="btn btn-block btn-infos1 mt-30" OnClick="btnGenerate_Click" />
                                    </div>
                                </div>
                            </div>

                            <table class="table table-font-size-13 table-cell-pad-5">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <%--<th>MasterAccount No</th>--%>
                                        <th>Fund Name</th>
                                        <th>Annual Report</th>
                                    </tr>
                                </thead>
                                <tbody id="fundReportTbody" runat="server">
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script>
        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });
    </script>
</asp:Content>
