﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.Helper
{
    public static class SMSService
    {
        public static string SendSMS(string toMobileNumber, string mobileVerificationPin, string title)
        {
            if(toMobileNumber.Contains(" "))
            {
                toMobileNumber = toMobileNumber.Replace(" ", "");
            }
            if (toMobileNumber.Contains("-"))
            {
                toMobileNumber = toMobileNumber.Replace("-", "");
            }
            string company = ConfigurationManager.AppSettings["company"].ToString();
            string username = ConfigurationManager.AppSettings["username"].ToString();
            string password = ConfigurationManager.AppSettings["password"].ToString();
            string fromMobileNumber = ConfigurationManager.AppSettings["fromMobileNumber"].ToString();
            string messageContent = ConfigurationManager.AppSettings["messageContentMA"].ToString();
            string messagedate = DateTime.Now.ToString();
            messageContent = messageContent.Replace("######", mobileVerificationPin);
            messageContent = messageContent.Replace("[date]", messagedate);
            messageContent = messageContent.Replace("[title]", title);

            // Create ViaNettSMS object with username and password
            ViaNettSMS s = new ViaNettSMS(username, password, company);
            // Declare Result object returned by the SendSMS function
            ViaNettSMS.Result result;
            try
            {
                // Send SMS through HTTP API
                result = s.sendSMS(fromMobileNumber, toMobileNumber, messageContent);
                // Show Send SMS response
                if (result.IsSuccess)
                    return result.Message;
                else
                    return result.Message;
            }
            catch (System.Net.WebException ex)
            {
                return ex.Message;
                
                //Catch error occurred while connecting to server.
                
            }
        }
    }
}
