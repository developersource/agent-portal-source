﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAdmin.aspx.cs" Inherits="Admin.AddAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="addAdminForm" runat="server">
        <div class="row" id="FormId" data-value="addAdminForm">
            <div class="col-md-12">

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">User Type:</label>
                    </div>
                    <div class="col-md-6">
                        <select id="LoginAttempts" name="LoginAttempts" runat="server" clientidmode="static" class="form-control">
                            <option value="0">Select</option>
                            <option value="3">Main Admin</option>
                            <option value="4">Account Admin</option>
                            <option value="5">Content Admin</option>
                            <option value="10">Admin Officer</option>
                            <option value="11">Admin Manager</option>
                        </select>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Username:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                        <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                        <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                        <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                        <input type="hidden" name="ModifiedBy" value="0" id="ModifiedBy" runat="server" clientidmode="static" />
                        <input type="hidden" name="ModifiedDate" value="0" id="ModifiedDate" runat="server" clientidmode="static" />
                        <input type="hidden" name="UserRoleId" value="2" id="UserRoleId" runat="server" clientidmode="static" />

                        <input type="hidden" name="Password" id="Password" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="TransPwd" id="TransPwd" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="UniqueKey" id="UniqueKey" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="IsOnline" value="0" id="IsOnline" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsActive" value="0" id="IsActive" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsPrimary" value="0" id="IsPrimary" runat="server" clientidmode="static" />
                        <input type="hidden" name="RegisterIp" value="0" id="RegisterIp" runat="server" clientidmode="static" />
                        <input type="hidden" name="LastLoginDate" value="" id="LastLoginOn" runat="server" clientidmode="static" />
                        <input type="hidden" name="LastLoginIp" value="0" id="LastLoginIp" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsSatChecked" value="0" id="IsSatChecked" runat="server" clientidmode="static" />
                        <input type="hidden" name="VerificationCode" value="0" id="VerificationCode" runat="server" clientidmode="static" />

                        <input type="text" name="Username" id="Username" runat="server" clientidmode="static" class="form-control" placeholder="Enter Username" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Email:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="EmailId" id="EmailId" runat="server" clientidmode="static" class="form-control" placeholder="Enter Email" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Mobile number:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="MobileNumber" id="MobileNumber" runat="server" clientidmode="static" class="form-control" placeholder="Enter Mobile number" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>
