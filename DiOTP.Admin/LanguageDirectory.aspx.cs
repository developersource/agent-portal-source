﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using KellermanSoftware.CompareNetObjects;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class LanguageDirectory : System.Web.UI.Page
    {
        private static readonly Lazy<ILanguageDirService> lazyLanguageDirService = new Lazy<ILanguageDirService>(() => new LanguageDirService());

        public static ILanguageDirService ILanguageDirService { get { return lazyLanguageDirService.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append(" 1=1 ");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameTextPage = Converter.GetColumnNameByPropertyName<LanguageDir>(nameof(LanguageDir.TextPage));
                    filter.Append(" and " + columnNameTextPage + " like '%" + Search.Value.Trim() + "%'");
                }
                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = ILanguageDirService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseLDList = ILanguageDirService.GetDataByFilter(filter.ToString(), skip, take, true);
                if (responseLDList.IsSuccess)
                {
                    List<LanguageDir> languageDirectory = (List<LanguageDir>)responseLDList.Data;

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (LanguageDir l in languageDirectory)
                    {
                        asb.Append(@"<tr>
                                <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + l.Id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (l.Status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                                </td>
                                <td>" + l.Module + @"</td>
                                <td>" + l.TextPage + @"</td>
                                <td>" + l.TextEn + @"</td>
                                <td>" + l.TextMs + @"</td>
                                <td>" + l.TextZh + @"</td>
                                <td>" + l.Remarks + @"</td>
                            </tr>");
                        index++;
                    }
                    langDirTableBody.InnerHtml = asb.ToString();
                }
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            try
            {
                string idString = String.Join(",", ids);
                Response responseLDList = ILanguageDirService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseLDList.IsSuccess)
                {
                    List<LanguageDir> languageDirs = (List<LanguageDir>)responseLDList.Data;
                    if (action == "Deactivate")
                    {
                        languageDirs.ForEach(x =>
                        {
                            x.Status = 0;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Language content with " + x.Id + " has been activated",
                                TableName = "language_dirs",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        ILanguageDirService.UpdateBulkData(languageDirs);
                    }
                    if (action == "Activate")
                    {
                        languageDirs.ForEach(x =>
                        {
                            x.Status = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Language content with " + x.Id + " has been deactivated",
                                TableName = "language_dirs",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        ILanguageDirService.UpdateBulkData(languageDirs);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("User accounts action: " + ex.Message);
                return false;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(LanguageDir obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            if (obj.Module == 5)
            {
                response.IsSuccess = false;
                response.Message = "Select the Module";
                return response;
            }
            if (obj.TextEn == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the English Text";
                return response;
            }
            if (obj.TextMs == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Malay Text";
                return response;
            }
            if (obj.TextZh == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Chinese Text";
                return response;
            }

            try
            {
                if (obj.Module != 0)
                {
                    obj.TextId = "1";
                }
                if (obj.Id == 0)
                {
                    ILanguageDirService.PostData(obj);
                    response.IsSuccess = true;
                    response.Message = "Success";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Language content successfully added",
                        TableName = "language_dirs",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }
                    
                    //Audit Log Ends here
                }
                else
                {
                    ILanguageDirService.UpdateData(obj);
                    response.IsSuccess = true;
                    response.Message = "Success";
                    //Audit Log starts here
                    Response responseEg1 = ILanguageDirService.GetDataByPropertyName(nameof(LanguageDir.TextEn), obj.TextEn, true, 0, 0, false);
                    List<LanguageDir> language = new List<LanguageDir>();
                    language = (List<LanguageDir>)responseEg1.Data;
                    
                    foreach (LanguageDir x in language){
                        if (x.TextMs != obj.TextMs) {
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Language content with " + obj.Id + ", " + x.TextMs + " has been updated to " + obj.TextMs,
                                TableName = "language_dirs",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "text_ms",
                                ValueOld = x.TextMs,
                                ValueNew = obj.TextMs,
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                        }
                        else if (x.TextZh != obj.TextZh) {
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Language content with " + obj.Id + ", " + x.TextZh + " has been updated to " + obj.TextZh,
                                TableName = "language_dirs",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "text_zh",
                                ValueOld = x.TextZh,
                                ValueNew = obj.TextZh,
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                        }
                    }
                    
                    //Audit Log Ends here
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add language dir action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object UpdateLanguageDirJSON()
        {
            Response response = new Response();
            try
            {
                Response responseLDList = ILanguageDirService.GetDataByFilter(" status='1'", 0, 0, false);
                if (responseLDList.IsSuccess)
                {
                    List<LanguageDir> languageDirs = (List<LanguageDir>)responseLDList.Data;
                    string json = JsonConvert.SerializeObject(languageDirs);

                    string path = ConfigurationManager.AppSettings["dir_path"].ToString();
                    DirectoryInfo parentDir = new DirectoryInfo(path);

                    string langDirJsonPath = path + ("\\\\language\\\\langDir.json");

                    File.WriteAllText(langDirJsonPath, json);
                    response.IsSuccess = true;
                }
            }
            catch(Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}