﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("uts.holder_ledger")]
    public class HolderLedger
    {
        [Database("HOLDER_NO"), Display("HolderNo")]
        public int HolderNo { get; set; }
        [Database("ID_NO"), Display("IDNO")]
        public string IDNO { get; set; }
        [Database("FUND_ID"), Display("FundID")]
        public string FundID { get; set; }
        [Database("TRANS_NO"), Display("TransNO")]
        public string TransNO { get; set; }
        [Database("TRANS_TYPE"), Display("TransType")]
        public string TransType { get; set; }
        [Database("DIV_IND"), Display("DivInd")]
        public string DivInd { get; set; }
        [Database("ENTER_DT"), Display("EnterDt")]
        public DateTime? EnterDt { get; set; }
        [Database("ENTER_TIME"), Display("EnterTime")]
        public DateTime? EnterTime { get; set; }
        [Database("ENTRY_DT"), Display("EntryDt")]
        public DateTime? EntryDt { get; set; }
        [Database("ENTRY_TIME"), Display("EntryTime")]
        public DateTime? EntryTime { get; set; }
        [Database("ENTRY_BY"), Display("EntryBy")]
        public string EntryBy { get; set; }
        [Database("ENTRY_TERM"), Display("EntryTerm")]
        public string EntryTerm { get; set; }
        [Database("TRANS_DT"), Display("TransDt")]
        public DateTime? TransDt { get; set; }
        [Database("TRANS_UNITS"), Display("TransUnits")]
        public Decimal? TransUnits { get; set; }
        [Database("TRANS_AMT"), Display("TransAmt")]
        public Decimal? TransAmt { get; set; }
        [Database("TRANS_PR"), Display("TransPr")]
        public Decimal? TransPr { get; set; }
        [Database("CUR_UNIT_HLDG"), Display("CurUnitHLDG")]
        public Decimal? CurUnitHLDG { get; set; }
        [Database("CUM_VALUE_HLDG"), Display("CUMValueHLDG")]
        public Decimal? CUMValueHLDG { get; set; }
        [Database("CUM_FEE"), Display("CUM_FEE")]
        public Decimal? CUMFee { get; set; }
        [Database("OTHER_CHARGES"), Display("OtherCharges")]
        public Decimal? OtherCharges { get; set; }
        [Database("BANK_CHARGES"), Display("BankCharges")]
        public Decimal? BankCharges { get; set; }
        [Database("STAMP_DUTY"), Display("StampDuty")]
        public Decimal? StampDuty { get; set; }
        [Database("CERT_FEE"), Display("CERT_FEE")]
        public Decimal? CERTFee { get; set; }
        [Database("IS_IND"), Display("IsIND")]
        public string IsIND { get; set; }
        [Database("OPT_IND"), Display("OptIND")]
        public string OptIND { get; set; }
        [Database("AGENT_ID"), Display("AGENT_ID")]
        public string AgentID { get; set; }
        [Database("AGENT_TYPE"), Display("AgentType")]
        public string AgentType { get; set; }
        [Database("AGENT_CODE"), Display("AgentCode")]
        public string AgentCode { get; set; }
        [Database("REF_NO"), Display("RefNo")]
        public string RefNo { get; set; }
        [Database("ACC_TYPE"), Display("AccType")]
        public string AccType { get; set; }
        [Database("ACC_TYPE_NO"), Display("AccTypeNO")]
        public int? AccTypeNO { get; set; }
        [Database("APP_FEE"), Display("AppFee")]
        public Decimal? AppFee { get; set; }
        [Database("BROKER_FEE"), Display("BrokerFee")]
        public Decimal? BrokerFee { get; set; }
        [Database("EXIT_FEE"), Display("ExitFee")]
        public Decimal? ExitFee { get; set; }
        [Database("COM_FEE"), Display("ComFee")]
        public Decimal? ComFee { get; set; }
        [Database("MGR_AVE_COST_PU"), Display("MGRAveCostPU")]
        public Decimal? MGRAveCostPU { get; set; }
        [Database("HLDR_AVE_COST"), Display("HLDRAveCost")]
        public Decimal? HLDRAveCost { get; set; }
        [Database("HLDR_AVE_PRICE"), Display("HLDRAvePrice")]
        public Decimal? HLDRAvePrice { get; set; }
        [Database("HLDR_AVE_FEE"), Display("HLDRAveFee")]
        public Decimal? HLDRAveFee { get; set; }
        [Database("LAST_AVE_PRICE"), Display("LastAvePrice")]
        public Decimal? LastAvePrice { get; set; }
        [Database("LAST_AVE_FEE"), Display("LastAveFee")]
        public Decimal? LastAveFee { get; set; }
        [Database("BROKER_DISC_RATE"), Display("BrokerDiscRate")]
        public Decimal? BrokerDiscRate { get; set; }
        [Database("COM_DISC_RATE"), Display("ComDiscRate")]
        public Decimal? ComDiscRate { get; set; }
        [Database("SORT_SEQ"), Display("SortSEQ")]
        public int? SortSEQ { get; set; }
        [Database("KWSP_STATUS"), Display("KWSPStatus")]
        public string KWSPStatus { get; set; }
        [Database("SYS_PRODUCT_NO"), Display("SysProductNO")]
        public int? SysProductNO { get; set; }
        [Database("DISBURSE_DT"), Display("DisburseDT")]
        public DateTime? DisburseDT { get; set; }
        [Database("GAIN_LOSS"), Display("GainLoss")]
        public Decimal? GainLoss { get; set; }
        [Database("REF_CLIENT_ID"), Display("RefClientID")]
        public string RefClientID { get; set; }
        [Database("REF_IND"), Display("RefIND")]
        public string RefIND { get; set; }
        [Database("REBATE_PERC"), Display("RebatePerc")]
        public Decimal? RebatePerc { get; set; }
        [Database("REBATE_AMT"), Display("RebateAMT")]
        public Decimal? RebateAMT { get; set; }
        [Database("DOWNLOAD_IND"), Display("DownloadIND")]
        public string DownloadIND { get; set; }
        [Database("FEE_AMT"), Display("FeeAMT")]
        public Decimal? FeeAMT { get; set; }
        [Database("UNIT_VALUE"), Display("UnitValue")]
        public Decimal? UnitValue { get; set; }
        [Database("GST_AMT"), Display("GstAMT")]
        public Decimal? GstAMT { get; set; }
        [Database("FUND_GST_AMT"), Display("FundGstAMT")]
        public Decimal? FundGstAMT { get; set; }
        [Database("DOC_TYPE"), Display("DOC_TYPE")]
        public string DocType { get; set; }
        [Database("DOC_NO"), Display("DocNo")]
        public string DocNo { get; set; }
        [Database("PRINT_IND"), Display("PrintIND")]
        public string PrintIND { get; set; }
        [Database("PYMT_MODE"), Display("PymtMode")]
        public string PymtMode { get; set; }
        [Database("TAX_INVOICE_NO"), Display("TaxInvoiceNo")]
        public string TaxInvoiceNo { get; set; }
        [Database("PYMT_REF"), Display("PymtRef")]
        public string PymtRef { get; set; }
        public decimal CostRM { get; set; }
    }
}
