﻿using DiOTP.Data.IRepo;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class AgentSignupRepo : IAgentSignupRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(AgentSignup obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AgentSignup>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<AgentSignup> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AgentSignup>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(AgentSignup obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AgentSignup>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<AgentSignup> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AgentSignup>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public AgentSignup DeleteData(Int32 Id)
        {
            Response responseULM = GetSingle(Id);
            if (responseULM.IsSuccess)
            {
                AgentSignup obj = (AgentSignup)responseULM.Data;
                try
                {
                    string query = obj.ObjectToQuery<AgentSignup>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseULM = GetSingle(Id);
                    if (responseULM.IsSuccess)
                    {
                        AgentSignup obj = (AgentSignup)responseULM.Data;
                        query += obj.ObjectToQuery<AgentSignup>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            AgentSignup obj = new AgentSignup();
            try
            {
                string query = "select * from agent_signups where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new AgentSignup
                       {
                           Id = x.Field<Int32>("id"),
                           UserId = x.Field<Int32>("user_id"),
                           RegistrationType = x.Field<Int32>("registration_type"),
                           IntroducerCode = x.Field<String>("introducer_code"),
                           UplineCode = x.Field<String>("upline_code"),
                           ExamPreferences = x.Field<String>("exam_preferences"),
                           ProcessStatus = x.Field<Int32>("process_status"),
                           Remarks = x.Field<String>("remarks"),
                           AgentCode = x.Field<String>("agent_code"),
                           IsNewAgent = x.Field<Int32>("is_new_agent"),
                           QualificationUrl = x.Field<String>("qualification_url"),
                           IcUrl = x.Field<String>("ic_url"),
                           SelfieUrl = x.Field<String>("selfie_url"),
                           PopUrl = x.Field<String>("pop_url"),
                           NewCardReceivedOn = x.Field<DateTime?>("new_card_received_on") == null ? default(DateTime) : x.Field<DateTime>("new_card_received_on"),
                           FIMMNo = x.Field<String>("FIMM_no"),
                           ApprovalDate = x.Field<DateTime?>("approval_date") == null ? default(DateTime) : x.Field<DateTime>("approval_date"),
                           ExpiryDate = x.Field<DateTime?>("expiry_date") == null ? default(DateTime) : x.Field<DateTime>("expiry_date"),
                           FromAgentMICRCode = x.Field<String>("from_agent_MICR_code"),
                           FromAgentBankName = x.Field<String>("from_agent_bank_name"),
                           FromAgentChequeNo = x.Field<String>("from_agent_cheque_no"),
                           FromAgentTransRefNo = x.Field<String>("from_agent_trans_ref_no"),
                           FromAgentAmount = x.Field<Decimal?>("from_agent_amount") == null ? 0 : x.Field<Decimal>("from_agent_amount"),
                           FromAgentDateReceived = x.Field<DateTime?>("from_agent_date_received") == null ? default(DateTime) : x.Field<DateTime>("from_agent_date_received"),
                           FromAgentPaymentRefNo = x.Field<String>("from_agent_payment_ref_no"),
                           FromAgentDepositAccNo = x.Field<String>("from_agent_deposit_acc_no"),
                           ToFiMMWithAccNo = x.Field<String>("to_FiMM_with_acc_no"),
                           ToFiMMMICRCode = x.Field<String>("to_FiMM_MICR_code"),
                           ToFiMMBankName = x.Field<String>("to_FiMM_bank_name"),
                           ToFiMMChequeNo = x.Field<String>("to_FiMM_cheque_no"),
                           ToFiMMDateSent = x.Field<DateTime?>("to_FiMM_date_sent") == null ? default(DateTime) : x.Field<DateTime>("to_FiMM_date_sent"),
                           ToFiMMFIMMRenewalAmount = x.Field<Decimal?>("to_FiMM_FIMM_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_FIMM_renewal_amount"),
                           ToFiMMUTMCRenewalAmount = x.Field<Decimal?>("to_FiMM_UTMC_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_UTMC_renewal_amount"),
                           IsActive = x.Field<Int32>("is_active"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           Status = x.Field<Int32>("status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AgentSignup> objs = new List<AgentSignup>();
            try
            {
                string query = "select * from agent_signups";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AgentSignup
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            RegistrationType = x.Field<Int32>("registration_type"),
                            IntroducerCode = x.Field<String>("introducer_code"),
                            UplineCode = x.Field<String>("upline_code"),
                            ExamPreferences = x.Field<String>("exam_preferences"),
                            ProcessStatus = x.Field<Int32>("process_status"),
                            Remarks = x.Field<String>("remarks"),
                            AgentCode = x.Field<String>("agent_code"),
                            IsNewAgent = x.Field<Int32>("is_new_agent"),
                            QualificationUrl = x.Field<String>("qualification_url"),
                            IcUrl = x.Field<String>("ic_url"),
                            SelfieUrl = x.Field<String>("selfie_url"),
                            PopUrl = x.Field<String>("pop_url"),
                            NewCardReceivedOn = x.Field<DateTime?>("new_card_received_on") == null ? default(DateTime) : x.Field<DateTime>("new_card_received_on"),
                            FIMMNo = x.Field<String>("FIMM_no"),
                            ApprovalDate = x.Field<DateTime?>("approval_date") == null ? default(DateTime) : x.Field<DateTime>("approval_date"),
                            ExpiryDate = x.Field<DateTime?>("expiry_date") == null ? default(DateTime) : x.Field<DateTime>("expiry_date"),
                            FromAgentMICRCode = x.Field<String>("from_agent_MICR_code"),
                            FromAgentBankName = x.Field<String>("from_agent_bank_name"),
                            FromAgentChequeNo = x.Field<String>("from_agent_cheque_no"),
                            FromAgentTransRefNo = x.Field<String>("from_agent_trans_ref_no"),
                            FromAgentAmount = x.Field<Decimal?>("from_agent_amount") == null ? 0 : x.Field<Decimal>("from_agent_amount"),
                            FromAgentDateReceived = x.Field<DateTime?>("from_agent_date_received") == null ? default(DateTime) : x.Field<DateTime>("from_agent_date_received"),
                            FromAgentPaymentRefNo = x.Field<String>("from_agent_payment_ref_no"),
                            FromAgentDepositAccNo = x.Field<String>("from_agent_deposit_acc_no"),
                            ToFiMMWithAccNo = x.Field<String>("to_FiMM_with_acc_no"),
                            ToFiMMMICRCode = x.Field<String>("to_FiMM_MICR_code"),
                            ToFiMMBankName = x.Field<String>("to_FiMM_bank_name"),
                            ToFiMMChequeNo = x.Field<String>("to_FiMM_cheque_no"),
                            ToFiMMDateSent = x.Field<DateTime?>("to_FiMM_date_sent") == null ? default(DateTime) : x.Field<DateTime>("to_FiMM_date_sent"),
                            ToFiMMFIMMRenewalAmount = x.Field<Decimal?>("to_FiMM_FIMM_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_FIMM_renewal_amount"),
                            ToFiMMUTMCRenewalAmount = x.Field<Decimal?>("to_FiMM_UTMC_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_UTMC_renewal_amount"),
                            IsActive = x.Field<Int32>("is_active"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from agent_signups";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpening>(propertyName);
            List<AgentSignup> objs = new List<AgentSignup>();
            try
            {
                string query = "select * from agent_signups where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from agent_signups where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AgentSignup
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            RegistrationType = x.Field<Int32>("registration_type"),
                            IntroducerCode = x.Field<String>("introducer_code"),
                            UplineCode = x.Field<String>("upline_code"),
                            ExamPreferences = x.Field<String>("exam_preferences"),
                            ProcessStatus = x.Field<Int32>("process_status"),
                            Remarks = x.Field<String>("remarks"),
                            AgentCode = x.Field<String>("agent_code"),
                            IsNewAgent = x.Field<Int32>("is_new_agent"),
                            QualificationUrl = x.Field<String>("qualification_url"),
                            IcUrl = x.Field<String>("ic_url"),
                            SelfieUrl = x.Field<String>("selfie_url"),
                            PopUrl = x.Field<String>("pop_url"),
                            NewCardReceivedOn = x.Field<DateTime?>("new_card_received_on") == null ? default(DateTime) : x.Field<DateTime>("new_card_received_on"),
                            FIMMNo = x.Field<String>("FIMM_no"),
                            ApprovalDate = x.Field<DateTime?>("approval_date") == null ? default(DateTime) : x.Field<DateTime>("approval_date"),
                            ExpiryDate = x.Field<DateTime?>("expiry_date") == null ? default(DateTime) : x.Field<DateTime>("expiry_date"),
                            FromAgentMICRCode = x.Field<String>("from_agent_MICR_code"),
                            FromAgentBankName = x.Field<String>("from_agent_bank_name"),
                            FromAgentChequeNo = x.Field<String>("from_agent_cheque_no"),
                            FromAgentTransRefNo = x.Field<String>("from_agent_trans_ref_no"),
                            FromAgentAmount = x.Field<Decimal?>("from_agent_amount") == null ? 0 : x.Field<Decimal>("from_agent_amount"),
                            FromAgentDateReceived = x.Field<DateTime?>("from_agent_date_received") == null ? default(DateTime) : x.Field<DateTime>("from_agent_date_received"),
                            FromAgentPaymentRefNo = x.Field<String>("from_agent_payment_ref_no"),
                            FromAgentDepositAccNo = x.Field<String>("from_agent_deposit_acc_no"),
                            ToFiMMWithAccNo = x.Field<String>("to_FiMM_with_acc_no"),
                            ToFiMMMICRCode = x.Field<String>("to_FiMM_MICR_code"),
                            ToFiMMBankName = x.Field<String>("to_FiMM_bank_name"),
                            ToFiMMChequeNo = x.Field<String>("to_FiMM_cheque_no"),
                            ToFiMMDateSent = x.Field<DateTime?>("to_FiMM_date_sent") == null ? default(DateTime) : x.Field<DateTime>("to_FiMM_date_sent"),
                            ToFiMMFIMMRenewalAmount = x.Field<Decimal?>("to_FiMM_FIMM_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_FIMM_renewal_amount"),
                            ToFiMMUTMCRenewalAmount = x.Field<Decimal?>("to_FiMM_UTMC_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_UTMC_renewal_amount"),
                            IsActive = x.Field<Int32>("is_active"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpening>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from agent_signups where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from agent_signups where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AgentSignup> objs = new List<AgentSignup>();
            try
            {
                string query = "select * from agent_signups where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AgentSignup
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            RegistrationType = x.Field<Int32>("registration_type"),
                            IntroducerCode = x.Field<String>("introducer_code"),
                            UplineCode = x.Field<String>("upline_code"),
                            ExamPreferences = x.Field<String>("exam_preferences"),
                            ProcessStatus = x.Field<Int32>("process_status"),
                            Remarks = x.Field<String>("remarks"),
                            AgentCode = x.Field<String>("agent_code"),
                            IsNewAgent = x.Field<Int32>("is_new_agent"),
                            QualificationUrl = x.Field<String>("qualification_url"),
                            IcUrl = x.Field<String>("ic_url"),
                            SelfieUrl = x.Field<String>("selfie_url"),
                            PopUrl = x.Field<String>("pop_url"),
                            NewCardReceivedOn = x.Field<DateTime?>("new_card_received_on") == null ? default(DateTime) : x.Field<DateTime>("new_card_received_on"),
                            FIMMNo = x.Field<String>("FIMM_no"),
                            ApprovalDate = x.Field<DateTime?>("approval_date") == null ? default(DateTime) : x.Field<DateTime>("approval_date"),
                            ExpiryDate = x.Field<DateTime?>("expiry_date") == null ? default(DateTime) : x.Field<DateTime>("expiry_date"),
                            FromAgentMICRCode = x.Field<String>("from_agent_MICR_code"),
                            FromAgentBankName = x.Field<String>("from_agent_bank_name"),
                            FromAgentChequeNo = x.Field<String>("from_agent_cheque_no"),
                            FromAgentTransRefNo = x.Field<String>("from_agent_trans_ref_no"),
                            FromAgentAmount = x.Field<Decimal?>("from_agent_amount") == null ? 0 : x.Field<Decimal>("from_agent_amount"),
                            FromAgentDateReceived = x.Field<DateTime?>("from_agent_date_received") == null ? default(DateTime) : x.Field<DateTime>("from_agent_date_received"),
                            FromAgentPaymentRefNo = x.Field<String>("from_agent_payment_ref_no"),
                            FromAgentDepositAccNo = x.Field<String>("from_agent_deposit_acc_no"),
                            ToFiMMWithAccNo = x.Field<String>("to_FiMM_with_acc_no"),
                            ToFiMMMICRCode = x.Field<String>("to_FiMM_MICR_code"),
                            ToFiMMBankName = x.Field<String>("to_FiMM_bank_name"),
                            ToFiMMChequeNo = x.Field<String>("to_FiMM_cheque_no"),
                            ToFiMMDateSent = x.Field<DateTime?>("to_FiMM_date_sent") == null ? default(DateTime) : x.Field<DateTime>("to_FiMM_date_sent"),
                            ToFiMMFIMMRenewalAmount = x.Field<Decimal?>("to_FiMM_FIMM_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_FIMM_renewal_amount"),
                            ToFiMMUTMCRenewalAmount = x.Field<Decimal?>("to_FiMM_UTMC_renewal_amount") == null ? 0 : x.Field<Decimal>("to_FiMM_UTMC_renewal_amount"),
                            IsActive = x.Field<Int32>("is_active"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from agent_signups where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
