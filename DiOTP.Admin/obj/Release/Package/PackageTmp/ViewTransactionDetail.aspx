﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewTransactionDetail.aspx.cs" Inherits="Admin.ViewTransactionDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <table class="display responsive nowrap table table-bordered mb-30" cellspacing="0" width="100%">
                        <thead id ="viewTransHead" runat="server">
                            <tr>
                                <th>No.</th>
                                <th>Fund Name</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody id="userTbody" runat="server">
                            <tr>
                                <th class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                        </div>
                                    </div>
                                    S.No
                                </th>
                                <td>Fund Name</td>
                                <td>Amount</td>
                                <td>Ref No / Remarks</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
