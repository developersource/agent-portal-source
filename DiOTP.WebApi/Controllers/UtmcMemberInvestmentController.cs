﻿using DiOTP.OracleData;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DiOTP.WebApi.Controllers
{
    public class UtmcMemberInvestmentController : ApiController
    {
        public List<UtmcMemberInvestment> Get(string accNo, string toDate, string fromDate)
        {
            return UtmcMemberInvestmentLiveData.GetDataByMemberNoAndDate(accNo, toDate, fromDate);
        }
    }
}
