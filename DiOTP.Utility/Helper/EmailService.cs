﻿//using DiOTP.Utility.CustomClasses;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Mail;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web;

//namespace DiOTP.Utility.Helper
//{
//    public class EmailService
//    {
//        public static string SMTP_IP = ConfigurationManager.AppSettings["SMTP_IP"].ToString();

//        public static Boolean SendVerificationLink(Email email, String emailCode)
//        {
//            int ur = email.user.UserRoleId;

//            bool send;
//            //try
//            //{
//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AccountActivation.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[url]", email.link + "?key=" + CustomEncryptorDecryptor.EncryptText(email.user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + emailCode));
//            mailText = mailText.Replace("[username]", email.user.Username);

//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Account Activation", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = "Account Activation";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }

//            }


//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }


//            return send;
//        }
//        public static Boolean SendActivationMail(Email email)
//        {
//            bool send;

//            string FilePath = HttpContext.Current.Server.MapPath("/EmailTemplate/AccountActivated.html");

//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[url]", email.link);
//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Account Activation", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);


//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = "Account Activation";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }


//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }



//            return send;
//        }
//        public static Boolean SendResetPasswordLink(Email email, String emailCode)
//        {
//            bool send;

//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/ResetPassword.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[url]", email.link + "?key=" + CustomEncryptorDecryptor.EncryptText(email.user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + emailCode));
//            mailText = mailText.Replace("[username]", email.user.Username);

//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Password Reset", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = "Password Reset";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;

//        }
//        public static Boolean SendResetPasswordLinkAdmin(Email email)
//        {
//            bool send;

//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/ResetPassword.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[url]", email.link + "?key=" + email.user.UniqueKey);
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[userid]", email.user.Username);


//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Password Reset", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = "Password Reset";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;

//        }

//        public static Boolean SendUpdateMail(Email email, String title, String content, String date, String content2)
//        {
//            bool send;


//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/UpdateMail.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[title]", title);
//            mailText = mailText.Replace("[content]", content);
//            mailText = mailText.Replace("[datetime]", date);
//            mailText = mailText.Replace("[content2]", content2);
//            try
//            {
//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = title;
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }
//        public static Boolean SendAlertUpdateMail(Email email, String ma, String title, String content, bool isWindowApp = false, string templatePath = "")
//        {
//            bool send;
//            string FilePath = "";
//            if (isWindowApp)
//            {
//                FilePath = templatePath;
//            }
//            else
//            {
//                FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/UpdateAlert.html");
//            }
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[ma]", ma);

//            if (email.subject == "Statement Notification")
//            {
//                string lastMessage = @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left ! important; color: black'>Should you have any queries, please feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you for your continued support.</p><br/><br/><br/>
                                                                                        
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//                mailText = mailText.Replace("[title]", email.body);
//                mailText = mailText.Replace("[content]", content);
//                mailText = mailText.Replace("[lastmessage]", lastMessage);
//            }
//            else
//            {
//                mailText = mailText.Replace("[title]", title);
//                mailText = mailText.Replace("[content]", content + " on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
//                mailText = mailText.Replace("[lastmessage]", "");
//            }

//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title, mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = title;
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }

//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }
//        public static Boolean SendLostGoogleAuthenticatorMail(Email email, String key)
//        {
//            bool send;

//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/LostGoogleAuthenticator.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[key]", key);
//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Reset Google Authenticator", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = "Reset Google Authenticator";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }

//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }
//        public static Boolean SendUnlockAccLink(Email email, String emailCode)
//        {
//            bool send;
//            //try
//            //{
//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/UnlockAcc.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[url]", email.link + "?key=" + CustomEncryptorDecryptor.EncryptText(email.user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + emailCode));
//            mailText = mailText.Replace("[username]", email.user.Username);

//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Unlock Account", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = "Unlock Account";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }

//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }
//        public static Boolean SendAccountOpeningMail(Email email, String title, String content, String date, String content2, int type, int relation)
//        {
//            bool send;
//            String lastMessage = "";
//            String content3 = "";
//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AccountOpening.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            if (type == 1)
//            {
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a>.<br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                content = "Thank you for choosing Apex Investment Services Berhad as your trusted investment partner.<br/><br/>We are committed to excellence and sustainability in managing your investment.<br/>Please input the verification code below for account creation in eApexis.com.my";

//                //title = "Verification code for Account Opening";

//                content3 = "Email Verification Code: " + content2 + "<br/><p style='font-size: 20px; '>(This code will expire in 24 hours)</p>";
//                content2 = "";
//            }
//            else if (type == 2)
//            {
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                content = "Thank you for completing the online " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account application process.";

//                title = "Successful Enrollment for Account Opening";
//                content3 = "This is to confirm that we have received your online " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account application. An email containing the details of <b>Master Account</b> will be sent to your registered email once it has been approved.";
//                content2 = "";
//            }
//            else if (type == 3)
//            {
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                content = "We are pleased to inform you that your online " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account application dated " + date + " has been approved.";
//                date = "";
//                title = "Approval for your Account Opening";

//                content3 = "Your Master Account number is: <b>" + content2 + "</b><br/><br/>You may now login to www.eApexis.apexis.com.my and start your investment today!<br/><br/>Once again, thank you for choosing Apex Investment Services Berhad as your trusted investment partner.";
//                content2 = "";
//            }
//            else if (type == 4)
//            {
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//We sincerely thank you for your interest in our Funds.<br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                content = "We regret to inform you that we were unable to process your Unit Trust Investment Account application dated " + date;
//                date = "";
//                title = "Account Opening Rejected";
//                content3 = "Reason: Do not meet the required criteria(s).";
//            }
//            else if (type == 5)
//            {
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                content = "We have notice that your registration has not been completed . Please click on the link below to continue with your registration:";

//                title = "Thank you for applying for a new Unit Trust Investment Account with Apex Investment Services Berhad dated";
//                content2 = "www.eApexis.apexis.com.my";
//                content3 = "This link will expire in 30 days from " + DateTime.Now.ToString();
//            }
//            else if (type == 6)
//            {
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                content = "This is to confirm that we have received your online Unit Trust Investment Account application. An email containing the details of <b>Master Account<b/> will be sent to your registered email once it has been approved.";
//                content2 = "";
//                title = "Successful Enrollment for Account Opening";
//                content3 = "Thank you for completing the online Unit Trust Investment Account application process.";
//            }
//            else
//            {

//            }
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[title]", title);
//            mailText = mailText.Replace("[content]", content);
//            mailText = mailText.Replace("[datetime]", date);
//            mailText = mailText.Replace("[content2]", content2);
//            mailText = mailText.Replace("[content3]", content3);
//            mailText = mailText.Replace("[lastMessage]", lastMessage);
//            try
//            {
//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = title;
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                Logger.WriteLog(ex.StackTrace);
//                Logger.WriteLog(ex.InnerException != null ? ex.InnerException.StackTrace : "");
//                send = false;
//            }

//            return send;
//        }
//        public static Boolean SendOrderEmail(List<UserOrder> userOrder, Email email, List<UtmcFundInformation> funds, List<UtmcFundInformation> funds2, String ma, List<HolderLedger> holderLedgers = null, bool isWindowApp = false, string templatePath = "", PaymentDetail paymentDetail = null, String fpxResponseCode = "", String bankName = "")
//        {
//            String title = "";
//            String msg = "";
//            String msg2 = "";
//            String table = "";
//            String lastMessage = "";
//            int i = 0;
//            //Pending Orders - Remainder Email
//            if (userOrder.FirstOrDefault().OrderStatus == 1)
//            {
//                title = "Apex - Payment Reminder for Order ";
//                msg = "We have received the following buy transaction from you on " + userOrder.FirstOrDefault().CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
//                msg2 = "";
//            }
//            else if (userOrder.FirstOrDefault().OrderStatus == 19)
//            {

//                title = "Apex - Voiding of Order";
//                msg = "We have not received any payment for the following transaction order which has more than 2 months in our system.Therefore, this order will now be voided.";
//                msg2 = "";
//            }
//            else if (userOrder.FirstOrDefault().OrderType == 1)
//            {
//                if (userOrder.FirstOrDefault().OrderStatus == 22)
//                {
//                    title = "Apex - Payment Notification";
//                    msg = "Payment Pending - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your payment for order #" + userOrder.FirstOrDefault().OrderNo + " is pending";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 2)
//                {
//                    title = "Apex - Payment Notification";
//                    msg = "Payment Success - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your payment for order #" + userOrder.FirstOrDefault().OrderNo + " is successful";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 29)
//                {
//                    title = "Apex - Payment Notification";
//                    msg = "Payment Failed - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your payment for order #" + userOrder.FirstOrDefault().OrderNo + " is failed";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 3)
//                {
//                    title = "Apex - Confirmation Advice Slip";
//                    msg = "";
//                    msg2 = "Here attached your Confirmation Advice Slip - Purchase";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    title = "Apex - Order Notification";
//                    msg = "Order Rejected - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been rejected (Reject reason: " + email.rejectReason + ")";
//                }

//                decimal total = Math.Round(userOrder.Sum(x => x.Amount), 2);
//                var tbody = "";

//                foreach (UserOrder us in userOrder)
//                {
//                    us.Amount = Math.Round(us.Amount, 2);

//                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);

//                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                    {
//                        tbody += @"<tr style='line-height:0px;'><td style='text-align: left'>" + utmcFundInformation.FundName.Capitalize() + @"</td><td style='text-align: right'>" + us.Amount.ToString("N2") + "</td></tr>";
//                    }
//                    else
//                    {
//                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == us.RejectReason);
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.OrderType.ToString() == "1" ? "Purchase" : us.OrderType.ToString() == "2" ? "Redemption" : "") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Investment Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Sales Charges %</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.AppFee.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Sales Charges (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.FeeAMT.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.GstAMT != null ? holderLedger.GstAMT.Value.ToString("N2") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.UnitValue.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                    }
//                    i++;
//                }
//                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                    table += @"<thead>";
//                    table += @"<tr style='line-height:0px;'><th style='text-align: left; font-size:18px;'> Fund Name </th><th style = 'text-align: right; font-size:18px;'> Amount(MYR) </th ></tr>";
//                    table += @"</thead>";
//                    table += @"<tbody>";
//                    table += tbody;
//                    table += @"</tbody>";
//                    table += @"<tfoot><tr style='line-height:0px;'><td style='text-align:left; font-size:18px;'>Total Amount (MYR)</td><td style='text-align:right; font-size:18px;'>" + total.ToString("N2") + "</td></tr>";
//                    table += @"</tfoot>";
//                    table += @"</table>";
//                    lastMessage += @"<table role='presentation' class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important;'>
//                                        <tbody>
//                                            <tr>
//                                                <td class='email_body tc' style='text-align: center;background-color: #eceff1;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
//                                                    <div align='center'>
//                                                        <table role='presentation' class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;max-width: 640px;Margin: 0 auto;text-align: center;width: 100%;min-width: 0 !important;'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center;background-color: #ffffff;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
//                                                                        <table role='presentation' class='colum' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important;font-family:Lato, sans-serif !important;text-align: center;font-size: 16px;color: #757575;vertical-align: top;padding-left: 8px;padding-right: 8px;'>
//                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 0px;font-size: 13px;color: #a8a8a8;' > For any enquiries, please contact our Customer Service Executive at +603-2095 9999</p>
//                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 24px;font-size: 13px;color: #a8a8a8;' > Thank you for Investing with us.We look forward to serving you again.</p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                }
//                else
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this purchase transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you for your continued support.</p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                }


//            }
//            else if (userOrder.FirstOrDefault().OrderType == 2)
//            {
//                if (userOrder.FirstOrDefault().OrderStatus == 2)
//                {
//                    title = "Apex - Order Notification";
//                    msg = "Order Confirmed - Redemption on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been placed and is pending for approval";
//                }

//                else if (userOrder.FirstOrDefault().OrderStatus == 3)
//                {
//                    title = "Apex - Confirmation Advice Slip";
//                    msg = "";
//                    msg2 = "Here attached your Confirmation Advice Slip - Redemption";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    title = "Apex - Order Notification";
//                    msg = "Order Rejected - Redemption on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been rejected (Reject reason: " + email.rejectReason + ")";
//                }

//                var tbody = "";

//                foreach (UserOrder us in userOrder)
//                {
//                    us.Amount = Math.Round(us.Amount, 2);

//                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);

//                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                    {
//                        tbody += @"<tr><td style='text-align: left'>" + utmcFundInformation.FundName.Capitalize() + @"</td><td style = 'text-align: right'>" + us.Units.ToString("N4") + "</td></tr>";
//                    }
//                    else
//                    {
//                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == us.RejectReason);
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.OrderType.ToString() == "1" ? "Purchase" : us.OrderType.ToString() == "2" ? "Redemption" : "") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Trans No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Gross Amount Redeemed (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.UnitValue.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees %</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.ExitFee != null ? holderLedger.ExitFee.Value.ToString("N2") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.FeeAMT != null ? holderLedger.FeeAMT.Value.ToString("N4") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.GstAMT != null ? holderLedger.GstAMT.Value.ToString("N2") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                    }



//                    i++;
//                }

//                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                    table += @"<thead>";
//                    table += @"<tr style='line-height:0px; font-size:18px;'><th style='text-align: left'> Fund Name </th><th style = 'text-align: right; font-size:18px;'> Units </th ></tr>";
//                    table += @"</thead>";
//                    table += @"<tbody>";
//                    table += tbody;
//                    table += @"</tbody>";
//                    table += @"</table>";
//                    lastMessage += @"<table role='presentation' class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
//                                        <tbody>
//                                            <tr>
//                                                <td class='email_body tc' style='text-align: center;background-color: #eceff1;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
//                                                    <div align='center'>
//                                                        <table role='presentation' class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;max-width: 640px;Margin: 0 auto;text-align: center;width: 100%;min-width: 0 !important;'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center;background-color: #ffffff;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
//                                                                        <table role='presentation' class='colum' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important;font-family:Lato, sans-serif !important;text-align: center;font-size: 16px;color: #757575;vertical-align: top;padding-left: 8px;padding-right: 8px;'>
//                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 0px;font-size: 13px;color: #a8a8a8;' > For any enquiries, please contact our Customer Service Executive at +603-2095 9999</p>
//                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 24px;font-size: 13px;color: #a8a8a8;' > Thank you for Investing with us.We look forward to serving you again.</p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                }
//                else
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this Redemption transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                                                                        Thank you for your continued support.</p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                                                                        eapexis.apexis.com.my
//                                                                                        </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//                }

//            }
//            else if (userOrder.FirstOrDefault().OrderType == 3 || userOrder.FirstOrDefault().OrderType == 4)
//            {
//                if (userOrder.FirstOrDefault().OrderStatus == 2)
//                {
//                    title = "Apex - Order Notification";
//                    msg = "Order Confirmed - Switch on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been placed and is pending for approval";
//                }

//                else if (userOrder.FirstOrDefault().OrderStatus == 3)
//                {
//                    title = "Apex - Confirmation Advice Slip";
//                    msg = "";
//                    msg2 = "Here attached your Confirmation Advice - Switch";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    title = "Apex - Order Notification";
//                    msg = "Order Rejected - Switch on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
//                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been rejected (Reject reason: " + email.rejectReason + ")";
//                }

//                decimal total = 0;
//                List<UserOrder> type4s = userOrder.Where(x => x.OrderType == 4).ToList();
//                userOrder = userOrder.Where(x => x.OrderType == 3).ToList();
//                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                    table += @"<tbody>";
//                    table += @"<tr style='line-height:0px; font-size:18px;'><th style='text-align:left;'>Fund Name</th><th style='text-align:left; font-size:18px;'>Type</th><th style='text-align:right;'>Unit</th></tr>";
//                }

//                foreach (UserOrder us in userOrder)
//                {
//                    UserOrder type4 = type4s.FirstOrDefault(x => x.OrderNo == us.OrderNo && x.FundId == us.FundId);

//                    us.Amount = Math.Round(us.Amount, 2);
//                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
//                    UtmcFundInformation utmcFundInformation2 = funds2.FirstOrDefault(x => x.Id == us.ToFundId);

//                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                    {
//                        table += @"<tr style='line-height:0px; font-size:18px;'><td style='text-align:left;'>" + utmcFundInformation.FundName.Capitalize() + "</td><td style='text-align:left;line-height: 18px;padding: 5px 15px;'>Switch Out</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Units.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px; font-size:18px;'><td style='text-align:left;'>" + utmcFundInformation2.FundName.Capitalize() + "</td><td style='text-align:left;line-height: 18px;padding: 5px 15px;'>Switch In</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>-</td></tr>";
//                    }
//                    else
//                    {
//                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == us.RejectReason);
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>Switch Out</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Trans No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Gross Amount Redeemed (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees %</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.ExitFee != null ? holderLedger.ExitFee.Value.ToString("N2") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.FeeAMT != null ? holderLedger.FeeAMT.Value.ToString("N2") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.GstAMT != null ? holderLedger.GstAMT.Value.ToString("N2") : "-") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.TransAmt.Value - holderLedger.OtherCharges.Value - holderLedger.GstAMT.Value).ToString("N2") + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                    }

//                    us.Amount = Math.Round(us.Amount, 2);
//                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                    {
//                        table += "";
//                    }
//                    else
//                    {
//                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == type4.RejectReason);
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>Switch In</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Trans No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation2.FundName.Capitalize() + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Investment Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Sales Charges (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.FeeAMT.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.GstAMT.Value.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.TransAmt.Value - holderLedger.FeeAMT.Value - holderLedger.GstAMT.Value).ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Switch Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");

//                    }

//                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
//                    {
//                        i = i + 2;
//                    }
//                    else
//                    {
//                        i++;
//                    }
//                }


//                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
//                {
//                    table += "</tbody></table>";
//                }
//                else
//                {

//                }
//                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    lastMessage += @"<table role='presentation' class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important;'>
//                                        <tbody>
//                                            <tr>
//                                                <td class='email_body tc' style='text-align: center;background-color: #eceff1;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
//                                                    <div align='center'>
//                                                        <table role='presentation' class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;max-width: 640px;Margin: 0 auto;text-align: center;width: 100%;min-width: 0 !important;'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center;background-color: #ffffff;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
//                                                                        <table role='presentation' class='colum' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important;font-family:Lato, sans-serif !important;text-align: center;font-size: 16px;color: #757575;vertical-align: top;padding-left: 8px;padding-right: 8px;'>
//                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 0px;font-size: 13px;color: #a8a8a8;' > For any enquiries, please contact our Customer Service Executive at +603-2095 9999</p>
//                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 24px;font-size: 13px;color: #a8a8a8;' > Thank you for Investing with us.We look forward to serving you again.</p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//                }
//                else
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this switch transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you for your continued support.</p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//                }

//            }
//            else if (userOrder.FirstOrDefault().OrderType == 6)
//            {

//                if (userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 22)
//                {
//                    title = "Apex - Regular Saving Subscription";
//                    msg = "";
//                    msg2 = "We have received your RSP application #" + userOrder.FirstOrDefault().OrderNo + " and the details are as stated below:";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 29)
//                {
//                    title = "Apex - FPX Payment on Regular Saving Plan Failed";
//                    msg = "";
//                    msg2 = "The following FPX Application #" + userOrder.FirstOrDefault().OrderNo + " request has failed:";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 3)
//                {
//                    title = "Apex - Regular Saving Subscription Successful";
//                    msg = "";
//                    msg2 = "We have received your FPX application #" + userOrder.FirstOrDefault().OrderNo + " and the details are as stated below:";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    title = "Apex - Regular Saving Subscription Rejected";
//                    msg = "";
//                    msg2 = "The following RSP application #" + userOrder.FirstOrDefault().OrderNo + " request has failed.";
//                }

//                var tbody = "";
//                foreach (UserOrder us in userOrder)
//                {
//                    us.Amount = Math.Round(us.Amount, 2);

//                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);


//                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
//                    {
//                        String applicationType = "";
//                        String maxFrequency = "";
//                        String frequencyMode = "";
//                        if (paymentDetail != null)
//                        {
//                            applicationType = paymentDetail.BuyerIban.Split(',')[0];
//                            maxFrequency = paymentDetail.BuyerIban.Split(',')[2];
//                            frequencyMode = paymentDetail.BuyerIban.Split(',')[3];
//                        }

//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>"; 
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (paymentDetail.TxnAmount == "" ? "" : paymentDetail.TxnAmount) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>RSP Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.PaymentMethod.ToString() == "1" ? "FPX" : "") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (paymentDetail.FpxTxnTime == "" ? "" : paymentDetail.FpxTxnTime) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Application Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (applicationType == "" ? "" : applicationType) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Mandate Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (paymentDetail.SellerOrderNo == "" ? "" : paymentDetail.SellerOrderNo) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Direct Debit Amount</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>FPX Transaction ID</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (paymentDetail.TransactionId == "" ? "" : paymentDetail.TransactionId) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Max Frequency</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (maxFrequency == "" ? "" : maxFrequency) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Frequency Mode</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (frequencyMode == "" ? "" : frequencyMode) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Buyer Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (bankName == "" ? "" : bankName) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Purpose of Payment</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (paymentDetail.ProductDesc == "" ? "" : paymentDetail.ProductDesc) + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Status</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (fpxResponseCode == "" ? "" : fpxResponseCode) + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                    }
//                    else if (userOrder.FirstOrDefault().OrderStatus == 3)
//                    {
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.Username + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.PaymentMethod.ToString() == "1" ? "FPX" : "") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Reference No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.RejectReason + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.BankCode + "</td></tr>";
//                        table += @"<tr style='line-height:22px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Max Debit Monthly Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Date Applied</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.CreatedDate + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                    }
//                    else if (userOrder.FirstOrDefault().OrderStatus == 29)
//                    {
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.Username + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.BankCode + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.PaymentMethod.ToString() == "1" ? "FPX" : "") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.PaymentDate.Value.ToString("dd/MM/yyyy HH:mm") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Reason</td><td style='text-align:right; font-size:16px;width: 300px;line-height: 18px;padding: 5px 15px;'>" + email.fpxResponseDesc + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                        Logger.WriteLog("Email service email.fpxResponseDesc 29: " + email.fpxResponseDesc);
//                        email.rejectReason = email.fpxResponseDesc;
//                    }
//                    else if (userOrder.FirstOrDefault().OrderStatus == 39)
//                    {
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.Username + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.PaymentMethod.ToString() == "1" ? "FPX" : "" + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.BankCode + "</td></tr>";
//                        table += @"<tr style='line-height:22px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Max Debit Monthly Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Date Applied</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.CreatedDate + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Date Rejected</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.SettlementDate + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Reject reason</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.RejectReason + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                        Logger.WriteLog("Email service email.fpxResponseDesc 39: " + email.fpxResponseDesc);
//                        email.rejectReason = us.RejectReason;
//                    }
//                    i++;
//                }

//                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>The contribution date is on the 1st of every month, or the following business day if the 1st is a non business day. 
//                                                                                        Clients are to ensure that there are sufficient balance in your bank account for successful creation of RSP subscription units.
//                                                                                        Failing which, <a href=''>eapexis.apexis.com.my</a> reserves the right not to create any RSP subscription contracts.
//                                                                                        Should you have any queries about this purchase, feel free to email us at eapexis.emandate@apexis.com.my<br/><br/>
//                                                                                        Thank you for your continued support.</p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                                                                        eapexis.apexis.com.my
//                                                                                        </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 3)
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: #black'>An e-mail will be sent to you once the FPX is approved.
//                                The FPX e-Mandate for each bank is subject to one-time approval only. 
//                                Should you have any queries about this purchase, feel free to email us at <a href='eapexis.emandate@apexis.com.my'>eapexis.emandate@apexis.com.my</a><br/><br/>
//                                Thank you for your continued support.
//                                </p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 29)
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: #black'>Please perform a new FPX Application request if you wish to continue your FPX application  (Reject reason: " + email.rejectReason + @").
//                                                                                        Should you have any queries about this purchase, feel free to email us at eapexis.emandate@apexis.com.my <br/><br/>
//                                                                                        Thank you for your continued support.
//                                                                                        </p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                                                                        eapexis.apexis.com.my</p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                ";
//                    Logger.WriteLog("Email service email.rejectReason 39: " + email.rejectReason);
//                }
//                else if (userOrder.FirstOrDefault().OrderStatus == 39)
//                {
//                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>This RSP Subscription has been terminated (Reject reason: " + email.rejectReason + @").
//                                Should you have any queries about this purchase, feel free to email us at eapexis.emandate@apexis.com.my <br/><br/>
//                                Thank you for your continued support.
//                                </p><br/><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                ";
//                    Logger.WriteLog("Email service email.rejectReason 39: " + email.rejectReason);
//                }

//            }
//            if (userOrder.FirstOrDefault().OrderStatus == 1)
//            {
//                var tbody = "";
//                foreach (UserOrder us in userOrder)
//                {
//                    us.Amount = Math.Round(us.Amount, 2);
//                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
//                    if (userOrder.FirstOrDefault().OrderType == 1)
//                    {
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + ma + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Investment Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                    }
//                    i++;
//                }
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 17px; text-align: left; color: #a8a8a8'>However we have yet to receive your payment.<br/><br/>
//                                                                                            If you have selected ‘Proof of payment’ as your payment method, please bank-in the cheque to our designated account or you may make a bank transfer through Online Transfer sto our designated bank accounts. <br/><br/>
//                                                                                            Our Designated Accounts: <br/>
//    Bank Name: Maybank Islamic Berhad
//    Account Name: APEX INVESTMENT SERVICES BHD - CLIENTS TRUST ACCOUNT
//    Account Number: 5640 1662 7254
//    <br/><br/>
//                                                                                            Please make sure that the payment receipt is clear and consists of date, time, Apex Investment Services Berhad’s bank account and amount. For recipient reference, please fill up your Order Number, Account Number, Payment Details and take a colour photo of your document, ensure it’s in focus and we can read all the information and upload accordingly to eApexIs.<br/><br/>
//                                                                                            If you do not intend to proceed with this transaction, you can void the order online by clicking on Order Status &#62;&#62; Pending &#62;&#62;Cancel.<br/><br/>
//                                                                                            Please disregard this email if payment has been made.<br/><br/>                                                                                        
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 17px; text-align: left; color: black'>Should you have any queries about this switch transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you for your continued support.</p><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 17px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//            }
//            else if (userOrder.FirstOrDefault().OrderStatus == 19)
//            {
//                var tbody = "";
//                foreach (UserOrder us in userOrder)
//                {
//                    us.Amount = Math.Round(us.Amount, 2);
//                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
//                    if (userOrder.FirstOrDefault().OrderType == 1)
//                    {
//                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
//                        table += @"<tbody>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
//                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Order received on </td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss") + "</td></tr>";
//                        table += @"</tbody>";
//                        table += @"</table>";
//                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
//                    }
//                    i++;
//                }
//                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this switch transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you for your continued support.</p><br/><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>
//                    ";
//            }

//            bool send;

//            string FilePath = "";
//            if (isWindowApp)
//            {
//                FilePath = templatePath;
//            }
//            else
//            {
//                FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/OrderNotification.html");
//            }
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[ma]", ma);
//            mailText = mailText.Replace("[msg]", msg);
//            mailText = mailText.Replace("[msg2]", msg2);
//            mailText = mailText.Replace("[table]", table);
//            mailText = mailText.Replace("[lastMessage]", lastMessage);



//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title + " " + userOrder.FirstOrDefault().OrderNo, mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = title;
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog("SendOrderEmail: " + ex.Message + " - Exception");
//                send = false;

//            }
//            return send;
//        }

//        public static Response SendeKYCDocumentVerificationEmail(AccountOpening accountOpening, Boolean isAdminEmail = false, String requrements = "", String date = "", Boolean isUnclearEmail = false, int relation = 0)
//        {
//            bool send;
//            String lastMessage = "";
//            String content3 = "";
//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AccountOpening.html");
//            StreamReader str = new StreamReader(FilePath);
//            string mailText = str.ReadToEnd();
//            str.Close();
//            Response response = new Response { IsSuccess = true };
//            lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//            string siteURL = ConfigurationManager.AppSettings["siteURL"];


//            string content = "Thank you for applying for a new " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account with Apex Investment Services Berhad dated " + date;
//            date = "";
//            string title = "Additional Documents required";
//            string content2 = "";
//            content3 = "We are currently processing your application and we need further documentations as follows to complete this account opening application.<br/><br/>i. Latest three (3) months’ pay slip/ latest bank statement / latest income statement, or;<br/>ii.Tax statement, i.e.latest J/ EA Form, or;<br/>iii.Letter of confirmation of employment.<br/><br/>Please click on the link below to upload one of the document above:<br/><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto; '><tbody><tr><td class='accent_b' style='padding: 0 !important; font-size: 18px; font-weight: bold; line-height: 22px; border-radius: 4px; text-align: center; background-color: #059cce;'><a href='" + siteURL + "UploadDocuments.aspx?key=" + CustomEncryptorDecryptor.EncryptText(accountOpening.Id + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + (isAdminEmail ? "admin" : "user") + "_" + requrements) + "' style='color: #ffffff;text-decoration: none;display: block !important;padding: 11px 24px !important;line-height: 26px !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'><span style='color: #ffffff;display: block !important;text-align: center !important;vertical-align: top !important;line-height: inherit !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'>Click to Upload</span></a></td></tr></tbody></tr>";

//            if (isUnclearEmail)
//            {
//                title = "Reupload Unclear Documents";
//                content3 = "We have noticed that the following is(are) not clear/missing:" +
//                    " " + requrements +
//                    "<div style='margin-top: 20px'>Please click on the link below to complete your registration:</div><br/><br/><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto; margin-top: 20px; '><tbody><tr><td class='accent_b' style='padding: 0 !important; font-size: 18px; font-weight: bold; line-height: 22px; border-radius: 4px; text-align: center; background-color: #059cce;'>" +
//                    "<a href='" + siteURL + "AccountOpening.aspx?key=" + CustomEncryptorDecryptor.EncryptText(accountOpening.Id + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + (isAdminEmail ? "admin" : "user") + "_" + accountOpening.AccountType + "_" + accountOpening.PrincipleUserId) + "' style='color: #ffffff;text-decoration: none;display: block !important;padding: 11px 24px !important;line-height: 26px !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'><span style='color: #ffffff;display: block !important;text-align: center !important;vertical-align: top !important;line-height: inherit !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'>Click to Upload</span></a>" +
//                    "</td></tr></tbody></tr>";
//            }

//            mailText = mailText.Replace("[username]", accountOpening.Name);
//            mailText = mailText.Replace("[title]", "<b style='color: black;'>" + title + "</b>");
//            mailText = mailText.Replace("[content]", content);
//            mailText = mailText.Replace("[datetime]", date);
//            mailText = mailText.Replace("[content2]", content2);
//            mailText = mailText.Replace("[content3]", content3);
//            mailText = mailText.Replace("[lastMessage]", lastMessage);
//            try
//            {
//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", accountOpening.Email))
//                {

//                    mm.Subject = title;
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);

//                //send = false;
//            }
//            return response;
//        }
//        public static Boolean SendReminderAccountOpening(Email email, String date, String url, String templatePath = "", Int32 relation = 0)
//        {
//            bool send;
//            String lastMessage = "";
//            String content3 = "";
//            String FilePath = templatePath;
//            StreamReader str = new StreamReader(FilePath);

//            string mailText = str.ReadToEnd();
//            str.Close();

//            lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                         <tbody>
//                                             <tr>
//                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                    <div align='center'>
//                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
//                                                            <tbody>
//                                                                <tr>
//                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
//                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
//                                                                            <tbody>
//                                                                                <tr>
//                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a>.<br/><br/>
//                                Thank you.<br/><br/>Yours sincerely,</p><br/>
//                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
//                                eapexis.apexis.com.my
//                                </p>
//                                                                                    </td>
//                                                                                </tr>
//                                                                            </tbody>
//                                                                        </table>
//                                                                    </td>
//                                                                </tr>
//                                                            </tbody>
//                                                        </table>
//                                                    </div>
//                                                </td>
//                                            </tr>
//                                        </tbody>
//                                    </table>";
//            String content = "Thank you for applying for a new " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account with Apex Investment Services Berhad dated " + date;
//            date = "";
//            String title = "Reminder for Account Opening";

//            content3 = "We have notice that your registration has not been completed . Please click on the link below to continue with your registration:<br/><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0'cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto;'><tbody><tr><td class='accent_b' style='padding: 0 !important;font-size: 18px;font-weight: bold;line-height: 22px;border-radius: 4px;text-align: center;background-color: #059cce;'><a href='" + url + "/AccountOpening.aspx' style='color: #ffffff; text-decoration: none;display: block !important;padding: 11px 24px !important;line-height: 26px !important;font-weight: 700 !important;font-family: Lato, sans-serif !important;'><span style='color: #ffffff;display: block !important;text-align: center !important;vertical-align: top !important;line-height: inherit !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;' >Click here</span></a></td></tr></tbody></table><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto; margin-top: 10px; '><tbody><tr><td class='accent_b' style='padding: 0 !important; font-size: 18px; font-weight: bold; line-height: 22px; border-radius: 4px; text-align: center; '>(This link will expire in 24 hours)</td></tr></tbody></table>";
//            String content2 = "";


//            mailText = mailText.Replace("[username]", email.user.Username);
//            mailText = mailText.Replace("[title]", title);
//            mailText = mailText.Replace("[content]", content);
//            mailText = mailText.Replace("[datetime]", date);
//            mailText = mailText.Replace("[content2]", content2);
//            mailText = mailText.Replace("[content3]", content3);
//            mailText = mailText.Replace("[lastMessage]", lastMessage);
//            try
//            {
//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
//                {

//                    mm.Subject = title;
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }

//        public static Boolean SendEnquiry(String email = "", String name = "", String content = "")
//        {
//            bool send;
//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AdminEnquiry.html");
//            StreamReader str = new StreamReader(FilePath);

//            string mailText = str.ReadToEnd();

//            mailText = mailText.Replace("[username]", name);
//            mailText = mailText.Replace("[email]", email);
//            mailText = mailText.Replace("[message]", content);
//            str.Close();
//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", "enquiry@apexis.com.my", "Client Enquiry", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email))
//                {

//                    mm.Subject = "Client Enquiry";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }
//        public static Boolean SendContactUs(String email, String name = "")
//        {
//            bool send;
//            String lastMessage = "";
//            String content3 = "";
//            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/ContactUs.html");
//            StreamReader str = new StreamReader(FilePath);

//            string mailText = str.ReadToEnd();

//            mailText = mailText.Replace("[username]", name);
//            str.Close();
//            try
//            {
//                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email, "Contact Apex", mailText);
//                //message.IsBodyHtml = true;
//                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
//                //emailClient.Send(message);
//                //send = true;

//                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email))
//                {

//                    mm.Subject = "Contact Apex";
//                    //string body = "Hello " + email.user.Username + ",";
//                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
//                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
//                    //body += "<br /><br />Thanks";
//                    mm.Body = mailText;
//                    mm.IsBodyHtml = true;
//                    SmtpClient smtp = new SmtpClient();
//                    smtp.Host = "mail.petraware.com";
//                    smtp.EnableSsl = true;
//                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
//                    smtp.UseDefaultCredentials = true;
//                    smtp.Credentials = NetworkCred;
//                    smtp.Port = 587;
//                    smtp.Send(mm);
//                    send = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                Logger.WriteLog(ex.Message);
//                send = false;
//            }

//            return send;
//        }
//    }
//}

using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DiOTP.Utility.Helper
{
    public class EmailService
    {
        public static string SMTP_IP = ConfigurationManager.AppSettings["SMTP_IP"].ToString();

        public static Boolean SendVerificationLink(Email email, String emailCode)
        {
            int ur = email.user.UserRoleId;

            bool send;
            //try
            //{
            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AccountActivation.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[url]", email.link + "?key=" + CustomEncryptorDecryptor.EncryptText(email.user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + emailCode));
            mailText = mailText.Replace("[username]", email.user.Username);

            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, "Account Activation", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = "Account Activation";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }

            }


            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }


            return send;
        }
        public static Boolean SendActivationMail(Email email)
        {
            bool send;

            string FilePath = HttpContext.Current.Server.MapPath("/EmailTemplate/AccountActivated.html");

            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[url]", email.link);
            try
            {
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Account Activation", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;


                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = "Account Activation";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }

                //using (MailMessage mm = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId))
                //{

                //    mm.Subject = "Account Activation";
                //    //string body = "Hello " + email.user.Username + ",";
                //    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                //    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                //    //body += "<br /><br />Thanks";
                //    mm.Body = mailText;
                //    mm.IsBodyHtml = true;
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = "mail.petraware.com";
                //    smtp.EnableSsl = true;
                //    NetworkCredential NetworkCred = new NetworkCredential("apex-noreply@apexis.com.my", "petraware123");
                //    smtp.UseDefaultCredentials = true;
                //    smtp.Credentials = NetworkCred;
                //    smtp.Port = 587;
                //    smtp.Send(mm);
                //    send = true;
                //}


            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }



            return send;
        }
        public static Boolean SendResetPasswordLink(Email email, String emailCode)
        {
            bool send;

            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/ResetPassword.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[url]", email.link + "?key=" + CustomEncryptorDecryptor.EncryptText(email.user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + emailCode));
            mailText = mailText.Replace("[username]", email.user.Username);

            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, "Password Reset", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = "Password Reset";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;

        }
        public static Boolean SendResetPasswordLinkAdmin(Email email)
        {
            bool send;

            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/ResetPassword.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[url]", email.link + "?key=" + email.user.UniqueKey);
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[userid]", email.user.Username);


            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, "Password Reset", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId))
                {

                    mm.Subject = "Password Reset";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;

        }

        public static Boolean SendUpdateMail(Email email, String title, String content, String date, String content2)
        {
            bool send;


            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/UpdateMail.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[title]", title);
            mailText = mailText.Replace("[content]", content);
            mailText = mailText.Replace("[datetime]", date);
            mailText = mailText.Replace("[content2]", content2);
            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;
                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = title;
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendAlertUpdateMail(Email email, String ma, String title, String content, bool isWindowApp = false, string templatePath = "")
        {
            bool send;
            string FilePath = "";
            if (isWindowApp)
            {
                FilePath = templatePath;
            }
            else
            {
                FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/UpdateAlert.html");
            }
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[ma]", ma);

            if (email.subject == "Statement Notification")
            {
                string lastMessage = @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left ! important; color: black'>Should you have any queries, please feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you for your continued support.</p><br/><br/><br/>

                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
                mailText = mailText.Replace("[title]", email.body);
                mailText = mailText.Replace("[content]", content);
                mailText = mailText.Replace("[lastmessage]", lastMessage);
            }
            else
            {
                mailText = mailText.Replace("[title]", title);
                mailText = mailText.Replace("[content]", content + " on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mailText = mailText.Replace("[lastmessage]", "");
            }

            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;


                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = title;
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendLostGoogleAuthenticatorMail(Email email, String key)
        {
            bool send;

            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/LostGoogleAuthenticator.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[key]", key);
            try
            {
                MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, "Reset Google Authenticator", mailText);
                message.IsBodyHtml = true;
                SmtpClient emailClient = new SmtpClient(SMTP_IP);
                emailClient.Send(message);
                send = true;
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Reset Google Authenticator", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                //using (MailMessage mm = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId))
                //{

                //    mm.Subject = "Reset Google Authenticator";
                //    //string body = "Hello " + email.user.Username + ",";
                //    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                //    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                //    //body += "<br /><br />Thanks";
                //    mm.Body = mailText;
                //    mm.IsBodyHtml = true;
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = "mail.petraware.com";
                //    smtp.EnableSsl = true;
                //    NetworkCredential NetworkCred = new NetworkCredential("apex-noreply@apexis.com.my", "petraware123");
                //    smtp.UseDefaultCredentials = true;
                //    smtp.Credentials = NetworkCred;
                //    smtp.Port = 587;
                //    smtp.Send(mm);
                //    send = true;
                //}

            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendUnlockAccLink(Email email, String emailCode)
        {
            bool send;
            //try
            //{
            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/UnlockAcc.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[url]", email.link + "?key=" + CustomEncryptorDecryptor.EncryptText(email.user.UniqueKey + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + emailCode));
            mailText = mailText.Replace("[username]", email.user.Username);

            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, "Unlock Account", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, "Unlock Account", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = "Unlock Account";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendAccountOpeningMail(Email email, String title, String content, String date, String content2, int type, int relation)
        {
            bool send;
            String lastMessage = "";
            String content3 = "";
            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AccountOpening.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            if (type == 1)
            {
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a>.<br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                content = "Thank you for choosing Apex Investment Services Berhad as your trusted investment partner.<br/><br/>We are committed to excellence and sustainability in managing your investment.<br/>Please input the verification code below for account creation in www.eapexis.apexis.com.my";

                //title = "Verification code for Account Opening";

                content3 = "Email Verification Code: " + content2 + "<br/><p style='font-size: 20px; '>(This code will expire in 24 hours)</p>";
                content2 = "";
            }
            else if (type == 2)
            {
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                content = "Thank you for completing the online " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account application process.";

                title = "Successful Enrollment for Account Opening";
                content3 = "This is to confirm that we have received your online " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account application. An email containing the details of <b>Master Account</b> will be sent to your registered email once it has been approved.";
                content2 = "";
            }
            else if (type == 3)
            {
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                content = "We are pleased to inform you that your online " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account application dated " + date + " has been approved.";
                date = "";
                title = "Approval for your Account Opening";

                content3 = "Your Master Account number is: <b>" + content2 + "</b><br/><br/>You may now login to www.eApexis.apexis.com.my and start your investment today!<br/><br/>Once again, thank you for choosing Apex Investment Services Berhad as your trusted investment partner.";
                content2 = "";
            }
            else if (type == 4)
            {
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
We sincerely thank you for your interest in our Funds.<br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                content = "We regret to inform you that we were unable to process your Unit Trust Investment Account application dated " + date;
                date = "";
                title = "Account Opening Rejected";
                content3 = "Reason: Do not meet the required criteria(s).";
            }
            else if (type == 5)
            {
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                content = "We have notice that your registration has not been completed . Please click on the link below to continue with your registration:";

                title = "Thank you for applying for a new Unit Trust Investment Account with Apex Investment Services Berhad dated";
                content2 = "www.eApexis.apexis.com.my";
                content3 = "This link will expire in 30 days from " + DateTime.Now.ToString();
            }
            else if (type == 6)
            {
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                content = "This is to confirm that we have received your online Unit Trust Investment Account application. An email containing the details of <b>Master Account<b/> will be sent to your registered email once it has been approved.";
                content2 = "";
                title = "Successful Enrollment for Account Opening";
                content3 = "Thank you for completing the online Unit Trust Investment Account application process.";
            }
            else
            {

            }
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[title]", title);
            mailText = mailText.Replace("[content]", content);
            mailText = mailText.Replace("[datetime]", date);
            mailText = mailText.Replace("[content2]", content2);
            mailText = mailText.Replace("[content3]", content3);
            mailText = mailText.Replace("[lastMessage]", lastMessage);
            try
            {
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = title;
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }

                //using (MailMessage mm = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId))
                //{

                //    mm.Subject = title;
                //    //string body = "Hello " + email.user.Username + ",";
                //    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                //    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                //    //body += "<br /><br />Thanks";
                //    mm.Body = mailText;
                //    mm.IsBodyHtml = true;
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = "mail.petraware.com";
                //    smtp.EnableSsl = true;
                //    NetworkCredential NetworkCred = new NetworkCredential("apex-noreply@apexis.com.my", "petraware123");
                //    smtp.UseDefaultCredentials = true;
                //    smtp.Credentials = NetworkCred;
                //    smtp.Port = 587;
                //    smtp.Send(mm);
                //    send = true;
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendOrderEmail(List<UserOrder> userOrder, Email email, List<UtmcFundInformation> funds, List<UtmcFundInformation> funds2, String ma, List<HolderLedger> holderLedgers = null, bool isWindowApp = false, string templatePath = "", PaymentDetail paymentDetail = null, String fpxResponseCode = "", String bankName = "")
        {
            String title = "";
            String msg = "";
            String msg2 = "";
            String table = "";
            String lastMessage = "";
            int i = 0;
            //Pending Orders - Remainder Email
            if (userOrder.FirstOrDefault().OrderStatus == 1)
            {
                title = "Apex - Payment Reminder for Order ";
                msg = "We have received the following buy transaction from you on " + userOrder.FirstOrDefault().CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
                msg2 = "";
            }
            else if (userOrder.FirstOrDefault().OrderStatus == 19)
            {

                title = "Apex - Voiding of Order";
                msg = "We have not received any payment for the following transaction order which has more than 3 days in our system.Therefore, this order will now be voided.";
                msg2 = "";
            }
            else if (userOrder.FirstOrDefault().OrderType == 1)
            {
                if (userOrder.FirstOrDefault().OrderStatus == 22)
                {
                    title = "Apex - Payment Notification";
                    msg = "Payment Pending - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your payment for order #" + userOrder.FirstOrDefault().OrderNo + " is pending";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 2)
                {
                    title = "Apex - Payment Notification";
                    msg = "Payment Success - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your payment for order #" + userOrder.FirstOrDefault().OrderNo + " is successful";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 29)
                {
                    title = "Apex - Payment Notification";
                    msg = "Payment Failed - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your payment for order #" + userOrder.FirstOrDefault().OrderNo + " is failed";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 3)
                {
                    title = "Apex - Confirmation Advice Slip";
                    msg = "";
                    msg2 = "Here attached your Confirmation Advice Slip - Purchase";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    title = "Apex - Order Notification";
                    msg = "Order Rejected - Purchase on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been rejected (Reject reason: " + email.rejectReason + ")";
                }

                decimal total = Math.Round(userOrder.Sum(x => x.Amount), 2);
                var tbody = "";

                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);

                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);

                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                    {
                        tbody += @"<tr style='line-height:0px;'><td style='text-align: left'>" + utmcFundInformation.FundName.Capitalize() + @"</td><td style='text-align: right'>" + us.Amount.ToString("N2") + "</td></tr>";
                    }
                    else
                    {
                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == us.RejectReason);
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.OrderType.ToString() == "1" ? "Purchase" : us.OrderType.ToString() == "2" ? "Redemption" : "") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Investment Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Sales Charges %</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.AppFee.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Sales Charges (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.FeeAMT.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.GstAMT != null ? holderLedger.GstAMT.Value.ToString("N2") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.UnitValue.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    }
                    i++;
                }
                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                    table += @"<thead>";
                    table += @"<tr style='line-height:0px;'><th style='text-align: left; font-size:18px;'> Fund Name </th><th style = 'text-align: right; font-size:18px;'> Amount(MYR) </th ></tr>";
                    table += @"</thead>";
                    table += @"<tbody>";
                    table += tbody;
                    table += @"</tbody>";
                    table += @"<tfoot><tr style='line-height:0px;'><td style='text-align:left; font-size:18px;'>Total Amount (MYR)</td><td style='text-align:right; font-size:18px;'>" + total.ToString("N2") + "</td></tr>";
                    table += @"</tfoot>";
                    table += @"</table>";
                    lastMessage += @"<table role='presentation' class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important;'>
                                        <tbody>
                                            <tr>
                                                <td class='email_body tc' style='text-align: center;background-color: #eceff1;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
                                                    <div align='center'>
                                                        <table role='presentation' class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;max-width: 640px;Margin: 0 auto;text-align: center;width: 100%;min-width: 0 !important;'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center;background-color: #ffffff;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
                                                                        <table role='presentation' class='colum' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important;font-family:Lato, sans-serif !important;text-align: center;font-size: 16px;color: #757575;vertical-align: top;padding-left: 8px;padding-right: 8px;'>
                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 0px;font-size: 13px;color: #a8a8a8;' > For any enquiries, please contact our Customer Service Executive at +603-2095 9999</p>
                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 24px;font-size: 13px;color: #a8a8a8;' > Thank you for Investing with us.We look forward to serving you again.</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                }
                else
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this purchase transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you for your continued support.</p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                }


            }
            else if (userOrder.FirstOrDefault().OrderType == 2)
            {
                if (userOrder.FirstOrDefault().OrderStatus == 2)
                {
                    title = "Apex - Order Notification";
                    msg = "Order Confirmed - Redemption on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been placed and is pending for approval";
                }

                else if (userOrder.FirstOrDefault().OrderStatus == 3)
                {
                    title = "Apex - Confirmation Advice Slip";
                    msg = "";
                    msg2 = "Here attached your Confirmation Advice Slip - Redemption";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    title = "Apex - Order Notification";
                    msg = "Order Rejected - Redemption on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been rejected (Reject reason: " + email.rejectReason + ")";
                }

                var tbody = "";

                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);

                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);

                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                    {
                        tbody += @"<tr><td style='text-align: left'>" + utmcFundInformation.FundName.Capitalize() + @"</td><td style = 'text-align: right'>" + us.Units.ToString("N4") + "</td></tr>";
                    }
                    else
                    {
                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == us.RejectReason);
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.OrderType.ToString() == "1" ? "Purchase" : us.OrderType.ToString() == "2" ? "Redemption" : "") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Trans No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Gross Amount Redeemed (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.UnitValue.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees %</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.ExitFee != null ? holderLedger.ExitFee.Value.ToString("N2") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.FeeAMT != null ? holderLedger.FeeAMT.Value.ToString("N4") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.GstAMT != null ? holderLedger.GstAMT.Value.ToString("N2") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    }



                    i++;
                }

                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                    table += @"<thead>";
                    table += @"<tr style='line-height:0px; font-size:18px;'><th style='text-align: left'> Fund Name </th><th style = 'text-align: right; font-size:18px;'> Units </th ></tr>";
                    table += @"</thead>";
                    table += @"<tbody>";
                    table += tbody;
                    table += @"</tbody>";
                    table += @"</table>";
                    lastMessage += @"<table role='presentation' class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
                                        <tbody>
                                            <tr>
                                                <td class='email_body tc' style='text-align: center;background-color: #eceff1;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
                                                    <div align='center'>
                                                        <table role='presentation' class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;max-width: 640px;Margin: 0 auto;text-align: center;width: 100%;min-width: 0 !important;'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center;background-color: #ffffff;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
                                                                        <table role='presentation' class='colum' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important;font-family:Lato, sans-serif !important;text-align: center;font-size: 16px;color: #757575;vertical-align: top;padding-left: 8px;padding-right: 8px;'>
                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 0px;font-size: 13px;color: #a8a8a8;' > For any enquiries, please contact our Customer Service Executive at +603-2095 9999</p>
                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 24px;font-size: 13px;color: #a8a8a8;' > Thank you for Investing with us.We look forward to serving you again.</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                }
                else
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this Redemption transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                                                                        Thank you for your continued support.</p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                                                                        eapexis.apexis.com.my
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
                }

            }
            else if (userOrder.FirstOrDefault().OrderType == 3 || userOrder.FirstOrDefault().OrderType == 4)
            {
                if (userOrder.FirstOrDefault().OrderStatus == 2)
                {
                    title = "Apex - Order Notification";
                    msg = "Order Confirmed - Switch on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been placed and is pending for approval";
                }

                else if (userOrder.FirstOrDefault().OrderStatus == 3)
                {
                    title = "Apex - Confirmation Advice Slip";
                    msg = "";
                    msg2 = "Here attached your Confirmation Advice - Switch";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    title = "Apex - Order Notification";
                    msg = "Order Rejected - Switch on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Your order #" + userOrder.FirstOrDefault().OrderNo + " has been rejected (Reject reason: " + email.rejectReason + ")";
                }

                decimal total = 0;
                List<UserOrder> type4s = userOrder.Where(x => x.OrderType == 4).ToList();
                userOrder = userOrder.Where(x => x.OrderType == 3).ToList();
                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                    table += @"<tbody>";
                    table += @"<tr style='line-height:0px; font-size:18px;'><th style='text-align:left;'>Fund Name</th><th style='text-align:left; font-size:18px;'>Type</th><th style='text-align:right;'>Unit</th></tr>";
                }

                foreach (UserOrder us in userOrder)
                {
                    UserOrder type4 = type4s.FirstOrDefault(x => x.OrderNo == us.OrderNo && x.FundId == us.FundId);

                    us.Amount = Math.Round(us.Amount, 2);
                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
                    UtmcFundInformation utmcFundInformation2 = funds2.FirstOrDefault(x => x.Id == us.ToFundId);

                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                    {
                        table += @"<tr style='line-height:0px; font-size:18px;'><td style='text-align:left;'>" + utmcFundInformation.FundName.Capitalize() + "</td><td style='text-align:left;line-height: 18px;padding: 5px 15px;'>Switch Out</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Units.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px; font-size:18px;'><td style='text-align:left;'>" + utmcFundInformation2.FundName.Capitalize() + "</td><td style='text-align:left;line-height: 18px;padding: 5px 15px;'>Switch In</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>-</td></tr>";
                    }
                    else
                    {
                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == us.RejectReason);
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>Switch Out</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Trans No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Gross Amount Redeemed (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees %</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.ExitFee != null ? holderLedger.ExitFee.Value.ToString("N2") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Exit Fees (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.FeeAMT != null ? holderLedger.FeeAMT.Value.ToString("N2") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.GstAMT != null ? holderLedger.GstAMT.Value.ToString("N2") : "-") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.TransAmt.Value - holderLedger.OtherCharges.Value - holderLedger.GstAMT.Value).ToString("N2") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                    }

                    us.Amount = Math.Round(us.Amount, 2);
                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                    {
                        table += "";
                    }
                    else
                    {
                        HolderLedger holderLedger = holderLedgers.FirstOrDefault(x => x.TransNO == type4.RejectReason);
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Invoice No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.DocNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransDt.Value.ToString("dd/MM/yyyy") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Type</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>Switch In</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Trans No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransNO + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation2.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Investment Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransAmt.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Sales Charges (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.FeeAMT.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Tax (MYR)</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.GstAMT.Value.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Net Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (holderLedger.TransAmt.Value - holderLedger.FeeAMT.Value - holderLedger.GstAMT.Value).ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Unit Price (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransPr.Value.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Switch Units</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + holderLedger.TransUnits.Value.ToString("N4") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");

                    }

                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
                    {
                        i = i + 2;
                    }
                    else
                    {
                        i++;
                    }
                }


                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
                {
                    table += "</tbody></table>";
                }
                else
                {

                }
                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    lastMessage += @"<table role='presentation' class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important;'>
                                        <tbody>
                                            <tr>
                                                <td class='email_body tc' style='text-align: center;background-color: #eceff1;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
                                                    <div align='center'>
                                                        <table role='presentation' class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;max-width: 640px;Margin: 0 auto;text-align: center;width: 100%;min-width: 0 !important;'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center;background-color: #ffffff;padding-left: 8px;padding-right: 8px;font-size: 0 !important;line-height: 100%;'>
                                                                        <table role='presentation' class='colum' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate;width: 100%;min-width: 0 !important;'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important;font-family:Lato, sans-serif !important;text-align: center;font-size: 16px;color: #757575;vertical-align: top;padding-left: 8px;padding-right: 8px;'>
                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 0px;font-size: 13px;color: #a8a8a8;' > For any enquiries, please contact our Customer Service Executive at +603-2095 9999</p>
                                                                                        <p style = 'font-weight: 400 !important;font-family:Lato, sans-serif !important;line-height: 23px;    Margin-top: 0;Margin-bottom: 24px;font-size: 13px;color: #a8a8a8;' > Thank you for Investing with us.We look forward to serving you again.</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
                }
                else
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this switch transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you for your continued support.</p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
                }

            }
            else if (userOrder.FirstOrDefault().OrderType == 6)
            {

                if (userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 22)
                {
                    title = "Apex - Regular Saving Subscription";
                    msg = "";
                    msg2 = "We have received your RSP application #" + userOrder.FirstOrDefault().OrderNo + " and the details are as stated below:";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 29)
                {
                    title = "Apex - FPX Payment on Regular Saving Plan Failed";
                    msg = "";
                    msg2 = "The following FPX Application #" + userOrder.FirstOrDefault().OrderNo + " request has failed:";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 3)
                {
                    title = "Apex - Regular Saving Subscription Successful";
                    msg = "";
                    msg2 = "We have received your FPX application #" + userOrder.FirstOrDefault().OrderNo + " and the details are as stated below:";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    title = "Apex - Regular Saving Subscription Rejected";
                    msg = "";
                    msg2 = "The following RSP application #" + userOrder.FirstOrDefault().OrderNo + " request has failed.";
                }

                var tbody = "";
                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);

                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);


                    if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
                    {
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>RSP Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.PaymentMethod.ToString() == "1" ? "FPX" : "" + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    }
                    else if (userOrder.FirstOrDefault().OrderStatus == 3)
                    {
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.Username + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.PaymentMethod.ToString() == "1" ? "FPX" : "") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Reference No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.RejectReason + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.BankCode + "</td></tr>";
                        table += @"<tr style='line-height:22px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Max Debit Monthly Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Date Applied</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.CreatedDate + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    }
                    else if (userOrder.FirstOrDefault().OrderStatus == 29)
                    {
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.Username + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.BankCode + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + (us.PaymentMethod.ToString() == "1" ? "FPX" : "") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Transaction Date</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.PaymentDate.Value.ToString("dd/MM/yyyy HH:mm") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Reason</td><td style='text-align:right; font-size:16px;width: 300px;line-height: 18px;padding: 5px 15px;'>" + email.fpxResponseDesc + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                        Logger.WriteLog("Email service email.fpxResponseDesc 29: " + email.fpxResponseDesc);
                        email.rejectReason = email.fpxResponseDesc;
                    }
                    else if (userOrder.FirstOrDefault().OrderStatus == 39)
                    {
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.Username + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Payment Method</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.PaymentMethod.ToString() == "1" ? "FPX" : "" + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Bank Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.BankCode + "</td></tr>";
                        table += @"<tr style='line-height:22px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>e-Mandate Max Debit Monthly Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Date Applied</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.CreatedDate + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Date Rejected</td><td style='text-align:right; font-size:18px; width: 200px;line-height: 18px;padding: 5px 15px;'>" + us.SettlementDate + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Reject reason</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.RejectReason + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                        Logger.WriteLog("Email service email.fpxResponseDesc 39: " + email.fpxResponseDesc);
                        email.rejectReason = us.RejectReason;
                    }
                    i++;
                }

                if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>The contribution date is on the 1st of every month, or the following business day if the 1st is a non business day. 
                                                                                        Clients are to ensure that there are sufficient balance in your bank account for successful creation of RSP subscription units.
                                                                                        Failing which, <a href=''>eapexis.apexis.com.my</a> reserves the right not to create any RSP subscription contracts.
                                                                                        Should you have any queries about this purchase, feel free to email us at eapexis.emandate@apexis.com.my<br/><br/>
                                                                                        Thank you for your continued support.</p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                                                                        eapexis.apexis.com.my
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 3)
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: #black'>An e-mail will be sent to you once the FPX is approved.
                                The FPX e-Mandate for each bank is subject to one-time approval only. 
                                Should you have any queries about this purchase, feel free to email us at <a href='eapexis.emandate@apexis.com.my'>eapexis.emandate@apexis.com.my</a><br/><br/>
                                Thank you for your continued support.
                                </p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 29)
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: #black'>Please perform a new FPX Application request if you wish to continue your FPX application  (Reject reason: " + email.rejectReason + @").
                                                                                        Should you have any queries about this purchase, feel free to email us at eapexis.emandate@apexis.com.my <br/><br/>
                                                                                        Thank you for your continued support.
                                                                                        </p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                                                                        eapexis.apexis.com.my</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                ";
                    Logger.WriteLog("Email service email.rejectReason 39: " + email.rejectReason);
                }
                else if (userOrder.FirstOrDefault().OrderStatus == 39)
                {
                    lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>This RSP Subscription has been terminated (Reject reason: " + email.rejectReason + @").
                                Should you have any queries about this purchase, feel free to email us at eapexis.emandate@apexis.com.my <br/><br/>
                                Thank you for your continued support.
                                </p><br/><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                ";
                    Logger.WriteLog("Email service email.rejectReason 39: " + email.rejectReason);
                }

            }
            if (userOrder.FirstOrDefault().OrderStatus == 1)
            {
                var tbody = "";
                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);
                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
                    if (userOrder.FirstOrDefault().OrderType == 1)
                    {
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + ma + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Investment Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    }
                    i++;
                }
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 17px; text-align: left; color: #a8a8a8'>However we have yet to receive your payment.<br/><br/>
                                                                                            If you have selected ‘Proof of payment’ as your payment method, please bank-in the cheque to our designated account or you may make a bank transfer through Online Transfer sto our designated bank accounts. <br/><br/>
                                                                                            Our Designated Accounts: <br/>
    Bank Name: Maybank Islamic Berhad
    Account Name: APEX INVESTMENT SERVICES BHD - CLIENTS TRUST ACCOUNT
    Account Number: 5640 1662 7254
    <br/><br/>
                                                                                            Please make sure that the payment receipt is clear and consists of date, time, Apex Investment Services Berhad’s bank account and amount. For recipient reference, please fill up your Order Number, Account Number, Payment Details and take a colour photo of your document, ensure it’s in focus and we can read all the information and upload accordingly to eApexIs.<br/><br/>
                                                                                            If you do not intend to proceed with this transaction, you can void the order online by clicking on Order Status &#62;&#62; Pending &#62;&#62;Cancel.<br/><br/>
                                                                                            Please disregard this email if payment has been made.<br/><br/>                                                                                        
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 17px; text-align: left; color: black'>Should you have any queries about this switch transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you for your continued support.</p><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 17px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
            }
            else if (userOrder.FirstOrDefault().OrderStatus == 19)
            {
                var tbody = "";
                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);
                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
                    if (userOrder.FirstOrDefault().OrderType == 1)
                    {
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Order received on </td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    }
                    i++;
                }
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                 <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this switch transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you for your continued support.</p><br/><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                    ";
            }

            bool send;

            string FilePath = "";
            if (isWindowApp)
            {
                FilePath = templatePath;
            }
            else
            {
                FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/OrderNotification.html");
            }
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[ma]", ma);
            mailText = mailText.Replace("[msg]", msg);
            mailText = mailText.Replace("[msg2]", msg2);
            mailText = mailText.Replace("[table]", table);
            mailText = mailText.Replace("[lastMessage]", lastMessage);



            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title + " " + userOrder.FirstOrDefault().OrderNo, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = title;
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("SendOrderEmail: " + ex.Message + " - Exception");
                send = false;

            }
            return send;
        }

        public static Response SendeKYCDocumentVerificationEmail(AccountOpening accountOpening, Boolean isAdminEmail = false, String requrements = "", String date = "", Boolean isUnclearEmail = false, int relation = 0)
        {
            bool send;
            String lastMessage = "";
            String content3 = "";
            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AccountOpening.html");
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            Response response = new Response { IsSuccess = true };
            lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
            string siteURL = ConfigurationManager.AppSettings["siteURL"];


            string content = "Thank you for applying for a new " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account with Apex Investment Services Berhad dated " + date;
            date = "";
            string title = "Additional Documents required";
            string content2 = "";
            content3 = "We are currently processing your application and we need further documentations as follows to complete this account opening application.<br/><br/>i. Latest three (3) months’ pay slip/ latest bank statement / latest income statement, or;<br/>ii.Tax statement, i.e.latest J/ EA Form, or;<br/>iii.Letter of confirmation of employment.<br/><br/>Please click on the link below to upload one of the document above:<br/><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto; '><tbody><tr><td class='accent_b' style='padding: 0 !important; font-size: 18px; font-weight: bold; line-height: 22px; border-radius: 4px; text-align: center; background-color: #059cce;'><a href='" + siteURL + "UploadDocuments.aspx?key=" + CustomEncryptorDecryptor.EncryptText(accountOpening.Id + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + (isAdminEmail ? "admin" : "user") + "_" + requrements) + "' style='color: #ffffff;text-decoration: none;display: block !important;padding: 11px 24px !important;line-height: 26px !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'><span style='color: #ffffff;display: block !important;text-align: center !important;vertical-align: top !important;line-height: inherit !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'>Click to Upload</span></a></td></tr></tbody></tr>";

            if (isUnclearEmail)
            {
                title = "Reupload Unclear Documents";
                content3 = "We have noticed that the following is(are) not clear/missing:" +
                    " " + requrements +
                    "<div style='margin-top: 20px'>Please click on the link below to complete your registration:</div><br/><br/><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto; margin-top: 20px; '><tbody><tr><td class='accent_b' style='padding: 0 !important; font-size: 18px; font-weight: bold; line-height: 22px; border-radius: 4px; text-align: center; background-color: #059cce;'>" +
                    "<a href='" + siteURL + "AccountOpening.aspx?key=" + CustomEncryptorDecryptor.EncryptText(accountOpening.Id + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + (isAdminEmail ? "admin" : "user") + "_" + accountOpening.AccountType + "_" + accountOpening.PrincipleUserId) + "' style='color: #ffffff;text-decoration: none;display: block !important;padding: 11px 24px !important;line-height: 26px !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'><span style='color: #ffffff;display: block !important;text-align: center !important;vertical-align: top !important;line-height: inherit !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;'>Click to Upload</span></a>" +
                    "</td></tr></tbody></tr>";
            }

            mailText = mailText.Replace("[username]", accountOpening.Name);
            mailText = mailText.Replace("[title]", "<b style='color: black;'>" + title + "</b>");
            mailText = mailText.Replace("[content]", content);
            mailText = mailText.Replace("[datetime]", date);
            mailText = mailText.Replace("[content2]", content2);
            mailText = mailText.Replace("[content3]", content3);
            mailText = mailText.Replace("[lastMessage]", lastMessage);
            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", accountOpening.Email, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", accountOpening.Email))
                {

                    mm.Subject = title;
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);

                //send = false;
            }
            return response;
        }
        public static Boolean SendReminderAccountOpening(Email email, String date, String url, String templatePath = "", Int32 relation = 0)
        {
            bool send;
            String lastMessage = "";
            String content3 = "";
            String FilePath = templatePath;
            StreamReader str = new StreamReader(FilePath);

            string mailText = str.ReadToEnd();
            str.Close();

            lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                         <tbody>
                                             <tr>
                                                <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                    <div align='center'>
                                                        <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                            <tbody>
                                                                <tr>
                                                                    <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                        <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 15px; text-align: left; color: black'>Should you need further clarification, please contact us at 03-2095 9999 or email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a>.<br/><br/>
                                Thank you.<br/><br/>Yours sincerely,</p><br/>
                                                                                        <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 15px; text-align: left; color: #a8a8a8' >Regards,<br/>
                                eapexis.apexis.com.my
                                </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>";
            String content = "Thank you for applying for a new " + (relation == 2 ? "Joint " : "") + "Unit Trust Investment Account with Apex Investment Services Berhad dated " + date;
            date = "";
            String title = "Reminder for Account Opening";

            content3 = "We have notice that your registration has not been completed . Please click on the link below to continue with your registration:<br/><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0'cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto;'><tbody><tr><td class='accent_b' style='padding: 0 !important;font-size: 18px;font-weight: bold;line-height: 22px;border-radius: 4px;text-align: center;background-color: #059cce;'><a href='" + url + "/AccountOpening.aspx' style='color: #ffffff; text-decoration: none;display: block !important;padding: 11px 24px !important;line-height: 26px !important;font-weight: 700 !important;font-family: Lato, sans-serif !important;'><span style='color: #ffffff;display: block !important;text-align: center !important;vertical-align: top !important;line-height: inherit !important;font-weight: 700 !important;font-family:Lato, sans-serif !important;' >Click here</span></a></td></tr></tbody></table><table role='presentation' class='ebtn tc' align='center' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; text-align: center; display: table; margin-left: auto; margin-right: auto; margin-top: 10px; '><tbody><tr><td class='accent_b' style='padding: 0 !important; font-size: 18px; font-weight: bold; line-height: 22px; border-radius: 4px; text-align: center; '>(This link will expire in 24 hours)</td></tr></tbody></table>";
            String content2 = "";


            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[title]", title);
            mailText = mailText.Replace("[content]", content);
            mailText = mailText.Replace("[datetime]", date);
            mailText = mailText.Replace("[content2]", content2);
            mailText = mailText.Replace("[content3]", content3);
            mailText = mailText.Replace("[lastMessage]", lastMessage);
            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = title;
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendEnquiry(String email = "", String name = "", String content = "")
        {
            bool send;
            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/AdminEnquiry.html");
            StreamReader str = new StreamReader(FilePath);

            string mailText = str.ReadToEnd();

            mailText = mailText.Replace("[username]", name);
            mailText = mailText.Replace("[email]", email);
            mailText = mailText.Replace("[message]", content);
            str.Close();
            try
            {
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", "enquiry@apexis.com.my", "Client Enquiry", mailText);
                MailMessage message = new MailMessage("apex-noreply@apexis.com.my", "enquiry@apexis.com.my", "Client Enquiry", mailText);
                message.IsBodyHtml = true;
                SmtpClient emailClient = new SmtpClient(SMTP_IP);
                emailClient.Send(message);
                send = true;

                //using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email))
                //{

                //    mm.Subject = "Client Enquiry";
                //    //string body = "Hello " + email.user.Username + ",";
                //    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                //    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                //    //body += "<br /><br />Thanks";
                //    mm.Body = mailText;
                //    mm.IsBodyHtml = true;
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = "mail.petraware.com";
                //    smtp.EnableSsl = true;
                //    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                //    smtp.UseDefaultCredentials = true;
                //    smtp.Credentials = NetworkCred;
                //    smtp.Port = 587;
                //    smtp.Send(mm);
                //    send = true;
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendContactUs(String email, String name = "")
        {
            bool send;
            String lastMessage = "";
            String content3 = "";
            string FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/ContactUs.html");
            StreamReader str = new StreamReader(FilePath);

            string mailText = str.ReadToEnd();

            mailText = mailText.Replace("[username]", name);
            str.Close();
            try
            {
                //MailMessage message = new MailMessage("pecmsupport@petraware.com", email, "Contact Apex", mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email))
                {

                    mm.Subject = "Contact Apex";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                send = false;
            }

            return send;
        }
        public static Boolean SendAgentOrderEmail(List<UserOrder> userOrder, Email email, List<UtmcFundInformation> funds, List<UtmcFundInformation> funds2, String ma, List<HolderLedger> holderLedgers = null, bool isWindowApp = false, string templatePath = "", PaymentDetail paymentDetail = null, String fpxResponseCode = "", String bankName = "")
        {
            String title = "";
            String msg = "";
            String msg2 = "";
            String table = "";
            String lastMessage = "";
            int i = 0;
            //Agent Orders - Remainder Email to Investor
            if (userOrder.FirstOrDefault().OrderType == 1)
            {
                
                    title = "Apex - Purchase Order Created by Agent";
                    msg = "Order Confirmation Pending - Purchase Order made by agent on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    msg2 = "Order #" + userOrder.FirstOrDefault().OrderNo + " is waiting for your approval";
                
                

                decimal total = Math.Round(userOrder.Sum(x => x.Amount), 2);
                var tbody = "";

                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);

                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);

                    
                        
                        table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        //table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                        table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                        table += @"</tbody>";
                        table += @"</table>";
                        table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    
                    i++;
                }
                
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                        <tbody>
                                            <tr>
                                            <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                <div align='center'>
                                                    <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                        <tbody>
                                                            <tr>
                                                                <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                    <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                    <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this purchase transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                            Thank you for your continued support.</p><br/><br/><br/>
                                                                                    <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                            eapexis.apexis.com.my
                            </p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>";
            }
            if (userOrder.FirstOrDefault().OrderType == 2)
            {

                title = "Apex - Redemption Order Created by Agent";
                msg = "Order Confirmation Pending - Redemption Order made by agent on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                msg2 = "Order #" + userOrder.FirstOrDefault().OrderNo + " is waiting for your approval";

                decimal total = Math.Round(userOrder.Sum(x => x.Amount), 2);
                var tbody = "";

                foreach (UserOrder us in userOrder)
                {
                    us.Amount = Math.Round(us.Amount, 2);

                    UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);

                    table += @"<table role='presentation' class='ebtn tc' border='0' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                    table += @"<tbody>";
                    //table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Account Number</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + email.user.UserIdUserAccounts.FirstOrDefault(x => x.Id == us.UserAccountId).AccountNo + "</td></tr>";
                    table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Seller Order No</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.OrderNo + "</td></tr>";
                    table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Fund Name</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + utmcFundInformation.FundName.Capitalize() + "</td></tr>";
                    table += @"<tr style='line-height:0px;'><td style='text-align:left; font-size:18px;line-height: 18px;padding: 5px 15px;'>Amount (MYR)</td><td class='currencyFormat' style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Amount.ToString("N2") + "</td></tr>";
                    table += @"</tbody>";
                    table += @"</table>";
                    table += (userOrder.Count > 1 ? "<br style='line-height: 20px; content: \"\";'><br>" : "");
                    i++;
                }

                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                        <tbody>
                                            <tr>
                                            <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                <div align='center'>
                                                    <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                        <tbody>
                                                            <tr>
                                                                <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                    <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                    <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this purchase transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                            Thank you for your continued support.</p><br/><br/><br/>
                                                                                    <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                            eapexis.apexis.com.my
                            </p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>";
            }
            if (userOrder.FirstOrDefault().OrderType == 3 || userOrder.FirstOrDefault().OrderType == 4 )
            {

                title = "Apex - Switch Order Created by Agent";
                msg = "Order Confirmation Pending - Switch Order made by agent on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                msg2 = "Order #" + userOrder.FirstOrDefault().OrderNo + " is waiting for your approval";

                decimal total = Math.Round(userOrder.Sum(x => x.Amount), 2);
                var tbody = "";

                foreach (UserOrder us in userOrder)
                {
                    if (us.OrderType == 3)
                    {
                        List<UserOrder> type4s = userOrder.Where(x => x.OrderType == 4).ToList();
                        us.Amount = Math.Round(us.Amount, 2);
                        table += @"<table role='presentation' class='ebtn tc' border='1' cellspacing='5' cellpadding='15' style='border-collapse: collapse;text-align:center;display:table;margin-left:auto;margin-right:auto;font-size:14px;width:100%;border-color: #d4d7d9;border: 1px solid #d4d7d9;'>";
                        table += @"<tbody>";
                        table += @"<tr style='line-height:0px; font-size:18px;'><th style='text-align:left;'>Fund Name</th><th style='text-align:left; font-size:18px;'>Type</th><th style='text-align:right;'>Unit</th></tr>";
                        UserOrder type4 = type4s.FirstOrDefault(x => x.OrderNo == us.OrderNo && x.FundId == us.FundId);

                        us.Amount = Math.Round(us.Amount, 2);
                        UtmcFundInformation utmcFundInformation = funds.FirstOrDefault(x => x.Id == us.FundId);
                        UtmcFundInformation utmcFundInformation2 = funds2.FirstOrDefault(x => x.Id == us.ToFundId);


                        table += @"<tr style='line-height:0px; font-size:18px;'><td style='text-align:left;'>" + utmcFundInformation.FundName.Capitalize() + "</td><td style='text-align:left;line-height: 18px;padding: 5px 15px;'>Switch Out</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>" + us.Units.ToString("N4") + "</td></tr>";
                        table += @"<tr style='line-height:0px; font-size:18px;'><td style='text-align:left;'>" + utmcFundInformation2.FundName.Capitalize() + "</td><td style='text-align:left;line-height: 18px;padding: 5px 15px;'>Switch In</td><td style='text-align:right; font-size:18px;line-height: 18px;padding: 5px 15px;'>-</td></tr>";


                        us.Amount = Math.Round(us.Amount, 2);
                        if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2 || userOrder.FirstOrDefault().OrderStatus == 29 || userOrder.FirstOrDefault().OrderStatus == 39)
                        {
                            table += "";
                        }


                        if (userOrder.FirstOrDefault().OrderStatus == 22 || userOrder.FirstOrDefault().OrderStatus == 2)
                        {
                            i = i + 2;
                        }
                        else
                        {
                            i++;
                        }
                    }
                }
                lastMessage += @"<table class='email_table' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                        <tbody>
                                            <tr>
                                            <td class='email_body tc' style='text-align: center; background-color: #eceff1; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                <div align='center'>
                                                    <table class='content_section' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; max-width: 640px; Margin: 0 auto; text-align: center; width: 100%; min-width: 0 !important'>
                                                        <tbody>
                                                            <tr>
                                                                <td class='content_cell content_b tc' style='text-align: center; background-color: #ffffff; padding-left: 8px; padding-right: 8px; font-size: 0 !important; line-height: 100%'>
                                                                    <table class='column' border='0' cellspacing='0' cellpadding='0' style='border-collapse: separate; width: 100%; min-width: 0 !important'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class='column_cell tc' style='font-weight: 400 !important; font-family: Lato, sans-serif !important; text-align: center; font-size: 16px; color: #757575; vertical-align: top; padding-left: 8px; padding-right: 8px'>
                                                                                    <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 0px; font-size: 13px; text-align: left; color: black'>Should you have any queries about this purchase transaction, feel free to email us at <a href='customer.aisb@apexis.com.my'>customer.aisb@apexis.com.my</a><br/><br/>
                            Thank you for your continued support.</p><br/><br/><br/>
                                                                                    <p style = 'font-weight: 400 !important; font-family: Lato, sans-serif !important; line-height: 23px; Margin-top: 0; Margin-bottom: 24px; font-size: 13px; text-align: left; color: #a8a8a8' >Regards,<br/>
                            eapexis.apexis.com.my
                            </p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>";
            }

            bool send;

            string FilePath = "";
            if (isWindowApp)
            {
                FilePath = templatePath;
            }
            else
            {
                FilePath = HttpContext.Current.Server.MapPath("~/EmailTemplate/OrderNotification.html");
            }
            StreamReader str = new StreamReader(FilePath);
            string mailText = str.ReadToEnd();
            str.Close();
            mailText = mailText.Replace("[username]", email.user.Username);
            mailText = mailText.Replace("[ma]", ma);
            mailText = mailText.Replace("[msg]", msg);
            mailText = mailText.Replace("[msg2]", msg2);
            mailText = mailText.Replace("[table]", table);
            mailText = mailText.Replace("[lastMessage]", lastMessage);



            try
            {
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;
                //MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email.user.EmailId, title + " " + userOrder.FirstOrDefault().OrderNo, mailText);
                //message.IsBodyHtml = true;
                //SmtpClient emailClient = new SmtpClient(SMTP_IP);
                //emailClient.Send(message);
                //send = true;

                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email.user.EmailId))
                {

                    mm.Subject = "Agent Order Notification";
                    //string body = "Hello " + email.user.Username + ",";
                    //body += "<br /><br />Please activate your MA account with Activation code sent to your mobile.";
                    //body += "<br /><a href = '" + email.link + "?key=" + email.user.UniqueKey + "'>Click here to activate your DiOTP account.</a>";
                    //body += "<br /><br />Thanks";
                    mm.Body = mailText;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    send = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("SendOrderEmail: " + ex.Message + " - Exception");
                send = false;

            }
            return send;
        }

    }
}