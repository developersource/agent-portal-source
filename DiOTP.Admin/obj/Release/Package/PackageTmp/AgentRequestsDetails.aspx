﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgentRequestsDetails.aspx.cs" Inherits="Admin.AgentRequestsDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">


                <%--<div class="col-md-6">
                    <h3 class="table-heading">Bank Details<small class="pull-right"><strong id="asOnDate" runat="server"></strong></small></h3>
                    <table class="table userDetailsTable">
                        <tr>
                            <th>No. of MA Accounts</th>
                            <td id="noOfMAAccounts" runat="server">-</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-right">
                                <a id="impersonateUserLink" runat="server" href='javascript:;' target='_blank' class='btn btn-sm btn-primary'>Impersonate User</a>
                            </td>
                        </tr>
                    </table>
                </div>--%>

                <div class="col-md-12">
                    <h3 class="table-heading">Agency</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>ID No.</th>
                                <th>Job Type</th>
                                <th>Marital Status</th>
                                <th>Agent Office/Branch</th>
                                <th>Region</th>
                            </tr>
                        </thead>
                        <tbody id="agencyTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Personal</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Date of Birth</th>
                                <th>Mother Maiden Name</th>
                                <th>Sex</th>
                                <th>Race</th>
                                <th>Religion</th>
                                <th>Highest Education</th>
                                <th>Telephone No. (Office)</th>
                                <th>Telephone No. (Home)</th>
                                <th>Handphone No.</th>
                                <th>Email</th>
                                <th>Contact Person</th>
                                <th>Income Tax No.</th>
                                <th>EPF No.</th>
                                <th>SOCSO</th>
                                <th>Language</th>
                                <th>Nationality</th>
                                <th>Marital Status</th>
                                <th>Bankruptcy Declaration</th>
                                <th>Spouse Name</th>
                                <th>Spouse ID No.</th>
                            </tr>
                        </thead>
                        <tbody id="personalTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Address</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Address</th>
                                <th>PostCode</th>
                                <th>State</th>
                                <th>Country</th>
                            </tr>
                        </thead>
                        <tbody id="addressTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Bank</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Currency</th>
                                <th>Bank Name</th>
                                <th>Branch Name</th>
                                <th>Type of Account</th>
                                <th>Account No.</th>
                            </tr>
                        </thead>
                        <tbody id="bankTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Work Experience</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Previous Employer Name</th>
                                <th>Position Held</th>
                                <th>Length of Service (Years)</th>
                                <th>Annual Income</th>
                                <th>Reason for Leaving</th>
                            </tr>
                        </thead>
                        <tbody id="workTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Uploaded Documents</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Document Type</th>
                                <th>Uploaded Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="documentsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">Recommendation</h3>
                    <%--<div id="divRecommendation" runat="server" class="form-control mb-30" style="height: 100px;"></div>--%>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Recommended By Agent Name</th>
                                <th>Recommended Date</th>
                                <th>Recommendation Remarks</th>
                            </tr>
                        </thead>
                        <tbody id="recommendationTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12" id="examsSection" runat="server">
                    <h3 class="table-heading">Exam Enrollment</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Location</th>
                                <th>Language</th>
                                <th>Exam Date</th>
                                <th>Exam Time</th>
                                <th>Status</th>
                                <th>Result</th>
                            </tr>
                        </thead>
                        <tbody id="examsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12" id="fimmSection" runat="server">
                    <h3 class="table-heading">FiMM Details</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>Liscense no.</th>
                                <th>Approval Date</th>
                                <th>Expiry Date</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody id="fimmTbody" runat="server">
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $('.dataTable.maAccounts').DataTable({
                        dom: '',
                        ordering: false
                    });
                }, 500);
            });
        </script>
    </form>
</body>
</html>
