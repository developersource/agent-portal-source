﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ao_bank_details")]
    public class AccountOpeningBankDetail 
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("account_opening_id"), Display("AccountOpeningId")]
        public Int32 AccountOpeningId { get; set; }
        [Database("agreement"), Display("Agreement")]
        public Int32 Agreement { get; set; }
        [Database("account_type"), Display("AccountType")]
        public String AccountType { get; set; }
        [Database("currency"), Display("Currency")]
        public String Currency { get; set; }
        [Database("bank_id"), Display("BankId")]
        public Int32 BankId { get; set; }
        [Database("account_name"), Display("AccountName")]
        public String AccountName { get; set; }
        [Database("account_no"), Display("AccountNo")]
        public String AccountNo { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
