﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class CommissionStructure : System.Web.UI.Page
    {
        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }
        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT cs.*, cd.code, cd.name, cst.code as structure_name FROM ");
            string mainQCount = (@"select count(*) as count from (");
            StringBuilder countFilter = new StringBuilder();
            filter.Append(@" commission_structures cs
                            join commission_defs cd on cd.id = cs.commission_def_id join commission_settings cst on cs.commission_settings_id = cst.id
                            where 1=1 ");

            //Populate dropdown value of commission settings
            string queryCSL = @"Select * from commission_settings";
            Response responseCSettingsList = GenericService.GetDataByQuery(queryCSL, 0, 0, false, null, false, null, true);
            if (responseCSettingsList.IsSuccess)
            {
                var csDyn = responseCSettingsList.Data;
                var responseCSJSON = JsonConvert.SerializeObject(csDyn);
                List<DiOTP.Utility.CommissionSettings> cs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionSettings>>(responseCSJSON);

                ddlCS.DataSource = cs;
                ddlCS.DataTextField = "code";
                ddlCS.DataValueField = "id";
                ddlCS.DataBind();
                ddlCS.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
            }

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (cd.name like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or cd.code like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or structure_name like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or cs.apply_to like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or cs.percent like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ddlCS"]))
                {

                    ddlCS.Value = Request.QueryString["ddlCS"].ToString();
                    var selectedSetting = Request.QueryString["ddlCS"].ToString();
                    filter.Append(" and cs.commission_settings_id in ('" + selectedSetting + "')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value == "0")
                    {
                        filter.Append(@"and cs.is_active in ('0') ");
                    }
                    else if (FilterValue.Value == "1")
                    {
                        filter.Append(@"and cs.is_active in ('1') ");
                    }
                    else
                    {

                    }
                }
                else
                {
                    filter.Append(@"and cs.is_active in ('0', '1') ");
                }


                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by cs." + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                {
                    countFilter.Append(filter);
                    filter.Append("order by cs.id desc");
                    countFilter.Append("group by cs.apply_to, cs.commission_settings_id order by cs.id");
                }


                int skip = 0, take = 0;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + countFilter.ToString() + ") a").Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }


                Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", skip, take, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var UsersDyn = responseUList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.CommissionStructureDetail> commissionStructureDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionStructureDetail>>(responseJSON);

                    var Structures = commissionStructureDetails;
                    var StructureGrouped = Structures.GroupBy(
                        x => new { x.commission_settings_id, x.apply_to },
                        x => x, (key, y) => new { percentValues = y.ToList() });

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    //DiOTP.Utility.CommissionStructureDetail compareObj = new DiOTP.Utility.CommissionStructureDetail();

                    if (commissionStructureDetails.Count() > 0)
                    {
                        for (int i = 0; i < StructureGrouped.Count(); i++)
                        {

                            asb.Append(@"<tr>
                        <td class='icheck'>
                            <div class='square single-row'>
                                <div class='checkbox'>
                                    <input type='checkbox' name='checkRow' class='checkRow' value='" + StructureGrouped.ElementAtOrDefault(i).percentValues[0].id + @"' /> <label>" + index + @"</label><br/>
                                </div>
                            </div>
                            <span class='row-status'>" + (StructureGrouped.ElementAtOrDefault(i).percentValues[0].is_active == 1 ? "<span class='label label-success'>Active</span>" :
                               StructureGrouped.ElementAtOrDefault(i).percentValues[0].is_active == 0 ? "<span class='label label-danger'>Inactive</span>" : "") + @"</span>
                        </td>
                        <td>
                            " + StructureGrouped.ElementAtOrDefault(i).percentValues[0].structure_name + @"
                        </td>
                        <td>
                            " + StructureGrouped.ElementAtOrDefault(i).percentValues[0].apply_to + @"
                        </td>
                        <td>
                            <table class='table'>
                                <tbody>");
                            int elementCount = StructureGrouped.ElementAtOrDefault(i).percentValues.Count();
                            for (int y = 0; y < StructureGrouped.ElementAtOrDefault(i).percentValues.Count(); y++)
                            {
                                asb.Append(@"                                
                                    <tr>
                                        <td>" + StructureGrouped.ElementAtOrDefault(i).percentValues[elementCount - (y + 1)].code + ":" + @"</td>
                                        <td><strong>" + StructureGrouped.ElementAtOrDefault(i).percentValues[elementCount - (y + 1)].percent + @"</strong></td>
                                    </tr>");
                            }
                            asb.Append(@"
                                </tbody>
                                </table>
                                </td>

                            </tr>");
                            index++;
                        }
                        //if (compareObj.commission_settings_id != csd.commission_settings_id || compareObj.apply_to != csd.apply_to) {
                        //    asb.Append(@"<tr>
                        //    <td class='icheck'>
                        //        <div class='square single-row'>
                        //            <div class='checkbox'>
                        //                <input type='checkbox' name='checkRow' class='checkRow' value='" + csd.id + @"' /> <label>" + index + @"</label><br/>
                        //            </div>
                        //        </div>
                        //        <span class='row-status'>" + (csd.is_active == 1 ? "<span class='label label-success'>Active</span>" :
                        //        csd.is_active == 0 ? "<span class='label label-danger'>Inactive</span>" : "") + @"</span>
                        //    </td>
                        //    <td>
                        //        " + csd.structure_name + @"
                        //    </td>
                        //    <td>
                        //        " + csd.apply_to + @"
                        //    </td>
                        //    <td>
                        //        <table class='table'>
                        //            <tbody>");
                        //    foreach (DiOTP.Utility.CommissionStructureDetail csd2 in commissionStructureDetails)
                        //    {
                        //        //only display if from same comm settings and apply to
                        //        if (csd.commission_settings_id == csd2.commission_settings_id && csd.apply_to == csd2.apply_to)
                        //        {
                        //            asb.Append(@"                                
                        //                <tr>
                        //                    <td>" + csd2.code + ":" + @"</td>
                        //                    <td><strong>" + csd2.percent + @"</strong></td>
                        //                </tr>");
                        //        }
                        //    }
                        //    asb.Append(@"
                        // </tbody>
                        //    </table>
                        //    </td>

                        //</tr>"
                        //    );
                        //    index++;
                        //}
                        //compareObj.commission_settings_id = csd.commission_settings_id;
                        //compareObj.apply_to = csd.apply_to;

                        commStructureTbody.InnerHtml = asb.ToString();
                    }


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseUList.Message + "\");", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(dynamic obj)
        {
            Response response = new Response();
            try
            {
                if (HttpContext.Current.Session["admin"] != null)
                {

                    User loginUser = (User)HttpContext.Current.Session["admin"];
                    String id = obj["Id"];
                    String commission_settings_id = obj["ddlCommissionSetting"];
                    String apply_to = obj["ddlApplyTo"];
                    //List<String> txtData = new List<String>();
                    //List<String> comDefId = new List<String>();
                    int index = 3;
                    int idIndex = 0;
                    int isActive = 0;
                    string queryPopulate = (@"SELECT * from commission_defs where is_active = 1 ");
                    Response responseCDList = GenericService.GetDataByQuery(queryPopulate, 0, 0, false, null, false, null, true);
                    if (responseCDList.IsSuccess)
                    {
                        var cdDyn = responseCDList.Data;
                        var responseCDJSON = JsonConvert.SerializeObject(cdDyn);
                        List<DiOTP.Utility.CommissionDefinitions> cd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseCDJSON);

                        List<DiOTP.Utility.CommissionStructure> commissionStructureList = new List<DiOTP.Utility.CommissionStructure>();
                        foreach (DiOTP.Utility.CommissionDefinitions comdef in cd)
                        {
                            //txtData.Add(obj["txt" + comdef.id.ToString()]);
                            //comDefId.Add(obj["comdefId" + comdef.id.ToString()]);

                            DiOTP.Utility.CommissionStructure commissionStructureObj = new DiOTP.Utility.CommissionStructure();
                            commissionStructureObj.apply_to = apply_to;
                            commissionStructureObj.commission_def_id = comdef.id;
                            commissionStructureObj.commission_settings_id = Convert.ToInt32(commission_settings_id);
                            commissionStructureObj.percent = Convert.ToDecimal(obj["txt" + comdef.id.ToString()]);
                            commissionStructureObj.is_active = isActive;
                            commissionStructureObj.created_date = DateTime.Now;
                            commissionStructureObj.created_by = loginUser.Id;
                            commissionStructureObj.status = 1;

                            commissionStructureList.Add(commissionStructureObj);
                        }

                        String querySync = @"Select * from commission_settings where id=" + commission_settings_id;
                        Response responseToSync = GenericService.GetDataByQuery(querySync, 0, 0, false, null, false, null, false);
                        if (responseToSync.IsSuccess)
                        {
                            var cstSyncDyn = responseToSync.Data;
                            var responseSyncJSON = JsonConvert.SerializeObject(cstSyncDyn);
                            List<DiOTP.Utility.CommissionSettings> cstSyncList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionSettings>>(responseSyncJSON);

                            if (cstSyncList.FirstOrDefault().isActive == 1)
                            {
                                isActive = 1;
                            }
                        }


                        //    for (int i = 4; i < obj.Count; i+=2) {
                        //    txtData.Add(obj["txt"+ index.ToString()]);
                        //    comDefId.Add(obj["comdefId" + index.ToString()]);



                        //    index++;
                        //}

                        //Get data to sync the is_active from commission_settings



                        //for (int x = 0; x < txtData.Count; x++)
                        //{
                        //    DiOTP.Utility.CommissionStructure commissionStructureObj = new DiOTP.Utility.CommissionStructure();
                        //    commissionStructureObj.apply_to = apply_to;
                        //    commissionStructureObj.commission_def_id = Convert.ToInt32(comDefId[x]);
                        //    commissionStructureObj.commission_settings_id = Convert.ToInt32(commission_settings_id);
                        //    commissionStructureObj.percent = Convert.ToDecimal(txtData[x]);
                        //    commissionStructureObj.is_active = isActive;
                        //    commissionStructureObj.created_date = DateTime.Now;
                        //    commissionStructureObj.created_by = loginUser.Id;
                        //    commissionStructureObj.status = 1;

                        //    commissionStructureList.Add(commissionStructureObj);
                        //}

                        Response responsePost = new Response();

                        //Get db record for validation
                        String validationAddQuery = @"Select * from commission_structures where commission_settings_id =" + commission_settings_id + " and apply_to='" + apply_to + "'";
                        Response responseCSList = GenericService.GetDataByQuery(validationAddQuery, 0, 0, false, null, false, null, true);
                        if (responseCSList.IsSuccess)
                        {
                            var CStructureDyn = responseCSList.Data;
                            var responseJSON = JsonConvert.SerializeObject(CStructureDyn);
                            List<DiOTP.Utility.CommissionStructure> CSList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionStructure>>(responseJSON);

                            //store values
                            var Structures = CSList;
                            var StructureGrouped = Structures.GroupBy(
                                x => new { x.commission_settings_id, x.apply_to },
                                x => x, (key, y) => new { structId = key.commission_settings_id, structApplyTo = key.apply_to, idValues = y.ToList() });


                            //if = 1 means record already exist
                            if (CSList.Count == 0 && id == "0")
                            {
                                responsePost = GenericService.PostBulkData<DiOTP.Utility.CommissionStructure>(commissionStructureList);
                                if (responsePost.IsSuccess)
                                {
                                    response.IsSuccess = true;
                                    response.Message = "Successfully added";
                                }
                                else
                                {
                                    response = responsePost;
                                }

                            }
                            else if (id != "0" && apply_to != CSList.FirstOrDefault().apply_to.ToString())
                            {
                                // to check if update change from a type that doesnt exist to exist, if count = 1 already exist.

                                commissionStructureList.ForEach(x =>
                                {
                                    x.id = Convert.ToInt32(StructureGrouped.FirstOrDefault().idValues[idIndex].id.ToString());
                                    responsePost = GenericService.UpdateData<DiOTP.Utility.CommissionStructure>(x);
                                    idIndex++;
                                });

                                if (responsePost.IsSuccess)
                                {
                                    response.IsSuccess = true;
                                    response.Message = "Successfully Updated";
                                }
                                else
                                {
                                    response = responsePost;
                                }

                            }
                            else
                            {
                                if (id != "0" && CSList.FirstOrDefault().commission_settings_id.ToString() == commission_settings_id)
                                {
                                    // 
                                    if (id != CSList.FirstOrDefault().id.ToString())
                                    {
                                        //not same record with database
                                        response.Message = "Combination already exist!";
                                    }
                                    else
                                    {
                                        commissionStructureList.ForEach(x =>
                                        {
                                            x.id = Convert.ToInt32(StructureGrouped.FirstOrDefault().idValues[idIndex].id.ToString());
                                            responsePost = GenericService.UpdateData<DiOTP.Utility.CommissionStructure>(x);
                                            idIndex++;
                                        });

                                        if (responsePost.IsSuccess)
                                        {
                                            response.IsSuccess = true;
                                            response.Message = "Successfully Updated";
                                        }
                                        else
                                        {
                                            response = responsePost;
                                        }
                                    }
                                }
                                else
                                {
                                    response.Message = "Combination already exist!";
                                }
                            }
                        }
                        else
                        {
                            response = responsePost;
                        }
                    }
                    else
                    {
                        response = responseCDList;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired!";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

    }
}