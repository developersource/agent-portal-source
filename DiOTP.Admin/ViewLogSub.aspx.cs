﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace Admin
{
    public partial class ViewLogSub : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyIUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyIUserServiceObj.Value; } }
        private static readonly Lazy<IUserLogMainService> lazyIUserLogServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());

        public static IUserLogMainService IUserLogMainService { get { return lazyIUserLogServiceObj.Value; } }
        private static readonly Lazy<IUserLogSubService> lazyIUserLogSuberviceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());

        public static IUserLogSubService IUserLogSubService { get { return lazyIUserLogSuberviceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String idString = Request.QueryString["id"];
                int index = 1;
                Response responseULSList = IUserLogSubService.GetDataByPropertyName(nameof(UserLogSub.UserLogMainId), idString, true, 0, 0, true);
                if (responseULSList.IsSuccess)
                {
                    Response responseUL = IUserLogMainService.GetSingle(Convert.ToInt32(idString));
                    if (responseUL.IsSuccess)
                    {
                        UserLogMain ulm = (UserLogMain)responseUL.Data;
                        List<UserLogSub> uls = (List<UserLogSub>)responseULSList.Data;

                        if (ulm.TableName == "user_accounts")
                        {
                            Response response = IUserService.GetSingle(ulm.UserId);
                            if (response.IsSuccess)
                            {
                                if (ulm.Description == "Update SAT successful")
                                {
                                    //Do nothing
                                }
                                else
                                {
                                    User user = (User)response.Data;
                                    if (user.IsSatChecked == 1)
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='CorporateAccounts.aspx']\").attr('data-url', 'CorporateAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All');$(\"#navAccordion [data-url='CorporateAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='CorporateAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").attr('data-url', 'CorporateAccounts.aspx');setTimeout(function(){ $('.data-url:contains(\"CorporateAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All\")').text(\"CorporateAccounts.aspx\"); }, 1000)", true);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='UserAccounts.aspx']\").attr('data-url', 'UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All');$(\"#navAccordion [data-url='UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").attr('data-url', 'UserAccounts.aspx');setTimeout(function(){ $('.data-url:contains(\"UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=All\")').text(\"UserAccounts.aspx\"); }, 1000)", true);
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert(\"" + response.Message + "\");", true);
                            }
                        }
                        else if (ulm.TableName == "users")
                        {
                            if (uls.Count > 0 && uls.FirstOrDefault().ColumnName == "is_hard_copy")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='HardcopyRequest.aspx']\").attr('data-url', 'HardcopyRequest.aspx?Search=" + ulm.RefValue + "&FilterValue=');$(\"#navAccordion [data-url='HardcopyRequest.aspx?Search=" + ulm.RefValue + "&FilterValue=']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='HardcopyRequest.aspx?Search=" + ulm.RefValue + "&FilterValue=']\").attr('data-url', 'HardcopyRequest.aspx');setTimeout(function(){ $('.data-url:contains(\"HardcopyRequest.aspx?Search=" + ulm.RefValue + "&FilterValue=\")').text(\"HardcopyRequest.aspx\"); }, 1000)", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='UserAccounts.aspx']\").attr('data-url', 'UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=');$(\"#navAccordion [data-url='UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=']\").attr('data-url', 'UserAccounts.aspx');setTimeout(function(){ $('.data-url:contains(\"UserAccounts.aspx?Search=" + ulm.RefValue + "&FilterValue=\")').text(\"UserAccounts.aspx\"); }, 1000)", true);
                            }
                        }
                        else if (ulm.TableName == "user_account_banks")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='MABanks.aspx']\").attr('data-url', 'MABanks.aspx?Search=" + ulm.RefValue + "&FilterValue=All');$(\"#navAccordion [data-url='MABanks.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='MABanks.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").attr('data-url', 'MABanks.aspx');setTimeout(function(){ $('.data-url:contains(\"MABanks.aspx?Search=" + ulm.RefValue + "&FilterValue=All\")').text(\"MABanks.aspx\"); }, 1000)", true);
                        }
                        else if (ulm.TableName == "user_orders")
                        {
                            string query = @"select uo.order_status, uo.ID, uo.order_type, uo.user_id, u.username, uo.user_account_id, ua.account_no, uo.order_no, uo.payment_date, uo.amount, uo.units, uo.payment_method, uo.created_date, uo.updated_date, uo.reject_reason, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits, uo.fpx_bank_code from  user_orders uo
                            join users u on u.id = uo.user_id
                            join user_accounts ua on ua.id = uo.user_account_id 
                            where 1=1  and (uo.order_no = 'SA2000000261') ";
                            Response responseUOList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);
                            if (responseUOList.IsSuccess)
                            {
                                var uoDyn = responseUOList.Data;
                                var responseJSON = JsonConvert.SerializeObject(uoDyn);
                                List<UserOrderCustom> userOrders = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserOrderCustom>>(responseJSON);
                                StringBuilder asb1 = new StringBuilder();
                                int index1 = 1;
                                userOrders.ForEach(u =>
                                {
                                    string checkBox = string.Empty;
                                    DateTime dateTime = u.CreatedDate;
                                    string url = "";
                                    Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), u.OrderNo, true, 0, 0, true);
                                    if (responseUOFList.IsSuccess)
                                    {
                                        UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                        if (uof != null)
                                            url = uof.Url;
                                    }
                                    asb1.Append(@"<tr>
                                            <td>" + dateTime.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                            <td>" + (u.OrderType == 1 ? "Buy" : u.OrderType == 6 ? "RSP" : "") + @"</td>
                                            <td>" + u.Username + @"</td>
                                            <td>" + u.AccountNo + @"</td>
                                            <td><a href='javascript:;' data-id='" + u.OrderNo + @"' data-original-title='View Detail' data-trigger='hover' data-placement='bottom' class='popovers action-button text-info' data-url='" + (u.OrderType == (int)OrderType.RSP ? "ViewDetailRSP.aspx" : "ViewTransactionDetail.aspx") + "' data-title='Transaction Detail - " + u.OrderNo + @"' data-action='View'>" + u.OrderNo + @"</a></td>
                                            <td class='text-right'>" + (u.OrderType == (int)OrderType.Buy || u.OrderType == (int)OrderType.RSP ? u.SumAmount.ToString("N2") : "") + @"</td>
                                            <td>" + (u.PaymentMethod == "1" ? "FPX PAYMENT (" + u.FpxBankCode + @")" : (u.PaymentMethod == "2" ? "<a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text-info' href=\"" + url + "\" target='_blank'><i class='fa fa-eye'></i></a>" : "")) + @"</td>               
                                            <td>" + (u.PaymentDate.HasValue && (u.OrderStatus == 2 || u.OrderStatus == 29) ? u.PaymentDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</td>
                                            <td>" + (u.RejectReason == null ? "" : u.RejectReason) + @"</td>
                                            </tr>
                
                                            ");
                                });
                                logSubDetails.Visible = false;
                                logDetailedView.Visible = true;
                                string tableHead = @"<thead>
                                                        <th>Order Date</th>
                                                        <th>Order Type</th>
                                                        <th>Username</th>
                                                        <th>MA Account</th>
                                                        <th>Order No</th>
                                                        <th class='text-right'>Amount(MYR)</th>
                                                        <th>Payment</th>
                                                        <th>Processed Date</th>
                                                        <th>Remarks</th>
                                                    </thead>";
                                string tableBody = @"<tbody>
                                                        " + asb1.ToString() + @"
                                                    </tbody>";
                                string table = "<table class='display responsive nowrap table table-bordered mb-30' cellspacing='0' width='100%'>" + (tableHead + tableBody) + "</table>";
                                logDetailedView.InnerHtml = table;
                            }
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='Payments.aspx']\").attr('data-url', 'Payments.aspx?Search=" + ulm.RefValue + "&FilterValue=All');$(\"#navAccordion [data-url='Payments.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='Payments.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").attr('data-url', 'Payments.aspx');setTimeout(function(){ $('.data-url:contains(\"Payments.aspx?Search=" + ulm.RefValue + "&FilterValue=All\")').text(\"Payments.aspx\"); }, 1000)", true);
                        }
                        else if (ulm.TableName == "ma_holder_bank")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "$(\"#navAccordion [data-url='BankDetailsVerification.aspx']\").attr('data-url', 'BankDetailsVerification.aspx?Search=" + ulm.RefValue + "&FilterValue=All');$(\"#navAccordion [data-url='BankDetailsVerification.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").click();$('#commonModal').click();$(\"#navAccordion [data-url='BankDetailsVerification.aspx?Search=" + ulm.RefValue + "&FilterValue=All']\").attr('data-url', 'BankDetailsVerification.aspx');setTimeout(function(){ $('.data-url:contains(\"BankDetailsVerification.aspx?Search=" + ulm.RefValue + "&FilterValue=All\")').text(\"BankDetailsVerification.aspx\"); }, 1000)", true);
                        }
                        StringBuilder asb = new StringBuilder();
                        if (uls.Count() != 0)
                        {
                            if (ulm.Description == "Update SAT successful")
                            {
                                logSubDetails.Visible = false;
                                logDetailedView.Visible = true;
                                Type t = GetType("db_column");
                                if (t != null) {
                                    string oldDate = uls.FirstOrDefault(l => Enumeration.GetById(t, (ulm.TableName + "_" + l.ColumnName).ToUpper()) == "SAT Update Date").ValueOld;
                                    string oldScore = uls.FirstOrDefault(l => Enumeration.GetById(t, (ulm.TableName + "_" + l.ColumnName).ToUpper()) == "SAT Score").ValueOld;

                                    string mewDate = uls.FirstOrDefault(l => Enumeration.GetById(t, (ulm.TableName + "_" + l.ColumnName).ToUpper()) == "SAT Update Date").ValueNew;
                                    string newScore = uls.FirstOrDefault(l => Enumeration.GetById(t, (ulm.TableName + "_" + l.ColumnName).ToUpper()) == "SAT Score").ValueNew;

                                    if(oldScore != "0") asb.Append("<div>SAT Score Last Updated on <strong>" + oldDate + "</strong> scoring <strong>" + oldScore + "</strong> points.</div>");
                                    asb.Append("<div class='"+(oldScore != "0" ? "mt-20" : "") +"'>SAT Score Updated on <strong>" + mewDate + "</strong> scoring <strong>" + newScore + "</strong> points.</div>");

                                    logDetailedView.InnerHtml = asb.ToString();
                                }
                            }
                            else
                            {
                                foreach (UserLogSub x in uls)
                                {
                                    Type t = GetType("db_column");
                                    if (t != null && Enumeration.GetById(t, (ulm.TableName + "_" + x.ColumnName).ToUpper()) != "")
                                    {
                                        x.ColumnName = Enumeration.GetById(t, (ulm.TableName + "_" + x.ColumnName).ToUpper());
                                    }
                                    if (IsValidFileFormat(x.ValueOld) && IsValidFileFormat(x.ValueNew))
                                    {
                                        asb.Append(@"<tr>
                                                    <td>" + index.ToString() + @"</td>
                                                    <td>" + x.ColumnName + @"</td>
                                                    <td><a href='" + x.ValueOld + @"' target='_blank'><img height='100' src='" + x.ValueOld + @"' title='" + x.ValueOld + @"' /></a></td>
                                                    <td><a href='" + x.ValueNew + @"' target='_blank'><img height='100' src='" + x.ValueNew + @"' title='" + x.ValueNew + @"'/></a></td>
                                                </tr>");
                                    }
                                    else
                                    {
                                        asb.Append(@"<tr>
                                        <td>" + index.ToString() + @"</td>
                                        <td>" + x.ColumnName + @"</td>
                                        <td>" + x.ValueOld + @"</td>
                                        <td>" + x.ValueNew + @"</td>
                                    </tr>");
                                    }
                                    index++;
                                }
                            }
                            userTbody.InnerHtml = asb.ToString();
                        }
                        else
                        {
                            asb.Append(@"<tr>
                                <td colspan='4' style='text-align:center'>No detail found!</td>
                            </tr>");
                            userTbody.InnerHtml = asb.ToString();
                        }
                    }
                    else
                    {
                        userTbody.InnerHtml = responseUL.Message.ToString();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseULSList.Message + "');", true);
                }
            }
        }

        public bool IsValidFileFormat(String url)
        {
            if (Path.GetExtension(url).ToLower() == ".jpg" ||
                Path.GetExtension(url).ToLower() == ".jpeg" ||
                Path.GetExtension(url).ToLower() == ".png" ||
                Path.GetExtension(url).ToLower() == ".pdf")
            {
                return true;
            }
            else
                return false;
        }

        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

    }
}