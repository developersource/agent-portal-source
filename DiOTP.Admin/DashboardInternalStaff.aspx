﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardInternalStaff.aspx.cs" Inherits="Admin.DashboardInternalStaff" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 63px;
        }
        .auto-style2 {
            height: 29px;
        }
        .badge-info {
          background-color: #3a87ad;
          font-size:14px;
        }
        .booking-cards {
            position: relative;
            display: block;
            min-width: 0;
            word-wrap: break-word;
            background-color:lightsteelblue; 
            background-clip: border-box;
            border: 0px solid #059CCE;
            border-radius: .25rem;
            box-shadow: 0 0 5px gray;
        }
        table.table-bordered > thead > tr > th , table.table-bordered > tbody > tr > td, table.table-bordered{
            border: 0.1px solid black;
        }
        .genericTable{
            
            background-color:mediumaquamarine;
        }
        .dashboardTable.table.table-bordered > thead{
            background-color:skyblue;
        }
        .dashboardTable.table.table-bordered > tbody > tr > td{
            background-color:floralwhite;
        }
    </style>
</head>
<body>
    <form id="DashboardInternalStaffForm" runat="server">
        <div class="container-fluid">
            <div class="container-fluid booking-cards">
                 <div class="row">
                        <br />
                        <span class="badge badge-info" style="margin-left:10px;">Comparison Chart</span>
                        <br />
                        <br />
                    <div class="col-md-6">
                        <figure class="highcharts-figure">
                            <div id="casesContainer"></div>
                        </figure>
                    </div>
                    <div class="col-md-6">
                        <figure class="highcharts-figure">
                            <div id="amountContainer"></div>
                        </figure>
                    </div>
                </div>
            </div>
            
            <hr />
            <div class="container-fluid booking-cards">
                <div class="row">
                    <div class="col-md-12">
                        <br />
                        <span class="badge badge-info">Offline Bookings</span>
                        <br />
                        <br />
                        <table class="table table-table-responsive table-bordered dashboardTable">
                            <thead>
                                <tr>
                                    <th class="text-center">Crit.</th>
                                    <th colspan="3" class="text-center">Today</th>
                                    <th colspan="3" class="text-center">Month To Date (MTD)</th>
                                    <th colspan="3" class="text-center">Year To Date(YTD)</th>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Sales</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Redemption</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Switch</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Transfer</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <hr />
            <div class="container-fluid booking-cards">
                <div class="row">
                    <div class="col-md-12">
                        <br />
                        <span class="badge badge-info">Online Bookings</span>
                        <br />
                        <br />
                        <table class="table table-table-responsive table-bordered dashboardTable">
                            <thead>
                                <tr>
                                    <th class="text-center">Crit.</th>
                                    <th colspan="3" class="text-center">Today</th>
                                    <th colspan="3" class="text-center">Month To Date (MTD)</th>
                                    <th colspan="3" class="text-center">Year To Date(YTD)</th>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Sales</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Redemption</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Switch</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Transfer</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <hr />
            <div class="container-fluid booking-cards">
                <div class="row">
                    <div class="col-md-12">
                        <br />
                        <span class="badge badge-info">Total</span>
                        <br />
                        <br />
                        <table class="table table-table-responsive table-bordered dashboardTable">
                            <thead>
                                <tr>
                                    <th class="text-center">Crit.</th>
                                    <th colspan="3" class="text-center">Today</th>
                                    <th colspan="3" class="text-center">Month To Date (MTD)</th>
                                    <th colspan="3" class="text-center">Year To Date(YTD)</th>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Cases</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Units</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Sales</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Redemption</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Switch</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Transfer</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>       
    </form>

    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <script src="/Content/js/highcharts.js"></script>
    <script>
        var casesBarChart = Highcharts.chart('casesContainer', {
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Offline vs Online Bookings (Cases)'
            },
            xAxis: {
                categories: [
                    'Sales',
                    'Redemption',
                    'Switch',
                    'Transfer'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cases'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>RM{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Offline Booking',
                data: [49.9, 71.5, 106.4, 129.2]

            }, {
                name: 'Online Booking',
                data: [83.6, 78.8, 98.5, 93.4]

            }]
        });

        var amountBarChart = Highcharts.chart('amountContainer', {
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Offline vs Online Booking (MYR)'
            },
            xAxis: {
                categories: [
                    'Sales',
                    'Redemption',
                    'Switch',
                    'Transfer'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount (MYR)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>RM{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Offline Booking',
                data: [49.9, 71.5, 106.4, 129.2]

            }, {
                name: 'Online Booking',
                data: [83.6, 78.8, 98.5, 93.4]

            }]
        });
    </script>

</body>
</html>
