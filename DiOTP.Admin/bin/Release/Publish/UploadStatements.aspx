﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadStatements.aspx.cs" Inherits="Admin.UploadStatements" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="uploadStatementsForm" runat="server">
        <div class="row" id="FormId" data-value="uploadStatementsForm">
            <div class="col-md-12">
                <input type="file" name="statements" id="statements" class="form-control" multiple="multiple" />
            </div>
            <div class="col-md-12 statementStatus hide">
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th style='width: 50px;'>S. No.</th>
                            <th style='width: 50px;'>Status</th>
                            <th>File Name</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody id="statementUploadResult">
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
