﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddLanguageDir : System.Web.UI.Page
    {
        private static readonly Lazy<ILanguageDirService> lazyLanguageDirService = new Lazy<ILanguageDirService>(() => new LanguageDirService());

        public static ILanguageDirService ILanguageDirService { get { return lazyLanguageDirService.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            string path = ConfigurationManager.AppSettings["dir_path"].ToString();
            DirectoryInfo parentDir = new DirectoryInfo(path);
            FileInfo[] Files = parentDir.GetFiles("*.aspx");
            TextPage.DataSource = Files;
            TextPage.DataTextField = "Name";
            TextPage.DataValueField = "Name";
            TextPage.DataBind();
            

            string idString = Request.QueryString["id"];

            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);
                Response response = ILanguageDirService.GetSingle(id);
                if (response.IsSuccess)
                {
                    LanguageDir languageDir = (LanguageDir)response.Data;
                    ID.Value = languageDir.Id.ToString();
                    Module.Value = languageDir.Module.ToString();
                    TextId.Value = languageDir.TextId;
                    TextPage.Value = languageDir.TextPage.ToString();
                    TextEn.Value = languageDir.TextEn.ToString();
                    TextMs.Value = languageDir.TextMs.ToString();
                    TextZh.Value = languageDir.TextZh.ToString();
                    Status.Value = languageDir.Status.ToString();
                    Remarks.Value = languageDir.Remarks;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}