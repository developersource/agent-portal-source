using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcFundFileRepo : IUtmcFundFileRepo
    {
        IUtmcFundFileTypesDefRepo IUtmcFundFileTypesDefRepo = new UtmcFundFileTypesDefRepo();
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcFundFile obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundFile>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundFile> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundFile>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundFile obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundFile>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundFile> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundFile>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundFile DeleteData(Int32 Id)
        {
            Response responseUFF = GetSingle(Id);
            if (responseUFF.IsSuccess)
            {
                UtmcFundFile obj = (UtmcFundFile)responseUFF.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcFundFile>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseUFF = GetSingle(Id);
                    if (responseUFF.IsSuccess)
                    {
                        UtmcFundFile obj = (UtmcFundFile)responseUFF.Data;
                        query += obj.ObjectToQuery<UtmcFundFile>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundFile obj = new UtmcFundFile();
            try
            {
                string query = "select * from utmc_fund_files where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcFundFile
                       {
                           Id = x.Field<Int32>("id"),
                           UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                           UtmcFundFileTypesDefId = x.Field<Int32>("utmc_fund_file_types_def_id"),
                           Name = x.Field<String>("name"),
                           Url = x.Field<String>("url"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           CreatedBy = x.Field<Int32>("created_by"),
                           UpdatedDate = x.Field<DateTime?>("updated_date"),
                           UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                           Status = x.Field<Int32?>("status") == null ? 0 : x.Field<Int32>("status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                Response responseUFFTD = IUtmcFundFileTypesDefRepo.GetSingle(obj.UtmcFundFileTypesDefId);
                if (responseUFFTD.IsSuccess)
                {
                    obj.UtmcFundFileTypesDefIdUtmcFundFileTypesDef = (UtmcFundFileTypesDef)responseUFFTD.Data;
                    response.IsSuccess = true;
                    response.Data = obj;
                }
                else
                {
                    response = responseUFFTD;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundFile> objs = new List<UtmcFundFile>();
            try
            {
                string query = "select * from utmc_fund_files";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundFile
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            UtmcFundFileTypesDefId = x.Field<Int32>("utmc_fund_file_types_def_id"),
                            Name = x.Field<String>("name"),
                            Url = x.Field<String>("url"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            Status = x.Field<Int32?>("status") == null ? 0 : x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();

                objs.ForEach(obj =>
                {
                    Response responseUFFTD = IUtmcFundFileTypesDefRepo.GetSingle(obj.UtmcFundFileTypesDefId);
                    if (responseUFFTD.IsSuccess)
                    {
                        obj.UtmcFundFileTypesDefIdUtmcFundFileTypesDef = (UtmcFundFileTypesDef)responseUFFTD.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_files";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundFile>(propertyName);
            List<UtmcFundFile> objs = new List<UtmcFundFile>();
            try
            {
                string query = "select * from utmc_fund_files where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from utmc_fund_files where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundFile
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            UtmcFundFileTypesDefId = x.Field<Int32>("utmc_fund_file_types_def_id"),
                            Name = x.Field<String>("name"),
                            Url = x.Field<String>("url"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            Status = x.Field<Int32?>("status") == null ? 0 : x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();

                objs.ForEach(obj =>
                {
                    Response responseUFFTD = IUtmcFundFileTypesDefRepo.GetSingle(obj.UtmcFundFileTypesDefId);
                    if (responseUFFTD.IsSuccess)
                    {
                        obj.UtmcFundFileTypesDefIdUtmcFundFileTypesDef = (UtmcFundFileTypesDef)responseUFFTD.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundFile>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_files where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from utmc_fund_files where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundFile> objs = new List<UtmcFundFile>();
            try
            {
                string query = "select * from utmc_fund_files where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundFile
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            UtmcFundFileTypesDefId = x.Field<Int32>("utmc_fund_file_types_def_id"),
                            Name = x.Field<String>("name"),
                            Url = x.Field<String>("url"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            Status = x.Field<Int32?>("status") == null ? 0 : x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();

                objs.ForEach(obj =>
                {
                    Response responseUFFTD = IUtmcFundFileTypesDefRepo.GetSingle(obj.UtmcFundFileTypesDefId);
                    if (responseUFFTD.IsSuccess)
                    {
                        obj.UtmcFundFileTypesDefIdUtmcFundFileTypesDef = (UtmcFundFileTypesDef)responseUFFTD.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_files where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
