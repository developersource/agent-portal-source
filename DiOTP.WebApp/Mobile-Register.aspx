﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Mobile-Register.aspx.cs" Inherits="DiOTP.WebApp.Mobile_Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="mobile-login-section">
        <div class="container-fluid">

            <div id="divMessage" runat="server"></div>

            <div class="login-mobile-head">
                <h4><strong>Account Activation</strong></h4>
            </div>

            <div class="mobile-login-content">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                    <asp:TextBox ID="txtMANumber" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your MA Number"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                    <asp:TextBox ID="txtICNo" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your IC Number"></asp:TextBox>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                    <asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your Email ID"></asp:TextBox>
                </div>
                <div class="form-group mb-12">
                    <asp:Button ID="btnRequestCode" runat="server" CssClass="btn login-btn-mobile btn-block" OnClick="btnRequestCode_Click" Text="Request Code" />
                    <asp:Button ID="btnRequestCodeForAdditionalAcc" runat="server" CssClass="btn login-btn-mobile btn-block" OnClick="btnRequestCodeForAdditionalAcc_Click" Visible="false" Text="Request Code" />
                </div>
                <div class="login-fp mb-8">
                    <a href="/Login.aspx" style="font-size: 16px; color: #987407;"><strong>Login Here</strong></a>
                </div>

               <div class="row mb-10 text-center">
                    <div class="col-sm-12">
                        <div class="fs-14">
                            <a href="/PrivacyPolicy.aspx">Privacy Policy</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="apex">
                            <p style="margin-bottom: 0px;">
                                Apex Technologies
                <br />
                                Co.Reg.No.000000-A
                <br />
                                GST Registration No. 000 000 000 000.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    </section>
    <script>
        /**
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        */
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });

        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
