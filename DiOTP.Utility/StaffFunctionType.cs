﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("staff_function_type")]
    public class StaffFunctionType
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("function_type_def_id"), Display("FunctionTypeDefId")]
        public Int32 FunctionTypeDefId { get; set; }

        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32 UpdatedBy { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
