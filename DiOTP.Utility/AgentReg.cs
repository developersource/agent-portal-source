﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_regs_copy")]
    public class AgentRegCopy
    {
        [Database("ID"), Display("Id")]
        public Int32 id { get; set; }
        [Database("user_id"), Display("Id")]
        public Int32 user_id { get; set; }
        [Database("agent_signup_id"), Display("Id")]
        public Int32 agent_signup_id { get; set; }
        [Database("agent_code"), Display("Id")]
        public String agent_code { get; set; }
        [Database("rank_id"), Display("Id")]
        public Int32? rank_id { get; set; }
        [Database("reg_type_uts"), Display("Id")]
        public Int32 reg_type_uts { get; set; }
        [Database("reg_type_prs"), Display("Id")]
        public Int32 reg_type_prs { get; set; }
        [Database("job_grade"), Display("Id")]
        public String job_grade { get; set; }
        [Database("rollout_no"), Display("Id")]
        public String rollout_no { get; set; }
        [Database("job_type_id"), Display("Id")]
        public Int32? job_type_id { get; set; }
        [Database("old_agent_code"), Display("Id")]
        public String old_agent_code { get; set; }
        [Database("industry_group_id"), Display("Id")]
        public Int32? industry_group_id { get; set; }
        [Database("recruiter"), Display("Id")]
        public String recruiter { get; set; }
        [Database("upline_agent_code"), Display("Id")]
        public String upline_agent_code { get; set; }
        [Database("supervisor"), Display("Id")]
        public String supervisor { get; set; }
        [Database("ex_upline_agent"), Display("Id")]
        public String ex_upline_agent { get; set; }
        [Database("office_id"), Display("Id")]
        public Int32? office_id { get; set; }
        [Database("blacklisted_from"), Display("Id")]
        public DateTime? blacklisted_from { get; set; }
        [Database("blacklisted_to"), Display("Id")]
        public DateTime? blacklisted_to { get; set; }
        [Database("equilization_from"), Display("Id")]
        public DateTime? equilization_from { get; set; }
        [Database("equilization_to"), Display("Id")]
        public DateTime? equilization_to { get; set; }
        [Database("agent_status_date"), Display("Id")]
        public DateTime? agent_status_date { get; set; }
        [Database("rank_date"), Display("Id")]
        public DateTime? rank_date { get; set; }
        [Database("UTMC_joining_date"), Display("Id")]
        public DateTime? UTMC_joining_date { get; set; }
        [Database("agent_status"), Display("Id")]
        public Int32 agent_status { get; set; }
        [Database("last_tab"), Display("Id")]
        public String last_tab { get; set; }
        [Database("created_date"), Display("Id")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("Id")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("Id")]
        public DateTime? updated_date { get; set; }
        [Database("updated_by"), Display("Id")]
        public Int32? updated_by { get; set; }
        [Database("status"), Display("Id")]
        public Int32 status { get; set; }


        public AgentRegPersonal AgentRegPersonal { get; set; }
        public AgentRegPaymentInsurance AgentRegPaymentInsurance { get; set; }
        public AgentRegExam AgentRegExam { get; set; }
        public AgentRegExp AgentRegExp { get; set; }
    }

    [Database("agent_regs")]
    public class AgentReg
    {
        [Database("ID"), Display("Id")]
        public Int32 id { get; set; }
        [Database("user_id"), Display("Id")]
        public Int32 user_id { get; set; }
        [Database("agent_code"), Display("Id")]
        public String agent_code { get; set; }
        [Database("is_new_agent"), Display("Id")]
        public Int32 is_new_agent { get; set; }
        [Database("introducer_code"), Display("Id")]
        public String introducer_code { get; set; }
        [Database("reg_type_uts"), Display("Id")]
        public Int32 reg_type_uts { get; set; }
        [Database("reg_type_prs"), Display("Id")]
        public Int32 reg_type_prs { get; set; }
        [Database("job_type_id"), Display("Id")]
        public Int32 job_type_id { get; set; }
        [Database("office_id"), Display("Id")]
        public Int32 office_id { get; set; }
        [Database("process_status"), Display("ProcessStatus")]
        public Int32 process_status { get; set; }
        [Database("recommendation"), Display("recommendation")]
        public String recommendation { get; set; }
        [Database("recommendation_date"), Display("recommendation_date")]
        public DateTime recommendation_date { get; set; }
        [Database("remarks"), Display("Remarks")]
        public String remarks { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("Id")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("Id")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("Id")]
        public DateTime? updated_date { get; set; }
        [Database("updated_by"), Display("Id")]
        public Int32? updated_by { get; set; }
        [Database("status"), Display("Id")]
        public Int32 status { get; set; }


        public AgentRegPersonal AgentRegPersonal { get; set; }
        public AgentRegPaymentInsurance AgentRegPaymentInsurance { get; set; }
        public AgentRegExam AgentRegExam { get; set; }
        public AgentRegExp AgentRegExp { get; set; }
        public List<AgentRegFile> AgentRegFiles { get; set; }
    }

    public class AgentRegInfo
    {
        public Int32 id { get; set; }
        public Int32 user_id { get; set; }
        public String agent_code { get; set; }
        public String office_name { get; set; }
        public String region_name { get; set; }
        public Int32 is_new_agent { get; set; }
        public String introducer_code { get; set; }
        public Int32 reg_type_uts { get; set; }
        public Int32 reg_type_prs { get; set; }
        public Int32 job_type_id { get; set; }
        public Int32 office_id { get; set; }
        public Int32 process_status { get; set; }
        public String remarks { get; set; }
        public String recommendation { get; set; }
        public DateTime recommendation_date { get; set; }
        public Int32 is_active { get; set; }
        public DateTime created_date { get; set; }
        public Int32 created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public Int32? updated_by { get; set; }
        public Int32 status { get; set; }
        public string username { get; set; }
        public string email_id { get; set; }
        public string mobile_number { get; set; }
        public string id_no { get; set; }
        public String name { get; set; }
        public DateTime? dob { get; set; }
        public String mother_maiden_name { get; set; }
        public String sex { get; set; }
        public String race { get; set; }
        public String religion { get; set; }
        public String highest_education { get; set; }
        public Int32 years_of_experience { get; set; }
        public String bankruptcy_declaration { get; set; }
        public String mail_addr1 { get; set; }
        public String mail_addr2 { get; set; }
        public String mail_addr3 { get; set; }
        public String mail_addr4 { get; set; }
        public String post_code { get; set; }
        public String state { get; set; }
        public String country { get; set; }
        public String tel_no_office { get; set; }
        public String tel_no_home { get; set; }
        public String hand_phone { get; set; }
        public String email { get; set; }
        public String contact_person { get; set; }
        public String income_tax_no { get; set; }
        public String epf_no { get; set; }
        public String socso { get; set; }
        public String language { get; set; }
        public String nationality { get; set; }
        public String marital_status { get; set; }
        public String spouse_name { get; set; }
        public String spouse_id_no { get; set; }
        public String agent_reg_payment_id { get; set; }
        public String insurance_beneficiary_name { get; set; }
        public String insurance_beneficiary_NRIC { get; set; }
        public String employer_name { get; set; }
        public String position_held { get; set; }
        public Int32? length_of_service_yrs { get; set; }
        public String annual_income { get; set; }
        public String reason_for_leaving { get; set; }
        public List<AgentRegFile> AgentRegFiles { get; set; }
    }
}
