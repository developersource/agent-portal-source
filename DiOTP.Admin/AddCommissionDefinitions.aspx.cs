﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;

namespace Admin
{
    public partial class AddCommissionDefinitions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] != null) {
                User sessionUser = (User)Session["admin"];
                string idString = Request.QueryString["Id"];

                if (idString != "" && idString != null)
                {
                    string query = (@" SELECT * FROM commission_defs where id='" + idString + "' ");
                    int id = Convert.ToInt32(idString);

                    DiOTP.Utility.CommissionDefinitions commissionDefinitionObj = new DiOTP.Utility.CommissionDefinitions();
                    Response responseCommissionDefinition = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);
                    if (responseCommissionDefinition.IsSuccess)
                    {
                        var commissionDyn = responseCommissionDefinition.Data;
                        var responseJSON = JsonConvert.SerializeObject(commissionDyn);
                        List<DiOTP.Utility.CommissionDefinitions> commissionDefinitionList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseJSON);
                        if (commissionDefinitionList.Count == 1)
                        {
                            DiOTP.Utility.CommissionDefinitions commissionDetails = commissionDefinitionList.FirstOrDefault();
                            Id.Value = commissionDetails.id.ToString();
                            commissionName.Value = commissionDetails.name;
                            commissionCode.Value = commissionDetails.code;

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Commission Code not found\");", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseCommissionDefinition.Message + "\");", true);
                    }
                }
            }     
            
        }
    }
}