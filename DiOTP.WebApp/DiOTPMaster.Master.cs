﻿using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Google.Authenticator;
using DiOTP.Service.IService;
using DiOTP.Service;
using System.Text;
using DiOTP.Utility.Helper;
using System.Net.Http;
using System.Configuration;
using DiOTP.WebApp.ServiceCalls;
using static DiOTP.WebApp.ServiceCalls.LanguageManager;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;

namespace DiOTP.WebApp
{
    public partial class DiOTPMaster : System.Web.UI.MasterPage
    {
        private static readonly Lazy<ILanguageDirService> lazyLanguageDirService = new Lazy<ILanguageDirService>(() => new LanguageDirService());

        public static ILanguageDirService ILanguageDirService { get { return lazyLanguageDirService.Value; } }

        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyObj = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyObj.Value; } }

        private static readonly Lazy<IMaHolderClassService> lazyMaHolderClassObj = new Lazy<IMaHolderClassService>(() => new MaHolderClassService());

        public static IMaHolderClassService IMaHolderClassService { get { return lazyMaHolderClassObj.Value; } }

        private static readonly Lazy<IUserOrderCartService> lazyUserOrderCartObj = new Lazy<IUserOrderCartService>(() => new UserOrderCartService());

        public static IUserOrderCartService IUserOrderCartService { get { return lazyUserOrderCartObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderObj.Value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            string languageSelected = "en";
            if (Request.Cookies["DiOTPSettings"] != null)
            {
                HttpCookie myCookie = (HttpCookie)Request.Cookies["DiOTPSettings"];
                if (myCookie.HasKeys)
                {
                    languageSelected = myCookie.Values["Language"].ToString();
                }
            }
            else
            {
                HttpCookie myCookie = new HttpCookie("DiOTPSettings");
                myCookie["Language"] = "en";
                Response.Cookies.Add(myCookie);
            }
            
            string path = HttpContext.Current.Request.Url.PathAndQuery;
            
            if (Session["user"] != null)
            {
                Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                if (IsImpersonate)
                {
                    impersonatedUserLabel.Visible = true;
                    lnkCancel.Visible = true;
                    lnkCancel2.Visible = true;
                    lnkLogout2.Visible = false;
                    lnkLogout.Visible = false;
                }
                else
                {
                    impersonatedUserLabel.Visible = false;
                    lnkCancel.Visible = false;
                    lnkCancel2.Visible = false;
                    lnkLogout2.Visible = true;
                    lnkLogout.Visible = true;
                }

                navbarBrand.HRef = "javascript:;";
                navbarBrandMobile.HRef = "javascript:;";
                User user = (User)(Session["user"]);
                Response responseUser = IUserService.GetSingle(user.Id);
                User temUser = (User)responseUser.Data;
                DateTime dateTime = (DateTime)(Session["CurrentLoginTime"]);
                if (dateTime.ToString("yyyyMMddHHmmss") == temUser.LastLoginDate.Value.ToString("yyyyMMddHHmmss") || ServicesManager.IsImpersonated(Context) || temUser.LastLoginDate.Value.ToString("yyyy").Equals("0001")) {
                    mobileAccount.Visible = false;
                    Response userDBResponse = IUserService.GetSingle(user.Id);
                    if (userDBResponse.IsSuccess)
                    {
                        User userDB = (User)userDBResponse.Data;
                        if (userDB.Status == 0 && userDB.Password.Trim() != "")
                        {
                            Response.Redirect("Deactivate.aspx", false);
                        }
                        if (userDB.Password.Trim() == "" && IsImpersonate)
                        {
                            Response.Redirect("Warning.aspx", false);
                        }
                    }
                    Response responseUOCList = IUserOrderService.GetDataByFilter(" user_id=" + user.Id.ToString() + " and order_type = 1 and order_status = 1 ", 0, 0, true);
                    if (responseUOCList.IsSuccess)
                    {
                        List<UserOrder> L = (List<UserOrder>)responseUOCList.Data;
                        L = L.Where(z => z.Status == 1 && z.OrderType == 1).ToList();
                        if (L.Count != 0)
                        {
                            notificationBell.Visible = true;
                            notification.InnerHtml = "<li>You have pending orders in order status page</li>";
                        }
                        else
                        {
                            notificationBell.Visible = false;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOCList.Message + "\", '');", true);
                    }
                    liLogout.Visible = true;
                    liRegister.Visible = false;
                    liLogout2.Visible = true;
                    liMobilelogin.Visible = false;
                    liLogin.Visible = false;
                    liAccount.Visible = true;
                    Username.InnerHtml = "<i class=' fa fa-user-circle'></i> " + user.Username + " <span class='caret'></span>";
                    usernameMobile.InnerHtml = user.Username;
                    if (Session["isVerified"] != null)
                    {
                        if (Session["isVerified"].ToString() == "1")
                        {
                            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                                {
                                    UserAccount primaryAcc = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();

                                    Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                                    MaHolderReg maHolderReg = new MaHolderReg();
                                    if (responseMHR.IsSuccess)
                                    {
                                        maHolderReg = (MaHolderReg)responseMHR.Data;
                                        primaryAccountClass.InnerHtml = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                                    }
                                    maNumber.InnerHtml = primaryAcc.AccountNo;
                                    primaryAccountButton.Attributes.Remove("class");
                                    primaryAccountButton.Attributes.Add("class", "btn btn-default dropdown-toggle " + (primaryAcc.IsPrinciple == 1 ? "" : "view-only"));
                                    primaryAccountClass.Attributes.Remove("title");
                                    primaryAccountClass.Attributes.Add("title", primaryAcc.AccountNo);
                                }

                                StringBuilder sb = new StringBuilder();
                                userAccounts.ForEach(uA =>
                                {
                                    Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                                    MaHolderReg maHolderReg = new MaHolderReg();
                                    if (responseMHR.IsSuccess)
                                    {
                                        maHolderReg = (MaHolderReg)responseMHR.Data;
                                        Response responseMHCList = IMaHolderClassService.GetDataByPropertyName(nameof(MaHolderClass.Code), maHolderReg.HolderCls, true, 0, 0, false);
                                        if (responseMHCList.IsSuccess)
                                        {
                                            MaHolderClass hc = ((List<MaHolderClass>)responseMHCList.Data).FirstOrDefault();
                                            string name = maHolderReg.Name1;
                                            string accountPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                                            if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT")
                                                name = maHolderReg.Name2;
                                           
                                                sb.Append("<li data-toggle='tooltip' data-account-no='" + uA.AccountNo + "' data-title='" + name + " " + (uA.IsPrinciple == 1 ? "" : "  view only") + "' data-placement='left'><a class='" + (uA.IsPrinciple == 1 ? "" : "view-only") + "' href='/Index.aspx?accountHolderNo=" + uA.AccountNo + "&redirectUrl=" + path + "'>" + (CustomValues.GetAccounPlan(maHolderReg.HolderCls)) + "<br/>" + uA.AccountNo + "</a></li>");
                                        }
                                    }
                                });
                                userAccountsList.InnerHtml = sb.ToString();

                                //If is verified and load the table
                                StringBuilder filter = new StringBuilder();
                                string mainQ = (@"SELECT a.id, a.material_name, a.material_description, a.material_url_path, a.created_date, a.created_by, a.updated_date, a.updated_by, a.is_active, a.status from ");
                                filter.Append(@" agent_trainee_materials a where status = 1 and is_active = 1");

                                int skip = 0, take = 20;

                                Response responseMaterials = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, true);
                                if (responseMaterials.IsSuccess)
                                {
                                    var MaterialsDyn = responseMaterials.Data;
                                    var responseJSON = JsonConvert.SerializeObject(MaterialsDyn);
                                    List<DiOTP.Utility.AgentTraineeMaterial> Materials = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentTraineeMaterial>>(responseJSON);

                                    StringBuilder asb = new StringBuilder();
                                    int index = 1;
                                    {
                                        Materials.ForEach(x =>
                                        {
                                        asb.Append(@"<tr>
                                    <td>" + index + @"</td>
                                            <td><a  target='_blank' href = '" + x.material_url_path + @"'>" + x.material_name + @" <i class='fa fa-eye' aria-hidden='true'></i></a></td>
                                            <td> " + x.material_description + @"
                                    </td>
                                    </tr>");
                                            index++;

                                        });
                                        tbodyAgentTraineeMaterialList.InnerHtml = asb.ToString();
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            accountSelectedDropdown.Visible = false;
                        }
                    }
                    else
                    {
                        accountSelectedDropdown.Visible = false;
                    }
                }
                else {
                    Session.Clear();
                    Session.Abandon();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Concurrent Login Detected','You have been kicked out due to concurrent login detected, Please login again to process.','Index.aspx');", true);
                }
            }
            else
            {
                mobileAccount.Visible = true;
                accountSelectedDropdown.Visible = false;
            }

        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Logout.aspx", false);
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Write("<script>window.close();</script>");
        }

        
    }
}