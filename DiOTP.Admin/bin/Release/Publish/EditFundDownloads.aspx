﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditFundDownloads.aspx.cs" Inherits="Admin.EditFundDownloads" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="editFundDownloads" runat="server">
        <div class="row" id="FormId" data-value="editFundDownloads">
            <div class="col-md-12">
                <div class="row mb-15">
                    <div class="col-md-4">
                        <label>Name:</label>
                    </div>
                    <div class="col-md-8">
                        <asp:HiddenField ID="Id" Value="0" runat="server" ClientIDMode="Static" />
                        <asp:TextBox ID="Name" runat="server" CssClass="document-title form-control" ClientIDMode="Static"></asp:TextBox>
                        <input type="hidden" id="UtmcFundInformationId" name="UtmcFundInformationId" runat="server" clientidmode="static" />
                        <input type="hidden" id="UtmcFundFileTypesDefId" name="UtmcFundFileTypesDefId" runat="server" clientidmode="static" />
                        <input type="hidden" id="CreatedDate" name="CreatedDate" runat="server" clientidmode="static" />
                        <input type="hidden" id="CreatedBy" name="CreatedBy" runat="server" clientidmode="static" />
                        <input type="hidden" id="UpdatedDate" name="UpdatedDate" runat="server" clientidmode="static" />
                        <input type="hidden" id="UpdatedBy" name="UpdatedBy" runat="server" clientidmode="static" />
                        <input type="hidden" id="Status" name="Status" runat="server" clientidmode="static" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>File:</label>
                    </div>
                    <div class="col-md-8">
                        <asp:TextBox ID="Url" runat="server" CssClass="document-title form-control" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                        <div class="fileupload fileupload-new" data-provides="fileupload" id="statusOption" runat="server" clientidmode="static">
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> <span id="btnFileText">Select file</span></span> <i class="fa fa-spinner fa-spin hide"></i>
                                        <asp:FileUpload ID="fuEditDocument" runat="server" ClientIDMode="Static" CssClass="default filePicker" />
                                    </span>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $ui(document).ready(function () {


                function sendFile(file) {
                    var formData = new FormData();
                    formData.append('file', file);
                    $ui.ajax({
                        type: 'post',
                        url: 'fileUploaderDownloads.ashx',
                        data: formData,
                        success: function (status) {
                            console.log(status);
                            $('#btnFileText').text("Select file");
                            $('#fuEditDocument').removeAttr('disabled');
                            $('.fileupload .fa.fa-spinner').addClass('hide');
                            $('#Url').val(status);
                        },
                        processData: false,
                        contentType: false,
                        error: function () {
                            $('.fileupload .fa.fa-spinner').addClass('hide');
                            $('#btnFileText').text("Select file");
                            $('#fuEditDocument').removeAttr('disabled');
                            alert("Whoops something went wrong!");
                        }
                    });
                }

                var _URL = window.URL || window.webkitURL;
                //$ui('.filePicker').unbind('change');
                $ui('.filePicker').change(function () {
                    var file, img;
                    if ((file = this.files[0])) {
                        $('#btnFileText').text("Loading...");
                        $('#fuEditDocument').attr('disabled', 'disabled');
                        $('.fileupload .fa.fa-spinner').removeClass('hide');
                        sendFile(file);
                    }
                });

            });
        </script>
    </form>
</body>
</html>
