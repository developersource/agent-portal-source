﻿using DiOTP.OracleData.ORACLEDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace DiOTP.OracleData
{
    public static class NewRepoClass
    {


        public static Response PostData<T>(T obj)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<T>("insert");
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response PostBulkData<T>(List<T> objs)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<T>("insert");
                });
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response UpdateData<T>(T obj)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<T>("update");
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response UpdateBulkData<T>(List<T> objs)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<T>("update");
                });
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Data = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response DeleteData<T>(Int32 Id)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            Response responseObj = GetSingle<T>(Id, true, null, false);
            if (responseObj.IsSuccess)
            {
                T obj = (T)responseObj.Data;
                try
                {
                    string query = obj.ObjectToQuery<T>("update");
                    OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    response.IsSuccess = true;
                    response.Data = obj;
                }
                catch (Exception ex)
                {
                    response.IsSuccess = false;
                    response.Data = ex.Message;
                    Console.Write(ex.ToString());
                }
                    mySQLDBConnect.CloseConnection();
                return response;
            }
            else
            {
                return responseObj;
            }
        }

        public static Response GetSingle<T>(Int32 Id, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            T obj = (T)Activator.CreateInstance(typeof(T));
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + " where ID=" + Id;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                obj = dt.ToDynamicObject<T>().FirstOrDefault();

                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    otherProperties.ForEach(op =>
                        {
                            if (op.PropertyType.IsGenericType)
                            {
                                if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                    string propName = className + "Id";
                                    Response responseList = NewRepoChildClass.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, responseList.Data);
                                    }
                                }
                            }
                            else if (op.PropertyType.IsClass)
                            {
                                var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                string propName = "Id";
                                string propValue = propPrimary.GetValue(obj).ToString();
                                if (op.PropertyType.Name == className)
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                else
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                var propertyName = op.Name;
                                //if (op.Name.Contains(op.PropertyType.Name))
                                //{
                                //    if (op.Name.EndsWith(op.PropertyType.Name))
                                //    {

                                //    }
                                //}
                                Response responseList = NewRepoChildClass.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                if (responseList.IsSuccess)
                                {
                                    prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                }
                            }
                        });
                }
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response GetData<T>(int skip, int take, bool isOrderByDesc, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            List<T> objs = new List<T>();
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject<T>();
                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    objs.ForEach(obj =>
                    {

                        otherProperties.ForEach(op =>
                        {
                            if (op.PropertyType.IsGenericType)
                            {
                                if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                {
                                    var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                    string propName = className + "Id";
                                    Response responseList = NewRepoChildClass.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, responseList.Data);
                                    }
                                }
                            }
                            else if (op.PropertyType.IsClass)
                            {
                                var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                string propName = "Id";
                                string propValue = propPrimary.GetValue(obj).ToString();
                                if (op.PropertyType.Name == className)
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                else
                                {
                                    propValue = propPrimary.GetValue(obj).ToString();
                                }
                                var propertyName = op.Name;
                                //if (op.Name.Contains(op.PropertyType.Name))
                                //{
                                //    if (op.Name.EndsWith(op.PropertyType.Name))
                                //    {

                                //    }
                                //}
                                Response responseList = NewRepoChildClass.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                if (responseList.IsSuccess)
                                {
                                    prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                }
                            }
                        });
                    });
                }
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response GetCount<T>()
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            var firstProperty = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace == "System").FirstOrDefault();
            Int32 count = 0;
            try
            {
                string databaseTableName = Converter.GetDatabaseName<T>();
                string firstColumnName = Converter.GetColumnNameByPropertyName<T>(firstProperty.Name);
                string query = "select count(" + firstColumnName + ") from " + databaseTableName;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                response.IsSuccess = true;
                response.Data = count;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response GetDataByFilter<T>(string filter, int skip, int take, bool isOrderByDesc, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            List<string> columns = new List<string>();
            if (!isAllProperties)
            {
                properties.ForEach(x =>
                {
                    string columnName = Converter.GetColumnNameByPropertyName<T>(x);
                    if (columnName != "")
                        columns.Add(columnName);
                });
            }
            List<T> objs = new List<T>();
            string databaseTableName = Converter.GetDatabaseName<T>();
            try
            {
                string query = "select " + (isAllProperties ? "*" : String.Join(",", columns.ToArray())) + " from " + databaseTableName + " where " + filter;
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject<T>();
                if (isGetChildren)
                {
                    var className = typeof(T).Name;
                    var otherProperties = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace != "System").ToList();
                    if (!isAllProperties)
                    {
                        otherProperties = otherProperties.Where(y => properties.Any(p => p == y.Name)).ToList();
                    }
                    objs.ForEach(obj =>
                        {

                            otherProperties.ForEach(op =>
                            {
                                if (op.PropertyType.IsGenericType)
                                {
                                    if (op.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                    {
                                        var listObjClass = op.PropertyType.GenericTypeArguments.FirstOrDefault();
                                        var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == "Id").FirstOrDefault();
                                        string propName = className + "Id";
                                        Response responseList = NewRepoChildClass.GetDataByProperty(listObjClass.UnderlyingSystemType, propName, propPrimary.GetValue(obj).ToString(), 0, 0, false);
                                        var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                        if (responseList.IsSuccess)
                                        {
                                            prop.SetValue(obj, responseList.Data);
                                        }
                                    }
                                }
                                else if (op.PropertyType.IsClass)
                                {
                                    var propPrimary = obj.GetType().GetProperties().Where(x => x.Name == ReplaceLastOccurrence(op.Name, op.PropertyType.Name, "")).FirstOrDefault();
                                    string propName = "Id";
                                    string propValue = propPrimary.GetValue(obj).ToString();
                                    if (op.PropertyType.Name == className)
                                    {
                                        propValue = propPrimary.GetValue(obj).ToString();
                                    }
                                    else
                                    {
                                        propValue = propPrimary.GetValue(obj).ToString();
                                    }
                                    var propertyName = op.Name;
                                    //if (op.Name.Contains(op.PropertyType.Name))
                                    //{
                                    //    if (op.Name.EndsWith(op.PropertyType.Name))
                                    //    {

                                    //    }
                                    //}
                                    Response responseList = NewRepoChildClass.GetDataByProperty(op.PropertyType, propName, propValue, 0, 0, false);
                                    var prop = obj.GetType().GetProperties().Where(x => x.Name == op.Name).FirstOrDefault();
                                    if (responseList.IsSuccess)
                                    {
                                        prop.SetValue(obj, ((IList)responseList.Data)[0]);
                                    }
                                }
                            });
                        });
                }
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static Response GetCountByFilter<T>(string filter, int skip, int take)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            var firstProperty = typeof(T).GetProperties().Where(y => y.PropertyType.Namespace == "System").FirstOrDefault();
            Int32 count = 0;
            try
            {
                string databaseTableName = Converter.GetDatabaseName<T>();
                string firstColumnName = Converter.GetColumnNameByPropertyName<T>(firstProperty.Name);
                string query = "select count(" + firstColumnName + ") from " + databaseTableName + " where " + filter;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                response.IsSuccess = true;
                response.Data = count;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int place = Source.LastIndexOf(Find);

            if (place == -1)
                return Source;

            string result = Source.Remove(place, Find.Length).Insert(place, Replace);
            return result;
        }

        public static DataTable GenerateDataTable(string Query)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            DataTable ds = new DataTable();
            try
            {
                string query = Query;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);

            }
            catch (Exception ex)
            {
                Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));

            }
            finally
            {
                mySQLDBConnect.CloseConnection();

            }
            return ds;
        }
        public static Response Inser_Fund_Nav()
        {
            Response response = new Response();
            try
            {
            List<UtmcDailyNavFund> utmcDailyNavFunds = new List<UtmcDailyNavFund>();
            DataTable DS = new DataTable();
                string fromdate = DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString();
                string todate = DateTime.Now.AddDays(1).Year.ToString() + "/" + DateTime.Now.AddDays(1).Month.ToString() + "/" + DateTime.Now.AddDays(1).Day.ToString();

                string SelectQuery = " SELECT A.FUND_ID, A.TRANS_DT, A.UP_SELL, A.UNIT_IN_ISSUE, A.UP_SELL * A.UNIT_IN_ISSUE "
                      + "  FROM UTS.PRICE_HISTORY A INNER JOIN "
                      + "  (SELECT FUND_ID, TRANS_DT, MAX(SEQ_NO)AS MaxRecord FROM UTS.PRICE_HISTORY GROUP BY FUND_ID, TRANS_DT) B "
                      + "  ON A.FUND_ID = B.FUND_ID AND A.TRANS_DT = B.TRANS_DT AND A.SEQ_NO = B.MaxRecord "
                      + "  WHERE A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07', '08', '09') AND "
                      + "  (A.trans_dt between to_date('" + fromdate + "', 'yyyy/mm/dd') "
                      + "  and to_date('" + todate + "', 'yyyy/mm/dd'))  AND A.PR_STATUS = 'C' "
                      + "  ORDER BY A.FUND_ID, A.TRANS_DT ASC ";
             DS = GenerateDataTable(SelectQuery);

            DataManager DB = new DataManager();
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund();
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

                    string IPD_Fund_Code = rdr[0].ToString();

                    DateTime ForamtedDate = Convert.ToDateTime(rdr[1].ToString());

                    //string day = NAVDATECONVERSION.Substring(0, 2);
                    //string month = NAVDATECONVERSION.Substring(3, 3);
                    //string yr = NAVDATECONVERSION.Substring(NAVDATECONVERSION.Length - 2);
                    //string ForamtedDate = yr + "-" + month + "-" + day;

                    DateTime dt = Convert.ToDateTime(ForamtedDate);
                    string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                    string Daily_NAV = rdr[4].ToString();
                    string Daily_Unit_created = rdr[3].ToString();
                    string Daily_Unit_Price = rdr[2].ToString();

                    decimal daily_Unit_Created_EPF = 0.4M;
                    decimal Daily_NAV_EPF = 0.2M;

                    var now = Convert.ToDateTime(ForamtedDate);
                    var startOfMonth = new DateTime(now.Year, now.Month, 1);
                    var DaysInMonth = DateTime.DaysInMonth(now.Year, now.Month);
                    var enddate = new DateTime(now.Year, now.Month, DaysInMonth);

                    string End_date = enddate.ToString("yyyy-MM-dd");

                    string Query = "Insert into utmc_daily_nav_fund("
                        + "EPF_IPD_Code, IPD_Fund_Code, Daily_NAV_Date, Daily_NAV, Report_Date,"
                           + "Daily_Unit_Created, Daily_NAV_EPF, Daily_Unit_Created_EPF, Daily_Unit_Price)"
                             + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + End_date + "','"
                             + Daily_Unit_created + "', " + " '" + Daily_NAV_EPF + "','" + daily_Unit_Created_EPF + "','" + Daily_Unit_Price + "');";

                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {

                        mysql_comm.ExecuteNonQuery();
                        Log.Write("UTMC_018 : " + IPD_Fund_Code + Daily_NAV_Date + "-" + Daily_NAV_EPF + " Inserted", End_date);
                    }
                    utmcDailyNavFund.DailyUnitPrice = !string.IsNullOrEmpty(Daily_Unit_Price) ? Convert.ToDecimal(Daily_Unit_Price) : 0;
                    utmcDailyNavFund.IpdFundCode = EPF_IPD_Code;
                    utmcDailyNavFunds.Add(utmcDailyNavFund);
                    response.IsSuccess = true;
                    response.Data = utmcDailyNavFunds;
                }

                mysql_conn.Close();
            }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetDataByQuery(string query, int skip, int take, bool isOrderByDesc, string orderByColumn, bool isGroupBy, string groupByColumn, bool maintainColumnName = true)
        {
            ORACLEDBConnect mySQLDBConnect = new ORACLEDBConnect();
            Response response = new Response();
            List<dynamic> objs = new List<dynamic>();
            try
            {
                if (isGroupBy)
                {
                    query += " group by " + groupByColumn;
                }
                if (isOrderByDesc)
                {
                    query += " order by " + orderByColumn + " desc";
                }
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                OracleCommand cmd = new OracleCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                OracleDataAdapter dA = new OracleDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject<dynamic>(maintainColumnName);
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
                mySQLDBConnect.CloseConnection();
            return response;
        }

    }
}
