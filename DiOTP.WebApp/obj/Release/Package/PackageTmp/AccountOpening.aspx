﻿<%@ Page Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="AccountOpening.aspx.cs" Inherits="DiOTP.WebApp.AccountOpening" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!DOCTYPE html>

    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <%--<link rel="stylesheet" href="https://www.bootstrapdash.com/demo/wizards-bundle-pro/wizard-4/assets/css/bd-wizard.css" />--%>
    <link rel="stylesheet" href="/Content/acc-open-steps/css/bd-wizard.css?v=2" />
    <style>
        body {
            font-size: initial;
        }

        @media (max-width: 767px) {
            p, label, small, span {
                color: #212529 !important;
            }

            .g-recaptcha {
                top: 0 !important;
                -moz-transform:scale(0.78); 
                -ms-transform:scale(0.78); 
                -o-transform:scale(0.78); 
                -moz-transform-origin:0; 
                -ms-transform-origin:0; 
                -o-transform-origin:0; 
                -webkit-transform:scale(0.78); 
                transform:scale(0.78); 
                -webkit-transform-origin:0 0; 
                transform-origin:0; 
                filter: progid:DXImageTransform.Microsoft.Matrix(M11=0.78,M12=0,M21=0,M22=0.78,SizingMethod='auto expand');
                display:inline-block;
            }
        }

        .container-fluid.no-padding {
            padding-left: 0;
            padding-right: 0;
        }

        .navbar .container, .navbar .container-fluid, .navbar .container-lg, .navbar .container-md, .navbar .container-sm, .navbar .container-xl {
            display: block;
            flex-wrap: unset;
        }

        .navbar-nav {
            flex-direction: initial;
        }

            .navbar-nav .dropdown-menu {
                position: absolute;
            }

        .dropdown-toggle::after {
            display: none;
        }

        .btn.ft-btn {
            font-size: 14px !important;
            font-weight: 600 !important;
            text-align: center !important;
            padding: 3px 20px 4px 20px !important;
            background-color: rgb(176, 143, 29) !important;
            border-color: rgb(176, 143, 29) !important;
            border-radius: 2px !important;
        }

        .collapse:not(.show) {
            display: unset;
        }

        .field-required {
            border-bottom: 1px solid #ff1b1b !important;
        }

        .field-success {
            border-bottom: 1px solid #28a745 !important;
        }

        .tooltip-inner {
            padding: 5px 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid no-padding">
        <main>
            <%--<script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>

            <video id="webcam1" autoplay playsinline width="640" height="480"></video>
<canvas id="canvas1" class="d-none"></canvas>
            <button type="button" id="snapShotFromWebCam">Snap</button>--%>

            <div class="card bd-wizard-card">
                <div class="card-body pb-85">
                    <h1 class="text-grey mb-20"><span id="AccountTypeSpan" runat="server">Individual</span></h1>
                    <div id="wizard">
                        <h3>
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="About You" data-placement="left" data-container="body">
                                    <i class="mdi mdi-account-circle"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Welcome to eApexis</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Identity
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section>
                            <h2 class="text-dark  font-weight-bold">Welcome to eApexis</h2>
                            <hr />
                            <div class="section-border">
                                <div class="row mb-10">
                                    <div class="col-xl-6">
                                        <p class="text-dark text-justify font-weight-bold">
                                        </p>
                                        <p>
                                            Thank you for choosing Apex Investment Services Berhad as your trusted investment partner. We are committed to excellence and sustainability in managing your investment.
                                        </p>
                                        <p>
                                            Please complete the login and account creation process to start your investment with us today.
                                            To be registered as an investor* with us, please have the following soft copy of documents ready:<br />
                                            1. NRIC/Passport<br />
                                            2. Any ONE of the supporting documents such as Driving license, Bank statement, Utility bill or EPF statement/i-Akaun Profile page. Your supporting documents must contain the following:
                                            <br />
                                            <ol type="a">
                                                <li>Your name as per NRIC;<br />
                                                </li>
                                                <li>Your address that is matched to your mailing address stated in the account opening form;<br />
                                                </li>
                                                <li>Must be not more than 3 months old from the account application date.</li>
                                            </ol>
                                        </p>
                                        <%--<p>
                                            To be an investor*, you must be Malaysian / Foreigner, age 18 & above and residing in Malaysia. Please have your NRIC/Passport ready for registration.
                                        </p>--%>
                                        <p>
                                            <small>*For cash investment account only.</small>
                                        </p>
                                        <hr />

                                        <style>
                                            input[type=checkbox] {
                                                margin-right: 5px;
                                            }

                                            .pb-85 {
                                                padding-bottom: 85px;
                                            }

                                            .usChecks > label {
                                                display: block;
                                            }

                                            .usChecks div {
                                                display: inline-block;
                                                width: 100px;
                                            }

                                            .buttonArea {
                                                /*position: absolute;
                                                bottom: 15px;
                                                right: 50px;*/
                                                text-align: right;
                                                margin-top: 15px;
                                            }

                                            label.col-md-3 {
                                                line-height: 30px;
                                                text-align: left;
                                            }

                                            .mobile-prefix {
                                                display: inline-block;
                                                width: 50px;
                                                height: 50px;
                                                line-height: 50px;
                                                text-align: right;
                                            }

                                            #txtMobileNo {
                                                padding-left: 85px;
                                            }
                                        </style>
                                    </div>
                                    <div class="col-xl-6 d-none d-xl-block">
                                        <img src="/Content/acc-open-steps/images/erp_facebook.jpg" alt="applicatin" class="img-fluid" />
                                    </div>
                                </div>

                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep0">Next</button>
                            </div>
                        </section>
                        <h3 id="divAboutYouHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="About You" data-placement="left" data-container="body">
                                    <i class="mdi mdi-information"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">About You</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Name, Nationality, Status, etc.,
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section id="divAboutYou" runat="server">
                            <h2 class="text-dark  font-weight-bold">About You</h2>
                            <hr />
                            <div id="taxResidentHead">
                                <h2 class="text-dark tax-residency-details font-weight-bold">Tax Residency Details</h2>
                                <hr />
                            </div>
                            <div class="section-border">
                                <div class="row">
                                    <div class="col-md-12 checkbox-group-1 usChecks" id="usChecks" runat="server" clientidmode="static">
                                        <label>Do you hold the United States (U.S) citizenship, green card or passport?</label>
                                        <div>
                                            <asp:CheckBox ID="USCGPYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                        </div>
                                        <div>
                                            <asp:CheckBox ID="USCGPNo" runat="server" Text="No" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div id="about-you" class="hide">

                                    <div class="form-group row">
                                        <label for="nric" class="col-md-3">
                                            NRIC / Passport</label>
                                        <br />

                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtNRIC" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter NRIC/ Passport"></asp:TextBox>
                                            <div class=""><small style="font-size: 12px; color: #686868;">Note: NRIC for Malaysian / Passport for Foreigner</small></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nric" class="col-md-3">Mobile No.</label>
                                        <div class="col-md-9">
                                            <div class="position-relative">
                                                <span class="position-absolute mobile-prefix" style="width: 75px;">+60
                                                </span>
                                                <asp:TextBox ID="txtMobileNo" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Mobile No."></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-9 col-md-offset-3" id="mobile-recaptcha">
                                            <div id="Div7" class="g-recaptcha" clientidmode="static" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" data-callback="ValidateStep2" data-expired-callback="recaptchaExpired" runat="server" style="width: 304px;"></div>
                                        </div>
                                    </div>
                                    <div class="row hide" id="requestOTPDiv">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="mobileNo" class="col-md-3">OTP</label>
                                                <div class="col-md-9 position-relative">
                                                    <style>
                                                        .btn.btn-warning {
                                                            color: #ffffff;
                                                            background-color: #f0ad4e;
                                                            border-color: #eea236;
                                                            position: absolute;
                                                            right: 15px;
                                                            margin-top: 0px;
                                                            padding: 13px 10px;
                                                            font-size: 14px;
                                                            font-weight: 600;
                                                        }
                                                    </style>
                                                    <button type="button" class="btn btn-warning" id="btnRequest">Request OTP</button>
                                                    <asp:TextBox ID="txtOTP" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter OTP"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-9">
                                                <small class="" id="lblInfo" runat="server" clientidmode="static"></small>
                                                <div class="mt-10 countdown">
                                                    <span id="countdownLabelText">&nbsp</span>
                                                    <label class="label label-warning hide" style="width: 70px; text-align: center; display: inline-block;" id="countDownTime" runat="server" clientidmode="static"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button" id="btnPreviousStep0">Previous</button>
                                <button class="bd-wizard-btn hide" data-step="next" type="button" id="btnNextStep1">Next</button>
                            </div>
                        </section>
                        <h3 id="divPersonalHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Personal Information" data-placement="left" data-container="body">
                                    <i class="mdi mdi-information"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Personal Information</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Name, Nationality, Status, etc.,
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section class="user-id-section" id="divPersonal" runat="server">


                            <h2 class="text-dark  font-weight-bold">Personal Information</h2>
                            Please provide your Personal details and ensure that your email address is valid as email verification code will be sent to you.
                            <hr />

                            <style>
                                .residentChecks > label {
                                    display: block;
                                }

                                .residentChecks div {
                                    display: inline-block;
                                    width: 200px;
                                }

                                .nationalityChecks > label {
                                    display: block;
                                }

                                .nationalityChecks div {
                                    display: inline-block;
                                    width: 200px;
                                }

                                .btn {
                                    font-size: 15px;
                                }
                            </style>
                            <div class="section-border">
                                <div class="row mb-20">
                                    <div class="col-md-12 checkbox-group-5">
                                        <div class="row">
                                            <label class="col-md-3">Resident of Malaysia</label>
                                            <div class="col-md-9 residentChecks">
                                                <div>
                                                    <asp:CheckBox ID="ROMYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                                </div>
                                                <div id="placeForResidentErr">
                                                    <asp:CheckBox ID="ROMNo" runat="server" Text="No" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-20">
                                    <div class="col-md-12 checkbox-group-10">
                                        <div class="row">
                                            <label class="col-md-3">Nationality</label>
                                            <div class="col-md-9 nationalityChecks">
                                                <div>
                                                    <asp:CheckBox ID="Malaysian" runat="server" Text="Malaysian" ClientIDMode="Static" />
                                                </div>
                                                <div id="placeForNationalityErr">
                                                    <asp:CheckBox ID="NonMalaysian" runat="server" Text="Non-Malaysian" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="title" class="col-md-3">Salutation</label>
                                            <div class="col-md-3">
                                                <select class="form-control" id="title">
                                                    <option value="">Select</option>
                                                    <option value="Dato">Dato</option>
                                                    <option value="Datin">Datin</option>
                                                    <option value="Mr">Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Ms">Ms</option>
                                                    <option value="Mdm">Mdm</option>
                                                    <option value="Dr.">Dr.</option>
                                                    <%--<option value="Others">Others</option>--%>
                                                </select>
                                            </div>
                                            <%--<div class="col-md-3">
                                                <input type="text" name="titleOther" id="titleOther" class="form-control d-none" placeholder="Enter Salutation" />
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="fullName" class="col-md-3">Name</label>
                                            <div class="col-md-9">
                                                <input type="text" name="fullName" id="fullName" class="form-control" placeholder="Enter Name" />
                                                <div class=""><small style="font-size: 12px; color: #686868;">Note: Full Name as per NRIC / Passport</small></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="dob" class="col-md-3">Date of Birth</label>
                                            <div class="col-md-9">
                                                <input type="text" name="dob" id="dob" class="form-control" readonly="readonly" placeholder="Select Date of Birth" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="gender" class="col-md-3">Gender</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="gender">
                                                    <option value="">Select Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="race" class="col-md-3">Race</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="race" runat="server" clientidmode="static">
                                                    <option value="Select">Select Race</option>
                                                    <option value="Malay">Malay</option>
                                                    <option value="Chinese">Chinese</option>
                                                    <option value="Indian">Indian</option>
                                                    <option value="Others">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 hide race-others" id="raceOthers">
                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="txtRaceDesc">Please Specify Race</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="RaceDesc" id="txtRaceDesc" class="form-control" placeholder="Enter Race" />
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="maritalStatus" class="col-md-3">Marital Status</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="maritalStatus">
                                                    <option value="">Select Status</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Widowed">Widowed</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="status" class="col-md-3">Bumiputera Status</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="bumiputraStatus">
                                                    <option value="">Select Status</option>
                                                    <option value="Bumiputera">Bumiputera</option>
                                                    <option value="Non-Bumiputera">Non-Bumiputera</option>
                                                    <option class="hide" value="Non-Malaysian">Non-Malaysian</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row " id="agentCodeDiv">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-3" style="height: 30px;"></div>
                                            <div class="col-md-9" style="height: 40px;">
                                                <small class="" id="Small1" runat="server" clientidmode="static">Please enter the consultant code if you are attended by Unit Trust Consultant.</small>
                                                <div class="mt-10 countdown">
                                                    <span id="agentText">&nbsp</span>
                                                    <label class="label label-warning hide" style="width: 70px; text-align: center; display: inline-block;" id="Label1" runat="server" clientidmode="static"></label>
                                                </div>
                                            </div>
                                            <label for="AgentCode" class="col-md-3" id="agentRemarks">Remarks</label>
                                            <div class="col-md-9 position-relative agentInput">

                                                <asp:TextBox ID="agent" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Please insert the Consultant code" ReadOnly="true" onfocus="this.removeAttribute('readonly');"></asp:TextBox>
                                                <div id="err-txtIntroCode" class="" style="width: 100%; height: 20px; float: left;"></div>
                                            </div>
                                            <div class="col-md-3"></div>

                                        </div>

                                    </div>



                                </div>

                                <h2 class="text-dark  font-weight-bold">User ID  & password creation</h2>
                                <hr />

                                <div class="form-group row">
                                    <label for="email" class="col-md-3">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Enter Password" />
                                    </div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9"><small id="pw-msg" style="font-size: 12px; color: #686868;">Note: Your password require at least <b>ONE</b> capital letter, <b>ONE</b> small letter, <b>ONE</b> number, and the length must between 8 to 16 characters</small></div>
                                </div>

                                <div class="form-group row" style="margin-bottom: 0px;">
                                    <label for="email" class="col-md-3">Re-enter Password</label>
                                    <div class="col-md-9">
                                        <input type="password" name="repassword" id="repassword" class="form-control" placeholder="Re-enter Password" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3" style="padding-left: 15px;"></div>
                                    <div class="col-md-9"><small class="hide" id="passwordNotMatch" style="font-size: 12px; color: #ff1b1b;">Password does not match</small></div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-3">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email" />
                                    </div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9"><small id="email-msg" style="font-size: 12px; color: #686868;">Note: Your email address will be used as your Login ID</small></div>
                                </div>
                                <div class="row " id="requestCodeDiv">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="EmailCode" class="col-md-3">Email Verification Code</label>
                                            <div class="col-md-9 position-relative">
                                                <button type="button" class="btn btn-warning" id="btnCodeRequest">Request Code</button>
                                                <asp:TextBox ID="txtEmailCode" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Email Verification Code"></asp:TextBox>

                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-9">
                                                <small class="" id="lblEmailCode" runat="server" clientidmode="static"></small>
                                                <div class="mt-10 countdown">
                                                    <span id="emailCountdownLabelText">&nbsp</span>
                                                    <label class="label label-warning hide" style="width: 70px; text-align: center; display: inline-block;" id="emailCountDownTime" runat="server" clientidmode="static"></label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                </div>


                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button" id="btnPreviousStep1">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep3">Next</button>
                            </div>
                        </section>
                        <h3 id="divContactHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Mailing & Residential Address" data-placement="left" data-container="body">
                                    <i class="mdi mdi-contact-mail"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Contact Information</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Mailing & Residential Address
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section class="contact-info" id="divContact" runat="server">
                            <h2 class="text-dark  font-weight-bold">Contact Information</h2>
                            <hr />
                            <div class="section-border">

                                <div id="contact-main">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="correspondenceAddress">Residential Address</label>
                                                <input type="text" name="addr1" id="addr1" class="form-control" placeholder="Enter Address Line 1" />
                                                <input type="text" name="addr1" id="addr2" class="form-control" placeholder="Enter Address Line 2" />
                                                <input type="text" name="addr1" id="addr3" class="form-control" placeholder="Enter Address Line 3" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="postCode">Post Code</label>
                                                <input type="text" name="postCode" id="postCode" class="form-control" placeholder="Enter Post Code" />
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <input type="text" id="city" name="city" class="form-control" placeholder="Enter City" />
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <input type="text" id="state" name="city" class="form-control" placeholder="Enter State" />
                                                <%--<select class="form-control" id="state" runat="server" clientidmode="static">
                                            <option value="Select">Select State</option>
                                        </select>--%>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="country">Country</label>
                                                <select class="form-control" id="country" runat="server" clientidmode="static">

                                                    <%--<option value="Malaysia">Malaysia</option>--%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="telNoO">Home Tel. No.</label>
                                            <div>
                                                <span class="position-absolute mobile-prefix" style="width: 75px;">
                                                    <select class="form-control" id="homeFrontNumber" style="padding: .375rem .75rem;">
                                                        <option value="60">+60</option>
                                                        <option value="1">+1</option>
                                                        <option value="20">+20</option>
                                                        <option value="211">+211</option>
                                                        <option value="212">+212</option>
                                                        <option value="213">+213</option>
                                                        <option value="216">+216</option>
                                                        <option value="218">+218</option>
                                                        <option value="220">+220</option>
                                                        <option value="221">+221</option>
                                                        <option value="222">+222</option>
                                                        <option value="223">+223</option>
                                                        <option value="224">+224</option>
                                                        <option value="225">+225</option>
                                                        <option value="226">+226</option>
                                                        <option value="227">+227</option>
                                                        <option value="228">+228</option>
                                                        <option value="229">+229</option>
                                                        <option value="230">+230</option>
                                                        <option value="231">+231</option>
                                                        <option value="232">+232</option>
                                                        <option value="233">+233</option>
                                                        <option value="234">+234</option>
                                                        <option value="235">+235</option>
                                                        <option value="236">+236</option>
                                                        <option value="237">+237</option>
                                                        <option value="238">+238</option>
                                                        <option value="239">+239</option>
                                                        <option value="240">+240</option>
                                                        <option value="241">+241</option>
                                                        <option value="242">+242</option>
                                                        <option value="243">+243</option>
                                                        <option value="244">+244</option>
                                                        <option value="245">+245</option>
                                                        <option value="246">+246</option>
                                                        <option value="247">+247</option>
                                                        <option value="248">+248</option>
                                                        <option value="249">+249</option>
                                                        <option value="250">+250</option>
                                                        <option value="251">+251</option>
                                                        <option value="252">+252</option>
                                                        <option value="253">+253</option>
                                                        <option value="254">+254</option>
                                                        <option value="255">+255</option>
                                                        <option value="256">+256</option>
                                                        <option value="257">+257</option>
                                                        <option value="258">+258</option>
                                                        <option value="260">+260</option>
                                                        <option value="261">+261</option>
                                                        <option value="262">+262</option>
                                                        <option value="263">+263</option>
                                                        <option value="264">+264</option>
                                                        <option value="265">+265</option>
                                                        <option value="266">+266</option>
                                                        <option value="267">+267</option>
                                                        <option value="268">+268</option>
                                                        <option value="269">+269</option>
                                                        <option value="27">+27</option>
                                                        <option value="290">+290</option>
                                                        <option value="291">+291</option>
                                                        <option value="297">+297</option>
                                                        <option value="298">+298</option>
                                                        <option value="299">+299</option>
                                                        <option value="30">+30</option>
                                                        <option value="31">+31</option>
                                                        <option value="32">+32</option>
                                                        <option value="33">+33</option>
                                                        <option value="34">+34</option>
                                                        <option value="350">+350</option>
                                                        <option value="351">+351</option>
                                                        <option value="352">+352</option>
                                                        <option value="353">+353</option>
                                                        <option value="354">+354</option>
                                                        <option value="355">+355</option>
                                                        <option value="356">+356</option>
                                                        <option value="357">+357</option>
                                                        <option value="358">+358</option>
                                                        <option value="359">+359</option>
                                                        <option value="36">+36</option>
                                                        <option value="370">+370</option>
                                                        <option value="371">+371</option>
                                                        <option value="372">+372</option>
                                                        <option value="373">+373</option>
                                                        <option value="374">+374</option>
                                                        <option value="375">+375</option>
                                                        <option value="376">+376</option>
                                                        <option value="377">+377</option>
                                                        <option value="378">+378</option>
                                                        <option value="379">+379</option>
                                                        <option value="380">+380</option>
                                                        <option value="381">+381</option>
                                                        <option value="382">+382</option>
                                                        <option value="385">+385</option>
                                                        <option value="386">+386</option>
                                                        <option value="387">+387</option>
                                                        <option value="388">+388</option>
                                                        <option value="389">+389</option>
                                                        <option value="39">+39</option>
                                                        <option value="40">+40</option>
                                                        <option value="41">+41</option>
                                                        <option value="420">+420</option>
                                                        <option value="421">+421</option>
                                                        <option value="423">+423</option>
                                                        <option value="43">+43</option>
                                                        <option value="44">+44</option>
                                                        <option value="45">+45</option>
                                                        <option value="46">+46</option>
                                                        <option value="47">+47</option>
                                                        <option value="48">+48</option>
                                                        <option value="49">+49</option>
                                                        <option value="500">+500</option>
                                                        <option value="501">+501</option>
                                                        <option value="502">+502</option>
                                                        <option value="503">+503</option>
                                                        <option value="504">+504</option>
                                                        <option value="505">+505</option>
                                                        <option value="506">+506</option>
                                                        <option value="507">+507</option>
                                                        <option value="508">+508</option>
                                                        <option value="509">+509</option>
                                                        <option value="51">+51</option>
                                                        <option value="52">+52</option>
                                                        <option value="53">+53</option>
                                                        <option value="54">+54</option>
                                                        <option value="55">+55</option>
                                                        <option value="56">+56</option>
                                                        <option value="57">+57</option>
                                                        <option value="58">+58</option>
                                                        <option value="590">+590</option>
                                                        <option value="591">+591</option>
                                                        <option value="592">+592</option>
                                                        <option value="593">+593</option>
                                                        <option value="594">+594</option>
                                                        <option value="595">+595</option>
                                                        <option value="596">+596</option>
                                                        <option value="597">+597</option>
                                                        <option value="598">+598</option>
                                                        <option value="599">+599</option>
                                                        <option value="61">+61</option>
                                                        <option value="62">+62</option>
                                                        <option value="63">+63</option>
                                                        <option value="64">+64</option>
                                                        <option value="65">+65</option>
                                                        <option value="66">+66</option>
                                                        <option value="670">+670</option>
                                                        <option value="672">+672</option>
                                                        <option value="673">+673</option>
                                                        <option value="674">+674</option>
                                                        <option value="675">+675</option>
                                                        <option value="676">+676</option>
                                                        <option value="677">+677</option>
                                                        <option value="678">+678</option>
                                                        <option value="679">+679</option>
                                                        <option value="680">+680</option>
                                                        <option value="681">+681</option>
                                                        <option value="682">+682</option>
                                                        <option value="683">+683</option>
                                                        <option value="685">+685</option>
                                                        <option value="686">+686</option>
                                                        <option value="687">+687</option>
                                                        <option value="688">+688</option>
                                                        <option value="689">+689</option>
                                                        <option value="690">+690</option>
                                                        <option value="691">+691</option>
                                                        <option value="692">+692</option>
                                                        <option value="7">+7</option>
                                                        <option value="800">+800</option>
                                                        <option value="808">+808</option>
                                                        <option value="81">+81</option>
                                                        <option value="82">+82</option>
                                                        <option value="84">+84</option>
                                                        <option value="850">+850</option>
                                                        <option value="852">+852</option>
                                                        <option value="853">+853</option>
                                                        <option value="855">+855</option>
                                                        <option value="856">+856</option>
                                                        <option value="86">+86</option>
                                                        <option value="870">+870</option>
                                                        <option value="878">+878</option>
                                                        <option value="880">+880</option>
                                                        <option value="881">+881</option>
                                                        <option value="882">+882</option>
                                                        <option value="883">+883</option>
                                                        <option value="886">+886</option>
                                                        <option value="888">+888</option>
                                                        <option value="90">+90</option>
                                                        <option value="91">+91</option>
                                                        <option value="92">+92</option>
                                                        <option value="93">+93</option>
                                                        <option value="94">+94</option>
                                                        <option value="95">+95</option>
                                                        <option value="960">+960</option>
                                                        <option value="961">+961</option>
                                                        <option value="962">+962</option>
                                                        <option value="963">+963</option>
                                                        <option value="964">+964</option>
                                                        <option value="965">+965</option>
                                                        <option value="966">+966</option>
                                                        <option value="967">+967</option>
                                                        <option value="968">+968</option>
                                                        <option value="970">+970</option>
                                                        <option value="971">+971</option>
                                                        <option value="972">+972</option>
                                                        <option value="973">+973</option>
                                                        <option value="974">+974</option>
                                                        <option value="975">+975</option>
                                                        <option value="976">+976</option>
                                                        <option value="977">+977</option>
                                                        <option value="979">+979</option>
                                                        <option value="98">+98</option>
                                                        <option value="991">+991</option>
                                                        <option value="992">+992</option>
                                                        <option value="993">+993</option>
                                                        <option value="994">+994</option>
                                                        <option value="995">+995</option>
                                                        <option value="996">+996</option>
                                                        <option value="998">+998</option>

                                                    </select></span>
                                                <input type="text" name="telNoH" id="telNoH" class="form-control" placeholder="Enter Tel. No. (Home)" style="padding-left: 30%;" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left" style="width: 30px; display: inline-block;">
                                            <input type="checkbox" id="sameAsResidentialAddr" name="sameAsResidentialAddr" />
                                        </div>
                                        <div class="pull-left" style="display: inline-block; line-height: 19px; margin-bottom: 15px;">
                                            <label class="w-97" for="sameAsResidentialAddr" style="margin-bottom: 0px;">If Mailing Address is same as Residential Address</label>
                                            <small style="font-size: 12px; color: #686868;">Note: Please complete if different from Mailing Address.</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="correspondenceAddress">Mailing Address</label>
                                            <input type="text" name="addr1" id="addr1M" class="form-control" placeholder="Enter Address Line 1" />
                                            <input type="text" name="addr1" id="addr2M" class="form-control" placeholder="Enter Address Line 2" />
                                            <input type="text" name="addr1" id="addr3M" class="form-control" placeholder="Enter Address Line 3" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="postCode">Post Code</label>
                                            <input type="text" name="postCode" id="postCodeM" class="form-control" placeholder="Enter Post Code" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" id="cityM" name="cityM" class="form-control" placeholder="Enter City" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <%--<select class="form-control" id="stateM" runat="server" clientidmode="static">

                                            <option value="Select">Select State</option>
                                        </select>--%>
                                            <input type="text" id="stateM" name="state" class="form-control" placeholder="Enter State" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-control" id="countryM" runat="server" clientidmode="static">

                                                <option value="Select">Select Country</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <br />


                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep5">Next</button>
                            </div>
                        </section>
                        <h3 id="divOccupationHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Occupation Information" data-placement="left" data-container="body">
                                    <i class="mdi mdi-account-network"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Occupation Information</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Occupation, Business, Income, etc.,
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section id="divOccupation" runat="server">
                            <h2 class="text-dark  font-weight-bold">Occupation Information</h2>
                            <hr />
                            <style>
                                .empChecks > label {
                                    display: block;
                                }

                                .empChecks div {
                                    display: inline-block;
                                    width: 135px;
                                }
                            </style>
                            <div class="section-border">
                                <div class="row">
                                    <div class="col-md-12 checkbox-group-2 empChecks">
                                        <div>
                                            <asp:CheckBox ID="Employed" runat="server" Text="Employed" ClientIDMode="Static" />
                                        </div>
                                        <div>
                                            <asp:CheckBox ID="Unemployed" runat="server" Text="Unemployed" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row hide occupation-field">
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="ddlOccupation" id="lblOccupation">Occupation</label>
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlOccupation" runat="server" clientidmode="static">

                                                    <option value="Others">Others (Please Specify)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 hide occupation-others" id="occupationOthers">
                                        <br />
                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="txtOccupationDesc">Please Specify</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="OccupationDesc" id="txtOccupationDesc" class="form-control" placeholder="Enter Occupation" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 hide employed-fields">
                                        <br />
                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="EmployerName">Name of Employer / Name of Company for self employed</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="EmployerName" id="txtEmployerName" class="form-control" placeholder="Name of Employer/Name of Company for self employed" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="employed-fields hide">
                                    <br />

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <label for="ddlNatureOfBusiness">Nature of Business</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select class="form-control" id="ddlNatureOfBusiness">
                                                        <option value="Select">Select Nature of Business</option>
                                                        <option value="Academician">Academician</option>
                                                        <option value="Actuarial">Actuarial</option>
                                                        <option value="Admin/Human Resources">Admin/Human Resources</option>
                                                        <option value="Advertising">Advertising</option>
                                                        <option value="Advisory Firm">Advisory Firm</option>
                                                        <option value="Agriculture & Plantation">Agriculture & Plantation</option>
                                                        <option value="Audit & Taxation">Audit & Taxation</option>
                                                        <option value="Audit Firm">Audit Firm</option>
                                                        <option value="Aviation">Aviation</option>
                                                        <option value="Banking/Financial">Banking/Financial</option>
                                                        <option value="Beauty Treatment Centres / Parlours">Beauty Treatment Centres / Parlours</option>
                                                        <option value="Biotechnology">Biotechnology</option>
                                                        <option value="Building and Construction">Building and Construction</option>
                                                        <option value="Business in Low Density Goods (e.g: Moblie Phones/Clothing)">Business in Low Density Goods (e.g: Moblie Phones/Clothing)</option>
                                                        <option value="Business Centres">Business Centres</option>
                                                        <option value="Casino/Betting/Gambling Related">Casino/Betting/Gambling Related</option>
                                                        <option value="Cable /DTH services">Cable /DTH services</option>
                                                        <option value="Cash Intensive Business (e.g: Convenient Store)">Cash Intensive Business (e.g: Convenient Store)</option>
                                                        <option value="Chemical Engineering">Chemical Engineering</option>
                                                        <option value="Chemistry">Chemistry</option>
                                                        <option value="Clerical/Administrative">Clerical/Administrative</option>
                                                        <option value="Coaching classes/ Training Institutes">Coaching classes/ Training Institutes</option>
                                                        <option value="Construction Agencies  / Contractors">Construction Agencies  / Contractors</option>
                                                        <option value="Corporate Finance/Investment">Corporate Finance/Investment</option>
                                                        <option value="Courier Services">Courier Services</option>
                                                        <option value="Customer Service">Customer Service</option>
                                                        <option value="Education">Education</option>
                                                        <option value="Educational Institutions">Educational Institutions</option>
                                                        <option value="Electricity Generation">Electricity Generation</option>
                                                        <option value="Electronics Engineering">Electronics Engineering</option>
                                                        <option value="Engineering">Engineering</option>
                                                        <option value="Entertainment Outlets/Karaoke/Spa Massage/Internet Café">Entertainment Outlets/Karaoke/Spa Massage/Internet Café</option>
                                                        <option value="Environmental Consultant">Environmental Consultant</option>
                                                        <option value="Environmental Engineering">Environmental Engineering</option>
                                                        <option value="Fashion Designing">Fashion Designing</option>
                                                        <option value="Film / TV Production Agency">Film / TV Production Agency</option>
                                                        <option value="Financial Consultant">Financial Consultant</option>
                                                        <option value="Financial Institutions">Financial Institutions</option>
                                                        <option value="Food Tech/Nutritionist">Food Tech/Nutritionist</option>
                                                        <option value="Food/Beverage">Food/Beverage</option>
                                                        <option value="Licensed Gaming Outlet">Licensed Gaming Outlet</option>
                                                        <option value="General Work">General Work</option>
                                                        <option value="General/Cost Accounting">General/Cost Accounting</option>
                                                        <option value="Geology/Geophysics">Geology/Geophysics</option>
                                                        <option value="Government Body">Government Body</option>
                                                        <option value="Gymkhana">Gymkhana</option>
                                                        <option value="Health Clinics / Fitness Centres">Health Clinics / Fitness Centres</option>
                                                        <option value="Healthcare">Healthcare</option>
                                                        <option value="Hospitals or Nursing  Homes">Hospitals or Nursing  Homes</option>
                                                        <option value="Hotel/Tourism">Hotel/Tourism</option>
                                                        <option value="Hotels / Boarding / Lodging">Hotels / Boarding / Lodging</option>
                                                        <option value="House Keeping Services">House Keeping Services</option>
                                                        <option value="Human Resources">Human Resources</option>
                                                        <option value="Industrial Engineering">Industrial Engineering</option>
                                                        <option value="Information Technology">Information Technology</option>
                                                        <option value="Insurance Services">Insurance Services</option>
                                                        <option value="Investment Banker">Investment Banker</option>
                                                        <option value="Investment Company">Investment Company</option>
                                                        <option value="Journalists/Editors">Journalists/Editors</option>
                                                        <option value="Law/Legal Services">Law/Legal Services</option>
                                                        <option value="Licensed Money Lending">Licensed Money Lending</option>
                                                        <option value="Logistics/Supply Chains">Logistics/Supply Chains</option>
                                                        <option value="Manpower providers / Labour Contractors">Manpower providers / Labour Contractors</option>
                                                        <option value="Manufacturing">Manufacturing</option>
                                                        <option value="Marine">Marine</option>
                                                        <option value="Marketing Services / Agencies">Marketing Services / Agencies</option>
                                                        <option value="Mechanical/Automotive Engineering">Mechanical/Automotive Engineering</option>
                                                        <option value="Media and event management Companies">Media and event management Companies</option>
                                                        <option value="Medical">Medical</option>
                                                        <option value="Military Transactions">Military Transactions</option>
                                                        <option value="Money Remittance">Money Remittance</option>
                                                        <option value="Money Lender">Money Lender</option>
                                                        <option value="Money Services Business / Money Changer">Money Services Business / Money Changer</option>
                                                        <option value="Non-Government Organizarion (NGO) /Charitable body">Non-Government Organizarion (NGO) /Charitable body</option>
                                                        <option value="Offshore Banking/Offshore Trust/Offshore Corporations">Offshore Banking/Offshore Trust/Offshore Corporations</option>
                                                        <option value="Oil/Gas Engineering">Oil/Gas Engineering</option>
                                                        <option value="Optician">Optician</option>
                                                        <option value="Pawn Brokers/Stock Brokers">Pawn Brokers/Stock Brokers</option>
                                                        <option value="Personal Care">Personal Care</option>
                                                        <option value="Pest Control Services">Pest Control Services</option>
                                                        <option value="Plantations">Plantations</option>
                                                        <option value="Police Force">Police Force</option>
                                                        <option value="Printing Press / Printing Agencies">Printing Press / Printing Agencies</option>
                                                        <option value="Precious Stone/Metal Dealer (e.g. Gold/Jewelry)">Precious Stone/Metal Dealer (e.g. Gold/Jewelry)</option>
                                                        <option value="Process Design & Control">Process Design & Control</option>
                                                        <option value="Producer">Producer</option>
                                                        <option value="Property/Real Estate">Property/Real Estate</option>
                                                        <option value="Public relation">Public relation</option>
                                                        <option value="Publisher">Publisher</option>
                                                        <option value="Reseller(includes Wholesalers)">Reseller(includes Wholesalers)</option>
                                                        <option value="Restaurants / Bar">Restaurants / Bar</option>
                                                        <option value="Retailer">Retailer</option>
                                                        <option value="Sales & Marketing">Sales & Marketing</option>
                                                        <option value="Science & Technology">Science & Technology</option>
                                                        <option value="Secretarial">Secretarial</option>
                                                        <option value="Security and Detective Agencies">Security and Detective Agencies</option>
                                                        <option value="Security/Armed Forces">Security/Armed Forces</option>
                                                        <option value="Service Centres / Maintenance Agencies">Service Centres / Maintenance Agencies</option>
                                                        <option value="Sports">Sports</option>
                                                        <option value="Tech & Helpdesk Support">Tech & Helpdesk Support</option>
                                                        <option value="Telecommunication Services">Telecommunication Services</option>
                                                        <option value="Tour and Travel Services">Tour and Travel Services</option>
                                                        <option value="Training and Placement Service Centre">Training and Placement Service Centre</option>
                                                        <option value="Transport">Transport</option>
                                                        <option value="Vehicle Rental Services">Vehicle Rental Services</option>
                                                        <option value="Veterinarian">Veterinarian</option>
                                                        <option value="Youth movement">Youth movement</option>
                                                        <option value="Used automobile/truck/machines part dealer">Used automobile/truck/machines part dealer</option>
                                                        <option value="Import/Export Companies">Import/Export Companies</option>
                                                        <option value="Professional service providers acting as intermediaries (Lawyer, accountants)">Professional service providers acting as intermediaries (Lawyer, accountants)</option>
                                                        <%--<option value="Audit/Accounting/Tax/Legal/Company Secretary">Audit/Accounting/Tax/Legal/Company Secretary</option>
                                                        <option value="Building/Construction Related">Building/Construction Related</option>
                                                        <option value="Business in Low Density Goods">Business in Low Density Goods (e.g: Moblie Phones/Clothing)</option>
                                                        <option value="Cash Intensive Business">Cash Intensive Business (e.g: Restaurant/Convenient Store)</option>
                                                        <option value="Casino/Betting/Gambling Related">Casino/Betting/Gambling Related</option>
                                                        <option value="Education">Education</option>
                                                        <option value="Engineering">Engineering</option>
                                                        <option value="Entertainment Outlets/Karaoke/Spa Massage/Internet Café">Entertainment Outlets/Karaoke/Spa Massage/Internet Café</option>
                                                        <option value="Farming/Fishing/Foresting">Farming/Fishing/Foresting</option>
                                                        <option value="Financial/Capital Market Institution/Intermediary">Financial/Capital Market Institution/Intermediary</option>
                                                        <option value="Government/Regulatory Authority">Government/Regulatory Authority</option>
                                                        <option value="Hotel/Travel Services">Hotel/Travel Services</option>
                                                        <option value="Medical/Health/Science">Medical/Health/Science</option>
                                                        <option value="Money Services Business">Money Services Business (e.g. Remittance Agent/Non-Bank Money Lender)</option>
                                                        <option value="Non-profit organization/Charity">Non-profit organization/Charity</option>
                                                        <option value="Offshore Banking/Offshore Trust">Offshore Banking/Offshore Trust</option>
                                                        <option value="Pawn Brokers/Stock Brokers">Pawn Brokers/Stock Brokers</option>
                                                        <option value="Precious Stone/Metal Dealer">Precious Stone/Metal Dealer (e.g. Gold/Jewelry)</option>
                                                        <option value="Real Estate/Property">Real Estate/Property</option>
                                                        <option value="Telecommunication">Telecommunication</option>
                                                        <option value="Import/Export Companies">Import/Export Companies</option>
                                                        <option value="Military Transactions">Military Transactions</option>
                                                        <option value="Used automobile/truck/machines part dealer">Used automobile/truck/machines part dealer</option>--%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <br />
                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <label for="ddlMonthlyIncome">Monthly Income</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select class="form-control" id="ddlMonthlyIncome" runat="server" clientidmode="static">
                                                        <option value="Select">Select Monthly Income</option>
                                                        <option value="Below 500 MYR">Below 500 MYR</option>
                                                        <option value="MYR 500 to MYR 1,000 MYR">500 MYR to 1,000 MYR</option>
                                                        <option value="MYR 1,001 to MYR 2,000 MYR">1,001 MYR to 2,000 MYR</option>
                                                        <option value="MYR 2,001 to MYR 5,000 MYR">2,001 MYR to 5,000 MYR</option>
                                                        <option value="MYR 5,001 to MYR 10,000 MYR">5,001 MYR to 10,000 MYR</option>
                                                        <option value="MYR 10,001 and above">10,001 MYR and above</option>
                                                        <option value="NIL">NIL</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <h3 class="correspondenceAddress text-dark  font-weight-bold">Office Address</h3>
                                    <div class="row">
                                        <br />
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <%--<label for="txtOfficeAddress1">Addr1</label>--%>
                                                <input type="text" name="Addr1" id="txtOfficeAddress1" class="form-control" placeholder="Address Line 1" />
                                                <input type="text" name="Addr2" id="txtOfficeAddress2" class="form-control" placeholder="Address Line 2" />
                                                <input type="text" name="Addr3" id="txtOfficeAddress3" class="form-control" placeholder="Address Line 3" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtPostCode">Post Code</label>
                                                <input type="text" name="Post Code" id="txtPostCode" class="form-control" placeholder="Enter Post Code" />
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtCity">City</label>
                                                <input type="text" name="City" id="txtCity" class="form-control" placeholder="Enter City" />
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="ddlState">State</label>
                                                <%--<select class="form-control" id="ddlState" runat="server" clientidmode="static">

                                                <option value="Select">Select State</option>
                                            </select>--%>
                                                <input type="text" id="ddlState" name="state" class="form-control" placeholder="Enter State" />
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group row">
                                                <label for="ddlCountry">Country</label>
                                                <select class="form-control" id="ddlCountry" runat="server" clientidmode="static">
                                                    <option value="Select">Select Country</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtTelephone">Office Tel. No.</label>
                                                <div>
                                                    <span class="position-absolute mobile-prefix" style="width: 75px;">
                                                        <select class="form-control" id="officeFrontNumber" style="padding: .375rem .75rem;">
                                                            <option value="60">+60</option>
                                                            <option value="1">+1</option>
                                                            <option value="20">+20</option>
                                                            <option value="211">+211</option>
                                                            <option value="212">+212</option>
                                                            <option value="213">+213</option>
                                                            <option value="216">+216</option>
                                                            <option value="218">+218</option>
                                                            <option value="220">+220</option>
                                                            <option value="221">+221</option>
                                                            <option value="222">+222</option>
                                                            <option value="223">+223</option>
                                                            <option value="224">+224</option>
                                                            <option value="225">+225</option>
                                                            <option value="226">+226</option>
                                                            <option value="227">+227</option>
                                                            <option value="228">+228</option>
                                                            <option value="229">+229</option>
                                                            <option value="230">+230</option>
                                                            <option value="231">+231</option>
                                                            <option value="232">+232</option>
                                                            <option value="233">+233</option>
                                                            <option value="234">+234</option>
                                                            <option value="235">+235</option>
                                                            <option value="236">+236</option>
                                                            <option value="237">+237</option>
                                                            <option value="238">+238</option>
                                                            <option value="239">+239</option>
                                                            <option value="240">+240</option>
                                                            <option value="241">+241</option>
                                                            <option value="242">+242</option>
                                                            <option value="243">+243</option>
                                                            <option value="244">+244</option>
                                                            <option value="245">+245</option>
                                                            <option value="246">+246</option>
                                                            <option value="247">+247</option>
                                                            <option value="248">+248</option>
                                                            <option value="249">+249</option>
                                                            <option value="250">+250</option>
                                                            <option value="251">+251</option>
                                                            <option value="252">+252</option>
                                                            <option value="253">+253</option>
                                                            <option value="254">+254</option>
                                                            <option value="255">+255</option>
                                                            <option value="256">+256</option>
                                                            <option value="257">+257</option>
                                                            <option value="258">+258</option>
                                                            <option value="260">+260</option>
                                                            <option value="261">+261</option>
                                                            <option value="262">+262</option>
                                                            <option value="263">+263</option>
                                                            <option value="264">+264</option>
                                                            <option value="265">+265</option>
                                                            <option value="266">+266</option>
                                                            <option value="267">+267</option>
                                                            <option value="268">+268</option>
                                                            <option value="269">+269</option>
                                                            <option value="27">+27</option>
                                                            <option value="290">+290</option>
                                                            <option value="291">+291</option>
                                                            <option value="297">+297</option>
                                                            <option value="298">+298</option>
                                                            <option value="299">+299</option>
                                                            <option value="30">+30</option>
                                                            <option value="31">+31</option>
                                                            <option value="32">+32</option>
                                                            <option value="33">+33</option>
                                                            <option value="34">+34</option>
                                                            <option value="350">+350</option>
                                                            <option value="351">+351</option>
                                                            <option value="352">+352</option>
                                                            <option value="353">+353</option>
                                                            <option value="354">+354</option>
                                                            <option value="355">+355</option>
                                                            <option value="356">+356</option>
                                                            <option value="357">+357</option>
                                                            <option value="358">+358</option>
                                                            <option value="359">+359</option>
                                                            <option value="36">+36</option>
                                                            <option value="370">+370</option>
                                                            <option value="371">+371</option>
                                                            <option value="372">+372</option>
                                                            <option value="373">+373</option>
                                                            <option value="374">+374</option>
                                                            <option value="375">+375</option>
                                                            <option value="376">+376</option>
                                                            <option value="377">+377</option>
                                                            <option value="378">+378</option>
                                                            <option value="379">+379</option>
                                                            <option value="380">+380</option>
                                                            <option value="381">+381</option>
                                                            <option value="382">+382</option>
                                                            <option value="385">+385</option>
                                                            <option value="386">+386</option>
                                                            <option value="387">+387</option>
                                                            <option value="388">+388</option>
                                                            <option value="389">+389</option>
                                                            <option value="39">+39</option>
                                                            <option value="40">+40</option>
                                                            <option value="41">+41</option>
                                                            <option value="420">+420</option>
                                                            <option value="421">+421</option>
                                                            <option value="423">+423</option>
                                                            <option value="43">+43</option>
                                                            <option value="44">+44</option>
                                                            <option value="45">+45</option>
                                                            <option value="46">+46</option>
                                                            <option value="47">+47</option>
                                                            <option value="48">+48</option>
                                                            <option value="49">+49</option>
                                                            <option value="500">+500</option>
                                                            <option value="501">+501</option>
                                                            <option value="502">+502</option>
                                                            <option value="503">+503</option>
                                                            <option value="504">+504</option>
                                                            <option value="505">+505</option>
                                                            <option value="506">+506</option>
                                                            <option value="507">+507</option>
                                                            <option value="508">+508</option>
                                                            <option value="509">+509</option>
                                                            <option value="51">+51</option>
                                                            <option value="52">+52</option>
                                                            <option value="53">+53</option>
                                                            <option value="54">+54</option>
                                                            <option value="55">+55</option>
                                                            <option value="56">+56</option>
                                                            <option value="57">+57</option>
                                                            <option value="58">+58</option>
                                                            <option value="590">+590</option>
                                                            <option value="591">+591</option>
                                                            <option value="592">+592</option>
                                                            <option value="593">+593</option>
                                                            <option value="594">+594</option>
                                                            <option value="595">+595</option>
                                                            <option value="596">+596</option>
                                                            <option value="597">+597</option>
                                                            <option value="598">+598</option>
                                                            <option value="599">+599</option>
                                                            <option value="61">+61</option>
                                                            <option value="62">+62</option>
                                                            <option value="63">+63</option>
                                                            <option value="64">+64</option>
                                                            <option value="65">+65</option>
                                                            <option value="66">+66</option>
                                                            <option value="670">+670</option>
                                                            <option value="672">+672</option>
                                                            <option value="673">+673</option>
                                                            <option value="674">+674</option>
                                                            <option value="675">+675</option>
                                                            <option value="676">+676</option>
                                                            <option value="677">+677</option>
                                                            <option value="678">+678</option>
                                                            <option value="679">+679</option>
                                                            <option value="680">+680</option>
                                                            <option value="681">+681</option>
                                                            <option value="682">+682</option>
                                                            <option value="683">+683</option>
                                                            <option value="685">+685</option>
                                                            <option value="686">+686</option>
                                                            <option value="687">+687</option>
                                                            <option value="688">+688</option>
                                                            <option value="689">+689</option>
                                                            <option value="690">+690</option>
                                                            <option value="691">+691</option>
                                                            <option value="692">+692</option>
                                                            <option value="7">+7</option>
                                                            <option value="800">+800</option>
                                                            <option value="808">+808</option>
                                                            <option value="81">+81</option>
                                                            <option value="82">+82</option>
                                                            <option value="84">+84</option>
                                                            <option value="850">+850</option>
                                                            <option value="852">+852</option>
                                                            <option value="853">+853</option>
                                                            <option value="855">+855</option>
                                                            <option value="856">+856</option>
                                                            <option value="86">+86</option>
                                                            <option value="870">+870</option>
                                                            <option value="878">+878</option>
                                                            <option value="880">+880</option>
                                                            <option value="881">+881</option>
                                                            <option value="882">+882</option>
                                                            <option value="883">+883</option>
                                                            <option value="886">+886</option>
                                                            <option value="888">+888</option>
                                                            <option value="90">+90</option>
                                                            <option value="91">+91</option>
                                                            <option value="92">+92</option>
                                                            <option value="93">+93</option>
                                                            <option value="94">+94</option>
                                                            <option value="95">+95</option>
                                                            <option value="960">+960</option>
                                                            <option value="961">+961</option>
                                                            <option value="962">+962</option>
                                                            <option value="963">+963</option>
                                                            <option value="964">+964</option>
                                                            <option value="965">+965</option>
                                                            <option value="966">+966</option>
                                                            <option value="967">+967</option>
                                                            <option value="968">+968</option>
                                                            <option value="970">+970</option>
                                                            <option value="971">+971</option>
                                                            <option value="972">+972</option>
                                                            <option value="973">+973</option>
                                                            <option value="974">+974</option>
                                                            <option value="975">+975</option>
                                                            <option value="976">+976</option>
                                                            <option value="977">+977</option>
                                                            <option value="979">+979</option>
                                                            <option value="98">+98</option>
                                                            <option value="991">+991</option>
                                                            <option value="992">+992</option>
                                                            <option value="993">+993</option>
                                                            <option value="994">+994</option>
                                                            <option value="995">+995</option>
                                                            <option value="996">+996</option>
                                                            <option value="998">+998</option>

                                                        </select></span>
                                                    <input type="text" name="Telephone" id="txtTelephone" class="form-control" placeholder="Telephone No. (Office)" style="padding-left: 30%;" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-control border-0"></label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group row">
                                            <%--<div id="captcha" class="g-recaptcha" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep6">Next</button>
                            </div>
                        </section>

                        <h3 id="divFinancialHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Financial Profile" data-placement="left" data-container="body">
                                    <i class="mdi mdi-account-cash"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Financial Profile</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Occupation, Business, Income, etc.,
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section id="divFinancial" runat="server">
                            <h2 class="text-dark  font-weight-bold">Financial Profile</h2>
                            <hr />
                            <style>
                                .info-circle-design i {
                                    color: #059cce;
                                }
                            </style>
                            <div class="section-border">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="ddlPurposeOfTransaction">Purpose of transaction</label>
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlPurposeOfTransaction">
                                                    <option value="Select">Select Purpose of transaction</option>
                                                    <option value="Retirement">Retirement</option>
                                                    <option value="Fund Children's Education">Fund Children's Education</option>
                                                    <option value="Create Wealth/Earn Higher Returns">Create Wealth/Earn Higher Returns</option>
                                                    <option value="Reach financial goals">Reach financial goals (buy home/car/start up business)</option>
                                                    <option value="Others">Others (Please Specify)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="col-md-12 hide purposeOfTransaction-others" id="purposeOfTransactionOthers" style="padding: 0px; margin-top: 25px;">
                                            <div class="form-group">
                                                <div class="col-md-3" style="padding-left: 0 !important;">
                                                    <label for="txtPurposeOfTransaction">Purpose of transaction Description</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" name="OccupationDesc" id="txtPurposeOfTransaction" class="form-control" placeholder="Enter Purpose Of Transaction" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="ddlSourceOfFunds">Source of funds <span class="info-circle-design" data-toggle="tooltip" data-title="Source of funds refer to the origin of particular funds or other assets which are the subject of the establishment of business relations (e.g. the amounts being invested, deposited, or wired as part of the business relations)."><i class="fa fa-info-circle"></i></span></label>
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlSourceOfFunds">
                                                    <option value="Select">Select Source of funds</option>
                                                    <option value="Salary/Pension">Salary/Pension</option>
                                                    <option value="Commission">Commission</option>
                                                    <option value="Insurance/Investment Payout">Insurance/Investment Payout</option>
                                                    <option value="Company's Profit">Company's Profit</option>
                                                    <option value="Credit Facilities/Loan">Credit Facilities/Loan</option>
                                                    <option value="Others">Others (Please Specify)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <style>
                                    .funderChecks > label {
                                        display: block;
                                    }

                                    .funderChecks div {
                                        display: inline-block;
                                        /*width: 135px;*/
                                    }

                                    .funderBenificialChecks > label {
                                        display: block;
                                    }

                                    .funderBenificialChecks div {
                                        display: inline-block;
                                        width: 135px;
                                    }

                                    .fade:not(.show) {
                                        opacity: 1;
                                    }

                                    .tooltip {
                                        font-size: 12px;
                                        text-align: left;
                                    }

                                    .tooltip-inner {
                                        width: auto;
                                        max-width: 300px;
                                        text-align: left;
                                    }
                                </style>
                                <div class="row hide" id="source-others">
                                    <br />
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="ddlRelationship">Relationship</label>
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlRelationship">
                                                    <option value="Select">Select Relationship</option>
                                                    <option value="Parents">Parents</option>
                                                    <option value="Siblings">Siblings</option>
                                                    <option value="Spouse">Spouse</option>
                                                    <option value="Relatives">Relatives</option>
                                                    <option value="Friends">Friends</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="txtFunderName">Name of Funder</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="FunderName" id="txtFunderName" class="form-control" placeholder="Name of Funder" />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="ddlFunderIndustry">Funder's Industry</label>
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlFunderIndustry" runat="server" clientidmode="static">
                                                    <option value="Select">Select Industry</option>
                                                    <option value="Audit/Accounting/Tax/Legal/Company Secretary">Audit/Accounting/Tax/Legal/Company Secretary</option>
                                                    <option value="Building/Construction Related">Building/Construction Related</option>
                                                    <option value="Business in Low Density Goods">Business in Low Density Goods (e.g: Moblie Phones/Clothing)</option>
                                                    <option value="Cash Intensive Business">Cash Intensive Business (e.g: Restaurant/Convenient Store)</option>
                                                    <option value="Casino/Betting/Gambling Related">Casino/Betting/Gambling Related</option>
                                                    <option value="Education">Education</option>
                                                    <option value="Engineering">Engineering</option>
                                                    <option value="Entertainment Outlets/Karaoke/Spa Massage/Internet Café">Entertainment Outlets/Karaoke/Spa Massage/Internet Café</option>
                                                    <option value="Farming/Fishing/Foresting">Farming/Fishing/Foresting</option>
                                                    <option value="Financial/Capital Market Institution/Intermediary">Financial/Capital Market Institution/Intermediary</option>
                                                    <option value="Government/Regulatory Authority">Government/Regulatory Authority</option>
                                                    <option value="Hotel/Travel Services">Hotel/Travel Services</option>
                                                    <option value="Medical/Health/Science">Medical/Health/Science</option>
                                                    <option value="Money Services Business">Money Services Business (e.g. Remittance Agent/Non-Bank Money Lender)</option>
                                                    <option value="Non-profit organization/Charity">Non-profit organization/Charity</option>
                                                    <option value="Offshore Banking/Offshore Trust">Offshore Banking/Offshore Trust</option>
                                                    <option value="Pawn Brokers/Stock Brokers">Pawn Brokers/Stock Brokers</option>
                                                    <option value="Precious Stone/Metal Dealer">Precious Stone/Metal Dealer (e.g. Gold/Jewelry)</option>
                                                    <option value="Real Estate/Property">Real Estate/Property</option>
                                                    <option value="Telecommunication">Telecommunication</option>
                                                    <option value="Import/Export Companies">Import/Export Companies</option>
                                                    <option value="Military Transactions">Military Transactions</option>
                                                    <option value="Used automobile/truck/machines part dealer">Used automobile/truck/machines part dealer</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <div class="checkbox-group-3 funderChecks row">
                                            <div class="pull-left col-md-12" style="display: inline-block; line-height: 19px;">
                                                <label class="w-97" for="" style="margin-bottom: 0px;">Is the funder involved / in the business of / employed by the following activities / employers that are:</label>
                                                <div><small style="font-size: 12px; color: #686868;">Note: Money changers; remittance agents; pawnbrokers; internet-based stored value facility holders</small></div>
                                                <br />
                                                <div class="" style="display: inline-block; width: 135px;">
                                                    <asp:CheckBox ID="chkFunderInvolvedYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                                </div>
                                                <div class="" style="display: inline-block; width: 135px;">
                                                    <asp:CheckBox ID="chkFunderInvolvedNo" runat="server" Text="No" ClientIDMode="Static" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div class="col-md-12 " style="margin-bottom: 10px;">
                                        <div class="checkbox-group-4 funderBenificialChecks row">
                                            <div class="pull-left col-md-12 checkbox-group-11" style="display: inline-block; line-height: 19px;">

                                                <label>Is the funder the beneficial owner of the funds?</label><br />
                                                <div>
                                                    <asp:CheckBox ID="chkFunderBeneficialOwnerYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                                </div>
                                                <div>
                                                    <asp:CheckBox ID="chkFunderBeneficialOwnerNo" runat="server" Text="No" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="txtFundOwnerName">Fund Owner Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="FunderName" id="txtFundOwnerName" class="form-control" placeholder="Fund Owner Name" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-left: 0 !important;">
                                                <label for="ddlEstimatedNetWorth">Estimated NetWorth <span class="info-circle-design" data-toggle="tooltip" data-title="Net-worth refers to the amount by which total assets / joint assets with his or her spouse (e.g. market value of your vehicles, money in your investment accounts, EPF, savings, value of jewellery, etc.) exceed total liabilities / joint liabilities with his or her spouse (e.g. loans, credit cards, etc.). This shall exclude the value of your primary residence."><i class="fa fa-info-circle"></i></span></label>
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlEstimatedNetWorth">
                                                    <option value="Select">Select Estimated NetWorth</option>
                                                    <option value="Up to MYR 100,000">Up to MYR 100,000</option>
                                                    <option value="MYR 100,000 - MYR 999,999">MYR 100,000 - MYR 999,999</option>
                                                    <option value="MYR 1,000,000 - MYR 2,999,999">MYR 1,000,000 - MYR 2,999,999</option>
                                                    <option value="Above MYR 3,000,000">Above MYR 3,000,000</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <%--<div class="row">
                                <div class="col-md-12 checkbox-group-6 funderBenificialChecks">
                                    <label>Are you employed by a Fund Management Company?</label>
                                    <div>
                                        <asp:CheckBox ID="chkFundManagementCompanyYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                    </div>
                                    <div>
                                        <asp:CheckBox ID="chkFundManagementCompanyNo" runat="server" Text="No" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>--%>
                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep7">Next</button>
                            </div>
                        </section>

                        <h3 id="bankDetailsHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Bank Acount Details" data-placement="left" data-container="body">
                                    <i class="mdi mdi-bank-transfer"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Direct Credit Bank Account Details</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Bank name, Account number.
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section id="bankDetails" runat="server">
                            <h2 class="text-dark  font-weight-bold">Direct Credit Bank Account Details</h2>
                            <hr />
                            <style>
                                .bankChecks > label {
                                    display: block;
                                }

                                .bankChecks div {
                                    display: inline-block;
                                    width: 200px;
                                }
                            </style>
                            <%--<div class="row">
                                <div class="col-md-12 checkbox-group-7">
                                    <div class="row">
                                        <small class="col-md-9">Please furnish your bank account details for unit trust redemption proceeds to your account. All redemption proceeds will only be payable to the main account holder and third party payments are strictly not allowed. Please deposit sales proceeds into my bank account?</small>
                                        <div class="col-md-3 bankChecks">
                                            <div class="col-md-6">
                                                <asp:CheckBox ID="chkDepositSalesBankAccountYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-md-6">
                                                <asp:CheckBox ID="chkDepositSalesBankAccountNo" runat="server" Text="No" ClientIDMode="Static" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="section-border">
                                <div class="row">
                                    <div class="col-md-12 checkbox-group-7">
                                        <div class="row">
                                            <small class="col-md-12">Please provide your bank account details for crediting of all future payments (distribution, redemption proceeds and other monies payable) to you. All payments will only be payable to the main account holder and third party payments are strictly not allowed.</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="ddlBankAccountType" class="col-md-3">Bank Account Type</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlBankAccountType">
                                                    <option value="Select Account Type">Select Account Type</option>
                                                    <option value="Savings Account">Saving Account</option>
                                                    <option value="Current Account">Current Account</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="ddlCurrency" class="col-md-3">Currency</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlCurrency" disabled="disabled">

                                                    <option selected="selected" value="MYR">MYR</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="ddlBank" class="col-md-3">Bank Name</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="ddlBank" clientidmode="static" runat="server">
                                                    <option selected="">Select Bank</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="txtFundOwnerName" class="col-md-3">Account Name</label>
                                            <div class="col-md-9">
                                                <input type="text" name="txtBankAccountName" id="txtBankAccountName" class="form-control" readonly="readonly" placeholder="Enter Bank Account Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="txtFundOwnerNum" class="col-md-3">Bank Account No</label>
                                            <div class="col-md-9">
                                                <a href="javascript:;" data-toggle="tooltip" title="Click here to view bank account number guide" style="position: absolute; right: 23px; margin-top: 12px; z-index: 999;">
                                                    <span onclick="helpBankAccount();"><i class="fa fa-info-circle text-info fs-18"></i></span>
                                                </a>
                                                <input type="text" name="txtBankAccountNo" id="txtBankAccountNo" class="form-control" placeholder="Enter Bank Account No" />
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep8">Next</button>
                            </div>
                        </section>

                        <h3 id="divDisclosureHeader" runat="server">
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Disclosure of Identity" data-placement="left" data-container="body">
                                    <i class="mdi mdi-account-card-details"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Disclosure of Identity</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        PEP, CRS
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section id="divDisclosure" runat="server">
                            <h2 class="text-dark  font-weight-bold">Disclosure of Identity</h2>
                            <br />


                            <style>
                                .PEPChecks > label {
                                    display: block;
                                }

                                .PEPChecks div {
                                    display: inline-block;
                                    width: 100px;
                                }
                            </style>
                            <div class="section-border">
                                <h2 class="text-dark  font-weight-bold">PEP - Politically Exposed Person<span class="info-circle-design" data-html="true" data-toggle="tooltip" data-placement="bottom" data-title="Foreign PEP i.e. individual who is or who has been entrusted with prominent public functions by a foreign country, for example Head of State or of government, senior politician, senior government, judicial or military official, senior executive of state owned corporation, important political party official; <br/>- Domestic PEP i.e. individual who is or has been entrusted domestically with prominent public functions, for example Head of State or of government, senior politician, senior government, judicial or military official, senior executive of state owned corporation, important political party official; or <br/>- Person who is or has been entrusted with a prominent function by an international organisation which refers to member of senior management, i.e. director, deputy director and member of the board or equivalent functions. <br/>*The definition of PEP is not intended to cover middle ranking or more junior individual in the foregoing categories." style="margin-left: 10px;"><i class="fa fa-info-circle"></i></span></h2>
                                <div class="row">
                                    <div class="col-md-12 checkbox-group-8 PEPChecks">
                                        <label>Are you Politically Exposed Person (PEP)/ close associate of a PEP/ family member of PEP?</label>
                                        <div>
                                            <asp:CheckBox ID="chkPEPYes" runat="server" Checked="false" Text="Yes" ClientIDMode="Static" />
                                        </div>
                                        <div>
                                            <asp:CheckBox ID="chkPEPNo" runat="server" Checked="false" Text="No" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row hide pep-specify">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtPEPSpecify">If tick yes, please specify</label>
                                            <input type="text" name="PEPSpecify" id="txtPEPSpecify" class="form-control" placeholder="Please specify your position" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtPEPFamilyM">If it is your family member, please state relationship</label>
                                            <input type="text" name="PEPFamilyM" id="txtPEPFamilyM" class="form-control" placeholder="Enter Relationship" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br />

                            <div class="section-border">
                                <h2 class="text-dark  font-weight-bold">CRS - Common Reporting Standard</h2>

                                <style>
                                    .OutMalaysiaChecks > label {
                                        display: block;
                                    }

                                    .OutMalaysiaChecks div {
                                        display: inline-block;
                                        width: 100px;
                                    }

                                    .out-malaysia-yes .tax-residency-box {
                                        padding: 15px;
                                        border: 1px solid rgba(140, 135, 135, 0.39);
                                        margin-bottom: 10px;
                                    }

                                    .tax-residency-remove {
                                        position: absolute;
                                        right: 30px;
                                        top: 10px;
                                        z-index: 9;
                                        cursor: pointer;
                                    }
                                </style>
                                <div class="row">
                                    <div class="col-md-12 checkbox-group-9 OutMalaysiaChecks">
                                        <label>Are you a tax resident in any jurisdiction(s) outside Malaysia?</label>
                                        <div>
                                            <asp:CheckBox ID="chkCRSYes" runat="server" Text="Yes" ClientIDMode="Static" />
                                        </div>
                                        <div>
                                            <asp:CheckBox ID="chkCRSNo" runat="server" Text="No" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div id="multiple-tax-residency-status-row">
                                    <div class="row hide out-malaysia-yes">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="tax-residency-box">
                                                <div class="tax-residency-remove hide">
                                                    <i class="fa fa-times-circle"></i>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="ddlTaxResidencyStatus">Tax Residency Status</label>
                                                            <select class="form-control ddlTaxResidencyStatus" id="ddlTaxResidencyStatus">
                                                                <option value="">Select Tax Residency Status</option>
                                                                <option value="Afghanistan">Afghanistan</option>
                                                                <option value="Åland Islands">Åland Islands</option>
                                                                <option value="Albania">Albania</option>
                                                                <option value="Algeria">Algeria</option>
                                                                <option value="American Samoa">American Samoa</option>
                                                                <option value="Andorra">Andorra</option>
                                                                <option value="Angola">Angola</option>
                                                                <option value="Anguilla">Anguilla</option>
                                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                <option value="Argentina">Argentina</option>
                                                                <option value="Armenia">Armenia</option>
                                                                <option value="Aruba">Aruba</option>
                                                                <option value="Antarctica">Antarctica</option>
                                                                <option value="Australia">Australia</option>
                                                                <option value="Austria">Austria</option>
                                                                <option value="Azerbaijan">Azerbaijan</option>
                                                                <option value="Bahamas">Bahamas</option>
                                                                <option value="Bahrain">Bahrain</option>
                                                                <option value="Bangladesh">Bangladesh</option>
                                                                <option value="Barbados">Barbados</option>
                                                                <option value="Belarus">Belarus</option>
                                                                <option value="Belgium">Belgium</option>
                                                                <option value="Belize">Belize</option>
                                                                <option value="Benin">Benin</option>
                                                                <option value="Bermuda">Bermuda</option>
                                                                <option value="Bhutan">Bhutan</option>
                                                                <option value="Bolivia">Bolivia</option>
                                                                <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                                                <option value="Bosnia-Herzegovina">Bosnia-Herzegovina</option>
                                                                <option value="Botswana">Botswana</option>
                                                                <option value="Bouvet island">Bouvet island</option>
                                                                <option value="Brazil">Brazil</option>
                                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                <option value="British Virgin Islands">British Virgin Islands</option>
                                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                <option value="Bulgaria">Bulgaria</option>
                                                                <option value="Burkina Faso">Burkina Faso</option>
                                                                <option value="Burundi">Burundi</option>
                                                                <option value="Cambodia">Cambodia</option>
                                                                <option value="Cameroon">Cameroon</option>
                                                                <option value="Canada">Canada</option>
                                                                <option value="Cape Verde">Cape Verde</option>
                                                                <option value="Cayman Islands">Cayman Islands</option>
                                                                <option value="Central African Republic">Central African Republic</option>
                                                                <option value="Chad">Chad</option>
                                                                <option value="Chile">Chile</option>
                                                                <option value="China">China</option>
                                                                <option value="Christmas Island">Christmas Island</option>
                                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                                <option value="Colombia">Colombia</option>
                                                                <option value="Comoros">Comoros</option>
                                                                <option value="Congo (Brazzaville)">Congo (Brazzaville)</option>
                                                                <option value="Congo, the Democratic Republic">Congo, the Democratic Republic</option>
                                                                <option value="Cook Islands">Cook Islands</option>
                                                                <option value="Costa Rica">Costa Rica</option>
                                                                <option value="Cote D'Ivoire">Cote D'Ivoire</option>
                                                                <option value="Croatia">Croatia</option>
                                                                <option value="Cuba">Cuba</option>
                                                                <option value="Curacao">Curacao</option>
                                                                <option value="Cyprus">Cyprus</option>
                                                                <option value="Czech Republic">Czech Republic</option>
                                                                <option value="Denmark">Denmark</option>
                                                                <option value="Djibouti">Djibouti</option>
                                                                <option value="Dominica">Dominica</option>
                                                                <option value="Dominican Republic">Dominican Republic</option>
                                                                <option value="East Timor">East Timor</option>
                                                                <option value="Ecuador">Ecuador</option>
                                                                <option value="Egypt">Egypt</option>
                                                                <option value="El Salvador">El Salvador</option>
                                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                <option value="Eritrea">Eritrea</option>
                                                                <option value="Estonia">Estonia</option>
                                                                <option value="Ethiopia">Ethiopia</option>
                                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                                <option value="Faroe islands">Faroe islands</option>
                                                                <option value="Fiji">Fiji</option>
                                                                <option value="Finland">Finland</option>
                                                                <option value="France">France</option>
                                                                <option value="French Guiana">French Guiana</option>
                                                                <option value="French Polynesia">French Polynesia</option>
                                                                <option value="Gabon">Gabon</option>
                                                                <option value="Gambia">Gambia</option>
                                                                <option value="Gaza Strip">Gaza Strip</option>
                                                                <option value="Georgia">Georgia</option>
                                                                <option value="Germany">Germany</option>
                                                                <option value="Ghana">Ghana</option>
                                                                <option value="Gibraltar">Gibraltar</option>
                                                                <option value="Greece">Greece</option>
                                                                <option value="Greenland">Greenland</option>
                                                                <option value="Grenada">Grenada</option>
                                                                <option value="Guadeloupe">Guadeloupe</option>
                                                                <option value="Guam">Guam</option>
                                                                <option value="Guatemala">Guatemala</option>
                                                                <option value="Guernsey">Guernsey</option>
                                                                <option value="Guinea">Guinea</option>
                                                                <option value="Guinea Bissau">Guinea Bissau</option>
                                                                <option value="Guyana">Guyana</option>
                                                                <option value="Haiti">Haiti</option>
                                                                <option value="Honduras">Honduras</option>
                                                                <option value="Hong Kong">Hong Kong</option>
                                                                <option value="Hungary">Hungary</option>
                                                                <option value="Iceland">Iceland</option>
                                                                <option value="India">India</option>
                                                                <option value="Indonesia">Indonesia</option>
                                                                <option value="Iran">Iran</option>
                                                                <option value="Iraq">Iraq</option>
                                                                <option value="Ireland">Ireland</option>
                                                                <option value="Isle Of Man">Isle Of Man</option>
                                                                <option value="Israel">Israel</option>
                                                                <option value="Italy">Italy</option>
                                                                <option value="Jamaica">Jamaica</option>
                                                                <option value="Japan">Japan</option>
                                                                <option value="Jersey">Jersey</option>
                                                                <option value="Jordan">Jordan</option>
                                                                <option value="Kazakhstan">Kazakhstan</option>
                                                                <option value="Kenya">Kenya</option>
                                                                <option value="Kiribati">Kiribati</option>
                                                                <option value="Kosovo">Kosovo</option>
                                                                <option value="Kuwait">Kuwait</option>
                                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                                <option value="Latvia">Latvia</option>
                                                                <option value="Lebanon">Lebanon</option>
                                                                <option value="Lesotho">Lesotho</option>
                                                                <option value="Liberia">Liberia</option>
                                                                <option value="Libya">Libya</option>
                                                                <option value="Liechtenstein">Liechtenstein</option>
                                                                <option value="Lithuania">Lithuania</option>
                                                                <option value="Luxembourg">Luxembourg</option>
                                                                <option value="Macau">Macau</option>
                                                                <option value="Macedonia">Macedonia</option>
                                                                <option value="Madagascar">Madagascar</option>
                                                                <option value="Malawi">Malawi</option>
                                                                <option value="Maldives">Maldives</option>
                                                                <option value="Mali">Mali</option>
                                                                <option value="Malta">Malta</option>
                                                                <option value="Marshall Islands">Marshall Islands</option>
                                                                <option value="Martinique">Martinique</option>
                                                                <option value="Mauritania">Mauritania</option>
                                                                <option value="Mauritius">Mauritius</option>
                                                                <option value="Mayotte">Mayotte</option>
                                                                <option value="Mexico">Mexico</option>
                                                                <option value="Micronesia">Micronesia</option>
                                                                <option value="Moldova">Moldova</option>
                                                                <option value="Monaco">Monaco</option>
                                                                <option value="Mongolia">Mongolia</option>
                                                                <option value="Montenegro">Montenegro</option>
                                                                <option value="Montserrat">Montserrat</option>
                                                                <option value="Morocco">Morocco</option>
                                                                <option value="Mozambique">Mozambique</option>
                                                                <option value="Myanmar">Myanmar</option>
                                                                <option value="Namibia">Namibia</option>
                                                                <option value="Nauru">Nauru</option>
                                                                <option value="Nepal">Nepal</option>
                                                                <option value="Netherlands">Netherlands</option>
                                                                <option value="New Caledonia">New Caledonia</option>
                                                                <option value="New Zealand">New Zealand</option>
                                                                <option value="Nicaragua">Nicaragua</option>
                                                                <option value="Niger">Niger</option>
                                                                <option value="Nigeria">Nigeria</option>
                                                                <option value="Niue">Niue</option>
                                                                <option value="Norfolk Island">Norfolk Island</option>
                                                                <option value="North Korea">North Korea</option>
                                                                <option value="North Mariana Islands">North Mariana Islands</option>
                                                                <option value="Norway">Norway</option>
                                                                <option value="Oman">Oman</option>
                                                                <option value="Pakistan">Pakistan</option>
                                                                <option value="Palau">Palau</option>
                                                                <option value="Panama">Panama</option>
                                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                                <option value="Paraguay">Paraguay</option>
                                                                <option value="Peru">Peru</option>
                                                                <option value="Philippines">Philippines</option>
                                                                <option value="Pitcairn">Pitcairn</option>
                                                                <option value="Poland">Poland</option>
                                                                <option value="Portugal">Portugal</option>
                                                                <option value="Puerto Rico">Puerto Rico</option>
                                                                <option value="Qatar">Qatar</option>
                                                                <option value="Réunion">Réunion</option>
                                                                <option value="Romania">Romania</option>
                                                                <option value="Russian Federation">Russian Federation</option>
                                                                <option value="Rwanda">Rwanda</option>
                                                                <option value="Saint Berthélemy">Saint Berthélemy</option>
                                                                <option value="Saint Helena, Ascension and Trista">Saint Helena, Ascension and Trista</option>
                                                                <option value="Saint Martin (French part)">Saint Martin (French part)</option>
                                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                                <option value="Samoa">Samoa</option>
                                                                <option value="San Marino">San Marino</option>
                                                                <option value="Sao Tome & Prin.">Sao Tome & Prin.</option>
                                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                                <option value="Senegal">Senegal</option>
                                                                <option value="Serbia">Serbia</option>
                                                                <option value="Seychelles">Seychelles</option>
                                                                <option value="Sierra Leone">Sierra Leone</option>
                                                                <option value="Singapore">Singapore</option>
                                                                <option value="Slovakia">Slovakia</option>
                                                                <option value="Slovenia">Slovenia</option>
                                                                <option value="Solomon Islands">Solomon Islands</option>
                                                                <option value="Somalia">Somalia</option>
                                                                <option value="South Africa">South Africa</option>
                                                                <option value="South Korea">South Korea</option>
                                                                <option value="South Sudan">South Sudan</option>
                                                                <option value="Spain">Spain</option>
                                                                <option value="Sri Lanka">Sri Lanka</option>
                                                                <option value="St Kitts & Nevis">St Kitts & Nevis</option>
                                                                <option value="St Lucia">St Lucia</option>
                                                                <option value="St Maarten">St Maarten</option>
                                                                <option value="St Vincent & Gren">St Vincent & Gren</option>
                                                                <option value="S.Georgia and S.Sandwich IsIs">S.Georgia and S.Sandwich IsIs</option>
                                                                <option value="Sudan">Sudan</option>
                                                                <option value="Suriname">Suriname</option>
                                                                <option value="Svalbard and Mayen">Svalbard and Mayen</option>
                                                                <option value="Swaziland (Eswatini)">Swaziland (Eswatini)</option>
                                                                <option value="Sweden">Sweden</option>
                                                                <option value="Switzerland">Switzerland</option>
                                                                <option value="Syria">Syria</option>
                                                                <option value="Taiwan">Taiwan</option>
                                                                <option value="Tajikistan">Tajikistan</option>
                                                                <option value="Tanzania">Tanzania</option>
                                                                <option value="Thailand">Thailand</option>
                                                                <option value="Timor-Leste">Timor-Leste</option>
                                                                <option value="Togo">Togo</option>
                                                                <option value="Tokelau">Tokelau</option>
                                                                <option value="Tonga">Tonga</option>
                                                                <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                                <option value="Tunisia">Tunisia</option>
                                                                <option value="Turkey">Turkey</option>
                                                                <option value="Turkmenistan">Turkmenistan</option>
                                                                <option value="Turks & Caicos">Turks & Caicos</option>
                                                                <option value="Tuvalu">Tuvalu</option>
                                                                <option value="Uganda">Uganda</option>
                                                                <option value="Ukraine">Ukraine</option>
                                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                                <option value="United Kingdom">United Kingdom</option>
                                                                <option value="United States">United States</option>
                                                                <option value="United States Virgin Islands">United States Virgin Islands</option>
                                                                <option value="Uruguay">Uruguay</option>
                                                                <option value="Uzbekistan">Uzbekistan</option>
                                                                <option value="Vanuatu">Vanuatu</option>
                                                                <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                                                                <option value="Venezuela">Venezuela</option>
                                                                <option value="Vietnam">Vietnam</option>
                                                                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                                <option value="West Bank (Palestinian Territory, Occupied)">West Bank (Palestinian Territory, Occupied)</option>
                                                                <option value="Western Sahara">Western Sahara</option>
                                                                <option value="Yemen">Yemen</option>
                                                                <option value="Zambia">Zambia</option>
                                                                <option value="Zimbabwe">Zimbabwe</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 checkbox-parent">
                                                        <div class="form-group">
                                                            <div class="col-md-12 tin-available">
                                                                <div class="form-group">
                                                                    <label for="txtTINNum">Please indicate your Taxpayer Identification number (TIN)</label>
                                                                    <input type="text" name="txtTINNum" id="txtTINNum" class="form-control txtTINNum" placeholder="Enter TIN No" />
                                                                </div>
                                                            </div>
                                                            <label>
                                                                <input type="checkbox" class="chkNoTINNum" id="chkNoTINNum" />
                                                                Click here if TIN number is unavailable
                                                            </label>
                                                            <div class="col-md-12 hide no-TIN" id="noTIN">
                                                                <div class="form-group row">
                                                                    <div class="col-md-4">
                                                                        <label for="txtnoTIN">Reason of unable to provide TIN number</label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <%--<input type="text" name="NoTINNum" id="txtnoTIN" class="form-control txtnoTIN" placeholder="Enter Reason" />--%>
                                                                        <select class="form-control txtnoTIN" id="txtnoTIN">
                                                                            <option value="Select">Select Reason</option>
                                                                            <option value="The country/jurisdiction where the Account Holder is resident does not issue TINs to its residents">Reason A</option>
                                                                            <option value="The Account Holder is otherwise unable to obtain a TIN or equivalent number">Reason B</option>
                                                                            <option value="No TIN is required">Reason C</option>
                                                                        </select>
                                                                        <input type="text" name="optionBReason" id="optionBReason" class="form-control optionBReason hide" placeholder="Enter reason for option B" style="margin-top: 10px;" />
                                                                        <div>
                                                                            <div class="col-md-9"><small style="font-size: 12px; color: #686868;"><b>Reason A:</b> The country/jurisdiction where the Account Holder is resident does not issue TINs to its residents.</small></div>
                                                                            <div class="col-md-9"><small style="font-size: 12px; color: #686868;"><b>Reason B:</b> The Account Holder is otherwise unable to obtain a TIN or equivalent number. (Please explain why you are unable to obtain a TIN if you have selected this reason)</small></div>
                                                                            <div class="col-md-9"><small style="font-size: 12px; color: #686868;"><b>Reason C:</b> No TIN is required. (Only select this reason if the domestic law of the relevant jurisdiction does not require the collection of the TIN issued by such jurisdiction)</small></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%--<asp:CheckBox ID="chkNoTINNum" CssClass="chkNoTINNum" runat="server" Text="Check here if No TIN Number is avaliable" ClientIDMode="Static" />--%>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="hide" type="button" id="addTaxResidencyStatus">Add Additional Tax Residency Status</button>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep9">Next</button>
                            </div>
                        </section>

                        <h3>
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Upload Documents" data-placement="left" data-container="body">
                                    <i class="mdi mdi-file-upload"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Upload Documents</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        NRIC, Selfie and other documents.
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section>
                            <h2 class="text-dark  font-weight-bold">Upload Documents</h2>
                            <hr />
                            <style>
                                label.file-upload {
                                    display: block;
                                    border: 1px solid #d3d3d3;
                                    padding: 15px;
                                }

                                    label.file-upload span:not(.account-type) {
                                        display: block;
                                        height: 250px;
                                        overflow-y: scroll;
                                    }

                                    label.file-upload i.fa-file-picture-o {
                                        font-size: 100px;
                                        line-height: 200px;
                                        opacity: 0.5;
                                    }

                                #signature-div span.sign {
                                    display: block;
                                    border: 1px solid #d3d3d3;
                                    padding: 15px;
                                }

                                #signature-div label {
                                    border: none;
                                    padding: 0;
                                }

                                .multi {
                                    width: 100px;
                                    display: inline-block;
                                }

                                label.file-upload .multi i {
                                    font-size: 100px;
                                    line-height: 100px;
                                }

                                label.file-upload span::-webkit-scrollbar {
                                    width: 3px;
                                }

                                #webcam .btn-info {
                                    position: absolute;
                                    z-index: 9;
                                    margin-left: 0;
                                    margin-top: 10px;
                                }

                                    #webcam .btn-info#try-snap-again {
                                        position: absolute;
                                        z-index: 9;
                                        margin-left: 0;
                                        margin-top: 10px;
                                        bottom: 10px;
                                        left: calc(50% - 34.0025px);
                                    }

                                button#snap {
                                    left: calc(50% - 34.0025px);
                                    /*bottom: 0;*/
                                    bottom: 75px;
                                }

                                #upload-row [class*='col-'] {
                                    margin-bottom: 15px;
                                }

                                .w-97 {
                                    width: 97%;
                                }

                                #webcam-pic {
                                    height: 254px;
                                    overflow-y: scroll;
                                    border: 1px solid #d3d3d3;
                                    padding: 15px;
                                }

                                #video {
                                    height: 350px;
                                    overflow-y: scroll;
                                    border: 1px solid #d3d3d3;
                                    padding: 15px;
                                }
                                .account-type {
                                    display: inline-block !important;
                                }
                            </style>
                            <div class="section-border">
                                <div class="row">
                                    <div class="col-md-12 checkbox-group-7">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <small>To complete your online account opening, you are required to upload below documents.</small>
                                            </div>
                                            <div class="col-md-12">
                                                <small class="pull-left" style="display: block; width: 50px;">Note:</small>
                                                <small class="pull-right" style="display: block; width: calc(100% - 50px);">
                                                    <small style="font-size: 12px; color: #686868;">-The NRIC and signature image must be clear with the white background.</small><br />
                                                    <small style="font-size: 12px; color: #686868;">-The selfie must be captured with you holding your NRIC (both your face and NRIC must be clear and readable).</small><br />
                                                    <small style="font-size: 12px; color: #686868;">-If your file size is too big, try doing a screenshot/printscreen of the large document to reduce the file size and try re-uploading again.</small><br />
                                                    <small style="font-size: 12px; color: #686868;">-File Type: .pdf, .jpg, .jpeg, .png </small>
                                                    <br />
                                                    <small style="font-size: 12px; color: #686868;">-Maximum File Size: 2MB</small><br />
                                                    <small style="font-size: 12px; color: #686868;">-Minimum File Size: 20KB</small>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row" id="upload-row">


                                    <div class="col-md-12">
                                        <label for="uploadNRICFrontPage" class="file-upload">
                                            Upload Copy of <span id="IdFrontAccountType" runat="server" class="account-type" style="height: 20px !important">Principal</span> Applicant's NRIC/Passport (Front):
                                            <br />
                                            <small>(click here to upload)</small><br />
                                            <small>Note: jpeg/png/pdf</small>
                                            <span style="height: 180px;">
                                                <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                            </span>
                                        </label>
                                        <input type="file" name="uploadNRIC" id="uploadNRICFrontPage" class="form-control hide" accept="image/*,application/pdf" />

                                    </div>
                                    <div class="col-md-12">
                                        <label for="uploadNRICBackPage" class="file-upload">
                                            Upload Copy of <span id="IDBackAccountType" runat="server" class="account-type" style="height: 20px !important">Principal</span> Applicant's NRIC (Back):
                                        <br />
                                            <small>(click here to upload)</small><br />
                                            <small>Note: jpeg/png/pdf</small>
                                            <span style="height: 180px;">
                                                <i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>
                                            </span>
                                        </label>
                                        <input type="file" name="uploadNRIC" id="uploadNRICBackPage" class="form-control hide" accept="image/*,application/pdf" />
                                    </div>
                                    <div class="col-md-12 hide" id="existing-selfie">
                                        <label for="uploadSelfie" class="file-upload">
                                            Upload <span id="selfisAccountType" runat="server" class="account-type" style="height: 20px !important">Principal</span> Applicant's selfie with NRIC/Passport (Front):
                                        <br />
                                            <small>(click here to snap)</small>
                                            <br />
                                            <small>Note: jpeg/png/pdf</small>
                                            <span>
                                                <i class="fa fa-file-image-o"></i>
                                            </span>
                                        </label>
                                        <input type="hidden" name="uploadSelfie" id="uploadSelfie" class="form-control" />
                                        <input type="hidden" name="uploadSelfieFileName" id="uploadSelfieFileName" class="form-control" />
                                        <input type="hidden" name="noCam" id="noCam" />
                                    </div>
                                    <%--<div class="col-md-6 hide" id="no-cam-div">
                                    <label for="uploadSelfieFile" class="file-upload">
                                        Upload Selfie with NRIC
                                        <br />
                                        <small>(click here to upload)</small>
                                        <small>Note: jpeg/png/pdf</small>
                                        <span>
                                            <i class="fa fa-file-image-o"></i>
                                        </span>
                                    </label>
                                    <input type="file" name="uploadSelfieFile" id="uploadSelfieFile" class="form-control hide" />

                                </div>--%>
                                    <div class="col-md-12 text-center" id="webcam">
                                        <button type="button" id="try-snap-again" class="btn btn-info hide">Try Snap again</button>
                                        <label>Upload
                                            <span id="selfis2AccountType" runat="server" class="account-type" style="height: 20px !important">Principal</span>
                                            Applicant's selfie with NRIC/Passport (Front):</label>
                                        <div class="position-relative text-center hide" id="webcam-pic" style="">
                                            <canvas id="canvas" width="640" height="480" style="position: absolute; width: 100%; max-width: 800px;"></canvas>
                                            <img id="inputImg" src="" style="max-width: 800px; width: 100%;" />
                                        </div>
                                        <div class="row" id="cam-option">
                                            <div class="col-md-12">
                                                <video id="video" width="640" height="480" autoplay></video>
                                                <button type="button" id="snap" class="btn btn-info">Snap Photo</button>
                                                <p style="margin-bottom: 15px;">(or)</p>
                                            </div>
                                        </div>
                                        <div class="row" id="upload-selfie-option">
                                            <div class="col-md-6 col-md-offset-3 text-center">
                                                <input type="file" name="uploadSelfieFile" id="uploadSelfieFile" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="col-md-6 hide">
                                    <label for="uploadSelfie" class="file-upload">
                                        Upload Selfie with NRIC
                                        <br />
                                        <small>(click here to upload)</small>
                                        <span>
                                            <i class="fa fa-file-image-o"></i>
                                        </span>
                                    </label>
                                    <input type="file" name="uploadSelfie" id="uploadSelfie" class="form-control hide" accept="image/*" />
                                </div>--%>
                                    <div class="col-md-12" id="signature-div">
                                        <div id="helpBank" class="bank-attachment-modal overlay hide">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="guide-popup">

                                                        <p class="text-justify"></p>
                                                        <img src="Content/MyImage/Sample Signature.jpg" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <span class="sign">
                                            <label for="uploadSignature" class="file-upload">
                                                Upload
                                                <span id="accTypeSign" runat="server" class="account-type">Principal</span>
                                                Applicant's signature (Black Ink & Blue Ink):
                                                <a href="javascript:;" data-toggle="tooltip" title="Click here to view guideline for attaching bank details file" onclick="helpBank();">
                                                    <i class="fa fa-info-circle text-info fs-16"></i>
                                                </a>
                                                <br />
                                                <small>(click here to upload)</small><br />
                                                <small>Note: jpeg/png/pdf</small>
                                                <span style="height: 180px;">
                                                    <i class="fa fa-file-image-o" style="max-height: 180px;"></i>
                                                </span>
                                            </label>
                                            <input type="file" name="uploadSignature" id="uploadSignature" class="form-control hide" accept="image/*,application/pdf" />

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="checkbox" id="chkSignature" name="chkSignature" style="float: left;" />
                                                    <label for="chkSignature" class="w-97">I hereby confirm that my signature uploaded is for the purpose of submitting my non face-to-face online unit trust investment application as received by me from Apex Investment Services Berhad.</label>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-md-12" id="supporting">

                                        <label for="uploadSupporting" class="file-upload">
                                            Supporting document 
                                            <a class="info-circle-design" data-toggle="tooltip" data-title="Any ONE of the supporting documents such as Driving license, Bank statement, Utility bill or EPF statement/i-Akaun Profile page. 
                                            Your supporting documents must contain the following:
	                                        a) Your name as per NRIC;
	                                        b) Your address that is matched to your mailing address stated in the account opening form;
	                                        c) Must be not more than 3 months old from the account application date."
                                                style="right: 23px; margin-top: 4px; z-index: 999; margin-left: 6px;"><i class="fa fa-info-circle"></i></a>
                                            <br />
                                            <small>(click here to upload)</small>
                                            <span style="height: 180px;">
                                                <i class="fa fa-file-image-o"></i>
                                            </span>
                                        </label>
                                        <input type="file" multiple="multiple" name="uploadSupporting" id="uploadSupporting" class="form-control hide" accept="image/*,application/pdf" />
                                    </div>
                                    <div class="col-md-12 hide additional-docs" id="additional">

                                        <label for="uploadAdditional" class="file-upload">
                                            Additional document 
                                            <a class="info-circle-design" data-toggle="tooltip" data-title="Additional Documents:
                                            i. Latest three (3) months’ pay slip/ latest bank statement / latest income statement, or;
                                            ii. Tax statement, i.e. latest J/EA Form, or;
                                            iii. Letter of confirmation of employment."
                                                style="right: 23px; margin-top: 4px; z-index: 999; margin-left: 6px"><i class="fa fa-info-circle"></i></a>
                                            <br />
                                            <small>(click here to upload)</small>
                                            <span style="height: 180px;">
                                                <%--<i class="fa fa-file-image-o"></i>--%>
                                            </span>
                                        </label>
                                        <input type="file" multiple="multiple" name="uploadAdditional" id="uploadAdditional" class="form-control hide" accept="image/*,application/pdf" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-control border-0"></label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <%--<div id="Div5" class="g-recaptcha" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>--%>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="next" type="button" id="btnNextStep11">Next</button>
                            </div>
                        </section>

                        <h3>
                            <div class="media">
                                <div class="bd-wizard-step-icon" data-toggle="tooltip" title="Complete" data-placement="left" data-container="body">
                                    <i class="mdi mdi-account-check"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bd-wizard-step-title">Complete</h5>
                                    <p class="bd-wizard-step-subtitle">
                                        Declaring the info
                                    </p>
                                </div>
                            </div>
                        </h3>
                        <section>
                            <h2 class="text-dark  font-weight-bold">Declaration</h2>
                            <hr />
                            <div class="section-border">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <ol type="a">
                                                <li>I declare that the information given in this application are true, accurate and complete. I undertake to notify Apex Investment Services Berhad should there be any change(s) in my personal particulars.<br />
                                                </li>
                                                <li>I have read, understood and agreed to the Terms and Conditions of eApexis Portal.<br />
                                                </li>
                                                <li>I have read, understood and agreed to be bound by the Privacy Policy and agree with the processing of my personal data.</li>
                                                <li>I hereby consent to receive notice of all statements and reports including statements of transaction, interim and annual statements, fund reports and/or other communications in electronic form and will be made available for my viewing and printing at eApexis Portal.</li>
                                                <li>I declare that I am not a U.S person and in the event of a change in my status that I become a U.S Person, I shall notify Apex Investment Services Berhad of the change.</li>
                                                <li>I declare that I am not undischarged bankrupt, have not committed any act of bankruptcy within the past 12 months and no bankruptcy order has been made against me or is pending against me during that period.  If any information is found false or misleading, Apex Investment Services Berhad may reject any of my application and instructions including but not limited to, any transactional-related activities</li>
                                                <li>I shall indemnify and keep Apex Investment Services Berhad fully indemnified against any actions, proceedings, claims, losses, damages, costs and expenses which may be brought against, suffered or incurred and/or failing to act on any instructions given by me unless due to the willful default or negligence of Apex Investment Services Berhad.<br />
                                                </li>
                                            </ol>



                                            <br />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="pull-left" style="width: 4%; display: inline-block;">
                                                        <input type="checkbox" id="chkDeclare" name="chkDeclare" style="float: left;" />
                                                    </div>
                                                    <div class="pull-left" style="display: inline-block; line-height: 19px; margin-bottom: 15px; width: 96%;">
                                                        <label class="w-97" for="chkDeclare" style="margin-bottom: 0px;">I have read, understood and agree to all the above and wish to apply for eApexis portal which allows me to access my investment account information, view statements and fund reports, perform investments and transaction requests.</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="divReenterPasswor" runat="server">
                                    <label for="password">Please re-enter password for validation</label>
                                    <input type="password" name="email" id="txtreEnterPassword" class="form-control" placeholder="Enter Password" />

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-control border-0"></label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <%--<div id="Div6" class="g-recaptcha" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="buttonArea">
                                <button class="bd-wizard-btn mr-3" data-step="previous" type="button">Previous</button>
                                <button class="bd-wizard-btn" data-step="finish" type="button" id="btnFinish">Finish</button>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <asp:HiddenField ID="hdnSMSExpirationTimeForAOInSeconds" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnBtnClickedId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnBankNoFormat" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsAdditional" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="gRecaptchaResponse" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnEmail" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnEmailRequested" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnEmailVerified" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="/Content/acc-open-steps/js/jquery.steps.min.js"></script>
    <script src="/Content/acc-open-steps/js/bd-wizard.js"></script>
    <link href="Content/acc-open-steps/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="Content/acc-open-steps/js/bootstrap-datepicker.js"></script>
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
    <script src="Content/acc-open-steps/js/face-api.js"></script>
    <script src="Content/acc-open-steps/js/commons.js"></script>
    <script src="Content/acc-open-steps/js/faceDetectionControls.js"></script>

    <script>
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });

        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });

        //ShowCustomMessage('Note', 'To be an investor, you must be Malaysian / Foreigner, age 18 & above and residing in Malaysia. Please have your NRIC/Passport ready for registration.', '');
        var hdnSMSExpirationTimeForAOInSeconds = parseInt($('#hdnSMSExpirationTimeForAOInSeconds').val());
        function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        function helpBank() {
            ShowCustomGuideMessage("Sample Signature", $(".guide-popup").html(), "");
        }
        (function ($) {
            hdnTermsPop = $('#hdnTermsPop').val();
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));
        function StartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeForAOInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelText').html('OTP Expired. Please Request Again.');
                    $('#countDownTime').addClass('hide');
                    $('#countDownTime').html('');
                    $('#lblInfo').html('');
                    $('#btnRequest').removeAttr('disabled');
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                    timer2 = hours + ':' + minutes + ':' + seconds;
                    $('#countDownTime').removeClass('hide');
                }
            }, 1000);
        }
        function StartEmailTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeForAOInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#emailCountdownLabelText').html('OTP Expired. Please Request Again.');
                    $('#emailCountDownTime').addClass('hide');
                    $('#emailCountDownTime').html('');
                    $('#lblEmailCode').html('');
                    $('#btnCodeRequest').removeAttr('disabled');
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#emailCountDownTime').html(hours + ':' + minutes + ':' + seconds);
                    timer2 = hours + ':' + minutes + ':' + seconds;
                    $('#emailCountDownTime').removeClass('hide');
                }
            }, 1000);
        }
        function recaptchaExpired() {
            grecaptcha.reset();
        }

        var gRecaptchaResponse;
        var validateCheck = 0;
        function ValidateStep2() {
            if (validateCheck == 0) {
                return new Promise(function (resolve, reject) {

                    if (grecaptcha === undefined) {
                        alert('Recaptcha non defined');
                        return;
                    }
                    console.log('verification');

                    var response = grecaptcha.getResponse();
                    console.log(response);
                    if (!response) {
                        alert('Coud not get recaptcha response');
                        return;
                    }
                    gRecaptchaResponse = response;
                    $('#gRecaptchaResponse').val(gRecaptchaResponse);
                    if ($('#txtNRIC').val().trim() != "" && $('#txtMobileNo').val().trim() != "") {
                        $("#requestOTPDiv").removeClass('hide');
                        $('#txtNRIC').removeClass('field-required');
                        $('#txtMobileNo').removeClass('field-required');
                    }
                    else {
                        if ($('#txtNRIC').val().trim() == "")
                            $('#txtNRIC').addClass('field-required');
                        else
                            $('#txtNRIC').removeClass('field-required');
                        if ($('#txtMobileNo').val().trim() == "")
                            $('#txtMobileNo').addClass('field-required');
                        else
                            $('#txtMobileNo').removeClass('field-required');
                    }
                    validateCheck = 1;
                }); //end promise
            }
            else {
                if ($('#txtNRIC').val().trim() != "" && $('#txtMobileNo').val().trim() != "") {
                    $("#requestOTPDiv").removeClass('hide');
                }
                else {
                    if ($('#txtNRIC').val().trim() == "")
                        $('#txtNRIC').addClass('field-required');
                    else
                        $('#txtNRIC').removeClass('field-required');
                    if ($('#txtMobileNo').val().trim() == "")
                        $('#txtMobileNo').addClass('field-required');
                    else
                        $('#txtMobileNo').removeClass('field-required');
                }
            }
        }
        var validateCheckUserId = 0;
        function ValidateStep3() {
            if ($('#email').val().trim() != "" && $('#password').val().trim() != "" && $('#repassword').val().trim() != "" && $('#password').val().trim() == $('#repassword').val().trim()) {
                $('#email').removeClass('field-required');
                $('#password').removeClass('field-required');
                $('#repassword').removeClass('field-required');
                $('#passwordNotMatch').addClass('hide');
                //$('.buttonArea [data-step="next"]').removeClass('hide');
            }
            else {
                if ($('#email').val().trim() == "")
                    $('#email').addClass('field-required');
                else
                    $('#email').removeClass('field-required');
                if ($('#password').val().trim() == "")
                    $('#password').addClass('field-required');
                else
                    $('#password').removeClass('field-required');
                if ($('#repassword').val().trim() == "")
                    $('#repassword').addClass('field-required');
                else if ($('#password').val().trim() != $('#repassword').val().trim()) {
                    $('#repassword').addClass('field-required');
                    if ($('#passwordNotMatch').hasClass('hide')) {
                        $('#passwordNotMatch').removeClass('hide');
                    }
                }
                else
                    $('#repassword').removeClass('field-required');
            }
            validateCheckUserId = 1;
        }
        var validateCheckPersonalInfo = 0;
        function ValidateStep4() {
            var atLeastOneIsChecked = false;
            $('.checkbox-group-5 input').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked && ($('#title').val() != "" && $('#title').val() == 'Others' && $('#titleOther').val() != "") && $('#fullName').val() != "" && $('#dob').val() != "" && $('#gender').val() != "" && $('#race').val() != "" && $('#maritalStatus').val() != "" && $('#bumiputraStatus').val() != "") {
                //$('.buttonArea [data-step="next"]').removeClass('hide');
                $('#title').removeClass('field-required');
                //$('#titleOther').removeClass('field-required');
                $('#fullName').removeClass('field-required');
                $('#dob').removeClass('field-required');
                $('#gender').removeClass('field-required');
                $('#race').removeClass('field-required');
                $('#maritalStatus').removeClass('field-required');
                $('#bumiputraStatus').removeClass('field-required');
            }
            else {
                if (!atLeastOneIsChecked) {
                    $('.checkbox-group-5').addClass('field-required');
                }
                if ($('#title').val().trim() == "")
                    $('#title').addClass('field-required');
                else
                    $('#title').removeClass('field-required');
                //if ($('#title').val().trim() == "Others" && $('#titleOther').val().trim() == "")
                //    $('#titleOther').addClass('field-required');
                //else
                //    $('#titleOther').removeClass('field-required');
                if ($('#fullName').val().trim() == "")
                    $('#fullName').addClass('field-required');
                else
                    $('#fullName').removeClass('field-required');
                if ($('#dob').val().trim() == "")
                    $('#dob').addClass('field-required');
                else
                    $('#dob').removeClass('field-required');
                if ($('#gender').val().trim() == "")
                    $('#gender').addClass('field-required');
                else
                    $('#gender').removeClass('field-required');
                if ($('#race').val().trim() == "")
                    $('#race').addClass('field-required');
                else
                    $('#race').removeClass('field-required');
                if ($('#maritalStatus').val().trim() == "")
                    $('#maritalStatus').addClass('field-required');
                else
                    $('#maritalStatus').removeClass('field-required');
                if ($('#bumiputraStatus').val().trim() == "")
                    $('#bumiputraStatus').addClass('field-required');
                else
                    $('#bumiputraStatus').removeClass('field-required');
            }
            validateCheckPersonalInfo = 1;
        }
        var validateCheckContactInfo = 0;
        function ValidateStep5() {
            if ($('#addr1').val() != "" && $('#city').val() != "" && $('#postCode').val() != "" && $('#state').val() != "" && $('#country').val() != "" && $('#addr1M').val() != "" && $('#cityM').val() != "" && $('#postCodeM').val() != "" && $('#stateM').val() != "" && $('#countryM').val() != "") {
                //$('.buttonArea [data-step="next"]').removeClass('hide');
                $('#addr1').removeClass('field-required');
                $('#city').removeClass('field-required');
                $('#postCode').removeClass('field-required');
                $('#state').removeClass('field-required');
                $('#country').removeClass('field-required');
                $('#addr1M').removeClass('field-required');
                $('#cityM').removeClass('field-required');
                $('#postCodeM').removeClass('field-required');
                $('#stateM').removeClass('field-required');
                $('#countryM').removeClass('field-required');
            }
            else {
                //$('.buttonArea [data-step="next"]').addClass('hide');
                if ($('#addr1').val().trim() == "")
                    $('#addr1').addClass('field-required');
                else
                    $('#addr1').removeClass('field-required');
                if ($('#city').val().trim() == "")
                    $('#city').addClass('field-required');
                else
                    $('#city').removeClass('field-required');
                if ($('#postCode').val().trim() == "")
                    $('#postCode').addClass('field-required');
                else
                    $('#postCode').removeClass('field-required');
                if ($('#state').val().trim() == "")
                    $('#state').addClass('field-required');
                else
                    $('#state').removeClass('field-required');
                if ($('#country').val().trim() == "")
                    $('#country').addClass('field-required');
                else
                    $('#country').removeClass('field-required');
                if ($('#addr1M').val().trim() == "")
                    $('#addr1M').addClass('field-required');
                else
                    $('#addr1M').removeClass('field-required');
                if ($('#cityM').val().trim() == "")
                    $('#cityM').addClass('field-required');
                else
                    $('#cityM').removeClass('field-required');
                if ($('#postCodeM').val().trim() == "")
                    $('#postCodeM').addClass('field-required');
                else
                    $('#postCodeM').removeClass('field-required');
                if ($('#stateM').val().trim() == "")
                    $('#stateM').addClass('field-required');
                else
                    $('#stateM').removeClass('field-required');
                if ($('#countryM').val().trim() == "")
                    $('#countryM').addClass('field-required');
                else
                    $('#countryM').removeClass('field-required');
            }
            validateCheckContactInfo = 1;
        }

        var validateCheckOccInfo = 0;
        function ValidateStep6() {
            var atLeastOneIsChecked = false;
            $('.checkbox-group-2 input').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked && $('#ddlOccupation').val() != "" && ($('#ddlOccupation').val() == 'Others' && $('txtOccupationDesc').val() != "") && ($('#Employed').is(':checked') && $('#ddlDesignation').val() != "" && $('#ddlNatureOfBusiness').val() != "" && $('#ddlMonthlyIncome').val() != "" && $('#txtOfficeAddress1').val() != "" && $('#txtPostCode').val() != "" && $('#txtCity').val() != "" && $('#ddlState').val() != "" && $('#ddlCountry').val() != "")) {
                //$('.buttonArea [data-step="next"]').removeClass('hide');
                $('#ddlOccupation').removeClass('field-required');
                $('#txtOccupationDesc').removeClass('field-required');
                $('#ddlDesignation').removeClass('field-required');
                $('#ddlNatureOfBusiness').removeClass('field-required');
                $('#ddlMonthlyIncome').removeClass('field-required');
                $('#txtOfficeAddress1').removeClass('field-required');
                $('#txtPostCode').removeClass('field-required');
                $('#txtCity').removeClass('field-required');
                $('#ddlState').removeClass('field-required');
                $('#ddlCountry').removeClass('field-required');
            }
            else {
                if (!atLeastOneIsChecked) {
                    $('.checkbox-group-2').addClass('field-required');
                }
                //$('.buttonArea [data-step="next"]').addClass('hide');
                if ($('#ddlOccupation').val().trim() == "")
                    $('#ddlOccupation').addClass('field-required');
                else
                    $('#ddlOccupation').removeClass('field-required');
                if ($('#race').val().trim() == "Others" && $('txtRaceDesc').val() == "")
                    $('#txtRaceDesc').addClass('field-required');
                else
                    $('#txtRaceDesc').removeClass('field-required');
                if ($('#ddlOccupation').val().trim() == "Others" && $('txtOccupationDesc').val() == "")
                    $('#txtOccupationDesc').addClass('field-required');
                else
                    $('#txtOccupationDesc').removeClass('field-required');
                if ($('#Employed').is(':checked')) {
                    alert('3');
                    if ($('#ddlDesignation').val().trim() == "")
                        $('#ddlDesignation').addClass('field-required');
                    else
                        $('#ddlDesignation').removeClass('field-required');
                    if ($('#ddlNatureOfBusiness').val().trim() == "")
                        $('#ddlNatureOfBusiness').addClass('field-required');
                    else
                        $('#ddlNatureOfBusiness').removeClass('field-required');
                    if ($('#ddlMonthlyIncome').val().trim() == "")
                        $('#ddlMonthlyIncome').addClass('field-required');
                    else
                        $('#ddlMonthlyIncome').removeClass('field-required');
                    if ($('#txtOfficeAddress1').val().trim() == "")
                        $('#txtOfficeAddress1').addClass('field-required');
                    else
                        $('#txtOfficeAddress1').removeClass('field-required');
                    if ($('#txtPostCode').val().trim() == "")
                        $('#txtPostCode').addClass('field-required');
                    else
                        $('#txtPostCode').removeClass('field-required');
                    if ($('#txtCity').val().trim() == "")
                        $('#txtCity').addClass('field-required');
                    else
                        $('#txtCity').removeClass('field-required');
                    if ($('#ddlState').val().trim() == "")
                        $('#ddlState').addClass('field-required');
                    else
                        $('#ddlState').removeClass('field-required');
                    if ($('#ddlCountry').val().trim() == "")
                        $('#ddlCountry').addClass('field-required');
                    else
                        $('#ddlCountry').removeClass('field-required');
                }
            }
            validateCheckOccInfo = 1;
        }
        //function ValidateStep7() {
            
        //    if (($('#ddlPurposeOfTransaction').val() != ""
        //        || ($('#ddlPurposeOfTransaction').val() == 'Others'
        //            && $('txtPurposeOfTransaction').val() != "")
        //        || ($('#ddlSourceOfFunds').val() != "")
        //        || ($('#ddlSourceOfFunds').val() == 'Others'
        //            && $('ddlRelationship').val() != "Select"
        //            && $('txtFunderName').val() != ""
        //            && $('ddlFunderIndustry').val() != "Select"
        //            && $('txtFundOwnerName').val() != ""
        //            && $('ddlEstimatedNetWorth').val() != "Select"))) {
        //        //$('.buttonArea [data-step="next"]').removeClass('hide');
        //        $('#ddlPurposeOfTransaction').removeClass('field-required');
        //        $('#ddlSourceOfFunds').removeClass('field-required');
        //        $('#ddlEstimatedNetWorth').removeClass('field-required');
        //    }
        //    else {
                
        //        //$('.buttonArea [data-step="next"]').addClass('hide');
        //        if ($('#ddlPurposeOfTransaction').val().trim() == "Select")
        //            $('#ddlPurposeOfTransaction').addClass('field-required');
        //        else
        //            $('#ddlPurposeOfTransaction').removeClass('field-required');
        //        //if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('txtRaceDesc').val() == "")
        //        //    $('#txtRaceDesc').addClass('field-required');
        //        //else
        //        //    $('#txtRaceDesc').removeClass('field-required');
        //        //if ($('#ddlOccupation').val().trim() == "Others" && $('txtOccupationDesc').val() == "")
        //        //    $('#txtOccupationDesc').addClass('field-required');
        //        //else
        //        //    $('#txtOccupationDesc').removeClass('field-required');
        //    }
        //    validateCheckOccInfo = 1;
        //}
        var ddlOccOptions;
        function btnNextStepAjax(id, formArray) {
            $('.loader-bg').fadeIn();
            $.ajax({
                url: "AccountOpening.aspx/btnNext_Click",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify({ formInputs: formArray, IPAddress: $("#hdnLocalIPAddress").val() }),
                success: function (data) {
                    $('.err-msg').remove();
                    $('.field-required').removeClass('field-required');

                    $('.loader-bg').fadeOut();
                    var json = data.d;
                    console.log(json.CustomValidations);
                    if (json.CustomValidations != null) {
                        if (json.CustomValidations.length > 0) {
                            $('#' + json.CustomValidations[0].Name).focus();
                        }
                        $.each(json.CustomValidations, function (idx, obj) {
                            console.log(obj);
                            if (!($('#' + obj.Name).hasClass('field-required'))) {
                                if (obj.Name == 'addr1') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#addr3');
                                }
                                else if (obj.Name == 'txtOfficeAddress1') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#txtOfficeAddress3');
                                }
                                else if (obj.Name == 'addr1M') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#addr3M');
                                }
                                else if (obj.Name == 'ddlTaxResidencyStatus') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.ddlTaxResidencyStatus').val() == "") {
                                            var tx = $(objTax).find('.ddlTaxResidencyStatus');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });
                                }
                                else if (obj.Name == 'txtTINNum') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.txtTINNum').val() == "") {
                                            var tx = $(objTax).find('.txtTINNum');
                                            $(tx).addClass('field-required');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                        }
                                        else {

                                        }
                                    });
                                }
                                else if (obj.Name == 'txtnoTIN') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.txtnoTIN').val() == "") {
                                            var tx = $(objTax).find('.txtnoTIN');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });

                                }
                                else if (obj.Name == 'optionBReason') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.optionBReason').val() == "") {
                                            var tx = $(objTax).find('.optionBReason');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });

                                }
                                else {
                                    $('<div class="err-msg" id="err-' + obj.Name + '"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#' + obj.Name);
                                }
                                $('#' + obj.Name).addClass('field-required');
                                if (obj.Name == 'checkbox-group-3') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-3');
                                }
                                else if (obj.Name == 'checkbox-group-4') {

                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-4');
                                }
                                else if (obj.Name == 'checkbox-group-1') {

                                    $('<div class="err-msg" style=""><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-1');
                                }
                                else if (obj.Name == 'checkbox-group-2') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-2');
                                }
                                else if (obj.Name == 'checkbox-group-5') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#placeForResidentErr');
                                }
                                else if (obj.Name == 'checkbox-group-8') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-8');
                                }
                                else if (obj.Name == 'checkbox-group-9') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-9');
                                }
                                else if (obj.Name == 'checkbox-group-10') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#placeForNationalityErr');
                                }

                            }
                        });
                    }
                    if (!json.IsSuccess) {
                        if (id != "btnFinish") {
                            $('section.current [data-step="previous"]').click();
                        }
                        if (json.Message.substring(0, 35) == 'E-KYC identity verification failed.')
                            ShowCustomMessage('Alert', 'E-KYC identity verification failed. Entered details do not match. Please retry.', 'Index.aspx');
                        else if (json.Message == 'Session Expired.') {
                            ShowCustomMessage("Alert", json.Message, 'AccountOpening.aspx');
                        }
                        else {
                            if (json.Message == "Please fill in details") { }
                            else
                                ShowCustomMessage('Alert', json.Message, '');
                            if (json.Message == 'Please verify yourself!' || json.Message == 'This Id Number has already been registered' || json.Message == 'This Id Number has already been registered as joint with you.' || json.Message == 'Please enter valid Mobile number!' || json.Message == 'Aged below 18 are not allowed')
                                recaptchaExpired();
                        }

                        //if (id == "btnNextStep3")
                        //{

                        //}

                    }
                    else if (json.IsSuccess) {

                        var accountOpeningExisting = json.Data;
                        if (id == "btnNextStep0") {
                            $('#USCGPYes').focus();
                            if (accountOpeningExisting.ProcessStatus == 10) {
                                $('#taxResidentHead').addClass('hide');
                                $('#about-you').removeClass('hide');
                                $('#btnNextStep1').removeClass('hide');
                                $('#txtNRIC').val(accountOpeningExisting.IdNo);
                                $('#txtNRIC').attr('readonly', 'readonly');
                                $('#txtMobileNo').val(accountOpeningExisting.MobileNo.substring(1));
                                $('#txtMobileNo').attr('readonly', 'readonly');
                                $('#Div7').addClass('hide');
                            }
                            //$('#about-you').removeClass('hide');
                            //$('#requestOTPDiv').hide();

                            //$('#ContentPlaceHolder1_Div7').hide();
                            //$('#usChecks').hide();
                            //$('#email').val(accountOpeningExisting.Email);
                            //$('#password').val('');
                            //$('#repassword').val('');
                            //$('#txtNRIC').attr('readonly', 'readonly');
                            //$('#requestOTPDiv').hide();
                            //$('#txtMobileNo').attr('readonly', 'readonly');
                            //$('#ContentPlaceHolder1_Div7').hide();
                            //$('#usChecks').hide();

                            //$("#ROMYes").prop('checked', false);
                            //$("#ROMNo").prop('checked', false);
                            //$("#Malaysian").prop('checked', false);
                            //$("#NonMalaysian").prop('checked', false);
                            //if (accountOpeningExisting.ResidentOfMalaysia == 1)
                            //    $("#ROMYes").prop('checked', true);
                            //else
                            //    $("#ROMNo").prop('checked', true);
                            //if (accountOpeningExisting.Nationality == "Malaysian")
                            //    $("#Malaysian").prop('checked', true);
                            //else if (accountOpeningExisting.Nationality == "NonMalaysian")
                            //    $("#NonMalaysian").prop('checked', true);
                            //if (accountOpeningExisting.Salutation != "") {
                            //    if ($('#title option[value="' + accountOpeningExisting.Salutation + '"').length == 0) {
                            //        $('#title').val('Others');
                            //        $('#titleOther').removeClass('d-none');
                            //        $('#titleOther').val(accountOpeningExisting.Salutation);
                            //    }
                            //    else {
                            //        $('#title').val(accountOpeningExisting.Salutation);
                            //    }
                            //}

                            //$('#fullName').val(accountOpeningExisting.Name);
                            //$('#dob').val(accountOpeningExisting.Dob);
                            //$('#gender').val(accountOpeningExisting.Gender);
                            //$('#race').val(accountOpeningExisting.Race);
                            //$('#maritalStatus').val(accountOpeningExisting.MaritalStatus);
                            //$('#bumiputraStatus').val(accountOpeningExisting.BumiputraStatus);
                        }
                        if (id == "btnNextStep1") {
                            if (accountOpeningExisting.ProcessStatus == 10) {
                                id = "btnNextStep9";
                            }
                            $('#email').val(accountOpeningExisting.Email);
                            $('#hdnEmail').val(accountOpeningExisting.Email);
                            $('#password').val('');
                            $('#repassword').val('');
                            $('#txtNRIC').attr('readonly', 'readonly');
                            $('#requestOTPDiv').hide();
                            $('#txtMobileNo').attr('readonly', 'readonly');
                            $('#ContentPlaceHolder1_Div7').hide();
                            $('#usChecks').hide();
                            $('.tax-residency-details').hide();
                            $('.checkbox-group-1').hide();
                            //$('#' + accountOpeningExisting.LastBtnClicked).parent('.buttonArea').parent('section')
                            //console.log(accountOpeningExisting.LastBtnClicked);
                            //console.log(Array.from($('[data-step="next"]')));
                            //var btnIndex = Array.from($('[data-step="next"]')).indexOf($('#' + accountOpeningExisting.LastBtnClicked)[0])
                            //console.log("btnIndex: " + btnIndex);
                            //$('[data-step="next"]').each(function (idx, ele) {
                            //    if (idx <= btnIndex) {
                            //        btnNextStepAjax(id, formArray)
                            //        //$(ele).click();
                            //        $("#wizard").steps('next');
                            //    }
                            //});
                            //$(".steps ul > li").each(function (idx, ele) {
                            //    var cssClass = "";
                            //    $(ele).removeClass('done');
                            //    $(ele).removeClass('current');
                            //    $(ele).removeClass('disabled');
                            //    $(ele).attr('aria-disabled', false);
                            //    $(ele).attr('aria-selected', false);
                            //    if (idx <= btnIndex) {
                            //        cssClass = "done";
                            //        $(ele).attr('aria-selected', false);
                            //        $(ele).attr('aria-disabled', false);
                            //    }
                            //    else if (idx == btnIndex + 1) {
                            //        cssClass = "current";
                            //        $(ele).attr('aria-selected', true);
                            //        $(ele).attr('aria-disabled', false);
                            //    }
                            //    else if (idx > btnIndex + 1) {
                            //        cssClass = "disabled";
                            //        $(ele).attr('aria-selected', false);
                            //        $(ele).attr('aria-disabled', false);
                            //    }
                            //    $(ele).addClass(cssClass);
                            //});
                            //$('#' + accountOpeningExisting.LastBtnClicked).click();

                            $("#ROMYes").focus();
                            $("#ROMYes").prop('checked', false);
                            $("#ROMNo").prop('checked', false);
                            //$("#Malaysian").prop('checked', false);
                            //$("#NonMalaysian").prop('checked', false);
                            if (accountOpeningExisting.Tin == '1') {
                                if (accountOpeningExisting.ResidentOfMalaysia == 1) {
                                    $("#ROMYes").prop('checked', true);
                                }
                                else {
                                    $("#ROMNo").prop('checked', true);
                                }
                            }
                            if (accountOpeningExisting.Nationality == "Malaysian")
                                $("#Malaysian").prop('checked', true);
                            else if (accountOpeningExisting.Nationality == "NonMalaysian")
                                $("#NonMalaysian").prop('checked', true);
                            if (accountOpeningExisting.Salutation != "") {
                                if ($('#title option[value="' + accountOpeningExisting.Salutation + '"').length == 0) {
                                    $('#title').val('Others');
                                    //$('#titleOther').removeClass('d-none');
                                    //$('#titleOther').val(accountOpeningExisting.Salutation);
                                }
                                else {
                                    $('#title').val(accountOpeningExisting.Salutation);
                                }
                            }

                            $('#fullName').val(accountOpeningExisting.Name);
                            $('#dob').datepicker('setDate', accountOpeningExisting.Dob);
                            //$('#dob').val(accountOpeningExisting.Dob);
                            $('#gender').val(accountOpeningExisting.Gender);
                            $('#race').val(accountOpeningExisting.Race);
                            if (accountOpeningExisting.Race == "Others")
                                $('.race-others').removeClass('hide');
                            $('#txtRaceDesc').val(accountOpeningExisting.RaceDescription);
                            $('#maritalStatus').val(accountOpeningExisting.MaritalStatus);
                            $('#bumiputraStatus').val(accountOpeningExisting.BumiputraStatus);
                            //$('#agent').val(accountOpeningExisting.AgentCode);

                            if (/*x.test(y)*/ !isNaN($('#txtNRIC').val()) && $('#txtNRIC').val().length == 12) {
                                //alert('in');
                                //$('#dob').attr('disabled', 'disabled');
                            }
                            if (accountOpeningExisting.IsEmailVerified == 1) {
                                $('#requestCodeDiv').addClass('hide');
                            }
                        }
                        if (id == "btnNextStep3") {
                            $('#addr1').focus();
                            if (accountOpeningExisting.accountOpeningAddresses != null && accountOpeningExisting.accountOpeningAddresses.length > 0) {
                                var aoar = $.grep(accountOpeningExisting.accountOpeningAddresses, function (obj, idx) {
                                    return obj.AddressType == 1;
                                });
                                var aoam = $.grep(accountOpeningExisting.accountOpeningAddresses, function (obj, idx) {
                                    return obj.AddressType == 2;
                                });
                                if (aoar.length == 1) {
                                    $('#addr1').val(aoar[0].Addr1);
                                    $('#addr2').val(aoar[0].Addr2);
                                    $('#addr3').val(aoar[0].Addr3);
                                    $('#city').val(aoar[0].City);
                                    $('#postCode').val(aoar[0].PostCode);
                                    $('#state').val(aoar[0].State);
                                    $('#country').val(aoar[0].Country);
                                    $('#telNoH').val(aoar[0].TelNo);
                                }
                                if (aoam.length == 1) {
                                    $('#addr1M').val(aoam[0].Addr1);
                                    $('#addr2M').val(aoam[0].Addr2);
                                    $('#addr3M').val(aoam[0].Addr3);
                                    $('#cityM').val(aoam[0].City);
                                    $('#postCodeM').val(aoam[0].PostCode);
                                    $('#stateM').val(aoam[0].State);
                                    $('#countryM').val(aoam[0].Country);
                                }
                                if (aoar.length == 1 && aoam.length == 1) {
                                    if (aoar[0].Addr1 == aoam[0].Addr1 &&
                                        aoar[0].Addr2 == aoam[0].Addr2 &&
                                        aoar[0].Addr3 == aoam[0].Addr3 &&
                                        aoar[0].City == aoam[0].City &&
                                        aoar[0].PostCode == aoam[0].PostCode &&
                                        aoar[0].State == aoam[0].State &&
                                        aoar[0].Country == aoam[0].Country) {
                                        $("#sameAsResidentialAddr").prop('checked', true);
                                        $('#addr1M').attr('readonly', 'readonly');
                                        $('#addr2M').attr('readonly', 'readonly');
                                        $('#addr3M').attr('readonly', 'readonly');
                                        $('#cityM').attr('readonly', 'readonly');
                                        $('#postCodeM').attr('readonly', 'readonly');
                                        $('#stateM').attr('readonly', 'readonly');
                                        $('#countryM').attr('disabled', 'disabled');
                                    }
                                }
                            }
                            if (accountOpeningExisting.IsEmailVerified == 1) {
                                ShowCustomMessage("Notice of Privacy Policy", "Your personal information will be use in accordance with our Privacy policy. You can view this policy on our portal. By accessing our portal, you agree to be bound by this policy.", "", "Accept", "addr1");
                                $('#hdnEmail').val(accountOpeningExisting.Email);
                                $('#hdnEmailVerified').val('1');
                            }
                        }
                        if (id == "btnNextStep5") {
                            $('#Employed').focus();
                            if (accountOpeningExisting.AccountOpeningOccupation != null) {

                                $('#Employed').prop('checked', false);
                                $('#Unemployed').prop('checked', false);
                                var occ = accountOpeningExisting.AccountOpeningOccupation;
                                if (occ.Employed == 1) {
                                    $('#Employed').prop('checked', true);
                                    $('.occupation-field').removeClass('hide');
                                    $('.employed-fields').removeClass('hide');
                                    $('#ddlOccupation option').remove();
                                    $.ajax({
                                        url: "AccountOpening.aspx/GetOccupations",
                                        contentType: 'application/json; charset=utf-8',
                                        type: "GET",
                                        dataType: "JSON",
                                        //data: $('form').serializeArray(),
                                        success: function (data) {
                                            console.log(data);
                                            if (data.d.IsSuccess) {
                                                var json = data.d.Data;
                                                var option = document.createElement("option");
                                                option.text = "Select";
                                                option.value = "";
                                                $('#ddlOccupation').append(option);
                                                $.each(json, function (idx, obj) {
                                                    var option = document.createElement("option");
                                                    option.text = obj.OCCCODE;
                                                    option.value = obj.DOWNLOADIND;
                                                    $('#ddlOccupation').append(option);
                                                });
                                                $('#ddlOccupation').val(occ.Occupation != "null" ? occ.Occupation : "");
                                                $('#ddlOccupation').change();
                                            }
                                            else {
                                                ShowCustomMessage("Alert", data.d.Message, "");
                                            }
                                        }
                                    });
                                    //$('.occupation-others').addClass('hide');
                                    //ddlOccOptions = $('#ddlOccupation option');
                                }
                                else {
                                    $('#Unemployed').prop('checked', true);
                                    $('.occupation-field').removeClass('hide');
                                    $('.employed-fields').addClass('hide');
                                    $('.occupation-others').addClass('hide');
                                    ddlOccOptions = $('#ddlOccupation option');
                                    $('#ddlOccupation option').remove();
                                    $('#ddlOccupation').append('<option value="">Select</option>');
                                    $('#ddlOccupation').append('<option value="Student">Student</option>');
                                    $('#ddlOccupation').append('<option value="Retired">Retired</option>');
                                    $('#ddlOccupation').append('<option value="Housewife">Housewife</option>');
                                    $('#ddlOccupation').append('<option value="Others">Others</option>');
                                    $('#ddlOccupation').val(occ.Occupation != "null" ? occ.Occupation : "");
                                }
                                if (occ.Occupation != 'Others')
                                    $('#ddlOccupation').val(occ.Occupation != "null" ? occ.Occupation : "");
                                $('#ddlOccupation').change();
                                $('#txtOccupationDesc').val(occ.OccupationOthers);
                                $('#ddlDesignation').val(occ.Designation);
                                $('#txtEmployerName').val(occ.EmployerName);
                                $('#ddlNatureOfBusiness').val(occ.NatureOfBusiness);
                                $('#ddlMonthlyIncome').val(occ.MonthlyIncome);

                                var aoar = $.grep(accountOpeningExisting.accountOpeningAddresses, function (obj, idx) {
                                    return obj.AddressType == 3;
                                });
                                if (aoar.length == 1) {
                                    $('#txtOfficeAddress1').val(aoar[0].Addr1);
                                    $('#txtOfficeAddress2').val(aoar[0].Addr2);
                                    $('#txtOfficeAddress3').val(aoar[0].Addr3);
                                    $('#txtCity').val(aoar[0].City);
                                    $('#txtPostCode').val(aoar[0].PostCode);
                                    $('#ddlState').val(aoar[0].State);
                                    $('#ddlCountry').val(aoar[0].Country);
                                    $('#txtTelephone').val(aoar[0].TelNo);
                                }
                            }
                        }
                        if (id == "btnNextStep6") {
                            $('#ddlPurposeOfTransaction').focus();
                            if (accountOpeningExisting.AccountOpeningFinancialProfile != null) {
                                var fp = accountOpeningExisting.AccountOpeningFinancialProfile;

                                $('#ddlPurposeOfTransaction').val(fp.Purpose);
                                if (fp.Purpose == "Others") {
                                    $('#purposeOfTransactionOthers').removeClass('hide');
                                    $('#txtPurposeOfTransaction').val(fp.PurposeDescription);
                                }
                                $('#ddlSourceOfFunds').val(fp.Source);
                                if (fp.Source == "Others") {
                                    $('#source-others').removeClass('hide');
                                    $('#ddlRelationship').val(fp.Relationship);
                                    $('#txtFunderName').val(fp.NameOfFunder);
                                    $('#ddlFunderIndustry').val(fp.FundersIndustory);

                                    if (fp.IsFunderMoneyChanger == 1) {
                                        $('#chkFunderInvolvedYes').prop('checked', true);
                                    }
                                    else {
                                        $('#chkFunderInvolvedNo').prop('checked', true);
                                    }
                                    if (fp.IsFunderBeneficialOwner == 1) {
                                        $('#chkFunderBeneficialOwnerYes').prop('checked', true);
                                    }
                                    else {
                                        $('#chkFunderBeneficialOwnerNo').prop('checked', true);
                                    }

                                    $('#txtFundOwnerName').val(fp.FundOwnerName);
                                }
                                else {
                                    $('#source-others').addClass('hide');
                                }
                                $('#ddlEstimatedNetWorth').val(fp.EstimatedNetWorth);
                                if (fp.EmployedByFundCompany == 1) {
                                    $('#chkFundManagementCompanyYes').prop('checked', true);
                                }
                                else {
                                    $('#chkFundManagementCompanyNo').prop('checked', true);
                                }
                                if (accountOpeningExisting.IsAdditionalRequired == 1) {
                                    $('#additional').removeClass('hide');
                                }
                                else {
                                    if (!$('#additional').hasClass('hide'))
                                        $('#additional').addClass('hide');
                                }
                            }
                        }
                        if (id == "btnNextStep7") {
                            $('#ddlBankAccountType').focus();
                            $('#txtBankAccountName').val(accountOpeningExisting.Name);
                            if (accountOpeningExisting.AccountOpeningBankDetail != null) {
                                var bd = accountOpeningExisting.AccountOpeningBankDetail;
                                if (bd.Agreement == 1) {
                                    $('#chkDepositSalesBankAccountYes').prop('checked', true);
                                }
                                else {
                                    $('#chkDepositSalesBankAccountNo').prop('checked', true);
                                }
                                $('#ddlBankAccountType').val(bd.AccountType);
                                $('#ddlCurrency').val(bd.Currency);
                                $('#ddlBank').val(bd.BankId);
                                //$('#txtBankAccountName').val(bd.AccountName);
                                $('#txtBankAccountNo').val(bd.AccountNo);
                            }
                            //if ($('#ddlEstimatedNetWorth').val() == 'Above MYR 3,000,000')
                            //{
                            //    if ($('.additional-docs').hasClass('hide'))
                            //        $('.additional-docs').removeClass('hide');
                            //}
                            id = "btnNextStep8";
                        }
                        if (id == "btnNextStep8") {
                            $('#chkPEPYes').focus();
                            if (accountOpeningExisting.TaxResidencyStatus == '1') {
                                if (accountOpeningExisting.AreYouPep == 1) {
                                    $('#chkPEPYes').prop('checked', true);
                                    $('.pep-specify').removeClass('hide');
                                }
                                else {
                                    $('#chkPEPNo').prop('checked', true);
                                    $('.pep-specify').addClass('hide');
                                }
                            }

                            $('#txtPEPSpecify').val(accountOpeningExisting.PepStatus);
                            $('#txtPEPFamilyM').val(accountOpeningExisting.FPepStatus);

                            if (accountOpeningExisting.TaxResidencyStatus != '1') {

                            }
                            else {
                                if (accountOpeningExisting.TaxResidentOutsideMalaysia == 1) {
                                    $('#chkCRSYes').prop('checked', true);
                                    $('.out-malaysia-yes').removeClass('hide');
                                    $('#addTaxResidencyStatus').removeClass('hide');
                                    if (accountOpeningExisting.accountOpeningCRSDetails != null && accountOpeningExisting.accountOpeningCRSDetails.length > 0) {
                                        //--------------
                                        $('#multiple-tax-residency-status-row .out-malaysia-yes:not(:first)').remove();
                                        $.each(accountOpeningExisting.accountOpeningCRSDetails, function (idx, obj) {
                                            if (idx == 0) {
                                                $('.ddlTaxResidencyStatus').val(obj.TaxResidencyStatus);
                                                if (obj.Tin == "") {
                                                    $('.chkNoTINNum').prop('checked', true);
                                                    $('.tin-available').addClass('hide');
                                                    $('.no-TIN').removeClass('hide');
                                                }
                                                $('.txtTINNum').val(obj.Tin);

                                                var reasons = obj.NoTinReason.split(', ');
                                                var noTinReason = reasons[0];
                                                var optionBReason = reasons.length > 1 ? reasons[1] : '';
                                                $('.txtnoTIN').val(noTinReason);
                                                if (noTinReason == 'The Account Holder is otherwise unable to obtain a TIN or equivalent number') {
                                                    $('.optionBReason').val(optionBReason);
                                                    $('.optionBReason').removeClass('hide');
                                                }
                                            }
                                            else {

                                                var divTax = $('.out-malaysia-yes').first();
                                                var divTaxClone = $(divTax).clone();
                                                $(divTaxClone).find('.ddlTaxResidencyStatus').val(obj.TaxResidencyStatus);
                                                $(divTaxClone).find('.tax-residency-remove').removeClass('hide');
                                                if (obj.Tin == "") {
                                                    $(divTaxClone).find('.chkNoTINNum').prop('checked', true);
                                                    $(divTaxClone).find('.tin-available').addClass('hide');
                                                    $(divTaxClone).find('.no-TIN').removeClass('hide');
                                                }
                                                else {
                                                    $(divTaxClone).find('.chkNoTINNum').prop('checked', false);
                                                    $(divTaxClone).find('.tin-available').removeClass('hide');
                                                    $(divTaxClone).find('.no-TIN').addClass('hide');
                                                }
                                                $(divTaxClone).find('.optionBReason').val('');
                                                $(divTaxClone).find('.txtTINNum').val(obj.Tin);
                                                var reasons = obj.NoTinReason.split(', ');
                                                $(divTaxClone).find('.txtnoTIN').val(reasons[0]);
                                                if (reasons[0] == 'The Account Holder is otherwise unable to obtain a TIN or equivalent number') {
                                                    $(divTaxClone).find('.optionBReason').val(reasons.length > 1 ? reasons[1] : '');
                                                    $(divTaxClone).find('.optionBReason').removeClass('hide');
                                                }

                                                $('#multiple-tax-residency-status-row').append(divTaxClone);
                                            }
                                        });
                                    }
                                }
                                else {
                                    $('#chkCRSNo').prop('checked', true);
                                    $('.out-malaysia-yes').addClass('hide');
                                }
                                //$('#ddlTaxResidencyStatus').val(accountOpeningExisting.TaxResidencyStatus);
                                //if (accountOpeningExisting.tin == "") {
                                //    $('#chkNoTINNum').prop('checked', true);
                                //    $('.tin-available').addClass('hide');
                                //}
                                //$('#txtTINNum').val(accountOpeningExisting.txtTINNum);
                            }
                            var bankcount = parseInt($('#hdnBankNoFormat').val());
                            if (!$('#txtBankAccountNo').val().length == bankcount) {
                                ShowCustomMessage("Alert", "Bank number length must be in format", "");
                            }

                        }
                        if (id == "btnNextStep9") {
                            $('#uploadNRICFrontPage').focus();
                            if (accountOpeningExisting.IsAdditionalRequired == 0) {
                                $('.additional-docs').addClass('hide');
                            }
                            else {
                                $('.additional-docs').removeClass('hide');
                            }
                            if (accountOpeningExisting.accountOpeningFiles != null && accountOpeningExisting.accountOpeningFiles.length > 0) {

                                var aof1 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 1;
                                });
                                if (aof1.length == 1) {
                                    var FileName = aof1[0].Name;
                                    var blobURL = aof1[0].Url;
                                    if (FileName.indexOf('.pdf') != -1)
                                        $('#uploadNRICFrontPage').prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    else
                                        $('#uploadNRICFrontPage').prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "' style='height: 100%'/>");
                                }
                                var aof2 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 2;
                                });
                                if (aof2.length == 1) {
                                    var FileName = aof2[0].Name;
                                    var blobURL = aof2[0].Url;
                                    if (FileName.indexOf('.pdf') != -1)
                                        $('#uploadNRICBackPage').prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    else
                                        $('#uploadNRICBackPage').prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' style='height: 100%'/>");
                                }
                                var aof3 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 3;
                                });
                                if (aof3.length > 0) {
                                    var FileName = aof3[0].Name;
                                    var blobURL = aof3[0].Url;
                                    $('#uploadSelfie').prev().find('span').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' style='height: 100%'/>");
                                    $('#existing-selfie').removeClass('hide');
                                    $('#webcam').addClass('hide');
                                    $('#upload-selfie-option').addClass('hide');
                                }
                                else {
                                    $('#existing-selfie').addClass('hide');
                                    $('#webcam').removeClass('hide');
                                    $('#upload-selfie-option').removeClass('hide');
                                }
                                var aof4 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 4;
                                });
                                if (aof4.length == 1) {
                                    var FileName = aof4[0].Name;
                                    var blobURL = aof4[0].Url;
                                    if (FileName.indexOf('.pdf') != -1)
                                        $('#uploadSignature').prev().find('span').html("<a target='_blank' href='" + blobURL + "' alt='Signature document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    else
                                        $('#uploadSignature').prev().find('span').html("<img src='" + blobURL + "' alt='Signature document' title='" + FileName + "' style='height: 100%'/>");
                                }
                                var aof5 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 5;
                                });
                                if (aof5.length > 0) {
                                    $('#uploadAdditional').prev().find('span').html("");
                                    $.each(aof5, function (idx, obj) {
                                        var FileName = obj.Name;
                                        var blobURL = obj.Url;
                                        $('#uploadAdditional').prev().find('span').append("<a target='_blank' href='" + blobURL + "' alt='Additional document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    });

                                }
                                var aof6 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 6;
                                });
                                if (aof6.length > 0) {
                                    $('#uploadSupporting').prev().find('span').html("");
                                    $.each(aof6, function (idx, obj) {
                                        var FileName = obj.Name;
                                        var blobURL = obj.Url;
                                        $('#uploadSupporting').prev().find('span').append("<a target='_blank' href='" + blobURL + "' alt='Supporting document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    });

                                }

                                //NEW
                                if (accountOpeningExisting.ProcessStatus == 10) {
                                    var ReUploadFileTypes = accountOpeningExisting.AdditionalDesc.split(',');
                                    $.each(ReUploadFileTypes, function (idx, obj) {
                                        if (obj == "1") {
                                            $('#uploadNRICFrontPage').prev().find('span').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "2") {
                                            $('#uploadNRICBackPage').prev().find('span').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "3") {
                                            $('#uploadSelfie').prev().find('span').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "4") {
                                            $('#uploadSignature').prev().find('span').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "5") {
                                            $('#uploadAdditional').prev().find('span').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "6") {
                                            $('#uploadSupporting').prev().find('span').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                    });
                                }
                            }
                        }
                        if (id == "btnNextStep11") {
                            $("#chkDeclare").focus();
                            if (accountOpeningExisting.Declaration == 1) {
                                $("#chkDeclare").prop('checked', true);
                            }
                        }
                        if (id == "btnFinish") {
                            if (accountOpeningExisting.AccountType == 1) {
                                ShowCustomMessage('Info', json.Message, 'Index.aspx');
                            }
                            else if (accountOpeningExisting.AccountType == 2) {
                                ShowCustomMessage('Info', json.Message, 'Portfolio.aspx');
                            }
                        }
                    }
                },
                error: function (jqXHR, exception) {
                    $('.loader-bg').fadeOut();
                    $('section.current [data-step="previous"]').click();
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    ShowCustomMessage('Error', msg, '');
                }

            });
        }

        function checkPassword(id, formArray) {


        }

        $(document).ready(function () {
            //$("#Malaysian").prop('checked', true);

            //document.getElementById("chkPEPYes").checked = false;
            //document.getElementById("chkPEPNo").checked = false;
            //document.getElementById("chkCRSYes").checked = false;
            //document.getElementById("chkCRSYes").checked = false;

            $('body').on('blur', ':input', function () {
                if ($(this).val() != "") {
                    $(this).removeClass('field-required');
                    $(this).parent().find('.err-msg').remove();
                }
            });
            $('#dob').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true
            });
            //$('.buttonArea [data-step="next"]').addClass('hide');
            $('.checkbox-group-1 input:checkbox').click(function () {
                $('.checkbox-group-1 input:checkbox').not(this).prop('checked', false);
                console.log($(this).attr('id'))
                if ($(this).attr('id') == 'USCGPYes') {
                    if ($(this).is(':checked')) {
                        $('#about-you').addClass('hide');
                        $('#btnNextStep1').addClass('hide');
                        ShowCustomMessage('Alert', 'We are sorry to inform that eApexis does not accept applications with any of the U.S.Indicia - U.S.citizenship/ U.S Permanent Resident Status/ Born in U.S/ U.S.Taxpayer Identification Number(TIN)/ U.S.Residential/ Mailing Address and/ or US Telephone number.', '');
                    }
                }
                else if ($(this).attr('id') == 'USCGPNo') {
                    if ($(this).is(':checked')) {
                        $('#about-you').removeClass('hide');
                        $('#btnNextStep1').removeClass('hide');
                    }
                    else {
                        $('#about-you').addClass('hide');
                        $('#btnNextStep1').addClass('hide');
                    }
                }

            });
            $('.checkbox-group-2 input:checkbox').click(function () {
                $('.checkbox-group-2 input:checkbox').not(this).prop('checked', false);
                $('.loader-bg').fadeIn();
                if ($(this).attr('id') == 'Employed') {
                    document.getElementById("lblOccupation").innerHTML = "Occupation";
                    if ($(this).is(':checked')) {
                        $('.occupation-field').removeClass('hide');
                        $('.employed-fields').removeClass('hide');
                        $('#occupationOthers').removeClass('hide');
                        if (!$('.occupation-others').hasClass('hide'))
                            $('.occupation-others').addClass('hide');
                        //ddlOccOptions = $('#ddlOccupation option');
                        $('#ddlOccupation option').remove();
                        $.ajax({
                            url: "AccountOpening.aspx/GetOccupations",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            //data: $('form').serializeArray(),
                            success: function (data) {
                                console.log(data);
                                if (data.d.IsSuccess) {
                                    var json = data.d.Data;
                                    var option = document.createElement("option");
                                    option.text = "Select";
                                    option.value = "";
                                    $('#ddlOccupation').append(option);
                                    $.each(json, function (idx, obj) {
                                        var option = document.createElement("option");
                                        option.text = obj.OCCCODE;
                                        option.value = obj.DOWNLOADIND;
                                        $('#ddlOccupation').append(option);
                                    });
                                    $('#ddlOccupation').val('');
                                }
                                else {
                                    ShowCustomMessage("Alert", data.d.Message, "");
                                }
                            }
                        });


                    }
                    else {
                        $('.occupation-field').addClass('hide');
                        $('.employed-fields').addClass('hide');
                    }

                }
                else if ($(this).attr('id') == 'Unemployed') {
                    if ($(this).is(':checked')) {
                        $('.occupation-field').removeClass('hide');
                        $('.employed-fields').addClass('hide');
                        if (!$('.occupation-others').hasClass('hide'))
                            $('.occupation-others').addClass('hide');
                        ddlOccOptions = $('#ddlOccupation option');
                        document.getElementById("lblOccupation").innerHTML = "Status";
                        $('#ddlOccupation option').remove();
                        $('#ddlOccupation').append('<option value="">Select Status</option>');
                        $('#ddlOccupation').append('<option value="Student">Student</option>');
                        $('#ddlOccupation').append('<option value="Retired">Retired</option>');
                        $('#ddlOccupation').append('<option value="Housewife">Housewife</option>');
                        $('#ddlOccupation').append('<option value="Others">Others</option>');
                    }
                    else {
                        $('.occupation-others').addClass('hide');
                        $('.occupation-field').addClass('hide');
                        $('.employed-fields').addClass('hide');
                        //$('.occupation-others').addClass('hide');
                    }
                }
                $('.loader-bg').fadeOut();
            });
            $('.checkbox-group-11 input:checkbox').click(function () {
                $('.checkbox-group-11 input:checkbox').not(this).prop('checked', false);

                if ($(this).attr('id') == 'chkFunderBeneficialOwnerYes') {

                    if ($(this).is(':checked')) {


                        $('#txtFundOwnerName').val($('#txtFunderName').val());
                        $('#txtFundOwnerName').attr('readonly', 'readonly');
                    }
                    else {

                    }

                }
                else if ($(this).attr('id') == 'chkFunderBeneficialOwnerNo') {
                    if ($(this).is(':checked')) {
                        $('#txtFundOwnerName').val('');
                        $('#txtFundOwnerName').removeAttr('readonly');
                    }
                    else {

                        //$('.occupation-others').addClass('hide');
                    }

                }

            });
            $('.checkbox-group-3 input:checkbox').click(function () {
                $('.checkbox-group-3 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-4 input:checkbox').click(function () {
                $('.checkbox-group-4 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-5 input:checkbox').click(function () {
                $('.checkbox-group-5 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-6 input:checkbox').click(function () {
                $('.checkbox-group-6 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-7 input:checkbox').click(function () {
                $('.checkbox-group-7 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-8 input:checkbox').click(function () {
                $('.checkbox-group-8 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkPEPYes') {
                    if ($(this).is(':checked')) {
                        $('.pep-specify').removeClass('hide');
                    }
                    else {
                        $('.pep-specify').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkPEPNo') {
                    if ($(this).is(':checked')) {
                        $('.pep-specify').addClass('hide');
                    }
                    else {

                    }
                }
            });
            $('.checkbox-group-9 input:checkbox').click(function () {
                $('.checkbox-group-9 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkCRSYes') {
                    if ($(this).is(':checked')) {
                        $('.out-malaysia-yes').removeClass('hide');
                        $('#addTaxResidencyStatus').removeClass('hide');
                    }
                    else {
                        $('.out-malaysia-yes').addClass('hide');
                        $('#addTaxResidencyStatus').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkCRSNo') {
                    if ($(this).is(':checked')) {
                        $('.out-malaysia-yes').addClass('hide');
                        $('#addTaxResidencyStatus').addClass('hide');
                    }
                    else {

                    }
                }
            });
            $('.checkbox-group-10 input:checkbox').click(function () {
                $('.checkbox-group-10 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'NonMalaysian') {
                    if ($(this).is(':checked')) {
                        $('#bumiputraStatus').attr('disabled', 'disabled');
                        $('#bumiputraStatus').val('Non-Malaysian');
                    }
                    else {
                        $('#bumiputraStatus').removeAttr('disabled');
                        $('#bumiputraStatus').val('');
                    }
                }
                else if ($(this).attr('id') == 'Malaysian') {
                    if ($(this).is(':checked')) {
                        $('#bumiputraStatus').removeAttr('disabled');
                        $('#bumiputraStatus').val('');
                    }
                    else {

                    }
                }
            });
            $('.checkbox-group-10 input:checkbox').click(function () {
                $('.checkbox-group-10 input:checkbox').not(this).prop('checked', false);
            });
            $('#multiple-tax-residency-status-row').on('click', '.chkNoTINNum', function () {
                if ($(this).is(':checked')) {
                    $(this).parents('.checkbox-parent').find('.tin-available').addClass('hide');
                    $(this).parents('.checkbox-parent').find('.no-TIN').removeClass('hide');
                    $(this).parents('.checkbox-parent').find('.no-TIN').val('');
                    $(this).parents('.checkbox-parent').next().find('.txtTINNum').val('');
                    //$('.tin-available').addClass('hide');
                }
                else {
                    $(this).parents('.checkbox-parent').find('.tin-available').removeClass('hide');
                    $(this).parents('.checkbox-parent').find('.no-TIN').addClass('hide');
                    //$('.tin-available').removeClass('hide');
                }
            });

            $('#multiple-tax-residency-status-row').on('change', '.txtnoTIN', function () {
                if ($(this).val() == 'The Account Holder is otherwise unable to obtain a TIN or equivalent number') {
                    $(this).next().removeClass('hide');
                    //$('.tin-available').addClass('hide');
                }
                else {
                    $(this).next().addClass('hide');
                    $(this).next().val('');
                    //$('.tin-available').removeClass('hide');
                }
            });

            $('#ddlOccupation').change(function () {
                if ($(this).val() == 'Others') {

                    $('.occupation-others').removeClass('hide');

                }
                else {
                    $('.occupation-others').addClass('hide');
                }
            });

            $('#race').change(function () {
                if ($(this).val() == 'Others') {

                    $('.race-others').removeClass('hide');

                }
                else {
                    $('.race-others').addClass('hide');
                    $('#txtRaceDesc').val('');
                }
            });

            $('#ddlPurposeOfTransaction').change(function () {
                if ($(this).val() == 'Others') {

                    $('.purposeOfTransaction-others').removeClass('hide');

                }
                else {
                    $('.purposeOfTransaction-others').addClass('hide');
                    $('#txtPurposeOfTransaction').val('');
                }
            });


            $('#ddlSourceOfFunds').change(function () {
                if ($(this).val() == 'Others') {
                    $('#source-others').removeClass('hide');
                }
                else {
                    $('#source-others').addClass('hide');
                }
            });

            $('.contact-main *').filter(':input').keyup(function () {
                if ($("#sameAsResidentialAddr").is(':checked')) {
                    $('#addr1M').val($('#addr1').val());
                    $('#addr2M').val($('#addr2').val());
                    $('#addr3M').val($('#addr3').val());
                    $('#cityM').val($('#city').val());
                    $('#postCodeM').val($('#postCode').val());
                    $('#stateM').val($('#state').val());
                    $('#countryM').val($('#country').val());


                }


            });
            $('.contact-main *').filter(':input').change(function () {
                if ($("#sameAsResidentialAddr").is(':checked')) {
                    $('#addr1M').val($('#addr1').val());
                    $('#addr2M').val($('#addr2').val());
                    $('#addr3M').val($('#addr3').val());
                    $('#cityM').val($('#city').val());
                    $('#postCodeM').val($('#postCode').val());
                    $('#stateM').val($('#state').val());
                    $('#countryM').val($('#country').val());
                }
            });

            function IsReCaptchaValid() {
                $.ajax({
                    url: "AccountOpening.aspx/IsReCaptchaValid",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: $('form').serializeArray(),
                    success: function (data) {
                        console.log(data);
                        var json = data.d;
                    }
                });
                return true;
            }

            //$('#txtNRIC').blur(function () {
            //    if (validateCheck == 1)
            //        ValidateStep2();
            //    else {
            //        if ($('#txtNRIC').val().trim() == "") {
            //            $('#txtNRIC').addClass('field-required');
            //        }
            //        else {
            //            $('#txtNRIC').removeClass('field-required');
            //        }
            //    }
            //});
            $('#nationalityChecks').blur(function () {
                if (validateCheck == 1)
                    ValidateStep2();
                else {
                    if ($('#nationalityChecks').val().trim() == "") {
                        $('<div class="err-msg" id="err-nationality" style="width:100%;"><small style="font-size: 12px; color: red ! important;">Nationality is required.</small></div>').insertAfter('#nationalityChecks');
                    }
                    else {
                        $('#err-nationality').remove();
                    }
                }
            });
            $('#txtNRIC').blur(function () {
                if (validateCheck == 1)
                    ValidateStep2();
                else {
                    if ($('#txtNRIC').val().trim() == "") {
                        $('#txtNRIC').addClass('field-required');
                    }
                    else {
                        $('#txtNRIC').removeClass('field-required');
                    }
                }
            });
            $('#ddlPurposeOfTransaction').blur(function () {

                if ($('#ddlPurposeOfTransaction').val().trim() == "") {
                    $('#ddlPurposeOfTransaction').addClass('field-required');
                }
                else {
                    $('#ddlPurposeOfTransaction').removeClass('field-required');
                }
            
            });
            $('#txtMobileNo').blur(function () {
                if (validateCheck == 1)
                    ValidateStep2();
                else {
                    if ($('#txtMobileNo').val().trim() == "") {
                        $('#txtMobileNo').addClass('field-required');
                    }
                    else {
                        $('#txtMobileNo').removeClass('field-required');
                    }
                }
            });

            $('#txtNRIC').inputFilter(function (value) {
                return /^([a-zA-Z0-9]){0,40}?$/.test(value);
            });

            $('#txtMobileNo').inputFilter(function (value) {
                return /^(\d{0,19})?$/.test(value);
            });

            $('#fullName').inputFilter(function (value) {
                return /^([\D]){0,40}?$/.test(value);
            });
            $('#txtEmployerName').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#postCode').inputFilter(function (value) {
                console.log(/^(\d{0,6})?$/.test(value));
                return /^(\d{0,6})?$/.test(value);
            });
            $('#postCodeM').inputFilter(function (value) {
                return /^(\d{0,6})?$/.test(value);
            });
            $('#txtPostCode').inputFilter(function (value) {
                return /^(\d{0,6})?$/.test(value);
            });
            $('#telNoH').inputFilter(function (value) {
                return /^(\d{0,20})?$/.test(value);
            });
            $('#txtTelephone').inputFilter(function (value) {
                return /^(\d{0,20})?$/.test(value);
            });
            $('#addr1').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr2').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr3').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr1M').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr2M').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr3M').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtOfficeAddress1').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtOfficeAddress2').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtOfficeAddress3').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtEmailCode').inputFilter(function (value) {
                return /^(\d{0,6})?$/.test(value);
            });
            $('#txtBankAccountNo').inputFilter(function (value) {
                //var bankcount = parseInt($('#hdnBankNoFormat').val()); //parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                //if (value.length > bankcount) {
                //    return false;
                //}
                //else {
                    return /^\d*$/.test(value);
                //}
                //return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
            });

            $('#btnRequest').click(function () {
                gRecaptchaResponse = grecaptcha.getResponse();
                $('#gRecaptchaResponse').val(gRecaptchaResponse);
                console.log(gRecaptchaResponse);
                $.ajax({
                    url: "AccountOpening.aspx/PhoneVerify",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: {
                        mobileNo: JSON.stringify($('#txtMobileNo').val()),
                        idNo: JSON.stringify($('#txtNRIC').val()),

                        gRecaptchaResponse: JSON.stringify(gRecaptchaResponse),
                        LocalIPAddress: JSON.stringify($('#hdnLocalIPAddress').val())
                    },
                    success: function (data) {

                        var jsons = data.d.Message.split(',');
                        var json = jsons[0];
                        $('#txtOTP').val('');
                        //console.log(json);
                        if (json == "sent") {
                            $('#lblInfo').html("Authentication OTP sent to mobile number " + jsons[1] + " successfully.");
                            $('#countdownLabelText').html('Please Enter your OTP in');
                            $('#countDownTime').removeClass('hide');
                            $('#btnRequest').attr('disabled', 'disabled');
                            hdnSMSExpirationTimeForAOInSeconds = jsons[2];
                            StartTimer();
                            var x = /^(\d{0,19})?$/;
                            var y = $('#txtNRIC').val();

                        }
                        else if (json == "already sent") {
                            $('#lblInfo').html("OTP already sent to mobile number " + jsons[1] + " .");
                            $('#countdownLabelText').html('Please Enter your OTP in');
                            $('#countDownTime').removeClass('hide');
                            $('#btnRequest').attr('disabled', 'disabled');
                            hdnSMSExpirationTimeForAOInSeconds = jsons[2];
                            StartTimer();
                        }
                        else if (json == "String was not recognized as a valid DateTime.") {
                            ShowCustomMessage("Alert", "Invalid NRIC/Passport format.", "");
                        }
                        else {
                            ShowCustomMessage("Alert", json, "");
                            if (json == 'Please verify yourself!' || json == 'This Id Number has already been registered' || json == 'This Id Number has already been registered as joint with you.' || json == 'Please enter valid Mobile number!' || json == 'Aged below 18 are not allowed')
                                recaptchaExpired();
                        }
                        if ($('#txtNRIC').val().length == 12 && Number.isInteger($('#txtNRIC').val())) {
                            $('#dob').attr('disabled', 'disabled');
                        }
                        else {
                            $('#dob').removeAttr('disabled');
                        }

                    },
                    error: function (jqXHR, exception) {
                        $('.loader-bg').fadeOut();
                        var msg = '';
                        if (jqXHR.status === 0) {
                            msg = 'Not connect.\n Verify Network.';
                        } else if (jqXHR.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            msg = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msg = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            msg = 'Time out error.';
                        } else if (exception === 'abort') {
                            msg = 'Ajax request aborted.';
                        } else {
                            msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        ShowCustomMessage('Error', msg, '');
                    }
                });
            });

            $('#btnCodeRequest').click(function () {
                //gRecaptchaResponse = grecaptcha.getResponse();
                //console.log(gRecaptchaResponse);
                $.ajax({
                    url: "AccountOpening.aspx/SendEmail",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { address: JSON.stringify($('#email').val()), title: JSON.stringify("Verification Code for Account Opening"), content: JSON.stringify("Verification Code"), type: 1, mobileNo: JSON.stringify($('#txtMobileNo').val()), idno: JSON.stringify($('#txtNRIC').val()), name: JSON.stringify($('#fullName').val()), relation: 0 },
                    success: function (data) {
                        var jsons = data.d.Message.split(',');
                        var json = jsons[0];
                        //console.log(json);

                        if (json == "sent") {
                            $('#lblEmailCode').html("Verification Email has been sent to your email");
                            $('#hdnEmailRequested').val('1');
                            $('#hdnEmail').val($('#email').val());
                            //$('#emailCountdownLabelText').html('You can request again in');
                            //$('#emailCountDownTime').removeClass('hide');
                            //$('#btnCodeRequest').attr('disabled', 'disabled');
                            //hdnSMSExpirationTimeForAOInSeconds = jsons[1];
                            //StartTimer();
                        }
                        else if (json == "already sent") {
                            $('#lblEmailCode').html("Verification code already sent to your email.");
                            $('#hdnEmailRequested').val('1');
                            $('#hdnEmail').val($('#email').val());
                            //$('#emailCountDownTime').removeClass('hide');
                            //$('#btnCodeRequest').attr('disabled', 'disabled');
                            //hdnSMSExpirationTimeForAOInSeconds = jsons[1];
                            //StartTimer();
                        }
                        else {
                            ShowCustomMessage("Alert", json, "");
                        }
                    }
                });
            });

            //$('#txtOTP').keyup(function () {
            //    if ($(this).val().length == 6) {
            //        var formArray = [];
            //        $('form *').filter(':input').each(function () {
            //            formArray.push({
            //                name: $(this).attr('id'),
            //                value: $(this).attr('id') == "hdnBtnClickedId" ? "btnNextStep1" : $(this).val()
            //            });
            //        });
            //        formArray = formArray.filter(function (item) {
            //            return item.name != undefined && item.name.indexOf('_') === -1;
            //        });
            //        $.ajax({
            //            url: "AccountOpening.aspx/btnNext_Click",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "POST",
            //            dataType: "JSON",
            //            data: JSON.stringify({ formInputs: formArray }),
            //            success: function (data) {
            //                var json = data.d;
            //                if (!json.IsSuccess) {
            //                    $('#txtOTP').addClass('field-required');
            //                }
            //                else {
            //                    $('#txtOTP').addClass('field-success');
            //                    //$('.buttonArea [data-step="next"]').removeClass('hide');
            //                }
            //            }
            //        });
            //    }
            //});

            $('#email').blur(function () {
                $('#err-email').remove();
                if ($('#email').val().trim() == "") {
                    $('#email').addClass('field-required');
                    $('<div id="err-email" class="err-msg"><small style="font-size: 12px; color: red ! important;">Email is required</small></div>').insertAfter('#email');
                }
                else {
                    $('#email').removeClass('field-required');
                    $('#err-email').remove();
                    $.ajax({
                        url: "AccountOpening.aspx/CheckEmail",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { email: JSON.stringify($("#email").val()) },
                        success: function (data) {
                            //$('#err-email').remove();
                            //$('#email').removeClass('field-required');

                            var json = data.d;
                            $('#err-email').remove();

                            if (data.d.Message != null) {
                                $('#email').addClass('field-required');
                                $('<div id="err-email" class="err-msg"><small style="font-size: 12px; color: red ! important;">' + data.d.Message + '</small></div>').insertAfter('#email-msg');
                            }





                        },
                        error: function (err) {
                            $('#err-email').remove();
                            $('#password').removeClass('field-required');
                        }

                    });
                    if ($('#email').val() == $('#hdnEmail').val()) {
                        if ($('#hdnEmailVerified').val() == '0') {
                            if ($('#requestCodeDiv').hasClass('hide')) {
                                $('#requestCodeDiv').removeClass('hide');
                            }
                        }
                        else {
                            if (!$('#requestCodeDiv').hasClass('hide')) {
                                $('#requestCodeDiv').addClass('hide');
                            }
                        }

                    }
                    else {
                        if ($('#requestCodeDiv').hasClass('hide')) {
                            $('#requestCodeDiv').removeClass('hide');
                        }
                    }
                }

            });
            $('#password').blur(function () {
                $('#err-password').remove();
                if ($('#password').val().trim() == "") {
                    $('#password').addClass('field-required');
                }
                else {
                    $('#password').removeClass('field-required');
                }

                $.ajax({
                    url: "AccountOpening.aspx/CheckPassword",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { password: JSON.stringify($("#password").val()) },
                    success: function (data) {
                        $('#err-password').remove();
                        $('#password').removeClass('field-required');

                        var json = data.d;


                        if (data.d.Message != null) {
                            $('#password').addClass('field-required');
                            $('<div id="err-password" class="err-msg"><small style="font-size: 12px; color: red ! important;">' + data.d.Message + '</small></div>').insertAfter('#pw-msg');
                        }





                    },
                    error: function (err) {
                        $('#err-password').remove();
                        $('#password').removeClass('field-required');
                    }

                });
            });
            $('#repassword').blur(function () {
                $('#err-repassword').remove();
                $('#passwordNotMatch').addClass('hide');
                if ($('#repassword').val().trim() == "") {
                    $('#repassword').addClass('field-required');
                    $('<div id="err-repassword" class="err-msg"><small style="font-size: 12px; color: red ! important;">Please re-enter your password</small></div>').insertAfter('#repassword');
                }
                else if ($('#password').val().trim() != $('#repassword').val().trim()) {
                    $('#repassword').addClass('field-required');
                    if ($('#passwordNotMatch').hasClass('hide')) {
                        $('#passwordNotMatch').removeClass('hide');
                    }
                }
                else
                    $('#repassword').removeClass('field-required');
            });

            $('#title').change(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#title').val() == "") {
                        $('#title').addClass('field-required');
                    }
                    else if ($('#title').val() == 'Others') {
                        $('#title').removeClass('field-required');
                        $('#titleOther').removeClass('d-none');
                    }
                    else {
                        $('#title').removeClass('field-required');
                        $('#titleOther').addClass('d-none');
                    }
                }
            });

            //$('#titleOther').blur(function () {
            //    if (validateCheckPersonalInfo == 1)
            //        ValidateStep4();
            //    else {
            //        if ($('#titleOther').val().trim() == "") {
            //            $('#titleOther').addClass('field-required');
            //        }
            //        else {
            //            $('#titleOther').removeClass('field-required');
            //        }
            //    }
            //});
            $('#fullName').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#fullName').val().trim() == "") {
                        $('#fullName').addClass('field-required');
                    }
                    else {
                        $('#fullName').removeClass('field-required');
                    }
                }
            });
            $('#dob').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#dob').val().trim() == "") {
                        $('#dob').addClass('field-required');
                    }
                    else {
                        $('#dob').removeClass('field-required');
                    }
                }
            });
            $('#gender').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#gender').val().trim() == "") {
                        $('#gender').addClass('field-required');
                    }
                    else {
                        $('#gender').removeClass('field-required');
                    }
                }
            });
            $('#race').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#race').val().trim() == "") {
                        $('#race').addClass('field-required');
                    }
                    else {
                        $('#race').removeClass('field-required');
                    }
                }
            });
            $('#maritalStatus').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#maritalStatus').val().trim() == "") {
                        $('#maritalStatus').addClass('field-required');
                    }
                    else {
                        $('#maritalStatus').removeClass('field-required');
                    }
                }
            });
            $('#bumiputraStatus').blur(function () {
                ValidateStep4();
            });

            $('#addr1').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#addr1').val().trim() == "") {
                        $('#addr1').addClass('field-required');
                    }
                    else {
                        $('#addr1').removeClass('field-required');
                    }
                }
            });
            $('#city').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#city').val().trim() == "") {
                        $('#city').addClass('field-required');
                    }
                    else {
                        $('#city').removeClass('field-required');
                    }
                }
            });
            $('#postCode').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#postCode').val().trim() == "") {
                        $('#postCode').addClass('field-required');
                    }
                    else {
                        $('#postCode').removeClass('field-required');
                    }
                }
            });
            $('#state').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#state').val().trim() == "") {
                        $('#state').addClass('field-required');
                    }
                    else {
                        $('#state').removeClass('field-required');
                    }
                }
            });
            $('#country').blur(function () {
                ValidateStep5();
            });

            $('#addr1M').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#addr1M').val().trim() == "") {
                        $('#addr1M').addClass('field-required');
                    }
                    else {
                        $('#addr1M').removeClass('field-required');
                    }
                }
            });
            $('#cityM').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#cityM').val().trim() == "") {
                        $('#cityM').addClass('field-required');
                    }
                    else {
                        $('#cityM').removeClass('field-required');
                    }
                }
            });
            $('#postCodeM').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#postCodeM').val().trim() == "") {
                        $('#postCodeM').addClass('field-required');
                    }
                    else {
                        $('#postCodeM').removeClass('field-required');
                    }
                }
            });
            $('#stateM').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#stateM').val().trim() == "") {
                        $('#stateM').addClass('field-required');
                    }
                    else {
                        $('#stateM').removeClass('field-required');
                    }
                }
            });
            $('#countryM').blur(function () {
                ValidateStep5();
            });

            $("#sameAsResidentialAddr").click(function () {
                if ($(this).is(':checked')) {
                    $('#addr1M').val($('#addr1').val());
                    $('#addr2M').val($('#addr2').val());
                    $('#addr3M').val($('#addr3').val());
                    $('#cityM').val($('#city').val());
                    $('#postCodeM').val($('#postCode').val());
                    $('#stateM').val($('#state').val());
                    $('#countryM').val($('#country').val());

                    $('#addr1M').attr('readonly', 'readonly');
                    $('#addr2M').attr('readonly', 'readonly');
                    $('#addr3M').attr('readonly', 'readonly');
                    $('#cityM').attr('readonly', 'readonly');
                    $('#postCodeM').attr('readonly', 'readonly');
                    $('#stateM').attr('readonly', 'readonly');
                    $('#countryM').attr('disabled', 'disabled');
                    $('.err-msg').remove();

                }
                else {
                    $('#addr1M').val('');
                    $('#addr2M').val('');
                    $('#addr3M').val('');
                    $('#cityM').val('');
                    $('#postCodeM').val('');
                    $('#stateM').val('');
                    $('#countryM').val('');

                    $('#addr1M').removeAttr('readonly');
                    $('#addr2M').removeAttr('readonly');
                    $('#addr3M').removeAttr('readonly');
                    $('#cityM').removeAttr('readonly');
                    $('#postCodeM').removeAttr('readonly');
                    $('#stateM').removeAttr('readonly');
                    $('#countryM').removeAttr('disabled');
                }
                ValidateStep5();
            });
            $('#ddlPurposeOfTransaction').blur(function () {
                
                    if ($('#ddlPurposeOfTransaction').val().trim() == "Select") {
                        $('#ddlPurposeOfTransaction').addClass('field-required');
                    }
                    else {
                        $('#ddlPurposeOfTransaction').removeClass('field-required');
                    }
                
            });
            $('#txtPurposeOfTransaction').blur(function () {
                
                    if ($('#ddlPurposeOfTransaction').val().trim() == "Others" && $('#txtPurposeOfTransaction').val().trim() == "") {
                        $('#txtPurposeOfTransaction').addClass('field-required');
                    }
                    else {
                        $('#txtPurposeOfTransaction').removeClass('field-required');
                    }
                
            });
            $('#ddlSourceOfFunds').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Select") {
                        $('#ddlSourceOfFunds').addClass('field-required');
                    }
                    else {
                        $('#ddlSourceOfFunds').removeClass('field-required');
                    }
                
            });
            $('#ddlRelationship').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#ddlRelationship').val().trim() == "Select") {
                        $('#ddlRelationship').addClass('field-required');
                    }
                    else {
                        $('#ddlRelationship').removeClass('field-required');
                    }
                
            });
            $('#txtFunderName').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#txtFunderName').val().trim() == "") {
                        $('#txtFunderName').addClass('field-required');
                    }
                    else {
                        $('#txtFunderName').removeClass('field-required');
                    }
                
            });
            $('#ddlFunderIndustry').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#ddlFunderIndustry').val().trim() == "Select") {
                        $('#ddlFunderIndustry').addClass('field-required');
                    }
                    else {
                        $('#ddlFunderIndustry').removeClass('field-required');
                    }
                
            });
            $('#txtFundOwnerName').blur(function () {

                if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#txtFundOwnerName').val().trim() == "") {

                        $('#txtFundOwnerName').addClass('field-required');
                    }
                    else {
                        $('#txtFundOwnerName').removeClass('field-required');
                }
                
            });
            $('#ddlEstimatedNetWorth').blur(function () {
                
                    if ($('#ddlEstimatedNetWorth').val().trim() == "Select") {
                        $('#ddlEstimatedNetWorth').addClass('field-required');
                    }
                    else {
                        $('#ddlEstimatedNetWorth').removeClass('field-required');
                    }
                
            });

            $('#addTaxResidencyStatus').click(function () {
                var divTax = $('.out-malaysia-yes').first();
                var divTaxClone = $(divTax).clone();
                $(divTaxClone).find(':input').val('');
                $(divTaxClone).find('input[type=checkbox]').val('on');
                $(divTaxClone).find('.tax-residency-remove').removeClass('hide');
                if ($(divTaxClone).find('.optionBReason').hasClass('hide')) { }
                else {
                    $(divTaxClone).find('.optionBReason').addClass('hide');
                }

                $('#multiple-tax-residency-status-row').append(divTaxClone);
            });

            $('#multiple-tax-residency-status-row').on('click', '.tax-residency-remove', function () {
                $(this).parents('.out-malaysia-yes').remove();
            });

            $('#uploadNRICFrontPage').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadNRICFrontPage').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadNRICFrontPage').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });
            $('#uploadNRICBackPage').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadNRICBackPage').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadNRICBackPage').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' />");
                    }
                }
            });

            $('#uploadSelfieFile').change(function () {
                $('#custom-popup-close').addClass('hide');
                ShowCustomMessage('Loading...', 'Please wait while we are processing the image.', '');
                try {
                    var fileInput = $(this)[0];
                    if ($(this)[0].files.length > 0) {
                        $('.loader-bg').fadeIn();
                        var fileType = $(this)[0].files[0]["type"];
                        var validImageTypes = ["image/jpeg", "image/png", "image/jpg"];
                        if ($.inArray(fileType, validImageTypes) < 0) {
                            $('#custom-popup-close').removeClass('hide');
                            $('#uploadSelfieFile').val('');
                            $('.loader-bg').fadeOut();
                            ShowCustomMessage('Alert', 'Invalid file', '');
                        }
                        else {
                            $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                            const fsize = $(this)[0].files[0].size;
                            const fileSizeMB = Math.round((fsize / 1024));
                            if (fileSizeMB >= 2048) {
                                ShowCustomMessage('Alert', 'File too Big, please select a file less than 2 MB', '');
                                $('#custom-popup-close').removeClass('hide');
                                $('#uploadSelfieFile').val('');
                                $('.loader-bg').fadeOut();
                                return false;
                            } else if (fileSizeMB < 20) {
                                ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                                $('#custom-popup-close').removeClass('hide');
                                $('#uploadSelfieFile').val('');
                                $('.loader-bg').fadeOut();
                                return false;
                            } else {
                                var FileName = $(this)[0].files[0].name;
                                var reader = new FileReader();
                                var baseString;
                                reader.onloadend = function () {
                                    baseString = reader.result;
                                    //console.log(baseString);
                                    $('#inputImg').get(0).src = baseString;
                                    updateResults();
                                };
                                reader.readAsDataURL($(this)[0].files[0]);
                                //var blobURL = URL.createObjectURL($(this)[0].files[0]);
                                //$('#inputImg').get(0).src = blobURL;
                                //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' />");
                            }
                        }
                    }
                }
                catch (ex) {
                    ShowCustomMessage('Alert', 'Invalid file', '');
                    $('#custom-popup-close').removeClass('hide');
                    $('.loader-bg').fadeOut();
                }
            });

            //$('#uploadSelfie').change(function () {
            //    var fileInput = $(this)[0];
            //    if ($(this)[0].files.length > 0) {
            //        var FileName = $(this)[0].files[0].name;
            //        var blobURL = URL.createObjectURL($(this)[0].files[0]);
            //        $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' />");
            //    }
            //});
            $('#uploadSignature').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadSignature').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadSignature').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Signature' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Signature' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Signature document' title='" + FileName + "' />");
                    }
                }
            });
            $('#uploadAdditional').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length == 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadAdditional').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadAdditional').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Additional document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                    }
                }
                else if ($(this)[0].files.length > 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    $.each($(this)[0].files, function (idx, obj) {
                        const fsize = $(this)[0].files[0].size;
                        const fileSizeMB = Math.round((fsize / 1024));
                        if (fileSizeMB >= 4096) {
                            ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                            $('#uploadAdditional').val('');
                            //return false;
                        } else if (fileSizeMB < 20) {
                            ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                            $('#uploadAdditional').val('');
                            //return false;
                        } else {
                            var FileName = obj.name;
                            var blobURL = URL.createObjectURL(obj);
                            //if (obj.type.indexOf('image') >= 0)
                            //$(fileInput).prev().find('span').append("<img class='multi' src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                            //else
                            $(fileInput).prev().find('span:not(.account-type)').append("<a class='multi' target='_blank' href='" + blobURL + "' alt='Additional document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                            //$(fileInput).prev().find('span').append("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                        }
                    });

                }
            });
            $('#uploadSupporting').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length == 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadSupporting').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadSupporting').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Supporting document' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Supporting document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                    }
                }
                else if ($(this)[0].files.length > 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    $.each($(this)[0].files, function (idx, obj) {
                        const fsize = $(this)[0].files[0].size;
                        const fileSizeMB = Math.round((fsize / 1024));
                        if (fileSizeMB >= 4096) {
                            ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                            $('#uploadSupporting').val('');
                            //return false;
                        } else if (fileSizeMB < 20) {
                            ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                            $('#uploadSupporting').val('');
                            //return false;
                        } else {
                            var FileName = obj.name;
                            var blobURL = URL.createObjectURL(obj);
                            //if (obj.type.indexOf('image') >= 0)
                            //$(fileInput).prev().find('span').append("<img class='multi' src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                            //else
                            $(fileInput).prev().find('span:not(.account-type)').append("<a class='multi' target='_blank' href='" + blobURL + "' alt='Supporting document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                            //$(fileInput).prev().find('span').append("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                        }
                    });

                }
            });

            var noOfFiles = 0;
            var loadingFiles = [];
            var formArray = [];
            $('[data-step="next"], [data-step="finish"]').click(function () {
                noOfFiles = 0;
                loadingFiles = [];
                formArray = [];
                //$('.buttonArea [data-step="next"]').addClass('hide');
                var id = $(this).attr('id');
                $('#hdnBtnClickedId').val(id);
                $('form *').filter(':input').each(function () {
                    if ($(this).attr("type") == 'file') {
                        if ($(this)[0].files.length > 0) {
                            var inputIdPrefix = $(this).attr('id')
                            $.each($(this)[0].files, function (idxFile, objFile) {
                                noOfFiles++;
                                var inputId = inputIdPrefix + (idxFile == 0 ? "" : idxFile);
                                var FileName = objFile.name;
                                var fReader = new FileReader();
                                fReader.readAsDataURL(objFile);
                                fReader.onloadend = function (event) {
                                    formArray.push({
                                        name: inputId,
                                        value: event.target.result
                                    });
                                    formArray.push({
                                        name: inputId + "FileName",
                                        value: FileName
                                    });
                                    loadingFiles.push(true);
                                    console.log(noOfFiles + " - " + loadingFiles.length);
                                    if (noOfFiles == loadingFiles.length) {
                                        btnNextStepAjax(id, formArray);
                                    }
                                }
                            });
                            //var inputId = $(this).attr('id');
                            //var FileName = $(this)[0].files[0].name;
                            ////var blobURL = URL.createObjectURL($(this)[0].files[0])
                            ////formArray.push({
                            ////    name: inputId,
                            ////    value: blobURL
                            ////});
                            ////btnNextStepAjax(id, formArray);
                            //var fReader = new FileReader();
                            //fReader.readAsDataURL($(this)[0].files[0]);
                            //fReader.onloadend = function (event) {
                            //    formArray.push({
                            //        name: inputId,
                            //        value: event.target.result
                            //    });
                            //    formArray.push({
                            //        name: inputId + "FileName",
                            //        value: FileName
                            //    });
                            //    loadingFiles.push(true);
                            //    console.log(noOfFiles + " - " + loadingFiles.length);
                            //    if (noOfFiles == loadingFiles.length) {
                            //        btnNextStepAjax(id, formArray);
                            //    }
                            //}
                        }
                    }
                    else if ($(this).attr("type") == 'checkbox') {
                        if ($(this).is(':checked')) {
                            formArray.push({
                                name: $(this).attr('id'),
                                value: $(this).val()
                            });
                        }
                        else {
                            formArray.push({
                                name: $(this).attr('id'),
                                value: null
                            });
                        }
                    }
                    else {
                        formArray.push({
                            name: $(this).attr('id'),
                            value: $(this).val()
                        });
                    }
                });
                formArray = formArray.filter(function (item) {
                    return item.name != undefined && item.name.indexOf('_') === -1;
                });
                console.log(formArray);
                //var formData = $('form').serializeArray();
                //console.log(formData);
                console.log(noOfFiles);
                if (noOfFiles == 0)
                    btnNextStepAjax(id, formArray);
                //--------------------
            });
            //$('#ddlBank').change(function () {
            //    $('#txtBankAccountNo').val('');
            //    $('#txtBankAccountNo').attr('readonly', 'readonly');
            //    if ($('#ddlBank').val() != "") {
            //        $.ajax({
            //            url: "AccountOpening.aspx/GetBankAccNoLength",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "GET",
            //            dataType: "JSON",
            //            async: true,
            //            data: { 'bankId': JSON.stringify($('#ddlBank').val()) },
            //            success: function (data) {
            //                $('#txtBankAccountNo').removeAttr('readonly');
            //                if (data.d.IsSuccess == true) {
            //                    $('#hdnBankNoFormat').val(data.d.Data);
            //                }
            //                else if (data.Message == 'Session expired') {
            //                    ShowCustomMessage("Alert", json.Message, 'AccountOpening.aspx');
            //                }
            //                else {
            //                    ShowCustomMessage('Alert', data.d.Message, '');
            //                }
            //            }
            //        });
            //    }
            //});
        });


        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        //console.log(faceapi.nets);
        faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
        faceapi.nets.faceLandmark68TinyNet.loadFromUri('/models')
        const modelPath = '/models';
        async function updateResults() {
            try {
                //const webcamElement = document.getElementById('webcam1');
                //const canvasElement = document.getElementById('canvas1');
                //const webcam = new Webcam(webcamElement, 'user', canvasElement);
                //webcam.start()
                //    .then(result => {
                //        console.log("webcam started");
                //    })
                //    .catch(err => {
                //        console.log(err);
                //    });
                //$("#snapShotFromWebCam").click(function () {
                //    console.log("webcam picture");
                //    var picture = webcam.snap();
                //    console.log(picture);
                //    $('#inputImg').get(0).src = picture;
                //});

                //faceapi.nets.tinyFaceDetector.load(modelPath);
                //faceapi.nets.faceLandmark68TinyNet.load(modelPath);
                //faceapi.nets.faceRecognitionNet.load(modelPath);
                //faceapi.nets.faceExpressionNet.load(modelPath);
                //faceapi.nets.ageGenderNet.load(modelPath);

                //if (document.getElementsByTagName("canvas").length == 0) {
                //    canvas = faceapi.createCanvasFromMedia(webcamElement)
                //    document.getElementById('webcam-container').append(canvas)
                //    faceapi.matchDimensions(canvas, displaySize)
                //}

                //faceDetection = setInterval(async () => {
                //    const detections = await faceapi.detectAllFaces(webcamElement, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks(true).withFaceExpressions().withAgeAndGender()
                //    const resizedDetections = faceapi.resizeResults(detections, displaySize)
                //    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
                //    faceapi.draw.drawDetections(canvas, resizedDetections)
                //    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
                //    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
                //    resizedDetections.forEach(result => {
                //        const { age, gender, genderProbability } = result
                //        new faceapi.draw.DrawTextField(
                //            [
                //                `${faceapi.round(age, 0)} years`,
                //                `${gender} (${faceapi.round(genderProbability)})`
                //            ],
                //            result.detection.box.bottomRight
                //        ).draw(canvas)
                //    })
                //}, 300)




                //console.log(isFaceDetectionModelLoaded());
                if (!isFaceDetectionModelLoaded()) {
                    return
                }

                const inputImgEl = $('#inputImg').get(0)
                const options = getFaceDetectorOptions()
                const useTinyModel = true
                const results = await faceapi.detectAllFaces(inputImgEl, options).withFaceLandmarks(useTinyModel)
                //console.log(results);
                if (results.length == 1) {
                    const canvas = $('#canvas').get(0);
                    console.log(canvas);
                    faceapi.matchDimensions(canvas, inputImgEl);
                    faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
                    ShowCustomMessage("Alert", "Face detected. Thank you.", "");
                    $('#custom-popup-close').removeClass('hide');
                    var img = inputImgEl;
                    var base64 = img.src;
                    $('#uploadSelfie').val(base64);
                    let r = Math.random().toString(36).substring(7);
                    console.log("random", r);
                    var contentType = img.src.split(',')[0].split(';')[0].split('/')[1];
                    var today = new Date();
                    $('#uploadSelfieFileName').val('Webcam' + r + today.getFullYear() + "" + (today.getMonth() + 1) + "" + today.getDate() + "" + today.getHours() + "" + today.getMinutes() + "" + today.getSeconds() + "" + today.getMilliseconds() + "." + contentType);
                    $('.loader-bg').fadeOut();
                    //var base64 = img.src.split(',')[1];
                    //var contentType = img.src.split(',')[0].split(';')[0];
                    //var blob = b64toBlob(base64, contentType);
                    //var blobUrl = URL.createObjectURL(blob);
                    //console.log(blobUrl);
                    //URL ***************************************************************
                    $('#video').addClass('hide');
                    $('#snap').addClass('hide');
                    $('#webcam-pic').removeClass('hide');
                    $('#cam-option').addClass('hide');
                    $('#try-snap-again').removeClass('hide');
                    $('#upload-selfie-option').addClass('hide');
                }
                else if (results.length == 0) {
                    $('.loader-bg').fadeOut();
                    ShowCustomMessage("Alert", "No face detected! Please try again.", "");
                    $('#custom-popup-close').removeClass('hide');
                    $('#try-snap-again').removeClass('hide');
                }
                else if (results.length > 1) {
                    $('.loader-bg').fadeOut();
                    const canvas = $('#canvas').get(0);
                    faceapi.matchDimensions(canvas, inputImgEl);
                    faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
                    ShowCustomMessage("Alert", "Multiple faces detected! Please try again.", "");
                    $('#custom-popup-close').removeClass('hide');
                    $('#try-snap-again').removeClass('hide');
                }
            }
            catch (ex) {
                ShowCustomMessage('Alert', 'Invalid file', '');
                $('#custom-popup-close').removeClass('hide');
                $('.loader-bg').fadeOut();
            }
        }

        updateResults();
        // Grab elements, create settings, etc.
        var video = document.getElementById('video');

        // Get access to the camera!
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            // Not adding `{ audio: true }` since we only want video now
            navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                //video.src = window.URL.createObjectURL(stream);
                video.srcObject = stream;
                video.play();
                $('#noCam').val("0");
                $('#cam-option').removeClass('hide');
            }, function () {
                document.getElementById('video').style.display = "none";
                $('#noCam').val("1");
                $('#cam-option').addClass('hide');
            });
        }

        /* Legacy code below: getUserMedia
    else if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: true }, function(stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: true }, function(stream){
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: true }, function(stream){
            video.srcObject = stream;
            video.play();
        }, errBack);
    }
    */

        // Elements for taking the snapshot
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var video = document.getElementById('video');



        // Trigger photo take
        document.getElementById("snap").addEventListener("click", function () {
            context.drawImage(video, 0, 0, 640, 480);
            var dataURL = $('#canvas')[0].toDataURL();
            //console.log(dataURL);
            $('#inputImg').get(0).src = dataURL;
            $('.loader-bg').fadeIn();
            ShowCustomMessage('Loading...', 'Please wait while we are processing the image.', '');
            $('#custom-popup-close').addClass('hide');
            updateResults();
        });

        document.getElementById("try-snap-again").addEventListener("click", function () {
            $('#video').removeClass('hide');
            $('#snap').removeClass('hide');
            $('#cam-option').removeClass('hide');
            $('#webcam-pic').addClass('hide');
            $('#try-snap-again').addClass('hide');
            $('#upload-selfie-option').removeClass('hide');
        });

        document.getElementById("existing-selfie").addEventListener("click", function () {
            $('#webcam').removeClass('hide');
            $('#existing-selfie').addClass('hide');
            $('#upload-selfie-option').removeClass('hide');
            if ($('#noCam').val() == "0") {
                $('#cam-option').removeClass('hide');
                //$('#webcam').removeClass('hide');
            }
            else {
                $('#cam-option').addClass('hide');
                //$('#webcam').addClass('hide');
                //$('#no-cam-div').removeClass('hide');
            }
        });

         $(document).ready(function () {
                $(".agentInput").on('focusout', '#agent', function (e) {
                    //ar keyCode = e.keyCode || e.which;
                    //if (keyCode == 9) {
                        //e.preventDefault();
                        // call custom function here
                        $.ajax({

                            url: "AccountOpening.aspx/AgentVerify",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            data: {
                                agentCode: JSON.stringify($('#agent').val()),
                            },
                            success: function (data, response) {

                                if (data.d != "Invalid Introducer Code" && data.d != "Please Enter Introducer Code") {
                                    $('#err-txtIntroCode').html("<span class='agTag' style='color:green; width: 100%; height: 20px; float: left; font-size: 12.5px'>Valid Agent Code" + "</span>");
                                }
                                else if (data.d == "Invalid Introducer Code") {
                                    $('#err-txtIntroCode').html("<span class='agTag' style='color:red; width: 100%; height: 20px; float: left; font-size: 12.5px'>Invalid Agent Code" + "</span>");
                                }
                                else {
                                    $('#err-txtIntroCode').html("<span class='agTag' style='color:red; width: 100%; height: 20px; float: left; font-size: 12.5px'>" + "</span>");
                                }
                                
                            },
                            error: function (jqXHR, exception) {
                                $('.loader-bg').fadeOut();
                                var msg = '';
                                if (jqXHR.status === 0) {
                                    msg = 'Not connect.\n Verify Network.';
                                } else if (jqXHR.status == 404) {
                                    msg = 'Requested page not found. [404]';
                                } else if (jqXHR.status == 500) {
                                    msg = 'Internal Server Error [500].';
                                } else if (exception === 'parsererror') {
                                    msg = 'Requested JSON parse failed.';
                                } else if (exception === 'timeout') {
                                    msg = 'Time out error.';
                                } else if (exception === 'abort') {
                                    msg = 'Ajax request aborted.';
                                } else {
                                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                                }
                                ShowCustomMessage('Error', msg, '');
                            }
                        });
                    //}
                });
              
            });
        //console.log(faceapi.nets);
        //faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
        //faceapi.nets.faceLandmark68TinyNet.loadFromUri('/models')
        //const modelPath = '/models';
        //async function updateResults() {

        //    //const webcamElement = document.getElementById('webcam1');
        //    //const canvasElement = document.getElementById('canvas1');
        //    //const webcam = new Webcam(webcamElement, 'user', canvasElement);
        //    //webcam.start()
        //    //    .then(result => {
        //    //        console.log("webcam started");
        //    //    })
        //    //    .catch(err => {
        //    //        console.log(err);
        //    //    });
        //    //$("#snapShotFromWebCam").click(function () {
        //    //    console.log("webcam picture");
        //    //    var picture = webcam.snap();
        //    //    console.log(picture);
        //    //    $('#inputImg').get(0).src = picture;
        //    //});

        //    //faceapi.nets.tinyFaceDetector.load(modelPath);
        //    //faceapi.nets.faceLandmark68TinyNet.load(modelPath);
        //    //faceapi.nets.faceRecognitionNet.load(modelPath);
        //    //faceapi.nets.faceExpressionNet.load(modelPath);
        //    //faceapi.nets.ageGenderNet.load(modelPath);

        //    //if (document.getElementsByTagName("canvas").length == 0) {
        //    //    canvas = faceapi.createCanvasFromMedia(webcamElement)
        //    //    document.getElementById('webcam-container').append(canvas)
        //    //    faceapi.matchDimensions(canvas, displaySize)
        //    //}

        //    //faceDetection = setInterval(async () => {
        //    //    const detections = await faceapi.detectAllFaces(webcamElement, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks(true).withFaceExpressions().withAgeAndGender()
        //    //    const resizedDetections = faceapi.resizeResults(detections, displaySize)
        //    //    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        //    //    faceapi.draw.drawDetections(canvas, resizedDetections)
        //    //    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        //    //    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
        //    //    resizedDetections.forEach(result => {
        //    //        const { age, gender, genderProbability } = result
        //    //        new faceapi.draw.DrawTextField(
        //    //            [
        //    //                `${faceapi.round(age, 0)} years`,
        //    //                `${gender} (${faceapi.round(genderProbability)})`
        //    //            ],
        //    //            result.detection.box.bottomRight
        //    //        ).draw(canvas)
        //    //    })
        //    //}, 300)




        //    console.log(isFaceDetectionModelLoaded());
        //    if (!isFaceDetectionModelLoaded()) {
        //        return
        //    }

        //    const inputImgEl = $('#inputImg').get(0)
        //    const options = getFaceDetectorOptions()
        //    const useTinyModel = true
        //    const results = await faceapi.detectAllFaces(inputImgEl, options).withFaceLandmarks(useTinyModel)
        //    const canvas = $('#canvas').get(0);
        //    console.log(canvas);
        //    faceapi.matchDimensions(canvas, inputImgEl);
        //    faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
        //}

        //updateResults();
        //// Grab elements, create settings, etc.
        //var video = document.getElementById('video');

        //// Get access to the camera!
        //if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        //    // Not adding `{ audio: true }` since we only want video now
        //    navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
        //        //video.src = window.URL.createObjectURL(stream);
        //        video.srcObject = stream;
        //        video.play();
        //    });
        //}

        /* Legacy code below: getUserMedia
    else if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: true }, function(stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: true }, function(stream){
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: true }, function(stream){
            video.srcObject = stream;
            video.play();
        }, errBack);
    }
    */

        //// Elements for taking the snapshot
        //var canvas = document.getElementById('canvas');
        //var context = canvas.getContext('2d');
        //var video = document.getElementById('video');



        //// Trigger photo take
        //document.getElementById("snap").addEventListener("click", function () {
        //    context.drawImage(video, 0, 0, 640, 480);
        //    var dataURL = $('#canvas')[0].toDataURL();
        //    //console.log(dataURL);
        //    $('#inputImg').get(0).src = dataURL;
        //    updateResults();
        //});


        $(function () {
            $('#occupation').change(function () {
                if ($(this).val() == 'Employment') {
                    $('#industryDiv').removeClass('d-none');
                }
                else {
                    $('#industryDiv').addClass('d-none');
                }
            });
        });
    </script>
</asp:Content>
