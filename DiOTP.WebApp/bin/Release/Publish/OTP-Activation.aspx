﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="OTP-Activation.aspx.cs" Inherits="DiOTP.WebApp.OTP_Activation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .required-input:after {
            content: "*";
            color: red;
            font-size: 1.15em;
            position: absolute;
            margin-top: -8px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        function clientValidation() {
            var isValid = true;
            var txtICNo = $('#txtICNo').val();
            if (!txtICNo) {
                $('#txtICNo').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
                isValid = false;
            } else {
                $('#txtICNo').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            }
            var txtMANumber = $('#txtMANumber').val();
            if (!txtMANumber) {
                $('#txtMANumber').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
                isValid = false;
            } else {
                $('#txtMANumber').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            }
            var txtEmail = $('#txtEmail').val();
            if (!txtEmail) {
                $('#txtEmail').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
                isValid = false;
            } else {
                $('#txtEmail').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            }
            if (!isValid) {
                //ShowCustomMessage('Alert', 'Please enter required(*) fields.', '');
                //$('#divMessage').addClass("alert text-danger");
                //$('#divMessage').html("<i class='fa fa-close icon'></i> Please enter required(*) fields.");
            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            return isValid;
        }
        /**
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        */
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });
    </script>

    <section class="section image">
        <div class="inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6">
                        <div class="Register-section">
                            <div class="register-head">
                                <h4><strong>Investor Registration</strong></h4>
                            </div>
                            <div class="register-content">
                                <%-- <div>
                                    <h4 class="mt-0 mb-8 fs-16">Select Identification Type:</h4>
                                    <div>
                                        <label class="radio-inline mb-10">
                                            <asp:RadioButton ID="rdnNRIC" runat="server" Text="New NRIC" GroupName="IC" />
                                        </label>
                                        <label class="radio-inline mb-10">
                                            <asp:RadioButton ID="rdnOthers" runat="server" Text="Old NRIC/Others" GroupName="IC" />
                                        </label>
                                    </div>
                                </div>--%>
                                <%--<small class="mb-4" style="font-family: 'Montserrat' , sans-serif; font-size: 12px; font-weight: 600; text-align: left;">NOTE: Please add dash (-) in new NRIC.</small>--%>
                                <div id="divIC" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="txtICNo" runat="server" CssClass="form-control" placeholder="NRIC / OLD NRIC / PASSPORT" ClientIDMode="Static"></asp:TextBox>
                                </div>

                                <div id="divMA" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="txtMANumber" runat="server" CssClass="form-control" placeholder="Enter your MasterAccount Number" ClientIDMode="Static"></asp:TextBox>
                                </div>
                                <div id="divEmail" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Enter your Email" ClientIDMode="Static"></asp:TextBox>
                                </div>
                                <%--<div class="input-group" style="visibility:hidden">
                                    <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email will be auto filled" ReadOnly="true"></asp:TextBox>
                                </div>--%>
                                
                                <div>
                                    <small><strong>Note:</strong> Upon successful registration, an OTP is required to activate eApexIs Account.</small>
                                </div>

                                <div class="margin-top
                                    
                                    
                                    
                                    " style="margin-top: 20px !important;">
                                    <div id="captcha" class="g-recaptcha" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>
                                </div>
                                <div style="height:25px; margin-top: 15px !important;">
                                    <asp:Button ID="btnRequestCode" runat="server" CssClass="btn login-btn pull-right" OnClientClick="return clientValidation()" OnClick="btnRequestCode_Click" Text="Submit" Enabled="true" />
                                    <asp:Button ID="btnRequestCodeForAdditionalAcc" runat="server" CssClass="btn login-btn pull-right" OnClientClick="return clientValidation()" OnClick="btnRequestCodeForAdditionalAcc_Click" Visible="false" Text="Submit" />
                                    <a id="cancelBtn" runat="server" href="Portfolio.aspx" class="btn pull-right" style="margin-left: 0;">Cancel</a>
                                </div>
                                <div style="margin-top: 10px !important;" id="unregisteredUser" runat="server">
                                    <p id="link-login-desk" style="color: #000; margin-top: 15px !important;">Already Registered?  <a href="/Index.aspx" class="ml-10"><strong>Login Here</strong></a></p>

                                    <p id="link-login-mobile" style="color: #fff; margin-top: 15px !important;">Already Registered?  <a href="/Login.aspx" class="ml-10"><strong>Login Here</strong></a></p>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 text-center hide">
                        <div id="divMessage" runat="server" clientidmode="Static"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="modalPlace" ID="modalHolder" runat="server">
    <div class="modal fade" id="modalMobileNoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm sett" role="document" style="width: 500px;">
            <div class="modal-content">
                <asp:Button ID="btnExit" runat="server" Text="x" OnClick="btnExit_Click" Visible="true" Style="position: relative; float: right; background-color: transparent; border: none;" />
                <div class="modal-header sett-modalhead text-white">
                    <strong>Confirmation</strong>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <tbody id="registrationDetailsTbody" runat="server">

                                </tbody>
                            </table>
                            <label id="lblMobileNo" runat="server" clientidmode="static" class="text-justify"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnProceed" runat="server" CssClass="btn login-btn pull-right" OnClick="btnProceed_Click" Text="Proceed" Enabled="true" ClientIDMode="Static" />
                    <button type="button" style="margin-top: -1px;" class="btn pull-right" onclick="javascript:window.location.href='OTP-Activation.aspx';">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnMobileNoPop" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" ID="scriptsPlace" runat="server">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
    <script>
        var txtICNoIsDisabled = $('#txtICNo').attr('disabled');
        if (txtICNoIsDisabled) {
            $('#cancelBtn').attr('href', 'Portfolio.aspx');
        }
        else {
            $('#cancelBtn').attr('href', 'Index.aspx');
        }
        var hdnMobileNoPop = parseInt($('#hdnMobileNoPop').val());
        $(document).ready(function () {

            $('#btnProceed').click(function () {
                $('.loader-bg').fadeIn();
                setTimeout(function () {
                    $('#btnProceed').attr('disabled', true);
                }, 100);
            });

            if (hdnMobileNoPop == 1) {
                $('#modalMobileNoPop').modal({
                    keyboard: false,
                    backdrop: "static"
                });
            }
        });
    </script>
</asp:Content>
