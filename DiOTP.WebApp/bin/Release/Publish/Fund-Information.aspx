﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Fund-Information.aspx.cs" Inherits="DiOTP.WebApp.Funds_Information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link rel="stylesheet" href="/Content/cdn-cgi/apps/head/CZ3CFmKcMfCcupa_86mxMcuVsN8.js" />--%>
    <link href="/Content/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/fixedHeader.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/responsive.bootstrap.min.css" rel="stylesheet" />

    <style>
        body > .printable {
            display: none !important;
        }

        @media print {
            body {
                margin: 0 !important;
            }

                body > :not(.printable), .not-printable {
                    display: none !important;
                }

            .printable-section {
                padding-top: 0;
                padding-bottom: 0;
            }

            body > .printable {
                display: block !important;
            }

            body > div:last-child {
                display: none !important;
            }

            .my-nav-tabs li:not(.active) {
                display: none;
            }
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section printable-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="pull-right not-printable">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx" id="homeLink" runat="server">Home</a></li>
                            <li><a href="/Funds-listing.aspx">Fund Center</a></li>
                            <li class="active">Fund Information</li>
                        </ol>
                    </div>

                    <h3 class="hr-left mb-20 pb-12 d-ib mob-fund-head tab-fund-head"><span id="fundNameHeading" runat="server"></span></h3>

                    <button class="print btn my-btn hidden-sm hidden-xs" type="button"><i class="fa fa-print"></i>Print</button>


                    <div class="tabs-horizontal my-tabs mob">
                        <ul class="nav nav-tabs nav-justified my-nav-tabs">
                            <li class="active">
                                <a href="#tab8" data-toggle="tab" class="mr-5"><i class="fa fa-info-circle mr-6"></i>
                                    <span class="text-uppercase">Information</span></a>
                            </li>
                            <li>
                                <a href="#tab7" data-toggle="tab" class="mr-5 ml-5"><i class="fa fa-money mr-6"></i>
                                    <span class="text-uppercase">Charges</span></a>
                            </li>
                            <li>
                                <a href="#tab9" data-toggle="tab" class="mr-5 ml-5"><i class="fa fa-save mr-6"></i>
                                    <span class="text-uppercase">Downloads</span></a>
                            </li>
                            <li>
                                <a href="#tab6" data-toggle="tab" class="ml-5"><i class="fa fa-line-chart mr-6"></i>
                                    <span class="text-uppercase">Charts</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab8">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="table-head">
                                            <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Information</h5>
                                        </div>

                                        <table id="example" class="display table table-bordered my-table" style="width: 100%; cellspacing: 0;">

                                            <tbody>
                                                <tr>
                                                    <th>Category</th>
                                                    <td id="fundCategory" runat="server">January 12, 2018</td>
                                                </tr>
                                                <tr>
                                                    <th>Launch Date</th>
                                                    <td id="fundLaunchDate" runat="server">January 12, 2018</td>
                                                </tr>
                                                <tr>
                                                    <th>Launch Price</th>
                                                    <td id="fundLaunchPrice" runat="server">MYR 0.50</td>
                                                </tr>
                                                <tr>
                                                    <th>Pricing Basis</th>
                                                    <td id="fundPricingBasis" runat="server">Forward Pricing</td>
                                                </tr>
                                                <tr>
                                                    <th><span>Latest NAV Price</span> (<a href="#navPriceHistory" data-toggle="modal">Click here for price history</a>)</th>
                                                    <td id="fundLatestNavPrice" runat="server">MYR 0.5008 (January 17, 2018)</td>
                                                </tr>
                                                <tr>
                                                    <th><span>Historical Income Distribution</span> (<a href="#dividendHistory" data-toggle="modal">Details</a>)</th>
                                                    <td id="fundHistoricalIncomeDistribution" runat="server">Yes</td>
                                                </tr>
                                                <tr>
                                                    <th>Approved by EPF</th>
                                                    <td id="fundApprovedByEPF" runat="server">Yes</td>
                                                </tr>
                                                <tr>
                                                    <th>Shariah Compliant</th>
                                                    <td id="fundShariahComplaint" runat="server">Yes</td>
                                                </tr>
                                                <tr>
                                                    <th>Risk Rating</th>
                                                    <td id="fundRiskRating" runat="server">0-Lowest Risk</td>
                                                </tr>
                                                <tr>
                                                    <th>Fund Size</th>
                                                    <td id="fundSize" runat="server">MYR 345.38 million (as at January 15, 2018)</td>
                                                </tr>
                                                <tr>
                                                    <th id="fundMinimumInitialInvestmentTitle" runat="server">Minimum Initial Investment (CASH/EPF)</th>
                                                    <td id="fundMinimumInitialInvestment" runat="server">MYR 5,000 / MYR 5,000</td>
                                                </tr>
                                                <tr>
                                                    <th id="fundMinimumSubsequentInvestmentTitle" runat="server">Minimum Subsequent Investment (CASH/EPF)</th>
                                                    <td id="fundMinimumSubsequentInvestment" runat="server">MYR 1,000 / MYR 1,000</td>
                                                </tr>
                                                <tr>
                                                    <th>Minimum RSP Investment (Initial/Additional)</th>
                                                    <td id="fundMinimumRSPInvestment" runat="server">MYR 500 / MYR 1,000</td>
                                                </tr>
                                                <tr>
                                                    <th>Minimum Redemption Amount</th>
                                                    <td id="fundMinimumRedemptionAmount" runat="server">100 Units</td>
                                                </tr>
                                                <tr>
                                                    <th>Minimum Holding</th>
                                                    <td id="fundMinimumHolding" runat="server">1000 Units</td>
                                                </tr>
                                                <tr>
                                                    <th colspan="2">
                                                        <h5>Cooling-off Period</h5>
                                                        <p class="text-justify" id="fundCoolingOffPeriod" runat="server">Unit Holder can exercise their cooling off right within six (6) business days from the transaction date.</p>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th colspan="2">
                                                        <h5>Distribution Policy</h5>
                                                        <p class="text-justify" id="fundDistributionPolicy" runat="server">Unit Holder can exercise their cooling off right within six (6) business days from the transaction date.</p>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th colspan="2">
                                                        <h5>Investment Objective</h5>
                                                        <p class="text-justify" id="fundInvestmentObjective" runat="server">Unit Holder can exercise their cooling off right within six (6) business days from the transaction date.</p>
                                                    </th>
                                                </tr>
                                                <tr id="fundInvestmentStrategyAndPolicyTD" runat="server" visible="false">
                                                    <th colspan="2">
                                                        <h5>Investment Strategy and Policy</h5>
                                                        <p class="text-justify" id="fundInvestmentStrategyAndPolicy" runat="server">Unit Holder can exercise their cooling off right within six (6) business days from the transaction date.</p>
                                                    </th>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="tab7">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-head">
                                            <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Charges</h5>
                                        </div>

                                        <table id="example1" class="display table table-bordered my-table" style="width: 100%; cellspacing: 0;">

                                            <tbody>
                                                <tr>
                                                    <th>Sales Charges</th>
                                                    <td id="fundDiscountedInitialSalesCharge" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>EPF Sales Charges</th>
                                                    <td id="fundEpfSalesCharge" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>Annual Management Charge</th>
                                                    <td id="fundAnnualManagementCharge" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>Trustee Fee</th>
                                                    <td id="fundTrusteeFee" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>Switching Fee</th>
                                                    <td id="fundSwitchingFee" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>Redemption Fee</th>
                                                    <td id="fundRedemptionFee" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>Transfer Fee</th>
                                                    <td id="fundTransferFee" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <th>Other Significant Fees</th>
                                                    <td id="fundOtherSignificantFee" runat="server">Switching Fee: MYR 25.00</td>
                                                </tr>
                                                <tr>
                                                    <td id="fundGSTFee" runat="server" colspan="2" style="display: none;"></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="tab9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-head">
                                            <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Downloads</h5>
                                        </div>

                                        <div class="panel-group" id="downloadFileLinks" runat="server">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="tab6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-head">
                                            <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Charts</h5>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mt-15 annualized hide">
                                                    <h6 class="mb-2" style="color: #3392b1;">FUND PERFORMANCE ( BID-TO-BID ANNUALIZED RETURNS). </h6>
                                                    <div class="table-responsive">
                                                        <table class="table chart-table table-bordered nowrap myDataTable" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr class="fs-13">
                                                                    <th style="width: 35%">Period</th>
                                                                    <th>3 months</th>
                                                                    <th>6 months</th>
                                                                    <th>1 year</th>
                                                                    <th>2 years</th>
                                                                    <th>3 years</th>
                                                                    <th>5 years</th>
                                                                    <th>10 years</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="fs-13">
                                                                    <td>BID TO BID Returns (%) - MYR </td>
                                                                    <td>0.24</td>
                                                                    <td>-1.88</td>
                                                                    <td>2.18</td>
                                                                    <td>3.14</td>
                                                                    <td>5.60</td>
                                                                    <td>4.88</td>
                                                                    <td>8.34</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="mt-15 cumulative">
                                                    <h6 class="mb-2" style="color: #3392b1;"></h6>
                                                    <div class="table-responsive">
                                                        <table class="table chart-table table-bordered myDataTable fund-table fundPerformanceCumulativeTable">
                                                            <thead>
                                                                <tr class="fs-13">
                                                                    <th style="width: 35%">Period <small class="pull-right">(Last updated on: <span id="dividendDateValue" runat="server"></span>)</small></th>
                                                                    <th>1 week </th>
                                                                    <th>1 month </th>
                                                                    <th>3 months</th>
                                                                    <th>6 months</th>
                                                                    <th>1 year</th>
                                                                    <th>2 years</th>
                                                                    <th>3 years</th>
                                                                    <th>5 years</th>
                                                                    <th>10 years</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="fs-13">
                                                                    <td>Cumulative Performance (%)<%-- - MYR--%> </td>
                                                                    <td><span id="weekCValue" runat="server"></span></td>
                                                                    <td><span id="monthCValue" runat="server"></span></td>
                                                                    <td><span id="month3CValue" runat="server"></span></td>
                                                                    <td><span id="month6CValue" runat="server"></span></td>
                                                                    <td><span id="yearCValue" runat="server"></span></td>
                                                                    <td><span id="year2CValue" runat="server"></span></td>
                                                                    <td><span id="year3CValue" runat="server"></span></td>
                                                                    <td><span id="year5CValue" runat="server"></span></td>
                                                                    <td><span id="year10CValue" runat="server"></span></td>
                                                                </tr>
                                                                <tr class="fs-13 hide">
                                                                    <td>Average </td>
                                                                    <td><span id="weekCValueAvg" runat="server"></span></td>
                                                                    <td><span id="monthCValueAvg" runat="server"></span></td>
                                                                    <td><span id="month3CValueAvg" runat="server"></span></td>
                                                                    <td><span id="month6CValueAvg" runat="server"></span></td>
                                                                    <td><span id="yearCValueAvg" runat="server"></span></td>
                                                                    <td><span id="year2CValueAvg" runat="server"></span></td>
                                                                    <td><span id="year3CValueAvg" runat="server"></span></td>
                                                                    <td><span id="year5CValueAvg" runat="server"></span></td>
                                                                    <td><span id="year10CValueAvg" runat="server"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr class="fs-12">
                                                                    <td colspan="10" style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;'>Performance figures are absolute returns based on the price of the fund as at <strong id="fundPricedate" runat="server">WHEN?</strong>, on NAV-to-NAV basis with dividends being 'reinvested' on the dividend date.</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="row mt-20">
                                                    <div class="col-md-8 col-md-offset-2 text-center">
                                                        <ul class="nav nav-tabs chart-tabs d-ib">
                                                            <!-- chart-tabs: Used in script -->
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="1">YTD</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="2">Last Year</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="3">1 WK</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="4">1 MTH</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="5">3 MTH</a>
                                                            </li>
                                                            <li>
                                                                <a class="active" href="#" data-toggle="tab" data-value="6">6 MTH</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="7">1 YR</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="8">2 YR</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="9">3 YR</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="10">5 YR</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tab" data-value="11">10 YR</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="loadingChartDiv">
                                                            <div class="typing_loader"></div>
                                                            <div class="text-center">Loading...</div>
                                                            <%--<i class="fa fa-spinner fa-spin"></i>--%>
                                                        </div>
                                                        <div id="linechart"></div>
                                                        <div class="table-responsive">
                                                        <table class="table chart-table table-bordered mb-5 myDataTable fund-table">
                                                            <thead>
                                                                <tr class="fs-13">
                                                                    <th width="35%">Fund Name</th>
                                                                    <th>Period</th>
                                                                    <th>Total Returns(%) </th>
                                                                    <th class="hide">3 yr Annualised Volatility(%) </th>
                                                                    <th>Min NAV </th>
                                                                    <th>Max NAV </th>
                                                                    <th>Current NAV </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="chartTableBody">
                                                            </tbody>
                                                        </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mt-15">
                                                    <h6 class="mb-2" style="color: #3392b1;">FUND PERFORMANCE ( BID-TO-BID CUMULATIVE RETURNS). </h6>
                                                    <div class="table-responsive">
                                                        <table class="table chart-table table-bordered mb-5 myDataTable fund-table">
                                                            <thead>
                                                                <tr class="fs-13">
                                                                    <th style="width: 35%">Period</th>
                                                                    <th>1yr high </th>
                                                                    <th>1yr low </th>
                                                                    <th>3yr high </th>
                                                                    <th>3yr low </th>
                                                                    <th>All time high </th>
                                                                    <th>All time low </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="fs-13">
                                                                    <td>Price(MYR) </td>
                                                                    <td><span id="NAV1HIGH" runat="server"></span></td>
                                                                    <td><span id="NAV1LOW" runat="server"></span></td>
                                                                    <td><span id="NAV3HIGH" runat="server"></span></td>
                                                                    <td><span id="NAV3LOW" runat="server"></span></td>
                                                                    <td><span id="NAVALLHIGH" runat="server"></span></td>
                                                                    <td><span id="NAVALLLOW" runat="server"></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div   >
                                                    <p class="fs-12" style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif; line-height: 15px;'>Historical prices shown are NAV prices. The " All time high" and " All time low" prices are the highest and the lowest NAV prices from the first dealing date since inception and the latest  available dealing date respectively.</p>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnFundId" runat="server" ClientIDMode="Static" />

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">

    <div class="modal fade" id="navPriceHistory" tabindex="-1" role="dialog" aria-labelledby="navPriceHistory">
        <div class="modal-dialog" role="document" style="margin: 10px auto; height: calc(100vh - 20px);">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <%--<h4 class="modal-title">
                        <a id="fundNameSpanNavPriceHistory" runat="server"></a>
                        <br />
                        <small>(Fund Prices for last 3 months)</small>
                    </h4>--%>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12" style="height: calc(100vh - 130px); overflow-y: scroll;">
                            <table id="navPriceHistoryTable" class="table myDataTable table-condensed table-striped dataTable table-cell-pad-5 my-popup">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nav Price</th>
                                        <th>Nav Date</th>
                                    </tr>
                                </thead>
                                <tbody id="navPriceHistoryTableTbody" runat="server">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="dividendHistory" tabindex="-1" role="dialog" aria-labelledby="navPriceHistory">
        <div class="modal-dialog two" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom: 1px;">



                    <div class="header-title" style="background-color: #059cce; color: white; text-align: center;">

                        <div style="padding: 10px;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&nbsp;&nbsp;&nbsp;<span aria-hidden="true" style="color: white;">&times;</span></button>
                            <span>Historical Income Distribution</span>
                        </div>
                        <div id="dividendHistoryFundName" runat="server" style="padding: 10px; padding-top: 0px;">
                            Fund Name:
                        </div>
                    </div>

                    <%--<h4 class="modal-title">
                        <span>HISTORICAL INCOME DISTRIBUTION (ALLOCATED)</span>
                        <span id="fundNameSpanDividendHistory" runat="server"></span>
                    </h4>--%>
                </div>
                <div class="modal-body" style="padding-top: 1px;">

                    <div class="row">
                        <div class="col-md-12">
                            <table id="dividendHistoryTable" class="table myDataTable table-condensed table-striped dataTable table-cell-pad-5 my-popup" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th style="padding-left: 5px !important;">Ex Date</th>
                                        <th style="padding-left: 15px !important;">Reinvestment Date</th>
                                        <th style="padding-left: 15px !important;">Gross Income Distribution Rate</th>
                                        <th style="padding-left: 15px !important;">Net Income Distribution Rate</th>
                                    </tr>
                                </thead>
                                <tbody id="dividendHistoryTableTbody" runat="server" style="text-align: center;">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="fs-12 pull-left" style="display: none;"><strong>Note:</strong> Reinvestment Date refers to the date that the fund house sends us the dividend proceeds. The allocation of dividends to investor will take up to 2 weeks.</span>
                </div>
            </div>
        </div>

    </div>
    <script src="/Content/js/jquery.dataTables.min.js"></script>
    <script src="/Content/js/dataTables.bootstrap.min.js"></script>
    <script src="/Content/js/dataTables.responsive.min.js"></script>
    <script src="/Content/js/responsive.bootstrap.min.js"></script>
    <script src="/Content/js/highcharts.js"></script>
    <%--<script src="https://code.highcharts.com/modules/exporting.js"></script>--%>

    <script>
        var chart;
        var dataTable;
        var scriptsApplied = 0;
        var fundChartObject;
        var dataValue = 0;
        $(document).ready(function () {

            $('[href="#dividendHistory"]').click(function () {
                setTimeout(function () {
                    $('#dividendHistoryTable').DataTable({
                        destroy: true,
                        dom: '',
                        responsive: true,
                        sorting: [],
                        searching: false,
                        paging: false,
                        ordering: false
                    });
                }, 200);
            });
            $('[href="#navPriceHistory"]').click(function () {
                setTimeout(function () {
                    $('#navPriceHistoryTable').DataTable({
                        destroy: true,
                        dom: '',
                        responsive: true,
                        sorting: [],
                        searching: false,
                        paging: false,
                        ordering: false
                    });
                }, 200);
            });
            $(document).bind("keyup keydown", function (e) {
                if (e.ctrlKey && e.keyCode == 80) {
                    $('.print').click();
                    return false;
                }
            });
            $('.print').click(function () {
                if ($('[href="#tab6"]').parent('li').hasClass('active'))
                    beforeChartPrint();
                $('.printable').html($('.printable-section')[0].outerHTML);

                $('body > div:last-child').attr('style', 'display: none !important');

                setTimeout(() => { $('body > div:last-child').attr('style', 'display: block !important'); }, 1000);

                window.print();
                afterChartPrint();
            });
            function PlotChart() {
                $('.loadingChartDiv').fadeIn();
                var selectedFunds = new Array(1);
                selectedFunds[0] = (parseInt($('#hdnFundId').val()));
                $.ajax({
                    url: "Fund-Comparison.aspx/GetFundChartSeries",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { 'selectedFunds': JSON.stringify(selectedFunds), 'dataValue': dataValue },
                    success: function (data) {
                        var json = data.d;
                        if (json.IsSuccess) {
                            var html = "";
                            fundChartObject = json.Data;
                            if (chart != null) {
                                while (chart.series.length > 0) chart.series[0].remove(true);
                                chart.xAxis[0].setCategories(json.Data[0].Labels);
                                //chart.yAxis[0].setCategories(json[0].NavPrices);

                                $.each(json.Data, function (idx, y) {
                                    var series = [];
                                    for (var x = 0; x < y.Values.length; x++) {
                                        series.push(parseFloat(y.Values[x]));
                                    }
                                    chart.addSeries({
                                        data: series,
                                        name: y.fundName
                                    });
                                    chart.setTitle({
                                        text: '<p style="font-size:12px; margin-bottom:5px; color:#aaaaaa;">Based on Change </p>' + ' <sup><a class="label label-primary not-printable" target="_blank" href="/Fund-Comparison.aspx?fundCode=' + y.fundCode + '">Go to fund comparison chart</a></sup>'
                                    });
                                    dataTable = $('.myDataTable').DataTable({
                                        destroy: true,
                                        dom: '',
                                        //responsive: true,
                                        sorting: [],
                                        searching: false,
                                        paging: false,
                                        ordering: false
                                    });
                                    var date = formatDate(y.currentNavDate);
                                    var dataValueName = $('[data-value="' + dataValue + '"]').text();
                                    $('#chartTableBody').html("<tr class='fs-13'><td>" + y.fundName + "</td><td>" + dataValueName + "</td><td>" + y.totalReturns.toFixed(2) + "</td><td class='hide'>" + y.threeYearAnnualisedPercent + "</td><td>" + y.minNav.toFixed(4) + "</td><td>" + y.maxNav.toFixed(4) + "</td><td>" + y.currentNav.toFixed(4) + " (" + date + ")</td></tr>");
                                });
                            }
                        }
                        else {
                            ShowCustomMessage('Exception', json.Message, '');
                        }
                    }
                });
            }
            function formatDate(input) {
                var datePart = input.match(/\d+/g),
                    year = datePart[0], // get only two digits
                    month = datePart[1], day = datePart[2];
                return day + '/' + month + '/' + year;
            }

            function ApplyScripts() {
                if (scriptsApplied == 0 || (scriptsApplied == 1 && dataValue != 0)) {
                    var labelLoading;
                    chart = Highcharts.chart('linechart', {
                        chart: {
                            resetZoomButton: {
                                position: {
                                    // align: 'right', // by default
                                    // verticalAlign: 'top', // by default
                                    x: 0,
                                    y: -80
                                }
                            },
                            height: '450px',
                            type: 'line',
                            events:
                            {
                                addSeries: function () {
                                    $('.loadingChartDiv').fadeOut();
                                    var label = this.renderer.label('A series was added', 30, 30)
                                        .attr({
                                            fill: Highcharts.getOptions().colors[0],
                                            padding: 10,
                                            zIndex: 8
                                        })
                                        .css({
                                            color: '#FFFFFF'
                                        })
                                        .add();

                                    setTimeout(function () {
                                        label.fadeOut();
                                    }, 1000);
                                },
                                load: function () {
                                }
                            },
                            borderColor: '#000000',
                            borderWidth: 0,
                            inverted: false,
                            zoomType: 'x',
                            spacingRight: 20
                        },
                        title: {
                            useHTML: true,
                            text: '<p style="font-size:12px; margin-bottom:5px; color:#aaaaaa;">Based on Change </p>' + ' <sup><a class="label label-primary" target="_blank" href="/Fund-Comparison.aspx?fund=">Go to fund comparison chart</a></sup>'
                        },
                        subtitle: {
                            useHTML: true,
                            text: (document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' :
                                '<div class="pinchZoom">Pinch the chart to zoom in</div>')
                        },
                        xAxis: {
                            allowDecimals: true,
                            labels: {
                                rotation: -45,
                                step: 30,
                                showLastLabel: true,
                                endOnTick: true,
                                x: -10,
                                formatter: function () {
                                    var x = this.value;
                                    var fundDetails = fundChartObject[0];
                                    var labelIndex = jQuery.inArray(($.grep(fundDetails.Labels, function (obj) {
                                        return obj === x;
                                    })[0]), fundDetails.Labels);
                                    var navDate = fundDetails.NavDates[labelIndex];
                                    var xLabelYears = $.unique($.map(fundDetails.NavDates, function (obj) {
                                        return (new Date(obj).getFullYear());
                                    }));
                                    if (xLabelYears.length == 1) {
                                        var xLabelMonths = $.unique($.map(fundDetails.NavDates, function (obj) {
                                            return (new Date(obj).getMonth() + 1);
                                        }));
                                        if (xLabelMonths.length == 2) {
                                            chart.xAxis[0].options.labels.step = 3;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                        else if (xLabelMonths.length == 1) {
                                            chart.xAxis[0].options.labels.step = 1;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                        else if (xLabelMonths.length == 4) {
                                            chart.xAxis[0].options.labels.step = 10;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                        else if (xLabelMonths.length == 12) {
                                            console.log(xLabelMonths.length);
                                            chart.xAxis[0].options.labels.step = 30;
                                            return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                        }
                                        else {
                                            chart.xAxis[0].options.labels.step = 14;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                    }
                                    else if (xLabelYears.length == 2) {
                                        var xLabelMonths = $.unique($.map(fundDetails.NavDates, function (obj) {
                                            return (new Date(obj).getMonth() + 1);
                                        }));
                                        if (xLabelMonths.length == 1) {
                                            chart.xAxis[0].options.labels.step = 1;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                        else if (xLabelMonths.length == 2) {
                                            chart.xAxis[0].options.labels.step = 3;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                        else if (xLabelMonths.length == 4) {
                                            chart.xAxis[0].options.labels.step = 10;
                                            return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                        }
                                        else if (xLabelMonths.length == 12) {
                                            console.log(xLabelMonths.length);
                                            chart.xAxis[0].options.labels.step = 30;
                                            return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                        }
                                        else {
                                            chart.xAxis[0].options.labels.step = 29;
                                            return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                        }
                                    }
                                    else if (xLabelYears.length == 3) {
                                        chart.xAxis[0].options.labels.step = 58;
                                        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                    }
                                    else if (xLabelYears.length == 4) {
                                        chart.xAxis[0].options.labels.step = 1;
                                        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                    }
                                    else if (xLabelYears.length == 6) {
                                        chart.xAxis[0].options.labels.step = 4;
                                        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                    }
                                    else {
                                        chart.xAxis[0].options.labels.step = 6;
                                        return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                    }

                                    //Highcharts.dateFormat('%b %Y', new Date(this.value))
                                },
                                style: {
                                    fontSize: '10px'
                                },
                                padding: 2,
                                distance: 5
                            },
                            categories: []
                        },
                        yAxis: {
                            endOnTick: false,
                            allowDecimals: true,
                            title: {
                                text: 'Change'
                            }
                        },
                        tooltip: {
                            shared: true,
                            pointFormat: '{series.name} produced <b>{point.y} </b><br/>warheads in {point.x}',
                            formatter: function () {
                                var s = [];
                                $.each(this.points, function (i, point) {
                                    var seriesName = point.series.name;
                                    var y = point.y;
                                    var x = point.x;
                                    var fundDetails = $.grep(fundChartObject, function (obj) { return obj.fundName == seriesName })[0];
                                    var labelIndex = jQuery.inArray(($.grep(fundDetails.Labels, function (obj) {
                                        return obj === x;
                                    })[0]), fundDetails.Labels);
                                    var navPrice = fundDetails.NavPrices[labelIndex];
                                    var navDate = fundDetails.NavDates[labelIndex];
                                    s.push('<span style="color:' + point.color + '">' + seriesName + ' </span><br/><span style="color:' + point.color + '">Price: ' + navPrice + ' (Change: ' + y + ') on ' +
                                        navDate + ' (Day: ' + x + ')</span>');
                                    //Highcharts.dateFormat('%b %Y', x)
                                });


                                return s.join('<br />');
                            },
                            followPointer: true,
                            //positioner: { x: 100, y: 100 },
                            crosshairs: [true, true],
                            crosshairs: {
                                color: 'rgba(0,0,0,0.3)',
                                dashStyle: 'Solid',
                                width: 2
                            },
                        },
                        credits: {
                            href: '/Index.aspx',
                            text: 'eapexis.apexis.com.my',
                            position: {
                                align: 'right',
                                verticalAlign: 'bottom',
                            }
                        },
                        plotOptions: {
                            line: {
                                marker: {
                                    enabled: false,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        },
                                    }
                                }
                            }
                        },
                        series: [
                            //{
                            //    name: 'Nav Price',
                            //    data: data11
                            //},
                            //{
                            //    name: 'Nav',
                            //    data: [null, null, null, null, null, null, null, null, null, null,
                            //        5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
                            //        4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
                            //        15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
                            //        33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
                            //        35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                            //        21000, 20000, 19000, 18000, 18000, 17000, 16000]
                            //}
                        ],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal'
                                    },
                                    yAxis: {
                                        labels: {
                                            align: 'left',
                                            x: 0,
                                            y: -5
                                        },
                                        title: {
                                            text: null
                                        }
                                    },
                                    subtitle: {
                                        text: null
                                    },
                                    credits: {
                                        enabled: false
                                    }
                                }
                            }],
                        }
                    });
                    Highcharts.setOptions({
                        lang: {
                            resetZoom: 'Reset zoom', resetZoomTitle: 'Reset zoom level 1:1'
                        }
                    });
                    scriptsApplied = 1;
                    $('[data-value="6"]').click();
                }
            }
            $('.chart-tabs a').click(function () {
                dataValue = $(this).attr('data-value');
                PlotChart();
            });
            $('[href="#tab6"]').click(function () {
                setTimeout(ApplyScripts, 500);
            });

            const beforeChartPrint = function () {
                chart.oldChartSize = chart.currentChartSize;
                chart.resetChartParams = [chart.chartWidth, chart.chartHeight, false];
                chart.setSize(800, 450, false);
            };

            const afterChartPrint = function () {
                chart.setSize.apply(chart, chart.resetChartParams);
                chart.currentChartSize = chart.oldChartSize;
                chart.reflow();
            };

            let testPrintAction = function (mql) {
                if (mql.matches) {
                    //beforeChartPrint();
                } else {
                    afterChartPrint();
                }
            };

            const mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(testPrintAction);

        });
    </script>
</asp:Content>
