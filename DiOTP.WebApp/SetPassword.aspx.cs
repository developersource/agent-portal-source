﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class SetPassword : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        public string key = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                key = Request.QueryString["key"];
                string propName = nameof(Utility.User.UniqueKey);
                Response responseUList = IUserService.GetDataByPropertyName(propName, key, true, 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUList.Data;
                    if (users.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Broken link', 'close-tab');", true);
                        Session.Clear();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnSetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                var hasUpperChar = txtPassword.Text.Any(char.IsUpper);
                var hasLowerChar = txtPassword.Text.Any(char.IsLower);
                
                var rule2 = txtPassword.Text.Any(char.IsNumber);
                int rule3;
                if (txtPassword.Text.Length >= 8 && txtPassword.Text.Length <= 16)
                    rule3 = 1;
                else
                    rule3 = 0;


                string newPassword = txtPassword.Text;
                string confirmPassword = txtConfirmPassword.Text;
                string propName = nameof(Utility.User.UniqueKey);
                string message = "";
                bool isValid = true;

                Response responseUList = IUserService.GetDataByPropertyName(propName, key, true, 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    User user = ((List<User>)responseUList.Data).FirstOrDefault();
                    if (newPassword == "" || confirmPassword == "")
                    {
                        message += "Please enter your password.<br/>";
                        isValid = false;
                    }
                    if (newPassword != confirmPassword)
                    {
                        message += "New Password and Confirm New Password must be the same.<br/>";
                        isValid = false;
                    }
                    if (hasUpperChar == false)
                    {
                        message += "New Password must contain at least one uppercase alphabet letter.<br/>";
                        isValid = false;
                    }
                    if (hasLowerChar == false)
                    {
                        message += "New Password must contain at least one lowercase alphabet letter.<br/>";
                        isValid = false;
                    }
                    if (rule2 == false)
                    {
                        message += "New Password must contain at least one number.<br/>";
                        isValid = false;
                    }
                    if (rule3 == 0)
                    {
                        message += "New Password length must between 8 to 16 characters.<br/>";
                        isValid = false;
                    }
                    if (CustomEncryptorDecryptor.EncryptPassword(newPassword) == user.Password)
                    {
                        message += "This password has been used before, please choose another password in order to proceed.<br/>";
                        isValid = false;
                    }

                    if (isValid == true)
                    {
                        string pass = user.Password;
                        user.Password = CustomEncryptorDecryptor.EncryptPassword(newPassword);
                        if (pass == "" || pass == user.TransPwd)
                        {
                            user.TransPwd = user.Password;
                        }
                        if (user.IsActive == 0)
                            user.IsActive = 0;
                        //user.IsActive = 1;
                        user.Status = 1;
                        user.ModifiedDate = DateTime.Now;
                        user.ModifiedBy = user.Id;
                        user.LoginAttempts = 0;
                        user.IsLoginLocked = 0;
                        user.EmailCode = "";
                        IUserService.UpdateData(user);
                        Response responseUTList = IUserTypeService.GetDataByPropertyName(nameof(UserType.UserId), user.Id.ToString(), true, 0, 0, true);
                        if (responseUTList.IsSuccess)
                        {
                            UserType userType = ((List<UserType>)responseUTList.Data).FirstOrDefault();
                            if (userType != null)
                            {
                                userType.IsVerified = 1;
                                IUserTypeService.UpdateData(userType);
                            }
                            
                            if (pass == "")
                            {
                                UserLogMain ulm = new UserLogMain()
                                {
                                    Description = "Set password",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    RefId = user.Id,
                                    RefValue = user.Username,
                                    StatusType = 1
                                };
                                Response responseLog = IUserLogMainService.PostData(ulm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                }
                            }
                            else
                            {
                                UserLogMain ulm = new UserLogMain()
                                {
                                    Description = "Reset password",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = user.Id,
                                    UserAccountId = 0,
                                    RefId = user.Id,
                                    RefValue = user.Username,
                                    StatusType = 1
                                };
                                Response responseLog = IUserLogMainService.PostData(ulm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                }
                            }

                            Response.Redirect("Index.aspx?q=SetPassword", false);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUTList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert',  \"" + message + "\" , '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setpassword Page btnSetPassword_Click: " + ex.Message);
            }
        }
    }
}