using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UtmcCompositionalTransactionService : IUtmcCompositionalTransactionService
    {
        private static readonly Lazy<IUtmcCompositionalTransactionRepo> lazy = new Lazy<IUtmcCompositionalTransactionRepo>(() => new UtmcCompositionalTransactionRepo());
        public static IUtmcCompositionalTransactionRepo IUtmcCompositionalTransactionRepo { get { return lazy.Value; } }
        public Response PostData(UtmcCompositionalTransaction obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcCompositionalTransactionRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcCompositionalTransaction> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcCompositionalTransactionRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcCompositionalTransaction obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcCompositionalTransactionRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcCompositionalTransaction> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcCompositionalTransactionRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcCompositionalTransaction DeleteData(Int32 Id)
        {
            UtmcCompositionalTransaction obj = new UtmcCompositionalTransaction();
            try
            {
                obj = IUtmcCompositionalTransactionRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcCompositionalTransactionRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcCompositionalTransaction obj = new UtmcCompositionalTransaction();
            try
            {
                response = IUtmcCompositionalTransactionRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            try
            {
                response = IUtmcCompositionalTransactionRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUtmcCompositionalTransactionRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            try
            {
                response = IUtmcCompositionalTransactionRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcCompositionalTransactionRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            try
            {
                response = IUtmcCompositionalTransactionRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcCompositionalTransactionRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
