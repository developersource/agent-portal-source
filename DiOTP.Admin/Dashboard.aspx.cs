﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderObj.Value; } }


        private static readonly Lazy<IMaHolderRegService> lazyIMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyIMaHolderRegServiceObj.Value; } }


        private static readonly Lazy<IMaHolderBankService> lazyIMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyIMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IAccountOpeningService> lazyObjAO = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());

        public static IAccountOpeningService IAccountOpeningService { get { return lazyObjAO.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                Response responseCheckConn = ServicesManager.CheckConnection();
                if (responseCheckConn.IsSuccess)
                {
                    if (!responseCheckConn.IsApiAvailable)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('API Server down.');", true);
                        Logger.WriteLog("Dashboard Page_Load responseCheckConn !IsApiAvailable: " + responseCheckConn.Message);
                    }
                    else if (!responseCheckConn.IsDBAvailable)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('API DB Connection failed.');", true);
                        Logger.WriteLog("Dashboard Page_Load responseCheckConn !IsDBAvailable: " + responseCheckConn.Message);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Connection check failed.');", true);
                    Logger.WriteLog("Dashboard Page_Load responseCheckConn !IsSuccess: " + responseCheckConn.Message);
                }
                if (!IsPostBack)
                {
                    string AsOfDate = "Data as on " + DateTime.Now.ToString("dd/MM/yyyy");
                    AsOfDate1.InnerHtml = AsOfDate;

                    Int32 addrCount = IMaHolderRegService.GetCountByFilter(" OTP_ACT_ST in ('0')  ");
                    addrVerCount.InnerHtml = addrCount + "";
                    Int32 bankCount = IMaHolderBankService.GetCountByFilter(" status = 0 ");
                    bankVerCount.InnerHtml = bankCount + "";
                    Int32 payPendingCount = IUserOrderService.GetCountByFilter(" status = 1 and order_status = 22 ");
                    paymentPendingCount.InnerHtml = payPendingCount + "";
                    String ordPendingCount = GenericService.GetCountByQuery(@"select count(distinct uo.order_no) as count from user_orders uo
                            join user_accounts ua on ua.id = uo.user_account_id
                            where  uo.status = 1 and uo.order_status = 2 and uo.order_type in (1, 2, 3, 4)").Data.ToString();
                    orderPendingCount.InnerHtml = ordPendingCount + "";
                    Int32 rspPendingCountDB = IUserOrderService.GetCountByFilter(" status = 1 and order_status = 2 and order_type in (6) ");
                    rspPendingCount.InnerHtml = rspPendingCountDB + "";
                    Int32 accountOpeningCountDB = IAccountOpeningService.GetCountByFilter(" status = 1 and process_status in (2, 10, 11, 20, 21, 3)");
                    AccountOpeningCount.InnerHtml = accountOpeningCountDB + "";
                    Int32 HardcopyRequestsCountDB = IUserService.GetCountByFilter(" status = 1 and user_role_id = 3 and is_hard_copy = 1 ");
                    Int32 SoftcopyRequestsCountDB = IUserService.GetCountByFilter(" status = 1 and user_role_id = 3 and is_hard_copy = 0 ");
                    newHardcopyRequestsCount.InnerHtml = HardcopyRequestsCountDB + " vs " + SoftcopyRequestsCountDB;


                    string accountfilter = " status=1 and user_account_id in (select ID from user_accounts) and ma_holder_bank_id in (select ID from ma_holder_bank)";
                    var banks = GenericService.GetCountByFilter<UserAccountBanks>(accountfilter, false, null);
                    Int32 bankBindingCount = (Int32)banks.Data;


                    newMABankBindingList.InnerHtml = bankBindingCount + "";

                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMACountStats()
        {
            Response responseCount = GenericService.GetCountByQuery(" select count(*) from (SELECT count(*) FROM user_accounts where is_verified=1 and status=1 group by account_no) a; ");

            Int32 masCount = 0;
            if (responseCount.IsSuccess)
            {
                masCount = (Int32)responseCount.Data;
            }
            String oracleMAsCount = "";
            Response responseOracleMACount = ServicesManager.GetActiveMAsCount();
            if (responseOracleMACount.IsSuccess)
            {
                oracleMAsCount = responseOracleMACount.Data.ToString();
            }

            string MaCountDiOTP = masCount.ToString();
            string MaCountOracle = oracleMAsCount;

            return new { MaCountDiOTP = MaCountDiOTP, MaCountOracle = MaCountOracle };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMAAssetStats()
        {
            string MaAssetOracle = "0";
            string MaAssetDiOTP = "0";
            try
            {
                Response responseOracleMAAsset = ServicesManager.GetAssetValue("");
                if (responseOracleMAAsset.IsSuccess)
                {
                    List<AssetStats> assetStats = (List<AssetStats>)responseOracleMAAsset.Data;
                    if (assetStats.Count > 0)
                        MaAssetOracle = assetStats.First().ASSETVALUE;
                }

                Response responseActiveMAs = IUserAccountService.GetDataByFilter(" status=1 and is_verified=1 ", 0, 0, false);
                if (responseActiveMAs.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseActiveMAs.Data;
                    if (userAccounts.Count > 0)
                    {
                        string holderNos = String.Join(",", userAccounts.Select(x => x.AccountNo).ToList());
                        Response responseDiOTPMAAsset = ServicesManager.GetAssetValue(holderNos);
                        if (responseDiOTPMAAsset.IsSuccess)
                        {
                            List<AssetStats> assetStats = (List<AssetStats>)responseDiOTPMAAsset.Data;
                            if (assetStats.Count > 0)
                                MaAssetDiOTP = assetStats.First().ASSETVALUE;
                        }
                    }
                    else
                    {
                        MaAssetDiOTP = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                return new { IsSuccess = false, Message = ex.Message };
            }
            return new { IsSuccess = true, MaAssetDiOTP = Math.Round(Convert.ToDecimal(MaAssetDiOTP), 0), MaAssetOracle = Math.Round(Convert.ToDecimal(MaAssetOracle), 0) };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMACountSeries(string PlotType)
        {
            if (PlotType == "MTD")
            {
                Response responseOracleMACountYTDLastYEar = ServicesManager.GetRegisteredMAsCountByMTDLastYear();
                if (responseOracleMACountYTDLastYEar.IsSuccess)
                {
                    List<OracleMACountStatsByYear> oracleStatsByYears = (List<OracleMACountStatsByYear>)responseOracleMACountYTDLastYEar.Data;

                    return oracleStatsByYears;
                }
            }

            return null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMAAssetSeries(string PlotType)
        {
            if (PlotType == "MTD")
            {
                Response responseOracleMACountYTDLastYEar = ServicesManager.GetRegisteredMAsAssetByMTDLastYear();
                if (responseOracleMACountYTDLastYEar.IsSuccess)
                {
                    List<OracleMAAssetStatsByYear> oracleStatsByYears = (List<OracleMAAssetStatsByYear>)responseOracleMACountYTDLastYEar.Data;

                    return oracleStatsByYears;
                }
            }

            return null;
        }

    }
}