﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_rank_configurations")]
    public class AgentRankConfiguration
    {
        public Int32 id { get; set; }
        public String rank_id { get; set; }
        public Int32 is_remisiery_allowed { get; set; }
        public Int32 rank_type { get; set; }
        public Int32 rank_level { get; set; }
        public String rank_name { get; set; }
        public String Mnemonic { get; set; }
        public Decimal min_agent_comm { get; set; }
        public Int32 allow_personal_sales_comm { get; set; }
        public Int32 allow_trailer_comm { get; set; }
        public Int32 allow_direct_comm_adj { get; set; }
        public Int32 alloe_trailer_comm_adj { get; set; }
        public Int32 exit_fee_allow { get; set; }
        public Int32 allow_change_upline { get; set; }
        public Int32 allow_direct_overriding_comm { get; set; }
        public Int32 allow_trailer_overriding_comm { get; set; }
        public Int32 allow_change_of_reporting_centre { get; set; }
        public Int32 allow_agent_insurance { get; set; }
        public Int32 allow_promo_demo { get; set; }
        public Int32 allow_grace_period { get; set; }
        public Int32 allow_office_subsidy { get; set; }
        public Int32 allow_performance_incentive { get; set; }
        public Int32 allow_equalisation { get; set; }
        public Int32 allow_overriding_own_sales { get; set; }
        public Int32 allow_overriding_trailer_own_sales { get; set; }
        public Int32 allow_recruitment_authority { get; set; }
        public Int32 allow_switching_comm { get; set; }
        public Int32 allow_switching_overriding_comm { get; set; }
        public Int32 is_active { get; set; }
        public DateTime created_date { get; set; }
        public Int32 status { get; set; }
    }
}
