﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.IO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;
using System.Configuration;
using Admin.FPXLibary;
using System.Collections.Specialized;
using System.Net.Http;

namespace Admin
{
    public partial class Payments : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        private static readonly Lazy<IPaymentDetailService> lazyPaymentDetailServiceObj = new Lazy<IPaymentDetailService>(() => new PaymentDetailService());

        public static IPaymentDetailService IPaymentDetailService { get { return lazyPaymentDetailServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"select uo.order_status, uo.ID, uo.order_type, uo.user_id, u.username, uo.user_account_id, ua.account_no, uo.order_no, uo.payment_date, uo.amount, uo.units, uo.payment_method, uo.created_date, uo.updated_date, uo.reject_reason, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits, uo.fpx_bank_code from ");
            string mainQCount = (@"select count(*) from ( ");
            filter.Append(@" user_orders uo
                            join users u on u.id = uo.user_id
                            join user_accounts ua on ua.id = uo.user_account_id where 1=1 ");

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["pageLength"]))
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["pageLength"]))
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate.Value = Request.QueryString["FromDate"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ToDate"]))
                {
                    ToDate.Value = Request.QueryString["ToDate"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    if (Search.Value.ToLower() == "buy")
                    {
                        filter.Append(" and uo.order_type = 1 ");
                    }
                    else if (Search.Value.ToLower() == "rsp")
                    {
                        filter.Append(" and uo.order_type = '6' ");
                    }
                    else
                    {
                        filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                        filter.Append(" or u.id_no like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.order_no like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.fpx_bank_code like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.reject_reason like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or ua.account_no like '%" + Search.Value.Trim() + "%') ");
                    }



                }

                if (IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                        filter.Append(" and uo.order_type IN ('1', '6') and uo.order_status in ('" + FilterValue.Value + "'" + (FilterValue.Value == "2" ? ",'3','39'" : "") + ") ");
                    else
                        filter.Append(" and uo.order_type IN ('1', '6') and uo.order_status IN('2', '22', '29', '3', '39') ");
                }
                else
                {
                    filter.Append(" and uo.order_type IN ('1', '6') and uo.order_status IN ('22') ");
                }
                if (!string.IsNullOrEmpty(FromDate.Value) && !string.IsNullOrEmpty(ToDate.Value))
                {
                    DateTime from = DateTime.ParseExact(FromDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime to = DateTime.ParseExact(ToDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (FromDate.Value.Trim() == ToDate.Value.Trim())
                    {
                        to = to.AddDays(1);
                    }
                    else if (from > to)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                "alert('From date cannot be greater than To date.');", true);
                    }
                    filter.Append(" and uo.created_date between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                }
                filter.Append(" group by uo.order_no ");
                filter.Append(" order by FIELD(uo.order_status, '22', '2', '29'), uo.created_date desc ");
                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") countIn ").Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseUOList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseUOList.IsSuccess)
                {
                    var uoDyn = responseUOList.Data;
                    var responseJSON = JsonConvert.SerializeObject(uoDyn);
                    List<UserOrderCustom> userOrders = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserOrderCustom>>(responseJSON);

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    userOrders.ForEach(u =>
                    {
                        string checkBox = string.Empty;

                        DateTime dateTime = u.CreatedDate;
                        if (u.OrderStatus == (int)Order_Status.Order_Approved || u.OrderStatus == (int)Order_Status.Order_Rejected || u.OrderStatus == (int)Order_Status.Payment_Rejected || u.OrderStatus == (int)Order_Status.Order_Canceled || u.OrderStatus == (int)Order_Status.Order_Verified)
                        {
                            checkBox = @"<label>" + index + @"</label><br/>";
                        }
                        else
                        {
                            checkBox = @"<div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type = 'checkbox' name='checkRow' class='checkRow' value='" + u.Id + @"' /> <label>" + index + @"</label><br/>
                                                    </div>
                                                </div>";
                        }

                        string url = "";
                        Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), u.OrderNo, true, 0, 0, true);
                        if (responseUOFList.IsSuccess)
                        {
                            UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                            if (uof != null)
                                url = uof.Url;
                        }
                        Type t = GetType("FPXBank");
                        string BankName = u.FpxBankCode;
                        FPXBank b = FPXEnumeration.GetByBankId(t, BankName);
                        asb.Append(@"<tr>
                                            <td class='icheck'>
                                                " + checkBox + @"
                                                <span class='row-status'>
                                                    " + (u.OrderStatus == (int)Order_Status.Payment_Pending ? "<span class='label label-warning'>PP</span>" :
                                     u.OrderStatus == (int)Order_Status.Order_Verified ? "<span class='label label-success'>PS</span>" :
                                     u.OrderStatus == (int)Order_Status.Payment_Rejected ? "<span class='label label-danger'>PR</span>" :
                                     u.OrderStatus == (int)Order_Status.Order_Placed ? ((u.OrderType == (int)OrderType.Sell || u.OrderType == (int)OrderType.SwitchIn || u.OrderType == (int)OrderType.SwitchOut) ? "<span class='label label-warning'>OP</span>" : "") : "")
                                  + @"
                                                </span>
                                            </td>
                                            <td>" + dateTime.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                            <td>" + (u.OrderType == 1 ? "Buy" : u.OrderType == 6 ? "RSP" : "") + @"</td>
                                            <td>" + u.Username + @"</td>
                                            <td>" + u.AccountNo + @"</td>
                                            <td><a href='javascript:;' data-id='" + u.OrderNo + @"' data-original-title='View Detail' data-trigger='hover' data-placement='bottom' class='popovers action-button text-info' data-url='" + (u.OrderType == (int)OrderType.RSP ? "ViewDetailRSP.aspx" : "ViewTransactionDetail.aspx") + "' data-title='Transaction Detail - " + u.OrderNo + @"' data-action='View'>" + u.OrderNo + @"</a></td>
                                            <td class='text-right'>" + (u.OrderType == (int)OrderType.Buy || u.OrderType == (int)OrderType.RSP ? u.SumAmount.ToString("N2") : "") + @"</td>
                                            <td>" + (u.PaymentMethod == "1" ? "FPX PAYMENT (" + (b == null ? BankName : b.DisplayName) + @")" : (u.PaymentMethod == "2" ? "<a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text-info' href=\"" + url + "\" target='_blank'><i class='fa fa-eye'></i></a>" : "")) + @"</td>               
                                            <td>" + (u.PaymentDate.HasValue && (u.OrderStatus == 2 || u.OrderStatus == 29) ? u.PaymentDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</td>
                                            <td>" + (u.RejectReason == null ? "" : u.RejectReason) + @"</td>
                                            </tr>
                
                                            ");
                        index++;
                    });
                    transactionsTbody.InnerHtml = asb.ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                string idString = String.Join(",", ids);
                Response responseUOList = IUserOrderService.GetDataByFilter(" ID in (" + idString + ") group by order_no ", 0, 0, false);
                if (responseUOList.IsSuccess)
                {
                    List<UserOrder> order = (List<UserOrder>)responseUOList.Data;
                    List<UserOrder> multipleOrders = new List<UserOrder>();
                    List<UserOrder> sent = new List<UserOrder>();

                    order.ForEach(x =>
                    {
                        Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                        if (response.IsSuccess)
                        {
                            List<UserOrder> ol = (List<UserOrder>)response.Data;
                            if (ol.Count > 0)
                            {
                                multipleOrders.AddRange(ol);
                            }
                        }
                    });

                    if (action == "Approve")
                    {
                        multipleOrders.ForEach(x =>
                        {
                            if (x.OrderStatus == 22)
                            {
                                x.UpdatedDate = DateTime.Now;
                                x.OrderStatus = 2;
                                x.Status = 1;
                                x.FpxTransactionId = rejectReason;
                                x.PaymentDate = DateTime.Now;
                            }
                        });

                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 2)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been approved");
                            }
                            else if (y.OrderStatus == 29)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is approved");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Payment for Order number " + y.OrderNo + " succesfully approved",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,

                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "22",
                                    ValueNew = "2",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }
                    if (action == "Reject")
                    {
                        multipleOrders.ForEach(x =>
                        {
                            if (x.OrderStatus == 22)
                            {
                                x.UpdatedDate = DateTime.Now;
                                x.RejectReason = rejectReason;
                                x.OrderStatus = 29;
                                x.Status = 1;
                                x.PaymentDate = DateTime.Now;
                            }
                        });

                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 2)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been approved");
                            }
                            else if (y.OrderStatus == 29)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is rejected");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Order number " + y.OrderNo + " has been rejected",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,

                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "22",
                                    ValueNew = "29",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }
                    if (action == "AE")
                    {
                        order.ForEach(x =>
                        {
                            if (x.OrderStatus == 22)
                            {
                                Response response = IUserService.GetSingle(x.UserId);
                                if (response.IsSuccess)
                                {
                                    User user = (User)response.Data;
                                    Response responsePaymentInfo = GenericService.GetDataByQuery(" select * from payment_details where transaction_id = '" + x.FpxTransactionId + "' ", 0, 0, false, null, false, null, false);
                                    if (responsePaymentInfo.IsSuccess)
                                    {
                                        var paymentsDyn = responsePaymentInfo.Data;
                                        var responseJSON = JsonConvert.SerializeObject(paymentsDyn);
                                        List<PaymentDetail> PaymentDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentDetail>>(responseJSON);
                                        if (PaymentDetails != null && PaymentDetails.Count > 0)
                                        {
                                            PaymentDetail paymentDetail = PaymentDetails.FirstOrDefault();
                                            string FPXPostRequestUrl = ConfigurationManager.AppSettings["FPXPostRequestUrl"].ToString();

                                            string str = "<html>";
                                            str += "<head>";
                                            str += "<title>SAMPLE FPX MERCHANT PAGE - Your One Stop Online Computer Shopping</title>";
                                            str += "<script type='text/JavaScript' language='JavaScript'>";
                                            str += "<" + (char)47 + "script>";
                                            str += "<link rel='stylesheet' type='text/css' href='files/style.css'>";
                                            str += "</head>";
                                            str += "<body bgcolor='#C0E7FE'>";
                                            str += "<form method='post' action='01_fpx_AEReq_main.aspx'>";
                                            str += "<center>";
                                            str += "<table border='0' cellpadding='0' cellspacing='0' height='300' width='722' align='center'>";
                                            str += "<tbody>";
                                            str += "<tr>";
                                            str += "<td colspan='3' align='left' height='111'><table style='background:#FDE6C4;border: 1px solid rgb(222, 217, 197);' cellpadding='0' cellspacing='0' height='111' width='722' >";
                                            str += "<tbody>";
                                            str += "<tr>";
                                            str += "<td align='center'><strong>SAMPLE MERCHANT RESPONSE PAGE</strong></td>";
                                            str += "</tr>";
                                            str += "</tbody>";
                                            str += "</table>";
                                            str += "</td>";
                                            str += "</tr>";
                                            str += "\n<!-- header_eof //--> \n";
                                            str += " \n<!-- body //-->";
                                            str += "<tr>";
                                            str += "<td style='padding-right: 1px;' align='right' valign='top' width='6'><br>";
                                            str += "</td>";
                                            str += "<td style='padding-left: 1px; padding-right: 1px;' align='left' valign='top' width='716' colspan=2>";
                                            str += "<table bgcolor='#FFFFFF' border='0' cellpadding='0' cellspacing='5' class='infoBelow' width='100%' height='100%'>";
                                            str += "<tbody>";
                                            str += "<tr>";
                                            str += "<td height='150' valign='top'>				";
                                            str += "<p class='normal'>Response </p>";
                                            str += "<p>&nbsp;</p>				  ";
                                            str += "<p class='normal'><b>TRANSACTION DETAILS</b></p>";
                                            str += "<table width='100%' align='center'>";

                                            string sHttpResonse = "";
                                            try
                                            {
                                                Controller c = new Controller();
                                                String checksum = "";
                                                Random RandomNumber = new Random();
                                                String fpx_msgType = "AE";
                                                String fpx_msgToken = "01";
                                                String fpx_sellerExId = "EX00008813";
                                                String fpx_sellerExOrderNo = x.OrderNo;
                                                String fpx_sellerOrderNo = x.OrderNo;
                                                String fpx_sellerTxnTime = x.TransNo;
                                                String fpx_sellerId = "SE00010202";
                                                String fpx_sellerBankCode = "01";
                                                String fpx_txnCurrency = "MYR";
                                                String fpx_txnAmount = paymentDetail.TxnAmount;
                                                String fpx_buyerEmail = paymentDetail.BuyerEmail;
                                                String fpx_buyerId = paymentDetail.BuyerId;
                                                String fpx_buyerName = paymentDetail.BuyerName;
                                                String fpx_buyerBankId = paymentDetail.BuyerBankId;
                                                String fpx_buyerBankBranch = paymentDetail.BuyerBankBranch;
                                                String fpx_buyerAccNo = paymentDetail.BuyerAccNo;
                                                String fpx_makerName = paymentDetail.MakerName;
                                                String fpx_buyerIban = paymentDetail.BuyerIban;
                                                String fpx_productDesc = "PD";
                                                String fpx_version = "6.0";
                                                String fpx_checkSum = "";
                                                fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
                                                fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
                                                fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
                                                fpx_checkSum = fpx_checkSum.Trim();

                                                checksum = c.RSASign(fpx_checkSum, HttpContext.Current.Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key name 
                                                String finalMsg = fpx_checkSum;

                                                string posting_data = "fpx_msgType=" + fpx_msgType + "&" + "fpx_msgToken=" + fpx_msgToken + "&" + "fpx_sellerExId=" + fpx_sellerExId + "&" + "fpx_sellerExOrderNo=" + fpx_sellerExOrderNo + "&" + "fpx_sellerTxnTime=" + fpx_sellerTxnTime + "&" + "fpx_sellerOrderNo=" + fpx_sellerOrderNo + "&" + "fpx_sellerBankCode=" + fpx_sellerBankCode + "&" + "fpx_txnCurrency=" + fpx_txnCurrency + "&" + "fpx_txnAmount=" + fpx_txnAmount + "&" + "fpx_buyerEmail=" + fpx_buyerEmail + "&" + "fpx_checkSum=" + fpx_checkSum + "&" + "fpx_buyerName=" + fpx_buyerName + "&" + "fpx_buyerBankId=" + fpx_buyerBankId + "&" + "fpx_buyerBankBranch=" + fpx_buyerBankBranch + "&" + "fpx_buyerAccNo=" + fpx_buyerAccNo + "&" + "fpx_buyerId=" + fpx_buyerId + "&" + "fpx_makerName=" + fpx_makerName + "&" + "fpx_buyerIban=" + fpx_buyerIban + "&" + "fpx_productDesc=" + fpx_productDesc + "&" + "fpx_version=" + fpx_version + "&" + "fpx_sellerId=" + fpx_sellerId + "&" + "checkSum_String=" + checksum;

                                                using (var client = new HttpClient())
                                                {
                                                    string baseURL = FPXPostRequestUrl;
                                                    client.BaseAddress = new Uri(baseURL);
                                                    client.DefaultRequestHeaders.Accept.Clear();
                                                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                                                    HttpContent content = new StringContent(posting_data, Encoding.UTF8, "application/json");
                                                    var responseH = client.PostAsJsonAsync("", posting_data).Result;
                                                    string responseString = "";
                                                    if (responseH.IsSuccessStatusCode)
                                                    {
                                                        responseString = responseH.Content.ReadAsStringAsync().Result;
                                                        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                                                        //maHolderReg = Newtonsoft.Json.JsonConvert.DeserializeObject<MaHolderReg>(responseString);
                                                    }
                                                }


                                                var resps = new Dictionary<string, string>();
                                                Boolean verifyMsg = false;
                                                String fpx_checkSumString = "";
                                                String finalVerifiMsg = "ERROR";
                                                System.Net.ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                                                string url = FPXPostRequestUrl;   //Here "URL" is my action method to receive HttpWebRequest
                                                System.Net.HttpWebRequest objRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                                                objRequest.Method = "POST";
                                                objRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                                                objRequest.Accept = "application/json";
                                                objRequest.AllowAutoRedirect = true;
                                                objRequest.KeepAlive = false;
                                                objRequest.Timeout = 300000;
                                                byte[] _byteVersion = Encoding.ASCII.GetBytes(string.Concat("content=", posting_data));
                                                objRequest.ContentLength = _byteVersion.Length;
                                                Stream stream = objRequest.GetRequestStream();
                                                stream.Write(_byteVersion, 0, _byteVersion.Length);
                                                stream.Close();
                                                String resp = "";
                                                sHttpResonse = "";
                                                NameValueCollection qscoll;
                                                using (System.Net.HttpWebResponse objResponse = (System.Net.HttpWebResponse)objRequest.GetResponse())
                                                using (StreamReader reader = new StreamReader(objResponse.GetResponseStream()))
                                                {
                                                    resp = ((reader.ReadToEnd()).Trim());
                                                    Console.WriteLine(resp);
                                                }
                                                sHttpResonse = resp;

                                                if (resp.Equals("ERROR") || resp.Contains("PROSESSING ERROR"))
                                                {
                                                    str += "<tr> ";
                                                    str += "<td width='20%' class='normal'>Response</td>";
                                                    str += "<td class='normal'> ";
                                                    str += "<div align='center'>:</div>";
                                                    str += "</td>";
                                                    str += "<td width='80%' class='normal'> ";
                                                    // str += resp + " [ERROR]";
                                                    str += "[ERROR]";
                                                    str += "</td>";
                                                    str += "</tr>";
                                                }
                                                else if ((fpx_msgToken).CompareTo("03") == 0)
                                                {
                                                    qscoll = System.Web.HttpUtility.ParseQueryString(resp);
                                                    str += "<table width='50%' >";
                                                    foreach (String key in qscoll.AllKeys)
                                                    {
                                                        str += "<tr class='normal'>";
                                                        str += "<td width='45%' class='normal' align='left' class='main'>" + key + "</td>";
                                                        str += "<td width='10%' class='normal' align='center' class='main'>:</td>";
                                                        if (key.CompareTo("fpx_checkSum") == 0)
                                                            str += "<td class='normal' width='45%' align='left' class='main'><strong><textarea rows=3 cols=100>" + qscoll[key] + "</textarea></td>";
                                                        else
                                                            str += "<td class='normal' width='45%' align='left' class='main'>" + qscoll[key] + "</td>";
                                                        str += "</tr>";
                                                        resps.Add(key, qscoll[key]);
                                                    }
                                                    str += "</table>";
                                                    str += "</td>";
                                                    str += "</tr>";
                                                    str += "</table>";
                                                    str += "</form>";
                                                    str += "<A HREF='01_fpx_b2b2send_main.aspx'> << Try again</A>";
                                                    str += "<br><br><input type='hidden' size=400 name='request'  value='" + posting_data + "'><br>";
                                                    str += "        <input type='hidden' size=400 name='response' value='" + sHttpResonse + "'>";
                                                    str += "</body>";
                                                    str += "</html>";
                                                    response1.Data = str;
                                                    verifyMsg = true;

                                                }
                                                else
                                                {
                                                    qscoll = System.Web.HttpUtility.ParseQueryString(resp);
                                                    int fgs = 0;
                                                    foreach (String key in qscoll.AllKeys)
                                                    {
                                                        resps.Add(key, qscoll[key]);
                                                        if ((key.CompareTo("fpx_debitAuthCode") == 0) && (qscoll[key].CompareTo("00") == 0))
                                                        {
                                                            fgs = 1;
                                                        }
                                                        if (fgs == 1)
                                                        {
                                                            if ((key.CompareTo("fpx_debitAuthCode") == 0) && (qscoll[key].CompareTo("00") == 0))
                                                            {
                                                                str += "<tr>";
                                                                str += "<td width='44%' align='left' class='main'>Transaction Status </td>";
                                                                str += "<td width='7%'  align='center' class='main'>:</td>";
                                                                str += "<td width='49%' align='left' class='main'><B>SUCCESSFULL</B></td> </tr>";
                                                            }
                                                            if ((key.CompareTo("fpx_fpxTxnId") == 0))
                                                            {
                                                                str += "<tr>";
                                                                str += "<td width='44%' align='left' class='main'>Transaction Id </td>";
                                                                str += "<td width='7%'  align='center' class='main'>:</td>";
                                                                str += "<td width='49%' align='left' class='main'>" + qscoll[key] + "</td> </tr>";
                                                            }
                                                            if ((key.CompareTo("fpx_sellerExOrderNo") == 0))
                                                            {
                                                                str += "<tr>";
                                                                str += "<td width='44%' align='left' class='main'>Seller Order No.</td>";
                                                                str += "<td width='7%'  align='center' class='main'>:</td>";
                                                                str += "<td width='49%' align='left' class='main'>" + qscoll[key] + "</td> </tr>";
                                                            }
                                                            if ((key.CompareTo("fpx_buyerBankId") == 0))
                                                            {
                                                                str += "<tr>";
                                                                str += "<td width='44%' align='left' class='main'>Buyer Bank</td>";
                                                                str += "<td width='7%'  align='center' class='main'>:</td>";
                                                                str += "<td width='49%' align='left' class='main'>" + qscoll[key] + "</td> </tr>";
                                                            }
                                                            if ((key.CompareTo("fpx_txnAmount") == 0))
                                                            {
                                                                str += "<tr>";
                                                                str += "<td width='44%' align='left' class='main'>Transaction Amount </td>";
                                                                str += "<td width='7%'  align='center' class='main'>:</td>";
                                                                str += "<td width='49%' align='left' class='main'>" + qscoll[key] + "</td> </tr>";
                                                            }


                                                        }

                                                        str += "<tr style='display:none'>";
                                                        str += "<td width='44%' align='left' class='main'>" + key + "</td>";
                                                        str += "<td width='7%'  align='center' class='main'>:</td>";
                                                        if (qscoll[key].Length > 500)
                                                        {
                                                            str += "<td width='49%' align='left' class='main'><textarea>" + qscoll[key] + "</textarea></td> </tr>";
                                                        }
                                                        else
                                                        {
                                                            str += "<td width='49%' align='left' class='main'>" + qscoll[key] + "</td> </tr>";
                                                        }
                                                    }
                                                    verifyMsg = true;
                                                }




                                                fpx_checkSum = "";
                                                if (verifyMsg)
                                                {
                                                    fpx_buyerBankBranch = resps["fpx_buyerBankBranch"];
                                                    fpx_buyerBankId = resps["fpx_buyerBankId"];
                                                    fpx_buyerIban = resps["fpx_buyerIban"];
                                                    fpx_buyerId = resps["fpx_buyerId"];
                                                    fpx_buyerName = resps["fpx_buyerName"];
                                                    String fpx_creditAuthCode = resps["fpx_creditAuthCode"];
                                                    String fpx_creditAuthNo = resps["fpx_creditAuthNo"];
                                                    String fpx_debitAuthCode = resps["fpx_debitAuthCode"];
                                                    String fpx_debitAuthNo = resps["fpx_debitAuthNo"];
                                                    String fpx_fpxTxnId = resps["fpx_fpxTxnId"];
                                                    String fpx_fpxTxnTime = resps["fpx_fpxTxnTime"];
                                                    fpx_makerName = resps["fpx_makerName"];
                                                    fpx_msgToken = resps["fpx_msgToken"];
                                                    fpx_msgType = resps["fpx_msgType"];
                                                    fpx_sellerExId = resps["fpx_sellerExId"];
                                                    fpx_sellerExOrderNo = resps["fpx_sellerExOrderNo"];
                                                    fpx_sellerId = resps["fpx_sellerId"];
                                                    fpx_sellerOrderNo = resps["fpx_sellerOrderNo"];
                                                    fpx_sellerTxnTime = resps["fpx_sellerTxnTime"];
                                                    fpx_txnAmount = resps["fpx_txnAmount"];
                                                    fpx_txnCurrency = resps["fpx_txnCurrency"];
                                                    fpx_checkSum = resps["fpx_checkSum"];

                                                    fpx_checkSumString = fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|" + fpx_creditAuthCode + "|" + fpx_creditAuthNo + "|" + fpx_debitAuthCode + "|" + fpx_debitAuthNo + "|" + fpx_fpxTxnId + "|" + fpx_fpxTxnTime + "|" + fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|";
                                                    fpx_checkSumString += fpx_sellerExId + "|" + fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency;
                                                    finalVerifiMsg = c.nvl_VerifiMsg(fpx_checkSumString, fpx_checkSum, HttpContext.Current.Request.PhysicalApplicationPath); //Certificate Path



                                                }
                                                else
                                                    finalVerifiMsg = "Message cannot be Verified - Response Parsing Error ";

                                                str += "<tr>";
                                                str += "<td  width='100%' colspan='3' class='main'><BR><center>" + finalVerifiMsg + "</td>";
                                                str += "</tr>";

                                                response1.IsSuccess = true;
                                            }
                                            catch (Exception ee)
                                            {

                                                str += "<tr><td width='100%' colspan='3'  align='center' class='main'><BR><color='red'>" + ee.Message + "</color></td> </tr>";
                                                //str += "<tr><td width='44%' align='left' class='main'> Invalid Response </td>";
                                                //str += "<td width='7%'  align='center' class='main'>:</td>";
                                                //str += "<td width='49%' align='left' class='main'>" + ee.Message  + "</td> </tr>";
                                            }
                                            response1.Data = str;
                                        }
                                        else
                                        {
                                            response1.IsSuccess = false;
                                            responseMsgs.Add("[" + x.OrderNo + "] Payment Details Not Found");
                                        }
                                    }
                                    else
                                    {
                                        response1.IsSuccess = false;
                                        responseMsgs.Add("[" + x.OrderNo + "] Payment Details : " + responsePaymentInfo.Message);
                                    }
                                }
                                else
                                {
                                    response1.IsSuccess = false;
                                    responseMsgs.Add("[" + x.OrderNo + "] User Exception : " + response.Message);
                                }
                            }
                        });


                    }
                    if(action != "AE")
                    sent.ForEach(x =>
                    {
                        Response response = IUserService.GetSingle(x.UserId);
                        if (response.IsSuccess)
                        {
                            string fundName = "";
                            string fundName2 = "";
                            List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                            List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();
                            List<UserOrder> allOrders = multipleOrders.Where(y => y.OrderNo == x.OrderNo).ToList();
                            User user = (User)response.Data;
                            UserAccount primaryAccount = user.UserIdUserAccounts.Where(w => w.Id == x.UserAccountId).FirstOrDefault();
                            Email email = new Email();
                            email.user = user;
                            allOrders.ForEach(z =>
                            {
                                Response responseF = IUtmcFundInformationService.GetSingle(z.FundId);
                                if (responseF.IsSuccess)
                                {
                                    UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF.Data;
                                    fundName = utmcFundInformation.FundName.Capitalize();
                                    funds.Add(utmcFundInformation);

                                }
                                if (z.ToFundId != 0)
                                {
                                    Response responseF2 = IUtmcFundInformationService.GetSingle(x.ToFundId);
                                    if (responseF2.IsSuccess)
                                    {
                                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF2.Data;
                                        fundName2 = utmcFundInformation.FundName.Capitalize();
                                        funds2.Add(utmcFundInformation);
                                    }
                                }
                            });
                            EmailService.SendOrderEmail(allOrders, email, funds, funds2, primaryAccount.AccountNo);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add(ex.Message);
                Console.WriteLine("Transactions action: " + ex.Message);
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = string.Empty;
            try
            {

                string tempPath = Path.GetTempPath() + "Payments_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Payments");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Order Date", "Order Status", "Username", "MA Account", "Order No.", "Fund Name", "Amount(MYR)", "Total Amount(MYR)", "Payment Method", "Processed Date", "Remarks" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Payments"];

                string docDetails = "Payments";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);


                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    filterQuery.Append(@"SELECT uo.ID, uo.user_id, u.username, u.id_no, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, 
uo.amount, uo.units, ufi.Fund_Name, group_concat(ufi.Fund_Name,'\r\n') as allfunds1, group_concat(uo.amount,'\r\n') as allamounts, 
ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, 
uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT, uo.order_status, 
uo.fpx_bank_code, uo.reject_reason as trans_no, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, uo.reject_reason, 
ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits,
if (uo.distribution_instruction = 1, 'Payout', if(uo.distribution_instruction = 3, 'Reinvest', '-')) as distribution_instruction 
FROM user_orders uo
left join users u on u.id = uo.user_id
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID 
                            where 1=1 ");

                    string fromDate = filter.Split(',')[1];
                    string toDate = filter.Split(',')[2];
                    filter = filter.Split(',')[0];

                    if (!string.IsNullOrEmpty(filter) && filter != "")
                    {
                        if (filter != "All")
                        {
                            docDetails += " | Status: " + (filter == "2" ? "Payment Approved" : (filter == "22" ? "Payment Pending" : (filter == "29" ? "Payment Rejected" : "")));
                            filterQuery.Append(" and uo.order_type IN ('1', '6') and uo.order_status in ('" + filter + "') ");
                        }
                        else
                        {
                            docDetails += " | Status: All (Payment Approved, Payment Pending, Payment Rejected)";
                            filterQuery.Append(" and uo.order_type IN ('1', '6') and uo.order_status IN('2', '22', '29') ");
                        }
                    }
                    else
                    {
                        docDetails += " | Status: Payment Pending ";
                        filterQuery.Append(" and uo.order_type IN ('1', '6') and uo.order_status IN ('22') ");
                    }

                    if (fromDate != "" && toDate != "")
                    {
                        DateTime from = DateTime.ParseExact(fromDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime to = DateTime.ParseExact(toDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        docDetails += " | Date Range: " + from + " - " + to;
                        filterQuery.Append(" and uo.created_date between '" + from.ToString("yyyy-MM-dd") + " 00:00:00' and '" + to.ToString("yyyy-MM-dd") + " 23:59:59' ");
                    }

                    filterQuery.Append(@"group by uo.order_no ");
                    filterQuery.Append(@"order by FIELD(uo.order_status, '2', '22', '29'), uo.created_date desc ");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responsePayments = GenericService.GetDataByQuery(filterQuery.ToString(), 0, 0, false, null, false, null, true);

                    if (responsePayments.IsSuccess)
                    {
                        var paymentsDyn = responsePayments.Data;
                        var responseJSON = JsonConvert.SerializeObject(paymentsDyn);
                        List<DiOTP.Utility.Payments> payments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Payments>>(responseJSON);

                        //string sumAmount = 0.0;
                        //decimal sumAmount = 0.0;

                        int index = 1;
                        payments.ForEach(x =>
                        {
                            string[] reasons = x.reject_reason.Split(',');
                            reasons = reasons.Distinct().ToArray();

                            string rejectReason = String.Join(",\r\n", reasons);

                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = x.created_date.ToString("dd/MM/yyyy");
                            worksheet.Cells[row, 3].Value = x.order_status == 2 ? "Payment Success" : x.order_status == 22 ? "Payment Pending" : x.order_status == 29 ? "Payment Rejected" : "";
                            worksheet.Cells[row, 4].Value = x.username.ToString();
                            worksheet.Cells[row, 5].Value = x.account_no;
                            worksheet.Cells[row, 6].Value = x.order_no;
                            worksheet.Cells[row, 7].Value = x.allfunds1;
                            worksheet.Cells[row, 8].Value = x.allamounts;
                            worksheet.Cells[row, 9].Value = x.SumAmount == 0 ? "" : x.SumAmount.ToString();
                            worksheet.Cells[row, 10].Value = x.payment_method == "1" ? "FPX PAYMENT" : (x.payment_method == "2" ? "PROOF OF PAYMENT" : "");
                            worksheet.Cells[row, 11].Value = (x.payment_date.HasValue && (x.order_status == 2 || x.order_status == 29) ? x.payment_date.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-");
                            worksheet.Cells[row, 12].Value = rejectReason;

                            row++;
                            index++;
                        });
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;

                    // Save this data as a file
                    excel.SaveAs(excelFile);

                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Payments List successfully downloaded",
                        TableName = "user_orders",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,

                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {

                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Payments" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

    }
}