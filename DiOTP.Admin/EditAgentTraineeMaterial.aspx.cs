﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class EditAgentTraineeMaterial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else {
                string idString = Request.QueryString["Id"];
                if (!String.IsNullOrEmpty(idString) && idString != "0")
                {
                    string query = (@" SELECT * FROM agent_trainee_materials where id='" + idString + "' ");

                    //get data
                    Response responseGetEditMaterial = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                    if (responseGetEditMaterial.IsSuccess)
                    {
                        var materialDyn = responseGetEditMaterial.Data;
                        var responseJSON = JsonConvert.SerializeObject(materialDyn);
                        List<AgentTraineeMaterial> materialList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTraineeMaterial>>(responseJSON);

                        if (materialList.Count == 1)
                        {
                            Id.Value = materialList.FirstOrDefault().id.ToString();
                            materialName.Text = materialList.FirstOrDefault().material_name.ToString();
                            materialDescription.Text = materialList.FirstOrDefault().material_description.ToString();
                            Url.Text = materialList.FirstOrDefault().material_url_path.ToString();

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Material not found\");", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseGetEditMaterial.Message + "\");", true);
                    }
                }
                else {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Material not found\");", true);
                }
            }
        }
    }
}