﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_fimms")]
    public class AgentRegFimm
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_reg_id"), Display("agent_reg_id")]
        public Int32 agent_reg_id { get; set; }
        [Database("new_card_received_on"), Display("new_card_received_on")]
        public DateTime? new_card_received_on { get; set; }
        [Database("FIMM_no"), Display("FIMM_no")]
        public String FIMM_no { get; set; }
        [Database("approval_date"), Display("approval_date")]
        public DateTime? approval_date { get; set; }
        [Database("expiry_date"), Display("expiry_date")]
        public DateTime? expiry_date { get; set; }
        [Database("remarks"), Display("remarks")]
        public String remarks { get; set; }
        [Database("is_renewal"), Display("is_renewal")]
        public Int32? is_renewal { get; set; }
        [Database("renewal_card_received_on"), Display("renewal_card_received_on")]
        public DateTime? renewal_card_received_on { get; set; }
        [Database("from_agent_MICR_code"), Display("from_agent_MICR_code")]
        public String from_agent_MICR_code { get; set; }
        [Database("from_agent_bank_name"), Display("from_agent_bank_name")]
        public String from_agent_bank_name { get; set; }
        [Database("from_agent_cheque_no"), Display("from_agent_cheque_no")]
        public String from_agent_cheque_no { get; set; }
        [Database("from_agent_trans_ref_no"), Display("from_agent_trans_ref_no")]
        public String from_agent_trans_ref_no { get; set; }
        [Database("from_agent_amount"), Display("from_agent_amount")]
        public Decimal? from_agent_amount { get; set; }
        [Database("from_agent_date_received"), Display("from_agent_date_received")]
        public DateTime? from_agent_date_received { get; set; }
        [Database("from_agent_payment_ref_no"), Display("from_agent_payment_ref_no")]
        public String from_agent_payment_ref_no { get; set; }
        [Database("from_agent_deposit_acc_no"), Display("from_agent_deposit_acc_no")]
        public String from_agent_deposit_acc_no { get; set; }
        [Database("to_FiMM_with_acc_no"), Display("to_FiMM_with_acc_no")]
        public String to_FiMM_with_acc_no { get; set; }
        [Database("to_FiMM_MICR_code"), Display("to_FiMM_MICR_code")]
        public String to_FiMM_MICR_code { get; set; }
        [Database("to_FiMM_bank_name"), Display("to_FiMM_bank_name")]
        public String to_FiMM_bank_name { get; set; }
        [Database("to_FiMM_cheque_no"), Display("to_FiMM_cheque_no")]
        public String to_FiMM_cheque_no { get; set; }
        [Database("to_FiMM_date_sent"), Display("to_FiMM_date_sent")]
        public DateTime? to_FiMM_date_sent { get; set; }
        [Database("to_FiMM_FIMM_renewal_amount"), Display("to_FiMM_FIMM_renewal_amount")]
        public Decimal? to_FiMM_FIMM_renewal_amount { get; set; }
        [Database("to_FiMM_UTMC_renewal_amount"), Display("to_FiMM_UTMC_renewal_amount")]
        public Decimal? to_FiMM_UTMC_renewal_amount { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32? created_by { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
