﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.Master" AutoEventWireup="true" CodeBehind="Switch-Fund.aspx.cs" Inherits="DiOTP.WebApp.Switch_Fund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/css/bootstrap-slider.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-10 col-md-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Switch Fund</li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h4 class="mb-4">Current Account Selected:</h4>
                        <h5>MA Account Number: <span id="maAccNumber" runat="server"></span></h5>
                        <h5>Account Type: <span id="accType" runat="server"></span></h5>
                        <h5>Account Name: <span id="accName" runat="server"></span></h5>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-center form-box">
                    <div class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-plus"></i></div>
                                <p>Fund Selection</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                                <p>Switching</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-file-text-o"></i></div>
                                <p>Confirmation</p>
                            </div>
                        </div>
                        <small class="text-danger" id="errorMessage"></small>

                        <fieldset class="fundSelectionFieldSet">
                            <div class="row">
                                <div class="col-md-12">
                                    <h6>Select the fund that you would like to switch.</h6>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Switch From.</h6>
                                            <div class="form-group mb-10">
                                                <label class="sr-only" for="f1-first-name">Fund</label>
                                                <asp:HiddenField ID="hdnId" runat="server" Value="0" ClientIDMode="Static" />
                                                <asp:DropDownList ID="ddlFundList" runat="server" CssClass="f1-first-name form-control" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>Switch to</h6>
                                            <div class="form-group mb-10">
                                                <label class="sr-only" for="f1-first-name">Fund</label>
                                                <asp:HiddenField ID="hdnId1" runat="server" Value="0" ClientIDMode="Static" />
                                                <asp:DropDownList ID="ddlFundList1" runat="server" CssClass="f1-first-name form-control" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="divFundDetails" class="hide">
                                                <div class="loadingDiv">
                                                    <div class="typing_loader"></div>
                                                    <div class="text-center">Loading...</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group mb-10">
                                                            <p class="fs-12 text-justify mb-5">* Please be informed that tou are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>
                                                        </div>
                                                        <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Fund Name</td>
                                                                    <td class="fundName">Fund Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Quantity</td>
                                                                    <td class="totalQuantity unitFormat">0</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="divFundDetails1" class="hide">
                                                <div class="loadingDiv">
                                                    <div class="typing_loader"></div>
                                                    <div class="text-center">Loading...</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Fund Name</td>
                                                                    <td class="fundName">Fund Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Quantity</td>
                                                                    <td class="totalQuantity unitFormat">0</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-5">
                                        <asp:CheckBox ID="chkConfirmFundInformation" ClientIDMode="Static" runat="server" CssClass="checkbox-inline" Text="I confirm that I have been given sufficient opportunity to read or access the electronic prospectus and the information therein." />
                                    </div>
                                    <div class="f1-buttons">
                                        <button type="button" class="btn btn-next">Next</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="redemptionFieldSet">
                            <div class="row">
                                <div class="col-md-12">
                                    <h6>Enter Switching Units:</h6>
                                    <div class="form-group mb-10">
                                        <label class="sr-only" for="txtRedemptionAmount">Switching Units</label>
                                        <asp:TextBox
                                            ID="txtRedemptionAmount"
                                            runat="server"
                                            class="form-control"
                                            ClientIDMode="Static"
                                            placeholder="Switching Units"></asp:TextBox>
                                    </div>
                                    <div class="form-group mb-10">
                                        <label>Enter Manually</label>
                                        <div>
                                            <asp:TextBox
                                                ID="txtRedemptionAmountManual"
                                                runat="server"
                                                class="form-control"
                                                ClientIDMode="Static"
                                                placeholder="Switching Units"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%-- <div class="form-group mb-5">
                                        <asp:CheckBox ID="chkSellAll" runat="server" ClientIDMode="Static" Text="Sell All" CssClass="checkbox-inline" />
                                    </div>--%>
                                </div>
                                <div class="col-md-4 hide">
                                    <h6>Account Plan:</h6>
                                    <div class="form-group">
                                        <label class="sr-only" for="ddlPaymentMethod">Account Plan</label>
                                        <asp:DropDownList ID="ddlPaymentMethod" runat="server" ClientIDMode="Static" CssClass="form-control">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="EPF">EPF</asp:ListItem>
                                            <asp:ListItem Value="CASH">CASH</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                            <div class="f1-buttons">
                                <button type="button" class="btn btn-previous">Previous</button>
                                <button type="button" class="btn btn-next">Next</button>
                            </div>
                        </fieldset>

                        <fieldset class="confirmationFieldSet">
                            <div class="row">
                                <div class="col-md-12">
                                    <h6>Confirm your Switching</h6>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <tr>
                                                    <td>Switching From Fund</td>
                                                    <td class="divFundDetailsfundName">Fund Name</td>
                                                </tr>
                                                <tr>
                                                    <td>Switching To Fund</td>
                                                    <td class="divFundDetails1fundName">Fund Name</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Units</td>
                                                    <td class="totalQuantity unitFormat">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Switching Units</td>
                                                    <td class="redemptionAmount unitFormat">0</td>
                                                </tr>
                                            </table>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <asp:Button ID="btnAddToCart" runat="server" ClientIDMode="Static" CssClass="btn btn-submit" Text="Add to Cart" OnClick="btnAddToCart_Click" />
                                                <asp:Button ID="btnUpdateToCart" runat="server" ClientIDMode="Static" CssClass="btn btn-submit hide" Text="Update to Cart" OnClick="btnUpdateToCart_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mt-20">
                    <div class="cart">
                        <div class="head">
                            <h5 class="mt-0 ff-ls text-center text-uppercase" style="margin-bottom: 2px !important;">Cart</h5>
                        </div>
                        <table class="table table-striped table-condensed table-cell-pad-5 table-font-size-13 table-cart">
                            <thead>
                                <tr>
                                    <th>Switching Fund</th>
                                    <th>To Fund</th>
                                    <th class="text-right">Investment</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="tbodyCart" runat="server">
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2">Total</th>
                                    <th class="text-right unitFormat" id="cartTotalAmount" runat="server" clientidmode="static">0</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <asp:Button ID="btnProceedToCart" runat="server" CssClass="btn btn-primary" Text="Proceed to Cart" OnClick="btnProceedToCart_Click" />
                    </div>
                </div>
            </div>
        </section>
    </div>


    <asp:HiddenField ID="hdnCurrentUnitHolding" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTotalQuantity" runat="server" ClientIDMode="Static" />
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/bootstrap-slider.js"></script>
    <script type="text/javascript">
        function scroll_to_class(element_class, removed_height) {
            var scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({ scrollTop: scroll_to }, 0);
            }
        }

        function bar_progress(progress_line_object, direction) {
            var number_of_steps = progress_line_object.data('number-of-steps');
            var now_value = progress_line_object.data('now-value');
            var new_value = 0;
            if (direction == 'right') {
                new_value = now_value + (100 / number_of_steps);
            }
            else if (direction == 'left') {
                new_value = now_value - (100 / number_of_steps);
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }
        var fundDetails;
        var minInitialInvestmentCashEPF = "0,0";
        var splits = '';
        function FundDetails(Id, divId) {
            $('#' + divId + ' .loadingDiv').removeClass('hide');
            $('#' + divId).removeClass('hide');
            $.ajax({
                url: "Sell-Fund.aspx/GetFundDetails",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { 'Id': Id },
                success: function (data) {
                    $('#' + divId + ' .loadingDiv').addClass('hide');
                    splits = data.d.splits;
                    var json = data.d.UtmcFundInformation;
                    console.log(data.d.utmcDetailedMemberInvestmentsByFund[0].utmcMemberInvestments[0]);
                    var utmcDetailedMemberInvestmentsByFund = data.d.utmcDetailedMemberInvestmentsByFund;
                    fundDetails = json;
                    console.log($(divId).find('.fundName').html());
                    $('#' + divId).find('.fundName').html(json.FundName);
                    $('.' + divId + 'fundName').html(json.FundName);
                    $('.totalQuantity').html(utmcDetailedMemberInvestmentsByFund[0].utmcMemberInvestments[0].ActualTransferredFromEpfRm.toFixed(2));
                    $('#hdnTotalQuantity').val(utmcDetailedMemberInvestmentsByFund[0].utmcMemberInvestments[0].ActualTransferredFromEpfRm.toFixed(2));
                    FormatAllUnit();
                }
            });
        }
        var isUniFormat;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: ' UNITS', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
        }
        FormatAllUnit();
        var investAmt = "";
        var selectedValue = $('#ddlFundList').val();
        var priceSlider;
        var priceSliderStep = 1;
        $(document).keydown(function (e) {
            if (priceSlider != undefined) {
                var value = priceSlider.data('slider').getValue();
                if (e.keyCode == 37) {
                    priceSlider.slider('setValue', value - priceSliderStep);
                }
                if (e.keyCode == 39) {
                    priceSlider.slider('setValue', value + priceSliderStep);
                }
            }
        });
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
        }
        var priceSliderValue = 0;
        $(document).ready(function () {
            $('.fundSelectionFieldSet .btn-next').click(function () {
                if ($('#ddlFundList').val() != "" && $('#chkConfirmFundInformation').is(':checked')) {
                    setTimeout(function () {
                        var hdnTotalQuantity = parseInt($('#hdnTotalQuantity').val());
                        $('#errorMessage').html('');
                        if (priceSlider != undefined) {
                            priceSlider.slider('destroy');
                        }
                        priceSlider = $('#txtRedemptionAmount').slider({
                            animate: true,
                            value: priceSliderValue,
                            min: 0,
                            max: hdnTotalQuantity,
                            //scale: 'logarithmic',
                            //step: 1,
                            tooltip: 'always',
                            //ticks: splits,
                            //ticks_labels: splits,//['$0', '$100', '$200', '$300', '$400'],
                            ticks_snap_bounds: 100
                        }).on('slide', function () {
                            var priceSlidersValue = parseFloat(priceSlider.data('slider').getValue());
                            $('#txtRedemptionAmountManual').val($('#txtRedemptionAmount').val());
                            FormatAllUnit();
                        }).on('change', function () {
                            var priceSlidersValue = parseFloat(priceSlider.data('slider').getValue());
                            $('#txtRedemptionAmountManual').val($('#txtRedemptionAmount').val());
                            FormatAllUnit();
                        });
                    }, 500);
                }
            });

            $('#txtRedemptionAmountManual').keyup(function (e) {
                if ($('#txtRedemptionAmountManual').val() != "") {
                    var hdnTotalQuantity = parseFloat($('#hdnTotalQuantity').val());
                    var redemptionAmount = parseFloat($('#txtRedemptionAmountManual').val());
                    console.log(redemptionAmount + " : " + hdnTotalQuantity);
                    if (redemptionAmount <= hdnTotalQuantity) {
                        if (priceSlider != undefined) {
                            priceSlider.slider('setValue', redemptionAmount);
                        }
                        if (redemptionAmount < hdnTotalQuantity)
                            $('#chkSellAll').removeAttr('checked');
                        if (redemptionAmount == hdnTotalQuantity)
                            $('#chkSellAll').attr('checked', 'checked');
                    }
                    else {
                        $('#txtRedemptionAmountManual').val(hdnTotalQuantity);
                        $('#chkSellAll').attr('checked', 'checked');
                        if (priceSlider != undefined) {
                            console.log(redemptionAmount);
                            priceSlider.slider('setValue', hdnTotalQuantity);
                        }
                    }
                }
            });

            //$('.schemeSelection .btn-next').click(function () {
            //    if ($('#ddlScheme').val() != "") {
            //        var schemeSelected = parseInt($('#ddlScheme').val());
            //        $.ajax({
            //            url: "Buy-Fund.aspx/GetFunds",
            //            async: true,
            //            contentType: 'application/json; charset=utf-8',
            //            type: "GET",
            //            dataType: "JSON",
            //            data: { 'scheme': schemeSelected },
            //            success: function (data) {
            //                var json = data.d;
            //                $('#ddlFundList option:not(:first)').remove();
            //                var fundsOptions = '';
            //                $.each(json, function (idx, y) {
            //                    if (selectedValue == y.Id)
            //                        fundsOptions += '<option value="' + y.Id + '" selected>' + y.FundName + '</option>';
            //                    else
            //                        fundsOptions += '<option value="' + y.Id + '">' + y.FundName + '</option>';
            //                });
            //                $('#ddlFundList').append(fundsOptions);
            //                $('#ddlFundList').change();
            //            }
            //        });
            //    }
            //});

            $('.table-cart').on('click', '.editFromCart', function () {
                var id = parseInt($(this).attr('data-id'));
                var fundId = parseInt($(this).attr('data-fundId'));
                var Amount = parseFloat($(this).attr('data-amount'));
                $('#btnUpdateToCart').removeClass('hide');
                $('#btnAddToCart').addClass('hide');
                $('#ddlFundList').val(fundId);
                $('#ddlFundList').change();
                $('#hdnId').val(id);
                $('#txtInvestmentAmount').val(Amount);
                priceSliderValue = Amount;
                if (priceSlider != undefined)
                    priceSlider.slider('setValue', Amount);
            });
            $('.table-cart').on('click', '.removeFromCart', function () {
                var that = $(this);
                var id = parseInt($(that).attr('data-id'));
                $.ajax({
                    url: "Buy-Fund.aspx/RemoveFromCart",
                    async: true,
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { 'Id': id },
                    success: function (data) {
                        var json = data.d;
                        console.log(json);
                        $('#cartTotalAmount').html(json);
                        $(that).parents('tr').remove();
                    }
                });
            });
            $('#txtRedemptionAmount').on('keyup', function (e) {
                var txt = $(this).val();
                if ($(this).val().split('.').length <= 2) {
                    if (txt.match(/^[0-9.]*$/) != null) {
                        investAmt = $(this).val();
                        $('#txtRedemptionAmount').val(investAmt);
                        $('.redemptionAmount').html(parseFloat(investAmt).toFixed(2));
                        return true;
                    }
                    else {
                        $('#txtRedemptionAmount').val(investAmt);
                        return false;
                    }
                }
                else {
                    $('#txtRedemptionAmount').val(investAmt);
                    return false;
                }
            });
            $('#ddlFundList').change(function () {
                if ($(this).val() != "" && $(this).val() != null) {
                    var Id = parseInt($(this).val());
                    FundDetails(Id, "divFundDetails");
                }
                else {
                    $('#divFundDetails').addClass('hide');
                }
            });
            $('#ddlFundList').change();

            $('#ddlFundList1').change(function () {
                if ($(this).val() != "" && $(this).val() != null) {
                    var Id = parseInt($(this).val());
                    $.ajax({
                        url: "Sell-Fund.aspx/GetFundDetails",
                        async: true,
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { 'Id': Id },
                        success: function (data) {
                            var json = data.d.UtmcFundInformation;
                            $('.divFundDetails1fundName').html(json.FundName);
                        }
                    });
                }
            });

            //$('#ddlFundList1').change(function () {
            //    if ($(this).val() != "" && $(this).val() != null) {
            //        var Id = parseInt($(this).val());
            //        FundDetails(Id, "divFundDetails1");
            //    }
            //    else {
            //        $('#divFundDetails1').addClass('hide');
            //    }
            //});
            /* Form */
            $('.f1 fieldset:first').fadeIn('slow');
            $('.f1 input[type="text"], .f1 input[type="password"], .f1 input[type="checkbox"], .f1 textarea').on('focus', function () {
                $(this).removeClass('input-error');
            });
            // next step
            $('.f1 .btn-next').on('click', function () {

                $('.redemptionAmount').html(parseFloat($('#txtRedemptionAmount').val()).toFixed(2));
                $('.paymentMethod').html($('#ddlPaymentMethod option:selected').text());
                var parent_fieldset = $(this).parents('fieldset');
                var next_step = true;
                // navigation steps / progress steps
                var current_active_step = $(this).parents('.f1').find('.f1-step.active');
                var progress_line = $(this).parents('.f1').find('.f1-progress-line');
                // fields validation
                parent_fieldset.find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                    if ($(this).val() == "") {
                        $(this).addClass('input-error');
                        next_step = false;
                    }
                    else {
                        if ($(this).is(':checkbox')) {
                            if (!$(this).attr('checked')) {
                                $(this).parents('.form-group').addClass('input-error');
                                next_step = false;
                            }
                            else {
                                $(this).parents('.form-group').removeClass('input-error');
                            }
                        }
                        else {
                            $(this).removeClass('input-error');
                        }
                    }

                    if (parent_fieldset.hasClass('investmentFieldSet') && $('#txtInvestmentAmount').val() != "") {
                        if ($(this).attr('type') != null)
                            if ($(this).attr('type') == 'text') {
                                var investVal = parseInt($('#txtInvestmentAmount').val());
                                if (investVal < 0) {
                                    $('#txtInvestmentAmount').addClass('input-error');
                                    $('#errorMessage').html('');
                                    next_step = false;
                                }
                                else {
                                    $(this).removeClass('input-error');
                                    $('#errorMessage').html('');
                                    FormatAllUnit();
                                }
                            }
                    }

                });
                // fields validation
                if (next_step) {
                    parent_fieldset.fadeOut(400, function () {
                        // change icons
                        current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                        // progress bar
                        bar_progress(progress_line, 'right');
                        // show next step
                        $(this).next().fadeIn();
                        // scroll window to beginning of the form
                        scroll_to_class($('.f1'), 20);
                    });
                }
            });

            // previous step
            $('.f1 .btn-previous').on('click', function () {
                // navigation steps / progress steps
                var current_active_step = $(this).parents('.f1').find('.f1-step.active');
                var progress_line = $(this).parents('.f1').find('.f1-progress-line');
                $(this).parents('fieldset').fadeOut(400, function () {
                    // change icons
                    current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                    // progress bar
                    bar_progress(progress_line, 'left');
                    // show previous step
                    $(this).prev().fadeIn();
                    // scroll window to beginning of the form
                    scroll_to_class($('.f1'), 20);
                });
            });
            // submit
            $('.f1').on('submit', function (e) {
                // fields validation
                $(this).find('input[type="text"], input[type="password"], input[type="checkbox"], textarea').each(function () {
                    if ($(this).val() == "") {
                        e.preventDefault();
                        $(this).addClass('input-error');
                    }
                    else {
                        $(this).removeClass('input-error');
                    }
                });
                // fields validation
            });
        });
    </script>
</asp:Content>
