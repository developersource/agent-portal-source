﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddInstructor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] != null)
            {
                User sessionUser = (User)Session["admin"];
                string idString = Request.QueryString["id"];
                if (idString != null && idString != "")
                {
                    int id = Convert.ToInt32(idString);

                    string queryAReg = (@" SELECT * FROM instructors where id='" + id + "' ");
                    Response responseIList = GenericService.GetDataByQuery(queryAReg, 0, 0, false, null, false, null, false);
                    if (responseIList.IsSuccess)
                    {
                        var instructorsDyn = responseIList.Data;
                        var responseJSON = JsonConvert.SerializeObject(instructorsDyn);
                        List<DiOTP.Utility.Instructor> instructors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Instructor>>(responseJSON);
                        if (instructors.Count == 1)
                        {
                            Instructor instructor = instructors.FirstOrDefault();
                            Id.Value = instructor.Id.ToString();
                            Name.Value = instructor.Name;
                            EmailId.Value = instructor.EmailId;
                            MobileNumber.Value = instructor.MobileNumber;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Instructor not found\");", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseIList.Message + "\");", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Invalid link\");", true);
                }
            }
        }
    }
}