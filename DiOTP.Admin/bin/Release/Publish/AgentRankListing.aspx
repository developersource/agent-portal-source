﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgentRankListing.aspx.cs" Inherits="Admin.AgentRankListing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="AgentRankListingForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully Demoted" data-status="Inactive" data-original-title="Demote" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Demote" data-isconfirm="1"><i class="fa fa-arrow-down"></i></button>
                                                <button data-message="Successfully Promoted" data-status="Active" data-original-title="Promote" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Promote" data-isconfirm="0"><i class="fa fa-arrow-up"></i></button>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can search here with Agent Name, Agent Code, Agent Email</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can Promote/Demote Agent Ranks</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                        <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="0">All</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="6">WA</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="7">WM</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="8">GM</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="9">SGM</button>
                                        <small>
                                            <br />
                                            <strong>Note:</strong>
                                            <span>Agent Type Filtering on Agent Accounts</span>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable bannerListing" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th>Agent Name</th>
                                            <th>Email Address</th>
                                            <th>Agent Code</th>
                                            <th>Agent Rank </th>                                    
                                        </tr>
                                    </thead>
                                    <tbody id="TraineeMaterialTBody" runat="server">
                                         <tr>
                                         <td class='icheck'>
                                            <div class='square single-row'>
                                                 <div class='checkbox'>
                                                    <input type='checkbox' name='checkRow' class='checkRow' value='1' /> <label>1</label><br/>
                                                 </div>
                                              </div>
                                             <span class='row-status'><span class='label label-success'>Active</span></span>
                                         </td>
                                        <td>James Wong</td>
                                        <td>AG2510</td>
                                        <td>WM</td>
                                        <td>22/10/2021</td>
                                    </tr>
                                    <tr>
                                        <td class='icheck'>
                                            <div class='square single-row'>
                                                 <div class='checkbox'>
                                                    <input type='checkbox' name='checkRow' class='checkRow' value='2' /> <label>2</label><br/>
                                                 </div>
                                              </div>
                                             <span class='row-status'><span class='label label-success'>Active</span></span>
                                         </td>
                                         <td>Wong Shi Kei</td>
                                        <td>AG1572</td>
                                        <td>WM</td>
                                        <td>22/10/2021</td>
                                    </tr>
                                    <tr>
                                        <td class='icheck'>
                                            <div class='square single-row'>
                                                 <div class='checkbox'>
                                                    <input type='checkbox' name='checkRow' class='checkRow' value='3' /> <label>3</label><br/>
                                                 </div>
                                              </div>
                                             <span class='row-status'><span class='label label-success'>Active</span></span>
                                         </td>
                                         <td>Lily James</td>
                                        <td>AG2520</td>
                                        <td>WM</td>
                                        <td>22/10/2021</td>
                                    </tr>
                                    </tbody>
                                   
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderByColumn" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderType" runat="server" ClientIDMode="Static" Value="0" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
