using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("form_data_field_values")]
    public class FormDataFieldValue
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("form_data_field_id"), Display("FormDataFieldId")]
        public Int32 FormDataFieldId { get; set; }
        [Database("value"), Display("Value")]
        public String Value { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        public FormDataField FormDataFieldIdFormDataField { get; set; }
        public UserAccount UserAccountIdUserAccount { get; set; }
    }
}
