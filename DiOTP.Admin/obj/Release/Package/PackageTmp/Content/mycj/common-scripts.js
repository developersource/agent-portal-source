﻿$(document).ready(function () {
    setTimeout(function () {


        var tabSelectedLi = $('li.tabs-selected');
        var selectedTabIndex = $('#tt ul.tabs li').index(tabSelectedLi);
        var selectedPanel = $('.panel.panel-htop')[selectedTabIndex];
        var formID = $(selectedPanel).find('form').attr('id');
        $('#' + formID + ' .square input').iCheck({
            checkboxClass: 'icheckbox_square',
            radioClass: 'iradio_square',
        });

        $('#' + formID + ' *').unbind('click');

        var pageL = parseInt($('#' + formID + ' #pageLength').val());

        var aspxURL = $(tabSelectedLi).find('.data-url').text();

        $('#' + formID + ' th[data-column-name]').click(function () {
            $('#' + formID + ' #IsNewSearch').val('2');
            var column = $(this).data('column-name');
            if ($('#' + formID + ' #hdnOrderByColumn').val() == column && $('#' + formID + ' #hdnOrderType').val() != 'desc') {
                $('#' + formID + ' #hdnOrderType').val('desc');
            }
            else {
                $('#' + formID + ' #hdnOrderType').val('');
            }
            $('#' + formID + ' #hdnOrderByColumn').val(column);
            loadContent();
        });

        $('#' + formID + ' .checkRowAll').unbind('ifChecked');
        $('#' + formID + ' .checkRowAll').on('ifChecked', function () {
            $.each($('#' + formID + ' input.checkRow'), function (idx, obj) {
                $(obj).iCheck('check');
            });
        });
        $('#' + formID + ' .checkRowAll').unbind('ifUnchecked');
        $('#' + formID + ' .checkRowAll').on('ifUnchecked', function () {
            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                $(obj).iCheck('uncheck');
            });
        });

        var selectedFilter = $('#' + formID + ' #FilterValue').val();
        if (selectedFilter == "") {
            $('#' + formID + ' button.first-load').removeClass('btn-primary');
            $('#' + formID + ' button.first-load').addClass('btn-secondary');
        }

        function refreshContent() {
            $('.loadingDivAdmin').show();
            var tabSelectedLi = $('li.tabs-selected');
            var selectedTabIndex = $('#tt ul.tabs li').index(tabSelectedLi);
            var selectedPanel = $('.panel.panel-htop')[selectedTabIndex];
            var formID = $(selectedPanel).find('form').attr('id');
            var aspxURLNew = $(tabSelectedLi).find('.data-url').text();
            var url = aspxURLNew;
            $.get(url, function (content) {
                var selectedTab = $('#tt').tabs('getSelected');
                $('#tt').tabs('update', {
                    tab: selectedTab,
                    options: {
                        content: content
                    }
                });
                var selectedFilter = $('#' + formID + ' #FilterValue').val();
                if (selectedFilter == "") {
                    $('#' + formID + ' button.first-load').removeClass('btn-primary');
                    $('#' + formID + ' button.first-load').addClass('btn-secondary');
                }
                else {
                    $('#' + formID + ' [data-filter="' + selectedFilter + '"]').removeClass('btn-primary');
                    $('#' + formID + ' [data-filter="' + selectedFilter + '"]').addClass('btn-secondary');
                }
                $('.loadingDivAdmin').hide();
            }, 'html');
        }

        function loadContent() {
            $('.loadingDivAdmin').show();
            var data = $('#' + formID).serializeArray();
            data = data.filter(function (item) {
                return item.name.indexOf('_') === -1;
            });
            //<NEW
            if (aspxURL.indexOf('?') != -1)
                aspxURL = aspxURL.split('?')[0];
            //NEW>
            var url = aspxURL + "?" + $.param(data);

            $.post(url, function (content) {
                var selectedTab = $('#tt').tabs('getSelected');
                $('#tt').tabs('update', {
                    tab: selectedTab,
                    options: {
                        content: content
                    }
                });
                $('.loadingDivAdmin').hide();

                var selectedFilter = $('#' + formID + ' #FilterValue').val();
                if (selectedFilter == "") {
                    $('#' + formID + ' button.first-load').removeClass('btn-primary');
                    $('#' + formID + ' button.first-load').addClass('btn-secondary');
                }
                else {
                    $('#' + formID + ' [data-filter="' + selectedFilter + '"]').removeClass('btn-primary');
                    $('#' + formID + ' [data-filter="' + selectedFilter + '"]').addClass('btn-secondary');
                }
                var columnSorted = $('#' + formID + ' #hdnOrderByColumn').val();
                var orderBy = $('#' + formID + ' #hdnOrderType').val();
                if (orderBy == 'desc') {
                    $('#' + formID + ' th[data-column-name="' + columnSorted + '"]').addClass('sorting_desc');
                }
                else {
                    $('#' + formID + ' th[data-column-name="' + columnSorted + '"]').addClass('sorting_asc');
                }
                //$('#' + formID + ' #Search').focus();
                //var tmpStr = $('#' + formID + ' #Search').val();
                //$('#' + formID + ' #Search').val('');
                //$('#' + formID + ' #Search').val(tmpStr);
            }, 'html');
        }

        if ($.fn.DataTable.isDataTable('#' + formID + ' .dataTable')) {
            $('#' + formID + ' .dataTable').DataTable().destroy();
        }


        $('#' + formID + ' .dataTable').DataTable({
            dom: "t<'row'<'col-md-4 infoDiv'><'col-md-8 text-right paginationDiv'>>",
            select: true,
            destroy: true,
            responsive: true,
            order: [],
            ordering: true,
            searching: true,
            length: false,
            pageLength: pageL,
            columnDefs: [
                { orderable: false, targets: 0 }
            ]
        });

        $('#' + formID + ' .dataTable').DataTable().on('order.dt search.dt', function () {
            $('#' + formID + ' .dataTable').DataTable().column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                $(cell).find('.checkbox label').html(i + 1);
            });
        }).draw();

        var pageNo = parseInt($('#' + formID + ' #hdnCurrentPageNo').val());
        var total = parseInt($('#' + formID + ' #hdnTotalRecordsCount').val());
        // var numPerPage = parseInt($('#' + formID + ' #hdnNumberPerPage').val());
        var pageNos = parseInt(Math.ceil(total / pageL));

        $('#' + formID + ' .infoDiv').html("Showing page " + (pageNo == NaN ? "1" : pageNo) + " of " + pageNos + " (Total records: " + total + ")");

        var ul = $('<ul class="pagination" />');
        var liFirst = $('<li />').addClass('firstPage ' + (pageNo == 1 ? 'active' : ''));
        var aFirst = $('<a />').attr('href', 'javascript:;').addClass('pageNumber ' + (pageNo == 1 ? 'active' : '')).html('First').appendTo(liFirst);
        liFirst.appendTo(ul);
        var liPrev = $('<li />').addClass('prev');
        var aPrev = $('<a />').attr('href', 'javascript:;').addClass('pageNumber').html('Prev').appendTo(liPrev);
        liPrev.appendTo(ul);
        var className = 'pageNumber pageNoDis';
        for (i = 1; i <= pageNos; i++) {
            if (i < 3) {
                if (i == pageNo)
                    className = 'pageNumber pageNoDis active';
                else
                    className = 'pageNumber pageNoDis';
                var li = $('<li />');
                if (i == pageNo)
                    li = $('<li class="active" />');
                var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                li.appendTo(ul);
            }
            else if (i > pageNo - 3 && i < pageNo + 3) {
                if (i == pageNo)
                    className = 'pageNumber pageNoDis active';
                else
                    className = 'pageNumber pageNoDis';
                var li = $('<li />');
                if (i == pageNo)
                    li = $('<li class="active" />');
                var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                li.appendTo(ul);
            }
            else if (i > pageNos - 2) {
                className = 'pageNumber pageNoDis';
                var li = $('<li />');
                if (i == pageNo)
                    li = $('<li class="active" />');
                var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                li.appendTo(ul);
            }
            if (i == 3 && pageNo >= 6 || i == pageNos - 3 && pageNo <= pageNos - 6) {
                className = 'pageNumber pageNoDis';
                var li = $('<li />');
                if (i == pageNo)
                    li = $('<li class="active" />');
                var a = $('<a href="javascript:;" />').html('...').appendTo(li);
                li.appendTo(ul);
            }
        }
        var liNext = $('<li />').addClass('next');
        var aNext = $('<a />').attr('href', 'javascript:;').addClass('pageNumber').html('Next').appendTo(liNext);
        liNext.appendTo(ul);
        var liLast = $('<li />').addClass('lastPage ' + (pageNo == pageNos ? 'active' : ''));
        var aLast = $('<a />').attr('href', 'javascript:;').addClass('pageNumber ' + (pageNo == pageNos ? 'active' : '')).html('Last').appendTo(liLast);
        liLast.appendTo(ul);
        $('#' + formID + ' .paginationDiv').append(ul);
        $('#' + formID + ' li.firstPage').click(function () {
            $('#' + formID + ' #IsNewSearch').val('0');
            var curPageNo = parseInt(1);
            $('#' + formID + ' #hdnCurrentPageNo').val(curPageNo);
            loadContent();
        });
        $('#' + formID + ' li.lastPage').click(function () {
            $('#' + formID + ' #IsNewSearch').val('0');
            var curPageNo = parseInt($('#' + formID + ' .pageNoDis').last().text());
            $('#' + formID + ' #hdnCurrentPageNo').val(curPageNo);
            loadContent();
        });
        $('#' + formID + ' li.next').click(function () {
            $('#' + formID + ' #IsNewSearch').val('0');
            var curPageNo = parseInt($('#' + formID + ' .pageNumber.pageNoDis.active').text());
            if (curPageNo >= parseInt($('.pageNoDis').last().text())) {

            }
            else {
                $('#' + formID + ' #hdnCurrentPageNo').val(curPageNo + 1);
                loadContent();
            }

        });
        $('#' + formID + ' li.prev').click(function () {
            $('#' + formID + ' #IsNewSearch').val('0');
            var curPageNo = parseInt($('#' + formID + ' .pageNumber.pageNoDis.active').text());
            if (curPageNo <= parseInt($('#' + formID + ' .pageNoDis').first().text())) {

            }
            else {
                $('#' + formID + ' #hdnCurrentPageNo').val(curPageNo - 1);
                loadContent();
            }
        });
        $('#' + formID + ' .pageNumber.pageNoDis').click(function () {
            $('#' + formID + ' #IsNewSearch').val('0');
            var curPageNo = parseInt($(this).text());
            $('#' + formID + ' #hdnCurrentPageNo').val(curPageNo);
            loadContent();
        });

        $('#' + formID + ' #pageLength').change(function () {
            pageL = parseInt($(this).val());
            loadContent();
        });

        function CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, rejectReason) {
            var checkedRowIds = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                return $(obj).val();
            });
            var postData = { ids: JSON.stringify(checkedRowIds), action: JSON.stringify(action), rejectReason: JSON.stringify(rejectReason) };
            $.ajax({
                url: aspxURLNew + "/Action",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: postData,
                success: function (data) {
                    if (action == 'Activate') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-success'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'Deactivate') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-danger'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'Promote') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-success'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'Demote') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-danger'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'Suspend') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-danger'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'Approve') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-success'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'Reject') {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else {
                            showSnackBar(actionMsg);
                            $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                                $(obj).parents('td').find('.row-status').html("<span class='label label-danger'>" + actionStatus + "</span>");
                                $(obj).iCheck('uncheck');
                            });
                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'GenerateToken') {
                        showSnackBar(actionMsg);
                        setTimeout(loadContent, 100);
                    }
                    else if (action == 'Download') {
                        showSnackBar(data.d.Message);
                        if (data.d.IsSuccess) {
                            var fileName = data.d.Data;
                            //alert(fileName);
                            var elementA = $("<a />");
                            $(elementA).attr('href', "/" + fileName);
                            $(elementA).hide();
                            $('body').append(elementA);
                            $(elementA).click();
                            $(elementA).remove();
                            //window.location.href = fileName;
                            $('.loadingDivAdmin').hide();
                        }
                    }
                    else if (action == 'VerifyID') {
                        if (data.d.IsSuccess != undefined) {
                            var jsonPretty = JSON.stringify(JSON.parse(data.d.Message), null, '\t');
                            const html = prettyPrintJson.toHtml(JSON.parse(data.d.Message));
                            if (!data.d.IsSuccess) {
                                $.alert({
                                    title: "Identity Verification Failed",
                                    content: "<pre>" + html + "</pre>",
                                    containerFluid: true
                                });
                            }
                            else {
                                $.alert({
                                    title: "Identity Verified",
                                    content: "<pre>" + html + "</pre>",
                                    containerFluid: true
                                });
                            }
                            setTimeout(loadContent, 100);
                        }
                    }
                    else {
                        if (data.d.IsSuccess != undefined) {
                            showSnackBar(data.d.Message);
                            setTimeout(loadContent, 100);
                        }
                        else
                            showSnackBar(actionMsg);
                        setTimeout(loadContent, 100);
                    }
                }
            });
        }

        $('#' + formID + ' .action-button').unbind('click');
        $('#' + formID + ' .action-button').click(function () {
            var tabSelectedLi = $('li.tabs-selected');
            var selectedTabIndex = $('#tt ul.tabs li').index(tabSelectedLi);
            var selectedPanel = $('.panel.panel-htop')[selectedTabIndex];
            var formID = $(selectedPanel).find('form').attr('id');
            var aspxURLNew = $(tabSelectedLi).find('.data-url').text();
            var statuses = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                return $(obj).parents('td').find('.row-status .label').text();
            });
            var checkedRowIds = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                return $(obj).val();
            });
            var that = $(this);
            var action = $(this).attr('data-action');
            if (action == 'Activate' || action == 'Deactivate' || action == 'Suspend' || action == 'Reject' || action == 'GenerateToken' || action == 'Recommend' || action == 'Approve' || action == 'ResendLink' || action == 'Toggle' || action == 'RequestFile' || action == 'RequestUnclearFile' || action == 'VerifyID' || action == "ApproveDoc" || action == "RejectDoc" || action == "Promote" || action == "Demote") {
                var actionStatus = $(this).attr('data-status');
                var actionTitle = $(this).attr('data-original-title');
                var actionMsg = $(this).attr('data-message');

                if (checkedRowIds.length == 0) {
                    $.alert('Please select records.');
                    return false;
                }
                if (action == 'VerifyID') {
                    if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                        $.alert('Please select one to approve.');
                        return false;
                    }
                }

                if (action == 'Promote' || action == 'Demote') {
                    if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                        $.alert('Please select only one agent to Promote/Demote.');
                        return false;
                    }
                }

                var isPromote = $(this).attr('data-ispromote');
                var isConfirm = $(this).attr('data-isconfirm');
                var isReason = $(this).attr('data-isreason');
                var reasonLabel = $(this).attr('data-reasonLabel');
                var reasonInput = $(this).attr('data-reasonInput');
                var isExcecute = 0;
                if ((action == 'Approve' || action == 'Activate') && (statuses.indexOf('Approved') != -1 || statuses.indexOf('OA') != -1)) {
                    showSnackBar('Already approved.');
                    $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                        $(obj).iCheck('uncheck');
                    });
                    $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                    return false;
                }
                if ((action == 'Approve' || action == 'Activate') && (statuses.indexOf('Rejected') != -1 || statuses.indexOf('OR') != -1)) {
                    showSnackBar('Rejected records cannot be approved.');
                    $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                        $(obj).iCheck('uncheck');
                    });
                    $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                    return false;
                }
                if (action == 'Reject' && (statuses.indexOf('Approved') != -1 || statuses.indexOf('OA') != -1)) {
                    showSnackBar('Approved records cannot be rejected.');
                    $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                        $(obj).iCheck('uncheck');
                    });
                    $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                    return false;
                }
                if (action == 'Reject' && (statuses.indexOf('Rejected') != -1 || statuses.indexOf('OR') != -1)) {
                    showSnackBar('Already rejected.');
                    $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
                        $(obj).iCheck('uncheck');
                    });
                    $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                    return false;
                }

                //If Demote
                if (isPromote == 0) {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure to ' + actionTitle + '?',
                        buttons: {
                            confirm: function () {
                                isExcecute = 1;
                                AgentPDFunction(action);
                                //insert function here to pass values perform AJAX call.
                                //$.alert('Confirmed!');
                            },
                            cancel: function () {
                                isExcecute = 0;
                                //$.alert('Canceled!');
                            }
                        }
                    });
                }
                //If Promote
                if (isPromote == 1) {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure to ' + actionTitle + '?',
                        buttons: {
                            confirm: function () {
                                isExcecute = 1;

                                //insert function here to pass values perform AJAX call.
                                //$.alert('Confirmed!');
                            },
                            cancel: function () {
                                isExcecute = 0;
                                //$.alert('Canceled!');
                            }
                        }
                    });
                }

                if (isConfirm == 1) {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure to ' + actionTitle + '?',
                        buttons: {
                            confirm: function () {
                                isExcecute = 1;

                                var rejectReason = '';
                                var othersScript = "javascript:if(this.value == 'Others'){document.getElementById('rejectReasonCustom').style.display='block';}else{document.getElementById('rejectReasonCustom').style.display='none'}";
                                if (action == "Reject" || action == "Deactivate" || action == "Suspend" || action == "RequestFile" || action == 'RequestUnclearFile' || action == "VerifyID" || action == "ApproveDoc" || action == "RejectDoc" || action == "ResendLink" || action == "Demote") {
                                    var confirmationPopupTitle = 'Reject reason';
                                    var content = '' +
                                        '<form action="" class="formName">' +
                                        '<div class="form-group">' +
                                        '<label>Enter something here</label>' +
                                        '<select class="form-control rejectReason" name="rejectReason" onchange="' + othersScript + '">' +
                                        '<option value="">Select</option>' +
                                        '<option value="Document invalid">Document invalid</option>' +
                                        '<option value="Payment failed">Payment failed</option>' +
                                        '<option value="Others">Others</option>' +
                                        '</select>' +
                                        '<br/>' +
                                        '<input type="text" class="form-control rejectReasonCustom" id="rejectReasonCustom" placeholder="Enter your reject reason" style="display:none;" />' +
                                        '</div>' +
                                        '</form>';
                                    if (action == "Deactivate" || action == "Suspend") {
                                        confirmationPopupTitle = 'Reject reason';
                                        content = '' +
                                            '<form action="" class="formName">' +
                                            '<div class="form-group">' +
                                            '<label>Enter something here</label>' +
                                            '<br/>' +
                                            '<input type="text" class="form-control rejectReason" id="rejectReasonCustom" placeholder="Enter your reject reason" />' +
                                            '</div>' +
                                            '</form>';
                                    }
                                    if (action == "RequestFile") {
                                        confirmationPopupTitle = 'Requirement';
                                        content = '' +
                                            '<form action="" class="formName">' +
                                            '<div class="form-group">' +
                                            '<label>Enter file requirement here</label>' +
                                            '<br/>' +
                                            '<input type="text" class="form-control rejectReason" id="rejectReasonCustom" placeholder="Enter your requirement" />' +
                                            '<div class="mt-10">' +
                                            '<p>i. Latest three (3) months’ pay slip/ latest bank statement / latest income statement, or</p>' +
                                            '<p>ii. Tax statement, i.e. latest J/EA Form, or</p>' +
                                            '<p>iii. Letter of confirmation of employment</p>' +
                                            '</div>' +
                                            '</div>' +
                                            '</form>';
                                    }
                                    if (action == "RequestUnclearFile") {
                                        confirmationPopupTitle = 'Unclear Documents';
                                        content = '' +
                                            '<form action="" class="formName">' +
                                            '<div class="form-group">' +
                                            '<label>Select unclear documents</label>' +
                                            '<br/>' +
                                            '<div class="mt-10">' +
                                            '<p><input type="checkbox" class="rejectReason" value="1" id="NRICFront" /> <label for="NRICFront">NRIC Front</label></p>' +
                                            '<p><input type="checkbox" class="rejectReason" value="2" id="NRICBack" /> <label for="NRICBack">NRIC Back</label></p>' +
                                            '<p><input type="checkbox" class="rejectReason" value="3" id="Selfie" /> <label for="Selfie">Selfie</label></p>' +
                                            '<p><input type="checkbox" class="rejectReason" value="4" id="Sign" /> <label for="Sign">Signature</label></p>' +
                                            '<p><input type="checkbox" class="rejectReason" value="5" id="Additional" /> <label for="Additional">Additional Documents</label></p>' +
                                            '<p><input type="checkbox" class="rejectReason" value="6" id="Supporting" /> <label for="Supporting">Supporting Documents</label></p>' +
                                            '</div>' +
                                            '</div>' +
                                            '</form>';
                                    }
                                    if (action == "ResendLink") {
                                        confirmationPopupTitle = 'Reason';
                                        content = '' +
                                            '<form action="" class="formName">' +
                                            '<div class="form-group">' +
                                            '<label>Enter reason here</label>' +
                                            '<br/>' +
                                            '<input type="text" class="form-control rejectReason" id="rejectReasonCustom" placeholder="Enter your reason" />' +
                                            '</div>' +
                                            '</form>';
                                    }
                                    if (isReason == undefined)
                                        isReason = 0;
                                    if (isReason == 1) {
                                        $.confirm({
                                            title: confirmationPopupTitle,
                                            content: content,
                                            buttons: {
                                                formSubmit: {
                                                    text: 'Submit',
                                                    btnClass: 'btn-blue',
                                                    action: function () {
                                                        var rejectReason;
                                                        if (action == "RequestUnclearFile") {
                                                            var rejectReasons = $.map(this.$content.find('.rejectReason:checked'), function (obj) {
                                                                return $(obj).val();
                                                            });
                                                            console.log(rejectReasons);
                                                            if (rejectReasons.length == 0) {
                                                                $.alert('Please select ' + confirmationPopupTitle.toLowerCase());
                                                                return false;
                                                            }
                                                            rejectReason = rejectReasons.join(',');
                                                            console.log(rejectReason);
                                                        }
                                                        else {
                                                            rejectReason = this.$content.find('.rejectReason').val();
                                                            if (!rejectReason) {
                                                                $.alert('Please provide a ' + confirmationPopupTitle.toLowerCase());
                                                                return false;
                                                            }
                                                            if (rejectReason == "Others")
                                                                rejectReason = this.$content.find('.rejectReasonCustom').val();
                                                        }
                                                        $('.loadingDivAdmin').show();
                                                        CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, rejectReason);
                                                    }
                                                },
                                                cancel: function () {
                                                    //close
                                                },
                                            },
                                            onContentReady: function () {
                                                // bind to events
                                                var jc = this;
                                                this.$content.find('form').on('submit', function (e) {
                                                    // if the user submits the form by pressing enter in the field.
                                                    e.preventDefault();
                                                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        var rejectReason = "";
                                        $('.loadingDivAdmin').show();
                                        CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, rejectReason);
                                    }

                                }

                                //$.alert('Confirmed!');
                            },
                            cancel: function () {
                                isExcecute = 0;
                                //$.alert('Canceled!');
                            }
                        }
                    });
                }
                //else if (action == 'Approve' && aspxURLNew.indexOf('Transactions.aspx') >= 0) {
                //    if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                //        $.alert('Please select one to approve.');
                //        return false;
                //    }
                //    var rejectReason = '';
                //    var othersScript = "javascript:if(this.value == 'Others'){document.getElementById('rejectReasonCustom').style.display='block';}else{document.getElementById('rejectReasonCustom').style.display='none'}";
                //    var noOfItems = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                //        return $(obj).data('itemcount');
                //    })[0];
                //    var Items = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                //        return $(obj).data('items');
                //    })[0].split(',');
                //    var orderType = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                //        return $(obj).data('type');
                //    })[0];
                //    var ref_no_string = '';

                //    if (orderType == 3) {
                //        for (var i = 0; i < noOfItems * 2; i++) {
                //            ref_no_string += '<label>' + Items[i] + ' (Out) </label><input type="text" class="form-control refNo" id="refNoCustom" placeholder="Enter transaction no" /><br/>';
                //            ref_no_string += '<label>' + Items[i + 1] + ' (In) </label><input type="text" class="form-control refNo" id="refNoCustom" placeholder="Enter transaction no" /><br/>';
                //            i++;
                //        }
                //    }
                //    else {
                //        for (var i = 0; i < noOfItems; i++) {
                //            ref_no_string += '<label>' + Items[i] + ' </label><input type="text" class="form-control refNo" id="refNoCustom" placeholder="Enter transaction no" /><br/>';
                //        }
                //    }


                //    var content = '' +
                //        '<form action="" class="formName">' +
                //        '<div class="form-group">' +
                //        ref_no_string +
                //        '</div>' +
                //        '</form>';
                //    $.confirm({
                //        title: 'Transaction no! (' + (orderType == 1 ? 'Buy' : (orderType == 2 ? 'SELL' : (orderType == '3' ? 'SWITCH' : ''))) + ')',
                //        content: content,
                //        buttons: {
                //            formSubmit: {
                //                text: 'Submit',
                //                btnClass: 'btn-blue',
                //                action: function () {
                //                    var istrue = true;
                //                    var refNos = this.$content.find('.refNo');
                //                    var refNoCount = this.$content.find('.refNo').length;
                //                    var allRefNos = [];
                //                    $(refNos).each(function (idx, ele) {
                //                        var refNo = $(ele).val();
                //                        if (!refNo) {
                //                            istrue = false;
                //                        }
                //                        else {
                //                            allRefNos[idx] = refNo;
                //                        }
                //                    });
                //                    if (istrue) {
                //                        $('.loadingDivAdmin').show();
                //                        CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, allRefNos.join());
                //                    }
                //                    else {
                //                        $.alert('Please provide transaction nos.');
                //                    }
                //                    return istrue;
                //                }
                //            },
                //            cancel: function () {
                //                //close
                //            },
                //        },
                //        onContentReady: function () {
                //            // bind to events
                //            var jc = this;
                //            this.$content.find('form').on('submit', function (e) {
                //                // if the user submits the form by pressing enter in the field.
                //                e.preventDefault();
                //                jc.$$formSubmit.trigger('click'); // reference the button and click it
                //            });
                //        }
                //    });
                //}
                //else if (action == 'Approve' && aspxURLNew.indexOf('AccountOpeningRequest.aspx') >= 0) {
                //    if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                //        $.alert('Please select one to approve.');
                //        return false;
                //    }
                //    var ref_no_string = '<label>MasterAccount No</label><input type="text" class="form-control refNo" id="refNoCustom" placeholder="Enter MasterAccount No" /><br/>';
                //    var content = '' +
                //        '<form action="" class="formName">' +
                //        '<div class="form-group">' +
                //        ref_no_string +
                //        '</div>' +
                //        '</form>';
                //    $.confirm({
                //        title: 'Approval Input',
                //        content: content,
                //        buttons: {
                //            formSubmit: {
                //                text: 'Submit',
                //                btnClass: 'btn-blue',
                //                action: function () {
                //                    var istrue = true;
                //                    var refNos = this.$content.find('.refNo');
                //                    var refNoCount = this.$content.find('.refNo').length;
                //                    var allRefNos = [];
                //                    $(refNos).each(function (idx, ele) {
                //                        var refNo = $(ele).val();
                //                        if (!refNo) {
                //                            istrue = false;
                //                        }
                //                        else {
                //                            allRefNos[idx] = refNo;
                //                        }
                //                    });
                //                    if (istrue) {
                //                        $('.loadingDivAdmin').show();
                //                        CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, allRefNos.join());
                //                    }
                //                    else {
                //                        $.alert('Please provide transaction nos.');
                //                    }
                //                    return istrue;
                //                }
                //            },
                //            cancel: function () {
                //                //close
                //            },
                //        },
                //        onContentReady: function () {
                //            // bind to events
                //            var jc = this;
                //            this.$content.find('form').on('submit', function (e) {
                //                // if the user submits the form by pressing enter in the field.
                //                e.preventDefault();
                //                jc.$$formSubmit.trigger('click'); // reference the button and click it
                //            });
                //        }
                //    });
                //}
                //else if (action == 'Approve' && aspxURLNew.indexOf('AgentRequests.aspx') >= 0) {
                //    if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                //        $.alert('Please select one to approve.');
                //        return false;
                //    }
                //    var ref_no_string = '<label>Agent Code</label><input type="text" class="form-control refNo" id="refNoCustom" placeholder="Enter Agent Code" /><br/>';
                //    var content = '' +
                //        '<form action="" class="formName">' +
                //        '<div class="form-group">' +
                //        ref_no_string +
                //        '</div>' +
                //        '</form>';
                //    $.confirm({
                //        title: 'Approval Input',
                //        content: content,
                //        buttons: {
                //            formSubmit: {
                //                text: 'Submit',
                //                btnClass: 'btn-blue',
                //                action: function () {
                //                    var istrue = true;
                //                    var refNos = this.$content.find('.refNo');
                //                    var refNoCount = this.$content.find('.refNo').length;
                //                    var allRefNos = [];
                //                    $(refNos).each(function (idx, ele) {
                //                        var refNo = $(ele).val();
                //                        if (!refNo) {
                //                            istrue = false;
                //                        }
                //                        else {
                //                            allRefNos[idx] = refNo;
                //                        }
                //                    });
                //                    if (istrue) {
                //                        $('.loadingDivAdmin').show();
                //                        CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, allRefNos.join());
                //                    }
                //                    else {
                //                        $.alert('Please provide agent code.');
                //                    }
                //                    return istrue;
                //                }
                //            },
                //            cancel: function () {
                //                //close
                //            },
                //        },
                //        onContentReady: function () {
                //            // bind to events
                //            var jc = this;
                //            this.$content.find('form').on('submit', function (e) {
                //                // if the user submits the form by pressing enter in the field.
                //                e.preventDefault();
                //                jc.$$formSubmit.trigger('click'); // reference the button and click it
                //            });
                //        }
                //    });
                //}
                else if (isReason == 1) {
                    if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                        $.alert('Please select one.');
                        return false;
                    }
                    if (reasonLabel == undefined)
                        reasonLabel = "Reason";
                    var ref_no_string = '<label>' + reasonLabel + '</label><textarea class="form-control refNo" rows="5" id="refNoCustom" placeholder="Enter ' + reasonLabel + '"></textarea><br/>';
                    var content = '' +
                        '<form action="" class="formName">' +
                        '<div class="form-group">' +
                        ref_no_string +
                        '</div>' +
                        '</form>';
                    $.confirm({
                        title: 'Input',
                        content: content,
                        buttons: {
                            formSubmit: {
                                text: 'Submit',
                                btnClass: 'btn-blue',
                                action: function () {
                                    var istrue = true;
                                    var refNos = this.$content.find('.refNo');
                                    var refNoCount = this.$content.find('.refNo').length;
                                    var allRefNos = [];
                                    $(refNos).each(function (idx, ele) {
                                        var refNo = $(ele).val();
                                        if (!refNo) {
                                            istrue = false;
                                        }
                                        else {
                                            allRefNos[idx] = refNo;
                                        }
                                    });
                                    if (istrue) {
                                        $('.loadingDivAdmin').show();
                                        CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, allRefNos.join());
                                    }
                                    else {
                                        $.alert('Please fill.');
                                    }
                                    return istrue;
                                }
                            },
                            cancel: function () {
                                //close
                            },
                        },
                        onContentReady: function () {
                            // bind to events
                            var jc = this;
                            if (reasonInput == 'date') {
                                $('#refNoCustom').datepicker({
                                    format: 'dd/mm/yyyy',
                                    pickTime: false,
                                    autoclose: true
                                });
                            }
                            this.$content.find('form').on('submit', function (e) {
                                // if the user submits the form by pressing enter in the field.
                                e.preventDefault();
                                jc.$$formSubmit.trigger('click'); // reference the button and click it
                            });
                        }
                    });
                }
                else {
                    isExcecute = 1;
                }
                if (isExcecute == 1) {
                    $('.loadingDivAdmin').show();
                    CommonActionFunction(aspxURLNew, action, actionMsg, actionStatus, '');
                }
            }

            $('#commonModal').find('.modal-footer button').removeClass('hide');
            $('#commonModalContent').html('<div class="text-center">Loading... Please wait</div>');

            if (action == 'Add') {
                $('body').attr('data-action-active', 'add');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');

                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('md');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url);
            }
            if (action == 'Edit') {
                $('body').attr('data-action-active', 'edit');
                if (checkedRowIds.length == 0 || checkedRowIds.length > 1) {
                    $.alert('Please select one to edit.');
                    return false;
                }
                else {
                    var title = $(this).attr('data-title');
                    var url = $(this).attr('data-url');
                    var modalSize = $(this).attr('data-modal-size');
                    $('#commonModal').find('.modal-dialog').removeClass('sm');
                    $('#commonModal').find('.modal-dialog').removeClass('md');
                    $('#commonModal').find('.modal-dialog').removeClass('lg');
                    $('#commonModal').modal('toggle');
                    if (modalSize == undefined) {
                        $('#commonModal').find('.modal-dialog').addClass('md');
                    }
                    else {
                        $('#commonModal').find('.modal-dialog').addClass(modalSize);
                    }
                    $('#commonModalTitle').html(title);
                    $('#commonModalContent').load(url + "?Id=" + checkedRowIds);
                }
            }
            if (action == 'Refresh') {
                $('#' + formID + ' .action-button[data-action="Search"]').click();
                //refreshContent();
            }
            if (action == 'AddIn') {
                $('body').attr('data-action-active', 'addin');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');

                var allDataAttributes = $(this).data();
                console.log(allDataAttributes);
                var cache = [];
                var jsonString = JSON.stringify(allDataAttributes, function (key, value) {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Duplicate reference found
                            try {
                                // If this value does not reference a parent it can be deduped
                                return JSON.parse(JSON.stringify(value));
                            } catch (error) {
                                // discard key if value cannot be deduped
                                return;
                            }
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = null; // Enable garbage collection
                //var jsonString = JSON.stringify(allDataAttributes);
                var jsonAttr = JSON.parse(jsonString);
                Object.keys(jsonAttr).forEach(function (key) {
                    if (key.indexOf('.') > -1)
                        delete jsonAttr[key];
                });
                var param = $.param(jsonAttr);

                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('md');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url + "?" + param);
            }
            if (action == 'EditIn') {
                $('body').attr('data-action-active', 'editin');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');

                var allDataAttributes = $(this).data();
                console.log(allDataAttributes);
                var cache = [];
                var jsonString = JSON.stringify(allDataAttributes, function (key, value) {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Duplicate reference found
                            try {
                                // If this value does not reference a parent it can be deduped
                                return JSON.parse(JSON.stringify(value));
                            } catch (error) {
                                // discard key if value cannot be deduped
                                return;
                            }
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = null; // Enable garbage collection
                //var jsonString = JSON.stringify(allDataAttributes);
                var jsonAttr = JSON.parse(jsonString);
                Object.keys(jsonAttr).forEach(function (key) {
                    if (key.indexOf('.') > -1)
                        delete jsonAttr[key];
                });
                var param = $.param(jsonAttr);

                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('md');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url + "?" + param);
            }

            //if (action == 'Refresh') {
            //    $('#' + formID + ' .action-button[data-action="Search"]').click();
            //    //refreshContent();
            //}
            //Only for Language
            if (action == 'UpdateJSON') {
                console.log('Update JSON');
                $('.loadingDivAdmin').show();
                $.ajax({
                    url: aspxURLNew + "/UpdateLanguageDirJSON",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: {},
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess) {
                            if (data.d.IsSuccess) {
                                refreshContent();
                                showSnackBar('Successfully updated');
                            }
                        }
                        else {
                            $('.loadingDivAdmin').hide();
                            showSnackBar(response.Message);
                            //showSnackBar('Updated');
                        }
                    }
                });
            }
            //Only for NAV Sync
            if (action == 'SyncNAV') {
                console.log('Sync NAV');
                $('.loadingDivAdmin').show();
                $.ajax({
                    url: aspxURLNew + "/SyncNAV",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: {},
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess) {
                            if (data.d.IsSuccess) {
                                refreshContent();
                                showSnackBar(data.d.Message);
                            }
                        }
                        else {
                            $('.loadingDivAdmin').hide();
                            showSnackBar(response.Message);
                            //showSnackBar('Updated');
                        }
                    }
                });
            }

            if (action == 'Search') {
                $('#' + formID + ' #IsNewSearch').val('1');
                setTimeout(loadContent, 100);
            }

            if (action == 'Filter') {
                $('#' + formID + ' #IsNewSearch').val('2');
                var filter = $(this).attr('data-filter');
                $('#' + formID + ' #FilterValue').val(filter);
                setTimeout(loadContent, 100);
            }
            if (action == 'View') {
                $('body').attr('data-action-active', 'view');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');
                var Id = $(this).attr('data-id');
                var allDataAttributes = $(this).data();
                console.log(allDataAttributes);
                var cache = [];
                var jsonString = JSON.stringify(allDataAttributes, function (key, value) {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Duplicate reference found
                            try {
                                // If this value does not reference a parent it can be deduped
                                return JSON.parse(JSON.stringify(value));
                            } catch (error) {
                                // discard key if value cannot be deduped
                                return;
                            }
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = null; // Enable garbage collection
                //var jsonString = JSON.stringify(allDataAttributes);
                var jsonAttr = JSON.parse(jsonString);
                Object.keys(jsonAttr).forEach(function (key) {
                    if (key.indexOf('.') > -1)
                        delete jsonAttr[key];
                });
                var param = $.param(jsonAttr);
                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('lg');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url + "?" + param);
                $('#commonModal').find('.modal-footer button').addClass('hide');
            }

            if (action == "Upload") {
                $('body').attr('data-action-active', 'upload');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');

                if ($('.statementStatus') != undefined)
                    $('.statementStatus').addClass('hide');

                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('md');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url + "?Id=" + checkedRowIds);
            }
            if (action == 'Download') {
                var filter = $('#' + formID + ' #FilterValue').val();
                if (aspxURLNew.indexOf('Transactions.aspx') >= 0 || aspxURLNew.indexOf('Payments.aspx') >= 0 || aspxURLNew.indexOf('RegularSavingPlans.aspx') >= 0) {
                    var FromDate = $('#' + formID + ' #FromDate').val();
                    var ToDate = $('#' + formID + ' #ToDate').val();
                    filter = filter + "," + FromDate + "," + ToDate;
                }
                if (aspxURLNew.indexOf('Enrollments.aspx') >= 0) {

                    if ($('#ddlCourseList').val() != "") {
                        var courseID = $('#ddlCourseList').val();
                        filter = filter + "," + courseID;
                    }
                }
                $('.loadingDivAdmin').show();
                $.ajax({
                    url: aspxURLNew + "/Download",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { filter: JSON.stringify(filter) },
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess) {
                            if (data.d.IsSuccess) {
                                var fileBase64 = data.d.Data;
                                showSnackBar(response.Message);
                                //var bindata = window.btoa(fileBase64);
                                //console.log(bindata);
                                var link = document.createElement('a');
                                link.download = (data.d.Message == null ? "download" : data.d.Message);
                                link.href = fileBase64;
                                link.click();

                                //window.location.href = fileBase64;
                                //$(elementA).remove();
                                //window.location.href = fileName;
                                setTimeout(function () {
                                    showSnackBar("Downloaded");
                                }, 1000);
                                $('.loadingDivAdmin').hide();
                            }
                            else {
                                showSnackBar(response.Message);
                                $('.loadingDivAdmin').hide();
                            }
                        }
                        else {
                            showSnackBar(response.Message);
                            $('.loadingDivAdmin').hide();
                        }

                    }
                });
            }

            if (action == "Generate") {
                $('body').attr('data-action-active', 'generate');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');

                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('md');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url);
            }

            $('#btnCommonModalSubmit').html('Save');

            if (action == 'ConfigureFund') {
                $('body').attr('data-action-active', 'ConfigureFund');
                var title = $(this).attr('data-title');
                var url = $(this).attr('data-url');

                $('#commonModal').find('.modal-dialog').removeClass('sm');
                $('#commonModal').find('.modal-dialog').removeClass('md');
                $('#commonModal').find('.modal-dialog').removeClass('lg');
                $('#commonModal').modal('toggle');
                $('#commonModal').find('.modal-dialog').addClass('sm');
                $('#commonModalTitle').html(title);
                $('#commonModalContent').load(url);
                $('#btnCommonModalSubmit').html('Submit');
            }

            if (action == 'Delete') {
                if (checkedRowIds.length == 0) {
                    $.alert('Please select records.');
                    return false;
                }
                $('.loadingDivAdmin').show();
                $.ajax({
                    url: aspxURLNew + "/Delete",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { filePaths: JSON.stringify(checkedRowIds) },
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess) {
                            if (data.d.IsSuccess) {
                                var fileBase64 = data.d.Data;
                                showSnackBar(response.Message);
                                setTimeout(loadContent, 100);
                                $('.loadingDivAdmin').hide();
                            }
                            else {
                                showSnackBar(response.Message);
                                $('.loadingDivAdmin').hide();
                            }
                        }
                        else {
                            showSnackBar(response.Message);
                            $('.loadingDivAdmin').hide();
                        }
                    }
                });
            }
        });



        $('#' + formID + ' td.icheck').unbind('click');
        $('#' + formID + ' td.icheck').click(function () {
            var checkbox = $(this).find('input:checkbox');
            $(checkbox).iCheck('toggle');
        });

        $('#btnCommonModalSubmit').unbind('click');
        $('#btnCommonModalSubmit').click(function () {
            var tabSelectedLi = $('li.tabs-selected');
            var selectedTabIndex = $('#tt ul.tabs li').index(tabSelectedLi);
            var selectedPanel = $('.panel.panel-htop')[selectedTabIndex];
            var formID = $(selectedPanel).find('form').attr('id');
            var aspxURLNew = $(tabSelectedLi).find('.data-url').text();
            var dataActionActive = $('body').attr('data-action-active');
            var modalForm = $('#commonModalContent').find('#FormId');
            var modalFormId = $(modalForm).attr('data-value');
            //var data = $('#' + modalFormId).serializeArray();
            var data = $('[data-value=' + modalFormId + '] :input').serializeArray();
            data = data.filter(function (item) {
                return item.name.indexOf('_') === -1;
            });
            var jsonObj = {};
            for (var key in data) {
                jsonObj[data[key].name] = data[key].value;
            }
            var formData = {
                obj: JSON.stringify(jsonObj)
            };
            //if (modalFormId == "addLanguageDirForm") {
            //    formData = {
            //        languageDir: JSON.stringify(jsonObj)
            //    };
            //}
            //else if (modalFormId == "addAnnouncementForm" || modalFormId == "addBannerForm") {
            //    formData = {
            //        siteContent: JSON.stringify(jsonObj)
            //    };
            //}
            if (dataActionActive === "add" || dataActionActive === "edit") {
                $('.loadingDivAdmin').show();
                $.ajax({
                    url: aspxURLNew + "/Add",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: formData,
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess != undefined) {
                            if (response.IsSuccess) {
                                refreshContent();
                                var checkbox = $('#' + formID).find('table tbody tr td.icheck .checkRow');
                                $(checkbox).iCheck('uncheck');
                                $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                                $('#' + modalFormId).find('input, select, textarea').val('');
                                if ($('#' + modalFormId).find('[name="ID"]').length > 0)
                                    $('#' + modalFormId).find('[name="ID"]').val('0');
                                if ($('#' + modalFormId).find('[name="Status"]').length > 0)
                                    $('#' + modalFormId).find('[name="Status"]').val('1');
                                $('#commonModal').modal('toggle');
                                $('body').removeAttr('data-action-active');
                            }
                            else {
                                $('.loadingDivAdmin').hide();
                            }
                            showSnackBar(response.Message);
                        }
                        else {
                            if (response) {

                                var checkbox = $('#' + formID).find('table tbody tr td.icheck .checkRow');
                                $(checkbox).iCheck('uncheck');
                                $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                                $('#' + modalFormId).find('input, select, textarea').val('');
                                if ($('#' + modalFormId).find('[name="ID"]').length > 0)
                                    $('#' + modalFormId).find('[name="ID"]').val('0');
                                if ($('#' + modalFormId).find('[name="Status"]').length > 0)
                                    $('#' + modalFormId).find('[name="Status"]').val('1');
                                if (modalFormId == "addLanguageDirForm") {
                                    $('#' + modalFormId).find('[name="TextId"]').val('0');
                                }
                                $('#commonModal').modal('toggle');
                                $('body').removeAttr('data-action-active');
                                $('body').removeAttr('data-action-active');
                                refreshContent();
                                if (dataActionActive == 'add')
                                    showSnackBar('Successfully added');
                                if (dataActionActive == 'edit')
                                    showSnackBar('Successfully updated');
                                if (dataActionActive == 'addin' || dataActionActive == 'editin') {
                                    showSnackBar('Successfully updated');
                                    setTimeout(loadContent, 100);
                                }
                                $('.loadingDivAdmin').hide();
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        //showSnackBar(xhr.Message);
                        showSnackBar('Please check that you have entered valid information!');
                        $('.loadingDivAdmin').hide();
                    }
                });
            }
            else if (dataActionActive == "upload") {

                var fileElement = $('#statements')[0];
                var statements = fileElement.files;
                var statementsBase64Strings = [];
                if (statements.length > 0) {
                    $('.loadingDivAdmin').show();
                    Object.keys(statements).forEach(i => {
                        var file = statements[i];
                        var reader = new FileReader();
                        var iSize = (file.size / 1024);
                        if (iSize / 1024 > 1 || iSize / 1024 < 1) {
                            if (((iSize / 1024) / 1024) < 1) {
                                iSize = (Math.round((iSize / 1024) * 100) / 100)
                                if (iSize <= 10) {

                                    //reader.readAsBinaryString(file);
                                    reader.readAsDataURL(file);
                                    reader.onload = function () {
                                        //server call for uploading or reading the files one-by-one
                                        //by using 'reader.result' or 'file'
                                        console.log(reader.result);
                                        console.log(file.name);
                                        var obj = { Base64String: reader.result, Name: file.name }
                                        statementsBase64Strings.push(obj);
                                        if (statements.length == statementsBase64Strings.length) {
                                            $.ajax({
                                                url: aspxURLNew + "/Upload",
                                                contentType: 'application/json; charset=utf-8',
                                                type: "POST",
                                                dataType: "JSON",
                                                data: "{ 'statements': " + JSON.stringify(statementsBase64Strings) + " }",
                                                success: function (data) {
                                                    var response = data.d;
                                                    console.log(response);
                                                    if (response.IsSuccess != undefined) {
                                                        if (response.IsSuccess) {
                                                            refreshContent();
                                                            var checkbox = $('#' + formID).find('table tbody tr td.icheck .checkRow');
                                                            $(checkbox).iCheck('uncheck');
                                                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                                                            $('#' + modalFormId).find('input, select, textarea').val('');
                                                            if ($('#' + modalFormId).find('[name="ID"]').length > 0)
                                                                $('#' + modalFormId).find('[name="ID"]').val('0');
                                                            if ($('#' + modalFormId).find('[name="Status"]').length > 0)
                                                                $('#' + modalFormId).find('[name="Status"]').val('1');
                                                            if ($('#statementUploadResult') != undefined) {
                                                                $('#statementUploadResult').html(response.Message);
                                                                $('.loadingDivAdmin').hide();
                                                                showSnackBar("Success!");
                                                            }
                                                            //$('#commonModal').modal('toggle');
                                                            //$('body').removeAttr('data-action-active');
                                                            $('.loadingDivAdmin').hide();
                                                            showSnackBar(response.Message);
                                                        }
                                                        else {
                                                            if ($('#statementUploadResult') != undefined) {
                                                                $('.statementStatus').removeClass('hide');
                                                                showSnackBar("Showing the status");
                                                                $('#statementUploadResult').html(response.Message);
                                                                $('.loadingDivAdmin').hide();
                                                            }
                                                            else {
                                                                $('.loadingDivAdmin').hide();
                                                                showSnackBar(response.Message);
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        if (response) {

                                                            var checkbox = $('#' + formID).find('table tbody tr td.icheck .checkRow');
                                                            $(checkbox).iCheck('uncheck');
                                                            $('#' + formID + ' .checkRowAll').iCheck('uncheck');
                                                            $('#' + modalFormId).find('input, select, textarea').val('');
                                                            if ($('#' + modalFormId).find('[name="ID"]').length > 0)
                                                                $('#' + modalFormId).find('[name="ID"]').val('0');
                                                            if ($('#' + modalFormId).find('[name="Status"]').length > 0)
                                                                $('#' + modalFormId).find('[name="Status"]').val('1');
                                                            if (modalFormId == "addLanguageDirForm") {
                                                                $('#' + modalFormId).find('[name="TextId"]').val('0');
                                                            }
                                                            $('#commonModal').modal('toggle');
                                                            $('body').removeAttr('data-action-active');
                                                            refreshContent();
                                                            if (dataActionActive == 'add')
                                                                showSnackBar('Successfully added');
                                                            if (dataActionActive == 'edit')
                                                                showSnackBar('Successfully updated');
                                                            if (dataActionActive == 'addin' || dataActionActive == 'editin') {
                                                                showSnackBar('Successfully updated');
                                                                setTimeout(loadContent, 100);
                                                            }
                                                        }
                                                        $('.loadingDivAdmin').hide();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                                else {
                                    showSnackBar('Uploaded file should not exceed 10MB');
                                    $('.loadingDivAdmin').hide();
                                }
                            }
                            else {
                                showSnackBar('Uploaded file should not exceed 10MB');
                                $('.loadingDivAdmin').hide();
                            }
                        }
                    })
                }
                else {
                    showSnackBar('No file(s) uploaded');
                    $('.loadingDivAdmin').hide();
                }
            }
            else {
                $('.loadingDivAdmin').show();
                $.ajax({
                    url: aspxURLNew + "/" + dataActionActive,
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: formData,
                    success: function (data) {
                        var response = data.d;
                        console.log(response);
                        if (response.IsSuccess != undefined) {
                            if (response.IsSuccess) {
                                refreshContent();
                                $('#commonModal').modal('toggle');
                                $('body').removeAttr('data-action-active');
                            }
                            else {
                                $('.loadingDivAdmin').hide();
                            }
                            showSnackBar(response.Message);
                        }
                        else {
                            if (response) {
                                $('#commonModal').modal('toggle');
                                $('body').removeAttr('data-action-active');
                                refreshContent();
                                $('.loadingDivAdmin').hide();
                            }
                        }
                    }
                });
            }
            return false;
        });

        //Agent Promote/Demote action event
        //function AgentPDFunction(action) {
        //    var checkedRowIds = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
        //        var checkedRowContent = $(this).find('.checkedAgentCode').value();
        //        return checkedRowContent;
        //    });
        //    console.log(checkedRowIds);
        //    var tabSelectedLi = $('li.tabs-selected');
        //    var selectedTabIndex = $('#tt ul.tabs li').index(tabSelectedLi);
        //    var selectedPanel = $('.panel.panel-htop')[selectedTabIndex];
        //    var formID = $(selectedPanel).find('form').attr('id');
        //    var modalForm = $('#commonModalContent').find('#FormId');
        //    var modalFormId = $(modalForm).attr('data-value');
        //    //var data = $('#' + modalFormId).serializeArray();
        //    var data = $(checkedRowIds).serializeArray();
        //    data = data.filter(function (item) {
        //        return item.name.indexOf('_') === -1;
        //    });
        //    var jsonObj = {};
        //    for (var key in data) {
        //        jsonObj[data[key].name] = data[key].value;
        //    }
        //    var formData = {
        //        agentCode: JSON.stringify(checkedRowIds), action: JSON.stringify(action)
        //    };
        //    $.ajax({
        //        url: "AgentRankListing.aspx/RankAction",
        //        contentType: 'application/json; charset=utf-8',
        //        type: "GET",
        //        dataType: "JSON",
        //        data: formData,
        //        success: function (data) {
        //            if (action == 'Promote') {
        //                if (data.d.IsSuccess != undefined) {
        //                    showSnackBar(data.d.Message);
        //                    setTimeout(loadContent, 100);
        //                }
        //                else {
        //                    showSnackBar(actionMsg);
        //                    $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
        //                        $(obj).parents('td').find('.row-status').html("<span class='label label-success'>" + actionStatus + "</span>");
        //                        $(obj).iCheck('uncheck');
        //                    });
        //                    $('#' + formID + ' .checkRowAll').iCheck('uncheck');
        //                    $('.loadingDivAdmin').hide();
        //                }
        //            }
        //            else if (action == 'Demote') {
        //                if (data.d.IsSuccess != undefined) {
        //                    showSnackBar(data.d.Message);
        //                    setTimeout(loadContent, 100);
        //                }
        //                else {
        //                    showSnackBar(actionMsg);
        //                    $.each($('#' + formID + ' input.checkRow:checked'), function (idx, obj) {
        //                        $(obj).parents('td').find('.row-status').html("<span class='label label-danger'>" + actionStatus + "</span>");
        //                        $(obj).iCheck('uncheck');
        //                    });
        //                    $('#' + formID + ' .checkRowAll').iCheck('uncheck');
        //                    $('.loadingDivAdmin').hide();
        //                }
        //            }
        //            else {
        //                if (data.d.IsSuccess != undefined) {
        //                    showSnackBar(data.d.Message);
        //                    setTimeout(loadContent, 100);
        //                }
        //                else
        //                    showSnackBar(actionMsg);
        //                setTimeout(loadContent, 100);
        //            }
        //        }
        //    });
        //}

        $('#btnCommonInnerModalSubmit').unbind('click');
        $('#btnCommonInnerModalSubmit').click(function () {
            var tabSelectedLi = $('li.tabs-selected');
            var selectedTabIndex = $('#tt ul.tabs li').index(tabSelectedLi);
            var selectedPanel = $('.panel.panel-htop')[selectedTabIndex];
            var formID = $(selectedPanel).find('form').attr('id');
            var aspxURLNew = $(tabSelectedLi).find('.data-url').text();
            var modalForm = $('#innerMediumModalContent').find('#FormId');
            var modalFormId = $(modalForm).attr('data-value');
            //var data = $('#' + modalFormId).serializeArray();
            var data = $('[data-value=' + modalFormId + '] :input').serializeArray();
            data = data.filter(function (item) {
                return item.name.indexOf('_') === -1;
            });
            var jsonObj = {};
            for (var key in data) {
                jsonObj[data[key].name] = data[key].value;
            }
            console.log(jsonObj);
            var formData = {
                obj: JSON.stringify(jsonObj)
            };
            $('.loadingDivAdmin').show();
            $.ajax({
                url: aspxURLNew + "/Upload",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: formData,
                success: function (data) {
                    var response = data.d;
                    if (response.IsSuccess) {
                        $('#innerMediumModal').modal('toggle');
                        //LOAD
                        var checkedRowIds = $.map($('#' + formID + ' input.checkRow:checked'), function (obj, idx) {
                            return $(obj).val();
                        });
                        var url = $('[data-action="Edit"]').attr('data-url');
                        $('#commonModalContent').load(url + "?Id=" + checkedRowIds);
                        showSnackBar(response.Message);
                    }
                    else {
                        showSnackBar(response.Message);
                    }
                    $('.loadingDivAdmin').hide();
                }
            });
            return false;
        });

        $('#' + formID + ' .popovers').popover();
        $('#' + formID + ' #IsNewSearch').val('0');

        $('#' + formID).unbind('keydown');
        $('#' + formID).keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $('#' + formID + ' .action-button[data-action="Search"]').click();
                return false;
            }
        });

        $('#' + formID + ' .search-features select').unbind('change');
        $('#' + formID + ' .search-features select').change(function () {
            $('#' + formID + ' .action-button[data-action="Search"]').click();
        });

        $('#' + formID + ' [data-toggle=confirmation]').unbind('confirmation');


    }, 100);
});
