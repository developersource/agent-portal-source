﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("instructors")]
    public class Instructor
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("email_id"), Display("EmailId")]
        public String EmailId { get; set; }
        [Database("mobile_number"), Display("MobileNumber")]
        public String MobileNumber { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32 UpdatedBy { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }

    public class InstructorCustom
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String EmailId { get; set; }
        public String MobileNumber { get; set; }

    }
}
