﻿using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DiOTP.Service
{
    public class GenerateStatementService
    {
        public static List<string> mcList = new List<string>();

        private static readonly Lazy<IUserStatementRepo> lazyIUserStatementRepo = new Lazy<IUserStatementRepo>(() => new UserStatementRepo());
        public static IUserStatementRepo IUserStatementRepo { get { return lazyIUserStatementRepo.Value; } }



        public static List<SummaryPortfolio> GetAll(DateTime startDate, DateTime endDate, string accountNo)
        {
            return GenerateStatementRepo.GetAll(startDate, endDate, accountNo);
        }
        public static List<TransactionByDate> GetAllTransaction(DateTime startDate, DateTime endDate, string accountNo)
        {
            return GenerateStatementRepo.GetAllTransaction(startDate, endDate, accountNo);
        }
        public static List<TransactionByDate> GetLastMonthSum(DateTime lastDate, string accountNo)
        {
            return GenerateStatementRepo.GetLastMonthSum(lastDate, accountNo);
        }

        public static List<SummaryPortfolio> GetAll(DateTime startDate, string accountNo)
        {
            return GenerateStatementRepo.GetAll(startDate, accountNo);
        }
        public static void Generate()
        {
            bool isYearly = false;
            bool isMonthly = false;
            var today = DateTime.Today;
            var currentYear = new DateTime(today.Year, 1, 1);
            var currentMonth = new DateTime(today.Year, today.Month, 1);
            var firstdForMonth = currentMonth.AddMonths(-1); //first day of this month
            var lastdForMonth = currentMonth.AddDays(-1); //last day of this month
            var firstdForYear = new DateTime(today.Year - 1, 1, 1);
            var lastdForYear = new DateTime(today.Year - 1, 12, 31);
            if (DateTime.Now.Date == currentYear)
            {
                currentMonth = new DateTime(today.Year, today.Month, 1);
                firstdForMonth = currentMonth.AddMonths(-1); //first day of this month
                lastdForMonth = currentMonth.AddDays(-1); //last day of this month
                firstdForYear = new DateTime(today.Year - 1, 1, 1);
                lastdForYear = new DateTime(today.Year - 1, 12, 31);
                if (DateTime.Now.Date == currentMonth)
                {
                    string folderPath = Path.GetFullPath("../../../DiOTP.WebApp/") + DateTime.Now.ToString("MM-yyyy");
                    try
                    {
                        if (!(Directory.Exists(folderPath)))
                            Directory.CreateDirectory(folderPath);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    string folderPath2 = Path.GetFullPath("../../../DiOTP.WebApp/") + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport";
                    try
                    {
                        if (!(Directory.Exists(folderPath2)))
                            Directory.CreateDirectory(folderPath2);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    isYearly = true;
                    isMonthly = true;
                }
            }
            {
                var month3 = new DateTime(today.Year, today.Month, 1);
                var first = month3.AddMonths(-1); //first day of this month
                var last = month3.AddDays(-1); //last day of this month
                if (DateTime.Now.Date == month3)
                {
                    string folderPath = Path.GetFullPath("../../../DiOTP.WebApp/") + DateTime.Now.ToString("MM-yyyy");

                    try
                    {
                        if (!(Directory.Exists(folderPath)))
                            Directory.CreateDirectory(folderPath);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    isMonthly = true;
                }
            }

            List<UserAccount> allUA = UserAccountService.GetAll();

            foreach (UserAccount ua in allUA)
            {
                if (isYearly)
                    GenerateReport(ua.AccountNo, firstdForYear, lastdForYear, 3);
                if (isMonthly)
                    GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 1);
                mcList.Add(ua.AccountNo);
            }
        }

        public static void GenerateMonthlyReport(DateTime firstdForMonth, DateTime lastdForMonth)
        {
            //Response responseUAs = GenericService.PullData<UserAccount>(" account_no in ('36708', '23351', '36707', '23354') ", 0, 0, false, null);
            Response responseUAs = GenericService.PullData<UserAccount>(" status=1  ", 0, 0, false, null);
            //List<UserAccount> allUA = UserAccountService.GetAll();
            if (responseUAs.IsSuccess && responseUAs.Data != null)
            {
                List<UserAccount> allUA = (List<UserAccount>)responseUAs.Data;

                foreach (UserAccount ua in allUA)
                    GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 1);
            }
        }

        public static void GenerateYearlyReport(DateTime firstdForMonth, DateTime lastdForMonth)
        {
            List<UserAccount> allUA = UserAccountService.GetAll();
            foreach (UserAccount ua in allUA)
                GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 3);
        }


        public static void GenerateReport(string accountNo, DateTime f, DateTime l, int statementCategory)
        {

            UserStatements us = new UserStatements();
            UserAccount ua = new UserAccount();
            ua = UserAccountService.GetByid(accountNo);

            string time1 = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime currenttime1 = DateTime.Parse(time1);

            string time2 = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime currenttime2 = DateTime.Parse(time2);

            int userAID = ua.Id;
            int userID = ua.UserId;

            List<UserStatement> us1 = new List<UserStatement>();
            //us1 = UserStatementService.GetAllUS();
            if (statementCategory == 1)
            {
                Response response = IUserStatementRepo.GetDataByFilter(" user_account_id=" + ua.Id + " and MONTH(created_date)=" + currenttime1.Month + " and user_statement_category_id=" + statementCategory + " ", 0, 0, false);
                us1 = (List<UserStatement>)response.Data;
            }
            else if (statementCategory == 3)
            {
                Response response = IUserStatementRepo.GetDataByFilter(" user_account_id=" + ua.Id + " and YEAR(created_date)=" + currenttime1.Year + " and user_statement_category_id=" + statementCategory + " ", 0, 0, false);
                us1 = (List<UserStatement>)response.Data;
            }
            string pdfDate = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
            try
            {
                if (us1.Count == 0 && (statementCategory == 1 || statementCategory == 3))
                {
                    string StatementsPath = ConfigurationManager.AppSettings["StatementsPath"].ToString();
                    string path = "";
                    //Monthly
                    if (statementCategory == 1)
                        path = Path.GetFullPath(StatementsPath) + "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //path = Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //Yearly
                    else if (statementCategory == 3)
                        path = Path.GetFullPath(StatementsPath) + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //path = Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";

                    if (!Directory.Exists(Path.GetFullPath(StatementsPath) + "/" + DateTime.Now.ToString("MM-yyyy")))
                        Directory.CreateDirectory(Path.GetFullPath(StatementsPath) + "/" + DateTime.Now.ToString("MM-yyyy"));

                    if (!Directory.Exists(Path.GetFullPath(StatementsPath) + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport"))
                        Directory.CreateDirectory(Path.GetFullPath(StatementsPath) + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport");

                    //if (!Directory.Exists(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy")))
                    //{
                    //    Directory.CreateDirectory(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy"));
                    //}
                    //if (!Directory.Exists(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport"))
                    //{
                    //    Directory.CreateDirectory(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport");
                    //}
                    FileStream fs = new FileStream(path, FileMode.CreateNew);
                    Document doc = new Document(PageSize.A4, 36, 36, 36, 135);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(doc, fs);

                    doc.Open();

                    PageEvent e = new PageEvent();
                    e.AddLogo(pdfWriter, doc);
                    //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                    //e.AddHeader(pdfWriter, doc, accountNo);
                    pdfWriter.PageEvent = e;

                    //table style
                    Font fontText = FontFactory.GetFont("ARIAL", 9);
                    Font fontTitle = FontFactory.GetFont("ARIAL", 9, BaseColor.WHITE);
                    BaseColor headBackgroundColor = BaseColor.DARK_GRAY;

                    // table title - Summary Portfolio
                    PdfPTable tableSummaryPortfolio = new PdfPTable(5);
                    tableSummaryPortfolio.WidthPercentage = 100;
                    int[] intTblWidth = { 30, 10, 20, 15, 15 };
                    tableSummaryPortfolio.SetWidths(intTblWidth);

                    tableSummaryPortfolio.Rows.Add(new PdfPRow(new PdfPCell[] {
                new PdfPCell(new Phrase("Trust Name", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Trust Code", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Units Held", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Redemption Price (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Current Market Value (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER}
            }));

                    //table content -  Summary Portfolio 
                    decimal totalCurrentMarketValue = 0;
                    int i = 0;
                    List<SummaryPortfolio> summaryPortfolioList = new List<SummaryPortfolio>();
                    //summaryPortfolioList = GenerateStatementService.GetAll(l, accountNo);

                    List<UtmcCompositionalTransaction> utmcCompositionalTransactions = GetCompositionalTransactionByHolderNo(accountNo, f.ToString("yyyy-MM-dd"), l.ToString("yyyy-MM-dd"));
                    utmcCompositionalTransactions = utmcCompositionalTransactions.OrderBy(y => y.DateOfTransaction.Value).ThenBy(y => y.IpdUniqueTransactionId).ToList();
                    utmcCompositionalTransactions.Where(y => y.ReportDate.Value >= l).GroupBy(y => y.IpdFundCode).Select(grp => grp.ToList()).ToList().ForEach(y =>
                    {
                        summaryPortfolioList.Add(new SummaryPortfolio
                        {
                            Fund_Name = "",
                            Fund_Code = y.FirstOrDefault().IpdFundCode,
                            Net_Cumulative_Closing_Balance_Units = y.Sum(z => z.Units.Value),
                            Market_Price_NAV = y.Sum(z => z.Units.Value * z.TransPR),
                            Current_Market_Value = y.Sum(z => z.Units.Value * z.TransPR),
                        });
                    });

                    List<string> fundCodeList = new List<string>();

                    foreach (SummaryPortfolio summaryPortfolio in summaryPortfolioList)
                    {
                        BaseColor contentBackgroundColor = i % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                        PdfPCell[] cells = new PdfPCell[] {
                    new PdfPCell(new Phrase(summaryPortfolio.Fund_Name, fontText)){ BackgroundColor = contentBackgroundColor },
                    new PdfPCell(new Phrase(summaryPortfolio.Fund_Code, fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                    new PdfPCell(new Phrase(summaryPortfolio.Net_Cumulative_Closing_Balance_Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                    new PdfPCell(new Phrase(summaryPortfolio.Market_Price_NAV.ToString("#,##0.000000000"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                    new PdfPCell(new Phrase(summaryPortfolio.Current_Market_Value.ToString("#,##0.00"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},


                };
                        fundCodeList.Add(summaryPortfolio.Fund_Code);
                        tableSummaryPortfolio.Rows.Add(new PdfPRow(cells));
                        i++;
                        totalCurrentMarketValue += summaryPortfolio.Current_Market_Value;
                    }

                    Font fontTextBold = FontFactory.GetFont("ARIAL", 9);
                    BaseColor contentBackgroundColor2 = i % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                    tableSummaryPortfolio.Rows.Add(new PdfPRow(new PdfPCell[] {
                new PdfPCell(new Phrase("Total Portfolio Value (RM)", fontTextBold)) { BackgroundColor = contentBackgroundColor2, Colspan=4},
                new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                new PdfPCell(new Phrase(totalCurrentMarketValue.ToString("#,##0.00"), fontTextBold)) { BackgroundColor = contentBackgroundColor2, HorizontalAlignment = Element.ALIGN_RIGHT}

        }));

                    // table title - Transaction
                    PdfPTable tableTransaction = new PdfPTable(8);
                    tableTransaction.WidthPercentage = 100;
                    int[] intTblWidth2 = { 12, 12, 10, 13, 14, 14, 12, 11 };
                    tableTransaction.SetWidths(intTblWidth2);
                    tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                new PdfPCell(new Phrase("Trans Date", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Processed Date", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Trust Code", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Trans No", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Number of units", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Unit Cost (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Trans Amount (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Servicing Agent", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER }
            }));

                    //table content - Transaction
                    int x = 0;
                    int count = 1;
                    decimal totalUnits = 0;
                    int countTransaction = 0;

                    List<TransactionByDate> balanceUnitsList = new List<TransactionByDate>();
                    //balanceUnitsList = GenerateStatementService.GetLastMonthSum(l, accountNo);

                    List<TransactionByDate> transactionByDateList = new List<TransactionByDate>();
                    //transactionByDateList = GenerateStatementService.GetAllTransaction(f, l, accountNo);

                    utmcCompositionalTransactions
                        //.Where(y => y.ReportDate.Value.ToString("yyyy-MM-dd") == l.ToString("yyyy-MM-dd"))
                        .GroupBy(y => y.IpdFundCode)
                        .Select(grp => grp.ToList())
                        .ToList().ForEach(y =>
                        {
                            balanceUnitsList.Add(new TransactionByDate
                            {
                                Sum_Units = y.Sum(z => z.Units.Value),
                                Report_Date = y.FirstOrDefault().ReportDate.Value,
                                Fund_Code = y.FirstOrDefault().IpdFundCode
                            });
                        });
                    utmcCompositionalTransactions
                        .ToList().ForEach(y =>
                        {
                            transactionByDateList.Add(new TransactionByDate
                            {
                                Date_Of_Transaction = y.DateOfTransaction.Value,
                                Date_Of_Settlement = y.DateOfSettlement.Value,
                                Fund_Code = y.IpdFundCode,
                                Fund_Name = "",
                                Transaction_Code = y.TransactionCode,
                                Transaction_Name = "",
                                IPD_Unique_Transaction_ID = y.IpdUniqueTransactionId,
                                Units = y.Units.Value,
                                Unit_Cost_RM = y.GrossAmountRm.Value == 0 ? 0 : y.Units.Value / y.GrossAmountRm.Value,
                                Gross_Amount_RM = y.GrossAmountRm.Value,
                                Report_Date = y.ReportDate.Value
                            });
                        });

                    foreach (TransactionByDate balanceUnits in balanceUnitsList)
                    {
                        if (balanceUnits != null)
                        {
                            BaseColor contentBackgroundColorLightGrey = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                            PdfPCell[] cells = new PdfPCell[] {
                        new PdfPCell(new Phrase(balanceUnits.Report_Date.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase("B/f Balance", fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(balanceUnits.Fund_Code.ToString(), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey},
                        new PdfPCell(new Phrase(balanceUnits.Sum_Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_RIGHT },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey }
                    };
                            totalUnits = balanceUnits.Sum_Units;
                            tableTransaction.Rows.Add(new PdfPRow(cells));
                            x++;

                            List<TransactionByDate> sorted = new List<TransactionByDate>();
                            sorted = transactionByDateList.Where(X => X.Fund_Code == balanceUnits.Fund_Code).ToList();

                            foreach (TransactionByDate transactionByDate in sorted)
                            {
                                if (transactionByDate != null)
                                {
                                    BaseColor contentBackgroundColorLightGrey2 = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                                    countTransaction++;
                                    PdfPCell[] cells2 = new PdfPCell[] {
                        new PdfPCell(new Phrase(transactionByDate.Date_Of_Transaction.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(transactionByDate.Date_Of_Settlement.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(transactionByDate.Fund_Code.ToString(), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase((transactionByDate.Transaction_Code + transactionByDate.IPD_Unique_Transaction_ID), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(transactionByDate.Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT },
                        new PdfPCell(new Phrase(transactionByDate.Unit_Cost_RM.ToString("#,##0.000000000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT  },
                        new PdfPCell(new Phrase(transactionByDate.Gross_Amount_RM.ToString("#,##0.00"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT  },
                        new PdfPCell(new Phrase("000000", fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_CENTER }
                            };
                                    tableTransaction.Rows.Add(new PdfPRow(cells2));
                                    totalUnits += transactionByDate.Units;
                                    x++;
                                }
                            }

                            if (sorted.Count != 0)
                            {
                                BaseColor contentBackgroundColorGrey = new BaseColor(220, 220, 220);
                                BaseColor contentBackgroundColor = BaseColor.WHITE;
                                tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, BorderWidthRight=0, BorderWidthLeft=0},
                        new PdfPCell(new Phrase("No of transaction: " + countTransaction, fontTextBold)) { BackgroundColor = contentBackgroundColorGrey, Colspan=2, BorderWidthRight=0, BorderWidthTop=0, BorderWidthLeft=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, Border=0, BorderWidthTop=0},
                        new PdfPCell(new Phrase(totalUnits.ToString("#,##0.0000"), fontTextBold)) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, HorizontalAlignment = Element.ALIGN_RIGHT},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0}

                    }));

                                BaseColor contentBackgroundColor3 = BaseColor.WHITE;
                                tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0}
                     }));

                            }
                            count++;
                            countTransaction = 0;
                            totalUnits = 0;
                        }
                    }


                    tableTransaction.DeleteLastRow();

                    tableSummaryPortfolio.SpacingBefore = 5f;
                    tableSummaryPortfolio.SpacingAfter = 12.5f;
                    tableTransaction.SpacingBefore = 5f;
                    tableTransaction.SpacingAfter = 12.5f;

                    doc.Add(tableSummaryPortfolio);
                    e.AddHeader2(pdfWriter, doc, f, l);
                    doc.Add(tableTransaction);
                    e.AddInformation(doc);
                    e.OnEndPage(pdfWriter, doc);
                    pdfWriter.CloseStream = true;
                    doc.Close();
                    e.AddFooter(path, doc);

                    fs.Close();

                    us.user_id = userID;
                    us.user_account_id = Convert.ToInt32(userAID);
                    us.user_statment_category_id = statementCategory;
                    us.name = accountNo + "_" + pdfDate + ".pdf";

                    if (statementCategory == 1)
                        us.url = "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                    else if (statementCategory == 3)
                        us.url = "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //us.url = "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".pdf";
                    us.createdate = currenttime2;
                    us.status = 1;
                    us = UserStatementService.Insert(us);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("GenerateReport: Exception - " + ex.Message);
            }
        }


        public static List<UtmcCompositionalTransaction> GetCompositionalTransactionByHolderNo(string holderNo, string startDate, string endDate)
        {
            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcCompositionalTransactionApi?holderNo=" + holderNo + "&startDate=" + startDate + "&endDate=" + endDate).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcCompositionalTransactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcCompositionalTransaction>>(responseString);
                }
            }
            return utmcCompositionalTransactions;
        }
    }
}
