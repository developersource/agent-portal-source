﻿using DiOTP.OracleData;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DiOTP.Utility.Helper;

namespace DiOTP.WebApi.Controllers
{
    public class PaymentApiController : ApiController
    {
        // GET api/PaymentApi
        public Response Get(string orderNo, string transDate, string txn_amt,
            string buyer_mail_id, string buyerName, string buyerBankBranch,
            string buyerAccNo, string makerName, string buyerIBAN,
            string buyerBankId)
        {
            //List<HolderInv> holderInvs = (List<HolderInv>)response.Data;
            Response response = new Response();
            response.IsSuccess = true;
            response.Message = "Test API Order No " + orderNo + " date :" + transDate;
            return response;
        }

        public Response GetDailyNAV()
        {
            Response response = new Response();
            string fromdate = DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString();
            string todate = DateTime.Now.AddDays(1).Year.ToString() + "/" + DateTime.Now.AddDays(1).Month.ToString() + "/" + DateTime.Now.AddDays(1).Day.ToString();

            string SelectQuery = " SELECT A.FUND_ID, A.TRANS_DT, A.UP_SELL, A.UNIT_IN_ISSUE, A.UP_SELL * A.UNIT_IN_ISSUE "
                  + "  FROM UTS.PRICE_HISTORY A INNER JOIN "
                  + "  (SELECT FUND_ID, TRANS_DT, MAX(SEQ_NO)AS MaxRecord FROM UTS.PRICE_HISTORY GROUP BY FUND_ID, TRANS_DT) B "
                  + "  ON A.FUND_ID = B.FUND_ID AND A.TRANS_DT = B.TRANS_DT AND A.SEQ_NO = B.MaxRecord "
                  + "  WHERE A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07', '08', '09') AND "
                  + "  (A.trans_dt between to_date('" + fromdate + "', 'yyyy/mm/dd') "
                  + "  and to_date('" + todate + "', 'yyyy/mm/dd'))  AND A.PR_STATUS = 'C' "
                  + "  ORDER BY A.FUND_ID, A.TRANS_DT ASC ";

            response = GenericRepo.GetDataByQuery(SelectQuery, 0, 0, false, null, false, null, true);
            return response;
        }

        // GET api/PaymentApi/InsertFundInfo
        [HttpGet]
        public Response InsertFundInfo()
        {
            try
            {
                Response response = NewRepoClass.Inser_Fund_Nav();
                if (response.IsSuccess)
                {
                    List<UtmcDailyNavFund> utmcDailyNavFunds = (List<UtmcDailyNavFund>)response.Data;
                    List<UserNotificationSetting> userNotificationSetting = new List<UserNotificationSetting>();
                    if (utmcDailyNavFunds != null && utmcDailyNavFunds.Count != 0)
                    {
                        foreach (var fund in utmcDailyNavFunds)
                        {
                            Response responseUNS = GenericRepo
                         .PullData<UserNotificationSetting>(" fund_code = '" + fund.EpfIpdCode + "' ", 0, 0, false, nameof(UserNotificationSetting.Id))
                         .LeftJoin(nameof(UserNotificationSetting.UserIdUser));
                            userNotificationSetting = (List<UserNotificationSetting>)responseUNS.Data;
                            foreach (UserNotificationSetting user in userNotificationSetting.Where(a => a.ActualPrice == fund.DailyUnitPrice).ToList())
                            {
                                string userMail = user.UserIdUser.EmailId;
                                // send mail
                                Email email = new Email();
                                email.body = "";
                                email.subject = "Found Notification";
                                email.user.EmailId = user.UserIdUser.EmailId;
                                String ma = ""; String title = ""; String content = "";
                                EmailService.SendAlertUpdateMail(email, ma, title, content, false, "");
                            }
                        }

                    }
                }
                //List<HolderInv> holderInvs = (List<HolderInv>)response.Data;
                response.IsSuccess = true;
                response.Message = "Test API Order No ";
                return response;
            }
            catch(Exception ex)
            {
                Response response = new Response();
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
