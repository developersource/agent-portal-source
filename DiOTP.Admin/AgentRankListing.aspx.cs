﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AgentRankListing : System.Web.UI.Page
    {

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //populate agent data
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                //Get data to populate and generate agent table dynamically
                StringBuilder filter = new StringBuilder();
                string mainQuery = (@"select * from ");
                string mainQCount = (@"select count(*) from ");
                filter.Append("users u Join user_types ut on ut.user_id = u.id Join user_types_def utd on ut.user_type_id = utd.id  where 1=1 and u.is_agent = '1' ");
                //Get data 
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]) && Request.QueryString["FilterValue"].Trim() != "0")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append("and ut.user_type_id in ('" + FilterValue.Value + "') ");
                }
                else {
                    filter.Append("and ut.user_type_id in (6,7,8,9) ");
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append("and (u.username like '%" + Search.Value.Trim() + "%' ");
                    filter.Append("or u.email_id like '%" + Search.Value.Trim() + "%' ");
                    filter.Append("or u.agent_code like '%" + Search.Value.Trim() + "%' ) ");
                }
                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by u.agent_status desc, u.is_active desc, u.created_date desc");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseAgent = GenericService.GetDataByQuery(mainQuery + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseAgent.IsSuccess)
                {
                    var agentDyn = responseAgent.Data;
                    var responseJSON = JsonConvert.SerializeObject(agentDyn);
                    List<dynamic> agentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);
                    StringBuilder sb = new StringBuilder();
                    int index = 1;

                    agentList.ForEach(x =>
                    {
                        sb.Append(@"<tr class='fs-12'>
                                    <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + x.ID + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    </td>
                                    <td>" + x.Username + @"</td>
                                    <td>" + x.EmailId + @"</td>
                                    <td>" + x.Name + @"</td>
                                    <td class='checkedAgentCode' value='"+x.AgentCode.ToString()+"'>" + x.AgentCode.ToString() + @"</td>
                                </tr>");
                        index++;

                    });
                    TraineeMaterialTBody.InnerHtml = sb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseAgent.Message + "');", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            // use id to get agentCode
            string idString = String.Join(",", ids);
            string CurrentAgentCode = "";
            string CurrentAgentGroup = "";
            //idString = "564";
            string CurrentAgentPos= "";
            //string uplineQueryString = "";
            User loginUser = (User)HttpContext.Current.Session["admin"];
            try
            {
                //Response for error handling
                Response response1 = new Response();

                string getAgentCodeQuery = @"select ap.* from agent_regs ar 
                                                left join agent_positions ap on ar.agent_code = ap.agent_code
                                                where ar.user_id in ("+idString+")";
                Response responseGetAgent = GenericService.GetDataByQuery(getAgentCodeQuery, 0, 0, false, null, false, null, false);
                if (responseGetAgent.IsSuccess) {
                    var agentDyn = responseGetAgent.Data;
                    var responseJSON = JsonConvert.SerializeObject(agentDyn);
                    List<AgentPosition> agentUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseJSON);

                    CurrentAgentCode = agentUser.FirstOrDefault().AgentCode;
                    CurrentAgentPos = agentUser.FirstOrDefault().AgentPos;
                    CurrentAgentGroup = agentUser.FirstOrDefault().AgentGroup;
                }

                
                //if (CurrentAgentPos.Length > 3)
                //{
                //    uplineQueryString = " OR (ap.agent_pos in(";

                //    for (int i = 3; i < CurrentAgentPos.Length; i += 3)
                //    {
                //        if ((CurrentAgentPos.Length - i) == 3)
                //        {
                //            uplineQueryString += "'" + CurrentAgentPos.Substring(0, (CurrentAgentPos.ToString().Length) - i) + "') and char_length(ap.agent_pos) <" + CurrentAgentPos.Length + " )";
                //        }
                //        else
                //        {
                //            uplineQueryString += "'" + CurrentAgentPos.Substring(0, (CurrentAgentPos.ToString().Length) - i) + "',";
                //        }
                //    }
                //}

                //Use Agent Code/Agent IdString to get agent position
                //      string getAgentsQuery = @"select arp.name,utd.name as agent_rank, arp.Status,ap.id,ap.Upline1,ap.Upline2,ap.Upline3, ap.prev_Upline1,ap.prev_Upline2,ap.prev_Upline3,ap.is_active, ap.status, ap.agent_pos, ap.agent_code,ap.created_date,ar.user_id, ut.id as UTID
                //                              from agent_positions ap
                //                              join agent_regs ar on ap.agent_code = ar.agent_code
                //                              join agent_reg_personal arp on ar.id = arp.agent_reg_id
                //join user_types ut on ut.user_id = ar.user_id
                //join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                //                              where (ap.agent_pos like '"+CurrentAgentPos+ "%' and char_length(ap.agent_pos) >" + CurrentAgentPos.Length+ " ) OR (ap.agent_pos like '"+CurrentAgentPos.Substring(0,3)+"%' and char_length(ap.agent_pos) <"+CurrentAgentPos.Length+" ) OR (ap.agent_pos = '"+CurrentAgentPos+"') group by arp.id";
                //      string getAgentsQuery = @"select arp.name,utd.name as agent_rank, arp.Status,ap.id,ap.Upline1,ap.Upline2,ap.Upline3, ap.prev_Upline1,ap.prev_Upline2,ap.prev_Upline3,ap.is_active, ap.status, ap.agent_pos, ap.agent_code,ap.created_date,ar.user_id, ut.id as UTID
                //                              from agent_positions ap
                //                              join agent_regs ar on ap.agent_code = ar.agent_code
                //                              join agent_reg_personal arp on ar.id = arp.agent_reg_id
                //join user_types ut on ut.user_id = ar.user_id
                //join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                //                              where (ap.agent_pos like '" + CurrentAgentPos + "%' and char_length(ap.agent_pos) >" + CurrentAgentPos.Length + " )" + uplineQueryString + " OR (ap.agent_pos = '" + CurrentAgentPos + "') group by arp.id";
                string getAgentsQuery = @"select arp.name,utd.name as agent_rank, arp.Status,ap.id,ap.Upline1,ap.Upline2,ap.Upline3, ap.prev_Upline1,ap.prev_Upline2,ap.prev_Upline3,ap.is_active, ap.status, ap.agent_pos, ap.agent_code,ap.created_date,ar.user_id, ut.id as UTID
                                        from agent_positions ap
                                        join agent_regs ar on ap.agent_code = ar.agent_code
                                        join agent_reg_personal arp on ar.id = arp.agent_reg_id
										join user_types ut on ut.user_id = ar.user_id
										join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                                        where ap.agent_group = '" + CurrentAgentGroup + "'group by arp.id";
                Response responseAgentHierarchy = GenericService.GetDataByQuery(getAgentsQuery, 0, 0, false, null, false, null, true);
                if (responseAgentHierarchy.IsSuccess)
                {

                    var agentHierarchyDyn = responseAgentHierarchy.Data;
                    var responseJSON = JsonConvert.SerializeObject(agentHierarchyDyn);
                    List<dynamic> agentHierarchyList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseJSON);

                    //Separation of agent categories
                    // 1. Current Agent
                    dynamic CurrentAgent = agentHierarchyList.FirstOrDefault(x => x.agent_code == CurrentAgentCode);

                    // 2. Direct Downline Agents
                    List<dynamic> DirectDownlineAgents = agentHierarchyList.Where(x => x.agent_pos.ToString().Length == (CurrentAgent.agent_pos.ToString().Length + 3)).ToList();

                    // 3. Indirect Downline Agents
                    List<dynamic> IndirectDownlineAgents = agentHierarchyList.Where(x => x.agent_pos.ToString().Length > (CurrentAgent.agent_pos.ToString().Length + 3)).ToList();

                    // 4. Upline Agent
                    List<dynamic> UplineAgent = agentHierarchyList.Where(x => x.agent_pos.ToString().Length <= (CurrentAgent.agent_pos.ToString().Length - 3)).ToList();

                    //WHEN FREE ENHANCE ABIT ON THE COUNT

                    //IF PROMOTE
                    if (action == "Promote")
                    {
                        //SGM Not Allowed to promote
                        if (CurrentAgent.agent_rank == "SGM")
                        {
                            response1.Message = "SGM cannot be promoted!";
                            response1.IsSuccess = false;
                            return response1;
                        }
                        else
                        {
                            if (CurrentAgent.agent_rank == "WA") {
                                //Update rank after promotion
                                UserType SingleAgentUT = new UserType
                                {
                                    Id = Convert.ToInt32(CurrentAgent.UTID),
                                    UserId = Convert.ToInt32(CurrentAgent.user_id),
                                    UserTypeId = 7, //7 = WM rank
                                    IsVerified = 1,
                                    Status = 1
                                };
                                Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);

                            }

                            if (CurrentAgent.agent_rank == "WM")
                            {
                                UserType SingleAgentUT = new UserType
                                {
                                    Id = Convert.ToInt32(CurrentAgent.UTID),
                                    UserId = Convert.ToInt32(CurrentAgent.user_id),
                                    UserTypeId = 8, //8 = GM rank
                                    IsVerified = 1,
                                    Status = 1
                                };
                                Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);

                                //Check if have upline > If no upline nothing changes >If have upline and check if current rank greater than upline rank.
                                //Check how many Uplines there are.
                                int UplineAgentCount = UplineAgent.Count;
                                List<dynamic> UplineAAPos = new List<dynamic>();

                                //If have 1 upline agent
                                if (UplineAgentCount == 1)
                                {
                                    string generateNewHeadQuery = @"SELECT * FROM agent_positions where  agent_pos NOT like '____%' order by agent_pos desc limit 1";
                                    Response responseAgentChild = GenericService.GetDataByQuery(generateNewHeadQuery, 0, 0, false, null, false, null, false);
                                    if (responseAgentChild.IsSuccess)
                                    {
                                        var agentChildDyn = responseAgentChild.Data;
                                        var responseAgentChildJSON = JsonConvert.SerializeObject(agentChildDyn);
                                        List<AgentPosition> agentChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentChildJSON);

                                        //New position for current Agent to explore new tree.
                                        string newAgentPosition = GenerateAgentPos(agentChild.FirstOrDefault().AgentPos, agentChild.Count);
                                        //Update current agent
                                        AgentPosition currentAgentUpdated = new AgentPosition
                                        {
                                            Id = Convert.ToInt32(CurrentAgent.id.ToString()),
                                            AgentCode = CurrentAgent.agent_code.ToString(),
                                            AgentPos = newAgentPosition,
                                            Upline1 = "MG",
                                            Upline2 = "MG",
                                            Upline3 = "MG",
                                            PrevUpline1 = UplineAgent.FirstOrDefault().agent_code.ToString(),
                                            PrevUpline2 = "",
                                            PrevUpline3 = "",
                                            IsActive = Convert.ToInt32(CurrentAgent.is_active.ToString()),
                                            CreatedDate = Convert.ToDateTime(CurrentAgent.created_date.ToString()),
                                            Status = 1
                                        };
                                        Response responseUpdateCurrentAgent = GenericService.UpdateData<AgentPosition>(currentAgentUpdated);

                                        DirectDownlineAgents.ForEach(x =>
                                        {
                                            string newDownlinePos = x.agent_pos.ToString().Replace(x.agent_pos.ToString().Substring(0, (x.agent_pos.ToString().Length)-3), newAgentPosition);
                                            //Update new agent position, and Uplines.
                                            AgentPosition newHead = new AgentPosition
                                            {
                                                Id = Convert.ToInt32(x.id.ToString()),
                                                AgentCode = x.agent_code.ToString(),
                                                AgentPos = newDownlinePos,
                                                Upline1 = CurrentAgent.agent_code.ToString(),
                                                Upline2 = "MG",
                                                Upline3 = "MG",
                                                PrevUpline1 = "",
                                                PrevUpline2 = "",
                                                PrevUpline3 = "",
                                                IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                Status = 1
                                            };

                                            Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);

                                            //Check indirect downline list if have similar position pattern as directdownline to adjust its hierarchy.
                                            IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.agent_pos.ToString())).ToList().ForEach(y =>
                                            {
                                                //this filters the list to only include specific indirect downline for the direct downline.
                                                //string newIndirectPos = newAgentPosition + y.agent_pos.ToString().Substring(y.agent_pos.Length - 3, 3);

                                                //Replace the first 3 characters of the string to update the root node.
                                                string newIndirectPos = y.agent_pos.ToString().Replace(y.agent_pos.ToString().Substring(0, (y.agent_pos.ToString().Length) - 3), newDownlinePos);

                                                //Update indirect downline agent position, and also update its Upline details.
                                                AgentPosition newIndirect = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(Convert.ToInt32(y.id.ToString()).ToString()),
                                                    AgentCode = y.agent_code.ToString(),
                                                    AgentPos = newIndirectPos,
                                                    Upline1 = x.agent_code.ToString(),
                                                    Upline2 = CurrentAgent.agent_code.ToString(),
                                                    Upline3 = "MG",
                                                    PrevUpline1 = "",
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(y.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(y.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateIndirectDownline = GenericService.UpdateData<AgentPosition>(newIndirect);
                                            });
                                        });

                                    }
                                }

                                //If have more than 1 Upline agent
                                if (UplineAgentCount > 1) {
                                    //assign the agent code for the upline agents 
                                    UplineAgent.ForEach(x =>
                                    {
                                        UplineAAPos.Add(x);
                                    });

                                    //Check the upline rank. for first default agent "Upline" after sorted
                                    var sortedUplineAgentAA = UplineAAPos.OrderByDescending(x => x.agent_pos.ToString().Length).ThenByDescending(x => Convert.ToInt32(x.agent_pos.ToString().Length)).ToList();

                                    //if upline same rank with you or lower, you need to create new tree
                                    if (sortedUplineAgentAA.FirstOrDefault().agent_rank == "WM" || sortedUplineAgentAA.FirstOrDefault().agent_rank == "WA")
                                    {
                                        //Then next check if your upline have upline or not.
                                        if (UplineAgentCount > 1)
                                        {
                                            //if have upline, we use that upline code to generate new position for the promoted agent and his downline if any.
                                            string upline2pos = sortedUplineAgentAA.Last().agent_pos.ToString();                                           

                                            //Lose all downline 
                                            string checkUplineChildQuery = @"SELECT * FROM agent_positions where agent_pos != '" + upline2pos + "' and agent_pos like '" + upline2pos + "%' and length(agent_pos) ='" + (Convert.ToInt32(upline2pos.ToString().Length) + 3) + "' order by agent_pos desc limit 1";
                                            Response responseUplineChild = GenericService.GetDataByQuery(checkUplineChildQuery, 0, 0, false, null, false, null, false);
                                            if (responseUplineChild.IsSuccess)
                                            {
                                                var uplineChildDyn = responseUplineChild.Data;
                                                var responseUplineChildJSON = JsonConvert.SerializeObject(uplineChildDyn);
                                                List<AgentPosition> uplineChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseUplineChildJSON);

                                                string newAgentPosition = GenerateAgentPos(uplineChild.FirstOrDefault().AgentPos, uplineChild.Count);

                                                //Update current agent
                                                AgentPosition currentAgentUpdated = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(CurrentAgent.id.ToString()),
                                                    AgentCode = CurrentAgent.agent_code.ToString(),
                                                    AgentPos = newAgentPosition,
                                                    Upline1 = sortedUplineAgentAA.Last().agent_code.ToString(),
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = sortedUplineAgentAA.FirstOrDefault().agent_code.ToString(),
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(CurrentAgent.is_active.ToString()),
                                                    CreatedDate = DateTime.Parse(CurrentAgent.created_date.ToString()),
                                                    Status = 1
                                                };
                                                Response responseUpdateCurrentAgent = GenericService.UpdateData<AgentPosition>(currentAgentUpdated);

                                                DirectDownlineAgents.ForEach(x =>
                                                {
                                                    string newDownlinePos = newAgentPosition + x.agent_pos.ToString().Substring(x.agent_pos.ToString().Length - 3,3);
                                                    AgentPosition newHead = new AgentPosition
                                                    {
                                                        Id = Convert.ToInt32(x.id.ToString()),
                                                        AgentCode = x.agent_code.ToString(),
                                                        AgentPos = newDownlinePos,
                                                        Upline1 = CurrentAgent.agent_code.ToString(),
                                                        Upline2 = sortedUplineAgentAA.Last().agent_code.ToString(),
                                                        Upline3 = "MG",
                                                        PrevUpline1 = "",
                                                        PrevUpline2 = "",
                                                        PrevUpline3 = "",
                                                        IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                        CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                        Status = 1
                                                    };

                                                    Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);

                                                    //Update indirect downline to match with the direct downline
                                                    //Check indirect downline list if have similar position pattern as directdownline to adjust its hierarchy.
                                                    IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.agent_pos.ToString())).ToList().ForEach(y =>
                                                    {
                                                        //this filters the list to only include specific indirect downline for the direct downline.
                                                        string newIndirectPos = newAgentPosition + y.agent_pos.ToString().Substring((y.agent_pos.ToString().Length) - 3, 3);

                                                        //Update indirect downline agent position, and also update its Upline details.
                                                        AgentPosition newIndirect = new AgentPosition
                                                        {
                                                            Id = Convert.ToInt32(Convert.ToInt32(y.id.ToString()).ToString()),
                                                            AgentCode = y.agent_code.ToString(),
                                                            AgentPos = newIndirectPos,
                                                            Upline1 = x.agent_code.ToString(),
                                                            Upline2 = CurrentAgent.agent_code.ToString(),
                                                            Upline3 = "MG",
                                                            PrevUpline1 = "",
                                                            PrevUpline2 = "",
                                                            PrevUpline3 = "",
                                                            IsActive = Convert.ToInt32(y.is_active.ToString()),
                                                            CreatedDate = Convert.ToDateTime(y.created_date.ToString()),
                                                            Status = 1
                                                        };

                                                        Response responseUpdateIndirectDownline = GenericService.UpdateData<AgentPosition>(newIndirect);
                                                    });                                                    
                                                });

                                            }
                                        }
                                    }
                                }

                                
                            }

                            if (CurrentAgent.agent_rank == "GM")
                            {
                                UserType SingleAgentUT = new UserType
                                {
                                    Id = Convert.ToInt32(CurrentAgent.UTID),
                                    UserId = Convert.ToInt32(CurrentAgent.user_id),
                                    UserTypeId = 9, //9 = SGM rank
                                    IsVerified = 1,
                                    Status = 1
                                };
                                Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);

                                //Update rank only if no upline.
                                //If have upline, branch new tree and downline follows.
                                if (UplineAgent.Count != 0) {
                                    string generateNewHeadQuery = @"SELECT * FROM agent_positions where  agent_pos NOT like '____%' order by agent_pos desc limit 1";
                                    Response responseAgentChild = GenericService.GetDataByQuery(generateNewHeadQuery, 0, 0, false, null, false, null, false);
                                    if (responseAgentChild.IsSuccess)
                                    {
                                        var agentChildDyn = responseAgentChild.Data;
                                        var responseAgentChildJSON = JsonConvert.SerializeObject(agentChildDyn);
                                        List<AgentPosition> agentChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentChildJSON);

                                        //New position for current Agent to explore new tree.
                                        string newAgentPosition = GenerateAgentPos(agentChild.FirstOrDefault().AgentPos, agentChild.Count);
                                        //Update current agent
                                        AgentPosition currentAgentUpdated = new AgentPosition
                                        {
                                            Id = Convert.ToInt32(CurrentAgent.id.ToString()),
                                            AgentCode = CurrentAgent.agent_code.ToString(),
                                            AgentPos = newAgentPosition,
                                            Upline1 = "MG",
                                            Upline2 = "MG",
                                            Upline3 = "MG",
                                            PrevUpline1 = UplineAgent.FirstOrDefault().agent_code.ToString(),
                                            PrevUpline2 = "",
                                            PrevUpline3 = "",
                                            IsActive = Convert.ToInt32(CurrentAgent.is_active.ToString()),
                                            CreatedDate = Convert.ToDateTime(CurrentAgent.created_date.ToString()),
                                            Status = 1
                                        };
                                        Response responseUpdateCurrentAgent = GenericService.UpdateData<AgentPosition>(currentAgentUpdated);

                                        DirectDownlineAgents.ForEach(x =>
                                        {
                                            string newDownlinePos = x.agent_pos.ToString().Replace(x.agent_pos.ToString().Substring(0, (x.agent_pos.ToString().Length)-3), newAgentPosition);
                                            //Update new agent position, and Uplines.
                                            AgentPosition newHead = new AgentPosition
                                            {
                                                Id = Convert.ToInt32(x.id.ToString()),
                                                AgentCode = x.agent_code.ToString(),
                                                AgentPos = newDownlinePos,
                                                Upline1 = CurrentAgent.agent_code.ToString(),
                                                Upline2 = "MG",
                                                Upline3 = "MG",
                                                PrevUpline1 = "",
                                                PrevUpline2 = "",
                                                PrevUpline3 = "",
                                                IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                Status = 1
                                            };

                                            Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);

                                            //Check indirect downline list if have similar position pattern as directdownline to adjust its hierarchy.
                                            IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.AgentPos.ToString())).ToList().ForEach(y =>
                                            {
                                                //this filters the list to only include specific indirect downline for the direct downline.
                                                //string newIndirectPos = newAgentPosition + y.agent_pos.ToString().Substring(y.agent_pos.Length - 3, 3);

                                                //Replace the first 3 characters of the string to update the root node.
                                                string newIndirectPos = y.agent_pos.ToString().Replace(y.agent_pos.ToString().Substring(0, (y.agent_pos.ToString().Length) - 3), newDownlinePos);

                                                //Update indirect downline agent position, and also update its Upline details.
                                                AgentPosition newIndirect = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(y.id.ToString()),
                                                    AgentCode = y.agent_code.ToString(),
                                                    AgentPos = newIndirectPos,
                                                    Upline1 = x.Upline1.ToString(),
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = "",
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateIndirectDownline = GenericService.UpdateData<AgentPosition>(newIndirect);
                                            });
                                        });

                                    }
                                }
                            }
                        }
                    }

                    //IF DEMOTE
                    if (action == "Demote")
                    {
                        //WA Not Allowed to demote
                        if (CurrentAgent.agent_rank == "WA")
                        {
                            response1.Message = "WA cannot be demoted!";
                            response1.IsSuccess = false;
                            return response1;
                        }
                        else
                        {
                            //Check the agent rank first 
                            if (CurrentAgent.agent_rank == "SGM")
                            {

                                int SameRankCount = DirectDownlineAgents.Where(x => x.agent_rank == "GM").ToList().Count;

                                //If no direct downline agent has same rank after demoted. Can remain the hierarchy untouched.
                                if (SameRankCount == 0)
                                {
                                    //Remain current agent details just change the rank.
                                    UserType SingleAgentUT = new UserType
                                    {
                                        Id = Convert.ToInt32(CurrentAgent.UTID),
                                        UserId = Convert.ToInt32(CurrentAgent.user_id),
                                        UserTypeId = 8, //8 = GM rank
                                        IsVerified = 1,
                                        Status = 1
                                    };
                                    Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);
                                }
                                else
                                {
                                    //Update the Current Agent Rank details.
                                    UserType SingleAgentUT = new UserType
                                    {
                                        Id = Convert.ToInt32(CurrentAgent.UTID),
                                        UserId = Convert.ToInt32(CurrentAgent.user_id),
                                        UserTypeId = 8, //8 = GM rank
                                        IsVerified = 1,
                                        Status = 1
                                    };
                                    Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);

                                    //Need to check if there are still downlines that does not have same rank
                                    //Only alter the agent hierarchy of downline agents that have same rank.
                                    //If same rank -> new branch in hierarchy -> Adjust downline branch to new branch, both direct and indirect if any.
                                    //Adjust for current direct-downline and their respective downlines.
                                    string generateNewHeadQuery = @"SELECT * FROM agent_positions where  agent_pos NOT like '____%' order by agent_pos desc limit 1";
                                    Response responseAgentChild = GenericService.GetDataByQuery(generateNewHeadQuery, 0, 0, false, null, false, null, false);
                                    if (responseAgentChild.IsSuccess)
                                    {
                                        var agentChildDyn = responseAgentChild.Data;
                                        var responseAgentChildJSON = JsonConvert.SerializeObject(agentChildDyn);
                                        List<AgentPosition> agentChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentChildJSON);

                                        //New position for head
                                        string newAgentPosition = GenerateAgentPos(agentChild.FirstOrDefault().AgentPos, agentChild.Count);

                                        DirectDownlineAgents.Where(x => x.agent_rank == "GM").ToList().ForEach(x =>
                                        {
                                            //Update new agent position, and Uplines.
                                            AgentPosition newHead = new AgentPosition
                                            {
                                                Id = Convert.ToInt32(x.id.ToString()),
                                                AgentCode = x.agent_code.ToString(),
                                                AgentPos = newAgentPosition,
                                                Upline1 = "MG",
                                                Upline2 = "MG",
                                                Upline3 = "MG",
                                                PrevUpline1 = CurrentAgent.agent_code.ToString(),
                                                PrevUpline2 = "",
                                                PrevUpline3 = "",
                                                IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                Status = 1
                                            };

                                            Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);

                                            //Check indirect downline list if have similar position pattern as directdownline to adjust its hierarchy.
                                            IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.AgentPos.ToString())).ToList().ForEach(y =>
                                            {
                                                string newIndirectPos = y.agent_pos.ToString().Replace(y.agent_pos.ToString().Substring(0, y.agent_pos.ToString().Length - 3), newAgentPosition);

                                                //Update indirect downline agent position, and also update its Upline details.
                                                AgentPosition newIndirect = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(y.id.ToString()),
                                                    AgentCode = y.agent_code.ToString(),
                                                    AgentPos = newIndirectPos,
                                                    Upline1 = x.agent_code.ToString(),
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = "",
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(y.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(y.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateIndirectDownline = GenericService.UpdateData<AgentPosition>(newIndirect);
                                            });
                                        });

                                    }
                                }
                            }

                            if (CurrentAgent.agent_rank == "GM")
                            {
                                int SameRankCount = DirectDownlineAgents.Where(x => x.agent_rank == "WM").ToList().Count;

                                //If no direct downline agent has same rank after demoted. Can remain the hierarchy untouched.
                                if (SameRankCount == 0)
                                {
                                    //Remain current agent details just change the rank.
                                    UserType SingleAgentUT = new UserType
                                    {
                                        Id = Convert.ToInt32(CurrentAgent.UTID),
                                        UserId = Convert.ToInt32(CurrentAgent.user_id),
                                        UserTypeId = 7, //7 = WM rank
                                        IsVerified = 1,
                                        Status = 1
                                    };
                                    Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);
                                }
                                else
                                {
                                    //Update the Current Agent Rank details.
                                    UserType SingleAgentUT = new UserType
                                    {
                                        Id = Convert.ToInt32(CurrentAgent.UTID),
                                        UserId = Convert.ToInt32(CurrentAgent.user_id),
                                        UserTypeId = 7, //7 = WM rank
                                        IsVerified = 1,
                                        Status = 1
                                    };
                                    Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);


                                    int UplineAgentCount = UplineAgent.Count;
                                    string UplineAAPos = "";

                                    //If have Upline agent
                                    if (UplineAgentCount > 0)
                                    {
                                        UplineAAPos = UplineAgent.FirstOrDefault().agent_pos.ToString();

                                        //GET THE UPLINE LATEST CODE FOR DOWN LINE Position
                                        string getUplinePosQuery = "SELECT * FROM agent_positions where agent_pos != '" + UplineAgent.FirstOrDefault().agent_pos.ToString() + "' and agent_pos like '" + UplineAgent.FirstOrDefault().agent_pos.ToString() + "%' and length(agent_pos) ='" + (Convert.ToInt32(UplineAgent.FirstOrDefault().agent_pos.ToString().Length) + 3) + "' order by agent_pos desc limit 1";
                                        Response responseUplinePos = GenericService.GetDataByQuery(getUplinePosQuery, 0, 0, false, null, false, null, false);
                                        if (responseUplinePos.IsSuccess)
                                        {
                                            var UplinePosDyn = responseUplinePos.Data;
                                            var responseUplinePosJSON = JsonConvert.SerializeObject(UplinePosDyn);
                                            List<AgentPosition> UplineAgentLatestAvailablePos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseUplinePosJSON);

                                            string newAgentPosition = GenerateAgentPos(UplineAgentLatestAvailablePos.FirstOrDefault().AgentPos, UplineAgentLatestAvailablePos.Count);

                                            DirectDownlineAgents.Where(x => x.agent_rank == "WM").ToList().ForEach(x =>
                                            {

                                                //need to generate new position based on current agent upline's latest available position
                                                //string newAgentPosition = UplineAAPos + x.AgentPos().ToString().Substring(x.AgentPos.Length - 3, 3);
                                                AgentPosition newHead = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(x.id.ToString()),
                                                    AgentCode = x.agent_code.ToString(),
                                                    AgentPos = newAgentPosition,
                                                    Upline1 = UplineAgent.FirstOrDefault().AgentCode,
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = CurrentAgent.agent_code.ToString(),
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);


                                                IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.AgentPos.ToString())).ToList().ForEach(y =>
                                                {
                                                    //this filters the list to only include specific indirect downline for the direct downline.
                                                    string newIndirectPos = newAgentPosition + y.agent_pos.ToString().Substring(y.agent_pos.Length - 3, 3);

                                                    //Update indirect downline agent position, and also update its Upline details.
                                                    AgentPosition newIndirect = new AgentPosition
                                                    {
                                                        Id = Convert.ToInt32(y.id.ToString()),
                                                        AgentCode = y.agent_code.ToString(),
                                                        AgentPos = newIndirectPos,
                                                        Upline1 = x.agent_code.ToString(),
                                                        Upline2 = "MG",
                                                        Upline3 = "MG",
                                                        PrevUpline1 = "",
                                                        PrevUpline2 = "",
                                                        PrevUpline3 = "",
                                                        IsActive = Convert.ToInt32(y.is_active.ToString()),
                                                        CreatedDate = Convert.ToDateTime(y.created_date.ToString()),
                                                        Status = 1
                                                    };

                                                    Response responseUpdateIndirectDownline = GenericService.UpdateData<AgentPosition>(newIndirect);
                                                });
                                            });
                                        }                                        
                                    }
                                    else
                                    {
                                        //If no Upline agent, it will branch out into a new hierarchy.

                                        //Need to check if there are still downlines that does not have same rank
                                        //Only alter the agent hierarchy of downline agents that have same rank.
                                        //If same rank -> new branch in hierarchy -> Adjust downline branch to new branch, both direct and indirect if any.
                                        //Adjust for current direct-downline and their respective downlines.
                                        string generateNewHeadQuery = @"SELECT * FROM agent_positions where  agent_pos NOT like '____%' order by agent_pos desc limit 1";
                                        Response responseAgentChild = GenericService.GetDataByQuery(generateNewHeadQuery, 0, 0, false, null, false, null, false);
                                        if (responseAgentChild.IsSuccess)
                                        {
                                            var agentChildDyn = responseAgentChild.Data;
                                            var responseAgentChildJSON = JsonConvert.SerializeObject(agentChildDyn);
                                            List<AgentPosition> agentChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentChildJSON);

                                            //New position for head
                                            string newAgentPosition = GenerateAgentPos(agentChild.FirstOrDefault().AgentPos, agentChild.Count);

                                            DirectDownlineAgents.Where(x => x.agent_rank == "WM").ToList().ForEach(x =>
                                            {
                                                //Update new agent position, and Uplines.
                                                AgentPosition newHead = new AgentPosition
                                                {
                                                    Id = x.ID,
                                                    AgentCode = x.agent_code.ToString(),
                                                    AgentPos = newAgentPosition,
                                                    Upline1 = UplineAgent.FirstOrDefault().AgentCode,
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = CurrentAgent.agent_code.ToString(),
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);

                                                //Check indirect downline list if have similar position pattern as directdownline to adjust its hierarchy.
                                                IndirectDownlineAgents.Where(y => y.agent_pos.ToString().Contains(x.AgentPos.ToString())).ToList().ForEach(y =>
                                                {
                                                    //this filters the list to only include specific indirect downline for the direct downline.
                                                    string newIndirectPos = newAgentPosition + y.agent_pos.ToString().Substring((y.agent_pos.ToString().Length) - 3, 3);

                                                    //Update indirect downline agent position, and also update its Upline details.
                                                    AgentPosition newIndirect = new AgentPosition
                                                    {
                                                        Id = Convert.ToInt32(y.id.ToString()),
                                                        AgentCode = y.agent_code.ToString(),
                                                        AgentPos = newIndirectPos,
                                                        Upline1 = x.agent_code.ToString(),
                                                        Upline2 = "MG",
                                                        Upline3 = "MG",
                                                        PrevUpline1 = "",
                                                        PrevUpline2 = "",
                                                        PrevUpline3 = "",
                                                        IsActive = Convert.ToInt32(y.is_active.ToString()),
                                                        CreatedDate = Convert.ToDateTime(y.created_date.ToString()),
                                                        Status = 1
                                                    };

                                                    Response responseUpdateIndirectDownline = GenericService.UpdateData<AgentPosition>(newIndirect);
                                                });
                                            });
                                        }
                                    }
                                }

                            }

                            if (CurrentAgent.agent_rank == "WM")
                            {
                                int SameRankCount = DirectDownlineAgents.Where(x => x.agent_rank == "WA").ToList().Count;

                                //If no direct downline agent has same rank after demoted. Can remain the hierarchy untouched.
                                if (SameRankCount == 0)
                                {
                                    //Remain current agent details just change the rank.
                                    UserType SingleAgentUT = new UserType
                                    {
                                        Id = Convert.ToInt32(CurrentAgent.UTID),
                                        UserId = Convert.ToInt32(CurrentAgent.user_id),
                                        UserTypeId = 6, //6 = WA rank
                                        IsVerified = 1,
                                        Status = 1
                                    };
                                    Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);
                                }
                                else
                                {
                                    //Update the Current Agent Rank details.
                                    UserType SingleAgentUT = new UserType
                                    {
                                        Id = Convert.ToInt32(CurrentAgent.UTID),
                                        UserId = Convert.ToInt32(CurrentAgent.user_id),
                                        UserTypeId = 6, //6 = WA rank
                                        IsVerified = 1,
                                        Status = 1
                                    };
                                    Response responseUpdate = GenericService.UpdateData<UserType>(SingleAgentUT);

                                    //Check how many Uplines there are.
                                    int UplineAgentCount = UplineAgent.Count;
                                    List<dynamic> UplineAAPos = new List<dynamic>();

                                    //If have Upline agent
                                    if (UplineAgentCount >= 1)
                                    {
                                        //assign the agent code for the upline agents 
                                        UplineAgent.ForEach(x =>
                                        {
                                            UplineAAPos.Add(x);
                                        });
                                        //Sort the List according to length of position. DESC -> direct upline is first or default
                                        var sortedUplineAgentAA = UplineAAPos.OrderByDescending(x => x.agent_pos.ToString().Length).ThenByDescending(x => Convert.ToInt32(x.agent_pos.ToString().Length)).ToList();

                                        //Lose all downline 
                                        string checkUplineChildQuery = @"SELECT * FROM agent_positions where agent_pos != '" + sortedUplineAgentAA.FirstOrDefault().agent_pos.ToString() + "' and agent_pos like '" + sortedUplineAgentAA.FirstOrDefault().agent_pos.ToString() + "%' and length(agent_pos) ='" + (Convert.ToInt32(sortedUplineAgentAA.FirstOrDefault().agent_pos.ToString().Length) + 3) + "' order by agent_pos desc limit 1";
                                        Response responseUplineChild = GenericService.GetDataByQuery(checkUplineChildQuery, 0, 0, false, null, false, null, false);
                                        if (responseUplineChild.IsSuccess)
                                        {
                                            var uplineChildDyn = responseUplineChild.Data;
                                            var responseUplineChildJSON = JsonConvert.SerializeObject(uplineChildDyn);
                                            List<AgentPosition> uplineChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseUplineChildJSON);

                                            string newAgentPosition = GenerateAgentPos(uplineChild.FirstOrDefault().AgentPos, uplineChild.Count);
                                            
                                            //Upate for current agent.

                                            DirectDownlineAgents.ForEach(x =>
                                            {
                                                AgentPosition newHead = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(x.id.ToString()),
                                                    AgentCode = x.agent_code.ToString(),
                                                    AgentPos = newAgentPosition,
                                                    Upline1 = UplineAgent.FirstOrDefault().AgentCode,
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = "",
                                                    PrevUpline2 = "",
                                                    PrevUpline3 = "",
                                                    IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);
                                            });

                                        }

                                    }
                                    else
                                    {
                                        //If no Upline agent, it will branch out into a new hierarchy.
                                        //SELECT * FROM agent_positions where  length(agent_pos) = 3 order by agent_pos desc limit 1 < OPTIMIZED
                                        string generateNewHeadQuery = @"SELECT * FROM agent_positions where  agent_pos NOT like '____%' order by agent_pos desc limit 1";
                                        Response responseAgentChild = GenericService.GetDataByQuery(generateNewHeadQuery, 0, 0, false, null, false, null, false);
                                        if (responseAgentChild.IsSuccess)
                                        {
                                            var agentChildDyn = responseAgentChild.Data;
                                            var responseAgentChildJSON = JsonConvert.SerializeObject(agentChildDyn);
                                            List<AgentPosition> agentChild = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentPosition>>(responseAgentChildJSON);

                                            //New position for head
                                            string newAgentPosition = GenerateAgentPos(agentChild.FirstOrDefault().AgentPos, 0);
                                            DirectDownlineAgents.ForEach(x =>
                                            {

                                                //Update new agent position, and Uplines.
                                                AgentPosition newHead = new AgentPosition
                                                {
                                                    Id = Convert.ToInt32(x.id.ToString()),
                                                    AgentCode = x.agent_code.ToString(),
                                                    AgentPos = newAgentPosition,
                                                    Upline1 = "MG",
                                                    Upline2 = "MG",
                                                    Upline3 = "MG",
                                                    PrevUpline1 = x.Upline1.ToString(),
                                                    PrevUpline2 = x.PrevUpline2,
                                                    PrevUpline3 = x.PrevUpline3,
                                                    IsActive = Convert.ToInt32(x.is_active.ToString()),
                                                    CreatedDate = Convert.ToDateTime(x.created_date.ToString()),
                                                    Status = 1
                                                };

                                                Response responseUpdateDirectDownline = GenericService.UpdateData<AgentPosition>(newHead);                                               
                                            });

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Agent Rank Listing action: " + ex.Message);
                return false;
            }
        }

        public static string GenerateAgentPos(string currPos, int childCount)
        {
            string genPos = "";
            char[] posIndex = currPos.ToCharArray();

            //If parent has child, take the last child
            if (childCount == 1)
            {
                if (posIndex[posIndex.Length - 1] != 'Z')
                {
                    posIndex[posIndex.Length - 1]++;
                    string generated = new string(posIndex);
                    genPos = generated;
                }
                else
                {
                    if (posIndex[posIndex.Length - 2] != 'Z')
                    {
                        posIndex[posIndex.Length - 2]++;
                        string generated = new string(posIndex);
                        genPos = generated;
                    }
                    else
                    {
                        if (posIndex[posIndex.Length - 3] != 'Z')
                        {
                            posIndex[posIndex.Length - 3]++;
                            string generated = new string(posIndex);
                            genPos = generated;
                        }
                        else
                        {
                            Console.WriteLine("Maximum agent Position achieved");
                        }
                    }
                }
            }
            else
            {
                genPos = currPos + "AAA";
            }

            return genPos;
        }
    }
}