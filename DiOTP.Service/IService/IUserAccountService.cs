using DiOTP.Utility.Helper;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service.IService
{
    public interface IUserAccountService : IDefaultInterface<UserAccount>
    {
    }
}
