﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Web;
using DiOTP.Utility;
using DiOTP.Utility.Helper;

namespace DiOTP.Service
{
    public class SendEmailService
    {
        public static Boolean SendVerificationLink(List<UserOrder> userOrder, string email, List<string> fundNames)
        {
            email = "chuajianhui@petraware.com";
            bool send;
            try
            {
                using (MailMessage mm = new MailMessage("pecmsupport@petraware.com", email))
                {
                    int i = 0;
                    mm.Subject = "Order Invoice";
                    mm.IsBodyHtml = true;
                    string body = string.Empty;
                    body = "<h3>Order Invoice</h3></br>";

                    body += "<h4>Order ID : " + userOrder.FirstOrDefault().OrderNo + "</h4>";

                    if (userOrder.FirstOrDefault().OrderType == 1)
                    {
                        body += "<h4>Type : Buy Fund</h4>";
                    }
                    else if (userOrder.FirstOrDefault().OrderType == 2)
                    {
                        body += "<h4>Type : Sell Fund</h4>";
                    }
                    else
                        body += "<h4>Type : Switching</h4>";
                    body += "<h4>Date : " + userOrder.FirstOrDefault().CreatedDate + "</h4>";

                    foreach (UserOrder us in userOrder)
                    {
                        body += @"<p>" + fundNames[i] + " : " + us.Amount + "(Amount)" + " : " + us.Units + "(Units)" + "</p>";
                        i++;

                    }

                    mm.Body = body;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "mail.petraware.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("pecmsupport@petraware.com", "petraware123");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                send = true;
            }
            catch
            {
                send = false;
            }
            return send;
        }

        public static Boolean SendEmail(List<UserOrder> userOrder, string email, List<string> fundNames, List<string> fundNames2, string maNo, string name)
        {
            email = "chuajianhui@petraware.com";
            String Subject = "";
            string body = string.Empty;

            if (userOrder.FirstOrDefault().OrderType == 1)
            {
                Subject = "Apex - Buy Transaction Notification";
                body += "Buy Transaction Submitted\n";
            }
            else if (userOrder.FirstOrDefault().OrderType == 2)
            {
                Subject = "Apex - Sell Transaction Notification";
                body += "Sell Transaction Submitted\n";
            }
            else if (userOrder.FirstOrDefault().OrderType == 3)
            {
                Subject = "Apex - Switch Transaction Notification";
                body += "Switch Transaction Submitted\n";
            }
            else if (userOrder.FirstOrDefault().OrderType == 6)
            {
                Subject = "Apex - Regular Saving Plan Notification";
                body += "Regular Saving Plan Enrollment Submitted\n";
            }

            body += "Date  : " + DateTime.Now.ToString("dd-MM-yyyy") + "\n";
            body += "Time  : " + DateTime.Now.ToString("hh:mm:ss") + "\n";
            body += "MA No : " + maNo + "\n";
            body += "Dear " + name + "\n";
            int i = 0;
            if (userOrder.FirstOrDefault().OrderType == 1 || userOrder.FirstOrDefault().OrderType == 2)
            {
                if (userOrder.FirstOrDefault().OrderType == 1)
                {
                    foreach (UserOrder us in userOrder)
                    {
                        body += @"" + fundNames[i] + " : " + us.Amount + "(Amount)\n";
                        i++;
                    }
                }
                else if (userOrder.FirstOrDefault().OrderType == 2)
                {
                    foreach (UserOrder us in userOrder)
                    {
                        body += @"" + fundNames[i] + " : " + us.Units + "(Units)" + "\n";
                        i++;
                    }
                }
            }
            else if (userOrder.FirstOrDefault().OrderType == 3 || userOrder.FirstOrDefault().OrderType == 4)
            {
                userOrder = userOrder.Where(x => x.OrderType == 3).ToList();

                foreach (UserOrder us in userOrder)
                {
                    body += @"" + fundNames[i] + " switching to " + fundNames2[i] + " : " + us.Units + "(Amount)\n";
                    i++;
                }
            }
            else if (userOrder.FirstOrDefault().OrderType == 6)
            {
                foreach (UserOrder us in userOrder)
                {
                    body += @"" + fundNames[i] + " : " + us.Amount + " Amount/Month " + "\n";
                    i++;
                }
            }

            body += "\n\nFor any equiries, please contact our Customer Service Executive at +603-2095 9999\nThank you for Investing with us.  We look forward to serving you again\n\n";
            body += "This is a computer generated e-mail and no reply is required\n****************************************************************************";

            try
            {
                MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email, Subject, body);
                SmtpClient emailClient = new SmtpClient("103.217.92.136");
                emailClient.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                return false;

            }
        }

        public static Boolean SendHardCopyNotification(int HardCopyStatus, string email, string username)
        {
            string Subject = "";
            string Body = "";

            if (HardCopyStatus == 1)
            {
                Subject = "HardCopy Activation";
                Body = "Hardcopy request\nUser Name : " + username + "\nYou have requested for Hardcopy Reports/Statements to be sent to your mailing address.\n\nHardcopy Reports/Statemetns will be made available to you in the next statement cycle.\n\nFor any enquiries, please contact our Customer Service Executive at +603-2095 9999\nThank you for Investing with us. We look forward to serving you again\n\nThis is a computer generated e-mail and no reply is required\n**************************************************";
            }
            else if (HardCopyStatus == 0)
            {
                Subject = "HardCopy Deactivation";
                Body = "Hardcopy request\nUser Name : " + username + "\nYou have requested for Softcopy Reports/Statements to be sent to your email address.\n\nSoftware Reports/Statemetns will be made available to you in the next statement cycle.\n\nFor any enquiries, please contact our Customer Service Executive at +603-2095 9999\nThank you for Investing with us. We look forward to serving you again\n\nThis is a computer generated e-mail and no reply is required\n**************************************************";
            }

            try
            {
                MailMessage message = new MailMessage("apex-noreply@apexis.com.my", email, Subject, Body);
                SmtpClient emailClient = new SmtpClient("103.217.92.136");
                emailClient.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                return false;

            }
        }
    }
}
