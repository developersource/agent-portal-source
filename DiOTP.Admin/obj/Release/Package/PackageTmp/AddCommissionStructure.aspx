﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCommissionStructure.aspx.cs" Inherits="Admin.AddCommissionStructure" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addCommissionStructureForm" runat="server">
        <div class="row" id="FormId" data-value="addCommissionStructureForm">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Commission Definition:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                        <select name="CommissionDefId" id="CommissionDefId" runat="server" clientidmode="static" class="form-control">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
                
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Apply To:</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList  ID="ddlApplyTo" runat="server" clientIdMode="Static" cssClass="form-control">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="WA">WA</asp:ListItem>
                            <asp:ListItem Value="WM">WM</asp:ListItem>
                            <asp:ListItem Value="GM">GM</asp:ListItem>
                            <asp:ListItem Value="SGM">SGM</asp:ListItem>
                        </asp:DropDownList>
                        <%--<input type="text" name="ApplyTo" id="ApplyTo" runat="server" clientidmode="static" class="form-control" placeholder="Enter Apply To" />--%>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Percentage:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="Percent" id="Percent" runat="server" clientidmode="static" class="form-control" placeholder="Enter Percentage" />
                    </div>
                </div>

            </div>
        </div>
    </form>
</body>
</html>

<script>
    $(document).ready(function () {


    });

</script>
