﻿using DiOTP.Data.IRepo;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class AccountOpeningFinancialProfileRepo : IAccountOpeningFinancialProfileRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(AccountOpeningFinancialProfile obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AccountOpeningFinancialProfile>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<AccountOpeningFinancialProfile> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AccountOpeningFinancialProfile>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(AccountOpeningFinancialProfile obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AccountOpeningFinancialProfile>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<AccountOpeningFinancialProfile> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AccountOpeningFinancialProfile>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public AccountOpeningFinancialProfile DeleteData(Int32 Id)
        {
            Response responseULM = GetSingle(Id);
            if (responseULM.IsSuccess)
            {
                AccountOpeningFinancialProfile obj = (AccountOpeningFinancialProfile)responseULM.Data;
                try
                {
                    string query = obj.ObjectToQuery<AccountOpeningFinancialProfile>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseULM = GetSingle(Id);
                    if (responseULM.IsSuccess)
                    {
                        AccountOpeningFinancialProfile obj = (AccountOpeningFinancialProfile)responseULM.Data;
                        query += obj.ObjectToQuery<AccountOpeningFinancialProfile>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            AccountOpeningFinancialProfile obj = new AccountOpeningFinancialProfile();
            try
            {
                string query = "select * from ao_financial_profiles where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new AccountOpeningFinancialProfile
                       {
                           Id = x.Field<Int32>("id"),
                           AccountOpeningId = x.Field<Int32>("account_opening_id"),
                           Purpose = x.Field<String>("purpose"),
                           PurposeDescription = x.Field<String>("purpose_description"),
                           Source = x.Field<String>("source"),
                           EmployedByFundCompany = x.Field<Int32>("employed_by_fund_company"),
                           Relationship = x.Field<String>("relationship"),
                           NameOfFunder = x.Field<String>("name_of_funder"),
                           FundersIndustory = x.Field<String>("funders_industory"),
                           IsFunderMoneyChanger = x.Field<Int32>("is_funder_money_changer"),
                           IsFunderBeneficialOwner = x.Field<Int32>("is_funder_beneficial_owner"),
                           FundOwnerName = x.Field<String>("fund_owner_name"),
                           EstimatedNetWorth = x.Field<String>("estimated_net_worth"),
                           Status = x.Field<Int32>("status")
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningFinancialProfile> objs = new List<AccountOpeningFinancialProfile>();
            try
            {
                string query = "select * from ao_financial_profiles";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpeningFinancialProfile
                        {
                            Id = x.Field<Int32>("id"),
                            AccountOpeningId = x.Field<Int32>("account_opening_id"),
                            Purpose = x.Field<String>("purpose"),
                            PurposeDescription = x.Field<String>("purpose_description"),
                            Source = x.Field<String>("source"),
                            EmployedByFundCompany = x.Field<Int32>("employed_by_fund_company"),
                            Relationship = x.Field<String>("relationship"),
                            NameOfFunder = x.Field<String>("name_of_funder"),
                           FundersIndustory = x.Field<String>("funders_industory"),
                            IsFunderMoneyChanger = x.Field<Int32>("is_funder_money_changer"),
                            IsFunderBeneficialOwner = x.Field<Int32>("is_funder_beneficial_owner"),
                            FundOwnerName = x.Field<String>("fund_owner_name"),
                            EstimatedNetWorth = x.Field<String>("estimated_net_worth"),
                            Status = x.Field<Int32>("status")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ao_financial_profiles";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpeningFinancialProfile>(propertyName);
            List<AccountOpeningFinancialProfile> objs = new List<AccountOpeningFinancialProfile>();
            try
            {
                string query = "select * from ao_financial_profiles where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from ao_financial_profiles where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpeningFinancialProfile
                        {
                            Id = x.Field<Int32>("id"),
                            AccountOpeningId = x.Field<Int32>("account_opening_id"),
                            Purpose = x.Field<String>("purpose"),
                            PurposeDescription = x.Field<String>("purpose_description"),
                            Source = x.Field<String>("source"),
                            EmployedByFundCompany = x.Field<Int32>("employed_by_fund_company"),
                            Relationship = x.Field<String>("relationship"),
                            NameOfFunder = x.Field<String>("name_of_funder"),
                           FundersIndustory = x.Field<String>("funders_industory"),
                            IsFunderMoneyChanger = x.Field<Int32>("is_funder_money_changer"),
                            IsFunderBeneficialOwner = x.Field<Int32>("is_funder_beneficial_owner"),
                            FundOwnerName = x.Field<String>("fund_owner_name"),
                            EstimatedNetWorth = x.Field<String>("estimated_net_worth"),
                            Status = x.Field<Int32>("status")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpeningFinancialProfile>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ao_financial_profiles where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from ao_financial_profiles where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningFinancialProfile> objs = new List<AccountOpeningFinancialProfile>();
            try
            {
                string query = "select * from ao_financial_profiles where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpeningFinancialProfile
                        {
                            Id = x.Field<Int32>("id"),
                            AccountOpeningId = x.Field<Int32>("account_opening_id"),
                            Purpose = x.Field<String>("purpose"),
                            PurposeDescription = x.Field<String>("purpose_description"),
                            Source = x.Field<String>("source"),
                            EmployedByFundCompany = x.Field<Int32>("employed_by_fund_company"),
                            Relationship = x.Field<String>("relationship"),
                            NameOfFunder = x.Field<String>("name_of_funder"),
                           FundersIndustory = x.Field<String>("funders_industory"),
                            IsFunderMoneyChanger = x.Field<Int32>("is_funder_money_changer"),
                            IsFunderBeneficialOwner = x.Field<Int32>("is_funder_beneficial_owner"),
                            FundOwnerName = x.Field<String>("fund_owner_name"),
                            EstimatedNetWorth = x.Field<String>("estimated_net_worth"),
                            Status = x.Field<Int32>("status")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ao_financial_profiles where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
