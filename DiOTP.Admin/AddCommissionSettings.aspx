﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCommissionSettings.aspx.cs" Inherits="Admin.AddCommissionSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="addCommissionSettingsForm" runat="server">
        <div id="FormId" data-value="addCommissionSettingsForm">
            <div class="row">
                <div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-3">
                            <label class="fs-14 mt-2">Commission Setting:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />                            
                            <input type="text" name="commissionSetting" id="commissionSetting" runat="server" clientidmode="static" class="form-control" placeholder="Enter a Commission Setting" />  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
