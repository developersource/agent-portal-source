﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="OpenJointAccount.aspx.cs" Inherits="DiOTP.WebApp.OpenJointAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">Open Joint Account</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h5 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Open Joint Account</h5>
            </div>

            <div class="row mb-20 mt-20">
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    Account Type
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <asp:DropDownList CssClass="form-control" ID="ddlAccountType" runat="server" Enabled="false">
                        <asp:ListItem>JOINT</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    What is your relationship with the joint holder?
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="form-control">
                        <asp:ListItem Value="">Select your relationship</asp:ListItem>
                        <asp:ListItem Value="Parents">Parents</asp:ListItem>
                        <asp:ListItem Value="Siblings">Siblings</asp:ListItem>
                        <asp:ListItem Value="Spouse">Spouse</asp:ListItem>
                        <asp:ListItem Value="Children (above 18 years old)">Children (above 18 years old)</asp:ListItem>
                        <asp:ListItem Value="Relatives">Relatives</asp:ListItem>
                        <asp:ListItem Value="Friends">Friends</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                    <asp:Button ID="btnNext" runat="server" ClientIDMode="Static" CssClass="btn btn-infos1 pull-right" Text="Next" OnClick="btnNext_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            ShowCustomMessage('Alert', '<ul><li>All transaction/activities to be carried out by "Principal Applicant Only", joint applicant is only allow "To View" function.</li><li>Any person(s) 18 years of age and above can invest in unit trust funds. You may have a Joint Holder on your account. A designated account can be opened for a Joint Holder above 18 years of age.</li><li>The Principle Holder will be solely the Authority to operate Account to effect all the transactions including buy, sell and switching in eApexIs Online Portal.</li></ul>', '');
        });
    </script>
</asp:Content>
