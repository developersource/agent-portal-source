﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="Admin.Settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="SettingsForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 mb-20">
                    <button data-original-title="Change Password" data-trigger="hover" data-placement="bottom" data-content="" type="button" class="btn action-button" data-action="Add" data-url="ChangePassword.aspx" data-title="Change Password"><i class="fa fa-edit"></i> Change Password</button>
                </div>
                <div class="col-md-12">
                    <h5 class="text-uppercase"><u>Background Service Instructions</u></h5>
                    <button id="generateStatusButton" runat="server" clientidmode="static" data-original-title="Generate Monthly Statements / Check Status" data-trigger="hover" data-placement="bottom" data-content="" type="button" class="btn action-button" data-action="Add" data-url="GenerateStatements.aspx" data-title="Generate Monthly Statements / Check Status"><i class="fa fa-file-o"></i> Generate Monthly Statements / Check Status</button>
                    <div class="text-info p-5" id="lastUpdatedMessage" runat="server"></div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
