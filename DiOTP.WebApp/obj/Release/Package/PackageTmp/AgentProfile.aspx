﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AgentMaster.master" CodeBehind="AgentProfile.aspx.cs" Inherits="DiOTP.WebApp.AgentProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>            
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li class="active">Agent Profile</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Agent Profile</h3>
            </div>
            <div id="containerPage">
                <%--<div id="snackbar"></div>--%>
                <style>

                    .trTextFormat {
                        text-align: center;
                    }

                    .agentDashboardHr {
                        margin-top: 20px;
                        margin-bottom: 20px;
                    }

                    .agentDashboardProfDetails {
                        margin-bottom: 5px;
                        border-bottom: 1px solid #808080;
                    }

                    .agentCard {
                        position: relative;
                        display: block;
                        min-width: 0;
                        word-wrap: break-word;
                        /*background-color: ghostwhite;*/
                        background-clip: border-box;
                        border: 0px solid #059CCE;
                        border-radius: .25rem;
                        /*new inserted*/
                        padding-bottom:10px;
                        margin-bottom:20px;
                        width:100%;
                    }

                     .agentDetailTable, tbody.agentDetailTable > tr{
                    
                        border:2px solid grey;
                        
                    }

                     /* new inserted - BEGIN*/
                     hr.lineSeparator{
                         margin: 0;
                     }
                     div.contentRow{
                         padding:10px 15px;
                         line-height:2em;
                         
                     }
                     .imgPhoto{
                         height:140px;
                         width:140px;
                         border-radius:50%;
                         object-fit:contain;
                         box-shadow: 0 0px 10px 0 rgb(0 0 0 / 20%), 0 6px 10px 0 rgb(0 0 0 / 10%) !important;
                     }
                     .profileSection{
                         font-size:18px;
                         font-weight:500;
                         background-color:rgba(5, 156, 206, 1);
                         color:#fff;
                         padding:5px 15px;
                         border-radius:3px;
                     }
                     .contentSection{
                         background-color:ghostwhite;
                         padding:0 15px 15px;
                     }
                     @media (max-width: 767px){
                         .agentCard{
                             background-color: #313131;
                         }
                         .agentCard>.row{ 
                             color: #929292;
                             font-weight: 600;
                             font-size: 13px;
                         }
                         hr.lineSeparator{
                             border: 1px solid grey;
                         }
                        .imgPhoto{
                             height:100px;
                             width:100px;
                             box-shadow: 0 0px 10px 0 rgb(0 0 0 / 20%), 0 6px 10px 0 rgb(0 0 0 / 10%) !important;
                        }
                        .contentSection {
                            background-color:transparent;
                        }
                     }
                     /* new inserted - END*/
                </style>

                <%--Segment 1--%>
                <div class="container agentCard">
                    
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="row contentRow">
                                <%--Agent Image--%>
                                <div class="col-sm-6">
                                    <asp:Image ID="agentImage" class="imgPhoto" runat="server" ImageUrl="../Content/MyImage/corporate-photoshoot-001.jpg"/>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 profileSection">
                            <%--<h4>My Personal Details</h4>--%>
                            <span>My Personal Details</span>
                            <%--<span class="ml-4"><button id="btnUpdate" style="background-color:transparent;border:none;" OnClientClick='return false;'><i class="fa fa-edit"></i></button></span>--%>
                            
                        </div>
                    </div>

                    <%--BEGIN 20211026I--%>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="row contentSection">
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div>Agent Name:</div>
                                        <div>
                                            <b><asp:Label ID="lblAgentName" Text="Albert Teng Hau Tuong" runat="server"></asp:Label></b>
                                            <asp:TextBox ID="txtAgentName" Text="Albert Teng Hau Tuong" runat="server" style="display:none;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Agent Code:</div>
                                        <div><b><asp:Label ID="lblAgentCode" Text="Code 1234" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Agent Rank:</div>
                                        <div><b><asp:Label ID="lblRank" Text="Rank 1" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Join Date:</div>
                                        <div><b><asp:Label ID="lblJoinDate" Text="25/10/2021" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Agency Group:</div>
                                        <div><b><asp:Label ID="lblAgencyGroup" Text="Galaxy Agency Group" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                </div>
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div>Region/Branch:</div>
                                        <div><b><asp:Label ID="lblRegion" Text="Region/Branch" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Business Development Manager:</div>
                                        <div><b><asp:Label ID="lblBDM" Text="Ms. Clarkson, 012-8752613" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Direct Downline Count:</div>
                                        <div><b><asp:Label ID="lblDirectDownline" Text="Albert Teng Hau Tuong" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                    <div class="row contentRow">
                                        <div>Indirect Downline Count:</div>
                                        <div><b><asp:Label ID="lblIndirectDownline" Text="Albert Teng Hau Tuong" runat="server"></asp:Label></b></div>
                                    </div>
                                    <hr class="lineSeparator"/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container agentCard">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 profileSection">
                            <span>My Upline Agent Details</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 contentSection">
                            <div class="row contentRow">
                                <div class="col-sm-6">Upline Name:</div>
                                <div class="col-sm-6"><b><asp:Label ID="lblUplineName" Text="Albert Teng Hau Tuong" runat="server"></asp:Label></b></div>   
                            </div>
                            <hr class="lineSeparator">

                            <div class="row contentRow">
                                <div class="col-sm-6">Rank:</div>
                                <div class="col-sm-6"><b><asp:Label ID="lblUplineRank" Text="Rank 2" runat="server"></asp:Label></b></div>
                            </div>
                            <hr class="lineSeparator">

                            <div class="row contentRow">
                                <div class="col-sm-6">Mobile No:</div>
                                <div class="col-sm-6"><b><asp:Label ID="lblUplineMobileNo" Text="0123456789" runat="server"></asp:Label></b></div>
                            </div>
                            <hr class="lineSeparator">
                        </div>
                    </div>
                </div>


                <div class="container agentCard">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 profileSection">
                            <span>My Address</span>
                            <span class="ml-4"><button id="btnUpdateAddress" style="background-color:transparent;border:none;" OnClick='editMode(true); return false;'><i class="fa fa-edit"></i></button></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="row contentSection">
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>Address Line 1</label><sup class="text-danger" id="addr1">*</sup>
                                            <asp:TextBox ID="txtAddressLine1" runat="server" CssClass="form-control" placeholder="Address Line 1" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hdnAddressLine1" runat="server" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>Address Line 2</label><sup class="text-danger" id="addr2">*</sup>
                                            <asp:TextBox ID="txtAddressLine2" runat="server" CssClass="form-control" placeholder="Address Line 2" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hdnAddressLine2" runat="server" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>Address Line 3</label>
                                            <asp:TextBox ID="txtAddressLine3" runat="server" CssClass="form-control" placeholder="Address Line 3" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hdnAddressLine3" runat="server" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>Address Line 4</label>
                                            <asp:TextBox ID="txtAddressLine4" runat="server" CssClass="form-control" placeholder="Address Line 4" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hdnAddressLine4" runat="server" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>Postcode:</label><sup class="text-danger" id="pcode">*</sup>
                                            <asp:TextBox ID="txtPostCode" runat="server" CssClass="form-control" placeholder="Postcode" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hdnPostCode" runat="server" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>State</label><sup class="text-danger" id="state">*</sup>
                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnState" runat="server" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row contentRow">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row pt-20">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="row contentRow">
                                        <div class="col-sm-12">
                                            <%--<asp:Button ID="btnSaveProfile" Text="Save Changes" runat="server" CssClass="btn btn-sett-confirm" style="display:none;"/>--%>
                                            <input type="submit" id="btnSaveProfile" text="Save Changes" runat="server" class="btn btn-sett-confirm" style="display:none;" onclick="submitUpdate();"/>
                                            <asp:HiddenField ID="hdnUpdateMessage" runat="server" ClientIDMode="Static" />
                                            <button type="button" id="btnSaveCancel" class="btn btn-sett-cancel" style="display:none;" onclick="editMode(false);return false;">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        </div>

        <%--<script src="Content/mycj/common-scripts.js?v=1.0"></script>--%>
        
        <script src="/Content/js/highcharts.js"></script>
        <script src="Content/assets/js/jquery.min.js"></script>

        
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">

            <script>
            function editMode(flag) {
                if (flag) {
                    $('#<%=txtAddressLine1.ClientID%>').removeAttr("disabled");
                    $('#<%=txtAddressLine2.ClientID%>').removeAttr("disabled");
                    $('#<%=txtAddressLine3.ClientID%>').removeAttr("disabled");
                    $('#<%=txtAddressLine4.ClientID%>').removeAttr("disabled");
                    $('#<%=txtPostCode.ClientID%>').removeAttr("disabled");
                    $('#<%=ddlState.ClientID%>').removeAttr("disabled");
                    $('#<%=btnSaveProfile.ClientID%>').removeAttr("style");
                    $('#btnSaveCancel').removeAttr("style");
                }
                else {
                    $('#<%=txtAddressLine1.ClientID%>').attr("disabled", "disabled"); 
                    $('#<%=txtAddressLine2.ClientID%>').attr("disabled", "disabled"); 
                    $('#<%=txtAddressLine3.ClientID%>').attr("disabled", "disabled"); 
                    $('#<%=txtAddressLine4.ClientID%>').attr("disabled", "disabled");
                    $('#<%=txtPostCode.ClientID%>').attr("disabled", "disabled");
                    $('#<%=ddlState.ClientID%>').attr("disabled", "disabled");
                    $('#<%=btnSaveProfile.ClientID%>').attr("style", "display:none");
                    $('#btnSaveCancel').attr("style", "display:none");
                }
            }
            <%--function saveProfile() {
                editMode(false);
                return false;
            }
            $('#<%=btnSaveProfile.ClientID%>').on('click', function () {
                saveProfile();
            });--%>
                function showSnackBar(message) {
                    console.log('clicked');
                    var x = $("#snackbar");
                    x.html(message);
                    x.addClass('show');
                    setTimeout(function () {
                        x.className = x.removeClass('show');
                    }, 6000); // 3000 = 3 seconds
                }

                function submitUpdate() {
                    
                    $.ajax({

                        url: "AgentProfile.aspx/UpdateAddress",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: {
                            addressline1: JSON.stringify($('#txtAddressLine1').val()), addressline2: JSON.stringify($('#txtAddressLine2').val()), addressline3: JSON.stringify($('#txtAddressLine3').val()), addressline4: JSON.stringify($('#txtAddressLine4').val()), postCode: JSON.stringify($('#txtPostCode').val()), stateCode: JSON.stringify($('#ddlState').val()), countryCode: JSON.stringify($('#ddlCountry').val())
                            //addressline1: JSON.stringify($('#txtAddressLine1'))
                        },
                        success: function (data, response) {

                            var response = data.d;
                            console.log(response);
                            if (response.IsSuccess) {
                                if (data.d.IsSuccess) {
                                    editMode(false);
                                    showSnackBar(response.Message);
                                }
                            }
                            else {
                                //$('.loadingDivAdmin').hide();
                                showSnackBar(response.Message);
                                //showSnackBar('Updated');
                            }
                        },
                    });
                }

            //$(document).ready(function () {
                

            //    $("#btnSaveProfile").submit(function () {
            //        console.log('clicked');
            //        //ar keyCode = e.keyCode || e.which;
            //        //if (keyCode == 9) {
            //        //e.preventDefault();
            //        // call custom function here
            //        $.ajax({

            //            url: "AgentProfile.aspx/UpdateAddress",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "GET",
            //            dataType: "JSON",
            //            data: {
            //                agentCode: JSON.stringify($('#txtIntroCode').val()), addressline1: JSON.stringify($('#txtAddressline1').val()), addressline2: JSON.stringify($('#txtAddressline2').val()),
            //                addressline3: JSON.stringify($('#txtAddressline3').val()), addressline4: JSON.stringify($('#txtAddressline4').val()), postCode: JSON.stringify($('#txtPostCode').val()),
            //                stateCode: JSON.stringify($('#ddlState').val()), countryCode: JSON.stringify($('#ddlCountry').val()),
            //            },
            //            success: function (data, response) {

            //                var response = data.d;
            //                console.log(response);
            //                if (response.IsSuccess) {
            //                    if (data.d.IsSuccess) {
            //                        editMode(false);
            //                        showSnackBar(response.Message);
            //                    }
            //                }
            //                else {
            //                    //$('.loadingDivAdmin').hide();
            //                    showSnackBar(response.Message);
            //                    //showSnackBar('Updated');
            //                }
            //            },
            //        });
            //    });
            //});
        </script>
</asp:Content>
