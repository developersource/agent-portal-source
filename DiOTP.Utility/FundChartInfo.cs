using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("fund_chart_info")]
    public class FundChartInfo
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("data_value"), Display("DataValue")]
        public Int32 DataValue { get; set; }
        [Database("fund_id"), Display("FundId")]
        public Int32 FundId { get; set; }
        [Database("fund_name"), Display("FundName")]
        public String FundName { get; set; }
        [Database("total_returns"), Display("TotalReturns")]
        public Decimal TotalReturns { get; set; }
        [Database("average"), Display("Average")]
        public Decimal Average { get; set; }
        [Database("three_year_annualised_percent"), Display("ThreeYearAnnualisedPercent")]
        public String ThreeYearAnnualisedPercent { get; set; }
        [Database("min_change"), Display("MinChange")]
        public Decimal MinChange { get; set; }
        [Database("max_change"), Display("MaxChange")]
        public Decimal MaxChange { get; set; }
        [Database("min_nav"), Display("MinNav")]
        public Decimal MinNav { get; set; }
        [Database("max_nav"), Display("MaxNav")]
        public Decimal MaxNav { get; set; }
        [Database("current_nav"), Display("CurrentNav")]
        public Decimal CurrentNav { get; set; }
        [Database("current_nav_date"), Display("CurrentNavDate")]
        public String CurrentNavDate { get; set; }
        [Database("labels"), Display("Labels")]
        public String Labels { get; set; }
        [Database("nav_changes"), Display("NavChanges")]
        public String NavChanges { get; set; }
        [Database("nav_prices"), Display("NavPrices")]
        public String NavPrices { get; set; }
        [Database("nav_dates"), Display("NavDates")]
        public String NavDates { get; set; }
    }
}
