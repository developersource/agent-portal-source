﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class OnlineReporting : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try { 
            if (Session["user"] == null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                }
            }
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                    foreach (UserAccount userAccount in userAccounts)
                    {
                        ddlFundAccount.Items.Add(new ListItem(userAccount.AccountNo, userAccount.Id.ToString()));
                    }
                    ddlTransactionType.Items.Add("Buy");
                    ddlTransactionType.Items.Add("Sell");
                    ddlTransactionType.Items.Add("Switch");
                    ddlTransactionType.Items.Add("Transfer");
                    ddlTransactionType.Items.Add("Cooling Off");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                }
            }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void GenerateTransactionHistory()
        {
            try
            {
                if (!(txtDateStart.Text.Equals("") && txtDateEnd.Text.Equals("")))
                {
                    DateTime startDate = Convert.ToDateTime(txtDateStart.Text);
                    DateTime endDate = Convert.ToDateTime(txtDateEnd.Text);
                    string selectedAccount = ddlFundAccount.SelectedItem.Text;
                    string type = ddlTransactionType.Text;

                    List<TransactionByDate> transactionByDateList = new List<TransactionByDate>();
                    if (type.Equals("buy", StringComparison.InvariantCultureIgnoreCase))
                    {
                        transactionByDateList = GenerateStatementService.GetAllTransaction(startDate, endDate, selectedAccount).Where(x => x.Transaction_Code == "NS" || x.Transaction_Code == "XS" || x.Transaction_Code == "DV").ToList();
                    }
                    else if (type.Equals("sell", StringComparison.InvariantCultureIgnoreCase))
                    {
                        transactionByDateList = GenerateStatementService.GetAllTransaction(startDate, endDate, selectedAccount).Where(x => x.Transaction_Code == "RE" || x.Transaction_Code == "XR").ToList();
                    }
                    else if (type.Equals("switch", StringComparison.InvariantCultureIgnoreCase))
                    {
                        transactionByDateList = GenerateStatementService.GetAllTransaction(startDate, endDate, selectedAccount).Where(x => x.Transaction_Code == "SI" || x.Transaction_Code == "SO" || x.Transaction_Code == "XI" || x.Transaction_Code == "XO").ToList();
                    }
                    else if (type.Equals("transfer", StringComparison.InvariantCultureIgnoreCase))
                    {
                        transactionByDateList = GenerateStatementService.GetAllTransaction(startDate, endDate, selectedAccount).Where(x => x.Transaction_Code == "TO" || x.Transaction_Code == "TI" || x.Transaction_Code == "RI" || x.Transaction_Code == "RO").ToList();
                    }
                    else if (type.Equals("cooling off", StringComparison.InvariantCultureIgnoreCase))
                    {
                        transactionByDateList = GenerateStatementService.GetAllTransaction(startDate, endDate, selectedAccount).Where(x => x.Transaction_Code == "XC").ToList();
                    }

                    if (transactionByDateList.Count > 0)
                    {
                        transactionByDateList = transactionByDateList.OrderByDescending(x => x.Date_Of_Transaction).ToList();

                        StringBuilder sb = new StringBuilder();
                        sb.Append(
                                @"<thead><tr>
                                <th align='center'>Trans Date</th>
                                <th align='center'>Processed Date</th>
                                <th align='center'>Trust Code</th>
                                <th align='center'>Trans No</th>
                                <th align='center'>Number of Units</th>
                                <th align='center'>Unit Cost (RM)</th>
                                <th align='center'>Trans Amount (RM)</th>
                                <th align='center'>Servicing Agent</th>
                                </tr></thead></br>"
                                );

                        foreach (TransactionByDate transaction in transactionByDateList)
                        {
                            sb.Append(
                                @"<tr>
                                <td align='center'>" + transaction.Date_Of_Transaction.ToString("dd/MM/yyyy") + @"</td>
                                <td align='center'>" + transaction.Date_Of_Settlement.ToString("dd/MM/yyyy") + @"</td>
                                <td align='center'>" + transaction.Fund_Code.ToString() + @"</td>
                                <td align='center'>" + (transaction.Transaction_Code + transaction.IPD_Unique_Transaction_ID) + @"</td>
                                <td align='center'>" + transaction.Units.ToString("#,##0.0000") + @"</td>
                                <td align='center'>" + transaction.Unit_Cost_RM.ToString("#,##0.000000000") + @"</td>
                                <td align='center'>" + transaction.Gross_Amount_RM.ToString("#,##0.00") + @"</td>
                                <td align='center'>" + "000000" + @"</td>
                            </tr>");
                        }
                        tbodyTransactionHistory.InnerHtml = sb.ToString();
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append(
                                @"<thead><tr>
                                <th>Trans Date</th>
                                <th>Processed Date</th>
                                <th>Trust Code</th>
                                <th>Trans No</th>
                                <th>Number of Units</th>
                                <th>Unit Cost (RM)</th>
                                <th>Trans Amount (RM)</th>
                                <th>Servicing Agent</th>
                                </tr></thead></br>
                            <tr>
                                <td colspan='7' align='center'>No transaction found.</td>
                            </tr>"
                                );

                        tbodyTransactionHistory.InnerHtml = sb.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Onlinereporting Page GenerateTransactionHistory: " + ex.Message);
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            GenerateTransactionHistory();
        }
    }
}