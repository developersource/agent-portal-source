﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateStatements.aspx.cs" Inherits="Admin.GenerateStatements" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="generateStatementsForm" runat="server">
        <style>
            .modal-footer #btnCommonModalSubmit{
                display: none;
            }
            .modal-footer [data-dismiss="modal"] {
                display: none;
            }
        </style>
        <div class="row" id="FormId" data-value="generateStatementsForm">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-12" id="generateStatus" runat="server">
                        Process handovered to background service. Statements will be generated soon. Please check after sometime for the status.
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
