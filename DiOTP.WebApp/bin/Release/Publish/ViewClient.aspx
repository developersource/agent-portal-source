﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="ViewClient.aspx.cs" Inherits="DiOTP.WebApp.ViewClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        .card {
            padding-top: 20px;
            margin: 10px 0 20px 0;
            background-color: rgba(214, 224, 226, 0.2);
            border-top-width: 0;
            border-bottom-width: 2px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .card .card-heading {
                padding: 0 20px;
                margin: 0;
            }

                .card .card-heading.simple {
                    font-size: 20px;
                    font-weight: 300;
                    color: #777;
                    border-bottom: 1px solid #e5e5e5;
                }

                .card .card-heading.image img {
                    display: inline-block;
                    width: 46px;
                    height: 46px;
                    margin-right: 15px;
                    vertical-align: top;
                    border: 0;
                    -webkit-border-radius: 50%;
                    -moz-border-radius: 50%;
                    border-radius: 50%;
                }

                .card .card-heading.image .card-heading-header {
                    display: inline-block;
                    vertical-align: top;
                }

                    .card .card-heading.image .card-heading-header h3 {
                        margin: 0;
                        font-size: 14px;
                        line-height: 16px;
                        color: #262626;
                    }

                    .card .card-heading.image .card-heading-header span {
                        font-size: 12px;
                        color: #999999;
                    }

            .card .card-body {
                padding: 0 20px;
                margin-top: 20px;
            }

            .card .card-media {
                padding: 0 20px;
                margin: 0 -14px;
            }

                .card .card-media img {
                    max-width: 100%;
                    max-height: 100%;
                }

            .card .card-actions {
                min-height: 30px;
                padding: 0 20px 20px 20px;
                margin: 20px 0 0 0;
            }

            .card .card-comments {
                padding: 20px;
                margin: 0;
                background-color: #f8f8f8;
            }

                .card .card-comments .comments-collapse-toggle {
                    padding: 0;
                    margin: 0 20px 12px 20px;
                }

                    .card .card-comments .comments-collapse-toggle a,
                    .card .card-comments .comments-collapse-toggle span {
                        padding-right: 5px;
                        overflow: hidden;
                        font-size: 12px;
                        color: #999;
                        text-overflow: ellipsis;
                        white-space: nowrap;
                    }

        .card-comments .media-heading {
            font-size: 13px;
            font-weight: bold;
        }

        .card.people {
            position: relative;
            display: inline-block;
            width: 170px;
            height: 300px;
            padding-top: 0;
            margin-left: 20px;
            overflow: hidden;
            vertical-align: top;
        }

            .card.people:first-child {
                margin-left: 0;
            }

            .card.people .card-top {
                position: absolute;
                top: 0;
                left: 0;
                display: inline-block;
                width: 170px;
                height: 150px;
                background-color: #ffffff;
            }

                .card.people .card-top.green {
                    background-color: #53a93f;
                }

                .card.people .card-top.blue {
                    background-color: #427fed;
                }

            .card.people .card-info {
                position: absolute;
                top: 150px;
                display: inline-block;
                width: 100%;
                height: 101px;
                overflow: hidden;
                background: #ffffff;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

                .card.people .card-info .title {
                    display: block;
                    margin: 8px 14px 0 14px;
                    overflow: hidden;
                    font-size: 16px;
                    font-weight: bold;
                    line-height: 18px;
                    color: #404040;
                }

                .card.people .card-info .desc {
                    display: block;
                    margin: 8px 14px 0 14px;
                    overflow: hidden;
                    font-size: 12px;
                    line-height: 16px;
                    color: #737373;
                    text-overflow: ellipsis;
                }

            .card.people .card-bottom {
                position: absolute;
                bottom: 0;
                left: 0;
                display: inline-block;
                width: 100%;
                padding: 10px 20px;
                line-height: 29px;
                text-align: center;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

        .card.hovercard {
            position: relative;
            padding-top: 0;
            overflow: hidden;
            text-align: center;
            background-color: rgba(214, 224, 226, 0.2);
        }

            .card.hovercard .cardheader {
                background-color: #059cce;
                /*                background: url("http://lorempixel.com/850/280/nature/4/");*/
                background-size: cover;
                height: 135px;
            }

            .card.hovercard .avatar {
                position: relative;
                top: -50px;
                margin-bottom: -50px;
            }

                .card.hovercard .avatar img {
                    width: 100px;
                    height: 100px;
                    max-width: 100px;
                    max-height: 100px;
                    -webkit-border-radius: 50%;
                    -moz-border-radius: 50%;
                    border-radius: 50%;
                    border: 5px solid rgba(255,255,255,0.8);
                    background: #fff;
                }

            .card.hovercard .info {
                padding: 4px 8px 10px;
            }

                .card.hovercard .info .title {
                    margin-bottom: 10px;
                    font-size: 24px;
                    line-height: 1;
                    color: #262626;
                    vertical-align: middle;
                }

                .card.hovercard .info .desc {
                    margin-bottom: 5px;
                    overflow: hidden;
                    font-size: 14px;
                    line-height: 24px;
                    color: #333333;
                    text-overflow: ellipsis;
                }

            .card.hovercard .bottom {
                padding: 10px 20px;
                background: #3f3f3f;
                color: #fff;
            }
            table td:first-child {
                vertical-align: middle !important;
            }
            table tr td[rowspan]:nth-child(2) {
                border-right: 2px solid #ddd !important;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/AgentDashboard.aspx">Dashboard</a></li>
                    <li><a href="/AgentClients.aspx">My Clients</a></li>
                    <li class="active">View Client</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">View Client</h3>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12" id="divViewClientHTML" runat="server">
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12" id="divViewAccountSummaryHTML" runat="server">
                    <a style='width:70px;' class='btn btn-sm btn-primary hide' id="lnkInvestNew" runat="server" href='/BuyFunds.aspx?ClientId=501&fundCode=AMGT&AccountNo=36968'>New Investment</a>
                    <table class="table table-condensed table-bordered dataTable table-cell-pad-5" id="investmentTable">
                        <thead>
                            <tr class="fs-13">
                                <th class="">No</th>
                                <th class="">Holder<br />No</th>
                                <th class="">Fund<br />Name</th>
                                <th class="text-right">Total<br />Units</th>
                                <th class="text-right">Actual Cost<br />MYR</th>
                                <%--<th class="text-right">Avg. Cost<br />MYR</th>--%>
                                <th class="text-right">Market Value<br />MYR</th>
                                <th class="text-right">NAV Price<br />MYR</th>
                                <th class="text-right">Unrealised Profit/Loss<br />MYR</th>
                                <th class="text-right">Unrealised Profit/Loss<br />%</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="fs-12" id="investmentPortfolioTbody" runat="server">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="Content/js/autoNumeric.js"></script>
    <script>
        function FormatAllCurrency() {
            $('.currencyFormat, .currencyFormatNoSymbol').each(function (i) {
                var self = $(this);
                try {
                    var v = self.autoNumeric('get');
                    self.autoNumeric('destroy');
                    self.val(v);
                } catch (err) {
                    //console.log("Not an autonumeric field: " + self.attr("name"));
                }
            });
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
    </script>
</asp:Content>
