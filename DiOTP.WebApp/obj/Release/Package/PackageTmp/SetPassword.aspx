﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="SetPassword.aspx.cs" Inherits="DiOTP.WebApp.SetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section image">
        <div class="inner">
            <div class="container">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4 col-sm-6">
                                <div class="Register-section">
                                    <div class="register-head">
                                        <h4><strong>Set Password</strong></h4>
                                    </div>
                                    <div class="register-content">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="width: 40px;"><i class="fa fa-key fa" aria-hidden="true"></i></span>
                                            <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your New Password"></asp:TextBox>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" style="width: 40px;"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                                            <asp:TextBox TextMode="Password" ID="txtConfirmPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Confirm your Password"></asp:TextBox>
                                        </div>
                                        <div class="form-group mb-20 pb-10">
                                            <asp:Button ID="btnSetPassword" runat="server" CssClass="btn login-btn pull-right" Text="Submit" OnClientClick="return clientPasswordValidation()" OnClick="btnSetPassword_Click" />
                                            <button type="button" class="btn pull-right" onclick="javascript:window.location.href='Index.aspx'">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-6">
                                <div id="divMessage" runat="server"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">

    <script type="text/javascript">
        function Showalert() {
            window.close();
        }

        function clientPasswordValidation() {
            var isValid = true;
            var message = "";
            debugger;
            $('#txtPassword,#txtConfirmPassword').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": "LightBlue"
                    });
                }
            });

            // Validate capital letters
            var hasLowerChar = /[a-z]/g;
            var hasUpperChar = /[A-Z]/g;
            if (hasLowerChar.test($('#txtPassword').val()) == false) {
                message += "New Password must contain at least one lowercase alphabet letter.<br/>";
                isValid = false;
            }
            if (hasUpperChar.test($('#txtPassword').val()) == false) {
                message += "New Password must contain at least one uppercase alphabet letter.<br/>";
                isValid = false;
            }

            // Validate numbers
            var numbers = /[0-9]/g;
            if (!$('#txtPassword').val().match(numbers)) {
                message += "New Password must contain at least one number.<br/>";
                isValid = false;
            }

            // Validate length
            if (!($('#txtPassword').val().length >= 8 && $('#txtPassword').val().length <= 16)) {
                message += "New Password length must between 8 to 16 characters.<br/>";
                isValid = false;
            }

            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                message += "New Password and Confirm New Password must be the same.<br/>";
                isValid = false;
            }

            if (isValid == false) {
                ShowCustomMessage('Alert', message, '');
                return false;
            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            // Validate lowercase letters (future upgrade)
            //var lowerCaseLetters = /[a-z]/g;
            //if (!$('#txtPassword').val().match(lowerCaseLetters)) {
            //    ShowCustomMessage('Alert', 'New Password must contain at least one lowercase letter', '');
            //    return false;
            //}
            // Validate capital letters (future upgrade)
            //var upperCaseLetters = /[A-Z]/g;
            //if (!$('#txtPassword').val().match(upperCaseLetters)) {
            //    ShowCustomMessage('Alert', 'New Password must contain at least one uppercase letter', '');
            //    return false;
            //}

        }
    </script>

</asp:Content>
