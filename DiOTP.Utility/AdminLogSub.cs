﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("admin_log_sub")]
    public class AdminLogSub
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("admin_log_main_id"), Display("AdminLogMainId")]
        public Int32 AdminLogMainId { get; set; }
        [Database("column_name"), Display("ColumnName")]
        public string ColumnName { get; set; }
        [Database("value_new"), Display("ValueNew")]
        public string ValueNew { get; set; }
        [Database("value_old"), Display("ValueOld")]
        public string ValueOld { get; set; }
    }
}
