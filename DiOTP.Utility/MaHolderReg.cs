using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ma_holder_reg")]
    public class MaHolderReg
    {
        public List<UserAccount> MaHolderRegIdUserAccounts { get; set; }
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("id_no"), Display("IdNo")]
        public String IdNo { get; set; }
        [Database("id_no_old"), Display("IdNoOld")]
        public String IdNoOld { get; set; }
        [Database("id_no_2"), Display("IdNo2")]
        public String IdNo2 { get; set; }
        [Database("id_no_old_2"), Display("IdNoOld2")]
        public String IdNoOld2 { get; set; }
        [Database("id_no_3"), Display("IdNo3")]
        public String IdNo3 { get; set; }
        [Database("id_no_old_3"), Display("IdNoOld3")]
        public String IdNoOld3 { get; set; }
        [Database("id_no_4"), Display("IdNo4")]
        public String IdNo4 { get; set; }
        [Database("id_no_old_4"), Display("IdNoOld4")]
        public String IdNoOld4 { get; set; }
        [Database("id_no_5"), Display("IdNo5")]
        public String IdNo5 { get; set; }
        [Database("id_no_old_5"), Display("IdNoOld5")]
        public String IdNoOld5 { get; set; }
        [Database("holder_no"), Display("HolderNo")]
        public Int32? HolderNo { get; set; }
        [Database("title"), Display("Title")]
        public String Title { get; set; }
        [Database("name_1"), Display("Name1")]
        public String Name1 { get; set; }
        [Database("name_2"), Display("Name2")]
        public String Name2 { get; set; }
        [Database("name_3"), Display("Name3")]
        public String Name3 { get; set; }
        [Database("name_4"), Display("Name4")]
        public String Name4 { get; set; }
        [Database("name_5"), Display("Name5")]
        public String Name5 { get; set; }
        [Database("addr_1"), Display("Addr1")]
        public String Addr1 { get; set; }
        [Database("addr_2"), Display("Addr2")]
        public String Addr2 { get; set; }
        [Database("addr_3"), Display("Addr3")]
        public String Addr3 { get; set; }
        [Database("addr_4"), Display("Addr4")]
        public String Addr4 { get; set; }
        [Database("postcode"), Display("Postcode")]
        public Int32? Postcode { get; set; }
        [Database("tel_no"), Display("TelNo")]
        public String TelNo { get; set; }
        [Database("region_code"), Display("RegionCode")]
        public Int32? RegionCode { get; set; }
        [Database("state_code"), Display("StateCode")]
        public Int32? StateCode { get; set; }
        [Database("country_issue"), Display("CountryIssue")]
        public String CountryIssue { get; set; }
        [Database("country_issue1"), Display("CountryIssue1")]
        public String CountryIssue1 { get; set; }
        [Database("country_issue2"), Display("CountryIssue2")]
        public String CountryIssue2 { get; set; }
        [Database("country_issue3"), Display("CountryIssue3")]
        public String CountryIssue3 { get; set; }
        [Database("country_res"), Display("CountryRes")]
        public String CountryRes { get; set; }
        [Database("country_incorp"), Display("CountryIncorp")]
        public String CountryIncorp { get; set; }
        [Database("nationality"), Display("Nationality")]
        public String Nationality { get; set; }
        [Database("race"), Display("Race")]
        public String Race { get; set; }
        [Database("occ_code"), Display("OccCode")]
        public String OccCode { get; set; }
        [Database("corp_status"), Display("CorpStatus")]
        public String CorpStatus { get; set; }
        [Database("business_type"), Display("BusinessType")]
        public String BusinessType { get; set; }
        [Database("contact_person"), Display("ContactPerson")]
        public String ContactPerson { get; set; }
        [Database("position_held"), Display("PositionHeld")]
        public String PositionHeld { get; set; }
        [Database("account_type"), Display("AccountType")]
        public String AccountType { get; set; }
        [Database("div_pymt"), Display("DivPymt")]
        public String DivPymt { get; set; }
        [Database("holder_cls"), Display("HolderCls")]
        public String HolderCls { get; set; }
        [Database("holder_ind"), Display("HolderInd")]
        public String HolderInd { get; set; }
        [Database("holder_status"), Display("HolderStatus")]
        public String HolderStatus { get; set; }
        [Database("reg_dt"), Display("RegDt")]
        public String RegDt { get; set; }
        [Database("reg_brn"), Display("RegBrn")]
        public String RegBrn { get; set; }
        [Database("birth_dt"), Display("BirthDt")]
        public String BirthDt { get; set; }
        [Database("sex"), Display("Sex")]
        public String Sex { get; set; }
        [Database("tel_no_2"), Display("TelNo2")]
        public String TelNo2 { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("agent_type"), Display("AgentType")]
        public String AgentType { get; set; }
        [Database("agent_id"), Display("AgentId")]
        public Int32? AgentId { get; set; }
        [Database("income_tax_no"), Display("IncomeTaxNo")]
        public String IncomeTaxNo { get; set; }
        [Database("hand_phone_no"), Display("HandPhoneNo")]
        public String HandPhoneNo { get; set; }
        [Database("off_phone_no"), Display("OffPhoneNo")]
        public String OffPhoneNo { get; set; }
        [Database("prmnt_addr_1"), Display("PrmntAddr1")]
        public String PrmntAddr1 { get; set; }
        [Database("prmnt_addr_2"), Display("PrmntAddr2")]
        public String PrmntAddr2 { get; set; }
        [Database("prmnt_addr_3"), Display("PrmntAddr3")]
        public String PrmntAddr3 { get; set; }
        [Database("prmnt_addr_4"), Display("PrmntAddr4")]
        public String PrmntAddr4 { get; set; }
        [Database("prmnt_post_code"), Display("PrmntPostCode")]
        public Int32? PrmntPostCode { get; set; }
        [Database("prmnt_region"), Display("PrmntRegion")]
        public Int32? PrmntRegion { get; set; }
        [Database("prmnt_state"), Display("PrmntState")]
        public String PrmntState { get; set; }
        [Database("salutation"), Display("Salutation")]
        public String Salutation { get; set; }
        [Database("h_acc_type"), Display("HAccType")]
        public String HAccType { get; set; }
        [Database("marital"), Display("Marital")]
        public String Marital { get; set; }
        [Database("no_of_dpndnt"), Display("NoOfDpndnt")]
        public Int32? NoOfDpndnt { get; set; }
        [Database("rcv_material"), Display("RcvMaterial")]
        public String RcvMaterial { get; set; }
        [Database("material_lan"), Display("MaterialLan")]
        public String MaterialLan { get; set; }
        [Database("field_1"), Display("Field1")]
        public String Field1 { get; set; }
        [Database("field_2"), Display("Field2")]
        public String Field2 { get; set; }
        [Database("field_3"), Display("Field3")]
        public String Field3 { get; set; }
        [Database("field_4"), Display("Field4")]
        public String Field4 { get; set; }
        [Database("field_5"), Display("Field5")]
        public String Field5 { get; set; }
        [Database("field_6"), Display("Field6")]
        public String Field6 { get; set; }
        [Database("field_7"), Display("Field7")]
        public String Field7 { get; set; }
        [Database("field_8"), Display("Field8")]
        public String Field8 { get; set; }
        [Database("field_9"), Display("Field9")]
        public String Field9 { get; set; }
        [Database("field_10"), Display("Field10")]
        public String Field10 { get; set; }
        [Database("field_desc_1"), Display("FieldDesc1")]
        public String FieldDesc1 { get; set; }
        [Database("field_desc_2"), Display("FieldDesc2")]
        public String FieldDesc2 { get; set; }
        [Database("field_desc_3"), Display("FieldDesc3")]
        public String FieldDesc3 { get; set; }
        [Database("field_desc_4"), Display("FieldDesc4")]
        public String FieldDesc4 { get; set; }
        [Database("field_desc_5"), Display("FieldDesc5")]
        public String FieldDesc5 { get; set; }
        [Database("field_desc_6"), Display("FieldDesc6")]
        public String FieldDesc6 { get; set; }
        [Database("field_desc_7"), Display("FieldDesc7")]
        public String FieldDesc7 { get; set; }
        [Database("field_desc_8"), Display("FieldDesc8")]
        public String FieldDesc8 { get; set; }
        [Database("field_desc_9"), Display("FieldDesc9")]
        public String FieldDesc9 { get; set; }
        [Database("field_desc_10"), Display("FieldDesc10")]
        public String FieldDesc10 { get; set; }
        [Database("last_disb_dt"), Display("LastDisbDt")]
        public DateTime LastDisbDt { get; set; }
        [Database("next_disb_dt"), Display("NextDisbDt")]
        public DateTime NextDisbDt { get; set; }
        [Database("pend_disb_dt"), Display("PendDisbDt")]
        public DateTime PendDisbDt { get; set; }
        [Database("download_ind"), Display("DownloadInd")]
        public String DownloadInd { get; set; }
        [Database("identity_ind"), Display("IdentityInd")]
        public String IdentityInd { get; set; }
        [Database("epf_i_status"), Display("EpfIStatus")]
        public String EpfIStatus { get; set; }
        [Database("epf_i_eff_dt"), Display("EpfIEffDt")]
        public DateTime EpfIEffDt { get; set; }
        [Database("epf_i_lst_upd_dt"), Display("EpfILstUpdDt")]
        public DateTime EpfILstUpdDt { get; set; }
        [Database("ent_dt"), Display("EntDt")]
        public DateTime EntDt { get; set; }
        [Database("ent_time"), Display("EntTime")]
        public DateTime EntTime { get; set; }
        [Database("ent_term"), Display("EntTerm")]
        public String EntTerm { get; set; }
        [Database("ent_by"), Display("EntBy")]
        public String EntBy { get; set; }
        [Database("chg_dt"), Display("ChgDt")]
        public DateTime ChgDt { get; set; }
        [Database("chg_time"), Display("ChgTime")]
        public DateTime ChgTime { get; set; }
        [Database("chg_term"), Display("ChgTerm")]
        public String ChgTerm { get; set; }
        [Database("chg_by"), Display("ChgBy")]
        public String ChgBy { get; set; }
        [Database("otp_act_st"), Display("OtpActSt")]
        public String OtpActSt { get; set; }
        [Database("otp_email_add"), Display("OtpEmailAdd")]
        public String OtpEmailAdd { get; set; }
        [Database("otp_mobile_no"), Display("OtpMobileNo")]
        public String OtpMobileNo { get; set; }
        [Database("otp_expiry_date"), Display("OtpExpiryDate")]
        public DateTime? OtpExpiryDate { get; set; }
        [Database("otp_ent_dt"), Display("OtpEntDt")]
        public DateTime? OtpEntDt { get; set; }
        [Database("otp_version"), Display("OtpVersion")]
        public String OtpVersion { get; set; }
        [Database("otp_ent_by"), Display("OtpEntBy")]
        public DateTime? OtpEntBy { get; set; }
        [Database("otp_tac_cd"), Display("OtpTacCd")]
        public DateTime? OtpTacCd { get; set; }


        [Database("joint_tel_no"), Display("JointTelNo")]
        public String JointTelNo { get; set; }

        [Database("joint_addr_1"), Display("JointAddr1")]
        public String JointAddr1 { get; set; }

        [Database("joint_addr_2"), Display("JointAddr2")]
        public String JointAddr2 { get; set; }

        [Database("joint_addr_3"), Display("JointAddr3")]
        public String JointAddr3 { get; set; }

        [Database("joint_addr_4"), Display("JointAddr4")]
        public String JointAddr4 { get; set; }

        [Database("joint_postcode"), Display("JointPostcode")]
        public String JointPostcode { get; set; }

        [Database("joint_region"), Display("JointRegion")]
        public String JointRegion { get; set; }

        [Database("joint_state"), Display("JointState")]
        public String JointState { get; set; }

        [Database("joint_country_res"), Display("JointCountryRes")]
        public String JointCountryRes { get; set; }

        [Database("joint_nationality"), Display("JointNationality")]
        public String JointNationality { get; set; }

        [Database("joint_race"), Display("JointRace")]
        public String JointRace { get; set; }

        [Database("joint_occ_code"), Display("JointOccCode")]
        public String JointOccCode { get; set; }

        [Database("joint_birth_dt"), Display("JointBirthDt")]
        public String JointBirthDt { get; set; }

        [Database("joint_sex"), Display("JointSex")]
        public String JointSex { get; set; }

        [Database("joint_income_code"), Display("JointIncomeCode")]
        public String JointIncomeCode { get; set; }

        [Database("joint_relation_code"), Display("JointRelationCode")]
        public String JointRelationCode { get; set; }

        [Database("joint_signature_ind"), Display("JointSignatureInd")]
        public String JointSignatureInd { get; set; }

        [Database("joint_no_of_dpndnt"), Display("JointNoOfDpndnt")]
        public Int32? JointNoOfDpndnt { get; set; }

        [Database("joint_download_ind"), Display("JointDownloadInd")]
        public String JointDownloadInd { get; set; }

        [Database("nob"), Display("nob")]
        public String Nob { get; set; }

        [Database("name_of_employer"), Display("name_of_employer")]
        public String NameOfEmployer { get; set; }

        [Database("office_address_1"), Display("office_address_1")]
        public String OfficeAddress1 { get; set; }

        [Database("office_address_2"), Display("office_address_2")]
        public String OfficeAddress2 { get; set; }

        [Database("office_address_3"), Display("office_address_3")]
        public String OfficeAddress3 { get; set; }

        [Database("office_postcode"), Display("office_postcode")]
        public String OfficePostCode { get; set; }

        [Database("office_city"), Display("office_city")]
        public String OfficeCity { get; set; }

        [Database("office_state"), Display("office_state")]
        public String OfficeState { get; set; }

        [Database("office_country"), Display("office_country")]
        public String OfficeCountry { get; set; }

        [Database("office_tel_no"), Display("office_tel_no")]
        public String OfficeTelNo { get; set; }
    }
}
