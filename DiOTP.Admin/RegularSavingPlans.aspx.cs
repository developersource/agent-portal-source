﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using MoreLinq;
using DiOTP.Utility.CustomClasses;
using System.IO;
using OfficeOpenXml;
using System.Configuration;
using Admin.FPXLibary;
using Admin.ServiceCalls;
using System.Globalization;
using Newtonsoft.Json;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace Admin
{
    public partial class RegularSavingPlans : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        string url = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                StringBuilder filter = new StringBuilder();
                string mainQ = (@"select uo.order_status, uo.ID, uo.order_type, uo.user_id, u.id_no, u.username, uo.settlement_date, uo.user_account_id, ua.account_no, ua.holder_class, uo.created_date, uo.order_no, uo.amount, uo.units, uo.fpx_bank_code as bank_code, uo.payment_method, uo.updated_date, uo.reject_reason, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits, uo.fpx_bank_code, count(uo.order_no ) as no_of_items, if(uo.order_type = 3, group_concat(ufi.Fund_Name , ',', ufi2.Fund_Name), group_concat(ufi.Fund_Name)) as fund_names, uo.consultantID as ConsultantId from ");
                string mainQCount = (@"select count(*) from ( ");
                filter.Append(@" user_orders uo
                            join users u on u.id = uo.user_id
                            join user_accounts ua on ua.id = uo.user_account_id
                            join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.order_type = 6 ");

                if (Session["admin"] == null)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
                else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    {
                        hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    {
                        hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    {
                        hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["pageLength"]))
                    {
                        pageLength.Value = Request.QueryString["pageLength"].ToString();
                    }

                    
                    
                    else
                    {
                        filter.Append(" and order_status in ('2','3','39') ");
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                    {
                        IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                    {
                        FromDate.Value = Request.QueryString["FromDate"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ToDate"]))
                    {
                        ToDate.Value = Request.QueryString["ToDate"].ToString();
                    }
                    if (IsNewSearch.Value == "1")
                    {
                        hdnCurrentPageNo.Value = "";
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                    {
                        Search.Value = Request.QueryString["Search"].ToString();
                        filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                        filter.Append(" or u.id_no like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.order_no like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.fpx_bank_code like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.reject_reason like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or ua.account_no like '%" + Search.Value.Trim() + "%') ");

                    }

                    if (IsNewSearch.Value == "2")
                    {
                        hdnCurrentPageNo.Value = "";
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                    {
                        FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                        if (FilterValue.Value != "All")
                            filter.Append(" and uo.order_status in ('" + FilterValue.Value + "') ");
                        else
                            filter.Append(" and uo.order_status IN('2', '3', '39') ");
                    }
                    else
                    {
                        filter.Append(" and uo.order_status IN ('2') ");
                    }
                    if (!string.IsNullOrEmpty(FromDate.Value) && !string.IsNullOrEmpty(ToDate.Value))
                    {
                        DateTime from = DateTime.ParseExact(FromDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime to = DateTime.ParseExact(ToDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        if (FromDate.Value.Trim() == ToDate.Value.Trim())
                        {
                            to = to.AddDays(1);
                        }
                        else if (from > to)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                    "alert('From date cannot be greater than To date.');", true);
                        }
                        filter.Append(" and uo.created_date between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                    }
                    filter.Append(" group by uo.order_no ");
                    filter.Append(" order by FIELD(uo.order_status, '2', '3', '39'), uo.created_date desc ");
                    int skip = 0, take = 20;
                    if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                        take = Convert.ToInt32(pageLength.Value);
                    if (hdnCurrentPageNo.Value == "")
                    {
                        skip = 0;
                        hdnNumberPerPage.Value = take.ToString();
                        hdnCurrentPageNo.Value = "1";
                        hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") countIn ").Data.ToString();
                    }
                    else
                    {
                        skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                    }

                    Response responseUOList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                    if (responseUOList.IsSuccess)
                    {
                        var uoDyn = responseUOList.Data;
                        var responseJSON = JsonConvert.SerializeObject(uoDyn);
                        List<UserOrderCustom> userOrders = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserOrderCustom>>(responseJSON);

                        StringBuilder asb = new StringBuilder();
                        int index = 1;
                        userOrders.ForEach(u =>
                        {
                            string checkBox = string.Empty;

                            DateTime dateTime = u.CreatedDate;
                            if (u.OrderStatus == (int)Order_Status.Order_Approved || u.OrderStatus == (int)Order_Status.Order_Rejected || u.OrderStatus == (int)Order_Status.Payment_Rejected)
                            {
                                checkBox = @"<label>" + index + @"</label><br/>";
                            }
                            else
                            {
                                checkBox = @"<div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRow' class='checkRow' value='" + u.Id + @"' /> <label>" + index + @"</label><br/>
                                                    </div>
                                                </div>";
                                
                            }

                            string url = "";
                            Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), u.OrderNo, true, 0, 0, true);
                            if (responseUOFList.IsSuccess)
                            {
                                UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                if (uof != null)
                                    url = uof.Url;
                            }
                            asb.Append(@"<tr>
                                                    <td class='icheck'>
                                                        " + checkBox + @"
                                                        <span class='row-status'>
                                                            " + (u.OrderStatus == 2 ? "<span class='label label-warning'>Enrolled</span>" :
                                                            (u.OrderStatus == 3 ? "<span class='label label-success'>Approved</span>" :
                                                                 u.OrderStatus == 39 ? "<span class='label label-danger'>Rejected</span>" : ""))
                                                              + @"
                                                        </span>
                                                    </td>
                                                    <td>" + u.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                                    <td>" + u.IdNo + @"</td>
                                                    <td>" + u.Username + @"</td>
                                                    <td>" + u.AccountNo + " (" + CustomValues.GetAccounPlan(u.HolderClass) + ")" + @"</td>
                                                    <td>" + u.BankCode + @"</td>
                                                    <td><a href='javascript:;' data-id='" + u.OrderNo + @"' data-original-title='View Detail' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='ViewDetailRSP.aspx' data-title='Regular Saving Detail - " + u.OrderNo + @"' data-action='View'>" + u.OrderNo + @"</a></td>
                                                    <td>" + (u.SettlementDate.HasValue && (u.OrderStatus == 3 || u.OrderStatus == 39) ? u.SettlementDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</td>
                                                    <td>" + u.RejectReason + @"</td>
                                                  </tr>
                                ");
                            
                            index++;
                        });
                        usersTbody.InnerHtml = asb.ToString();
                    }
                }
                
                //<td>" + u.TransId + @"</td>
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                string idString = String.Join(",", ids);
                Response responseUOList = IUserOrderService.GetDataByFilter(" ID in (" + idString + ") group by order_no ", 0, 0, false);
                if (responseUOList.IsSuccess)
                {
                    List<UserOrder> order = (List<UserOrder>)responseUOList.Data;
                    List<UserOrder> multipleOrders = new List<UserOrder>();
                    List<UserOrder> sent = new List<UserOrder>();

                    string eMandateRefNoFormat = "EM" + DateTime.Now.ToString("yyMMdd");
                    Response responseLastRSPOrder = IUserOrderService.GetDataByFilter(" order_type = 6 and order_status = 3 and reject_reason like '" + eMandateRefNoFormat + "%' group by order_no ", 0, 1, true);
                    UserOrder lastUO = ((List<UserOrder>)responseLastRSPOrder.Data).FirstOrDefault();
                    string eMandateRefNo = eMandateRefNoFormat + "0000";
                    if (lastUO != null)
                    {
                        eMandateRefNo = lastUO.RejectReason;
                    }
                    order.ForEach(x =>
                    {
                        int lasteMandateRefNo = Convert.ToInt32(eMandateRefNo.Substring(8, eMandateRefNo.Length - 8));
                        string eMandateRefNoPost = (lasteMandateRefNo + 1).ToString().PadLeft(4, '0');
                        eMandateRefNo = eMandateRefNoFormat + eMandateRefNoPost;
                        Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                        if (response.IsSuccess)
                        {
                            List<UserOrder> ol = (List<UserOrder>)response.Data;
                            if (ol.Count > 0)
                            {
                                ol.ForEach(n =>
                                {
                                    n.RejectReason = eMandateRefNo;
                                });
                                multipleOrders.AddRange(ol);
                            }
                        }
                    });

                    if (action == "Approve")
                    {
                        multipleOrders.ForEach(x =>
                        {
                            x.UpdatedDate = DateTime.Now;
                            x.OrderStatus = 3;
                            x.Status = 1;
                            //x.RejectReason = rejectReason;
                            x.SettlementDate = DateTime.Now;
                        });

                        order.ForEach(y =>
                        {
                            response1.IsSuccess = true;
                            responseMsgs.Add("[" + y.OrderNo + "] is approved");
                            Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                            if (responseOrder.IsSuccess)
                            {
                                List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                sent.AddRange(orders);
                            }
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Subscription for Order number " + y.OrderNo + " succesfully approved",
                                TableName = "user_orders",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "order_status",
                                ValueOld = "2",
                                ValueNew = "3",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }
                    if (action == "Reject")
                    {
                        multipleOrders.ForEach(x =>
                        {
                            x.UpdatedDate = DateTime.Now;
                            x.RejectReason = rejectReason;
                            x.OrderStatus = 39;
                            x.Status = 1;
                            x.SettlementDate = DateTime.Now;
                        });

                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 39)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is rejected");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Subscription for Order number " + y.OrderNo + " has been rejected",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "2",
                                    ValueNew = "39",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }
                    sent.ForEach(x =>
                    {
                        Response response = IUserService.GetSingle(x.UserId);
                        if (response.IsSuccess)
                        {
                            string fundName = "";
                            string fundName2 = "";
                            List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                            List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();
                            List<UserOrder> allOrders = multipleOrders.Where(y => y.OrderNo == x.OrderNo).ToList();
                            User user = (User)response.Data;
                            UserAccount primaryAccount = user.UserIdUserAccounts.Where(w => w.Id == x.UserAccountId).FirstOrDefault();
                            Email email = new Email();
                            email.user = user;
                            allOrders.ForEach(z =>
                            {
                                Response responseF = IUtmcFundInformationService.GetSingle(z.FundId);
                                if (responseF.IsSuccess)
                                {
                                    UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF.Data;
                                    fundName = utmcFundInformation.FundName.Capitalize();
                                    funds.Add(utmcFundInformation);

                                }
                                if (z.ToFundId != 0)
                                {
                                    Response responseF2 = IUtmcFundInformationService.GetSingle(x.ToFundId);
                                    if (responseF2.IsSuccess)
                                    {
                                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF2.Data;
                                        fundName2 = utmcFundInformation.FundName.Capitalize();
                                        funds2.Add(utmcFundInformation);
                                    }
                                }
                            });
                            EmailService.SendOrderEmail(allOrders, email, funds, funds2, primaryAccount.AccountNo);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add(ex.Message);
                Console.WriteLine("User orders action: " + ex.Message);
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        public void PaymentTerminated()
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            // Payment
            string month = DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            string startDate = DateTime.Now.Day.ToString() + month + DateTime.Now.Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2);
            string endDate = DateTime.Now.Day.ToString() + month + DateTime.Now.AddYears(1).Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2);
            Response responseUOsList = IUserOrderService.GetDataByFilter(" ID in (352)", 0, 0, false);
            string orderNo = string.Empty;
            string TransNo = string.Empty;
            string amount = string.Empty;
            User user1 = new DiOTP.Utility.User();
            string bankCode = string.Empty;
            string userIC = string.Empty;
            if (responseUOsList.IsSuccess)
            {
                List<UserOrder> order = (List<UserOrder>)responseUOsList.Data;
                orderNo = order.FirstOrDefault().OrderNo;
                TransNo = order.FirstOrDefault().TransNo;
                bankCode = order.FirstOrDefault().BankCode;
                amount = Math.Round(order.Sum(x => x.Amount), 2).ToString();
                Response response = IUserService.GetSingle(order.FirstOrDefault().UserId);
                if (response.IsSuccess)
                {
                    user1 = (User)response.Data;
                }
                string accountNo = user1.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).AccountNo;
                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(accountNo);
                MaHolderReg maHolderReg = new MaHolderReg();
                if (responseMHR.IsSuccess)
                {
                    maHolderReg = (MaHolderReg)responseMHR.Data;
                    if (!string.IsNullOrEmpty(maHolderReg.IdNo))
                        userIC = maHolderReg.IdNo.Replace("-", "");
                }
            }
            FPXPayment(orderNo, TransNo, amount, user1.EmailId, "" + userIC + ",1", user1.Username, "0", user1.Username, "APEX", "03," + user1.MobileNumber + ",1,MT," + startDate + "," + endDate + "", bankCode, "AD");
        }


        public void FPXPayment(
            string orderNo, string transDate, string txn_amt,
            string buyer_mail_id, string buyer_id, string buyerName, string buyerBankBranch,
            string buyerAccNo, string makerName, string buyerIBAN,
            string buyerBankId, string msgType)
        {
            Controller c = new Controller();
            String checksum = "";
            Random RandomNumber = new Random();
            String fpx_msgType = msgType;
            String fpx_msgToken = "01";
            String fpx_sellerExId = "EX00008813";
            String fpx_sellerExOrderNo = orderNo;
            String fpx_sellerOrderNo = orderNo;
            String fpx_sellerTxnTime = transDate;
            String fpx_sellerId = "SE00010202";
            String fpx_sellerBankCode = "01";
            String fpx_txnCurrency = "MYR";
            String fpx_txnAmount = txn_amt;
            String fpx_buyerEmail = buyer_mail_id;
            String fpx_buyerId = buyer_id;
            String fpx_buyerName = buyerName;
            String fpx_buyerBankId = buyerBankId;
            String fpx_buyerBankBranch = buyerBankBranch;
            String fpx_buyerAccNo = buyerAccNo;
            String fpx_makerName = makerName;
            String fpx_buyerIban = buyerIBAN;
            String fpx_productDesc = "PD";
            String fpx_version = "6.0";
            String fpx_checkSum = "";
            fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
            fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
            fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
            fpx_checkSum = fpx_checkSum.Trim();

            checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key name 
            String finalMsg = fpx_checkSum;
            string FPXPostPaymentUrl = ConfigurationManager.AppSettings["FPXPostPaymentUrl"].ToString();
            var formPostText = @"<html><body><div>
                    <form method='POST' action='" + FPXPostPaymentUrl + @"' name='FPXPaymentForm'>
                      <input type='hidden' name='txn_amt' value='" + fpx_txnAmount + @"' /> 
                      <input type='hidden' value='" + fpx_msgType + @"' name='fpx_msgType'>

                      <input type='hidden' value='" + fpx_msgToken + @"' name='fpx_msgToken'>
                      <input type='hidden' value='" + fpx_sellerExId + @"' name='fpx_sellerExId'>
                      <input type='hidden' value='" + fpx_sellerExOrderNo + @"' name='fpx_sellerExOrderNo'>
                      <input type='hidden' value='" + fpx_sellerTxnTime + @"' name='fpx_sellerTxnTime'>
                      <input type='hidden' value='" + fpx_sellerOrderNo + @"' name='fpx_sellerOrderNo'>
                      <input type='hidden' value='" + fpx_sellerBankCode + @"' name='fpx_sellerBankCode'>
                      <input type='hidden' value='" + fpx_txnCurrency + @"' name='fpx_txnCurrency'>
                      <input type='hidden' value='" + fpx_txnAmount + @"' name='fpx_txnAmount'>
                      <input type='hidden' value='" + fpx_buyerEmail + @"' name='fpx_buyerEmail'>
                      <input type='hidden' value='" + checksum + @"' name='fpx_checkSum'>
                      <input type='hidden' value='" + fpx_buyerName + @"' name='fpx_buyerName'>
                      <input type='hidden' value='" + fpx_buyerBankId + @"' name='fpx_buyerBankId'>
                      <input type='hidden' value='" + fpx_buyerBankBranch + @"' name='fpx_buyerBankBranch'>
                      <input type='hidden' value='" + fpx_buyerAccNo + @"' name='fpx_buyerAccNo'>
                      <input type='hidden' value='" + fpx_buyerId + @"' name='fpx_buyerId'>
                      <input type='hidden' value='" + fpx_makerName + @"' name='fpx_makerName'>
                      <input type='hidden' value='" + fpx_buyerIban + @"' name='fpx_buyerIban'>
                      <input type='hidden' value='" + fpx_productDesc + @"' name='fpx_productDesc'>
                      <input type='hidden' value='" + fpx_version + @"' name='fpx_version'>
                      <input type='hidden' value='" + fpx_sellerId + @"' name='fpx_sellerId'>
                      <input type='hidden' value='" + checksum + @"' name='checkSum_String'>
                      <input type='hidden' value='" + Request.PhysicalApplicationPath + @"'>
                    </form></div><script type='text/javascript'>document.FPXPaymentForm.submit();</script></body></html>
                    ";
            Response.Write(formPostText);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "Regular_Saving_Plans_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Regular Saving Plans");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Status", "Date", "ID No", "Username", "MA Account", "Bank Name", "Order No.", "Fund Name", "Subscription Amount", "Processed Date", "E-mandate Number/Remarks" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Regular Saving Plans"];

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    string docDetails = "Regular Saving Plans";

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    filterQuery.Append(@"select *, u.id_no, sum(amount) as SumAmount, sum(units) as SumUnits, uo.settlement_date, group_concat(ufi.Fund_Name SEPARATOR ',\r\n') as allfunds, group_concat(uo.amount SEPARATOR ',\r\n') as allamounts 
                                        from user_orders uo join utmc_fund_information ufi on uo.fund_id = ufi.id
                                        join users u on uo.user_id = u.id
                                        join user_accounts ua on uo.user_account_id = ua.id
                                        where uo.order_type = 6 
                                        ");
                    string orderType = "";

                    string fromDate = filter.Split(',')[1];
                    string toDate = filter.Split(',')[2];
                    filter = filter.Split(',')[0];

                    if (!string.IsNullOrEmpty(filter) && filter != "")
                    {
                        if (filter != "All")
                        {
                            docDetails += " | Status: " + (filter == "2" ? "Order Pending" : (filter == "3" ? "Order Approved" : (filter == "39" ? "Order Rejected" : "")));
                            filterQuery.Append(" and uo.order_type IN ('1', '6') and uo.order_status in ('" + filter + "') ");
                        }
                        else
                        {
                            docDetails += " | Status: All (Order Pending, Order Approved, Order Rejected)";
                            filterQuery.Append(" and uo.order_type IN ('1', '6') and uo.order_status IN('2', '3', '39') ");
                        }
                    }
                    else
                    {
                        docDetails += " | Status: Order Pending ";
                        filterQuery.Append(" and uo.order_type IN ('1', '6') and uo.order_status IN ('2') ");
                    }

                    if (fromDate != "" && toDate != "")
                    {
                        DateTime from = DateTime.ParseExact(fromDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime to = DateTime.ParseExact(toDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        docDetails += " | Date Range: " + from + " - " + to;
                        filterQuery.Append(" and uo.created_date between '" + from.ToString("yyyy-MM-dd") + " 00:00:00' and '" + to.ToString("yyyy-MM-dd") + " 23:59:59' ");
                    }

                    int skip = 0, take = 20;
                    filterQuery.Append(@" group by uo.order_no ");
                    filterQuery.Append(@" order by FIELD(uo.order_status, '2', '3', '39') , uo.created_date desc ");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseTransactions = GenericService.GetDataByQuery(filterQuery.ToString(), 0, 0, false, null, false, null, true);

                    if (responseTransactions.IsSuccess)
                    {
                        var regularSavingPlansDyn = responseTransactions.Data;
                        var responseJSON = JsonConvert.SerializeObject(regularSavingPlansDyn);
                        List<DiOTP.Utility.RegularSavingPlans> transactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.RegularSavingPlans>>(responseJSON);

                        
                        string bankCode = "";
                        int index = 1;
                        transactions.ForEach(x =>
                        {
                            if (x.fpx_bank_code == "" || x.fpx_bank_code == null)
                            {
                                bankCode = "";
                            }
                            else
                            {
                                bankCode = x.fpx_bank_code;
                            }

                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = (x.order_status == 3 ? "Approved" : x.order_status == 2 ? "Pending" : x.order_status == 39 ? "Rejected" : "");
                            worksheet.Cells[row, 3].Value = x.created_date.ToString("dd/MM/yyyy");                            
                            worksheet.Cells[row, 4].Value = x.id_no;
                            worksheet.Cells[row, 5].Value = x.username.ToString();
                            worksheet.Cells[row, 6].Value = x.account_no;
                            worksheet.Cells[row, 7].Value = x.fpx_bank_code;
                            worksheet.Cells[row, 8].Value = x.order_no;
                            worksheet.Cells[row, 9].Value = x.allfunds;
                            worksheet.Cells[row, 10].Value = x.allamounts;
                            worksheet.Cells[row, 11].Value = (x.settlement_date.HasValue && (x.order_status == 3 || x.order_status == 39) ? x.settlement_date.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-");
                            worksheet.Cells[row, 12].Value = x.reject_reason;
                            row++;
                            index++;
                        });
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;

                    // Save this data as a file
                    excel.SaveAs(excelFile);

                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Regular Savings Plan List successfully downloaded",
                        TableName = "user_orders",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "RegularSavingPlans" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

    }
}