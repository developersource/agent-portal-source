﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_exams")]
    public class AgentRegExam
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_reg_id"), Display("agent_reg_id")]
        public Int32 agent_reg_id { get; set; }
        [Database("is_enroll"), Display("is_enroll")]
        public Int32 is_enroll { get; set; }
        [Database("is_prefer"), Display("is_prefer")]
        public Int32 is_prefer { get; set; }
        [Database("language"), Display("language")]
        public String language { get; set; }
        [Database("session_id"), Display("session_id")]
        public String session_id { get; set; }
        [Database("location"), Display("location")]
        public String location { get; set; }
        [Database("exam_date"), Display("exam_date")]
        public DateTime? exam_date { get; set; }
        [Database("is_first_time"), Display("is_first_time")]
        public Int32? is_first_time { get; set; }
        [Database("resit_date"), Display("resit_date")]
        public DateTime? resit_date { get; set; }
        [Database("result"), Display("result")]
        public String result { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32? created_by { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
