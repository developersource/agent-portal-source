﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class Admins
    {
        public int Id { get; set; }
        public String username { get; set; }
        public String user_id { get; set; }
        public int user_type_id { get; set; }
        public String user_type { get; set; }
        public String mobile_number { get; set; }
        public String email_id { get; set; }
        public DateTime last_login_date { get; set; }
        public DateTime created_date { get; set; }
        public int status { get; set; }
    }
}
