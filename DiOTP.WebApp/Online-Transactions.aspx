﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Online-Transactions.aspx.cs" Inherits="DiOTP.WebApp.Online_Transactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section section-online-transactions">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Online Transactions</li>
                        </ol>
                    </div>
                    <div class="text-left">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Online Transactions</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <small class="text-danger pull-right" id="errorMessage" runat="server" clientidmode="static"></small>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default" id="consolidateSummaryforAccount">
                                <div class="table-head">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">MA Account Summary
                                        <small class="pull-right text-right">
                                            <span class="display-block ma-account-summary-head-label text-capitalize">Acc No. 
                                                <span id="selectedAccountNo" runat="server"></span>
                                            </span>
                                        </small>
                                    </h5>
                                </div>
                                <div class="panel-body tableInvestments">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="maAccount-box">
                                                <h5 class="text-uppercase underline mb-5 text-muted">Current Unit Holding</h5>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h5 class="text-uppercase mb-5 text-primary">RM</h5>
                                                        <span id="maAccountRMLabel"><strong>0.00</strong></span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5 class="text-uppercase mb-5 text-primary">UNITS</h5>
                                                        <span id="maAccountUNITSLabel"><strong>0.0000</strong></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5>Proceed with any of the Transaction</h5>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="transaction-box-wrapper">
                                <div class="transaction-box bg-diotp-blue">
                                    <div class="transaction-name text-center">BUY</div>
                                    <div class="btn-group-justified">
                                        <asp:LinkButton ID="lnkBuy" runat="server" ClientIDMode="Static" CssClass="btn btn-sm btn-default" Text="PROCEED" OnClick="lnkBuy_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="transaction-box-wrapper">
                                <div class="transaction-box bg-diotp-blue">
                                    <div class="transaction-name text-center">SELL</div>
                                    <div class="btn-group-justified">
                                        <asp:LinkButton ID="lnkSell" runat="server" ClientIDMode="Static" CssClass="btn btn-sm btn-default" Text="PROCEED" OnClick="lnkSell_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="transaction-box-wrapper">
                                <div class="transaction-box bg-diotp-blue">
                                    <div class="transaction-name text-center">SWITCH</div>
                                    <div class="btn-group-justified">
                                        <asp:LinkButton ID="lnkSwitch" runat="server" ClientIDMode="Static" CssClass="btn btn-sm btn-default" Text="PROCEED" OnClick="lnkSwitch_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="transaction-box-wrapper">
                                <div class="transaction-box bg-diotp-blue">
                                    <div class="transaction-name text-center">TRANSFER</div>
                                    <div class="btn-group-justified">
                                        <asp:LinkButton ID="lnkTransfer" runat="server" ClientIDMode="Static" CssClass="btn btn-sm btn-default" Text="PROCEED" OnClick="lnkTransfer_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="transaction-box-wrapper">
                                <div class="transaction-box bg-diotp-blue">
                                    <div class="transaction-name text-center">COOLING OFF</div>
                                    <div class="btn-group-justified">
                                        <asp:LinkButton ID="lnkCoolingOff" runat="server" ClientIDMode="Static" CssClass="btn btn-sm btn-default" Text="PROCEED" OnClick="lnkCoolingOff_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnSelectedAccountNo" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsSATUptoDate" runat="server" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Content/js/jquery-ui.js"></script>
    <script>
        $(document).ready(function () {
            $(".selectable").selectable({
                autoRefresh: false,
                selecting: function (event, ui) {
                    //$("body").css("overflow", "hidden");
                },
                stop: function (event, ui) {
                    var values = $.map($('.selectable li.ui-selected'), function (li) {
                        return parseInt($(li).attr('value'));
                    });
                    if (values.length > 1) {
                        $('.selectable li.ui-selected').removeClass("ui-selected");
                        $('.selectable li[value=' + values[0] + ']').addClass("ui-selected");
                    }
                    $('#hdnSelectedAccountNo').val(values[0]);
                    //$("body").css("overflow", "auto");
                }
            });
            //$(".selectable li").click(function () {
            //    $(".selectable li").removeClass("ui-selected");
            //    $(this).addClass("ui-selected");
            //});
        });
    </script>
</asp:Content>
