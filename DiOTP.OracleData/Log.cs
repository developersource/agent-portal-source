﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class Log
    {
        public static void Write(string ErrMsg, string filename)
        {

            string LogfileName = filename;
            LogfileName = LogfileName.Replace("/", "-");
            string LogPathName = ConfigurationManager.AppSettings["Logfile_path"];
            string LogFileName = LogfileName + ".txt";
            StreamWriter sw = new StreamWriter(LogPathName + LogFileName, true);
            sw.WriteLine(DateTime.Now.ToString() + ":");
            sw.WriteLine(ErrMsg);
            sw.Close();
        }
    }
}
