﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using DiOTP.Utility.Helper;
using DiOTP.Utility.CustomClasses;
using DiOTP.WebApp.ServiceCalls;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.WebApp
{
    public partial class Invoice : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        string selectedAccountHolderNo = "";
                        
                        UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == selectedAccountHolderNo).FirstOrDefault();

                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;

                            String orderNo = "SW1800000003";

                            if (Request.QueryString["OrderNo"] != null)
                                orderNo = Request.QueryString["OrderNo"];
                            else if (Session["orderNo"] != null)
                                orderNo = Session["orderNo"].ToString();

                            Response responseO = IUserOrderService.GetDataByFilter(" order_no='" + orderNo + "' and user_account_id=" + primaryAcc.Id + " and user_id=" + user.Id + " ", 0, 0, false);
                            if (responseO.IsSuccess)
                            {
                                List<UserOrder> userOrders = (List<UserOrder>)responseO.Data;
                                if (userOrders.Count > 0)
                                {
                                    Response responseUOList = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), orderNo, true, 0, 0, false);
                                    if (responseUOList.IsSuccess)
                                    {
                                        userOrders = (List<UserOrder>)responseUOList.Data;
                                        if (userOrders.Count > 0)
                                        {
                                            if (userOrders.FirstOrDefault().ConsultantId != "" || userOrders.FirstOrDefault().ConsultantId != "0")
                                            {
                                                agentIdDiv.Visible = true;
                                                agentID.InnerText = userOrders.FirstOrDefault().ConsultantId;
                                            }
                                            else
                                                agentIdDiv.Visible = false;

                                            UserOrder uos = userOrders.FirstOrDefault();
                                            string actionsHTML = "";
                                            string status = "";
                                            if (uos.OrderStatus == 1 && (uos.OrderType == (int)OrderType.Buy || uos.OrderType == (int)OrderType.RSP))
                                            {
                                                status = "Order placed";
                                                if (uos.OrderType == (int)OrderType.RSP)
                                                    status = "Pending for verification";
                                            }
                                            else if (uos.OrderStatus == 2 && (uos.OrderType == (int)OrderType.RSP))
                                            {
                                                status = "FPX verified";
                                            }
                                            else if (uos.OrderStatus == 1 && (uos.OrderType == (int)OrderType.Sell || uos.OrderType == (int)OrderType.SwitchIn || uos.OrderType == (int)OrderType.SwitchOut))
                                                status = "Pending for order approval";
                                            else if (uos.OrderStatus == 18)
                                                status = "Order cancelled";
                                            else if (uos.OrderStatus == 22 && uos.PaymentMethod == "1")
                                                status = "Pending for payment";
                                            else if (uos.OrderStatus == 22 && uos.PaymentMethod == "3")
                                                status = "Pending for payment approval";
                                            else if (uos.OrderStatus == 29 && uos.PaymentMethod == "1")
                                                status = "Payment failed";
                                            else if (uos.OrderStatus == 29 && uos.PaymentMethod == "3")
                                                status = "Payment rejected";
                                            else if (uos.OrderStatus == 3)
                                                status = "Order approved";
                                            else if (uos.OrderStatus == 39)
                                                status = "Order rejected";

                                            orderstatuslabel.InnerHtml = status;
                                            orderNoLabel.InnerHtml = orderNo;
                                            orderByUserLabel.InnerHtml = user.Username + "(" + primaryAcc.AccountNo + ")";
                                            accountplan.InnerHtml = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                                            orderPaymentMethodLabel.InnerHtml = (userOrders.Select(x => x.PaymentMethod).First() == "1" ? "FPX Payment" : (userOrders.Select(x => x.PaymentMethod).First() == "3" ? "Proof of Payment" : "-"));
                                            orderDateLabel.InnerHtml = userOrders.Select(x => x.CreatedDate).First().ToString("MMM dd, yyyy");

                                            StringBuilder sb = new StringBuilder();
                                            Decimal orderTotal = 0;
                                            int orderType = 0;
                                            userOrders = userOrders.GroupBy(x => x.RefNo).Select(y => y.First()).ToList();
                                            foreach (UserOrder uo in userOrders)
                                            {
                                                Response response = IUtmcFundInformationService.GetSingle(uo.FundId);
                                                if (response.IsSuccess)
                                                {
                                                    UtmcFundInformation uFT = (UtmcFundInformation)response.Data;
                                                    UtmcFundInformation uFT2 = new UtmcFundInformation();
                                                    if (uo.ToFundId != 0)
                                                    {
                                                        Response response2 = IUtmcFundInformationService.GetSingle(uo.ToFundId);
                                                        if (response2.IsSuccess)
                                                        {
                                                            uFT2 = (UtmcFundInformation)response2.Data;
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response2.Message + "\", '');", true);
                                                        }
                                                    }
                                                    Decimal total = uo.Amount;
                                                    if (uo.OrderType != 1 && uo.OrderType != 6)
                                                    {
                                                        total = uo.Units;
                                                        thFees.Visible = false;
                                                        tFootTD.ColSpan = 1;
                                                    }
                                                    orderTotal += total;
                                                    UserAccount toAcc = new UserAccount();
                                                    if (uo.ToAccountId != 0)
                                                    {
                                                        Response response2 = IUserAccountService.GetSingle(uo.ToAccountId);
                                                        if (response2.IsSuccess)
                                                        {
                                                            toAcc = (UserAccount)response2.Data;
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response2.Message + "\", '');", true);
                                                        }
                                                    }
                                                    sb.Append(@"<tr>
                                            <td>" + (uo.ToFundId != 0 ? "Switch from " : "") + "" + uFT.FundName.Capitalize() + "" + (uo.ToFundId != 0 ? "<br /> to " + uFT2.FundName.Capitalize() + @"" : "") + (uo.ToAccountId != 0 ? "Transfer to " + toAcc.AccountNo + "" + toAcc.MaHolderRegIdMaHolderReg.Name1 : "") + @"</td>
                                            <td class='text-right " + (uo.OrderType == 1 || uo.OrderType == 6 ? "currencyFormatNoSymbol" : "unitFormat") + "'>" + (uo.OrderType == 1 || uo.OrderType == 6 ? uo.Amount.ToString("N", new CultureInfo("en-US")) : uo.Units.ToString("N", new CultureInfo("en-US"))) + @"</td>
                                            " + (uo.OrderType == 1 || uo.OrderType == 6 ? "<td class='text-right'>" + uFT.UtmcFundInformationIdUtmcFundCharges.First().InitialSalesChargesPercent + @"</td>" : "") + @"
                                            <td class='text-right " + (uo.OrderType == 1 || uo.OrderType == 6 ? "currencyFormatNoSymbol" : "unitFormat") + "'>" + total.ToString("N", new CultureInfo("en-US")) + @"</td>
                                        </tr>");
                                                    orderType = uo.OrderType;
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                                }
                                            }
                                            if (orderType == 1 || orderType == 6)
                                            {
                                                transType.InnerHtml = "Investment Amount<br /> MYR";
                                                txtUnit.Text = "MYR";
                                            }
                                            else if (orderType == 2)
                                            {
                                                transType.InnerHtml = "Redemption Units";
                                                txtUnit.Text = "Units";
                                            }
                                            else if (orderType == 3 || orderType == 4)
                                            {
                                                transType.InnerHtml = "Switching Units";
                                                txtUnit.Text = "Units";
                                            }
                                            ordersTbody.InnerHtml = sb.ToString();
                                            orderTotalLabel.Attributes.Remove("class");
                                            orderTotalLabel.Attributes.Add("class", (orderType == 1 || orderType == 6 ? "no-line text-right currencyFormatNoSymbol" : "no-line text-right unitFormatNoSymbol"));
                                            orderTotalLabel.InnerHtml = orderTotal.ToString("N", new CultureInfo("en-US"));
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOList.Message + "\", '');", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOList.Message + "\", '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid Order!', 'OrderList.aspx');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseO.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}
