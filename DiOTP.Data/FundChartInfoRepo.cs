using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class FundChartInfoRepo : IFundChartInfoRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(FundChartInfo obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FundChartInfo>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FundChartInfo> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FundChartInfo>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FundChartInfo obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FundChartInfo>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FundChartInfo> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FundChartInfo>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FundChartInfo DeleteData(Int32 Id)
        {
            Response responseFCI = GetSingle(Id);
            if (responseFCI.IsSuccess)
            {
                FundChartInfo obj = (FundChartInfo)responseFCI.Data;
                try
                {
                    string query = obj.ObjectToQuery<FundChartInfo>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseFCI = GetSingle(Id);
                    if (responseFCI.IsSuccess)
                    {
                        FundChartInfo obj = (FundChartInfo)responseFCI.Data;
                        query += obj.ObjectToQuery<FundChartInfo>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FundChartInfo obj = new FundChartInfo();
            try
            {
                string query = "select * from fund_chart_info where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new FundChartInfo
                       {
                           Id = x.Field<Int32>("id"),
                           DataValue = x.Field<Int32>("data_value"),
                           FundId = x.Field<Int32>("fund_id"),
                           FundName = x.Field<String>("fund_name"),
                           TotalReturns = x.Field<Decimal>("total_returns"),
                           Average = x.Field<Decimal>("average"),
                           ThreeYearAnnualisedPercent = x.Field<String>("three_year_annualised_percent"),
                           MinChange = x.Field<Decimal>("min_change"),
                           MaxChange = x.Field<Decimal>("max_change"),
                           MinNav = x.Field<Decimal>("min_nav"),
                           MaxNav = x.Field<Decimal>("max_nav"),
                           CurrentNav = x.Field<Decimal>("current_nav"),
                           CurrentNavDate = x.Field<String>("current_nav_date"),
                           Labels = x.Field<String>("labels"),
                           NavChanges = x.Field<String>("nav_changes"),
                           NavPrices = x.Field<String>("nav_prices"),
                           NavDates = x.Field<String>("nav_dates"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundChartInfo> objs = new List<FundChartInfo>();
            try
            {
                string query = "select * from fund_chart_info";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundChartInfo
                        {
                            Id = x.Field<Int32>("id"),
                            DataValue = x.Field<Int32>("data_value"),
                            FundId = x.Field<Int32>("fund_id"),
                            FundName = x.Field<String>("fund_name"),
                            TotalReturns = x.Field<Decimal>("total_returns"),
                            Average = x.Field<Decimal>("average"),
                            ThreeYearAnnualisedPercent = x.Field<String>("three_year_annualised_percent"),
                            MinChange = x.Field<Decimal>("min_change"),
                            MaxChange = x.Field<Decimal>("max_change"),
                            MinNav = x.Field<Decimal>("min_nav"),
                            MaxNav = x.Field<Decimal>("max_nav"),
                            CurrentNav = x.Field<Decimal>("current_nav"),
                            CurrentNavDate = x.Field<String>("current_nav_date"),
                            Labels = x.Field<String>("labels"),
                            NavChanges = x.Field<String>("nav_changes"),
                            NavPrices = x.Field<String>("nav_prices"),
                            NavDates = x.Field<String>("nav_dates"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_chart_info";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<FundChartInfo>(propertyName);
            List<FundChartInfo> objs = new List<FundChartInfo>();
            try
            {
                string query = "select * from fund_chart_info where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from fund_chart_info where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundChartInfo
                        {
                            Id = x.Field<Int32>("id"),
                            DataValue = x.Field<Int32>("data_value"),
                            FundId = x.Field<Int32>("fund_id"),
                            FundName = x.Field<String>("fund_name"),
                            TotalReturns = x.Field<Decimal>("total_returns"),
                            Average = x.Field<Decimal>("average"),
                            ThreeYearAnnualisedPercent = x.Field<String>("three_year_annualised_percent"),
                            MinChange = x.Field<Decimal>("min_change"),
                            MaxChange = x.Field<Decimal>("max_change"),
                            MinNav = x.Field<Decimal>("min_nav"),
                            MaxNav = x.Field<Decimal>("max_nav"),
                            CurrentNav = x.Field<Decimal>("current_nav"),
                            CurrentNavDate = x.Field<String>("current_nav_date"),
                            Labels = x.Field<String>("labels"),
                            NavChanges = x.Field<String>("nav_changes"),
                            NavPrices = x.Field<String>("nav_prices"),
                            NavDates = x.Field<String>("nav_dates"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<FundChartInfo>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_chart_info where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from fund_chart_info where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundChartInfo> objs = new List<FundChartInfo>();
            try
            {
                string query = "select * from fund_chart_info where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundChartInfo
                        {
                            Id = x.Field<Int32>("id"),
                            DataValue = x.Field<Int32>("data_value"),
                            FundId = x.Field<Int32>("fund_id"),
                            FundName = x.Field<String>("fund_name"),
                            TotalReturns = x.Field<Decimal>("total_returns"),
                            Average = x.Field<Decimal>("average"),
                            ThreeYearAnnualisedPercent = x.Field<String>("three_year_annualised_percent"),
                            MinChange = x.Field<Decimal>("min_change"),
                            MaxChange = x.Field<Decimal>("max_change"),
                            MinNav = x.Field<Decimal>("min_nav"),
                            MaxNav = x.Field<Decimal>("max_nav"),
                            CurrentNav = x.Field<Decimal>("current_nav"),
                            CurrentNavDate = x.Field<String>("current_nav_date"),
                            Labels = x.Field<String>("labels"),
                            NavChanges = x.Field<String>("nav_changes"),
                            NavPrices = x.Field<String>("nav_prices"),
                            NavDates = x.Field<String>("nav_dates"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_chart_info where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
