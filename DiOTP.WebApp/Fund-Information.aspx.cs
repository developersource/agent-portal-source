﻿using DiOTP.PolicyAndRules;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.PolicyAndRules.FundPolicy;

namespace DiOTP.WebApp
{
    public partial class Funds_Information : System.Web.UI.Page
    {

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcDailyNavFundService> lazyUtmcDailyNavFundObj = new Lazy<IUtmcDailyNavFundService>(() => new UtmcDailyNavFundService());

        public static IUtmcDailyNavFundService IUtmcDailyNavFundService { get { return lazyUtmcDailyNavFundObj.Value; } }

        private static readonly Lazy<IUtmcFundCorporateActionService> lazyUtmcFundCorporateActionObj = new Lazy<IUtmcFundCorporateActionService>(() => new UtmcFundCorporateActionService());

        public static IUtmcFundCorporateActionService IUtmcFundCorporateActionService { get { return lazyUtmcFundCorporateActionObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundReturnService> lazyIFundReturnServiceObj = new Lazy<IFundReturnService>(() => new FundReturnService());

        public static IFundReturnService IFundReturnService { get { return lazyIFundReturnServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> IutmcFundFileService = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IutmcFundFile { get { return IutmcFundFileService.Value; } }


        public static string Prospectus = "";
        public static string Annual_Report = "";
        public static string Product_Highlight_Sheet = "";
        public static string Factsheet = "";
        public static string Others = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    homeLink.HRef = "/Portfolio.aspx";
                }
                String fundCode = Request.QueryString["fundCode"];
                if (!string.IsNullOrEmpty(fundCode))
                {

                    Response responseFund = (Response)FundPolicy.Get(new FundPolicy.FundClass { Step = 2, FundCode = fundCode, httpContext = Context });

                    if (responseFund.IsSuccess)
                    {
                        ResponseFundStep2 responseFundStep2 = (ResponseFundStep2)responseFund.Data;
                        FundPolicyEnum fundPolicyEnum = (FundPolicyEnum)Enum.Parse(typeof(FundPolicyEnum), responseFundStep2.Code);
                        string message = fundPolicyEnum.ToDescriptionString();
                        if (message == "SUCCESS")
                        {
                            UtmcFundInformation utmcFundInformation = responseFundStep2.utmcFundInformation;
                            FundInfo fundInfo = responseFundStep2.fundInfo;
                            List<UtmcFundFile> Downloadfiles = responseFundStep2.Downloadfiles;

                            hdnFundId.Value = utmcFundInformation.Id.ToString();

                            StringBuilder sbDownloadLinks = new StringBuilder();

                            int index = 1;
                            foreach (UtmcFundFile f in Downloadfiles)
                            {
                                if (f.Url != null)
                                {
                                    f.Url = "\"" + f.Url + "\"";
                                    sbDownloadLinks.Append(@"<div class='panel'>
                                                <div class='panel-heading'>
                                                     " + index + @". <a href=" + f.Url + @" target='_blank'>Click here for " + f.Name + @"
                                                    </a>
                                                </div>
                                            </div>");
                                    index++;
                                }
                            }

                            downloadFileLinks.InnerHtml = sbDownloadLinks.ToString();

                            dividendHistoryFundName.InnerHtml = utmcFundInformation.FundName.Capitalize();
                            fundNameHeading.InnerHtml = utmcFundInformation.FundName.Capitalize() + " - <small>" + utmcFundInformation.FundCode + "</small>";
                            UtmcFundDetail uFD = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault();
                            fundCategory.InnerHtml = utmcFundInformation.UtmcFundCategoriesDefIdUtmcFundCategoriesDef.Name;
                            fundLaunchDate.InnerHtml = uFD.RelaunchDate == default(DateTime) ? uFD.LaunchDate.ToString("dd/MM/yyyy") : uFD.RelaunchDate.Value.ToString("dd/MM/yyyy") + "(Relaunch Date)";
                            fundLaunchPrice.InnerHtml = "MYR " + uFD.LaunchPrice.ToString("N4", new CultureInfo("en-US"));
                            fundPricingBasis.InnerHtml = uFD.PricingBasis;
                            fundLatestNavPrice.InnerHtml = "MYR " + fundInfo.CurrentUnitPrice + " (" + fundInfo.CurrentNavDate.ToString("MMMM dd, yyyy") + ")";
                            fundHistoricalIncomeDistribution.InnerHtml = uFD.HistoricalIncomeDistribution == 0 ? "No" : "Yes";
                            fundApprovedByEPF.InnerHtml = uFD.IsEpfApproved == 0 ? "No" : "Yes";
                            fundShariahComplaint.InnerHtml = uFD.ShariahCompliant == 0 ? "No" : "Yes";
                            fundRiskRating.InnerHtml = uFD.RiskRating;
                            fundSize.InnerHtml = "MYR " + uFD.FundSizeRm;
                            fundMinimumInitialInvestmentTitle.InnerHtml = "Minimum Initial Investment (CASH" + ((uFD.MinInitialInvestmentEpf == 0) ? ")" : "/EPF)");
                            fundMinimumInitialInvestment.InnerHtml = "MYR " + uFD.MinInitialInvestmentCash.ToString("N", new CultureInfo("en-US")) + ((uFD.MinInitialInvestmentEpf == 0) ? "" : "/MYR " + uFD.MinInitialInvestmentEpf.ToString("N", new CultureInfo("en-US")));
                            fundMinimumSubsequentInvestmentTitle.InnerHtml = "Minimum Subsequent Investment (CASH" + ((uFD.MinSubsequentInvestmentEpf == 0) ? ")" : "/EPF)");
                            fundMinimumSubsequentInvestment.InnerHtml = "MYR " + uFD.MinSubsequentInvestmentCash.ToString("N", new CultureInfo("en-US")) + ((uFD.MinSubsequentInvestmentEpf == 0) ? "" : "/MYR " + uFD.MinSubsequentInvestmentEpf.ToString("N", new CultureInfo("en-US")));
                            fundMinimumRSPInvestment.InnerHtml = uFD.MinRspInvestmentInitialRm == 0 ? "-" : "MYR " + uFD.MinRspInvestmentInitialRm.ToString("N", new CultureInfo("en-US")) + "/ " + ((uFD.MinRspInvestmentAdditionalRm == 0) ? "-" : "MYR " + uFD.MinRspInvestmentAdditionalRm.ToString("N", new CultureInfo("en-US")));
                            fundMinimumRedemptionAmount.InnerHtml = uFD.MinRedAmountUnits == 0 ? "No" : uFD.MinRedAmountUnits.ToString("N", new CultureInfo("en-US")) + " Units";
                            fundMinimumHolding.InnerHtml = uFD.MinHoldingUnits.ToString("N", new CultureInfo("en-US")) + " Units";
                            fundCoolingOffPeriod.InnerHtml = uFD.CollingOffPeriod;
                            fundDistributionPolicy.InnerHtml = uFD.DistributionPolicy;
                            fundInvestmentObjective.InnerHtml = uFD.InvestmentObjective;
                            fundInvestmentStrategyAndPolicy.InnerHtml = uFD.InvestmentStrategyAndPolicy;
                            if (uFD.InvestmentStrategyAndPolicy != "" && uFD.InvestmentStrategyAndPolicy != null)
                                fundInvestmentStrategyAndPolicyTD.Visible = true;

                            //Charges
                            UtmcFundCharge uFC = utmcFundInformation.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault();
                            fundDiscountedInitialSalesCharge.InnerHtml = uFC.InitialSalesChargesPercent == 0 ? "NIL" : (uFC.InitialSalesChargesPercent).ToString() + "%";
                            fundEpfSalesCharge.InnerHtml = uFC.EpfSalesChargesPercent == 0 ? "NIL" : (uFC.EpfSalesChargesPercent).ToString() + "%";
                            fundAnnualManagementCharge.InnerHtml = uFC.AnnualManagementChargePercent == 0 ? "NIL" : uFC.AnnualManagementChargePercent.ToString() + "% p.a of the NAV";
                            fundTrusteeFee.InnerHtml = uFC.TrusteeFeePercent == 0 ? "NIL" : uFC.TrusteeFeePercent + "%  p.a of the NAV";
                            fundSwitchingFee.InnerHtml = uFC.SwitchingFeePercent == 0 ? "NIL" : "3 free switches per account per calendar year, subsequent switches will be charged a " + uFC.SwitchingFeePercent + "% of redemption moneys for administrative purpose.";
                            fundRedemptionFee.InnerHtml = uFC.RedemptionFeePercent == 0 ? "NIL" : uFC.RedemptionFeePercent + "% p.a of the NAV if units are redeemed within the 30-day Redemption Notice Period.";
                            fundTransferFee.InnerHtml = uFC.TransferFeeRm == 0 ? "Not Applicable" : "A fee of RM" + uFC.TransferFeeRm + " will be charged for each transfer.";
                            fundOtherSignificantFee.InnerHtml = uFC.OtherSignificantFeeRm == 0 ? "-" : "" + uFC.OtherSignificantFeeRm;
                            fundGSTFee.InnerHtml = "* Subject to GST of " + uFC.GstPercent + "%";

                            //Fund Performance
                            DateTime currentDate = fundInfo.CurrentNavDate;
                            DateTime ToDate = DateTime.Now;

                            //Nav History Popup
                            ToDate = currentDate.AddMonths(-3);

                            StringBuilder sb = new StringBuilder();
                            int idx = 1;
                            List<UtmcDailyNavFund> utmcDailyNavFunds = responseFundStep2.utmcDailyNavFunds;
                            foreach (UtmcDailyNavFund uDNF in utmcDailyNavFunds.Where(x => x.DailyNavDate > ToDate).ToList())
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>" + idx + "</td>");
                                sb.Append("<td>" + uDNF.DailyUnitPrice + "</td>");
                                sb.Append("<td>" + uDNF.DailyNavDate.ToString("dd/MM/yyyy") + "</td>");
                                sb.Append("</tr>");
                                idx++;
                            }
                            navPriceHistoryTableTbody.InnerHtml = sb.ToString();
                            sb = new StringBuilder();
                            idx = 1;
                            List<UtmcFundCorporateActions> Distributions = responseFundStep2.Distributions;
                            foreach (UtmcFundCorporateActions uFCA in Distributions)
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>" + idx + "</td>");
                                sb.Append("<td>" + uFCA.Corporate_Action_Date.ToString("dd/MM/yyyy") + "</td>");
                                sb.Append("<td>" + uFCA.Report_Date.ToString("dd/MM/yyyy") + "</td>");
                                sb.Append("<td>" + "RM " + uFCA.Distributions + " per unit" + "</td>");
                                sb.Append("<td>" + "RM " + Math.Round(uFCA.Net_Distribution, 4) + " per unit" + "</td>");
                                sb.Append("</tr>");
                                idx++;
                            }
                            dividendHistoryTableTbody.InnerHtml = sb.ToString();

                            FundReturn fr = responseFundStep2.fr;

                            dividendDateValue.InnerText = currentDate.ToString("dd/MM/yyyy");
                            fundPricedate.InnerText = currentDate.ToString("dd/MM/yyyy");
                            
                            weekCValue.InnerText = Math.Round(fr.OneWeek, 2).ToString();
                            ToDate = currentDate.AddDays(-7);
                            weekCValueAvg.InnerText = Math.Round(fr.OneWeek / (currentDate - ToDate).Days, 2).ToString();

                            monthCValue.InnerText = Math.Round(fr.OneMonth, 2).ToString();
                            ToDate = currentDate.AddMonths(-1);
                            monthCValueAvg.InnerText = Math.Round(fr.OneMonth / (currentDate - ToDate).Days, 2).ToString();

                            month3CValue.InnerText = Math.Round(fr.ThreeMonth, 2).ToString();
                            ToDate = currentDate.AddMonths(-3);
                            month3CValueAvg.InnerText = Math.Round(fr.ThreeMonth / (currentDate - ToDate).Days, 2).ToString();

                            month6CValue.InnerText = Math.Round(fr.SixMonth, 2).ToString();
                            ToDate = currentDate.AddMonths(-6);
                            month6CValueAvg.InnerText = Math.Round(fr.SixMonth / (currentDate - ToDate).Days, 2).ToString();

                            yearCValue.InnerText = Math.Round(fr.OneYear, 2).ToString();
                            ToDate = currentDate.AddYears(-1);
                            yearCValueAvg.InnerText = Math.Round(fr.OneYear / (currentDate - ToDate).Days, 2).ToString();
                            
                            List<UtmcDailyNavFund> utmcDailyNavFundsAll = utmcDailyNavFunds;
                            
                            utmcDailyNavFunds = utmcDailyNavFundsAll.Where(x => x.DailyNavDate >= ToDate && x.DailyNavDate <= currentDate).ToList();
                            NAV1HIGH.InnerText = utmcDailyNavFunds.Max(x => x.DailyUnitPrice).ToString();
                            NAV1LOW.InnerText = utmcDailyNavFunds.Min(x => x.DailyUnitPrice).ToString();


                            year2CValue.InnerText = Math.Round(fr.TwoYear, 2).ToString();
                            ToDate = currentDate.AddYears(-2);
                            year2CValueAvg.InnerText = Math.Round(fr.TwoYear / (currentDate - ToDate).Days, 2).ToString();

                            year3CValue.InnerText = Math.Round(fr.ThreeYear, 2).ToString();
                            ToDate = currentDate.AddYears(-3);
                            year3CValueAvg.InnerText = Math.Round(fr.ThreeYear / (currentDate - ToDate).Days, 2).ToString();
                            
                            utmcDailyNavFunds = utmcDailyNavFundsAll.Where(x => x.DailyNavDate >= ToDate && x.DailyNavDate <= currentDate).ToList();
                            NAV3HIGH.InnerText = utmcDailyNavFunds.Max(x => x.DailyUnitPrice).ToString();
                            NAV3LOW.InnerText = utmcDailyNavFunds.Min(x => x.DailyUnitPrice).ToString();


                            year5CValue.InnerText = Math.Round(fr.FiveYear, 2).ToString();
                            ToDate = currentDate.AddYears(-5);
                            year5CValueAvg.InnerText = Math.Round(fr.FiveYear / (currentDate - ToDate).Days, 2).ToString();

                            year10CValue.InnerText = Math.Round(fr.TenYear, 2).ToString();
                            ToDate = currentDate.AddYears(-5);
                            year10CValueAvg.InnerText = Math.Round(fr.TenYear / (currentDate - ToDate).Days, 2).ToString();

                            ToDate = Convert.ToDateTime("2010-01-01");
                            
                            utmcDailyNavFunds = utmcDailyNavFundsAll.Where(x => x.DailyNavDate >= ToDate && x.DailyNavDate <= currentDate).ToList();
                            NAVALLHIGH.InnerText = utmcDailyNavFundsAll.Max(x => x.DailyUnitPrice).ToString();
                            NAVALLLOW.InnerText = utmcDailyNavFundsAll.Min(x => x.DailyUnitPrice).ToString();

                        }
                        else if (message == "EXCEPTION")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseFundStep2.Message + "','');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + message + "','');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseFund.Message + "','');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}