﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="LoginHistory.aspx.cs" Inherits="DiOTP.WebApp.LoginHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">Login History</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Login History</h3>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <h5>Recent login history for account: <span id="spanUsername" runat="server"></span></h5>
                    <table class="table table-font-size-13 table-cell-pad-5 table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Login Date and Time</th>
                                <th>Login IP Address</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyUserLoginHistory" runat="server">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
