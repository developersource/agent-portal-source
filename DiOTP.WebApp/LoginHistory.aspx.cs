﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class LoginHistory : System.Web.UI.Page
    {
        private static readonly Lazy<IUserLoginHistoryService> lazyUserLoginHistoryServiceObj = new Lazy<IUserLoginHistoryService>(() => new UserLoginHistoryService());

        public static IUserLoginHistoryService IUserLoginHistoryService { get { return lazyUserLoginHistoryServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    Response responseULHList = IUserLoginHistoryService.GetDataByPropertyName(nameof(UserLoginHistory.UserId), user.Id.ToString(), true, 0, 10, true);
                    if (responseULHList.IsSuccess)
                    {
                        List<UserLoginHistory> userLoginHistorys = (List<UserLoginHistory>)responseULHList.Data;
                        spanUsername.InnerHtml = user.Username;
                        if (userLoginHistorys.Count > 0)
                        {
                            int i = 1;
                            StringBuilder sb = new StringBuilder();
                            foreach (UserLoginHistory uLH in userLoginHistorys)
                            {
                                sb.Append(@"<tr>
                                <td>" + i + @"</td>
                                <td>" + uLH.LoginDate + @"</td>
                                <td>" + uLH.LoginIp + @"</td>
                            </tr>");
                                i++;
                            }
                            tbodyUserLoginHistory.InnerHtml = sb.ToString();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseULHList.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}