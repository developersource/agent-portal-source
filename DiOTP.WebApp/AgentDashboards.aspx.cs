﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class AgentDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = (User)(Session["user"]);
            if (user != null)
            {
                //To instantiate hidden agent code for ajax
                agentCode.Value = user.AgentCode;

                StringBuilder asb = new StringBuilder();
                string agentPos = "";
                string uplineCode = "";
                //To populate agent details + agent upline detail
                string queryAgentDetail = @"select arp.name as agent_name, ar.agent_code, ar.updated_date as join_date, ar.introducer_code as upline_code, ao.name as office_name, arf.url as selfie_url, ap.agent_pos as agent_position from agent_reg_personal arp
                                            join agent_regs ar on arp.agent_reg_id = ar.id
                                            join agent_offices ao on ar.office_id =  ao.id
                                            join agent_reg_files arf on arp.agent_reg_id = arf.agent_reg_id
                                            join agent_positions ap on ar.agent_code = ap.agent_code
                                            where ar.user_id = '" + user.Id + "' and arf.file_type = '3'";

                Response responseAgentDetailList = GenericService.GetDataByQuery(queryAgentDetail, 0, 0, false, null, false, null, true);
                if (responseAgentDetailList.IsSuccess)
                {
                    var agentDetailDyn = responseAgentDetailList.Data;
                    var responseAgentJSON = JsonConvert.SerializeObject(agentDetailDyn);
                    List<dynamic> agentDetailsObjList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentJSON);
                    dynamic agentObj = agentDetailsObjList.FirstOrDefault();

                    lblAgentName.Text = agentObj.agent_name.ToString();
                    lblAgentCode.Text = agentObj.agent_code.ToString();
                    lblRank.Text = user.UserIdUserTypes.Where(a => a.UserTypeIdUserTypesDef.Id == 6 || a.UserTypeIdUserTypesDef.Id == 7 || a.UserTypeIdUserTypesDef.Id == 8 || a.UserTypeIdUserTypesDef.Id == 9).FirstOrDefault().UserTypeIdUserTypesDef.Name;
                    lblJoinDate.Text = agentObj.join_date.ToString("dd/MM/yyyy");
                    lblRegion.Text = agentObj.office_name.ToString();
                    agentImage.InnerHtml = "<img src='" + agentObj.selfie_url.ToString() + "' alt='Agent Image' class='img-responsive' style='height:320px'/>";

                    agentPos = agentObj.agent_position.ToString();
                    uplineCode = agentObj.upline_code.ToString();
                }

                int agentPosLength = agentPos.Length + 3;
                //To populate direct + indirect Downline Count
                string queryCountUDLine = @"select count(*) as direct_downline, (select count(*) from agent_positions where agent_pos like '" + agentPos + "%' and char_length(agent_pos) >" + agentPosLength + @") as indirect_downline 
                                            from di_otp_ag.agent_positions 
                                            where agent_pos like '" + agentPos + "%' and CHAR_LENGTH(agent_pos) ='" + agentPosLength + "'";
                Response responseCountUDList = GenericService.GetDataByQuery(queryCountUDLine, 0, 0, false, null, false, null, true);
                if (responseCountUDList.IsSuccess)
                {
                    var countUDDyn = responseCountUDList.Data;
                    var responseCountUD = JsonConvert.SerializeObject(countUDDyn);
                    List<dynamic> countUDList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseCountUD);

                    lblDirectDownline.Text = countUDList.FirstOrDefault().direct_downline.ToString();
                    lblIndirectDownline.Text = countUDList.FirstOrDefault().indirect_downline.ToString();
                }

                //To populate agent upline details
                string queryAgentUpline = @"select utd.name as agent_rank, arp.* from agent_reg_personal arp 
                                            join agent_regs ar on arp.agent_reg_id = ar.id
                                            join user_types ut on ut.user_id = ar.user_id
                                            join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')
                                            where ar.agent_code = '" + uplineCode+"'";
                Response responseAgentUpline = GenericService.GetDataByQuery(queryAgentUpline, 0, 0, false, null, false, null, true);
                if (responseAgentUpline.IsSuccess)
                {
                    var agentUplineDyn = responseAgentUpline.Data;
                    var responseAgent = JsonConvert.SerializeObject(agentUplineDyn);
                    List<dynamic> agentUplineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgent);

                    lblUplineName.Text = agentUplineList.FirstOrDefault().name.ToString();
                    lblUplineRank.Text = agentUplineList.FirstOrDefault().agent_rank.ToString();
                    lblUplineMobileNo.Text = agentUplineList.FirstOrDefault().hand_phone.ToString();
                }

                //To Populate CPD Points
                string mainQE = @"SELECT SUM(att.cpd_points) as cpd_points FROM agent_training_topics att
                join agent_training_enrollments ate on att.id = ate.agent_training_topic_id
                where (ate.is_reported=1 and ate.is_attended=1) and ((att.is_assessment = 1 and ate.is_passed=1) or (att.is_assessment=0 and ate.is_passed=0)) and ate.user_id=" + user.Id + " and YEAR(ate.created_date)=" + DateTime.Now.Year + ";";
                    Response responseEList = GenericService.GetDataByQuery(mainQE, 0, 0, false, null, false, null, true);
                    if (responseEList.IsSuccess)
                    {
                        var CourseEDyn = responseEList.Data;
                        var responseEJSON = JsonConvert.SerializeObject(CourseEDyn);
                        List<dynamic> CPDPointsObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseEJSON);
                        if (CPDPointsObj.Count > 0)
                        {
                            lblCPDPoints.InnerHtml = CPDPointsObj.FirstOrDefault().cpd_points;
                        }
                        else
                        {
                            lblCPDPoints.InnerHtml = "0";
                        }
                        CPDYear.InnerHtml = "(" + DateTime.Now.Year + ")";
                    }

                //To populate the Transactions for AUM data
                string queryGetMaHolder = @"select GROUP_CONCAT(ua.account_no) as account_no, ap.agent_pos, ap.agent_code,ar.user_id
                                            from di_otp_ag.agent_positions ap
                                            join agent_regs ar on ap.agent_code = ar.agent_code
                                            join agent_reg_personal arp on ar.id = arp.agent_reg_id
                                            join agent_clients ac on ac.agent_user_id = ar.user_id
                                            join user_accounts ua on ua.user_id = ac.user_id
                                            where ap.agent_pos like '"+agentPos+ "%' group by ar.user_id order by char_length(agent_pos) asc";
                Response responseGetMaHolder = GenericService.GetDataByQuery(queryGetMaHolder, 0, 0, false, null, false, null, true);
                if (responseGetMaHolder.IsSuccess) {
                    var maHolderDyn = responseGetMaHolder.Data;
                    var responseMaHolderDyn = JsonConvert.SerializeObject(maHolderDyn);
                    List<dynamic> maHolderAccountList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseMaHolderDyn);
                    string personalMaHolderAcc = "";
                    string downlineMaHolderAcc = "";
                    string indirectDownlineMaHolderAcc = "";
                    double totalGroup = 0.0;
                    double personalGroup = 0.0;
                    int agentPosCounter = 0;
                    int directCount = 0;
                    int indirectCount = 0;
                    maHolderAccountList.ForEach(x=>{

                        //if same agentPos, personal
                        if (x.agent_pos == agentPos)
                        {
                            personalMaHolderAcc = x.account_no;
                            agentPosCounter = Convert.ToString(x.agent_pos).Length;
                        }
                        //if it is initial + 3 = downline. Else indirect.
                        else if (Convert.ToString(x.agent_pos).Length == (agentPosCounter+3) ) {
                            //if == 0, it is first acc
                            if (directCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            downlineMaHolderAcc = x.account_no;
                            directCount++;
                        }
                        else {
                            //if == 0, it is first acc
                            if (indirectCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            indirectDownlineMaHolderAcc = x.account_no;
                            indirectCount++;
                        }
                    });

                    //personal
                    if (!String.IsNullOrEmpty(personalMaHolderAcc)) {
                        //To get current holdings from Oracle for personal
                        string queryGetPCH = @"select Sum(curr_unit_hldg) as current_unit_holdings from UTS.holder_inv where holder_no in (" + personalMaHolderAcc + ")";
                        Response responseGetPCH = ServicesManager.GetDataByQuery(queryGetPCH);
                        if (responseGetPCH.IsSuccess)
                        {
                            var getPCHDyn = responseGetPCH.Data;
                            var responseGetPCHData = JsonConvert.SerializeObject(getPCHDyn);
                            List<dynamic> personalCHList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetPCHData);

                            lblPersonalVal.Text = personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? "0.00" : Convert.ToDouble(personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS).ToString("N", CultureInfo.CurrentCulture);
                            personalGroup += (personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? 0.00 : Convert.ToDouble(personalCHList.FirstOrDefault().CURRENTUNITHOLDINGS));
                        }
                    }

                    if (!String.IsNullOrEmpty(downlineMaHolderAcc)) {
                        //To get current holdings from Oracle for group
                        string queryGetDirectPGCH = @"select Sum(curr_unit_hldg) as current_unit_holdings from UTS.holder_inv where holder_no in (" + downlineMaHolderAcc + ")";
                        Response responseGetDirectPGCH = ServicesManager.GetDataByQuery(queryGetDirectPGCH);
                        if (responseGetDirectPGCH.IsSuccess)
                        {
                            var getDirectPGCHDyn = responseGetDirectPGCH.Data;
                            var responseGetDirectPGCHData = JsonConvert.SerializeObject(getDirectPGCHDyn);
                            List<dynamic> personalGroupCHList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetDirectPGCHData);

                            //personalGroup += personalGroupCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ?? 0;
                            personalGroup += (personalGroupCHList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? 0.00 : Convert.ToDouble(personalGroupCHList.FirstOrDefault().CURRENTUNITHOLDINGS));
                            totalGroup += personalGroup;
                        }
                    }

                    if (!String.IsNullOrEmpty(indirectDownlineMaHolderAcc))
                    {

                        //Populate current holdings for personal + group = total group
                        //To get current holdings from Oracle for Indirect Downlines.
                        string queryGetIndirectPGCH = @"select Sum(curr_unit_hldg) as current_unit_holdings from UTS.holder_inv where holder_no in (" + indirectDownlineMaHolderAcc + ")";
                        Response responseGetIndirectPGCH = ServicesManager.GetDataByQuery(queryGetIndirectPGCH);
                        if (responseGetIndirectPGCH.IsSuccess)
                        {
                            var getIndirectPGCHDyn = responseGetIndirectPGCH.Data;
                            var responseGetIndirectPGCHData = JsonConvert.SerializeObject(getIndirectPGCHDyn);
                            List<dynamic> indirectGroupList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetIndirectPGCHData);

                            totalGroup += (indirectGroupList.FirstOrDefault().CURRENTUNITHOLDINGS == null ? 0 : Convert.ToDouble(indirectGroupList.FirstOrDefault().CURRENTUNITHOLDINGS));
                        }
                    }
                    lblPersonalGroupVal.Text = personalGroup.ToString("N", CultureInfo.CurrentCulture);
                    lblTotalGroupVal.Text = totalGroup.ToString("N", CultureInfo.CurrentCulture);
                }

                //To populate Sales and redemption table data
                string querySR = @"select 
                                     GROUP_CONCAT(uo.reject_reason) as trans_no, 
                                    uo.order_type, uo.order_status, uo.consultantId, 
                                    ap.agent_pos, DAY(uo.updated_date) as updated_day, MONTH(uo.updated_date) as updated_month, YEAR(uo.updated_date) as updated_year 
                                    from user_orders uo
                                    join user_accounts ua on uo.user_id = ua.user_id
                                    join agent_regs ar on ar.agent_code = uo.consultantId
                                    join agent_positions ap on ar.agent_code = ap.agent_code
                                    where ap.agent_pos like '"+agentPos+"%' group by uo.consultantId  Order by char_length(ap.agent_pos) asc";
                Response responserSR = GenericService.GetDataByQuery(querySR, 0, 0, false, null, false, null, true);
                if (responserSR.IsSuccess) {
                    var srDyn = responserSR.Data;
                    var responseSR = JsonConvert.SerializeObject(srDyn);
                    List<dynamic> agentSR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseSR);

                    //maholder account numbers
                    string personalMaHolderAcc = "";
                    string downlineMaHolderAcc = "";
                    string indirectDownlineMaHolderAcc = "";
                    int maDownlineCount = 0;
                    int maIndirectCount = 0;

                    //Temp for initial pos.
                    int agentPosCounter = 0;

                    //values for sales
                    double salesPersonalMTD = 0.0;
                    double salesPGMTD = 0.0;
                    double salesPGTMTD = 0.0;
                    double salesPersonalYTD = 0.0;
                    double salesPGYTD = 0.0;
                    double salesPGTYTD = 0.0;

                    //valus for redemption
                    double rdPersonalMTD = 0.0;
                    double rdPGMTD = 0.0;
                    double rdPGTMTD = 0.0;
                    double rdPersonalYTD = 0.0;
                    double rdPGYTD = 0.0;
                    double rdPGTYTD = 0.0;

                    agentSR.ForEach(x=>{
                        //if order_type = 1 (Sales), order_status = 3 (approved) 39(rejected),MTD = all days in current month, agentPos = current agent Pos
                        string[] transNos = x.trans_no.ToString().Split(',');
                        for (int i = 0; i < transNos.Length; i++) {
                            transNos[i] = "'" + transNos[i] + "'";
                        }
                        string transactionsNo = String.Join(",",transNos);
                        if (x.agent_pos == agentPos)
                        {

                            personalMaHolderAcc = transactionsNo;
                            agentPosCounter = Convert.ToString(x.agent_pos).Length;
                        }
                        else if (Convert.ToString(x.agent_pos).Length == (agentPosCounter + 3))
                        {
                            //if == 0, it is first acc
                            if (maDownlineCount > 0) {
                                downlineMaHolderAcc += ",";
                            }
                            downlineMaHolderAcc += transactionsNo;
                            maDownlineCount++;
                        }
                        else {
                            //if == 0, it is first acc
                            if (maIndirectCount > 0)
                            {
                                downlineMaHolderAcc += ",";
                            }
                            indirectDownlineMaHolderAcc = transactionsNo;
                            maIndirectCount++;
                        }
                    });
                    
                    //populate personal value
                    string queryGetPMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + personalMaHolderAcc+")";
                    Response responseGetPersonalSR = ServicesManager.GetDataByQuery(queryGetPMaholderSR);
                    if (responseGetPersonalSR.IsSuccess)
                    {
                        var getPersonalSRDyn = responseGetPersonalSR.Data;
                        var responseGetPersonalSRData = JsonConvert.SerializeObject(getPersonalSRDyn);
                        List<dynamic> personalSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetPersonalSRData);

                        salesPersonalMTD = personalSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                        salesPersonalYTD = personalSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                        rdPersonalMTD = personalSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                        rdPersonalYTD = personalSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                        //display sales value @personal
                        lblSalesMTDPS.Text = salesPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDPS.Text = salesPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                        //display redemption value @personal
                        lblRdMPD.Text = rdPersonalMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYPD.Text = rdPersonalYTD.ToString("N", CultureInfo.CurrentCulture);
                    }

                    //populate PG value
                    string queryGetDirectMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + downlineMaHolderAcc + ")";
                    Response responseGetDirectSR = ServicesManager.GetDataByQuery(queryGetDirectMaholderSR);
                    if (responseGetDirectSR.IsSuccess)
                    {
                        var getDirectSRDyn = responseGetDirectSR.Data;
                        var responseGetDirectSRData = JsonConvert.SerializeObject(getDirectSRDyn);
                        List<dynamic> directDownlinelSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetDirectSRData);

                        salesPGMTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                        salesPGYTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                        rdPGMTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                        rdPGYTD = directDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                        //display sales value @personal
                        salesPGMTD += salesPersonalMTD;
                        salesPGYTD += salesPersonalYTD;
                        lblSalesMTDPGS.Text = salesPGMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDPGS.Text = salesPGYTD.ToString("N", CultureInfo.CurrentCulture);
                        //display redemption value @personal
                        rdPGMTD += rdPersonalMTD;
                        rdPGYTD += rdPersonalYTD;
                        lblRdMPGD.Text = rdPGMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYPGD.Text = rdPGYTD.ToString("N", CultureInfo.CurrentCulture);
                    }
                    else {
                        //display sales value @personal
                        salesPGMTD += salesPersonalMTD;
                        salesPGYTD += salesPersonalYTD;
                        lblSalesMTDPGS.Text = salesPGMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDPGS.Text = salesPGYTD.ToString("N", CultureInfo.CurrentCulture);
                        //display redemption value @personal
                        rdPGMTD += rdPersonalMTD;
                        rdPGYTD += rdPersonalYTD;
                        lblRdMPGD.Text = rdPGMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYPGD.Text = rdPGYTD.ToString("N", CultureInfo.CurrentCulture);
                    }

                    //populate PGT value
                    string queryGetIndirectMaholderSR = @"select holder_no,trans_no, trans_amt, trans_type, extract(day from trans_dt) as trans_day, extract(month from trans_dt) as trans_month, extract( year from trans_dt) as trans_year from uts.holder_ledger where trans_no in (" + indirectDownlineMaHolderAcc + ")";
                    Response responseGetIndirectSR = ServicesManager.GetDataByQuery(queryGetIndirectMaholderSR);
                    if (responseGetIndirectSR.IsSuccess)
                    {
                        var getIndirectSRDyn = responseGetIndirectSR.Data;
                        var responseGetIndirectSRData = JsonConvert.SerializeObject(getIndirectSRDyn);
                        List<dynamic> indirectDownlinelSRList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetIndirectSRData);

                        salesPGTMTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                        salesPGTYTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "SA" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                        rdPGTMTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSMONTH == DateTime.Now.Month && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);
                        rdPGTYTD = indirectDownlinelSRList.Where(x => x.TRANSTYPE == "RD" && x.TRANSYEAR == DateTime.Now.Year).Sum(x => x.TRANSAMT);

                        //display sales value @personal
                        salesPGTMTD += salesPGMTD;
                        salesPGTYTD += salesPGYTD;
                        lblSalesMTDTGS.Text = salesPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDTGS.Text = salesPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                        //display redemption value @personal
                        rdPGTMTD += rdPGMTD;
                        rdPGTYTD += rdPGYTD;
                        lblRdMTGD.Text = rdPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYTGD.Text = rdPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    }
                    else {
                        //display sales value @personal
                        salesPGTMTD += salesPGMTD;
                        salesPGTYTD += salesPGYTD;
                        lblSalesMTDTGS.Text = salesPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblSalesYTDTGS.Text = salesPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                        //display redemption value @personal
                        rdPGTMTD += rdPGMTD;
                        rdPGTYTD += rdPGYTD;
                        lblRdMTGD.Text = rdPGTMTD.ToString("N", CultureInfo.CurrentCulture);
                        lblRdYTGD.Text = rdPGTYTD.ToString("N", CultureInfo.CurrentCulture);
                    }
                }

                //To populate commission table data
                string queryCommTable = @"select *, YEAR(updated_date) as updated_year, Month(updated_date) as updated_month from comm_ledger where agent_code = '" + user.AgentCode+"'";
                Response responseAgentCommTable = GenericService.GetDataByQuery(queryCommTable, 0, 0, false, null, false, null, true);
                if (responseAgentCommTable.IsSuccess)
                {
                    var agentCommTableDyn = responseAgentCommTable.Data;
                    var responseAgentComm = JsonConvert.SerializeObject(agentCommTableDyn);
                    List<dynamic> agentCommList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentComm);

                    double commMTD = agentCommList.Where(x => x.updated_month == DateTime.Now.Month && x.updated_year == DateTime.Now.Year).Sum(x => x.comm_psc + x.comm_oc);
                    double commYTD = agentCommList.Where(x => x.updated_year == DateTime.Now.Year).Sum(x => x.comm_psc + x.comm_oc);

                    lblCommissionMPSC.Text = commMTD.ToString("N",CultureInfo.CurrentCulture);
                    lblCommissionYPSC.Text = commYTD.ToString("N",CultureInfo.CurrentCulture);
                }


                    //To populate direct and indirect agents table data
                    string queryDownlines = @"select arp.name,utd.name as agent_rank,GROUP_CONCAT(ua.account_no) as account_no, arp.Status, ap.agent_pos, ap.agent_code,ar.user_id,  IFNULL(SUM(att.cpd_points),0) as cpd_points,  DATEDIFF(arf.expiry_date,NOW()) as expire_time_left
                                        from di_otp_ag.agent_positions ap
                                        join agent_regs ar on ap.agent_code = ar.agent_code
                                        join agent_reg_personal arp on ar.id = arp.agent_reg_id
										join user_types ut on ut.user_id = ar.user_id
										join user_types_def utd on utd.id=ut.user_type_id and utd.name in ('SGM', 'GM', 'WM', 'WA')                             
                                        join agent_reg_fimms arf on arf.agent_reg_id = ar.id
										left join agent_clients ac on ac.agent_user_id = ar.user_id
										left join user_accounts ua on ua.user_id = ac.user_id
                                        left join agent_training_enrollments ate on ate.user_id = ar.user_id 
                                        left join agent_training_topics att on ate.agent_training_topic_id  = att.id and (ate.is_reported=1 and ate.is_attended=1) and ((att.is_assessment = 1 and ate.is_passed=1) or (att.is_assessment=0 and ate.is_passed=0))
                                        where ap.agent_pos like '" + agentPos+"%' and char_length(ap.agent_pos) >"+agentPos.Length+ " group by arp.id";
                Response responseAgentDownlines = GenericService.GetDataByQuery(queryDownlines, 0, 0, false, null, false, null, true);
                if (responseAgentDownlines.IsSuccess) {
                    var agentDownlineDyn = responseAgentDownlines.Data;
                    var responseAgentDownline = JsonConvert.SerializeObject(agentDownlineDyn);
                    List<dynamic> agentDownlineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseAgentDownline);

                    string accNos = String.Join(",", agentDownlineList.Where(x => !String.IsNullOrEmpty(Convert.ToString(x.account_no))).Select(x => x.account_no));

                    //To get current holdings from Oracle 
                    string queryGetAUM = @"select curr_unit_hldg, holder_no from UTS.holder_inv where holder_no in (" + accNos + ")";
                    Response responseGetAUM = ServicesManager.GetDataByQuery(queryGetAUM);
                    if (responseGetAUM.IsSuccess)
                    {
                        var getAUMDyn = responseGetAUM.Data;
                        var responseGetAUMData = JsonConvert.SerializeObject(getAUMDyn);
                        List<dynamic> personalAUMList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseGetAUMData);


                        int directIndex = 0;
                        int indirectIndex = 0;
                        StringBuilder directTableBuilder = new StringBuilder();
                        StringBuilder indirectTableBuilder = new StringBuilder();
                        agentDownlineList.ForEach(x =>
                        {
                            string agentAccNos = x.account_no;
                            string aum = "0.00";
                            if (!String.IsNullOrEmpty(agentAccNos))
                            {
                                string[] agentAccountNosSplit = agentAccNos.Split(',');
                                var agentAUM = personalAUMList.Where(y => agentAccountNosSplit.ToList().Contains(Convert.ToString(y.HOLDERNO))).Sum(y => y.CURRUNITHLDG);
                                aum = Convert.ToDouble(agentAUM).ToString("N", CultureInfo.CurrentCulture);
                            }

                            string agentPositionString = x.agent_pos;
                            int agentPositionLength = agentPositionString.Length;

                            //Populate Direct Downline
                            if (agentPositionLength > agentPos.Length && agentPositionLength <= agentPos.Length + 3)
                            {
                                ++directIndex;
                                directTableBuilder.Append(
                                                @"
                                            <tr class='" + "trTextFormat" + @"'>
                                            <td>" + directIndex + @"</td>
                                            <td>" + x.name + @"</td>
                                            <td>" + x.agent_code + @"</td>
                                            <td>" + x.Status + @"</td>
                                            <td>" + x.agent_rank + @"</td>
                                            <td>" + aum + @"</td>
                                            <td>10</td>
                                            <td>" + x.cpd_points + @"</td>
                                            <td>" + x.expire_time_left + @" days until expire</td>
                                        </tr>");

                            }
                            //Populate Indirect Downline
                            else
                            {
                                ++indirectIndex;
                                indirectTableBuilder.Append(
                                                @"
                                            <tr class='" + "trTextFormat" + @"'>
                                            <td>" + directIndex + @"</td>
                                            <td>" + x.name + @"</td>
                                            <td>" + x.agent_code + @"</td>
                                            <td>" + x.Status + @"</td>
                                            <td>" + x.agent_rank + @"</td>
                                            <td>" + aum + @"</td>
                                            <td>10</td>
                                            <td>" + x.cpd_points + @"</td>
                                            <td>" + x.expire_time_left + @" days until expire</td>
                                        </tr>");

                            }
                        });
                        if (directIndex != 0)
                        {
                            tbodyDirectDownline.InnerHtml = directTableBuilder.ToString();
                        }
                        else
                        {
                            tbodyDirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td>No data available for display.</td></tr>";
                        }

                        if (indirectIndex != 0)
                        {
                            tbodyIndirectDownline.InnerHtml = indirectTableBuilder.ToString();
                        }
                        else
                        {
                            tbodyIndirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td>No data available for display.</td></tr>";
                        }

                    }
                    else {
                        tbodyDirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td>No data available for display.</td></tr>";

                        tbodyIndirectDownline.InnerHtml = " <tr class='" + "trTextFormat" + @"'>
                                            <td>No data available for display.</td></tr>";
                    }

                }

            }
            else {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetCommissionStats(string commCode)
        {
            User user = (User) HttpContext.Current.Session["user"];
            if (user != null) {
                string queryCommStats = @"select sum(comm_psc) as comm_psc, sum(comm_oc) as comm_oc, YEAR(updated_date) as updated_year, Month(updated_date)as updated_month from comm_ledger 
                                    where agent_code = '" + user.AgentCode + "' group by month(updated_date) order by month(updated_date) asc";
                Response responseCommStats = GenericService.GetDataByQuery(queryCommStats, 0, 0, false, null, false, null, true);
                if (responseCommStats.IsSuccess)
                {
                    var commStatsDyn = responseCommStats.Data;
                    var responseCommStatsJSON = JsonConvert.SerializeObject(commStatsDyn);
                    List<dynamic> commStatsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(responseCommStatsJSON);
                    List<getStatsByYear> statByYear = new List<getStatsByYear>();

                    


                    statByYear.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(DateTime.Now.Year),
                    });

                    statByYear.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year)-1),
                    });

                    statByYear.Add(new getStatsByYear
                    {
                        Year = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 2),
                    });

                    //To initialize 0 values for agent, who has no commission on a specific month.
                    statByYear.ForEach(y=> {
                        List<getStatsByMonth> statByMonths = new List<getStatsByMonth>();
                        // take first 3 years assign months, year and commval 0;
                        for (int i = 0; i < 12; i++)
                        {

                            getStatsByMonth statMonth = new getStatsByMonth();
                            statMonth.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i + 1);
                            statMonth.commVal = "0";
                            statByMonths.Add(statMonth);

                        }
                        y.statByMonth = statByMonths;
                    });
                    

                    if (commCode == "PC")
                    {
                        commStatsList.GroupBy(x => x.updated_year).ToList().ForEach(x =>
                        {

                            if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().updated_year,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = x.First().comm_psc }).ToList()
                                };

                                var yearIndex = statByYear.IndexOf(statByYear.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
                                var monthIndex = statByYear[yearIndex].statByMonth.IndexOf(statByYear[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
                                statByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                            }
                        });

                    }

                    if (commCode == "OC")
                    {
                        commStatsList.GroupBy(x => x.updated_year).ToList().ForEach(x =>
                        {
                            if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().updated_year,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = x.First().comm_oc }).ToList()
                                };

                                var yearIndex = statByYear.IndexOf(statByYear.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
                                var monthIndex = statByYear[yearIndex].statByMonth.IndexOf(statByYear[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
                                statByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                            }
                        });
                    }

                    if (commCode == "ALL")
                    {
                        commStatsList.GroupBy(x => x.updated_year).ToList().ForEach(x =>
                        {
                            if (DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) >= 0 && DateTime.Now.Year - Convert.ToInt32(x.First().updated_year) < 3)
                            {
                                getStatsByYear singleStatByYear = new getStatsByYear
                                {
                                    Year = x.First().updated_year,
                                    statByMonth = x.Select(y => new getStatsByMonth { Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month)), commVal = (Convert.ToDouble(x.First().comm_psc) + Convert.ToDouble(x.First().comm_oc)).ToString("N",CultureInfo.CurrentCulture) }).ToList()
                                };

                                var yearIndex = statByYear.IndexOf(statByYear.Where(i => i.Year == Convert.ToString(x.First().updated_year)).First());
                                var monthIndex = statByYear[yearIndex].statByMonth.IndexOf(statByYear[yearIndex].statByMonth.First(i => i.Month == CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(x.First().updated_month))));
                                statByYear[yearIndex].statByMonth[monthIndex].commVal = singleStatByYear.statByMonth.First().commVal;
                            }
                        });
                    }

                    statByYear = statByYear.OrderBy(x => x.Year).ToList();

                    return statByYear;
                }
            }
            
            return null;
        }

        public class getStatsByMonth {
            public string Month { get; set; }
            public string commVal { get; set; }
        }

        public class getStatsByYear {

            public string Year { get; set; }
            public List<getStatsByMonth> statByMonth { get; set; }
        }
    }
}