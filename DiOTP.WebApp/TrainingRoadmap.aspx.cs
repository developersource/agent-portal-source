﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class TrainingRoadmap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRoadmap();
            }
        }

        public void BindRoadmap()
        {
            User user = (User)(Session["user"]);
            if (user != null)
            {
                StringBuilder filterE = new StringBuilder();
                string mainQE = (@"SELECT t.*, i.name as instructor_name, e.created_date as enrolled_date ");
                filterE.Append(@" FROM agent_training_enrollments e
                            left join agent_training_topics t on t.id = e.agent_training_topic_id
                            left join instructors i on i.id = t.instructor_id
                             where 1=1 ");
                filterE.Append("and e.is_active=1 and e.user_id='" + user.Id + "' ");
                filterE.Append("order by e.id desc");
                Response responseEList = GenericService.GetDataByQuery(mainQE + filterE.ToString(), 0, 0, false, null, false, null, false);
                if (responseEList.IsSuccess)
                {
                    var CourseEDyn = responseEList.Data;
                    var responseEJSON = JsonConvert.SerializeObject(CourseEDyn);
                    List<CourseListingView> courselisting = JsonConvert.DeserializeObject<List<CourseListingView>>(responseEJSON);
                    StringBuilder timelineHtml = new StringBuilder();
                    courselisting.ForEach(x =>
                    {
                        timelineHtml.Append(@"<li class='rb-item'>
                                                <div class='timestamp'>
                                                    " + x.EnrolledDate.ToString("dd/MM/yyyy") + @"<br>
                                                    " + x.EnrolledDate.ToString("hh:mm tt") + @"
                                                </div>
                                                <div class='item-title'>" + x.Topic + @" (Training Enrolled)</div>
                                            </li>");
                    });
                    timelineUL.InnerHtml = timelineHtml.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseEList.Message + "\", '');", true);
                }
            }
        }
    }
}