﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_signups")]
    public class AgentSignup
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("registration_type"), Display("RegistrationType")]
        public Int32 RegistrationType { get; set; }
        [Database("introducer_code"), Display("IntroducerCode")]
        public String IntroducerCode { get; set; }
        [Database("upline_code"), Display("UplineCode")]
        public String UplineCode { get; set; }
        [Database("exam_preferences"), Display("ExamPreferences")]
        public String ExamPreferences { get; set; }
        [Database("process_status"), Display("ProcessStatus")]
        public Int32 ProcessStatus { get; set; }
        [Database("remarks"), Display("Remarks")]
        public String Remarks { get; set; }
        [Database("agent_code"), Display("AgentCode")]
        public String AgentCode { get; set; }
        [Database("is_new_agent"), Display("IsNewAgent")]
        public Int32 IsNewAgent { get; set; }
        [Database("qualification_url"), Display("QualificationUrl")]
        public String QualificationUrl { get; set; }
        [Database("ic_url"), Display("IcUrl")]
        public String IcUrl { get; set; }
        [Database("selfie_url"), Display("SelfieUrl")]
        public String SelfieUrl { get; set; }
        [Database("pop_url"), Display("PopUrl")]
        public String PopUrl { get; set; }
        [Database("new_card_received_on"), Display("NewCardReceivedOn")]
        public DateTime? NewCardReceivedOn { get; set; }
        [Database("FIMM_no"), Display("FIMMNo")]
        public String FIMMNo { get; set; }
        [Database("approval_date"), Display("ApprovalDate")]
        public DateTime? ApprovalDate { get; set; }
        [Database("expiry_date"), Display("ExpiryDate")]
        public DateTime? ExpiryDate { get; set; }
        [Database("from_agent_MICR_code"), Display("FromAgentMICRCode")]
        public String FromAgentMICRCode { get; set; }
        [Database("from_agent_bank_name"), Display("FromAgentBankName")]
        public String FromAgentBankName { get; set; }
        [Database("from_agent_cheque_no"), Display("FromAgentChequeNo")]
        public String FromAgentChequeNo { get; set; }
        [Database("from_agent_trans_ref_no"), Display("FromAgentTransRefNo")]
        public String FromAgentTransRefNo { get; set; }
        [Database("from_agent_amount"), Display("FromAgentAmount")]
        public Decimal? FromAgentAmount { get; set; }
        [Database("from_agent_date_received"), Display("FromAgentDateReceived")]
        public DateTime? FromAgentDateReceived { get; set; }
        [Database("from_agent_payment_ref_no"), Display("FromAgentPaymentRefNo")]
        public String FromAgentPaymentRefNo { get; set; }
        [Database("from_agent_deposit_acc_no"), Display("FromAgentDepositAccNo")]
        public String FromAgentDepositAccNo { get; set; }
        [Database("to_FiMM_with_acc_no"), Display("ToFiMMWithAccNo")]
        public String ToFiMMWithAccNo { get; set; }
        [Database("to_FiMM_MICR_code"), Display("ToFiMMMICRCode")]
        public String ToFiMMMICRCode { get; set; }
        [Database("to_FiMM_bank_name"), Display("ToFiMMBankName")]
        public String ToFiMMBankName { get; set; }
        [Database("to_FiMM_cheque_no"), Display("ToFiMMChequeNo")]
        public String ToFiMMChequeNo { get; set; }
        [Database("to_FiMM_date_sent"), Display("ToFiMMDateSent")]
        public DateTime? ToFiMMDateSent { get; set; }
        [Database("to_FiMM_FIMM_renewal_amount"), Display("ToFiMMFIMMRenewalAmount")]
        public Decimal? ToFiMMFIMMRenewalAmount { get; set; }
        [Database("to_FiMM_UTMC_renewal_amount"), Display("ToFiMMUTMCRenewalAmount")]
        public Decimal? ToFiMMUTMCRenewalAmount { get; set; }
        [Database("is_active"), Display("IsActive")]
        public Int32 IsActive { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
