﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentMaster.master" AutoEventWireup="true" CodeBehind="ELearning.aspx.cs" Inherits="DiOTP.WebApp.ELearning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" type="text/css" />
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" />  --%>
    <script src="https://code.jquery.com/jquery.min.js"></script>

    <meta charset="utf-8" />
    <style>
        .iconbox:hover {
            -webkit-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
            -moz-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
            -ms-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
            -o-box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.06);
            -webkit-transform: translateY(-10px);
            -moz-transform: translateY(-10px);
            -ms-transform: translateY(-10px);
            -o-transform: translateY(-10px);
            transform: translateY(-10px);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/AgentDashboard.aspx">Dashboard</a></li>
                    <li class="active">e-Learning</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">e-Learning</h3>
            </div>
            <div id="containerPage">
                <div class="row">
                    <div class="col-md-8">
                        <div>
                            <div class="intro" style="padding-bottom: 50px;">
                                <div class="row">
                                    <div style="padding-bottom: 10px;">
                                        <b>IMPORTANT NOTICE</b>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-1">
                                        <img src="Content/MyImage/9.png" style="height: 10px; width: 10px;" />
                                    </div>
                                    <div class="col-md-11">
                                        <div>Welcome back</div>
                                        <div>In order to improve the efficiency and effectiveness</div>
                                    </div>
                                </div>
                            </div>
                            <div class="training" style="padding-bottom: 50px; padding-top: 50px;">
                                <div class="row">
                                    <div style="padding-bottom: 20px;">
                                        <b>TRAINING</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 containers">
                                        <a href="TrainingRoadmap.aspx" class="" style="color: black;">
                                                <div style="text-align: center;">
                                            <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                        </div>
                                        <div style="text-align: center; font-size: 20px;">
                                            My Training Roadmap
                                        </div>
                                            </a>
                                        
                                    </div>
                                    <div class="col-md-4 containers">
                                        <a href="TrainingSession.aspx?type=0" class="" style="color: black;">
                                                <div style="text-align: center;">
                                            <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                        </div>
                                        <div style="text-align: center; font-size: 20px;">
                                            Classroom Training
                                        </div>
                                            </a>
                                    </div>
                                    <div class="col-md-4 containers">
                                        <a href="TrainingSession.aspx?type=1" class="" style="color: black;">
                                                <div style="text-align: center;">
                                            <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                        </div>
                                        <div style="text-align: center; font-size: 20px;">
                                            Online Training
                                        </div>
                                            </a>
                                    </div>
                                </div>
                            </div>
                            <%--<div class="reports" style="padding-bottom: 50px; padding-top: 10px;">
                                <div class="row">
                                    <div style="padding-bottom: 20px;">
                                        <b>REPORTS</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="javascript::" class="" data-toggle="modal" data-target="" style="color: black;">
                                                <div style="text-align: center;">
                                            <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                        </div>
                                        <div style="text-align: center; font-size: 20px;">
                                            Test
                                        </div>
                                            </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="javascript::" class="" data-toggle="modal" data-target="" style="color: black;">
                                                <div style="text-align: center;">
                                            <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                        </div>
                                        <div style="text-align: center; font-size: 20px;">
                                            Test
                                        </div>
                                            </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="javascript::" class="" data-toggle="modal" data-target="" style="color: black;">
                                                <div style="text-align: center;">
                                            <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                        </div>
                                        <div style="text-align: center; font-size: 20px;">
                                            Test
                                        </div>
                                            </a>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
            <%--<div id="selectionPage" class="hide">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <div class="intro" style="padding-bottom: 50px;">
                                <div class="row">
                                    <div style="padding-bottom: 10px;">
                                        <b>IMPORTANT NOTICE</b>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-1">
                                        <img src="Content/MyImage/9.png" style="height: 10px; width: 10px;" />
                                    </div>
                                    <div class="col-md-11">
                                        <div>Welcome back</div>
                                        <div>In order to improve the efficiency and effectiveness</div>
                                    </div>
                                </div>
                            </div>
                            <div class="training" style="padding-bottom: 50px; padding-top: 50px;">
                                <div class="row">
                                    <div style="padding-bottom: 20px;">
                                        <b>TRAINING</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="iconbox" style="height: 200px; padding: 40px 40px 30px 40px; transition: all 0.3s ease-in-out; background: #f3f3f3;">
                                            <a href="#" class="" data-toggle="modal" data-target="#detailsPanel" style="color: black;">
                                                <div style="text-align: center;">
                                                    <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                                </div>
                                                <div style="text-align: center; font-size: 20px;">
                                                    Test
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="iconbox" style="height: 200px; padding: 40px 40px 30px 40px; transition: all 0.3s ease-in-out; background: #f3f3f3;">
                                            <a href="#" class="" data-toggle="modal" data-target="#detailsPanel" style="color: black;">
                                                <div style="text-align: center;">
                                                    <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                                </div>
                                                <div style="text-align: center; font-size: 20px;">
                                                    Test
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="iconbox" style="height: 200px; padding: 40px 40px 30px 40px; transition: all 0.3s ease-in-out; background: #f3f3f3;">
                                            <div style="text-align: center;">
                                                <img src="Content/MyImage/9.png" style="height: 120px; width: 120px;" />
                                            </div>
                                            <div style="text-align: center; font-size: 20px;">
                                                Test
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div class="modal fade" id="detailsPanel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Test1</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row mt-m-40">
                                <div class="col-md-8">
                                    <%--<a href="javascript:;" class="btn portfolio-btn btn-active btn-infos1" id="ConsolidatedAccount"><i class="fa fa-caret-down"></i>Consolidated Account</a>--%>
                                    <!--begin tabs going in narrow content -->
                                    <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist">
                                        <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Recent</a></li>
                                        <li><a href="#tab-2" role="tab" data-toggle="tab">Popular</a></li>
                                        <li><a href="#tab-3" role="tab" data-toggle="tab">Tags</a></li>
                                    </ul>
                                    <!--/.nav-tabs.sidebar-tabs -->
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab-1">
                                            <div class="table-responsive" style="border: none!important;">
                                                <div class="tab-content tabs-content" id="myTabContent">
                                                    <div class="" id="prod-overview" role="tabpanel" aria-labelledby="prod-overview-tab">
                                                        <div class="">
                                                            <!-- Cource Overview -->
                                                            <div class="">
                                                                <div class="inner-box">
                                                                    <h4>Educavo Course Details</h4>
                                                                    <p>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus.</p>
                                                                    <p>Eleifend euismod pellentesque vel Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus.</p>
                                                                    <ul class="student-list">
                                                                        <li>23,564 Total Students</li>
                                                                        <li><span class="theme_color">4.5</span> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>(1254 Rating)</li>
                                                                        <li>256 Reviews</li>
                                                                    </ul>
                                                                    <h3>What you’ll learn?</h3>
                                                                    <ul class="review-list">
                                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                                        <li>Sed consequat justo non mauris pretium at tempor justo.</li>
                                                                        <li>Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                                        <li>Sed consequat justo non mauris pretium at tempor justo.</li>
                                                                        <li>Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                                    </ul>
                                                                    <h3>Requirements</h3>
                                                                    <ul class="review-list">
                                                                        <li>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                                        <li>Ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel.</li>
                                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                                        <li>Varius et commodo ut, ultricies vitae velit. Ut nulla tellus.</li>
                                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="prod-curriculum" role="tabpanel" aria-labelledby="prod-curriculum-tab">
                                                        <div class="content">
                                                            <div id="accordion" class="accordion-box">
                                                                <div class="card accordion block">
                                                                    <div class="card-header" id="headingOne">
                                                                        <h5 class="mb-0">
                                                                            <button class="btn btn-link acc-btn" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">UI/ UX Introduction</button>
                                                                        </h5>
                                                                    </div>

                                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                                        <div class="card-body acc-content current">
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a class="popup-videos play-icon" href="https://www.youtube.com/watch?v=atMUy_bPoQI"><i class="fa fa-play"></i>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"><i class="ripple"></i></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card accordion block">
                                                                    <div class="card-header" id="headingTwo">
                                                                        <h5 class="mb-0">
                                                                            <button class="btn btn-link acc-btn collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Color Theory</button>
                                                                        </h5>
                                                                    </div>
                                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                                        <div class="card-body acc-content">
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"><i class="ripple"></i></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card accordion block">
                                                                    <div class="card-header" id="headingThree">
                                                                        <h5 class="mb-0">
                                                                            <button class="btn btn-link acc-btn collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Basic Typography</button>
                                                                        </h5>
                                                                    </div>
                                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                                        <div class="card-body acc-content">
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"><i class="ripple"></i></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="prod-instructor" role="tabpanel" aria-labelledby="prod-instructor-tab">
                                                        <div class="content pt-30 pb-30 pl-30 pr-30 white-bg">
                                                            <h3 class="instructor-title">Instructors</h3>
                                                            <div class="row rs-team style1 orange-color transparent-bg clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 sm-mb-30">
                                                                    <div class="team-item">
                                                                        <img src="assets/images/team/1.jpg" alt="">
                                                                        <div class="content-part">
                                                                            <h4 class="name"><a href="#">Jhon Pedrocas</a></h4>
                                                                            <span class="designation">Professor</span>
                                                                            <ul class="social-links">
                                                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                                    <div class="team-item">
                                                                        <img src="assets/images/team/2.jpg" alt="">
                                                                        <div class="content-part">
                                                                            <h4 class="name"><a href="#">Jhon Pedrocas</a></h4>
                                                                            <span class="designation">Professor</span>
                                                                            <ul class="social-links">
                                                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="prod-faq" role="tabpanel" aria-labelledby="prod-faq-tab">
                                                        <div class="content">
                                                            <div id="accordion3" class="accordion-box">
                                                                <div class="card accordion block">
                                                                    <div class="card-header" id="headingSeven">
                                                                        <h5 class="mb-0">
                                                                            <button class="btn btn-link acc-btn" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">UI/ UX Introduction</button>
                                                                        </h5>
                                                                    </div>

                                                                    <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-parent="#accordion3">
                                                                        <div class="card-body acc-content current">
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a class="popup-videos play-icon" href="https://www.youtube.com/watch?v=atMUy_bPoQI"><i class="fa fa-play"></i>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"><i class="ripple"></i></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card accordion block">
                                                                    <div class="card-header" id="headingEight">
                                                                        <h5 class="mb-0">
                                                                            <button class="btn btn-link acc-btn collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Color Theory</button>
                                                                        </h5>
                                                                    </div>
                                                                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion3">
                                                                        <div class="card-body acc-content">
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"><i class="ripple"></i></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card accordion block">
                                                                    <div class="card-header" id="headingNine">
                                                                        <h5 class="mb-0">
                                                                            <button class="btn btn-link acc-btn collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">Basic Typography</button>
                                                                        </h5>
                                                                    </div>
                                                                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion3">
                                                                        <div class="card-body acc-content">
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"><i class="ripple"></i></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content">
                                                                                <div class="clearfix">
                                                                                    <div class="pull-left">
                                                                                        <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon"><span class="fa fa-play"></span>What is UI/ UX Design?</a>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <div class="minutes">35 Minutes</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="prod-reviews" role="tabpanel" aria-labelledby="prod-reviews-tab">
                                                        <div class="content pt-30 pb-30 white-bg">
                                                            <div class="cource-review-box mb-30">
                                                                <h4>Stephane Smith</h4>
                                                                <div class="rating">
                                                                    <span class="total-rating">4.5</span> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>&ensp; 256 Reviews
                                                                </div>
                                                                <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                                                                <div class="helpful">Was this review helpful?</div>
                                                                <ul class="like-option">
                                                                    <li><i class="fa fa-thumbs-o-up"></i></li>
                                                                    <li><i class="fa fa-thumbs-o-down"></i></li>
                                                                    <li><a class="report" href="#">Report</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="cource-review-box mb-30">
                                                                <h4>Anna Sthesia</h4>
                                                                <div class="rating">
                                                                    <span class="total-rating">4.5</span> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>&ensp; 256 Reviews
                                                                </div>
                                                                <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                                                                <div class="helpful">Was this review helpful?</div>
                                                                <ul class="like-option">
                                                                    <li><i class="fa fa-thumbs-o-up"></i></li>
                                                                    <li><i class="fa fa-thumbs-o-down"></i></li>
                                                                    <li><a class="report" href="#">Report</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="cource-review-box mb-30">
                                                                <h4>Petey Cruiser</h4>
                                                                <div class="rating">
                                                                    <span class="total-rating">4.5</span> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>&ensp; 256 Reviews
                                                                </div>
                                                                <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                                                                <div class="helpful">Was this review helpful?</div>
                                                                <ul class="like-option">
                                                                    <li><i class="fa fa-thumbs-o-up"></i></li>
                                                                    <li><i class="fa fa-thumbs-o-down"></i></li>
                                                                    <li><a class="report" href="#">Report</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="cource-review-box">
                                                                <h4>Rick O'Shea</h4>
                                                                <div class="rating">
                                                                    <span class="total-rating">4.5</span> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>&ensp; 256 Reviews
                                                                </div>
                                                                <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                                                                <div class="helpful">Was this review helpful?</div>
                                                                <ul class="like-option">
                                                                    <li><i class="fa fa-thumbs-o-up"></i></li>
                                                                    <li><i class="fa fa-thumbs-o-down"></i></li>
                                                                    <li><a class="report" href="#">Report</a></li>
                                                                </ul>
                                                                <a href="#" class="more">View More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.tab-pane -->
                                        <div class="tab-pane" id="tab-2">
                                            <p>Popular Content</p>
                                        </div>
                                        <!--/.tab-pane -->
                                        <div class="tab-pane" id="tab-3">
                                            <p>Tag Content</p>
                                        </div>
                                        <!--/.tab-pane -->
                                    </div>
                                    <!--/.tab-content -->
                                    <%--<span id="tab" class="hidden-xs"><span class="mr-2"><a href="javascript:;" id="tab1" class="maHolderRegNo btn btn-infos1-default" data-accno="Test1" data-id="0" data-toggle="tooltip" title="" data-original-title="Click to See Details"><i class="fa fa-caret-down"></i>Test1 - <small class="fs-10">Test1 </small></a></span><span class="mr-2"><a href="javascript:;" class="maHolderRegNo btn btn-infos1-default" data-accno="Test2" data-id="0" data-toggle="tooltip" title="" data-original-title="Click to See Details"><i class="fa fa-caret-down"></i>Test2 - <small class="fs-10">Test2 </small></a></span><span class="mr-2"><a href="javascript:;" class="maHolderRegNo btn btn-infos1-default" data-accno="Test3" data-id="0" data-toggle="tooltip" title="" data-original-title="Click to See Details"><i class="fa fa-caret-down"></i>Test3 - <small class="fs-10">Test3 </small></a></span></span>
                                            <select name="ddlPorfolioAccountsMenuMobile" id="ddlPorfolioAccountsMenuMobile" class="form-control hidden-md hidden-lg">
	                                            <option value="">Select Account</option>
	                                            <option value="Test1">Test1</option>
	                                            <option value="Test2">Test2</option>
	                                            <option value="Test3">Test3</option>

                                            </select>--%>
                                </div>
                                <div class="col-md-4">
                                    <div style="height: 350px; width: 100%;">
                                        <img src="Content/MyImage/9.png" style="height: 100%; width: 100%;" />
                                    </div>
                                    <div style="padding: 10px 10px 10px 10px">
                                        <div style="width: 100%; background-color: #ffffff; border: 1px solid #ebebeb;">
                                            <div style="padding: 10px 10px 10px 10px">
                                                <div style="padding: 10px 10px 10px 10px">Lectures</div>
                                                <div style="padding: 10px 10px 10px 10px">Quizzes</div>
                                                <div style="padding: 10px 10px 10px 10px">Duration</div>
                                                <div style="padding: 10px 10px 10px 10px">Students</div>
                                                <div style="padding: 10px 10px 10px 10px">Assessments</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding: 10px 10px 10px 10px">
                                        <div style="width: 100%; background-color: #ffffff; border: 1px solid #ebebeb;">
                                            <div style="padding: 20px 20px 20px 20px">
                                                <a href="javascript:;" class="btn btn-infos1" style="padding: 10px 10px 10px 10px; width: 100%;">Enroll now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-infos1" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/moment@2.24.0/min/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.js"></script>
    <script>
        $(document).ready(function () {
            
            
        });
        $(function () {

                $('#calendar').fullCalendar({
                    themeSystem: 'jquery-ui',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        //right: 'month,agendaWeek,agendaDay,listMonth'
                        right: 'month,agendaWeek,listMonth'
                    },
                    //weekNumbers: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                        { "title": "All Day Event", "start": "2020-10-01" },
                        {
                            "title": "Long Event",
                            "start": "2020-10-07", "end": "2020-10-10"
                        },
                        { "groupId": "999", "title": "Repeating Event", "start": "2020-10-09T16:00:00+00:00" },
                        { "groupId": "999", "title": "Repeating Event", "start": "2020-10-16T16:00:00+00:00" },
                        { "title": "Conference", "start": "2020-10-15", "end": "2020-10-17" },
                        { "title": "Meeting", "start": "2020-10-16T10:30:00+00:00", "end": "2020-10-16T12:30:00+00:00" },
                        { "title": "Lunch", "start": "2020-10-16T12:00:00+00:00" },
                        { "title": "Birthday Party", "start": "2020-10-17T07:00:00+00:00" },
                        { "url": "http:\/\/google.com\/", "title": "Click for Google", "start": "2020-10-28" }]
                    //events: 'https://fullcalendar.io/demo-events.json'
                });

            });
        
    </script>
</asp:Content>
