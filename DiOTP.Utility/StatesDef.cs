using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("states_def")]
    public class StatesDef
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("country_def_id"), Display("CountryDefId")]
        public Int32 CountryDefId { get; set; }
        [Database("code"), Display("Code")]
        public String Code { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public CountriesDef CountryDefIdCountriesDef { get; set; }
    }
}
