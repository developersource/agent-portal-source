﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AgentTraineeMaterialListing : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazyUserServiceObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                //Get data to populate and generate agent table dynamically
                StringBuilder filter = new StringBuilder();
                string mainQuery = (@"select * from ");
                string mainQCount = (@"select count(*) from ");
                filter.Append("agent_trainee_materials atm where 1=1 ");
                //Get data 
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }
                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append("and atm.material_name like '%" + Search.Value.Trim() + "%' ");
                }
                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                filter.Append("order by atm.status desc, atm.is_active desc, atm.created_date desc");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseMaterial = GenericService.GetDataByQuery(mainQuery + filter.ToString(), 0, 0, false, null, false, null, true);
                if (responseMaterial.IsSuccess)
                {
                    var materialDyn = responseMaterial.Data;
                    var responseJSON = JsonConvert.SerializeObject(materialDyn);
                    List<AgentTraineeMaterial> materialList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTraineeMaterial>>(responseJSON);
                    StringBuilder sb = new StringBuilder();
                    int index = 1;

                    materialList.ForEach(x =>
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath(x.material_url_path);

                        sb.Append(@"<tr class='fs-12'>
                                    <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + x.id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (x.is_active == 1 ? "<span class='label label-success'>Active</span>" : (x.is_active == 0 ? "<span class='label label-danger'>Inactive</span" : ""/*"<span class='label label-danger'>Suspended</span>"*/)) + @"</span>
                                    </td>
                                    <td><a href='" + x.material_url_path + "'target='_blank'>" + x.material_name + " " + @"<i class='fa fa-eye'></i></a></td>
                                    <td>" + x.material_description + @"</td>
                                    <td>" + x.created_date.ToString("dd/MM/yyyy") + @"</td>
                                </tr>");
                        index++;

                    });
                    TraineeMaterialTBody.InnerHtml = sb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('" + responseMaterial.Message + "');", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(dynamic obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            int id = Convert.ToInt32(obj["Id"]);
            String materialName;
            String materialDescription;
            String materialFileName;
            if (id == 0)
            {
                materialName = obj["MaterialNameTxt"];
                materialDescription = obj["MaterialDescriptionTxtArea"];
                materialFileName = obj["MaterialURL"];
            }
            else {
                materialName = obj["materialName"];
                materialDescription = obj["materialDescription"];
                materialFileName = obj["Url"];
            }
            

            string path = "/Content/agent-trainee-course-materials/";
            FileInput singleFileInput = new FileInput();
            
            //Check if paths exist, if not exist > create file path
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(path))) {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
            }               
            int fileCount = Directory.GetFiles(HttpContext.Current.Server.MapPath(path)).Count();
            string str_document = "agent_trainee_material_" + fileCount.ToString() + "." + materialFileName.Split('.')[1];
            //full file path
            string filePath = path + str_document;

            //string Base64 = x.Base64String.Split(',')[1];
            //byte[] bytes = Convert.FromBase64String(Base64);
            //File.WriteAllBytes(HttpContext.Current.Server.MapPath(path) + x.FileNameNew, bytes);

            Response response = new Response();
            if (materialName == "" || materialDescription == "")
            {
                response.IsSuccess = false;
                response.Message = "Please fill in all fields";
                return response;
            }
            if (materialFileName == "")
            {
                response.IsSuccess = false;
                response.Message = "Please provide a document";
                return response;
            }

            try
            {
                if (id == 0)
                {
                    AgentTraineeMaterial singleATM = new AgentTraineeMaterial
                    {
                        id = id,
                        material_name = materialName,
                        material_description = materialDescription,
                        material_url_path = filePath,
                        created_date = DateTime.Now,
                        created_by = loginUser.Id,
                        updated_by = loginUser.Id,
                        updated_date = DateTime.Now,
                        is_active = 0,
                        status = 1
                    };
                    Response responsePost = GenericService.PostData<AgentTraineeMaterial>(singleATM);
                    if (responsePost.IsSuccess)
                    {
                        response.IsSuccess = true;
                        response.Message = "Successfully Added New Material";
                    }

                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Agent Trainee Materials" + singleATM.id + " has successfully inserted",
                        TableName = "agent_trainee_materials",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }
                    //Audit Log Ends here
                }
                else
                {
                    //Get selected row data via the id
                    String queryGetMaterial = @"SELECT * FROM agent_trainee_materials where id in (" + id +")";
                    Response responseGetMaterial = GenericService.GetDataByQuery(queryGetMaterial, 0, 0, false, null, false, null, true);
                    if (responseGetMaterial.IsSuccess) {
                        var materialDyn = responseGetMaterial.Data;
                        var responseJSON = JsonConvert.SerializeObject(materialDyn);
                        List<AgentTraineeMaterial> materialList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTraineeMaterial>>(responseJSON);

                        materialList.ForEach(x =>
                        {
                            AgentTraineeMaterial singleATM = new AgentTraineeMaterial
                            {
                                id = id,
                                material_name = materialName,
                                material_description = materialDescription,
                                material_url_path = filePath,
                                created_date = x.created_date,
                                created_by = loginUser.Id,
                                updated_by = loginUser.Id,
                                updated_date = DateTime.Now,
                                is_active = x.is_active,
                                status = x.status
                            };
                            Response responsePost = GenericService.UpdateData<AgentTraineeMaterial>(singleATM);
                            if (responsePost.IsSuccess)
                            {
                                response.IsSuccess = true;
                                response.Message = "Successfully Updated Material";
                            }

                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Agent Trainee Materials" + singleATM.id + " has successfully updated",
                                TableName = "agent_trainee_materials",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            //Audit Log Ends here
                        });
                    }
                }
                return response;
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add language dir action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            try
            {
                string idString = String.Join(",", ids);
                string query = @"Select * from agent_trainee_materials atm where atm.id in (" + idString + ")";
                Response responseGetMaterial = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                if (responseGetMaterial.IsSuccess)
                {
                    int tempStatus;
                    var materialDyn = responseGetMaterial.Data;
                    var responseJSON = JsonConvert.SerializeObject(materialDyn);
                    List<AgentTraineeMaterial> agentTraineeMaterialListing = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgentTraineeMaterial>>(responseJSON);
                    Response responsePost = new Response();
                    if (action == "Deactivate")
                    {
                        string queryDFilter = @"Select * from commission_structures where commission_def_id in (" + idString + ")";
                        Response responseCSList = GenericService.GetDataByQuery(queryDFilter, 0, 0, false, null, false, null, true);
                        if (responseCSList.IsSuccess)
                        {
                            agentTraineeMaterialListing.ForEach(x =>
                            {
                                tempStatus = x.is_active;
                                x.is_active = 0;
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = x.material_name + " - Agent Trainee Material deactivated",
                                    TableName = "`agent_trainee_materials`",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "is_active",
                                    ValueOld = tempStatus.ToString(),
                                    ValueNew = "0",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                                //update com def
                                responsePost = GenericService.UpdateData<AgentTraineeMaterial>(x);
                            });


                        }

                    }
                    if (action == "Activate")
                    {
                        agentTraineeMaterialListing.ForEach(x =>
                        {
                            tempStatus = x.is_active;
                            x.is_active = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.material_name + " - Agent Trainee Material activated",
                                TableName = "`commission_defs`",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "is_active",
                                ValueOld = tempStatus.ToString(),
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                            responsePost = GenericService.UpdateData<AgentTraineeMaterial>(x);
                        });
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Agent Trainee Material Listing action: " + ex.Message);
                return false;
            }
        }

        public class FileInput
        {
            public Int32 Type { get; set; }
            public String Base64String { get; set; }
            public String FileName { get; set; }
            public String FileNameNew { get; set; }
        }
    }
}