﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_files")]
    public class AgentRegFile
    {
        [Database("ID"), Display("Id")]
        public Int32 id { get; set; }
        [Database("agent_reg_id"), Display("AgentRegId")]
        public Int32 agent_reg_id { get; set; }
        [Database("file_type"), Display("FileType")]
        public Int32 file_type { get; set; }
        [Database("name"), Display("Name")]
        public String name { get; set; }
        [Database("url"), Display("Url")]
        public String url { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("updated_date"), Display("updated_date")]
        public DateTime updated_date { get; set; }
        [Database("status"), Display("Status")]
        public Int32 status { get; set; }
    }
}
