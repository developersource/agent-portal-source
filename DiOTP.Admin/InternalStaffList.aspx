﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalStaffList.aspx.cs" Inherits="Admin.InternalStaffList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="internalStaffForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-2">
                                    <select id="ddlDepartmentType" class="form-control" runat="server">
                                        <option value="0">All</option>
                                        <option value="1">Operation</option>
                                        <option value="2">Finance</option>
                                        <option value="3">Sales Channel</option>
                                        <option value="4">Compliance</option>
                                    </select>
                                    <%--<asp:DropDownList ID="ddlDepartmentType" runat="server" clientidmode="Static">
                                        <asp:ListItem Value ="0" Text="Select"></asp:ListItem>
                                        <asp:ListItem Value ="1" Text="Operation"></asp:ListItem>
                                        <asp:ListItem Value ="2" Text="Finance"></asp:ListItem>
                                        <asp:ListItem Value ="3" Text="Sales Channel"></asp:ListItem>
                                        <asp:ListItem Value ="4" Text="Compliance"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                </div>
                                <div class="col-md-2">
                                    <select id="ddlUserType" class="form-control" runat="server">
                                        <option value="0">All</option>
                                        <option value="14">Trade Officer</option>
                                        <option value="15">Trade Manager</option>
                                        <option value="16">Trade Admin</option>
                                    </select>
<%--                                    <asp:DropDownList ID="ddlUserType" runat="server" clientidmode="Static">
                                        <asp:ListItem Value ="0" Text="Select"></asp:ListItem>
                                        <asp:ListItem Value ="14" Text="Trade Officer"></asp:ListItem>
                                        <asp:ListItem Value ="15" Text="Trade Manager"></asp:ListItem>
                                        <asp:ListItem Value ="16" Text="Trade Admin"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                </div>
                                <div class="col-md-2">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully deactivated" data-status="Inactive" data-original-title="Deactivate" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Deactivate" data-isconfirm="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Successfully activated" data-status="Active" data-original-title="Activate" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Activate" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                <span class="common-divider pull-right"></span>
                                                <button data-message="Successfully sent" data-status="Link" data-original-title="Resend link" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="ResendLink" data-isconfirm="0" data-content="Resend Activation Link"><i class="fa fa-refresh"></i></button>
                                                <button data-original-title="Add" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Add" data-url="AddStaff.aspx" data-title="Add Staff"><i class="fa fa-plus"></i></button>
                                                <button data-original-title="Edit" data-trigger="hover" data-placement="bottom" data-content="Select one record" type="button" class="popovers btn action-button pull-right" data-action="Edit" data-url="AddStaff.aspx" data-title="Edit Staff"><i class="fa fa-edit"></i></button>
                                                <%--<button id="download" data-message="Success" data-status="Active" data-original-title="Download as excel" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Download" data-isconfirm="0"><i class="fa fa-download"></i></button>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-top:5px;">
                                    <small class="pull-right">
                                        <strong>Note:</strong>
                                        <span>You can search here with Username, Email, and Mobile number</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can Edit, Add, Activate, and Deactivate Internal Staff Accounts</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                        <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="0">All</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="13">Operational</button>
                                        <small>
                                            <br />
                                            <strong>Note:</strong>
                                            <span>User Type Filtering on Internal Staff Accounts</span>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable userAccounts" cellspacing="0" width="100%">
                                    <thead class="genericTable">
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th class="br-0" data-column-name="username">Internal Staff details</th>
                                            <th class="bl-0"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="internalStaffTbody" runat="server"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderByColumn" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderType" runat="server" ClientIDMode="Static" Value="0" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
