﻿using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddCommissionStructure : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] != null)
            {
                User sessionUser = (User)Session["admin"];
                string idString = Request.QueryString["id"];

                //string queryCDRegEdit = (@"SELECT * FROM commission_defs");
                //string queryCDRegAdd = (@" SELECT * FROM commission_defs where is_active=1 ");
                //string queryPopulate;
                //if (idString != "0" && idString != null && idString != "")
                //{
                //    queryPopulate = queryCDRegEdit;


                //}
                //else {
                //    queryPopulate = queryCDRegAdd;
                //}
                string queryPopulate = (@"SELECT * FROM commission_defs");
                Response responseCDList = GenericService.GetDataByQuery(queryPopulate, 0, 0, false, null, false, null, false);
                if (responseCDList.IsSuccess)
                {
                    var cdsDyn = responseCDList.Data;
                    var responseCDJSON = JsonConvert.SerializeObject(cdsDyn);
                    List<DiOTP.Utility.CommissionDefinitions> cds = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionDefinitions>>(responseCDJSON);
                    cds.ForEach(x =>
                    {
                        x.code = x.code + " - " + x.name;
                    });
                    CommissionDefId.DataSource = cds;
                    CommissionDefId.DataTextField = "code";
                    CommissionDefId.DataValueField = "id";
                    CommissionDefId.DataBind();
                    CommissionDefId.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                    }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseCDList.Message + "\");", true);
                }
                
                if (idString != null && idString != "")
                {
                    if (idString != "0")
                        CommissionDefId.Disabled = true;

                    int idEdit = Convert.ToInt32(idString);
                    string queryAReg = (@" SELECT * FROM commission_structures where id='" + idEdit + "' ");
                    Response responseIList = GenericService.GetDataByQuery(queryAReg, 0, 0, false, null, false, null, true);
                    if (responseIList.IsSuccess)
                    {
                        var cssDyn = responseIList.Data;
                        var responseJSON = JsonConvert.SerializeObject(cssDyn);
                        List<DiOTP.Utility.CommissionStructure> css = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionStructure>>(responseJSON);
                        if (css.Count == 1)
                        {
                            DiOTP.Utility.CommissionStructure cs = css.FirstOrDefault();
                            Id.Value = cs.id.ToString();
                            CommissionDefId.Value = cs.commission_def_id.ToString();
                            ddlApplyTo.SelectedValue= cs.apply_to;
                            Percent.Value = cs.percent.ToString();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"Commission Structure not found\");", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseIList.Message + "\");", true);
                    }

                }
            }
        }
    }
}