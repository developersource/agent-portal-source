﻿using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Admin
{
    /// <summary>
    /// Summary description for fileUploaderDownloads
    /// </summary>
    public class fileUploaderDownloads : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Server.ScriptTimeout = 3600;
            context.Response.ContentType = "text/plain";
            try
            {
                string localPath = "/Downloads/";
                string dirFullPath = HttpContext.Current.Server.MapPath(localPath);
                Logger.WriteLog(dirFullPath);
                if (!Directory.Exists(dirFullPath))
                {
                    Directory.CreateDirectory(dirFullPath);
                }
                string[] files;
                int numFiles;
                files = System.IO.Directory.GetFiles(dirFullPath);
                numFiles = files.Length;
                Logger.WriteLog(numFiles + "");
                numFiles = numFiles + 1;
                string str_image = "";
                string pathToSave_100 = "";
                Logger.WriteLog("context.Request.Files.Count: " + context.Request.Files.Count + "");
                foreach (string s in context.Request.Files)
                {
                    HttpPostedFile file = context.Request.Files[s];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;
                    Logger.WriteLog(fileName);

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileExtension = Path.GetExtension(fileName);
                        string name = fileName.Replace(fileExtension, "");
                        str_image = name + "_" + numFiles.ToString() + fileExtension;
                        pathToSave_100 = HttpContext.Current.Server.MapPath("/Downloads/") + str_image;
                        localPath += "" + str_image;
                        Logger.WriteLog(localPath);
                        file.SaveAs(pathToSave_100);
                    }
                }
                //  database record update logic here  ()

                context.Response.Write(localPath);
            }
            catch (Exception ac)
            {
                Logger.WriteLog("Exception: " + ac.Message);
                Logger.WriteLog("StackTrace: " + ac.StackTrace);
                context.Response.Write(ac.Message);

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}