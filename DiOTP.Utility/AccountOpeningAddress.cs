﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ao_addresses")]
    public class AccountOpeningAddress
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("address_type"), Display("AddressType")]
        public Int32 AddressType { get; set; }
        [Database("account_opening_id"), Display("AccountOpeningId")]
        public Int32 AccountOpeningId { get; set; }
        [Database("addr_1"), Display("Addr1")]
        public String Addr1 { get; set; }
        [Database("addr_2"), Display("Addr2")]
        public String Addr2 { get; set; }
        [Database("addr_3"), Display("Addr3")]
        public String Addr3 { get; set; }
        [Database("city"), Display("City")]
        public String City { get; set; }
        [Database("post_code"), Display("PostCode")]
        public String PostCode { get; set; }
        [Database("state"), Display("State")]
        public String State { get; set; }
        [Database("country"), Display("Country")]
        public String Country { get; set; }
        [Database("tel_no"), Display("TelNo")]
        public String TelNo { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }

}
