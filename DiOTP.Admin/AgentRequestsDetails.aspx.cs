﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Admin.ServiceCalls.ServicesManager;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace Admin
{
    public partial class AgentRequestsDetails : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        public static IOccupationCodesDefService IOccupationCodesDefService { get { return lazyOccupationCodesDefServiceObj.Value; } }

        private static readonly Lazy<IRaceDefService> lazyIRaceDefServiceObj = new Lazy<IRaceDefService>(() => new RaceDefService());

        public static IRaceDefService IRaceDefService { get { return lazyIRaceDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());

        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String idString = Request.QueryString["id"];

                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);

                    string mainQ = (@"select a.*, u.username, u.email_id, u.mobile_number, u.id_no, arp.*, arpi.*, aex.*, ao.name as office_name, r.name as region_name, arpi.id as agent_reg_payment_id
                                from agent_regs a 
                                join users u on u.id = a.user_id 
                                join agent_offices ao on a.office_id = ao.id 
								join regions r on ao.region_id 
								join agent_reg_personal arp on a.ID = arp.agent_reg_id 
								left join agent_reg_payment_insurance arpi on a.ID = arpi.agent_reg_id 
								left join agent_reg_exps aex on a.ID = aex.agent_reg_id 
                                where a.id='" + id + "'");
                    Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 20, false, null, false, null, true);
                    if (responseUList.IsSuccess)
                    {
                        var UsersDyn = responseUList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                        List<DiOTP.Utility.AgentRegInfo> agentRequestList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegInfo>>(responseJSON);
                        DiOTP.Utility.AgentRegInfo agentRequest = agentRequestList.FirstOrDefault();

                        StringBuilder asb = new StringBuilder();
                        asb.Append(@"<tr>
                                <td>" + agentRequest.name + @"</td>
                                <td>" + agentRequest.id_no + @"</td>
                                <td>" + agentRequest.job_type_id + @"</td>
                                <td>" + agentRequest.marital_status + @"</td>
                                <td>" + agentRequest.office_name + @"</td>
                                <td>" + agentRequest.region_name + @"</td>
                            </tr>");
                        agencyTbody.InnerHtml = asb.ToString();
                        asb.Clear();

                        asb.Append(@"<tr>
                                <td>" + agentRequest.dob + @"</td>
                                <td>" + agentRequest.mother_maiden_name + @"</td>
                                <td>" + (agentRequest.sex == "F" ? "Female" : agentRequest.sex == "M" ? "Male" : "-") + @"</td>
                                <td>" + (agentRequest.race == "M" ? "Malay" : agentRequest.race == "C" ? "Chinese" : agentRequest.race == "I" ? "Indian" : "Others") + @"</td>
                                <td>" + agentRequest.religion + @"</td>
                                <td>" + agentRequest.highest_education + @"</td>
                                <td>" + agentRequest.tel_no_office + @"</td>                                
                                <td>" + agentRequest.tel_no_home + @"</td>
                                <td>" + agentRequest.hand_phone + @"</td>
                                <td>" + agentRequest.email + @"</td>
                                <td>" + agentRequest.contact_person + @"</td>
                                <td>" + agentRequest.income_tax_no + @"</td>
                                <td>" + agentRequest.epf_no + @"</td>
                                <td>" + agentRequest.socso + @"</td>
                                <td>" + agentRequest.language + @"</td>
                                <td>" + agentRequest.nationality + @"</td>
                                <td>" + agentRequest.marital_status + @"</td>
                                <td>" + agentRequest.bankruptcy_declaration + @"</td>
                                <td>" + agentRequest.spouse_name + @"</td>
                                <td>" + agentRequest.spouse_id_no + @"</td>

                            </tr>");
                        personalTbody.InnerHtml = asb.ToString();
                        asb.Clear();

                        asb.Append(@"<tr>
                            <td>" + agentRequest.mail_addr1 + @",<br/>
                            " + agentRequest.mail_addr2 + @",<br/>
                            " + agentRequest.mail_addr3 + @",<br/>
                            " + agentRequest.mail_addr4 + @"</td>
                            <td>" + agentRequest.post_code + @"</td>
                            <td>" + agentRequest.state + @"</td>
                            <td>" + agentRequest.country + @"</td>
                        </tr>");
                        addressTbody.InnerHtml = asb.ToString();
                        asb.Clear();

                        string queryAgentRegPaymentBanks = (@" select * from agent_reg_banks where agent_reg_payment_id=" + agentRequest.agent_reg_payment_id + " and status=1 ");
                        Response responseAgentRegPaymentBanksList = GenericService.GetDataByQuery(queryAgentRegPaymentBanks, 0, 0, false, null, false, null, true);
                        if (responseAgentRegPaymentBanksList.IsSuccess)
                        {
                            var agentregPaymentBanksDyn = responseAgentRegPaymentBanksList.Data;
                            var responseAgentRegPaymentBanksJSON = JsonConvert.SerializeObject(agentregPaymentBanksDyn);
                            List<AgentRegBank> agentRegPaymentBanks = JsonConvert.DeserializeObject<List<DiOTP.Utility.AgentRegBank>>(responseAgentRegPaymentBanksJSON);
                            agentRegPaymentBanks.ForEach(bank =>
                            {
                                asb.Append(@"<tr>
                                    
                                    <td>" + bank.currency + @"</td>
                                    <td>" + bank.bank_name + @"</td>
                                    <td>" + bank.bank_type + @"</td>
                                    <td>" + bank.bank_account_type + @"</td>
                                    <td>" + bank.account_no + @"</td>
                                </tr>");
                            });
                            bankTbody.InnerHtml = asb.ToString();
                            asb.Clear();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAgentRegPaymentBanksList.Message + "\", '');", true);
                        }

                        asb.Clear();
                        asb.Append(@"<tr>
                                <td>" + agentRequest.employer_name + @"</td>
                                <td>" + agentRequest.position_held + @"<br/>
                                <td>" + agentRequest.length_of_service_yrs + @"</td>
                                <td>" + agentRequest.annual_income + @"</td>
                                <td>" + agentRequest.reason_for_leaving + @"</td>
                            </tr>");
                        workTbody.InnerHtml = asb.ToString();
                        asb.Clear();


                        string query = " select * from agent_reg_files where agent_reg_id = '" + agentRequest.id + "' and status = 1 ";
                        Response responseARFist = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, true);
                        if (responseARFist.IsSuccess)
                        {
                            var agentFilesDyn = responseARFist.Data;
                            var responseAgentFilesJSON = JsonConvert.SerializeObject(agentFilesDyn);
                            List<AgentRegFile> agentRegFiles = JsonConvert.DeserializeObject<List<AgentRegFile>>(responseAgentFilesJSON);

                            agentRegFiles.ForEach(file =>
                            {
                                asb.Append(@"<tr>
                                                <td>" + (file.file_type == 1 ? "NRIC Front" : (file.file_type == 2 ? "NRIC Back" : (file.file_type == 3 ? "Selfie" : (file.file_type == 4 ? "Proof of Payment" : (file.file_type == 5 ? "Qualification" : (file.file_type == 6 ? "FiMM Card" : (file.file_type == 7 ? "Result" : ""))))))) + @"<br/>
                                                <td>" + file.created_date.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                                <td><a target='_blank' href='" + file.url + @"'>View <i class='fa fa-eye'></i></a></td>
                                            </tr>");
                            });
                            documentsTbody.InnerHtml = asb.ToString();
                            asb.Clear();
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseARFist.Message + "\");", true);

                        //divRecommendation.InnerHtml = agentRequest.recommendation;
                        string queryRecommendedByAgent = @"SELECT ar.id, ar.agent_code, ar.recommendation,ar.recommendation_date, arp.name from agent_regs ar left join agent_reg_personal arp on ar.id = arp.agent_reg_id where ar.agent_code = '" +agentRequest.introducer_code +"'";
                        Response responseRecommendedByAgentList = GenericService.GetDataByQuery(queryRecommendedByAgent, 0, 0, false, null, false, null, true);
                        if (responseRecommendedByAgentList.IsSuccess) {
                            var recommendedByAgentDyn = responseRecommendedByAgentList.Data;
                            var responseRecommendedByAgentJSON = JsonConvert.SerializeObject(recommendedByAgentDyn);
                            List<dynamic> rbaDynamicList = JsonConvert.DeserializeObject<List<dynamic>>(responseRecommendedByAgentJSON);
                            dynamic rbaSingle = rbaDynamicList.First();

                            asb.Append(@"<tr>
                                <td>" + rbaSingle.name + @"</td>
                                <td>" + agentRequest.recommendation_date + @"</td>
                                <td>" + agentRequest.recommendation + @"</td>
                            </tr>");

                            recommendationTbody.InnerHtml = asb.ToString();
                            asb.Clear();
                        }
                        //if (agentRequest.process_status == 2)
                        //{
                        //    examsSection.Visible = false;
                        //}
                        //if (agentRequest.process_status == 3)
                        //{
                        //    fimmSection.Visible = false;
                        //}

                        string queryExam = " select * from agent_reg_exams where agent_reg_id = '" + agentRequest.id + "' and status = 1 ";
                        Response responseExamList = GenericService.GetDataByQuery(queryExam, 0, 0, false, null, false, null, true);
                        if (responseExamList.IsSuccess)
                        {
                            var examsDyn = responseExamList.Data;
                            var responseExamsJSON = JsonConvert.SerializeObject(examsDyn);
                            List<AgentRegExam> agentRegExams = JsonConvert.DeserializeObject<List<AgentRegExam>>(responseExamsJSON);

                            agentRegExams.ForEach(exam =>
                            {
                                asb.Append(@"<tr>
                                                <td>" + exam.location + @"<br/>
                                                <td>" + exam.language + @"</td>
                                                <td>" + (exam.exam_date.HasValue ? exam.exam_date.Value.ToString("dd/MM/yyyy") : "") + @"</td>
                                                <td>" + (exam.exam_date.HasValue ? exam.exam_date.Value.ToString("HH:mm tt") : "") + @"</td>
                                                <td>" + (exam.is_enroll == 1 ? "Enrolled" : "-") + @"</td>
                                                <td>" + (exam.result == "" ? "Pending" : exam.result) + @"</td>
                                            </tr>");
                            });
                            examsTbody.InnerHtml = asb.ToString();
                            asb.Clear();
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseExamList.Message + "\");", true);

                        string queryFiMM = " select * from agent_reg_fimms where agent_reg_id = '" + agentRequest.id + "' and status = 1 ";
                        Response responseFiMMList = GenericService.GetDataByQuery(queryFiMM, 0, 0, false, null, false, null, true);
                        if (responseFiMMList.IsSuccess)
                        {
                            var fimmsDyn = responseFiMMList.Data;
                            var responseFiMMsJSON = JsonConvert.SerializeObject(fimmsDyn);
                            List<AgentRegFimm> agentRegFimms = JsonConvert.DeserializeObject<List<AgentRegFimm>>(responseFiMMsJSON);

                            agentRegFimms.ForEach(fimm =>
                            {
                                asb.Append(@"<tr>
                                                <td class='bg-success'>" + fimm.FIMM_no + @"<br/>
                                                <td class='bg-success'>" + (fimm.approval_date.HasValue ? fimm.approval_date.Value.ToString("dd/MM/yyyy") : "") + @"</td>
                                                <td class='bg-success'>" + (fimm.expiry_date.HasValue ? fimm.expiry_date.Value.ToString("dd/MM/yyyy") : "") + @"</td>
                                                <td class='bg-success'>" + fimm.remarks + @"</td>
                                            </tr>");
                            });

                            if (agentRegFimms.Count == 0) {
                                asb.Append(@"<tr><td valign='top' colspan='4' class='dataTables_empty bg-danger'>No data available in table</td>
                                            <td style='display:none'></td>
                                               <td style='display:none'></td>
                                               <td style='display:none'></td></tr>");
                            }
                            fimmTbody.InnerHtml = asb.ToString();
                            asb.Clear();
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseFiMMList.Message + "\");", true);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert(\"" + responseUList.Message + "\");", true);
                    }

                }
            }

        }
    }
}