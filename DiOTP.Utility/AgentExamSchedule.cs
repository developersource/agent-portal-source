﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_exam_schedules")]
    public class AgentExamSchedule
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("session_id"), Display("session_id")]
        public String session_id { get; set; }
        [Database("language"), Display("language")]
        public String language { get; set; }
        [Database("location"), Display("location")]
        public String location { get; set; }
        [Database("exam_datetime"), Display("exam_datetime")]
        public DateTime exam_datetime { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32 created_by { get; set; }
        [Database("updated_date"), Display("updated_date")]
        public DateTime? updated_date { get; set; }
        [Database("updated_by"), Display("updated_by")]
        public Int32? updated_by { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }

    public class CustomAgentExamSchedule : AgentExamSchedule
    {
        public String exam_datetimes { get; set; }
    }

}
