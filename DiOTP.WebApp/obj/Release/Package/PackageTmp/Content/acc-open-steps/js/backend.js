﻿const truliooMiddleware = require('trulioo-embedid-middleware')({
    apiKey: 'b0a149684916b389b9c66c485d1f0e29'
});
const express = require('express');
const app = express();
const port = 8080;

app.use(truliooMiddleware)
app.listen(port, () => console.log('Example app listening on port ${port}!'));