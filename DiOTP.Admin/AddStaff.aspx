﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddStaff.aspx.cs" Inherits="Admin.AddStaff" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   <div id="addStaffForm" runat="server">
        <div class="row" id="FormId" data-value="addStaffForm">
            <div class="col-md-12">

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Internal Staff Type:</label>
                    </div>
                    <div class="col-md-8">
                        <select id="LoginAttempts" name="LoginAttempts" runat="server" clientidmode="static" class="form-control">
                            <option value="0">Select</option>
                            <option value="14">Trade Admin</option>
                            <option value="15">Trade Officer</option>
                            <option value="16">Trade Manager</option>
                            <option value="13">Operational Staff</option>
                        </select>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Department Type:</label>
                    </div>
                    <div class="col-md-8">
                        <select id="DepartmentType" name="DepartmentType" runat="server" clientidmode="static" class="form-control">
                            <option value="0">Select</option>
                            <option value="1">Operation</option>
                            <option value="2">Finance</option>
                            <option value="3">Sales Channel</option>
                            <option value="4">Compliance</option>
                        </select>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">User Matrix:</label>
                    </div>
                    <div class="col-md-8 checkbox-group-user-matrix" id="UserMatrix">
                        <div class="col-sm-3">
                            <input id="Maker" type="checkbox" runat="server" clientidmode="static" name="Maker" style="margin-right: 5px;" value="1"/><label for="Maker">Maker</label>
                        </div>
                        <div class="col-sm-3">
                            <input id="Checker" type="checkbox" runat="server" clientidmode="static" name="Checker" style="margin-right: 5px;" value="2"/><label for="Checker">Checker</label>
                        </div>
                        <div class="col-sm-3">
                            <input id="ViewReport" type="checkbox" runat="server" clientidmode="static" name="ViewReport" style="margin-right: 5px;" value="3"/><label for="ViewReport">View Report</label>
                        </div>
                        <div class="col-sm-3">
                            <input id="GenerateReport" type="checkbox" runat="server" clientidmode="static" name="GenerateReport" style="margin-right: 5px;" value="4"/><label for="GenerateReport">Generate Report</label>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Username:</label>
                    </div>
                    <div class="col-md-8">
                        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                        <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                        <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                        <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                        <input type="hidden" name="ModifiedBy" value="0" id="ModifiedBy" runat="server" clientidmode="static" />
                        <input type="hidden" name="ModifiedDate" value="0" id="ModifiedDate" runat="server" clientidmode="static" />
                        <input type="hidden" name="UserRoleId" value="2" id="UserRoleId" runat="server" clientidmode="static" />

                        <input type="hidden" name="Password" id="Password" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="TransPwd" id="TransPwd" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="UniqueKey" id="UniqueKey" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="IsOnline" value="0" id="IsOnline" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsActive" value="0" id="IsActive" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsPrimary" value="0" id="IsPrimary" runat="server" clientidmode="static" />
                        <input type="hidden" name="RegisterIp" value="0" id="RegisterIp" runat="server" clientidmode="static" />
                        <input type="hidden" name="LastLoginDate" value="" id="LastLoginOn" runat="server" clientidmode="static" />
                        <input type="hidden" name="LastLoginIp" value="0" id="LastLoginIp" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsSatChecked" value="0" id="IsSatChecked" runat="server" clientidmode="static" />
                        <input type="hidden" name="VerificationCode" value="0" id="VerificationCode" runat="server" clientidmode="static" />

                        <input type="text" name="Username" id="Username" runat="server" clientidmode="static" class="form-control" placeholder="Enter Username" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Email:</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="EmailId" id="EmailId" runat="server" clientidmode="static" class="form-control" placeholder="Enter Email" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Mobile number:</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="MobileNumber" id="MobileNumber" runat="server" clientidmode="static" class="form-control" placeholder="Enter Mobile number" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>
