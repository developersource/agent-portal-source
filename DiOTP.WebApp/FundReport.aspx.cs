﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class FundReport : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcMemberInvestmentService> lazyUtmcMemberInvestmentObj = new Lazy<IUtmcMemberInvestmentService>(() => new UtmcMemberInvestmentService());

        public static IUtmcMemberInvestmentService IUtmcMemberInvestmentService { get { return lazyUtmcMemberInvestmentObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }
        public static List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Login.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Index.aspx')", true);
                else
                {
                    if (!IsPostBack)
                    {
                        String isVerified = "0";
                        if (Session["isVerified"] == null)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please verify yourself', 'Portfolio.aspx');", true);
                        }
                        else
                        {
                            isVerified = (Session["isVerified"].ToString());
                            if (isVerified == "0")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please verify yourself', 'Portfolio.aspx');", true);
                            }
                            else if (isVerified == "1")
                            {
                                User user = (User)Session["user"];
                                Response response = IUserService.GetSingle(user.Id);
                                if (response.IsSuccess)
                                {
                                    user = (User)response.Data;
                                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                                    if (responseUAList.IsSuccess)
                                    {
                                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                        userAccounts = userAccounts.Where(x => x.IsVerified == 1 && x.Status == 1).ToList();

                                        string all = String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToArray());
                                        ddlFundAccount.Items.Add(new ListItem("All Accounts", all));
                                        foreach (UserAccount userAccount in userAccounts)
                                        {
                                            ddlFundAccount.Items.Add(new ListItem(CustomValues.GetAccounPlan(userAccount.HolderClass) + " - " + userAccount.AccountNo, userAccount.Id.ToString()));
                                        }

                                        List<UtmcFundInformation> UTMCFundInformations1 = new List<UtmcFundInformation>();
                                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status = 'Active' ", 0, 0, false);
                                        if (responseUFIList.IsSuccess)
                                        {
                                            UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;


                                            userAccounts.ForEach(uA =>
                                        {
                                            List<HolderInv> holderInvs = new List<HolderInv>();

                                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                                            MaHolderReg maHolderReg = new MaHolderReg();
                                            if (responseMHR.IsSuccess)
                                            {
                                                maHolderReg = (MaHolderReg)responseMHR.Data;

                                                holderInvs = ServicesManager.GetHolderInvByHolderNo(uA.AccountNo, UTMCFundInformations);
                                                holderInvs.ForEach(x =>
                                                {
                                                    {
                                                        {
                                                            UtmcFundInformation Fund = UTMCFundInformations.FirstOrDefault(y => y.IpdFundCode == x.FundId);
                                                            Fund.SatGroup = uA.AccountNo;
                                                            Int32 fundId = Fund.Id;
                                                            if (x.CurrUnitHldg > 0)
                                                            {
                                                                if (UTMCFundInformations1.Count > 0)
                                                                {
                                                                    if (UTMCFundInformations1.Where(y => y.Id == fundId).Count() == 0)
                                                                        UTMCFundInformations1.Add(Fund);
                                                                    else
                                                                        UTMCFundInformations1.Add(Fund);
                                                                }
                                                                else
                                                                    UTMCFundInformations1.Add(Fund);

                                                            }
                                                        }
                                                    }

                                                });
                                            }
                                        });

                                            //UTMCFundInformations
                                            StringBuilder sb = new StringBuilder();
                                            Int32 idx = 1;
                                            var grpUTMCFundInformations = UTMCFundInformations1.GroupBy(x => new { x.FundCode }).ToList().Select(grp => grp.ToList()).ToList();
                                            grpUTMCFundInformations.ForEach(x =>
                                            {
                                                UtmcFundFile AnnualReport = x.FirstOrDefault().UtmcFundInformationIdUtmcFundFiles.Where(y => y.UtmcFundFileTypesDefId == 2 && y.Status == 1).FirstOrDefault();

                                                List<HolderInv> holderInvs = new List<HolderInv>();

                                                {

                                                    {
                                                        sb.Append((AnnualReport != null ? @"<tr>
                                                            <td>" + idx + @"</td>
                                                            <!--<td>" + String.Join(",", x.Select(y => y.SatGroup).ToArray()) + @"</td>-->
                                                            <td>" + x.FirstOrDefault().FundName.Capitalize() + @"</td>
                                                            <td><a href='" + AnnualReport.Url + @"' target='_blank'>View</a></td>
                                                            </tr>" : "")
                                                            );
                                                        idx++;
                                                    }

                                                }
                                                ;
                                            });

                                            fundReportTbody.InnerHtml = sb.ToString();
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please verify yourself', 'Portfolio.aspx');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            GenerateFundReport();
        }

        public void GenerateFundReport()
        {
            try
            {
                if (Session["user"] == null)
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Login.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed.', 'Index.aspx')", true);
                else
                {
                    string selectedAccount = ddlFundAccount.SelectedItem.Text;
                    List<HolderInv> holderInvs = new List<HolderInv>();
                    List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
                    List<UtmcFundInformation> UTMCFundInformations1 = new List<UtmcFundInformation>();

                    Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                    if (responseUFIList.IsSuccess)
                    {
                        UTMCFundInformations1 = (List<UtmcFundInformation>)responseUFIList.Data;
                    }

                    if (selectedAccount == "All")
                    {
                        selectedAccount = ddlFundAccount.SelectedValue;
                    }

                    holderInvs = ServicesManager.GetHolderInvByHolderNo(selectedAccount, UTMCFundInformations);
                    if (holderInvs.Count > 0)
                    {
                        holderInvs.ForEach(x =>
                        {
                            UtmcFundInformation Fund = UTMCFundInformations1.Where(y => y.IpdFundCode == x.FundId).FirstOrDefault();
                            Fund.SatGroup = x.HolderNo.ToString();
                            Int32 fundId = Fund.Id;
                            if (x.CurrUnitHldg > 0)
                            {
                                if (UTMCFundInformations.Count > 0)
                                {
                                    if (UTMCFundInformations.Where(y => y.Id == fundId).Count() == 0)
                                        UTMCFundInformations.Add(Fund);
                                    else
                                        UTMCFundInformations.Add(Fund);
                                }
                                else
                                    UTMCFundInformations.Add(Fund);

                            }
                        });

                        StringBuilder sb = new StringBuilder();
                        Int32 idx = 1;
                        UTMCFundInformations = UTMCFundInformations.GroupBy(x => new { x.FundCode, x.SatGroup }).ToList().Select(grp => grp.FirstOrDefault()).ToList();
                        UTMCFundInformations.ForEach(x =>
                        {
                            UtmcFundFile AnnualReport = x.UtmcFundInformationIdUtmcFundFiles.Where(y => y.UtmcFundFileTypesDefId == 2).FirstOrDefault();

                            sb.Append(@"<tr>
                                            <td>" + idx + @"</td>
                                            <td>" + x.SatGroup + @"</td>
                                            <td>" + x.FundName.Capitalize() + @"</td>
                                            <td><a href='" + AnnualReport.Url + @"' target='_blank'>" + AnnualReport.Name + @"</a></td>
                                        </tr>");
                            idx++;

                        });
                        fundReportTbody.InnerHtml = sb.ToString();
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append(@"<tr>
                                    <td colspan='9' align='center'>No transaction record found.</td>
                                </tr>");
                        fundReportTbody.InnerHtml = sb.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("FundReport Page GenerateFundReport: " + ex.Message);
            }
        }
    }
}