﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class CorporateAccountOpeningDocs : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderStatementService> lazyUserStatementServiceObj = new Lazy<IUserOrderStatementService>(() => new UserOrderStatementService());

        public static IUserOrderStatementService IUserOrderStatementService { get { return lazyUserStatementServiceObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        private static readonly Lazy<IUserNotificationSettingService> lazyUserNotificationSettingServiceObj = new Lazy<IUserNotificationSettingService>(() => new UserNotificationSettingService());

        public static IUserNotificationSettingService IUserNotificationSettingService { get { return lazyUserNotificationSettingServiceObj.Value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            string dirPath = HttpContext.Current.Server.MapPath("/Downloads/Corporate Documents/");

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (!Directory.Exists(dirPath))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert('No directory');", true);
                }
                else
                {
                    var dirInfo = new DirectoryInfo(dirPath);

                    StringBuilder filter = new StringBuilder();

                    if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                    {
                        IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                    }
                    if (IsNewSearch.Value == "1")
                    {
                        hdnCurrentPageNo.Value = "";
                    }
                    if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                    {
                        Search.Value = Request.QueryString["Search"].ToString();
                        string matchingWords1 = "income distribution id";
                        string matchingWords2 = "unit split us";
                        if (matchingWords1.Contains(Search.Value.Trim().ToLower()))
                        {
                            filter.Append("*_ID_*");
                        }
                        else if (matchingWords2.Contains(Search.Value.Trim().ToLower()))
                        {
                            filter.Append("*_US_*");
                        }
                        else
                            filter.Append("*" + Search.Value.Trim() + "*");
                    }

                    int skip = 0, take = 20;
                    if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                        take = Convert.ToInt32(pageLength.Value);
                    if (hdnCurrentPageNo.Value == "")
                    {
                        skip = 0;
                        hdnNumberPerPage.Value = take.ToString();
                        hdnCurrentPageNo.Value = "1";
                        hdnTotalRecordsCount.Value = dirInfo.GetFiles("*", SearchOption.AllDirectories).Length.ToString();
                    }
                    else
                    {
                        skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                    }
                    List<FileInfo> fileInfosList = dirInfo.GetFiles(filter.Length == 0 ? "*" : filter.ToString(), SearchOption.AllDirectories)
                        //.OrderByDescending(f => DateTime.ParseExact(f.Name.Replace(".PDF", "").Replace(".pdf", "").Split('_')[3], "yyyyMMdd", CultureInfo.InvariantCulture))
                        .Skip(skip).Take(take)
                        .ToList();
                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    fileInfosList.ForEach(x =>
                    {
                        string fileName = x.Name.Replace(".PDF", "").Replace(".pdf", "");
                        //string[] keys = fileName.Split('_');

                        //string accNo = keys[0];
                        //string fundId = keys[1].ToUpper();
                        //string statementType = keys[2].ToUpper();
                        //string statementDate = keys[3].ToUpper();
                        //string statementTag = "";
                        //if (keys.Length == 5)
                        //    statementTag = keys[4].ToUpper();

                        //Response responseUFI = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code = '" + fundId + "' ", 0, 0, false);
                        UtmcFundInformation utmcFundInformation = new UtmcFundInformation();
                        //if (responseUFI.IsSuccess)
                        //{
                        //    utmcFundInformation = ((List<UtmcFundInformation>)responseUFI.Data).FirstOrDefault();
                        //}

                        DateTime date;


                        asb.Append(@"<tr>
                                            <td class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRow' class='checkRow' value='" + x.FullName + @"' /> <label>" + index + @"</label><br/>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><a target='_blank' href='/Downloads/Corporate Documents/" + fileName + @".pdf'>" + fileName + @"</a></td>
                                            <td>" + x.CreationTime + @"</td>
                                            
                                            
                                            
                                        </tr>
                                    ");
                        index++;
                    });

                    statementLinksTbody.InnerHtml = asb.ToString();
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object Upload(Statement[] statements)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            List<string> responseMsgs = new List<string>();
            int idx = 1;
            try
            {
                foreach (Statement s in statements)
                {
                    string ex = Path.GetExtension(s.Name).ToUpper();
                    if (ex == ".PDF")
                    {
                        string fileName = s.Name.Replace(".PDF", "").Replace(".pdf", "");
                            string dirPath = HttpContext.Current.Server.MapPath("/Downloads/Corporate Documents/");
                            string filename = Path.GetFileName(s.Name);

                            string filePath = dirPath + filename;
                            Directory.CreateDirectory(dirPath);
                            if (File.Exists(filePath))
                                File.Delete(filePath);
                            byte[] result = Convert.FromBase64String(s.Base64String.Replace("data:application/pdf;base64,", ""));
                            File.WriteAllBytes(filePath, result);



                            responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-success'><i class='fa fa-check'></i></label></td><td><strong class='text-success'>" + s.Name + "</strong></td><td> <strong class='text-success'>Successfully uploaded! " + "</strong></td></tr>");
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = s.Name + " succesfully uploaded",
                                TableName = "user_order_statements",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }

                            //Audit Log Ends here
                            idx++;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        responseMsgs.Add("<tr><td>" + idx + ". </td><td><label class='label label-danger'><i class='fa fa-times'></i></label></td><td>" + s.Name + "</td><td> File type Invalid!</td></tr>");
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                responseMsgs.Add("<tr><td colspan='4' class='text-center'><label class='label label-danger'><i class='fa fa-times'></i></label>" + ex.Message + "</td></tr>");
            }
            response.Message = String.Join("<br/>", responseMsgs.ToArray());
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Delete(String[] filePaths)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            response.IsSuccess = true;
            List<string> responseMsgs = new List<string>();
            int idx = 1;
            try
            {
                foreach (String fp in filePaths)
                {
                    {
                        if (File.Exists(fp))
                            File.Delete(fp);
                        responseMsgs.Add(" Successfully deleted! ");
                        //Audit Log starts here
                        AdminLogMain alm = new AdminLogMain()
                        {
                            Description = fp + " succesfully deleted",
                            TableName = "user_order_statements",
                            UpdatedDate = DateTime.Now,
                            UserId = loginUser.Id,
                        };
                        Response responseLog = IAdminLogMainService.PostData(alm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                        }
                        else
                        {

                        }
                        //Audit Log Ends here
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                responseMsgs.Add("<tr><td colspan='4' class='text-center'><label class='label label-danger'><i class='fa fa-times'></i></label>" + ex.Message + "</td></tr>");
            }
            response.Message = String.Join("<br/>", responseMsgs.ToArray());
            return response;
        }

        public class Statement
        {
            public string Base64String { get; set; }
            public string Name { get; set; }
        }
    }
}