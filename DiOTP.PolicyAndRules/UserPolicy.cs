﻿using DiOTP.Utility;
using DiOTP.Service;
using System;
using System.Collections.Generic;
using DiOTP.Service.IService;
using System.ComponentModel;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System.Threading;
using System.Configuration;
using Google.Authenticator;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DiOTP.PolicyAndRules.UserPolicy;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.PolicyAndRules
{
    public static class UserPolicy
    {


        private static readonly Lazy<IUserService> lazyObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyObjUserTypeService = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyObjUserTypeService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        public static Int32 SMSLockTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSLockTimeInSeconds"]);

        public static Int32 SMSExpirationTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeInSeconds"]);

        public static Int32 SMSExpirationTimeForAOInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeForAOInSeconds"]);

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }


        private static readonly Lazy<IAccountOpeningService> lazyObjAO = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());

        public static IAccountOpeningService IAccountOpeningService { get { return lazyObjAO.Value; } }

        private static readonly Lazy<IAccountOpeningAddressService> lazyObjAOA = new Lazy<IAccountOpeningAddressService>(() => new AccountOpeningAddressService());

        public static IAccountOpeningAddressService IAccountOpeningAddressService { get { return lazyObjAOA.Value; } }

        private static readonly Lazy<IAccountOpeningOccupationService> lazyObjO = new Lazy<IAccountOpeningOccupationService>(() => new AccountOpeningOccupationService());

        public static IAccountOpeningOccupationService IAccountOpeningOccupationService { get { return lazyObjO.Value; } }

        private static readonly Lazy<IAccountOpeningFinancialProfileService> lazyObjFP = new Lazy<IAccountOpeningFinancialProfileService>(() => new AccountOpeningFinancialProfileService());

        public static IAccountOpeningFinancialProfileService IAccountOpeningFinancialProfileService { get { return lazyObjFP.Value; } }

        private static readonly Lazy<IAccountOpeningBankDetailService> lazyObjBD = new Lazy<IAccountOpeningBankDetailService>(() => new AccountOpeningBankDetailService());

        public static IAccountOpeningBankDetailService IAccountOpeningBankDetailService { get { return lazyObjBD.Value; } }

        private static readonly Lazy<IAccountOpeningCRSDetailService> lazyObjCRS = new Lazy<IAccountOpeningCRSDetailService>(() => new AccountOpeningCRSDetailService());

        public static IAccountOpeningCRSDetailService IAccountOpeningCRSDetailService { get { return lazyObjCRS.Value; } }

        private static readonly Lazy<IAccountOpeningFileService> lazyObjF = new Lazy<IAccountOpeningFileService>(() => new AccountOpeningFileService());

        public static IAccountOpeningFileService IAccountOpeningFileService { get { return lazyObjF.Value; } }

        public class UserClass // User Policy input
        {
            public String email { get; set; }
            public String password { get; set; }
            public User User { get; set; }
            public List<UserAccount> UserAccounts { get; set; }
            public Int32 Step { get; set; } // Step 1 = login; Step 2 = request otp button; Step 3 = Two-factorAuthenticator(both otp & google authenticator)/MA Activation; Step 4 = null step
            public Int32 SubStep { get; set; }
            public HttpContext httpContext { get; set; }
            public String localIP { get; set; }
            public FlowTypeEnum flowTypeEnum { get; set; }
            public String authPin { get; set; }
            public String authPin2 { get; set; }
            public Boolean Condition1 { get; set; }  // MAActive?
            public Int32 Type { get; set; } //Authenticator type '0' = otp; Authenticator type '1' = google authenticator
            public String IsMAActive { get; set; }
            public String additionalAccNo { get; set; }
            public Boolean Condition2 { get; set; }
            public String title { get; set; }
            public String bankID { get; set; }
            public String bankAccountNum { get; set; }
            public String AccountName { get; set; }
            //public Boolean IsMobileOTP { get; set; }
            public String fileName { get; set; }
            public String path { get; set; }
            //public Boolean IsHasFile { get; set; }
            public System.Web.UI.WebControls.FileUpload FileUpload1 { get; set; }
            public MaHolderReg maHolderReg { get; set; }
            public List<BanksDef> bankdefs { get; set; }
            public UserAccount UserAccount { get; set; }
        }

        public enum FlowTypeEnum
        {
            Get,
            Post
        }

        public class UserLoginStep // User Login output
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public User User { get; set; }
            public List<UserAccount> UserAccounts { get; set; }
        }

        public class UserCenterStep // User Center output
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public User User { get; set; }
            public List<UserAccount> UserAccounts { get; set; }
            public UserAccount UserAccount { get; set; }
            public MaHolderReg MaHolderReg { get; set; }
            public MaHolderBank maHolderBank { get; set; }
            public List<BanksDef> BanksDefs { get; set; }
            public Boolean Flag1 { get; set; }
            public Boolean Flag2 { get; set; }
            public List<Boolean> Flags1 { get; set; }
            public List<string> BankName { get; set; }
            public List<MaHolderBank> maHolderBanks { get; set; }
            public List<UserAccountBanks> userAccountBanks { get; set; }
            public List<string> BankStatus { get; set; }
            public string Url { get; set; }
            public string ManualEntryKey { get; set; }
            public UserSecurity UserSecurity { get; set; }
        }

        // User Login
        public enum UserPolicyEnum
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 99,
            [Description("Invalid Email ID.")]
            UA1 = 1,
            [Description("Too many attempts. Login Locked.")]
            UA2 = 2,
            [Description("Invalid password.")]
            UA3 = 3,
            [Description("No user type.")]
            UA4 = 4,
            [Description("Account is Locked.")]
            UA5 = 5,
            [Description("This account is deavtivated")]
            UA6 = 6,
            [Description("Invalid Email ID.")]
            UA7 = 7,
            [Description("Please enter (<sup>*</sup>) required fields.")]
            UA8 = 8,
            [Description("Invalid Password.")]
            UA9 = 9,
        }
        // User clicks request otp
        public enum UserPolicyEnum2
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 99,
            [Description("No Account Registered with this Mobile Number.")] // Mobile
            UA1 = 1,
            [Description("sent")] // Mobile
            UA2 = 2,
            [Description("already sent")] // Mobile
            UA3 = 3,
            [Description("sms locked")] // Mobile
            UA4 = 4,
        }

        // User Click google authentication  or otp submit button
        public enum UserPolicyEnum3
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 99,
            [Description("Please Enter OTP.")] // Mobile
            UA1 = 1,
            [Description("OTP must be Numeric.")] // Mobile
            UA2 = 2,
            [Description("Please Request OTP.")] // Mobile
            UA3 = 3,
            [Description("Wrong OTP. Please Enter Again.")] // Mobile
            UA4 = 4,
            [Description("OTP Expired. Please Request Again.")] // Mobile
            UA5 = 5,
            [Description("Please Enter Code.")] // Google
            UA6 = 6,
            [Description("Verification failed.")] // Google
            UA7 = 7,
            [Description("MasterAccount is verified.")]
            UA8 = 8,
            [Description("Please bind Google Authenticator.")]
            UA9 = 9,
            [Description("Authentication code must be Numeric.")]
            UA10 = 10,
            [Description("Please enter Code and OTP.")]
            UA11 = 11,
        }

        // 
        public enum UserPolicyEnum4
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 0,
            [Description("Please select no other than jpg/jpeg/png/pdf file to upload.")] // Mobile
            UA1 = 1,
            [Description("Please choose file to upload.")] // Mobile
            UA2 = 2,
            [Description("Please enter the correct number of digits for the chosen bank.")] // Mobile
            UA3 = 3,
            [Description("Please unbind the bank before delete.")] // Mobile
            UA4 = 4,
        }

        private static readonly Lazy<IUtmcFundInformationService> lazyObjFu = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObjFu.Value; } }
        public static object ValidateLogin(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            if (userClass.Step == 1) //__________________________________________________________________________STEP 1___________________________________________________________________________________
            {
                UserLoginStep userLoginStep1 = new UserLoginStep();
                try
                {
                    if (!string.IsNullOrEmpty(userClass.email) && !string.IsNullOrEmpty(userClass.password))
                    {
                        if (Utility.Helper.CustomValidator.IsValidEmail(userClass.email))// _____________________________________________Email Format Checker_________________________________________________
                        {
                            Response responseUList = IUserService.GetDataByPropertyName(nameof(Utility.User.EmailId), userClass.email, true, 0, 0, false);
                            if (responseUList.IsSuccess)
                            {
                                userClass.User = ((List<User>)responseUList.Data).FirstOrDefault();
                            }
                        }
                        else
                        {
                            userLoginStep1.Code = UserPolicyEnum.UA1.ToString();
                            response.Data = userLoginStep1;

                        }
                        if (userClass.User != null)
                        {
                            Response responseUTList = IUserTypeService.GetDataByPropertyName(nameof(UserType.UserId), userClass.User.Id.ToString(), true, 0, 0, false);
                            if (userClass.User.Status == 1)
                            {
                                if (responseUTList.IsSuccess)
                                {
                                    List<UserType> userTypes = (List<UserType>)responseUTList.Data;
                                    if (userClass.User.Password != CustomEncryptorDecryptor.EncryptPassword(userClass.password))
                                    {
                                        userClass.User.LoginAttempts = userClass.User.LoginAttempts + 1;

                                        IUserService.UpdateData(userClass.User);


                                        if (userClass.User.LoginAttempts >= 3)
                                        {
                                            userClass.User.IsLoginLocked = 1;

                                            IUserService.UpdateData(userClass.User);
                                            userLoginStep1.Code = UserPolicyEnum.UA2.ToString();
                                            response.Data = userLoginStep1;

                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                Description = "Account locked due to exceeded number of authentication failure.",
                                                TableName = "users",
                                                UpdatedDate = DateTime.Now,
                                                UserId = userClass.User.Id,
                                                UserAccountId = 0,
                                                RefId = userClass.User.Id,
                                                RefValue = userClass.User.Username,
                                                StatusType = 0
                                            };
                                            Response responseLog = IUserLogMainService.PostData(ulm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                            }
                                        }
                                        else
                                        {
                                            userLoginStep1.Code = UserPolicyEnum.UA3.ToString();
                                            response.Data = userLoginStep1;
                                        }
                                    }
                                    else
                                    {
                                        if (userClass.User.IsLoginLocked == 0)
                                        {
                                            if (userTypes.Count > 0)
                                            {
                                                UserType ut = userTypes.Where(x => x.UserTypeId == 1 && x.IsVerified == 1 && x.Status == 1).FirstOrDefault();
                                                if (ut != null)
                                                {

                                                    userLoginStep1.Code = UserPolicyEnum.S.ToString();
                                                    response.Data = userLoginStep1;

                                                    userClass.httpContext.Session["user"] = userClass.User;
                                                    userClass.httpContext.Session["LastLoginIP"] = userClass.User.LastLoginIp;
                                                    userClass.httpContext.Session["LastLoginTime"] = userClass.User.LastLoginDate;
                                                    userClass.httpContext.Session["isAgent"] = userClass.User.IsAgent;
                                                    if (userClass.User.IsAgent == 1)
                                                    {
                                                        UserType utAgent = userClass.User.UserIdUserTypes.Where(x => new List<Int32> { 6, 7, 8, 9 }.Contains(x.UserTypeId) && x.IsVerified == 1 && x.Status == 1).FirstOrDefault();
                                                        if (utAgent != null)
                                                        {
                                                            userClass.httpContext.Session["AgentType"] = utAgent.UserTypeIdUserTypesDef.Name;
                                                        }
                                                    }
                                                    userClass.User.IsLoginLocked = 0;
                                                    userClass.User.LoginAttempts = 0;
                                                    userClass.User.LastLoginDate = DateTime.Now;
                                                    userClass.httpContext.Session["CurrentLoginTime"] = userClass.User.LastLoginDate;
                                                    userClass.User.LastLoginIp = TrackIPAddress.GetUserPublicIP(userClass.localIP) + ", " + userClass.localIP;
                                                    userClass.User.VerificationCode = 0;
                                                    userClass.User.VerifyExpired = 1;
                                                    Response resUpdate = IUserService.UpdateData(userClass.User);
                                                    if (resUpdate.IsSuccess)
                                                    {
                                                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + userClass.User.Id + "' and status='1' ", 0, 0, false);
                                                        if (responseUAList.IsSuccess)
                                                        {
                                                            List<UserAccount> uAs = (List<UserAccount>)responseUAList.Data;
                                                            UserAccount uA = ((List<UserAccount>)responseUAList.Data).Where(x => x.IsPrimary == 1).FirstOrDefault();
                                                            if (uA.IsVerified == 0)
                                                            {
                                                                foreach (UserAccount ua in uAs)
                                                                {
                                                                    ua.VerificationCode = 0;
                                                                    ua.VerifyExpired = 1;
                                                                }
                                                                IUserAccountService.UpdateBulkData(uAs);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            userLoginStep1.Code = UserPolicyEnum.E.ToString();
                                                            userLoginStep1.Message = responseUAList.Message;
                                                            response.Data = userLoginStep1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        userLoginStep1.Code = UserPolicyEnum.E.ToString();
                                                        userLoginStep1.Message = resUpdate.Message;
                                                        response.Data = userLoginStep1;
                                                    }
                                                    userLoginStep1.User = userClass.User;
                                                    userLoginStep1.UserAccounts = userClass.UserAccounts;

                                                }
                                                else
                                                {
                                                    userLoginStep1.Code = UserPolicyEnum.UA3.ToString();
                                                    response.Data = userLoginStep1;
                                                }
                                            }
                                            else
                                            {
                                                userLoginStep1.Code = UserPolicyEnum.UA4.ToString();
                                                response.Data = userLoginStep1;
                                            }
                                        }
                                        else
                                        {
                                            userLoginStep1.Code = UserPolicyEnum.UA5.ToString();
                                            response.Data = userLoginStep1;
                                        }
                                    }
                                }
                                else
                                {
                                    userLoginStep1.Code = UserPolicyEnum.E.ToString();
                                    userLoginStep1.Message = responseUTList.Message;
                                    response.Data = userLoginStep1;

                                }
                            }
                            else
                            {
                                userLoginStep1.Code = UserPolicyEnum.UA6.ToString();
                                response.Data = userLoginStep1;
                            }
                        }
                        else
                        {
                            userLoginStep1.Code = UserPolicyEnum.UA7.ToString();
                            response.Data = userLoginStep1;
                        }
                    }
                    else
                    {
                        userLoginStep1.Code = UserPolicyEnum.UA8.ToString();
                        response.Data = userLoginStep1;
                    }
                }
                catch (Exception ex)
                {
                    response.IsSuccess = false;
                    response.Message = ex.Message;
                }
            }
            else if (userClass.Step == 2) //_________________________________________________________________________________STEP 2_______________________________________________________________________________________________
            {
                UserLoginStep userLoginStep2 = new UserLoginStep();
                if (userClass.Condition1 == true) // Is MAActive?
                {

                    try
                    {
                        User user = (User)HttpContext.Current.Session["user"];
                        Boolean secondFlag = true;
                        Boolean BankFlag = false;
                        Response responseUAList = IUserAccountService.GetDataByFilter("user_id = '" + user.Id + "' and is_primary = 1 and status=1 ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            if (userClass.Condition2 == true) // IsUserCenter?
                            {
                                if (userClass.title == "LOGIN P/W UPDATE" || userClass.title == "TRANSACTION P/W UPDATE")
                                {
                                    string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(userClass.password);
                                    if (user.Password == encryptedPassword)
                                    {
                                        secondFlag = true;
                                    }
                                    else
                                    {
                                        secondFlag = false;
                                    }
                                }
                                else if (userClass.title == "ADD BANK DETAILS" || userClass.title == "GOOGLE AUTH")
                                {
                                    secondFlag = true;
                                    BankFlag = true;

                                }
                            }
                            if (secondFlag == true)
                            {
                                UserAccount ua = ((List<UserAccount>)responseUAList.Data).ToList().FirstOrDefault();
                                Response responseMAHolderRegList = ServiceManager.GetMaHolderRegByAccountNo(ua.AccountNo);
                                if (responseMAHolderRegList.IsSuccess)
                                {
                                    MaHolderReg maHolderReg = (MaHolderReg)responseMAHolderRegList.Data;
                                    if (user.IsSecurityChecked == 1)
                                    {
                                        string toMobileNumber = user.MobileNumber;
                                        response = CheckAndRequestPin(toMobileNumber, userClass.title);
                                        if (response.IsSuccess)
                                        {
                                            string[] tmpMessage = response.Message.Split(',');
                                            if (tmpMessage[0] == "sms locked")
                                            {
                                                userLoginStep2.Code = UserPolicyEnum2.UA4.ToString();
                                                response.Data = userLoginStep2;
                                            }
                                            else if (tmpMessage[0] == "sent")
                                            {
                                                userLoginStep2.Code = UserPolicyEnum2.S.ToString();
                                                response.Data = userLoginStep2;
                                            }
                                            else if (tmpMessage[0] == "already sent")
                                            {
                                                userLoginStep2.Code = UserPolicyEnum2.S.ToString();
                                                response.Data = userLoginStep2;
                                            }
                                            else
                                            { }
                                            response.Message = (tmpMessage[0] == "sms locked" ? tmpMessage[0] : tmpMessage[0] + "," + tmpMessage[1]);
                                            response.IsSuccess = true;
                                            //return response;
                                        }
                                        else
                                        {
                                            userLoginStep2.Code = UserPolicyEnum2.E.ToString();
                                            userLoginStep2.Message = response.Message;
                                            response.Data = userLoginStep2;
                                        }
                                    }
                                    else
                                    {
                                        string toMobileNumber = maHolderReg.HandPhoneNo;
                                        if (ua.IsPrinciple == 0)
                                            toMobileNumber = maHolderReg.JointTelNo;
                                        if (toMobileNumber != null)
                                        {
                                            response = CheckAndRequestPin(toMobileNumber, userClass.title);
                                            if (response.IsSuccess)
                                            {
                                                string[] tmpMessage = response.Message.Split(',');
                                                if (tmpMessage[0] == "sms locked")
                                                {
                                                    userLoginStep2.Code = UserPolicyEnum2.UA4.ToString();
                                                    response.Data = userLoginStep2;
                                                }
                                                else if (tmpMessage[0] == "sent")
                                                {
                                                    userLoginStep2.Code = UserPolicyEnum2.S.ToString();
                                                    response.Data = userLoginStep2;
                                                }
                                                else if (tmpMessage[0] == "already sent")
                                                {
                                                    userLoginStep2.Code = UserPolicyEnum2.S.ToString();
                                                    response.Data = userLoginStep2;
                                                }
                                                else
                                                { }
                                                response.Message = (tmpMessage[0] == "sms locked" ? tmpMessage[0] : tmpMessage[0] + "," + tmpMessage[1]);
                                                response.IsSuccess = true;
                                                //return response;
                                            }
                                            else
                                            {
                                                userLoginStep2.Code = UserPolicyEnum2.E.ToString();
                                                userLoginStep2.Message = response.Message;
                                                response.Data = userLoginStep2;
                                            }
                                        }
                                        else
                                        {
                                            userLoginStep2.Code = UserPolicyEnum2.UA1.ToString();
                                            response.Data = userLoginStep2;
                                        }
                                    }
                                }
                                else
                                {
                                    userLoginStep2.Code = UserPolicyEnum2.E.ToString();
                                    userLoginStep2.Message = responseMAHolderRegList.Message;
                                    response.Data = userLoginStep2;
                                }
                            }
                            else if (secondFlag == false && BankFlag == false)
                            {
                                response.Message = "Wrong password";
                                //return response;
                            }
                            else
                            {
                                response.Message = "failed";
                                //return response;
                            }

                        }
                        else
                        {
                            userLoginStep2.Code = UserPolicyEnum2.E.ToString();
                            userLoginStep2.Message = responseUAList.Message;
                            response.Data = userLoginStep2;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.IsSuccess = false;
                        response.Message = ex.Message;
                    }

                }
                else
                {

                    try
                    {
                        User user = (User)HttpContext.Current.Session["user"];
                        Response responseUAList = IUserAccountService.GetDataByFilter("user_id = '" + user.Id + "' and is_primary = 1 and status=1 ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            UserAccount ua = ((List<UserAccount>)responseUAList.Data).ToList().FirstOrDefault();
                            if (user.IsSecurityChecked == 1)
                            {
                                string toMobileNumber = user.MobileNumber;
                                return CheckAndRequestPin(toMobileNumber, userClass.title);
                            }
                            else
                            {
                                Response responseMAHolderRegList = ServiceManager.GetMaHolderRegByAccountNo(ua.AccountNo);
                                if (responseMAHolderRegList.IsSuccess)
                                {
                                    MaHolderReg maHolderReg = (MaHolderReg)responseMAHolderRegList.Data;
                                    string toMobileNumber = maHolderReg.HandPhoneNo;
                                    if (ua.IsPrinciple == 0)
                                        toMobileNumber = maHolderReg.JointTelNo;
                                    if (toMobileNumber != null)
                                        return CheckAndRequestPin(toMobileNumber, userClass.title);
                                    else
                                        return response;
                                }
                                else
                                {
                                    response.Message = responseMAHolderRegList.Message;
                                    return response;
                                }
                            }
                        }
                        else
                        {
                            response.Message = responseUAList.Message;
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.IsSuccess = false;
                        response.Message = ex.Message;
                    }
                }
            }
            else if (userClass.Step == 3) //_________________________________________________________________________________________STEP 3_________________________________________________________________________________________
            {
                UserLoginStep userLoginStep3 = new UserLoginStep();
                if (userClass.Condition1 == true) // Is MAActive?
                {
                    User user = userClass.User;
                    List<UserAccount> userAccounts = userClass.UserAccounts;

                    try
                    {
                        if (userClass.Type == 0) //________________________________________________MOBILE AUTHENTICATION______________________________________________________________
                        {

                            int result = 0;
                            if (userClass.authPin == "" || userClass.authPin == null || userClass.authPin.Trim() == "0")
                            {

                                userLoginStep3.Code = UserPolicyEnum3.UA1.ToString();
                                response.Data = userLoginStep3;
                            }
                            else if (!Int32.TryParse(userClass.authPin, out result))
                            {
                                userLoginStep3.Code = UserPolicyEnum3.UA2.ToString();
                                response.Data = userLoginStep3;
                            }
                            else if (result == 0)
                            {
                                userLoginStep3.Code = UserPolicyEnum3.UA1.ToString();
                                response.Data = userLoginStep3;
                            }
                            else
                            {
                                if (userClass.httpContext.Session["user"] != null)
                                {
                                    user = (User)userClass.httpContext.Session["user"];

                                    Response responseSubmit = IUserService.GetSingle(user.Id);
                                    if (responseSubmit.IsSuccess)
                                    {
                                        user = (User)responseSubmit.Data;
                                        if (user.VerificationCode.Value == 0)
                                        {
                                            userLoginStep3.Code = UserPolicyEnum3.UA3.ToString();
                                            response.Data = userLoginStep3;
                                        }
                                        else
                                        {
                                            Int32 mobilePin = Convert.ToInt32(userClass.authPin);
                                            if (user.VerifyExpired == 0)
                                            {
                                                if (mobilePin == user.VerificationCode)
                                                {
                                                    UserLogMain ulm = new UserLogMain()
                                                    {
                                                        Description = "Two-factor authentication successful - Mobile.",
                                                        TableName = "users",
                                                        UpdatedDate = DateTime.Now,
                                                        UserId = user.Id,
                                                        UserAccountId = 0,
                                                        RefId = user.Id,
                                                        RefValue = user.Username,
                                                        StatusType = 1
                                                    };
                                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                                    if (!responseLog.IsSuccess)
                                                    {
                                                        //Audit log failed
                                                    }

                                                    user.SmsAttempts = 0;
                                                    //Mobile Pin OTP Commented 1234567890
                                                    user.VerifyExpired = 1;
                                                    user.VerificationCode = 0;
                                                    //user.IsSatChecked = 1;
                                                    IUserService.UpdateData(user);
                                                    userLoginStep3.Code = UserPolicyEnum3.S.ToString();
                                                    response.Data = userLoginStep3;
                                                }
                                                else
                                                {
                                                    UserLogMain ulm = new UserLogMain()
                                                    {
                                                        Description = "Two-factor authentication failed - Mobile.",
                                                        TableName = "users",
                                                        UpdatedDate = DateTime.Now,
                                                        UserId = user.Id,
                                                        UserAccountId = 0,
                                                        RefId = user.Id,
                                                        RefValue = user.Username,
                                                        StatusType = 0
                                                    };
                                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                                    if (!responseLog.IsSuccess)
                                                    {
                                                        //Audit log failed
                                                    }
                                                    userLoginStep3.Code = UserPolicyEnum3.UA4.ToString();
                                                    response.Data = userLoginStep3;
                                                }
                                            }
                                            else
                                            {
                                                userLoginStep3.Code = UserPolicyEnum3.UA5.ToString();
                                                response.Data = userLoginStep3;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        userLoginStep3.Code = UserPolicyEnum3.E.ToString();
                                        userLoginStep3.Message = responseSubmit.Message;
                                        response.Data = userLoginStep3;
                                    }

                                }
                            }
                        }
                        else if (userClass.Type == 1) //___________________________________________________GOOGLE AUTHENTICATION____________________________________________________________________________
                        {
                            if (userClass.authPin == "" || userClass.authPin == null) //___________________________________NULL DETECTOR______________________________________________________________________________________
                            {
                                userLoginStep3.Code = UserPolicyEnum3.UA6.ToString();
                                response.Data = userLoginStep3;
                            }
                            else
                            {
                                try
                                {
                                    if (userClass.httpContext.Session["user"] != null)
                                    {
                                        user = (User)userClass.httpContext.Session["user"];
                                        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                        if (responseUSList.IsSuccess)
                                        {
                                            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                                            if (googleAuthUserSecurity.IsVerified != 0)
                                            {
                                                if (Int32.TryParse(userClass.authPin.Trim(), out int result))
                                                {

                                                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                                    bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, userClass.authPin);
                                                    if (isCorrectPIN) //_______________________________________________________________AUTHENTICATION SUCCESS_________________________________________________________________________________
                                                    {
                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "Two-factor authentication successful - Google Authenticator.",
                                                            TableName = "users",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = 0,
                                                            RefId = user.Id,
                                                            RefValue = user.Username,
                                                            StatusType = 1
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                        }

                                                        //user.IsSatChecked = 1;
                                                        IUserService.UpdateData(user);
                                                        Response responseUSList2 = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                                        if (responseUSList2.IsSuccess)
                                                        {
                                                            List<UserSecurity> userSecurities = ((List<UserSecurity>)responseUSList2.Data).Where(x => x.UserSecurityTypeId != 3).ToList();
                                                            userSecurities.Add(googleAuthUserSecurity);
                                                            user.UserIdUserSecurities = userSecurities;
                                                            userLoginStep3.Code = UserPolicyEnum3.S.ToString();
                                                            response.Data = userLoginStep3;
                                                            //RefreshData();
                                                        }
                                                        else
                                                        {

                                                            userLoginStep3.Code = UserPolicyEnum.E.ToString();
                                                            userLoginStep3.Message = responseUSList.Message;
                                                            response.Data = userLoginStep3;
                                                        }


                                                    }
                                                    else //______________________________________________________________________AUTHENTICATION FAIL_____________________________________________________________________________
                                                    {
                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "Two-factor authentication failed - Google Authenticator.",
                                                            TableName = "users",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = 0,
                                                            RefId = user.Id,
                                                            RefValue = user.Username,
                                                            StatusType = 0
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                        }

                                                        userLoginStep3.Code = UserPolicyEnum3.UA7.ToString();
                                                        response.Data = userLoginStep3;
                                                    }
                                                }
                                                else
                                                {
                                                    userLoginStep3.Code = UserPolicyEnum3.UA10.ToString();
                                                    userLoginStep3.Message = responseUSList.Message;
                                                    response.Data = userLoginStep3;
                                                }
                                            }
                                            else
                                            {
                                                userLoginStep3.Code = UserPolicyEnum3.UA9.ToString();
                                                userLoginStep3.Message = responseUSList.Message;
                                                response.Data = userLoginStep3;
                                            }
                                        }
                                        else
                                        {
                                            userLoginStep3.Code = UserPolicyEnum.E.ToString();
                                            userLoginStep3.Message = responseUSList.Message;
                                            response.Data = userLoginStep3;
                                        }
                                    }
                                    else
                                    {
                                        userLoginStep3.Code = UserPolicyEnum.E.ToString();
                                        userLoginStep3.Message = "Session Expired";
                                        response.Data = userLoginStep3;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog("Exception: " + ex.ToString());
                                    response.Message = ex.Message;
                                }
                            }
                        }
                        else
                        {
                            //___________________________NULL AUTHENTICATOR TYPE______________________________________
                        }
                    }
                    catch (Exception ex)
                    {
                        response.Message = ex.Message;
                    }
                }
                else //___________________________________________________________________________________________FIRST TIME MA LOGGED IN____________________________________________________________________________________
                {
                    User user = userClass.User;
                    List<UserAccount> userAccounts = userClass.UserAccounts;

                    try
                    {
                        if (userClass.additionalAccNo == "0") //Means MA inactive (Single MA)
                        {
                            Response responseUAList2 = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                            if (responseUAList2.IsSuccess)
                            {
                                List<UserAccount> uAs = (List<UserAccount>)responseUAList2.Data;
                                Int32 VerificationCode = uAs.FirstOrDefault().VerificationCode.Value;
                                Int32 VerificationExpiry = uAs.FirstOrDefault().VerifyExpired;
                                List<UserAccount> userAccounts2 = (List<UserAccount>)responseUAList2.Data;

                                if (userClass.authPin == "" || userClass.authPin == null)
                                {
                                    userLoginStep3.Code = UserPolicyEnum3.UA1.ToString();
                                    response.Data = userLoginStep3;

                                }
                                else
                                {
                                    if (VerificationCode != 0)
                                    {
                                        if (VerificationExpiry == 0)
                                        {
                                            if (Int32.TryParse(userClass.authPin.Trim(), out int result))
                                            {

                                                Int32 verPin = Convert.ToInt32(userClass.authPin.TrimStart(new Char[] { '0' }) == "" ? "0" : userClass.authPin.TrimStart(new Char[] { '0' }));
                                                if (VerificationCode == verPin) //_______________________________________________________________MA ACTIVATION SUCCESSFUL________________________________________________________________
                                                {

                                                    uAs.ForEach(uA =>
                                                    {
                                                        uA.IsVerified = 1;
                                                        uA.VerificationCode = 0;
                                                        uA.VerifyExpired = 1;
                                                        IUserAccountService.UpdateData(uA);


                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "MA (" + uA.AccountNo + ") Activation successful - Mobile.",
                                                            TableName = "user_accounts",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = uA.Id,
                                                            RefId = uA.Id,
                                                            RefValue = uA.AccountNo,
                                                            StatusType = 1
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                        }



                                                    });

                                                    Response responseUFIs = IUtmcFundInformationService.GetDataByFilter(" 1=1 ", 0, 0, false);
                                                    if (responseUFIs.IsSuccess)
                                                    {
                                                        List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFIs.Data;
                                                        DateTime currentTime = DateTime.Now;
                                                        DateTime lastTime = currentTime.AddMonths(-12);

                                                        while (lastTime <= currentTime)
                                                        {
                                                            DateTime fromDate = new DateTime(lastTime.Year, lastTime.Month, 1);
                                                            fromDate = fromDate.AddMonths(-1);
                                                            DateTime toDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                                                            foreach (UserAccount ua in uAs)
                                                                StatementService.GenerateReport(ua.AccountNo, fromDate, toDate, 1, utmcFundInformations);
                                                            lastTime = lastTime.AddMonths(1);
                                                        }

                                                    }
                                                    UserSecurity MobileSecurity = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 2).FirstOrDefault();
                                                    MobileSecurity.IsVerified = 1;
                                                    MobileSecurity.VerifiedDate = DateTime.Now;
                                                    MobileSecurity.VerificationPin = VerificationCode;
                                                    IUserSecurityService.UpdateData(MobileSecurity);

                                                    Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                                    if (responseUSList.IsSuccess)
                                                    {
                                                        List<UserSecurity> uSs = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId != 2).ToList();
                                                        uSs.Add(MobileSecurity);
                                                        user.UserIdUserSecurities = uSs;
                                                        userLoginStep3.Code = UserPolicyEnum3.S.ToString();
                                                        response.Data = userLoginStep3;
                                                    }
                                                    else
                                                    {
                                                        userLoginStep3.Code = UserPolicyEnum3.E.ToString();
                                                        userLoginStep3.Message = responseUSList.Message;
                                                        response.Data = userLoginStep3;

                                                    }

                                                    Response responseU = IUserService.GetSingle(user.Id);
                                                    if (responseU.IsSuccess)
                                                    {
                                                        User u = (User)responseU.Data;
                                                        u.HardcopyUpdatedDate = DateTime.Now;
                                                        u.ActivationDate = DateTime.Now;
                                                        IUserService.UpdateData(u);
                                                    }

                                                }
                                                else
                                                {
                                                    UserLogMain ulm = new UserLogMain()
                                                    {
                                                        Description = "MA Activation failed - Mobile.",
                                                        TableName = "users",
                                                        UpdatedDate = DateTime.Now,
                                                        UserId = user.Id,
                                                        UserAccountId = 0,
                                                        RefId = user.Id,
                                                        RefValue = user.Username,
                                                        StatusType = 0
                                                    };
                                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                                    if (!responseLog.IsSuccess)
                                                    {
                                                        //Audit log failed
                                                    }
                                                    userLoginStep3.Code = UserPolicyEnum3.UA4.ToString();
                                                    response.Data = userLoginStep3;

                                                }

                                            }
                                            else
                                            {
                                                userLoginStep3.Code = UserPolicyEnum3.UA2.ToString();
                                                response.Data = userLoginStep3;

                                            }
                                        }
                                        else
                                        {
                                            userLoginStep3.Code = UserPolicyEnum3.UA5.ToString();
                                            response.Data = userLoginStep3;

                                        }
                                    }
                                    else
                                    {
                                        userLoginStep3.Code = UserPolicyEnum3.UA3.ToString();
                                        response.Data = userLoginStep3;

                                    }
                                }


                            }
                            else
                            {

                                userLoginStep3.Code = UserPolicyEnum.E.ToString();
                                userLoginStep3.Message = responseUAList2.Message;
                                response.Data = userLoginStep3;
                            }
                        }
                        else if (userClass.additionalAccNo == "1")// ________________________________________________________Additional MA Activation__________________________________________________________________
                        {
                            String additionalAccNo2 = userClass.additionalAccNo;
                            Response responseUAList2 = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                            if (responseUAList2.IsSuccess)
                            {
                                List<UserAccount> userAccounts2 = (List<UserAccount>)responseUAList2.Data;
                                UserAccount uA = userAccounts2.Where(x => x.AccountNo == additionalAccNo2).FirstOrDefault();
                                Int32 VerificationExpiry = uA.VerifyExpired;
                                Int32 VerificationCode = uA.VerificationCode.Value;

                                if (userClass.authPin == "" || userClass.authPin == null)
                                {
                                    userLoginStep3.Code = UserPolicyEnum3.UA1.ToString();

                                }
                                else
                                {
                                    if (VerificationCode != 0)
                                    {
                                        if (VerificationExpiry == 0)
                                        {
                                            if (Int32.TryParse(userClass.authPin.Trim(), out int result))
                                            {
                                                if (uA.IsVerified == 0)
                                                {
                                                    Int32 verPin = Convert.ToInt32(userClass.authPin.TrimStart(new Char[] { '0' }) == "" ? "0" : userClass.authPin.TrimStart(new Char[] { '0' }));
                                                    if (VerificationCode == verPin)
                                                    {
                                                        uA.IsVerified = 1;
                                                        uA.VerificationCode = 0;
                                                        IUserAccountService.UpdateData(uA);
                                                        List<UserAccount> uAs = userAccounts.Where(x => x.AccountNo != additionalAccNo2).ToList();
                                                        uAs.Add(uA);
                                                        user.UserIdUserAccounts = uAs;
                                                        userLoginStep3.Code = UserPolicyEnum3.S.ToString();
                                                        response.Data = userLoginStep3;

                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "Additional MA (" + uA.AccountNo + ") Activation successful - Mobile.",
                                                            TableName = "users",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = uA.Id,
                                                            RefId = uA.Id,
                                                            RefValue = uA.AccountNo,
                                                            StatusType = 1
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                        }


                                                    }
                                                    else
                                                    {
                                                        UserLogMain ulm = new UserLogMain()
                                                        {
                                                            Description = "Additional MA (" + uA.AccountNo + ") Activation failed - Mobile.",
                                                            TableName = "users",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = user.Id,
                                                            UserAccountId = uA.Id,
                                                            RefId = uA.Id,
                                                            RefValue = uA.AccountNo,
                                                            StatusType = 0
                                                        };
                                                        Response responseLog = IUserLogMainService.PostData(ulm);
                                                        if (!responseLog.IsSuccess)
                                                        {
                                                            //Audit log failed
                                                        }

                                                        userLoginStep3.Code = UserPolicyEnum3.UA4.ToString();
                                                        response.Data = userLoginStep3;

                                                    }
                                                }
                                                else
                                                {
                                                    userLoginStep3.Code = UserPolicyEnum3.UA8.ToString();
                                                    response.Data = userLoginStep3;

                                                }
                                            }
                                            else
                                            {
                                                userLoginStep3.Code = UserPolicyEnum3.UA2.ToString();
                                                response.Data = userLoginStep3;

                                            }
                                        }
                                        else
                                        {
                                            userLoginStep3.Code = UserPolicyEnum3.UA5.ToString();
                                            response.Data = userLoginStep3;

                                        }
                                    }
                                    else
                                    {
                                        userLoginStep3.Code = UserPolicyEnum3.UA3.ToString();
                                        response.Data = userLoginStep3;

                                    }
                                }
                            }
                            else
                            {

                                userLoginStep3.Code = UserPolicyEnum.E.ToString();
                                userLoginStep3.Message = responseUAList2.Message;
                                response.Data = userLoginStep3;
                            }
                        }
                        else
                        {
                            // NULL ADDITIONAL MA INDICATOR
                        }

                    }
                    catch (Exception ex)
                    {
                        response.IsSuccess = false;
                        response.Message = ex.Message;
                    }
                }
            }
            else if (userClass.Step == 4)
            {
                // NULL STEP
            }
            return response;
        }
        public static Response CheckAndRequestPin(String mobileNo, String title)
        {
            Response response = new Response();
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    user = (User)responseUList.Data;
                    if (user != null)
                    {
                        if (user.IsSmsLocked == 1)
                        {
                            Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                            PinTimerThread.Start();
                            returnString = "sms locked";
                            response.IsSuccess = false;
                            response.Message = returnString;
                        }
                        else if (user.VerifyExpired == 1)
                        {
                            if (user.SmsAttempts >= 3)
                            {
                                user.IsSmsLocked = 1;
                                user.VerifyExpired = 1;
                                Thread PinTimerThread = new Thread(() => PinTimerForSMSLock(user));
                                PinTimerThread.Start();
                                returnString = "sms locked";
                                response.IsSuccess = false;
                                response.Message = returnString;
                            }
                            else
                            {

                                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                String result = "";

                                result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                Int32 length = mobileNo.Length;
                                String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);
                                user.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                user.VerifyExpired = 0;
                                user.SmsAttempts = user.SmsAttempts + 1;

                                Thread PinTimerThread = new Thread(() => PinTimer(user));
                                PinTimerThread.Start();
                                returnString = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);

                                response.IsSuccess = true;
                                response.Message = returnString;
                            }
                            IUserService.UpdateData(user);
                        }
                        else
                        {
                            Thread PinTimerThread = new Thread(() => PinTimer(user));
                            PinTimerThread.Start();
                            returnString = "already sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                            response.IsSuccess = true;
                            response.Message = returnString;
                        }
                    }
                    else
                    {
                        returnString = "no account";
                        response.IsSuccess = false;
                        response.Message = returnString;
                    }
                }
                else
                {
                    returnString = responseUList.Message;
                    response.IsSuccess = false;
                    response.Message = returnString;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnFuAddressUpload_Click: " + ex.Message);
                returnString = ex.Message;
                response.IsSuccess = false;
                response.Message = returnString;
            }

            return response;
        }

        public static string CheckMAActivation(String mobileNo)
        {
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetSingle(user.Id);
                if (responseUList.IsSuccess)
                {
                    user = (User)responseUList.Data;
                    if (user != null)
                    {
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        {
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> uas = (List<UserAccount>)responseUAList.Data;
                                UserAccount ua1 = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();

                                if (ua1.VerifyExpired == 1)
                                {
                                    //String toMobileNumber = user.MobileNumber;
                                    String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                    String result = "";
                                    String title = "MA ACTIVATION";
                                    result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                                    Int32 length = mobileNo.Length;
                                    String displayMoileNumber = new String('X', length - 4) + mobileNo.Substring(length - 4);

                                    foreach (UserAccount ua in uas)
                                    {
                                        ua.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                        ua.VerifyExpired = 0;

                                        IUserAccountService.UpdateData(ua);
                                    }

                                    Thread PinTimerThread = new Thread(() => PinTimerMA(user));
                                    PinTimerThread.Start();
                                    returnString = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);

                                    IUserService.UpdateData(user);

                                    UserLogMain ulm = new UserLogMain()
                                    {
                                        Description = "Request OTP for MA Activation successful",
                                        TableName = "users",
                                        UpdatedDate = DateTime.Now,
                                        UserId = user.Id,
                                        UserAccountId = 0,
                                        RefId = user.Id,
                                        RefValue = user.Username,
                                        StatusType = 1
                                    };
                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }


                                    return returnString;
                                }
                                else
                                {
                                    Thread PinTimerThread = new Thread(() => PinTimerMA(user));
                                    PinTimerThread.Start();

                                    returnString = "already sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3);
                                }
                            }
                        }
                    }
                    else
                        returnString = "no account";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page btnFuAddressUpload_Click: " + ex.Message);
            }
            return returnString;
        }

        public static void PinTimerForSMSLock(User user)
        {
            try
            {
                //User user = (User)HttpContext.Current.Session["user"];
                //User user = sessionUser;

                while (currentSecLock <= SMSLockTimeInSeconds)
                {
                    if (currentSecLock == SMSLockTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.SmsAttempts = 0;
                        user.IsSmsLocked = 0;
                        IUserService.UpdateData(user);
                        currentSecLock = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecLock++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page PinTimerForSMSLock: " + ex.Message);
            }
        }

        public static void PinTimer(User user)
        {
            try
            {

                while (currentSec <= SMSExpirationTimeInSeconds)
                {
                    if (currentSec == SMSExpirationTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.VerifyExpired = 1;
                        IUserService.UpdateData(user);
                        currentSec = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSec++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page PinTimer: " + ex.Message);
            }
        }

        public static void PinTimerMA(User user)
        {
            try
            {
                //User user = sessionUser;

                while (currentSecMA <= SMSExpirationTimeInSeconds)
                {
                    if (currentSecMA == SMSExpirationTimeInSeconds)
                    {
                        Response responseUserAccsList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        List<UserAccount> userAccs = (List<UserAccount>)responseUserAccsList.Data;
                        foreach (UserAccount ua in userAccs)
                        {

                            ua.VerifyExpired = 1;
                            IUserAccountService.UpdateData(ua);
                        }
                        currentSecMA = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecMA++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Portfolio Page PinTimer: " + ex.Message);
            }
        }

        public static Int32 currentSec = 0;
        public static Int32 currentSecLock = 0;
        public static Int32 currentSecMA = 0;

        public static List<BanksDef> bankdefs = new List<BanksDef>();

        public static object AddBank(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            if (userClass.Step == 1) //__________________________________________________________________________STEP 1___________________________________________________________________________________
            {
                UserCenterStep userCenterStep1 = new UserCenterStep();
                try
                {
                    User user = userClass.User;
                    Response responseBD = IBanksDefService.GetSingle(Convert.ToInt32(userClass.bankID));
                    if (responseBD.IsSuccess)
                    {
                        BanksDef bd = (BanksDef)responseBD.Data;
                        Boolean isValidAccountNumber = false;
                        if (bd.NoFormat != null)
                        {
                            if (userClass.bankAccountNum.Trim().Length == Convert.ToInt32(bd.NoFormat) && Regex.IsMatch(userClass.bankAccountNum.Trim(), @"^\d+$"))
                            {
                                isValidAccountNumber = true;
                            }
                        }
                        if (isValidAccountNumber)
                        {

                            Response responseNew = IUserService.GetSingle(user.Id);
                            if (responseNew.IsSuccess)
                            {
                                RefreshOTP(responseNew);
                                {

                                    string Date = DateTime.Now.ToString("ddMMyyyy-mmsshh");
                                    string G = Guid.NewGuid().ToString();
                                    string dbpath = string.Empty;

                                    if (userClass.FileUpload1.HasFile)
                                    {
                                        if (Path.GetExtension(userClass.FileUpload1.FileName).ToLower() == ".jpg" ||
                                            Path.GetExtension(userClass.FileUpload1.FileName).ToLower() == ".jpeg" ||
                                            Path.GetExtension(userClass.FileUpload1.FileName).ToLower() == ".png" ||
                                            Path.GetExtension(userClass.FileUpload1.FileName).ToLower() == ".pdf")
                                        {
                                            string subdir = userClass.path;
                                            dbpath = "/UserKyc/";

                                            if (!Directory.Exists(subdir))
                                                Directory.CreateDirectory(subdir);

                                            string filename = "ID" + user.Id + "_" + Date + (Path.GetFileName(userClass.FileUpload1.FileName));
                                            subdir = subdir + filename;
                                            dbpath = dbpath + filename;
                                            userClass.FileUpload1.SaveAs(subdir);
                                            MaHolderBank x = new MaHolderBank()
                                            {
                                                UserId = user.Id,
                                                IsPrimary = 1,
                                                BankDefId = Convert.ToInt32(userClass.bankID),
                                                AccountName = userClass.AccountName,
                                                BankAccountNo = userClass.bankAccountNum,
                                                Remarks = "",
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = user.Id,

                                                UpdatedDate = DateTime.Now,
                                                UpdatedBy = user.Id,
                                                Version = 1,
                                                Status = 0,
                                                Image = dbpath,
                                            };

                                            Response responseX = IMaHolderBankService.PostData(x);
                                            x = (MaHolderBank)responseX.Data;

                                            UserLogMain z = new UserLogMain()
                                            {
                                                TableName = "ma_holder_bank",
                                                Description = "User requested for add Bank details",
                                                UpdatedDate = DateTime.Now,
                                                UserId = user.Id,
                                                RefId = x.Id,
                                                RefValue = x.AccountName,
                                                StatusType = 1
                                            };

                                            Response response1 = IUserLogMainService.PostData(z);
                                            z = (UserLogMain)response1.Data;

                                            Email email = new Email();
                                            email.user = user;
                                            userCenterStep1.Code = UserPolicyEnum4.S.ToString();

                                            //Response responseMA = ServiceManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(y => y.IsPrimary == 1).FirstOrDefault().AccountNo);
                                            //if (responseMA.IsSuccess)
                                            //{
                                            //    MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                            //    // alert message
                                            //    //bindBankDetails(user, maHolderReg);
                                            //}
                                            //Re-check

                                        }
                                        else
                                        {
                                            userCenterStep1.Code = UserPolicyEnum4.UA1.ToString();



                                        }
                                    }
                                    else
                                    {
                                        userCenterStep1.Code = UserPolicyEnum4.UA2.ToString();


                                    }

                                }
                            }
                            else
                            {
                                userCenterStep1.Code = UserPolicyEnum4.E.ToString();


                            }


                        }
                        else
                        {
                            userCenterStep1.Code = UserPolicyEnum4.UA3.ToString();


                        }
                    }
                    response.Data = userCenterStep1;
                    //txtPacNoBank.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog("Setting Page btnBankDetailsSubmit_Click " + ex.Message);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
                }
            }
            else if (userClass.Step == 2) //__________________________________________________________________________STEP 2___________________________________________________________________________________
            {
                UserCenterStep userCenterStep1 = new UserCenterStep();

                if (userClass.Condition1 == true) // if Bind Bank
                {
                    //User sessionUser = (User)Session["user"];
                    //string idString = hdnBindBankId.Value;
                    //Int32 id = Convert.ToInt32(idString);

                    string accountfilter = " user_account_id=" + userClass.UserAccount.Id + " ";
                    var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                    List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
                    bool isUpdated = false;
                    listBanks.ForEach(x =>
                    {
                        if (x.MaHolderBankId == Convert.ToInt32(userClass.bankID))
                        {
                            isUpdated = true;
                            x.Status = 1;
                            x.UpdatedDate = DateTime.Now;
                            Response maHolderBankResponse = IMaHolderBankService.GetSingle(x.MaHolderBankId);
                            MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
                            Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
                            BanksDef banksDef = (BanksDef)banksDefResponse.Data;
                            GenericService.UpdateData<UserAccountBanks>(x);
                            userCenterStep1.Code = UserPolicyEnum4.S.ToString();
                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Bind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") to MA (" + userClass.UserAccount.AccountNo + ") successful",
                                TableName = "user_account_banks",
                                UpdatedDate = DateTime.Now,
                                UserId = userClass.User.Id,
                                UserAccountId = userClass.UserAccount.Id,
                                RefId = x.ID,
                                RefValue = userClass.UserAccount.AccountNo,
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }
                        }
                        else if (x.Status != 0)
                        {
                            isUpdated = true;
                            x.Status = 0;
                            Response maHolderBankResponse = IMaHolderBankService.GetSingle(x.MaHolderBankId);
                            MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
                            Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
                            BanksDef banksDef = (BanksDef)banksDefResponse.Data;
                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Auto Unbind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") from MA (" + userClass.UserAccount.AccountNo + ") successful",
                                TableName = "user_account_banks",
                                UpdatedDate = DateTime.Now,
                                UserId = userClass.User.Id,
                                UserAccountId = userClass.UserAccount.Id,
                                RefId = x.ID,
                                RefValue = userClass.UserAccount.AccountNo,
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }
                            GenericService.UpdateData<UserAccountBanks>(x);
                            userCenterStep1.Code = UserPolicyEnum4.S.ToString();
                        }
                    });
                    if (!isUpdated)
                    {
                        UserAccountBanks userAccountBanks = new UserAccountBanks
                        {
                            MaHolderBankId = Convert.ToInt32(userClass.bankID),
                            UserAccountId = userClass.UserAccount.Id,
                            CreatedBy = userClass.User.Id,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            Status = 1,
                        };

                        Response maHolderBankResponse = IMaHolderBankService.GetSingle(Convert.ToInt32(userClass.bankID));
                        MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
                        Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
                        BanksDef banksDef = (BanksDef)banksDefResponse.Data;
                        Response responsePostBank = GenericService.PostData<UserAccountBanks>(userAccountBanks);
                        if (responsePostBank.IsSuccess)
                        {
                            userAccountBanks = (UserAccountBanks)responsePostBank.Data;
                            userCenterStep1.Code = UserPolicyEnum4.S.ToString();
                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Bind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") to MA (" + userClass.UserAccount.AccountNo + ") successful",
                                TableName = "user_account_banks",
                                UpdatedDate = DateTime.Now,
                                UserId = userClass.User.Id,
                                UserAccountId = userClass.UserAccount.Id,
                                RefId = userAccountBanks.ID,
                                RefValue = userClass.UserAccount.AccountNo,
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }
                        }
                        else
                        {
                            userCenterStep1.Code = UserPolicyEnum4.E.ToString();
                            response.Message = responsePostBank.Message;
                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Bind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") to MA (" + userClass.UserAccount.AccountNo + ") failed",
                                TableName = "user_account_banks",
                                UpdatedDate = DateTime.Now,
                                UserId = userClass.User.Id,
                                UserAccountId = userClass.UserAccount.Id,
                                RefId = userClass.UserAccount.Id,
                                RefValue = userClass.UserAccount.AccountNo,
                                StatusType = 0
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }
                        }
                    }
                    else
                    {
                        userCenterStep1.Code = UserPolicyEnum4.S.ToString();
                    }

                    //hdnBindBankId.Value = "";
                    //BindBankApproval(userClass.User.Id);

                    //string redirectBack = "";
                    //if (!string.IsNullOrEmpty(Request.QueryString["redirectBack"]))
                    //{
                    //    redirectBack = Request.QueryString["redirectBack"].ToString();
                    //}
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Bank details successfully binded', '" + redirectBack + "?AccountNo=" + userAccount.AccountNo + "');", true);
                }
                else // if Unbind Bank
                {
                    //User sessionUser = (User)HttpContext.Current.Session["user"];
                    //string idString = hdnBindBankId.Value;
                    //Int32 id = Convert.ToInt32(idString);

                    string accountfilter = " user_account_id=" + userClass.UserAccount.Id + " and ma_holder_bank_id='" + userClass.bankID + "' ";
                    var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                    List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
                    UserAccountBanks userAccountBanks = listBanks.FirstOrDefault();
                    userAccountBanks.Status = 0;
                    userAccountBanks.UpdatedDate = DateTime.Now;
                    Response resUpdateBank = GenericService.UpdateData<UserAccountBanks>(userAccountBanks);
                    Response maHolderBankResponse = IMaHolderBankService.GetSingle(userAccountBanks.MaHolderBankId);
                    MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
                    Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
                    BanksDef banksDef = (BanksDef)banksDefResponse.Data;
                    if (resUpdateBank.IsSuccess)
                    {
                        userCenterStep1.Code = UserPolicyEnum4.S.ToString();
                        UserLogMain ulm = new UserLogMain()
                        {
                            Description = "Unbind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") from MA (" + userClass.UserAccount.AccountNo + ") successful",
                            TableName = "user_account_banks",
                            UpdatedDate = DateTime.Now,
                            UserId = userClass.User.Id,
                            UserAccountId = userClass.UserAccount.Id,
                            RefId = userAccountBanks.ID,
                            RefValue = userClass.UserAccount.AccountNo,
                            StatusType = 1
                        };
                        Response responseLog = IUserLogMainService.PostData(ulm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                        }
                    }
                    else
                    {
                        userCenterStep1.Code = UserPolicyEnum4.E.ToString();
                        response.Message = resUpdateBank.Message;
                        UserLogMain ulm = new UserLogMain()
                        {
                            Description = "Unbind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") from MA (" + userClass.UserAccount.AccountNo + ") failed",
                            TableName = "user_account_banks",
                            UpdatedDate = DateTime.Now,
                            UserId = userClass.User.Id,
                            UserAccountId = userClass.UserAccount.Id,
                            RefId = userAccountBanks.ID,
                            RefValue = userClass.UserAccount.AccountNo,
                            StatusType = 0
                        };
                        Response responseLog = IUserLogMainService.PostData(ulm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                        }
                    }

                    //hdnBindBankId.Value = "";
                    //BindBankApproval(userClass.User.Id);

                    //string redirectBack = "";
                    //if (!string.IsNullOrEmpty(Request.QueryString["redirectBack"]))
                    //{
                    //    redirectBack = Request.QueryString["redirectBack"].ToString();
                    //}
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Bank details successfully unbinded', '" + redirectBack + "?AccountNo=" + userAccount.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                }
                response.Data = userCenterStep1;
            }
            return response;
        }

        public static void RefreshOTP(Response response)
        {
            User newUser = (User)response.Data;
            newUser.SmsAttempts = 0;
            newUser.VerifyExpired = 1;
            newUser.VerificationCode = 0;
            Response responseNew2 = IUserService.UpdateData(newUser);
        }

        public static object bindBankDetails(User user, MaHolderReg maHolderReg)
        {
            Response response = new Response();
            response.IsSuccess = true;
            try
            {
                //Bank Details
                //ddlBank.Items.Clear();
                UserCenterStep userCenterStep = new UserCenterStep();
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == maHolderReg.HolderNo.ToString()).FirstOrDefault();
                Response responseBDList = IBanksDefService.GetDataByFilter(" status=1 ", 0, 0, true);

                if (responseBDList.IsSuccess)
                {
                    bankdefs = (List<BanksDef>)responseBDList.Data;
                    userCenterStep.BanksDefs = bankdefs;

                    //ListItem selectItem = new ListItem
                    //{
                    //    Text = "Select Bank",
                    //    Value = "0"
                    //};
                    //ddlBank.Items.Add(selectItem);

                    //foreach (BanksDef x in bankdefs.OrderBy(a => a.Name).ToList())
                    //{
                    //    ListItem listItem = new ListItem
                    //    {
                    //        Text = x.Name,
                    //        Value = x.Id.ToString()
                    //    };
                    //    listItem.Attributes.Add("accountnumlength", x.NoFormat);
                    //    ddlBank.Items.Add(listItem);
                    //}
                }
                //ddlAccountname.Items.Clear();

                else
                {
                    //ddlAccountname.Items.Add(new ListItem(maHolderReg.Name1, maHolderReg.Name1));
                }
                //bankdetail.InnerHtml = "";
                //bankdenied.Visible = false;
                response.Data = userCenterStep;
            }
            catch (Exception ex)
            {

            }
            //BindBankApproval(bankdefs);
            return response;
        }

        public static object BindBankApproval(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            Boolean IsImpersonate = false;
            UserCenterStep userCenterStep = new UserCenterStep();
            if (userClass.Step == 1)
            {
                if (userClass.Condition1)
                {
                    Response responseuserMHBList = IMaHolderBankService.GetDataByFilter(" user_id='" + userClass.User.Id + "' and status not in (8,9) ", 0, 0, true);
                    if (responseuserMHBList.IsSuccess)
                    {
                        List<MaHolderBank> hbs = (List<MaHolderBank>)responseuserMHBList.Data;
                        if (hbs.Count > 0)
                        {
                            userCenterStep.maHolderBanks = hbs;
                            userCenterStep.Flag1 = true;
                            //bankDetailsBindStatus.InnerHtml = "Binded (" + hbs.Count + ")";
                            //bankDetailsBindStatus.Attributes.Add("class", "text-success");
                            StringBuilder stringBuilder = new StringBuilder();
                            //int i = 0;
                            userCenterStep.BankStatus = new List<string>();
                            userCenterStep.BankName = new List<string>();
                            foreach (var hb in hbs)
                            {
                                string bankName = string.Empty;
                                if (userClass.bankdefs.Count != 0)
                                    bankName = (userClass.bankdefs.FirstOrDefault(a => a.Id == hb.BankDefId).Name);
                                string bankStatus = string.Empty;

                                switch (hb.Status)
                                {
                                    case (int)CustomStatus.UserBank_Status.Pending:
                                        bankStatus = CustomStatus.UserBank_Status.Pending.ToString();
                                        break;
                                    case (int)CustomStatus.UserBank_Status.Approved:
                                        bankStatus = CustomStatus.UserBank_Status.Approved.ToString();
                                        break;
                                    case (int)CustomStatus.UserBank_Status.OldApproved:
                                        bankStatus = CustomStatus.UserBank_Status.OldApproved.ToString();
                                        break;
                                    case (int)CustomStatus.UserBank_Status.Rejected:
                                        bankStatus = CustomStatus.UserBank_Status.Rejected.ToString();
                                        break;
                                }
                                userCenterStep.BankStatus.Add(bankStatus);
                                if (bankStatus != "Rejected")
                                {
                                    //userCenterStep.Flags1[i] = true;
                                    string accountfilter = " ma_holder_bank_id=" + hb.Id + " and status=1 ";
                                    var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                                    List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
                                    userCenterStep.BankName.Add(bankName);
                                    userCenterStep.userAccountBanks = listBanks;
                                    //    stringBuilder.Append(@"<tr>

                                    //                <td>" + bankName + @"</td>

                                    //                <td>" + hb.BankAccountNo + @"</td>

                                    //                <td>" + (listBanks.Count == 0 ? "-" : String.Join(", ", userClass.User.UserIdUserAccounts.Where(x => listBanks.Select(y => y.UserAccountId).Contains(x.Id)).Select(x => x.AccountNo).ToArray())) + @"</td>

                                    //                <td><a target='_blank' href='" + hb.Image + @"'>" + image + @"</a></td>
                                    //                <td>" + hb.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                    //                <td>" + (bankdefs.FirstOrDefault(a => a.Id == hb.BankDefId).Status == 0 ? "Inactive bank" : bankStatus) + @"</td>
                                    //                <td>" + (IsImpersonate == true ? "" : "<a href='javascript:;' class='btn btn-sm deleteBank' data-id='" + hb.Id + @"'><i class='fa fa-trash'></i></a>") + @" </td>
                                    //            </tr>");

                                }
                                //else
                                //    userCenterStep.Flags1[i] = false;
                                //i++;
                            }
                            userCenterStep.Code = UserPolicyEnum4.S.ToString();
                            //tbodyUserBankAccounts.InnerHtml = stringBuilder.ToString();
                            if (hbs.Count == 5)
                            {
                                //userCenterStep.Flag2 = true;
                                //divAddBank.Visible = false;
                                //AddBank.Visible = false;
                            }
                            //else
                            //    userCenterStep.Flag2 = false;
                        }
                        else
                        {
                            //userCenterStep.Flag1 = false;
                            //bankDetailsBindStatus.InnerHtml = "Not binded";
                            //bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                        }
                    }
                    else
                    {
                        userCenterStep.Code = UserPolicyEnum4.E.ToString();

                    }

                }
                else
                {
                    if (userClass.User != null)
                    {
                        Response responseBA = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserAccountId), userClass.UserAccount.Id.ToString(), true, 0, 0, true);
                        if (responseBA.IsSuccess)
                        {
                            List<MaHolderBank> maHolderBanks = (List<MaHolderBank>)responseBA.Data;
                            MaHolderBank ba = maHolderBanks.FirstOrDefault();
                            if (ba == null)
                            {
                                //bankDetailsBindStatus.InnerHtml = "Not Binded";
                                //bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                            }
                            if (ba != null)
                            {
                                if (ba.Status == 0)
                                {
                                    //pending
                                    //banknotverified.Visible = true;
                                    //bankDetailsBindStatus.InnerHtml = "Not Binded";
                                    //bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                                }
                                else if (ba.Status == 1)
                                {
                                    //approved
                                    //bankverified.Visible = true;
                                    //bankDetailsBindStatus.InnerHtml = "Binded";
                                    //bankDetailsBindStatus.Attributes.Add("class", "text-success");
                                }
                                else
                                {
                                    //denied
                                    //bankdenied.Visible = true;
                                    //bankDetailsBindStatus.InnerHtml = "Not Binded";
                                    //bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                                }
                            }
                            userCenterStep.maHolderBank = ba;
                        }
                    }
                }
            }
            else
            {

            }

            response.Data = userCenterStep;
            return response;
        }

        public static object DeleteBank(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            Boolean IsImpersonate = false;
            UserCenterStep userCenterStep = new UserCenterStep();
            string BankId = userClass.bankID;

            if (!string.IsNullOrEmpty(BankId))
            {
                User user = userClass.User;
                Response responseHB = IMaHolderBankService.GetSingle(Convert.ToInt32(BankId));
                if (responseHB.IsSuccess)
                {
                    MaHolderBank hb = (MaHolderBank)responseHB.Data;
                    //Check if bank binded to MA.
                    string accountfilter = " ma_holder_bank_id=" + hb.Id + " and status=1 ";
                    var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                    List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
                    if (listBanks.Count == 0)
                    {
                        //Update bank status.
                        hb.Status = 8;
                        hb.UpdatedBy = user.Id;
                        hb.UpdatedDate = DateTime.Now;
                        IMaHolderBankService.UpdateData(hb);
                        UserLogMain ulm = new UserLogMain()
                        {
                            Description = "User has deleted Bank details",
                            TableName = "ma_holder_bank",
                            UpdatedDate = DateTime.Now,
                            UserId = user.Id,
                            RefId = hb.Id,
                            RefValue = hb.AccountName,
                            StatusType = 1
                        };
                        Response responseLog = IUserLogMainService.PostData(ulm);
                        if (responseLog.IsSuccess)
                        {
                            ulm = (UserLogMain)responseLog.Data;
                        }
                        else
                        {
                            //Audit log failed
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                        }
                        userCenterStep.Code = UserPolicyEnum4.S.ToString();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Successfully deleted.', '/UserSettings.aspx?isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                    }
                    else
                    {
                        userCenterStep.Code = UserPolicyEnum4.UA4.ToString();
                        UserAccount uA = user.UserIdUserAccounts.FirstOrDefault(x => x.Id == listBanks.FirstOrDefault().UserAccountId);
                        userCenterStep.UserAccount = uA;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomConfirmationMessage('Confirmation', 'Please unbind the bank before delete.', 'Settings.aspx?AccountNo=" + uA.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')&redirectBack=UserSettings.aspx');", true);
                    }
                }
                else
                {
                    userCenterStep.Code = UserPolicyEnum4.E.ToString();
                }
            }
            response.Data = userCenterStep;
            return response;
        }

        public static object GoogleBinding(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            UserCenterStep userCenterStep = new UserCenterStep();

            try
            {
                if (userClass.Step == 1)
                {

                    //User user = (User)Session["user"];
                    Response responseUser = IUserService.GetSingle(userClass.User.Id);
                    if (responseUser.IsSuccess)
                    {
                        User user = (User)responseUser.Data;

                        Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                        if (responseUSList.IsSuccess)
                        {
                            UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();

                            //imgQRCode.ImageUrl = googleAuthUserSecurity.Url;
                            userCenterStep.Url = googleAuthUserSecurity.Url;
                            //lblManualEntryKey.Text = googleAuthUserSecurity.Value;
                            userCenterStep.ManualEntryKey = googleAuthUserSecurity.Value;
                            //googleAuthRebindButton.Visible = false;
                            userCenterStep.Flag1 = false;
                            //googleAuthBindCode.Visible = true;
                            userCenterStep.Flag2 = true;

                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Requested rebind for Google Authenticator",
                                TableName = "user_securities",
                                UpdatedDate = DateTime.Now,
                                UserId = user.Id,
                                UserAccountId = 0,
                                RefId = googleAuthUserSecurity.Id,
                                RefValue = googleAuthUserSecurity.Value,
                                StatusType = 1
                            };
                            userCenterStep.Code = UserPolicyEnum4.S.ToString();
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                            }


                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "openPopup('googleAuth');", true);
                        }
                        else
                        {
                            userCenterStep.Code = UserPolicyEnum4.E.ToString();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        userCenterStep.Code = UserPolicyEnum4.E.ToString();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
                    }

                }
                else if (userClass.Step == 2)
                {
                    if (userClass.Type == 0)
                    {
                        if (userClass.authPin == "" || userClass.authPin == null) //___________________________________NULL DETECTOR______________________________________________________________________________________
                        {
                            userCenterStep.Code = UserPolicyEnum3.UA6.ToString();
                            response.Data = userCenterStep;
                        }
                        else
                        {
                            try
                            {
                                if (userClass.httpContext.Session["user"] != null)
                                {
                                    User user = (User)userClass.httpContext.Session["user"];
                                    Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                    if (responseUSList.IsSuccess)
                                    {
                                        UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();

                                        if (Int32.TryParse(userClass.authPin.Trim(), out int result))
                                        {

                                            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                            bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, userClass.authPin);
                                            if (isCorrectPIN) //_______________________________________________________________AUTHENTICATION SUCCESS_________________________________________________________________________________
                                            {
                                                UserLogMain ulm = new UserLogMain()
                                                {
                                                    Description = "Two-factor authentication successful - Google Authenticator.",
                                                    TableName = "users",
                                                    UpdatedDate = DateTime.Now,
                                                    UserId = user.Id,
                                                    UserAccountId = 0,
                                                    RefId = user.Id,
                                                    RefValue = user.Username,
                                                    StatusType = 1
                                                };
                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                if (!responseLog.IsSuccess)
                                                {
                                                    //Audit log failed
                                                }

                                                //user.IsSatChecked = 1;
                                                IUserService.UpdateData(user);
                                                Response responseUSList2 = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                                if (responseUSList2.IsSuccess)
                                                {
                                                    List<UserSecurity> userSecurities = ((List<UserSecurity>)responseUSList2.Data).Where(x => x.UserSecurityTypeId != 3).ToList();
                                                    userSecurities.Add(googleAuthUserSecurity);
                                                    user.UserIdUserSecurities = userSecurities;
                                                    userCenterStep.Code = UserPolicyEnum3.S.ToString();

                                                    //RefreshData();
                                                }
                                                else
                                                {

                                                    userCenterStep.Code = UserPolicyEnum.E.ToString();
                                                    userCenterStep.Message = responseUSList.Message;

                                                }


                                            }
                                            else //______________________________________________________________________AUTHENTICATION FAIL_____________________________________________________________________________
                                            {
                                                UserLogMain ulm = new UserLogMain()
                                                {
                                                    Description = "Two-factor authentication failed - Google Authenticator.",
                                                    TableName = "users",
                                                    UpdatedDate = DateTime.Now,
                                                    UserId = user.Id,
                                                    UserAccountId = 0,
                                                    RefId = user.Id,
                                                    RefValue = user.Username,
                                                    StatusType = 0
                                                };
                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                if (!responseLog.IsSuccess)
                                                {
                                                    //Audit log failed
                                                }

                                                userCenterStep.Code = UserPolicyEnum3.UA7.ToString();

                                            }
                                        }
                                        else
                                        {
                                            userCenterStep.Code = UserPolicyEnum3.UA10.ToString();
                                            userCenterStep.Message = responseUSList.Message;

                                        }

                                    }
                                    else
                                    {
                                        userCenterStep.Code = UserPolicyEnum.E.ToString();
                                        userCenterStep.Message = responseUSList.Message;

                                    }
                                }
                                else
                                {
                                    userCenterStep.Code = UserPolicyEnum.E.ToString();
                                    userCenterStep.Message = "Session Expired";

                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog("Exception: " + ex.ToString());
                                response.Message = ex.Message;
                            }
                        }
                    }
                    else if (userClass.Type == 1)
                    {
                        Response responseUser = IUserService.GetSingle(userClass.User.Id);
                        if (responseUser.IsSuccess)
                        {
                            User user = (User)responseUser.Data;
                            if (string.IsNullOrEmpty(userClass.authPin) || string.IsNullOrEmpty(userClass.authPin2))
                            {
                                userCenterStep.Code = UserPolicyEnum3.UA11.ToString();
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Please enter Code and OTP.', '');", true);
                            }
                            else
                            {
                                //if (user.VerificationCode != 0)
                                //{
                                //    if (user.VerifyExpired == 0)
                                //    {
                                //        if (Int32.TryParse(userClass.authPin.Trim(), out int result))
                                //        {
                                //hdnMobilePinRequestedGoogle.Value = "0";
                                userCenterStep.Flag1 = true;
                                Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                if (responseUSList.IsSuccess)
                                {
                                    UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                                    if (user.VerificationCode == Convert.ToInt32(userClass.authPin2.ToString()))
                                    {
                                        TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                        bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, userClass.authPin);
                                        if (isCorrectPIN)
                                        {
                                            //Session["isVerified"] = 1;
                                            if (googleAuthUserSecurity.IsVerified == 0)
                                            {
                                                googleAuthUserSecurity.VerificationPin = Convert.ToInt32(userClass.authPin);
                                                googleAuthUserSecurity.IsVerified = 1;
                                                googleAuthUserSecurity.VerifiedDate = DateTime.Now;
                                                IUserSecurityService.UpdateData(googleAuthUserSecurity);
                                                Response responseMA = ServiceManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
                                                if (responseMA.IsSuccess)
                                                {
                                                    userCenterStep.MaHolderReg = (MaHolderReg)responseMA.Data;
                                                    //BindSettings(maHolderReg);
                                                }
                                                userCenterStep.UserSecurity = googleAuthUserSecurity;
                                                //googleAuthBindedDate.InnerHtml = googleAuthUserSecurity.VerifiedDate.Value.ToString("dd/MM/yyyy");

                                                //SendEmail("User Information Update", "Google Authenticator has been binded on ");
                                                //txtPin.Text = "";
                                                //txtOTP.Text = "";

                                                UserLogMain ulm = new UserLogMain()
                                                {
                                                    Description = "Bind Google Authenticator successful",
                                                    TableName = "user_securities",
                                                    UpdatedDate = DateTime.Now,
                                                    UserId = user.Id,
                                                    UserAccountId = 0,
                                                    RefId = googleAuthUserSecurity.Id,
                                                    RefValue = googleAuthUserSecurity.VerificationPin.Value.ToString(),
                                                    StatusType = 1
                                                };
                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                if (!responseLog.IsSuccess)
                                                {
                                                    //Audit log failed
                                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                }

                                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication binded successfully', '');", true);
                                            }
                                            else
                                            {
                                                googleAuthUserSecurity.VerificationPin = Convert.ToInt32(userClass.authPin);
                                                googleAuthUserSecurity.VerifiedDate = DateTime.Now;
                                                IUserSecurityService.UpdateData(googleAuthUserSecurity);

                                                UserLogMain ulm = new UserLogMain()
                                                {
                                                    Description = "Bind Google Authenticator successful",
                                                    TableName = "user_securities",
                                                    UpdatedDate = DateTime.Now,
                                                    UserId = user.Id,
                                                    UserAccountId = 0,
                                                    RefId = googleAuthUserSecurity.Id,
                                                    RefValue = googleAuthUserSecurity.VerificationPin.Value.ToString(),
                                                    StatusType = 1
                                                };
                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                if (!responseLog.IsSuccess)
                                                {
                                                    //Audit log failed
                                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                }

                                                //SendEmail("User Information Update", "Google Authenticator has been binded on ");
                                                //txtPin.Text = "";
                                                //txtOTP.Text = "";
                                                //googleAuthBindedDate.InnerHtml = googleAuthUserSecurity.VerifiedDate.Value.ToString("dd/MM/yyyy");
                                                userCenterStep.Code = UserPolicyEnum3.S.ToString();
                                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Google authentication binded successfully', '');", true);
                                            }
                                        }
                                        else
                                        {
                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                Description = "Bind Google Authenticator failed",
                                                TableName = "user_securities",
                                                UpdatedDate = DateTime.Now,
                                                UserId = user.Id,
                                                UserAccountId = 0,
                                                RefId = googleAuthUserSecurity.Id,
                                                RefValue = googleAuthUserSecurity.Value,
                                                StatusType = 0
                                            };
                                            Response responseLog = IUserLogMainService.PostData(ulm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                            }
                                            userCenterStep.Code = UserPolicyEnum3.UA7.ToString();
                                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Verification failed.', '');", true);
                                        }
                                    }
                                    else
                                    {
                                        googleAuthUserSecurity.IsVerified = 0;
                                        IUserSecurityService.UpdateData(googleAuthUserSecurity);
                                        userCenterStep.Code = UserPolicyEnum3.UA4.ToString();
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Wrong OTP. Google authentication unbinded. Try again.', '');", true);
                                    }
                                }
                                else
                                {
                                    userCenterStep.Code = UserPolicyEnum3.E.ToString();
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                                }


                                //        }
                                //        else
                                //        {
                                //            userCenterStep.Code = UserPolicyEnum3.UA2.ToString();
                                //            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'OTP must be Numeric.', '');", true);
                                //        }
                                //    }
                                //    else
                                //    {
                                //        userCenterStep.Code = UserPolicyEnum3.UA5.ToString();
                                //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'OTP Expired. Please Request Again.', '');", true);
                                //        //hdnMobilePinRequestedGoogle.Value = "0";
                                //    }
                                //}
                                //else
                                //{
                                //    userCenterStep.Code = UserPolicyEnum3.UA3.ToString();
                                //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('googleAuth');ShowCustomMessage('Alert', 'Please Request OTP.', '');", true);
                                //    //hdnMobilePinRequestedGoogle.Value = "0";
                                //}
                            }
                        }
                        else
                        {
                            userCenterStep.Code = UserPolicyEnum3.E.ToString();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
                        }


                    }
                    else if (userClass.Type == 2)
                    {

                    }
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                userCenterStep.Code = UserPolicyEnum4.E.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('googleAuth');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }

            response.Data = userCenterStep;
            return response;
        }

        public static object ChangePassword(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            UserCenterStep userCenterStep = new UserCenterStep();

            response.Data = userCenterStep;
            return response;
        }

        public static object WatchList(UserClass userClass)
        {
            Response response = new Response();
            response.IsSuccess = true;
            UserCenterStep userCenterStep = new UserCenterStep();

            response.Data = userCenterStep;
            return response;
        }

        public static Thread PinTimerThread;
        public static object RequestMobileVerification(string mobileNo, string idno, string title, Int32 AccountType = 1, User principleUser = null, string relationship = "")
        {
            Response response = new Response();
            response.IsSuccess = true;
            Response responseUList = IAccountOpeningService.GetDataByFilter(" id_no = '" + idno + "' and mobile_no = '" + mobileNo + "' and status in (0,1) ", 0, 0, false);
            if (responseUList.IsSuccess)
            {
                List<AccountOpening> accountOpenings = (List<AccountOpening>)responseUList.Data;
                if (accountOpenings.Count == 0)
                {
                    String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                    String result = "";
                    result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                    AccountOpening accountOpening = new AccountOpening
                    {
                        AccountType = AccountType,
                        PrincipleId = principleUser != null ? principleUser.IdNo : "",
                        PrincipleUserId = principleUser != null ? principleUser.Id : 0,
                        JointRelationship = relationship,
                        IdNo = idno,
                        MobileNo = mobileNo,
                        OtpCode = Convert.ToInt32(mobileVerificationPin),
                        OtpAttempts = 1,
                        IsOtpRequested = 1,
                        IsOtpExpired = 0,
                        Status = 0
                    };
                    Response responsePost = IAccountOpeningService.PostData(accountOpening);
                    if (responsePost.IsSuccess)
                    {
                        accountOpening = (AccountOpening)responsePost.Data;
                        PinTimerThread = new Thread(() => PinTimerAO(accountOpening));
                        PinTimerThread.Start();
                        response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                        response.Data = accountOpening;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responsePost.Message;
                    }
                }
                else if (accountOpenings.Count > 0)
                {
                    AccountOpening accountOpening = new AccountOpening();
                    if (accountOpenings.FirstOrDefault(x => x.AccountType == AccountType) != null)
                    {
                        accountOpening = accountOpenings.FirstOrDefault(x => x.AccountType == AccountType);
                    }
                    else
                    {
                        accountOpening = accountOpenings.FirstOrDefault();
                        if (accountOpening.AccountType != AccountType)
                        {
                            accountOpening.Id = 0;
                            accountOpening.AccountType = AccountType;
                            accountOpening.IsOtpExpired = 1;
                            accountOpening.IsMobileVerified = 0;
                            accountOpening.Email = "";
                            accountOpening.IsEmailVerified = 0;
                            accountOpening.IsIdentityVerified = 0;
                            accountOpening.IsDocumentsVerified = 0;
                            accountOpening.ProcessStatus = 0;
                            accountOpening.AccountType = AccountType;
                            Response responsePost = IAccountOpeningService.PostData(accountOpening);
                            if (responsePost.IsSuccess)
                            {
                                accountOpening = (AccountOpening)responsePost.Data;
                            }
                            else
                            {
                                return responsePost;
                            }
                        }
                    }
                    if (accountOpening.ProcessStatus != (int)AOProcessStatus.Incomplete && accountOpening.ProcessStatus != (int)AOProcessStatus.Completed && accountOpening.ProcessStatus != (int)AOProcessStatus.IDVerificationFailed && accountOpening.ProcessStatus != (int)AOProcessStatus.UnclearDocsRequested && accountOpening.AccountType == AccountType)
                    {
                        response.IsSuccess = false;
                        response.Message = "This ID number has already submitted.";
                        return response;
                    }

                    if (accountOpening.IsOtpExpired == 1)
                    {
                        String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                        String result = "";

                        result = SMSService.SendSMS(mobileNo, mobileVerificationPin, title);
                        accountOpening.AccountType = AccountType;
                        accountOpening.OtpCode = Convert.ToInt32(mobileVerificationPin);
                        accountOpening.IsOtpExpired = 0;
                        accountOpening.IsMobileVerified = 0;
                        accountOpening.OtpAttempts = accountOpening.OtpAttempts + 1;

                        PinTimerThread = new Thread(() => PinTimerAO(accountOpening));
                        PinTimerThread.Start();
                        response.IsSuccess = true;
                        response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                        response.Data = accountOpening;
                        Response responsePost = IAccountOpeningService.UpdateData(accountOpening);
                        if (responsePost.IsSuccess)
                        {
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = responsePost.Message;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = "already sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + (SMSExpirationTimeForAOInSeconds - currentSecAO);
                        if (PinTimerThread == null || !PinTimerThread.IsAlive)
                        {
                            accountOpening.IsOtpExpired = 1;
                            accountOpening.IsMobileVerified = 0;
                            accountOpening.AccountType = AccountType;
                            Response responsePost = IAccountOpeningService.UpdateData(accountOpening);
                            if (responsePost.IsSuccess)
                            {
                                RequestMobileVerification(mobileNo, idno, title);
                                response.Message = "sent," + "*******" + mobileNo.Substring(mobileNo.Length - 3) + "," + SMSExpirationTimeForAOInSeconds;
                                response.Data = accountOpening;
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = responsePost.Message;
                            }
                        }
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Multiple records";
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = responseUList.Message;
            }
            return response;
        }
        public static int currentSecAO = 0;
        public static void PinTimerAO(AccountOpening accountOpening)
        {
            try
            {
                while (currentSecAO <= SMSExpirationTimeForAOInSeconds)
                {
                    if (currentSecAO == SMSExpirationTimeForAOInSeconds)
                    {
                        Response responseUser = IAccountOpeningService.GetSingle(accountOpening.Id);
                        accountOpening = (AccountOpening)responseUser.Data;
                        accountOpening.OtpCode = 0;
                        accountOpening.IsOtpExpired = 1;
                        IAccountOpeningService.UpdateData(accountOpening);
                        currentSecAO = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecAO++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserPolicy PinTimerAO: " + ex.Message);
            }
        }
    }



    public static class UserPolicyEnumExtensions
    {
        public static string ToDescriptionString(this UserPolicyEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }

    public static class UserPolicyEnum2Extensions
    {
        public static string ToDescriptionString(this UserPolicyEnum2 val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }

    public static class UserPolicyEnum3Extensions
    {
        public static string ToDescriptionString(this UserPolicyEnum3 val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }

    public static class UserPolicyEnum4Extensions
    {
        public static string ToDescriptionString(this UserPolicyEnum4 val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
