﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class Settings : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else
            {
                Response response = ServicesManager.isProcessedMonthlyCheck(101);
                if (response.IsSuccess)
                {
                    List<WS_Process> processList = (List<WS_Process>)response.Data;
                    if (processList.Count > 0)
                    {
                        if (processList.FirstOrDefault().status == 0)
                        {
                            lastUpdatedMessage.InnerHtml = processList.FirstOrDefault().message + " on " + processList.FirstOrDefault().processed_date.ToString("dd/MM/yyyy HH:mm:ss");
                        }
                        else if (processList.FirstOrDefault().status == 1)
                        {
                            lastUpdatedMessage.InnerHtml = processList.FirstOrDefault().message + " on " + processList.FirstOrDefault().processed_date.ToString("dd/MM/yyyy HH:mm:ss"); ;

                        }
                    }
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(User obj)
        {
            Response response = new Response();
            var hasUpperChar = obj.TransPwd.ToString().Any(char.IsUpper);
            var hasLowerChar = obj.TransPwd.ToString().Any(char.IsLower);
            string message = "";
            bool isValid = true;

            if (obj.Password == "" || obj.TransPwd == "" || obj.MobileNumber == "")
            {
                message += "Please enter your password.<br/>";
                isValid = false;
            }
            if (obj.TransPwd != obj.MobileNumber)
            {
                message += "New Password and Confirm New Password must be the same.<br/>";
                isValid = false;
            }
            if (obj.TransPwd.Length < 8 || obj.TransPwd.Length > 16)
            {
                message += "New Password length must between 8 to 16 characters.<br/>";
                isValid = false;
            }
            if (!obj.TransPwd.ToString().Any(char.IsNumber))
            {
                message += "New Password must contain at least one number.<br/>";
                isValid = false;
            }
            if (hasUpperChar == false)
            {
                message += "New Password must contain at least one uppercase alphabet letter.<br/>";
                isValid = false;
            }
            if (hasLowerChar == false)
            {
                message += "New Password must contain at least one lowercase alphabet letter.<br/>";
                isValid = false;
            }

            Int32 LoginUserId = 0;
            if (HttpContext.Current.Session["admin"] != null)
            {
                User sessionUser = (User)HttpContext.Current.Session["admin"];
                LoginUserId = sessionUser.Id;
            }
            try
            {
                if (isValid == true)
                {
                    Response responseGet = IUserService.GetSingle(LoginUserId);
                    if (responseGet.IsSuccess)
                    {
                        User user = (User)responseGet.Data;
                        if (user.Password == CustomEncryptorDecryptor.EncryptPassword(obj.Password))
                        {
                            if (CustomEncryptorDecryptor.EncryptPassword(obj.TransPwd) != user.Password)
                            {
                                user.ModifiedDate = DateTime.Now;
                                user.ModifiedBy = LoginUserId;
                                user.Password = CustomEncryptorDecryptor.EncryptPassword(obj.TransPwd);
                                IUserService.UpdateData(user);
                                response.IsSuccess = true;
                                response.Message = "Success";
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "This password has been used before, please choose another password in order to proceed";
                                return response;
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Wrong existing password";
                            return response;
                        }
                    }
                    else
                    {
                        return responseGet;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = message;
                    return response;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Change password action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
            return response;
        }
    }
}