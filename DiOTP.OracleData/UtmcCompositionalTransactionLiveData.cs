﻿using DiOTP.Utility;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class UtmcCompositionalTransactionLiveData
    {
        static DataManager DB = new DataManager();

        public static List<UtmcCompositionalTransaction> GetDataByHolderNo(string holderNo)
        {
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();

            try
            {
                string query = "select  A.FUND_ID, "
                                    + " B.ID_NO_2 AS EPF_NO, "
                                    + " A.HOLDER_NO, "
                                    + " A.TRANS_DT, "
                                    + " A.TRANS_NO, "
                                    + " A.TRANS_TYPE, "
                                    + " A.ACC_TYPE, "
                                    + " A.TRANS_UNITS, "
                                    + " A.TRANS_AMT,  "
                                    + " A.FEE_AMT, "
                                    + " A.GST_AMT,  "
                                    + " A.UNIT_VALUE, "
                                    + " (A.GST_AMT+ A.UNIT_VALUE) AS GAIN, "
                                    + " A.TRANS_DT AS SETTLEMENT_DT, "
                                    + "A.HLDR_AVE_COST, "
                                     + " A.SORT_SEQ, "
                                     + " A.TRANS_PR "
                                    + " from UTS.HOLDER_LEDGER A "
                                    + " INNER JOIN UTS.HOLDER_REG B "
                                    + " ON A.HOLDER_NO  = B.HOLDER_NO "
                                    + " where "
                                    + " A.HOLDER_NO in (" + holderNo + ") "
                                    + " and instr(A.TRANS_NO,'X') = 0 "
                                    + " and A.TRANS_NO NOT IN(select REPLACE(TRANS_NO, 'X', '') as TRANS_NO from UTS.HOLDER_LEDGER where HOLDER_NO in ("+ holderNo + ") and instr(TRANS_NO, 'X') > 0) "
                                    + "  and A.fund_id in ('01', '02', '03', '04', '05', '06', '07', '08', '09') "
                                    + " ORDER BY A.TRANS_TYPE, ACC_TYPE, A.FUND_ID, A.TRANS_DT ASC";
                //--(SELECT RCPT_DT from UTS.UTSTRANS WHERE TRANS_NO = A.TRANS_NO AND TRANS_DT = A.TRANS_DT) AS SETTLEMENT_DT,
                //+ " C.RCPT_DT AS SETTLEMENT_DT, "
                //+ " INNER JOIN UTS.UTSTRANS C ON C.TRANS_NO = A.TRANS_NO AND C.TRANS_DT = A.TRANS_DT "
                objs = GenerateData(query);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return objs;
        }

        public static List<UtmcCompositionalTransaction> GetDataByHolderNo(string holderNo, string startDate, string endDate)
        {
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();

            try
            {
                string query = "select  A.FUND_ID, "
                                    + " B.ID_NO_2 AS EPF_NO, "
                                    + " A.HOLDER_NO, "
                                    + " A.TRANS_DT, "
                                    + " A.TRANS_NO, "
                                    + " A.TRANS_TYPE, "
                                    + " A.ACC_TYPE, "
                                    + " A.TRANS_UNITS, "
                                    + " A.TRANS_AMT,  "
                                    + " A.FEE_AMT, "
                                    + " A.GST_AMT,  "
                                    + " A.UNIT_VALUE, "
                                    + " (A.GST_AMT+ A.UNIT_VALUE) AS GAIN, "
                                    + " A.TRANS_DT AS SETTLEMENT_DT, "
                                    + "A.HLDR_AVE_COST, "
                                     + " A.SORT_SEQ, "
                                     + " A.TRANS_PR "
                                    + " from UTS.HOLDER_LEDGER A "
                                    + " INNER JOIN UTS.HOLDER_REG B "
                                    + " ON A.HOLDER_NO  = B.HOLDER_NO "
                                    + " where "
                                    + " A.HOLDER_NO in (" + holderNo + ") and "
                                    + " A.TRANS_DT >= to_date('"+ startDate + "', 'YYYY/MM/DD') and "
                                    + " A.TRANS_DT <= to_date('" + endDate + "', 'YYYY/MM/DD') "
                                    + " and instr(A.TRANS_NO,'X') = 0 "
                                    + " and A.TRANS_NO NOT IN(select REPLACE(TRANS_NO, 'X', '') as TRANS_NO from UTS.HOLDER_LEDGER where HOLDER_NO in (" + holderNo + ") and instr(TRANS_NO, 'X') > 0) "
                                    + "  and A.fund_id in ('01', '02', '03', '04', '05', '06', '07', '08', '09') "
                                    + " ORDER BY A.TRANS_TYPE, ACC_TYPE, A.FUND_ID, A.TRANS_DT ASC";
                //--(SELECT RCPT_DT from UTS.UTSTRANS WHERE TRANS_NO = A.TRANS_NO AND TRANS_DT = A.TRANS_DT) AS SETTLEMENT_DT,
                //+ " C.RCPT_DT AS SETTLEMENT_DT, "
                //+ " INNER JOIN UTS.UTSTRANS C ON C.TRANS_NO = A.TRANS_NO AND C.TRANS_DT = A.TRANS_DT "
                objs = GenerateData(query);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return objs;
        }

        public static List<UtmcCompositionalTransaction> GenerateData(string Query)
        {
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            DataTable ds = new DataTable();

            string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"].ToString();

            string ConnectionString = DB.GetOracleConnection();
            try
            {
                string mysql = Query;
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                conn.Open();
                    using (OracleCommand cmd = new OracleCommand(mysql, conn))
                    {
                        using (OracleDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                decimal Cost_RM = 0.00M;
                                string IPD_Fund_Code = rdr[0].ToString();
                                string EPF_No = rdr[1].ToString();
                                string IPD_Member_Acc_No = rdr[2].ToString();
                                DateTime dt = Convert.ToDateTime(rdr[3].ToString());

                                var startDate = new DateTime(dt.Year, dt.Month, 1);
                                var endDate = startDate.AddMonths(1).AddDays(-1);
                                string LastDay = endDate.ToString("yyyy-MM-dd");

                                string Effective_Date = dt.ToString("yyyy-MM-dd");
                                //string Date_Of_Transaction = rdr[7].ToString();                                 
                                string Settlementdate = rdr[13].ToString();
                                DateTime Settlementdatetime = new DateTime();
                                if (Settlementdate == "")
                                {
                                    Settlementdate = Effective_Date;
                                    Settlementdatetime = dt;
                                }
                                else
                                {
                                    DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                                    Settlementdate = dt1.ToString("yyyy-MM-dd");
                                    Settlementdatetime = dt1;
                                }
                                // DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                                // string Settlementdate = dt1.ToString("yyyy-MM-dd");

                                string IPD_Unique_Transaction_ID = rdr[4].ToString();
                                string Transaction_Code = rdr[5].ToString().Trim();
                                string AccountType = rdr[6].ToString().Trim();
                                string Reversed_Transaction_ID = "";
                                decimal AverageCost = Convert.ToDecimal((rdr.IsDBNull(14) ? "0" : rdr[14].ToString()));

                                string Sort_SEQ = rdr[15].ToString();
                                string TransPR = rdr[16].ToString();
                                decimal Units = Math.Abs(Convert.ToDecimal(rdr[7].ToString()));
                                decimal Gross_Amount_RM = Convert.ToDecimal(rdr[8].ToString());
                                decimal Net_Amount_RM = Convert.ToDecimal((rdr[11].ToString() == "" ? "0" : rdr[11].ToString()));
                                decimal Fees_RM = Convert.ToDecimal((rdr[9].ToString() == "" ? "0" : rdr[9].ToString()));
                                decimal GST_RM = Convert.ToDecimal((rdr[10].ToString() == "" ? "0" : rdr[10].ToString()));
                                decimal Proceeds_RM = 0.00M;
                                decimal Realised_Gain_Loss = 0.00M;
                                AverageCost = decimal.Round(AverageCost, 4);
                                char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];

                                if (Check_reversed == 'X')
                                {
                                    if (Transaction_Code == "SA")
                                    {

                                        if (AccountType == "SW")
                                        {
                                            Transaction_Code = "XI";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                        }
                                        else
                                        {
                                            Transaction_Code = "XS";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                        }



                                    }
                                    else if (Transaction_Code == "RD")
                                    {

                                        if (AccountType == "SW")
                                        {
                                            Transaction_Code = "XO";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                        }
                                        else
                                        {
                                            Transaction_Code = "XR";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                        }
                                    }

                                    else if (Transaction_Code == "DD")
                                    {

                                        if (Units == 0)
                                        {
                                            Transaction_Code = "XD";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                        }
                                        else
                                        {
                                            Transaction_Code = "XV";
                                            Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                        }

                                    }
                                }

                                if ((Transaction_Code == "SA") || (Transaction_Code == "DD"))
                                {

                                }
                                if ((Transaction_Code == "SA") && (AccountType == "SW"))
                                {
                                    Transaction_Code = "SI";

                                }
                                if ((Transaction_Code == "RD") && (AccountType == "SW"))
                                {
                                    Transaction_Code = "SO";
                                    //Gross_Amount_RM = 0.00M;                  // as per request SO
                                    Net_Amount_RM = 0.00M;

                                }

                                if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO") || (Transaction_Code == "XR") || (Transaction_Code == "XD") || (Transaction_Code == "XO") || (Transaction_Code == "XV"))
                                {
                                    Proceeds_RM = Gross_Amount_RM;
                                    // Cost_RM = Units * AverageCost;
                                    Realised_Gain_Loss = Proceeds_RM - Cost_RM;
                                    // Cost_RM = decimal.Round(Cost_RM, 2);
                                    Proceeds_RM = decimal.Round(Proceeds_RM, 2);
                                    Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);


                                }

                                //filter by sign requirement 

                                if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO")
                                    || (Transaction_Code == "XS") || (Transaction_Code == "XC") || (Transaction_Code == "XD")
                                    || (Transaction_Code == "XV") || (Transaction_Code == "UX") || (Transaction_Code == "XI")
                                    || (Transaction_Code == "RO") || (Transaction_Code == "RI") || (Transaction_Code == "HO") || (Transaction_Code == "XO"))
                                {

                                    if (!(Net_Amount_RM == 0))
                                    {
                                        Net_Amount_RM = -Net_Amount_RM;

                                    }
                                    if (!(Fees_RM == 0))
                                    {
                                        Fees_RM = -Fees_RM;

                                    }
                                    if (!(GST_RM == 0))
                                    {
                                        GST_RM = -GST_RM;

                                    }
                                    Gross_Amount_RM = -Gross_Amount_RM;

                                    if (!(Transaction_Code == "XO"))
                                    {
                                        Units = -Units;
                                    }
                                    if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO"))
                                    {
                                        Proceeds_RM = -Proceeds_RM;
                                    }


                                }
                                ////////// hard code  adjustment Start

                                if (Transaction_Code == "SA")
                                {
                                    Transaction_Code = "NS";
                                    // final  aaa ---
                                    Cost_RM = Gross_Amount_RM;
                                    // final  aaa ---

                                }
                                if (Transaction_Code == "SI")
                                {
                                    // final  aaa ---
                                    Cost_RM = Gross_Amount_RM;
                                    // final  aaa ---


                                }
                                if (Transaction_Code == "DD")
                                {
                                    Transaction_Code = "DV";
                                    Realised_Gain_Loss = 0.00M;
                                }




                                if (Transaction_Code == "XD")
                                {
                                    Realised_Gain_Loss = 0.00M;
                                    // Net_Amount_RM = Gross_Amount_RM;
                                    Proceeds_RM = 0.00M;

                                }

                                if (Transaction_Code == "XV")
                                {
                                    Realised_Gain_Loss = 0.00M;

                                    // Net_Amount_RM = Gross_Amount_RM;
                                    Proceeds_RM = 0.00M;
                                }




                                if (Transaction_Code == "RD")
                                {
                                    Transaction_Code = "RE";
                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate);


                                    Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);

                                    // final  aaa ---
                                    Gross_Amount_RM = Proceeds_RM;
                                    Net_Amount_RM = Proceeds_RM;
                                    // final  aaa ---

                                    Cost_RM = -Cost_RM;
                                }
                                if (Transaction_Code == "TR")
                                {
                                    Transaction_Code = "TO";
                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate);
                                    Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                    // final  aaa ---
                                    Gross_Amount_RM = Proceeds_RM;
                                    Net_Amount_RM = Proceeds_RM;
                                    // final  aaa ---
                                }


                                if (Transaction_Code == "SO" || Transaction_Code == "XO")
                                {
                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate);
                                    Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                                    // final  aaa ---
                                    Gross_Amount_RM = Proceeds_RM;
                                    Net_Amount_RM = Proceeds_RM;
                                    // final  aaa ---
                                    Cost_RM = -Cost_RM;
                                }

                                if (Transaction_Code == "XO")
                                {
                                    Cost_RM = -Cost_RM;
                                    Realised_Gain_Loss = -Realised_Gain_Loss;
                                    //Units = Units;

                                }

                                // FILTERED 0 UNIT DIVIDEND 
                                Boolean bolFILTER = true;
                                if (Transaction_Code == "DV")
                                {
                                    if (Units == 0)
                                    {
                                        bolFILTER = false;
                                    }

                                }

                                if (Transaction_Code == "XS")

                                {
                                    Cost_RM = Gross_Amount_RM;

                                }


                                if (Transaction_Code == "XI" || Transaction_Code == "XR")
                                {
                                    Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ, endDate);

                                    if (Transaction_Code == "XI")
                                    {
                                        Cost_RM = Gross_Amount_RM;
                                    }

                                    if (Transaction_Code == "XR")
                                    {
                                        Realised_Gain_Loss = -(Proceeds_RM - Cost_RM);
                                        Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);

                                    }

                                }

                                if (Transaction_Code == "DV" || Transaction_Code == "XV")
                                {
                                    Net_Amount_RM = Gross_Amount_RM;
                                }

                                //Query = @"INSERT INTO utmc_compositional_transactions 
                                //(EPF_IPD_Code, IPD_Fund_Code, Member_EPF_No, IPD_Member_Acc_No, Effective_Date, Date_Of_Transaction, 
                                //Date_Of_Settlement, IPD_Unique_Transaction_ID, Transaction_Code, Reversed_Transaction_ID, Units, 
                                //Gross_Amount_RM, Net_Amount_RM, Fees_RM, GST_RM, Cost_RM, Proceeds_RM, Realised_Gain_Loss,report_date) VALUES "
                                //+ "('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + EPF_No + "','" + IPD_Member_Acc_No + "','" + LastDay + "','" + Effective_Date + @"',
                                //'" + Settlementdate + "','" + IPD_Unique_Transaction_ID + "','" + Transaction_Code + "','" + Reversed_Transaction_ID + "','" + Units + @"',
                                //'" + Gross_Amount_RM + "','" + Net_Amount_RM + "','" + Fees_RM + "','" + GST_RM + "','" + Cost_RM + "','" + Proceeds_RM + "','" + Realised_Gain_Loss + "','" + End_date + "')";
                                Transaction_Code = rdr[5].ToString().Trim();

                                if (bolFILTER)
                                {
                                    objs.Add(new UtmcCompositionalTransaction()
                                    {
                                        EpfIpdCode = ConfigurationManager.AppSettings["IPD_CODE"].ToString(),
                                        IpdFundCode = IPD_Fund_Code,
                                        MemberEpfNo = EPF_No,
                                        IpdMemberAccNo = IPD_Member_Acc_No,
                                        EffectiveDate = endDate,
                                        DateOfTransaction = dt,
                                        DateOfSettlement = Settlementdatetime,
                                        IpdUniqueTransactionId = IPD_Unique_Transaction_ID,
                                        TransactionCode = Transaction_Code,
                                        ReversedTransactionId = Reversed_Transaction_ID,
                                        Units = Units,
                                        GrossAmountRm = Gross_Amount_RM,
                                        NetAmountRm = Net_Amount_RM,
                                        FeesRm = Fees_RM,
                                        GstRm = GST_RM,
                                        CostRm = Cost_RM,
                                        ProceedsRm = Proceeds_RM,
                                        RealisedGainLoss = Realised_Gain_Loss,
                                        ReportDate = endDate,
                                        TransPR = TransPR != null && TransPR != "" ? Convert.ToDecimal(TransPR) : 0.0M,
                                        AverageCost = AverageCost
                                    });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
            }
            return objs;
        }

        protected static decimal Get_Redumption_Cost_UTMC10(string Holder_No, string Fund_ID, string Sort_Seq, DateTime End_date)
        {

            string Sqlconnstring = DB.GetOracleConnection();
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;

                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                   + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                   + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG, SORT_SEQ"
                                   + " FROM UTS.HOLDER_LEDGER "
                                   + " WHERE "
                                   + " HOLDER_NO='" + Holder_No + "' AND "
                                   + " FUND_ID='" + Fund_ID + "' AND "
                                   + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                   // + " trans_type IN ('SA','RD','DD','TR') "
                                   + " AND  TRANS_DT <= to_date('" + End_date.ToString("yyyy/MM/dd") + "', 'yyyy/mm/dd') "
                                   + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";



                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }
                            Array.Resize(ref numb, Count);

                            Count = 0;
                            string Trans_String;

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }
                                            else
                                            {
                                                Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                        }
                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {



                                            Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                            Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);


                                            /* FINAL AAAA OLD COST
                                            UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);
                                            /* FINAL AAAA OLD COST */

                                            UnitCostRDfinal = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                                * UnitCostRD);


                                            UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);


                                            if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                            {



                                                Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                           * Total_Redemption_cost);
                                                Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                          * Total_Redemption_cost);



                                                Book_value = decimal.Round(Book_value, 2);
                                                Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                            }
                                            else
                                            {
                                                Actual_transfer_value = 0.0000M;
                                                Book_value = 0.0000M;
                                                Unit_Holding_value = 0.0000M;
                                            }

                                            // if (Trans_Type == "TR")
                                            // {
                                            //   UnitCostRDfinal = 0.00M;
                                            // }

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if (Sort_Seq == row[10].ToString())
                                            {
                                                break;
                                            }

                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString(), End_date.ToString("dd/MM/yyyy"));
                                }

                            }

                            return UnitCostRDfinal;
                            /*
                      
                            ////////////////////////////////////////////////////////////////////////////////////////////// hard code  adjustment end

                            Cost_update_for_UTMC10(Holder_No, Fund_ID, UnitCostRDfinal, Realised_Gain_Loss);
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Actual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();
                            */

                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date.ToString("dd/MM/yyyy"));
                return 0.00M;

            }
        }
    }
}
