﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddCommStructureMapping : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] != null)
            {
                User sessionUser = (User)Session["admin"];
                string idString = Request.QueryString["id"];

                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    List<UtmcFundInformation> UtmcFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                    ddlFundList.DataSource = UtmcFundInformations;
                    ddlFundList.DataTextField = "FundName";
                    ddlFundList.DataValueField = "id";
                    ddlFundList.DataBind();
                    ddlFundList.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                }

                string queryCSettingsReg = (@" SELECT * FROM commission_settings where is_active=1 ");
                Response responseCSettingsList = GenericService.GetDataByQuery(queryCSettingsReg, 0, 0, false, null, false, null, false);
                if (responseCSettingsList.IsSuccess)
                {
                    var cdsDyn = responseCSettingsList.Data;
                    var responseCDJSON = JsonConvert.SerializeObject(cdsDyn);
                    List<DiOTP.Utility.CommissionSettings> cds = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommissionSettings>>(responseCDJSON);
                    ddlCommSettings.DataSource = cds;
                    ddlCommSettings.DataTextField = "code";
                    ddlCommSettings.DataValueField = "id";
                    ddlCommSettings.DataBind();
                    ddlCommSettings.Items.Insert(0, new ListItem { Text = "Select", Value = "" });
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseCSettingsList.Message + "\");", true);
                }


                StringBuilder filter = new StringBuilder();
                string mainQ = (@"SELECT csm.*, cst.code as commission_setting_name, ufi.Fund_Name as fund_name FROM ");
                StringBuilder countFilter = new StringBuilder();
                filter.Append(@" comm_structure_mappings csm
                                join utmc_fund_information ufi on ufi.id = csm.fund_id 
                                join commission_settings cst on csm.commission_setting_id = cst.id
                                where csm.id='" + idString + "' ");
                Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", 0, 0, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var UsersDyn = responseUList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.CommStructMapDetail> commissionStructureDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommStructMapDetail>>(responseJSON);
                    if (commissionStructureDetails.Count == 1)
                    {
                        CommStructMapDetail commStructMapDetail = commissionStructureDetails.First();
                        Id.Value = commStructMapDetail.id.ToString();
                        ddlFundList.SelectedValue = commStructMapDetail.fund_id.ToString();
                        ddlCommSettings.SelectedValue = commStructMapDetail.commission_setting_id.ToString();
                    }
                }


            }
        }
    }
}