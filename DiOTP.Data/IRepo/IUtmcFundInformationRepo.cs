using DiOTP.Utility.Helper;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DiOTP.Data.IRepo
{
    public interface IUtmcFundInformationRepo : IDefaultInterface<UtmcFundInformation>
    {
    }
}
