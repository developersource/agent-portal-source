﻿using DiOTP.Utility.CustomAttributes;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiOTP.Utility.Helper
{
    public static class CustomValidator
    {
        public static bool IsValidEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                bool isValid = Regex.Match(email,
                   @"^[_a-z0-9-]+[._a-z0-9-]*@[_a-z0-9-]+(\.[a-z]{2,})+(.)?([a-z]{2,})?$").Success;
                if (isValid)
                    return true;
                else
                    return false;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static Response IsValid<T>(this T obj)
        {
            Response response = new Response();
            try
            {
                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
                var objectProperties = typeof(T).GetProperties(flags).Where(y => y.PropertyType.Namespace == "System").ToList();
                objectProperties.ForEach(prop =>
                {
                    RequiredAttribute RequiredAttribute = prop.GetCustomAttributes(typeof(RequiredAttribute), true).FirstOrDefault() as RequiredAttribute;
                    LengthOrValueAttribute LengthOrValueAttribute = prop.GetCustomAttributes(typeof(LengthOrValueAttribute), true).FirstOrDefault() as LengthOrValueAttribute;
                    EmailAttribute EmailAttribute = prop.GetCustomAttributes(typeof(EmailAttribute), true).FirstOrDefault() as EmailAttribute;
                    var value = prop.GetValue(obj);
                    if(RequiredAttribute != null)
                    {
                        if (value != null)
                        {
                            if (prop.PropertyType.Name == typeof(String).Name)
                            {
                                if (value.ToString().Trim() == "")
                                {
                                    response.IsSuccess = false;
                                    response.Message = prop.Name + " Required";
                                }
                            }
                            else if (prop.PropertyType.Name == typeof(Decimal).Name || prop.PropertyType.Name == typeof(int).Name || prop.PropertyType.Name == typeof(Int32).Name)
                            {
                                if (Convert.ToInt32(value.ToString()) == 0)
                                {
                                    response.IsSuccess = false;
                                    response.Message = prop.Name + " Required";
                                }
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = prop.Name + " Required";
                        }
                    }
                    if(LengthOrValueAttribute != null)
                    {
                        if (prop.PropertyType.Name == typeof(String).Name)
                        {
                            if (value.ToString().Trim().Length < LengthOrValueAttribute.Min)
                            {
                                response.IsSuccess = false;
                                response.Message = prop.Name + " Required min " + LengthOrValueAttribute.Min;
                            }
                            if (value.ToString().Trim().Length > LengthOrValueAttribute.Max)
                            {
                                response.IsSuccess = false;
                                response.Message = prop.Name + " max " + LengthOrValueAttribute.Max;
                            }
                        }
                        else if (prop.PropertyType.Name == typeof(Decimal).Name || prop.PropertyType.Name == typeof(int).Name || prop.PropertyType.Name == typeof(Int32).Name)
                        {
                            if (Convert.ToInt32(value.ToString()) < LengthOrValueAttribute.Min)
                            {
                                response.IsSuccess = false;
                                response.Message = prop.Name + " Required min " + LengthOrValueAttribute.Min;
                            }
                            if (Convert.ToInt32(value.ToString()) > LengthOrValueAttribute.Max)
                            {
                                response.IsSuccess = false;
                                response.Message = prop.Name + " max " + LengthOrValueAttribute.Max;
                            }
                        }
                    }
                    if(EmailAttribute != null)
                    {
                        bool isValid = Regex.Match(value.ToString(),
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$").Success;
                        if (!isValid)
                        {
                            response.IsSuccess = false;
                            response.Message = prop.Name+ " invalid";
                        }
                    }
                });
                if (response.IsSuccess)
                {
                    response.Data = obj;
                }
                
                return response;
            }
            catch (FormatException)
            {
                response.IsSuccess = false;
            }
            return response;
        }

    }
}
