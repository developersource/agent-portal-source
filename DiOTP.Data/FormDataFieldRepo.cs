using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class FormDataFieldRepo : IFormDataFieldRepo
    {
        //IFormTypeRepo IFormTypeRepo = new FormTypeRepo();
        IFormDataFieldOptionRepo IFormDataFieldOptionRepo = new FormDataFieldOptionRepo();
        IFormDataFieldValueRepo IFormDataFieldValueRepo = new FormDataFieldValueRepo();
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(FormDataField obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FormDataField>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FormDataField> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FormDataField>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FormDataField obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FormDataField>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FormDataField> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FormDataField>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FormDataField DeleteData(Int32 Id)
        {
            Response responseFDF = GetSingle(Id);
            if (responseFDF.IsSuccess)
            {
                FormDataField obj = (FormDataField)responseFDF.Data;
                try
                {
                    string query = obj.ObjectToQuery<FormDataField>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseFDF = GetSingle(Id);
                    if (responseFDF.IsSuccess)
                    {
                        FormDataField obj = (FormDataField)responseFDF.Data;
                        query += obj.ObjectToQuery<FormDataField>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FormDataField obj = new FormDataField();
            try
            {
                string query = "select * from form_data_fields where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new FormDataField
                       {
                           Id = x.Field<Int32>("id"),
                           FormTypeId = x.Field<Int32>("form_type_id"),
                           Name = x.Field<String>("name"),
                           FieldId = x.Field<String>("field_id"),
                           FieldSet = x.Field<Int32>("field_set"),
                           FieldType = x.Field<String>("field_type"),
                           Status = x.Field<Int32>("status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();

                string propertyName = nameof(FormDataFieldOption.FormDataFieldId);
                Response responseFDFOList = IFormDataFieldOptionRepo.GetDataByPropertyName(propertyName, obj.Id.ToString(), true, 0, 0, false);
                if (responseFDFOList.IsSuccess)
                {
                    obj.FormDataFieldIdFormDataFieldOptions = (List<FormDataFieldOption>)responseFDFOList.Data;
                }
                Response responseFDFVList = IFormDataFieldValueRepo.GetDataByPropertyName(propertyName, obj.Id.ToString(), true, 0, 0, false);
                if (responseFDFVList.IsSuccess)
                {
                    obj.FormDataFieldIdFormDataFieldValues = (List<FormDataFieldValue>)responseFDFVList.Data;
                }

                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormDataField> objs = new List<FormDataField>();
            try
            {
                string query = "select * from form_data_fields";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormDataField
                        {
                            Id = x.Field<Int32>("id"),
                            FormTypeId = x.Field<Int32>("form_type_id"),
                            Name = x.Field<String>("name"),
                            FieldId = x.Field<String>("field_id"),
                            FieldSet = x.Field<Int32>("field_set"),
                            FieldType = x.Field<String>("field_type"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(formDataField =>
                {
                    //formDataField.FormTypeIdFormType = IFormTypeRepo.GetSingle(formDataField.FormTypeId);
                    string propertyName = nameof(FormDataFieldOption.FormDataFieldId);
                    Response responseFDFOList = IFormDataFieldOptionRepo.GetDataByPropertyName(propertyName, formDataField.Id.ToString(), true, 0, 0, false);
                    if (responseFDFOList.IsSuccess)
                    {
                        formDataField.FormDataFieldIdFormDataFieldOptions = (List<FormDataFieldOption>)responseFDFOList.Data;
                    }
                    Response responseFDFVList = IFormDataFieldValueRepo.GetDataByPropertyName(propertyName, formDataField.Id.ToString(), true, 0, 0, false);
                    if (responseFDFVList.IsSuccess)
                    {
                        formDataField.FormDataFieldIdFormDataFieldValues = (List<FormDataFieldValue>)responseFDFVList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_data_fields";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<FormDataField>(propertyName);
            List<FormDataField> objs = new List<FormDataField>();
            try
            {
                string query = "select * from form_data_fields where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from form_data_fields where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormDataField
                        {
                            Id = x.Field<Int32>("id"),
                            FormTypeId = x.Field<Int32>("form_type_id"),
                            Name = x.Field<String>("name"),
                            FieldId = x.Field<String>("field_id"),
                            FieldSet = x.Field<Int32>("field_set"),
                            FieldType = x.Field<String>("field_type"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(formDataField =>
                {
                    //formDataField.FormTypeIdFormType = IFormTypeRepo.GetSingle(formDataField.FormTypeId);
                    string propertyName1 = nameof(FormDataFieldOption.FormDataFieldId);
                    Response responseFDFOList = IFormDataFieldOptionRepo.GetDataByPropertyName(propertyName1, formDataField.Id.ToString(), true, 0, 0, false);
                    if (responseFDFOList.IsSuccess)
                    {
                        formDataField.FormDataFieldIdFormDataFieldOptions = (List<FormDataFieldOption>)responseFDFOList.Data;
                    }
                    Response responseFDFVList = IFormDataFieldValueRepo.GetDataByPropertyName(propertyName1, formDataField.Id.ToString(), true, 0, 0, false);
                    if (responseFDFVList.IsSuccess)
                    {
                        formDataField.FormDataFieldIdFormDataFieldValues = (List<FormDataFieldValue>)responseFDFVList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<FormDataField>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_data_fields where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from form_data_fields where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormDataField> objs = new List<FormDataField>();
            try
            {
                string query = "select * from form_data_fields where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormDataField
                        {
                            Id = x.Field<Int32>("id"),
                            FormTypeId = x.Field<Int32>("form_type_id"),
                            Name = x.Field<String>("name"),
                            FieldId = x.Field<String>("field_id"),
                            FieldSet = x.Field<Int32>("field_set"),
                            FieldType = x.Field<String>("field_type"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(formDataField =>
                {
                    //formDataField.FormTypeIdFormType = IFormTypeRepo.GetSingle(formDataField.FormTypeId);
                    string propertyName = nameof(FormDataFieldOption.FormDataFieldId);
                    Response responseFDFOList = IFormDataFieldOptionRepo.GetDataByPropertyName(propertyName, formDataField.Id.ToString(), true, 0, 0, false);
                    if (responseFDFOList.IsSuccess)
                    {
                        formDataField.FormDataFieldIdFormDataFieldOptions = (List<FormDataFieldOption>)responseFDFOList.Data;
                    }
                    Response responseFDFVList = IFormDataFieldValueRepo.GetDataByPropertyName(propertyName, formDataField.Id.ToString(), true, 0, 0, false);
                    if (responseFDFVList.IsSuccess)
                    {
                        formDataField.FormDataFieldIdFormDataFieldValues = (List<FormDataFieldValue>)responseFDFVList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_data_fields where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
