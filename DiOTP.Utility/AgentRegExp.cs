﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("agent_reg_exps")]
    public class AgentRegExp
    {
        [Database("id"), Display("id")]
        public Int32 id { get; set; }
        [Database("agent_reg_id"), Display("agent_reg_id")]
        public Int32 agent_reg_id { get; set; }
        [Database("employer_name"), Display("employer_name")]
        public String employer_name { get; set; }
        [Database("position_held"), Display("position_held")]
        public String position_held { get; set; }
        [Database("length_of_service_yrs"), Display("length_of_service_yrs")]
        public Int32? length_of_service_yrs { get; set; }
        [Database("annual_income"), Display("annual_income")]
        public String annual_income { get; set; }
        [Database("reason_for_leaving"), Display("reason_for_leaving")]
        public String reason_for_leaving { get; set; }
        [Database("is_active"), Display("is_active")]
        public Int32 is_active { get; set; }
        [Database("created_by"), Display("created_by")]
        public Int32? created_by { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime created_date { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
