using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class MaHolderRegService : IMaHolderRegService
    {
        private static readonly Lazy<IMaHolderRegRepo> lazy = new Lazy<IMaHolderRegRepo>(() => new MaHolderRegRepo());
        public static IMaHolderRegRepo IMaHolderRegRepo { get { return lazy.Value; } }
        public Response PostData(MaHolderReg obj)
        {
            Response response = new Response();
            try
            {
                response = IMaHolderRegRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<MaHolderReg> objs)
        {
            Int32 result = 0;
            try
            {
                result = IMaHolderRegRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(MaHolderReg obj)
        {
            Response response = new Response();
            try
            {
                response = IMaHolderRegRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<MaHolderReg> objs)
        {
            Int32 result = 0;
            try
            {
                result = IMaHolderRegRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public MaHolderReg DeleteData(Int32 Id)
        {
            MaHolderReg obj = new MaHolderReg();
            try
            {
                obj = IMaHolderRegRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IMaHolderRegRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            MaHolderReg obj = new MaHolderReg();
            try
            {
                response = IMaHolderRegRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                response = IMaHolderRegRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IMaHolderRegRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                response = IMaHolderRegRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IMaHolderRegRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderReg> objs = new List<MaHolderReg>();
            try
            {
                response = IMaHolderRegRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IMaHolderRegRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
