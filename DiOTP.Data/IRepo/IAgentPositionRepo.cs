﻿using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data.IRepo
{
    public interface IAgentPositionRepo : IDefaultInterface<AgentPosition>
    {
    }
}
