﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class CommissionMappings : System.Web.UI.Page
    {
        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());
        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }
        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT csm.*, cst.code as commission_setting_name, ufi.Fund_Name as fund_name FROM ");
            string mainQCount = (@"select count(*) as count from (");
            StringBuilder countFilter = new StringBuilder();
            filter.Append(@" comm_structure_mappings csm
                                join utmc_fund_information ufi on ufi.id = csm.fund_id 
                                join commission_settings cst on csm.commission_setting_id = cst.id
                                where 1=1 ");

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (ufi.Fund_Name like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or cst.code like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value == "0")
                    {
                        filter.Append(@"and csm.is_active in ('0') ");
                    }
                    else if (FilterValue.Value == "1")
                    {
                        filter.Append(@"and csm.is_active in ('1') ");
                    }
                    else
                    {

                    }
                }
                else
                {
                    filter.Append(@"and csm.is_active in ('0', '1') ");
                }


                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by csm." + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                {
                    countFilter.Append(filter);
                    filter.Append("order by csm.id desc");
                }


                int skip = 0, take = 0;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + countFilter.ToString() + ") a").Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }


                Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", skip, take, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var UsersDyn = responseUList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.CommStructMapDetail> commissionStructureDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.CommStructMapDetail>>(responseJSON);

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    //DiOTP.Utility.CommissionStructureDetail compareObj = new DiOTP.Utility.CommissionStructureDetail();
                    if (commissionStructureDetails.Count() > 0)
                    {
                        foreach (CommStructMapDetail commStructMapDetail in commissionStructureDetails)
                        {

                            asb.Append(@"<tr>
                                            <td class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRow' class='checkRow' value='" + commStructMapDetail.id + @"' /> <label>" + index + @"</label><br/>
                                                    </div>
                                                </div>
                                                <span class='row-status'>" + (commStructMapDetail.is_active == 1 ? "<span class='label label-success'>Active</span>" :
                                                    commStructMapDetail.is_active == 0 ? "<span class='label label-danger'>Inactive</span>" : "") + @"</span>
                                            </td>
                                            <td>
                                                " + commStructMapDetail.fund_name + @"
                                            </td>
                                            <td>
                                                " + commStructMapDetail.commission_setting_name + @"
                                            </td>

                                        </tr>");
                            index++;
                        }
                        commStructureTbody.InnerHtml = asb.ToString();
                    }


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseUList.Message + "\");", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(dynamic obj)
        {
            Response response = new Response();
            try
            {
                if (HttpContext.Current.Session["admin"] != null)
                {

                    User loginUser = (User)HttpContext.Current.Session["admin"];
                    String id = obj["Id"];
                    String fund_id = obj["ddlFundList"];
                    String commission_settings_id = obj["ddlCommSettings"];

                    if (fund_id == "")
                    {
                        response.IsSuccess = false;
                        response.Message = "Invalid Fund";
                        return response;
                    }
                    if (commission_settings_id == "")
                    {
                        response.IsSuccess = false;
                        response.Message = "Please Select Commission structure";
                        return response;
                    }

                    Response responsePost = new Response();

                    StringBuilder filter = new StringBuilder();
                    string mainQ = (@"SELECT csm.* FROM ");
                    StringBuilder countFilter = new StringBuilder();
                    filter.Append(@" comm_structure_mappings csm
                                join utmc_fund_information ufi on ufi.id = csm.fund_id 
                                join commission_settings cst on csm.commission_setting_id = cst.id
                                where csm.fund_id='" + fund_id + "' and csm.is_active=1 ");
                    Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", 0, 0, false, null, false, null, true);
                    if (responseUList.IsSuccess)
                    {
                        var UsersDyn = responseUList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                        List<CommStructureMapping> commissionStructureDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CommStructureMapping>>(responseJSON);
                        if (commissionStructureDetails.Count == 1)
                        {
                            CommStructureMapping commStructMapDetail = commissionStructureDetails.First();
                            commStructMapDetail.commission_setting_id = Convert.ToInt32(commission_settings_id);
                            responsePost = GenericService.UpdateData<CommStructureMapping>(commStructMapDetail);
                        }
                        else
                        {
                            CommStructureMapping commStructMapDetail = new CommStructureMapping();
                            commStructMapDetail.fund_id = Convert.ToInt32(fund_id);
                            commStructMapDetail.commission_setting_id = Convert.ToInt32(commission_settings_id);
                            commStructMapDetail.is_active = 1;
                            commStructMapDetail.created_date = DateTime.Now;
                            commStructMapDetail.created_by = loginUser.Id;
                            commStructMapDetail.updated_date = DateTime.Now;
                            commStructMapDetail.updated_by = loginUser.Id;
                            commStructMapDetail.status = 1;
                            responsePost = GenericService.PostData<CommStructureMapping>(commStructMapDetail);
                        }
                    }
                    else
                    {
                        response = responseUList;
                    }


                    if (!responsePost.IsSuccess)
                    {
                        response = responsePost;
                    }
                    else
                    {
                        response.IsSuccess = true;
                        response.Message = "Successfully updated!";
                    }

                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Session Expired!";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}