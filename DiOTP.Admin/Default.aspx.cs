﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class Default : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                navAccordion.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Default.aspx'", true);
            }
            else
            {
                User user = (User)(Session["admin"]);
                userName.InnerHtml = user.Username;

                bool isSuperAdmin = false;
                bool isMainAdmin = false;
                bool isAccountAdmin = false;
                bool isContentAdmin = false;

                bool isAdminOfficer = false;
                bool isAdminManager = false;

                if (user.Status == 1)
                {
                    if (Session["isSuperAdmin"] != null && Session["isSuperAdmin"].ToString() == "1")
                        isSuperAdmin = true;
                    if (Session["isMainAdmin"] != null && Session["isMainAdmin"].ToString() == "1")
                        isMainAdmin = true;
                    if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                        isAccountAdmin = true;
                    if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                        isContentAdmin = true;
                    if (Session["isAdminOfficer"] != null && Session["isAdminOfficer"].ToString() == "1")
                        isAdminOfficer = true;
                    if (Session["isAdminManager"] != null && Session["isAdminManager"].ToString() == "1")
                        isAdminManager = true;

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append(@"<li>
                                <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Dashboard.aspx' data-title='Dashboard' data-closable='false'>
                                           <span> Dashboard </span>
                                       </a>
                                   </li>");

                    string adminListHTML = "";
                    if (isSuperAdmin || isMainAdmin)
                        adminListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Admins.aspx' data-title='Admin List'>
                                        <span>Admin List</span>
                                    </a></li>";
                    if (isSuperAdmin || isMainAdmin || isContentAdmin)
                        adminListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='InstructorList.aspx' data-title='Instructor List'>
                                        <span>Instructor List</span>
                                    </a></li>";
                        adminListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='InternalStaffList.aspx' data-title='Internal Staff List'>
                                            <span>Internal Staff List</span>
                                            </a></li>";
                    // Users
                    if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-users'></i>
                                    <span>Users</span>
                                </a>
                                <ul class='sub'>
                                    " + adminListHTML + @"
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='UserAccounts.aspx' data-title='Individual Accounts'>
                                        <span>Individual Accounts</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CorporateAccounts.aspx' data-title='Corporate Accounts'>
                                        <span>Corporate Accounts</span>
                                    </a></li>
                                    <!--<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='KYCVerification.aspx' data-title='KYC Verification'>
                                        <span>KYC Verification</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AddressVerification.aspx' data-title='Address Verification'>
                                        <span>Address Verification</span>
                                    </a></li>-->
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AddressChanging.aspx' data-title='MA Address Verification'>
                                        <span>MA Address Verification</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BankDetailsVerification.aspx' data-title='Bank Details Verification'>
                                        <span>Bank Details Verification</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='MABanks.aspx' data-title='MA Bank Binding List'>
                                        <span>MA Bank Binding List</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='HardcopyRequest.aspx' data-title='Hardcopy Request'>
                                        <span>Hardcopy Request</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='ChangeLog.aspx?pageLength=50' data-title='Audit Log'>
                                        <span>Audit Log</span>
                                    </a></li>" + (isSuperAdmin == true || isMainAdmin == true ? @"
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AdminLog.aspx?pageLength=50' data-title='Admin Log'>
                                        <span>AdminLog</span>
                                    </a></li>" : "") + @"
                                    <!--<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='DistributionInstruction.aspx' data-title='Distribution Instruction'>
                                        <span>Distribution Instruction</span>
                                    </a></li>-->
                                </ul>
                            </li>");
                    string agentListHTML = "";
                    if (isSuperAdmin || isMainAdmin || isAdminOfficer || isAdminManager)
                    {
                        agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Agents.aspx' data-title='Agent List'>
                                        <span>Agent List</span>
                                    </a></li>";
                        agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AgentRequests.aspx' data-title='Agent Recruitment'>
                                        <span>Agent Recruitment</span>
                                    </a></li>";
                        agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AgentRankListing.aspx' data-title='Agent Rank'>
                                        <span>Agent Rank Management</span>
                                    </a></li>";
                        //agentListHTML += @"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Enrollments.aspx' data-title='E-Learning Enrollment'>
                        //                <span>Enrollments</span>
                        //            </a></li>";
                    }
                    if (isSuperAdmin || isMainAdmin || isAdminOfficer || isAdminManager)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-briefcase'></i>
                                    <span>Agents</span>
                                </a>
                                <ul class='sub'>
                                    " + agentListHTML + @"
                                </ul>
                            </li>");
                    //E-Learning

                    if (isSuperAdmin || isMainAdmin || isAdminOfficer || isAdminManager) {
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-file-text'></i>
                                    <span>E-Learning</span>
                                </a>
                                <ul class='sub'>
                                   <li><a href = 'javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Enrollments.aspx' data-title='Enrollments'>
                                        <span>E-Learning Enrollments</span>
                                    </a></li>");
                        //if (isSuperAdmin || isMainAdmin || isContentAdmin || isAdminOfficer)
                        //{
                        //    stringBuilder.Append(@"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CourseListing.aspx' data-title='Course Listing'>
                        //                <span>Course Listing</span>
                        //            </a></li>");
                        //}

                        stringBuilder.Append( @"</ul>
                          </li>");
                    }                       
                    if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-users'></i>
                                    <span>Account Opening</span>
                                </a>
                                <ul class='sub'>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AccountOpeningRequest.aspx' data-title='Account Opening Requests'>
                                        <span>Account Opening Requests</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CorporateAccountOpeningDocs.aspx' data-title='Corporate Accounts Opening Docs'>
                                        <span>Corporate Accounts Opening Docs</span>
                                    </a></li>
                                    
                                </ul>
                            </li>");

                    // Funds
                    if (isSuperAdmin || isMainAdmin || isContentAdmin)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-money'></i>
                                    <span>Funds</span>
                                </a>
                                <ul class='sub'>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='FundsList.aspx' data-title='Funds List'>
                                        <span>Funds List</span>
                                    </a></li>
                                </ul>
                            </li>");
                    // Transactions
                    if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-dollar'></i>
                                    <span>Transactions</span>
                                </a>
                                <ul class='sub'>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Payments.aspx' data-title='Payments'>
                                        <span>Payments</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Transactions.aspx' data-title='Transactions'>
                                        <span>Transactions</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='RegularSavingPlans.aspx' data-title='Regular Saving Enrollment'>
                                        <span>Regular Saving Enrollment</span>
                                    </a></li>
                                </ul>
                            </li>");
                    // Statements
                    if (isSuperAdmin || isMainAdmin || isAccountAdmin)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-list'></i>
                                    <span>Statements</span>
                                </a>
                                <ul class='sub'>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='StatementLinks.aspx' data-title='Statement Links'>
                                        <span>Update Statement Links</span>
                                    </a></li>
                                </ul>
                            </li>");
                    // Content Management
                    StringBuilder subContentMenu = new StringBuilder();
                    if (isSuperAdmin || isMainAdmin || isContentAdmin || isAdminOfficer)
                    {
                        subContentMenu.Append(@"<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CourseListing.aspx' data-title='Course Listing'>
                                        <span>Course Listing</span>
                                    </a></li>");
                    }
                    if (isSuperAdmin || isMainAdmin || isContentAdmin)
                    {
                        subContentMenu.Append(@"
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BannerListing.aspx' data-title='Banner Listing'>
                                        <span>Banner Listing</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionDefinitions.aspx' data-title='Commission Listing'>
                                        <span>Commission Listing</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionSettings.aspx' data-title='Commission Settings Listing'>
                                        <span>Commission Settings Listing</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AnnouncementListing.aspx' data-title='Announcement Listing'>
                                        <span>Announcement Listing</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item hide' data-url='LanguageDirectory.aspx' data-title='Language Directory'>
                                        <span>Language Directory</span>
                                    </a></li>");
                    }
                    if (isSuperAdmin || isMainAdmin || isContentAdmin || isAdminOfficer)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-list-alt'></i>
                                    <span>Content Management</span>
                                </a>
                                <ul class='sub'>
                                    "+ subContentMenu.ToString() + @"
                                </ul>
                            </li>");

                    if (isSuperAdmin || isMainAdmin || isContentAdmin)
                        stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-cogs'></i>
                                    <span>Configuration</span>
                                </a>
                                <ul class='sub'>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='ExamSchedule.aspx' data-title='Exam Schedules'>
                                        <span>Exam Schedules</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionStructure.aspx' data-title='Commission Structure'>
                                        <span>Commission Structure</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='CommissionMappings.aspx' data-title='Commission Mappings'>
                                        <span>Commission Mappings</span>
                                    </a></li>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AgentTraineeMaterialListing.aspx' data-title='Trainee Materials'>
                                        <span>Trainee Materials</span>
                                    </a></li>
                                </ul>
                            </li>");

                    if (isAdminOfficer)stringBuilder.Append(@"<li class='sub-menu'>
                                <a href='javascript:;'>
                                    <i class='fa fa-cogs'></i>
                                    <span>Configuration</span>
                                </a>
                                <ul class='sub'>
                                    <li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='ExamSchedule.aspx' data-title='Exam Schedules'>
                                        <span>Exam Schedules</span>
                                    </a></li>
                                </ul>
                            </li>");
                    //<li><a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BankList.aspx' data-title='Bank List'>
                    //                    <span>Bank List</span>
                    //                </a></li>
                    navAccordion.InnerHtml = stringBuilder.ToString();
                }
                else
                {
                    navAccordion.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Account is deactivated.'); window.location.href='Login.aspx?redirectUrl=Default.aspx'", true);
                }
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            if (loginUser != null)
            {
                //Audit Log starts here
                AdminLogMain alm = new AdminLogMain()
                {
                    Description = "Admin Logout Successful",
                    TableName = "users",
                    UpdatedDate = DateTime.Now,
                    UserId = loginUser.Id
                };
                Response responseLog = IAdminLogMainService.PostData(alm);
                if (!responseLog.IsSuccess)
                {
                    //Audit log failed
                }
                else
                {

                }
                //Audit Log Ends here

                Session.Clear();
                Session.Abandon();
            }
            else
            {

            }

            Response.Redirect("Login.aspx", false);

        }
    }
}