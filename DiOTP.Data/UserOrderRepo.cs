using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UserOrderRepo : IUserOrderRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        IUserOrderStatementRepo IUserOrderStatementRepo = new UserOrderStatementRepo();

        public Response PostData(UserOrder obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserOrder>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserOrder> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserOrder>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserOrder obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserOrder>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserOrder> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserOrder>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserOrder DeleteData(Int32 Id)
        {
            Response responseUO = GetSingle(Id);
            if (responseUO.IsSuccess)
            {
                UserOrder obj = (UserOrder)responseUO.Data;
                try
                {
                    string query = obj.ObjectToQuery<UserOrder>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUO = GetSingle(Id);
                    if (responseUO.IsSuccess)
                    {
                        UserOrder obj = (UserOrder)responseUO.Data;
                        query += obj.ObjectToQuery<UserOrder>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserOrder obj = new UserOrder();
            try
            {
                string query = "select * from user_orders where ID = " + Id + "";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UserOrder
                       {
                           Id = x.Field<Int32>("id"),
                           RefNo = x.Field<String>("ref_no"),
                           OrderType = x.Field<Int32>("order_type"),
                           FundId = x.Field<Int32>("fund_id"),
                           ToFundId = x.Field<Int32>("to_fund_id"),
                           ToAccountId = x.Field<Int32>("to_account_id"),
                           UserId = x.Field<Int32>("user_id"),
                           UserAccountId = x.Field<Int32>("user_account_id"),
                           OrderNo = x.Field<String>("order_no"),
                           PaymentMethod = x.Field<String>("payment_method"),
                           Amount = x.Field<Decimal>("amount"),
                           Units = x.Field<Decimal>("units"),
                           TransId = x.Field<Int32>("trans_id"),
                           TransNo = x.Field<String>("trans_no"),
                           CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                           UpdatedDate = x.Field<DateTime?>("updated_date"),
                           IsTaxInvoice = x.Field<Int32>("is_tax_invoice"),
                           IsCas = x.Field<Int32>("is_cas"),
                           IsCreditNote = x.Field<Int32>("is_credit_note"),
                           Status = x.Field<Int32>("status"),
                           ConsultantId = x.Field<String>("consultantID"),
                           RejectReason = x.Field<String>("reject_reason"),
                           OrderStatus = x.Field<Int32>("order_status"),
                           FpxTransactionId = x.Field<String>("fpx_transaction_id"),
                           FpxStatus = x.Field<String>("fpx_status"),
                           BankCode = x.Field<String>("fpx_bank_code"),
                           PaymentDate = x.Field<DateTime?>("payment_date"),
                           SettlementDate = x.Field<DateTime?>("settlement_date"),
                           DistributionInstruction = x.Field<Int32>("distribution_instruction"),
                           BankId = x.Field<Int32>("bank_id"),
                           TransAmt = x.Field<Decimal?>("trans_amt") == null ? 0 : x.Field<Decimal>("trans_amt"),
                           TransUnits = x.Field<Decimal?>("trans_units") == null ? 0 : x.Field<Decimal>("trans_units"),
                           AppFee = x.Field<Decimal?>("app_fee") == null ? 0 : x.Field<Decimal>("app_fee"),
                           ExitFee = x.Field<Decimal?>("exit_fee") == null ? 0 : x.Field<Decimal>("exit_fee"),
                           FeeAmt = x.Field<Decimal?>("fee_amt") == null ? 0 : x.Field<Decimal>("fee_amt"),
                           NetAmt = x.Field<Decimal?>("net_amt") == null ? 0 : x.Field<Decimal>("net_amt"),
                           TransPr = x.Field<Decimal?>("trans_pr") == null ? 0 : x.Field<Decimal>("trans_pr"),
                           ConfirmationDate = x.Field<DateTime?>("confirmation_date"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                Response responseUOSList = IUserOrderStatementRepo.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), obj.RefNo, true, 0, 0, false);
                if (responseUOSList.IsSuccess)
                {
                    obj.RefNoUserOrderStatements = (List<UserOrderStatement>)responseUOSList.Data;
                }
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserOrder> objs = new List<UserOrder>();
            try
            {
                string query = "select * from user_orders ";
                //string query = "select * from user_orders group by order_no";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserOrder
                        {
                            Id = x.Field<Int32>("id"),
                            RefNo = x.Field<String>("ref_no"),
                            OrderType = x.Field<Int32>("order_type"),
                            FundId = x.Field<Int32>("fund_id"),
                            ToFundId = x.Field<Int32>("to_fund_id"),
                            ToAccountId = x.Field<Int32>("to_account_id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            PaymentMethod = x.Field<String>("payment_method"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            TransId = x.Field<Int32>("trans_id"),
                            TransNo = x.Field<String>("trans_no"),
                            CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            IsTaxInvoice = x.Field<Int32>("is_tax_invoice"),
                            IsCas = x.Field<Int32>("is_cas"),
                            IsCreditNote = x.Field<Int32>("is_credit_note"),
                            Status = x.Field<Int32>("status"),
                            ConsultantId = x.Field<String>("consultantID"),
                            RejectReason = x.Field<String>("reject_reason"),
                            OrderStatus = x.Field<Int32>("order_status"),
                            FpxTransactionId = x.Field<String>("fpx_transaction_id"),
                            FpxStatus = x.Field<String>("fpx_status"),
                            BankCode = x.Field<String>("fpx_bank_code"),
                            PaymentDate = x.Field<DateTime?>("payment_date"),
                            SettlementDate = x.Field<DateTime?>("settlement_date"),
                           DistributionInstruction = x.Field<Int32>("distribution_instruction"),
                           BankId = x.Field<Int32>("bank_id"),
                            TransAmt = x.Field<Decimal?>("trans_amt") == null ? 0 : x.Field<Decimal>("trans_amt"),
                            TransUnits = x.Field<Decimal?>("trans_units") == null ? 0 : x.Field<Decimal>("trans_units"),
                            AppFee = x.Field<Decimal?>("app_fee") == null ? 0 : x.Field<Decimal>("app_fee"),
                            ExitFee = x.Field<Decimal?>("exit_fee") == null ? 0 : x.Field<Decimal>("exit_fee"),
                            FeeAmt = x.Field<Decimal?>("fee_amt") == null ? 0 : x.Field<Decimal>("fee_amt"),
                            NetAmt = x.Field<Decimal?>("net_amt") == null ? 0 : x.Field<Decimal>("net_amt"),
                            TransPr = x.Field<Decimal?>("trans_pr") == null ? 0 : x.Field<Decimal>("trans_pr"),
                            ConfirmationDate = x.Field<DateTime?>("confirmation_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(obj =>
                {
                    Response responseUOSList = IUserOrderStatementRepo.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), obj.RefNo, true, 0, 0, false);
                    if (responseUOSList.IsSuccess)
                    {
                        obj.RefNoUserOrderStatements = (List<UserOrderStatement>)responseUOSList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_orders";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UserOrder>(propertyName);
            List<UserOrder> objs = new List<UserOrder>();
            try
            {
                string query = "select * from user_orders where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select * from user_orders where " + columnName + " != '" + propertyValue + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserOrder
                        {
                            Id = x.Field<Int32>("id"),
                            RefNo = x.Field<String>("ref_no"),
                            OrderType = x.Field<Int32>("order_type"),
                            FundId = x.Field<Int32>("fund_id"),
                            ToFundId = x.Field<Int32>("to_fund_id"),
                            ToAccountId = x.Field<Int32>("to_account_id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            PaymentMethod = x.Field<String>("payment_method"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            TransId = x.Field<Int32>("trans_id"),
                            TransNo = x.Field<String>("trans_no"),
                            CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            IsTaxInvoice = x.Field<Int32>("is_tax_invoice"),
                            IsCas = x.Field<Int32>("is_cas"),
                            IsCreditNote = x.Field<Int32>("is_credit_note"),
                            Status = x.Field<Int32>("status"),
                            ConsultantId = x.Field<String>("consultantID"),
                            RejectReason = x.Field<String>("reject_reason"),
                            OrderStatus = x.Field<Int32>("order_status"),
                            FpxTransactionId = x.Field<String>("fpx_transaction_id"),
                            FpxStatus = x.Field<String>("fpx_status"),
                            BankCode = x.Field<String>("fpx_bank_code"),
                            PaymentDate = x.Field<DateTime?>("payment_date"),
                            SettlementDate = x.Field<DateTime?>("settlement_date"),
                           DistributionInstruction = x.Field<Int32>("distribution_instruction"),
                           BankId = x.Field<Int32>("bank_id"),
                            TransAmt = x.Field<Decimal?>("trans_amt") == null ? 0 : x.Field<Decimal>("trans_amt"),
                            TransUnits = x.Field<Decimal?>("trans_units") == null ? 0 : x.Field<Decimal>("trans_units"),
                            AppFee = x.Field<Decimal?>("app_fee") == null ? 0 : x.Field<Decimal>("app_fee"),
                            ExitFee = x.Field<Decimal?>("exit_fee") == null ? 0 : x.Field<Decimal>("exit_fee"),
                            FeeAmt = x.Field<Decimal?>("fee_amt") == null ? 0 : x.Field<Decimal>("fee_amt"),
                            NetAmt = x.Field<Decimal?>("net_amt") == null ? 0 : x.Field<Decimal>("net_amt"),
                            TransPr = x.Field<Decimal?>("trans_pr") == null ? 0 : x.Field<Decimal>("trans_pr"),
                            ConfirmationDate = x.Field<DateTime?>("confirmation_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(obj =>
                {
                    Response responseUOSList = IUserOrderStatementRepo.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), obj.RefNo, true, 0, 0, false);
                    if (responseUOSList.IsSuccess)
                    {
                        obj.RefNoUserOrderStatements = (List<UserOrderStatement>)responseUOSList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UserOrder>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_orders where " + columnName + " = '" + propertyValue + "'";
                if (!isEqual)
                    query = "select count(*) from user_orders where " + columnName + " != '" + propertyValue + "'";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserOrder> objs = new List<UserOrder>();
            try
            {
                string query = "select *, sum(amount) as SumAmount, sum(units) as SumUnits from user_orders where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                if( dt.Rows.Count != 0 && !dt.Rows[0].IsNull(0))
                objs = (from x in dt.AsEnumerable()
                        select new UserOrder
                        {
                            Id = x.Field<Int32>("id"),
                            RefNo = x.Field<String>("ref_no"),
                            OrderType = x.Field<Int32>("order_type"),
                            FundId = x.Field<Int32>("fund_id"),
                            ToFundId = x.Field<Int32>("to_fund_id"),
                            ToAccountId = x.Field<Int32>("to_account_id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            PaymentMethod = x.Field<String>("payment_method"),
                            Amount = x.Field<Decimal>("SumAmount"),
                            Units = x.Field<Decimal>("SumUnits"),
                            TransId = x.Field<Int32>("trans_id"),
                            TransNo = x.Field<String>("trans_no"),
                            CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            IsTaxInvoice = x.Field<Int32>("is_tax_invoice"),
                            IsCas = x.Field<Int32>("is_cas"),
                            IsCreditNote = x.Field<Int32>("is_credit_note"),
                            Status = x.Field<Int32>("status"),
                            ConsultantId = x.Field<String>("consultantID"),
                            RejectReason = x.Field<String>("reject_reason"),
                            OrderStatus= x.Field<Int32>("order_status"),
                            FpxTransactionId = x.Field<String>("fpx_transaction_id"),
                            FpxStatus = x.Field<String>("fpx_status"),
                            BankCode = x.Field<String>("fpx_bank_code"),
                            PaymentDate = x.Field<DateTime?>("payment_date"),
                            SettlementDate = x.Field<DateTime?>("settlement_date"),
                           DistributionInstruction = x.Field<Int32>("distribution_instruction"),
                           BankId = x.Field<Int32>("bank_id"),
                            TransAmt = x.Field<Decimal?>("trans_amt") == null ? 0 : x.Field<Decimal>("trans_amt"),
                            TransUnits = x.Field<Decimal?>("trans_units") == null ? 0 : x.Field<Decimal>("trans_units"),
                            AppFee = x.Field<Decimal?>("app_fee") == null ? 0 : x.Field<Decimal>("app_fee"),
                            ExitFee = x.Field<Decimal?>("exit_fee") == null ? 0 : x.Field<Decimal>("exit_fee"),
                            FeeAmt = x.Field<Decimal?>("fee_amt") == null ? 0 : x.Field<Decimal>("fee_amt"),
                            NetAmt = x.Field<Decimal?>("net_amt") == null ? 0 : x.Field<Decimal>("net_amt"),
                            TransPr = x.Field<Decimal?>("trans_pr") == null ? 0 : x.Field<Decimal>("trans_pr"),
                            ConfirmationDate = x.Field<DateTime?>("confirmation_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(obj =>
                {
                    Response responseUOSList = IUserOrderStatementRepo.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), obj.RefNo, true, 0, 0, false);
                    if (responseUOSList.IsSuccess)
                    {
                        obj.RefNoUserOrderStatements = (List<UserOrderStatement>)responseUOSList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(distinct order_no) from user_orders where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        

        public int GetCountByColumnGroup(string propertyName)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UserOrder>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from (SELECT * FROM user_orders group by " + columnName + ") a;";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }

        public int GetCountByColumnGroup(string filter, string groupByColumnName)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from (SELECT * FROM user_orders where "+ filter + " group by " + groupByColumnName + ") a;";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
